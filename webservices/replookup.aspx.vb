﻿Imports Config, Common

Partial Class RepLookup
    Inherits System.Web.UI.Page

    Private strSearch As String = HttpContext.Current.Request("text")
    Private strLenderType As String = HttpContext.Current.Request("CompanyID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub repLookup()
        Dim strJSON As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName, PostalDistrict FROM vwreplookup " & _
            "WHERE CompanyID = '" & CompanyID & "'"
        If (checkValue(strSearch)) Then
            strSQL += "AND (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1)))"
        End If
        strSQL += " ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        Dim x As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (x > 0) Then
                    strJSON += ",{""" & Row.Item("UserID") & """:""" & Row.Item("UserFullName") & "|" & Row.Item("PostalDistrict") & """}"
                Else
                    strJSON += "{""" & Row.Item("UserID") & """:""" & Row.Item("UserFullName") & "|" & Row.Item("PostalDistrict") & """}"
                End If
                x += 1
            Next
        End If
        dsCache = Nothing
        Response.Write(strJSON)
    End Sub

End Class
