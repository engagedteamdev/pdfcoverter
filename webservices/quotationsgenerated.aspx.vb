﻿Imports Config, Common

Partial Class quotesgenerated
    Inherits System.Web.UI.Page
	
    Public Sub quotesgenerated()
		
      
            Dim strXML As String = ""
            Dim strSQL As String = "select * from vwlivequotecases"
           

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<cases>"
                    strXML += "<NumberOfQuotes>" & Row.Item("Apps") & "</NumberOfQuotes>"
                    strXML += "<CallCentreUser>" & Row.Item("Call Centre User") & "</CallCentreUser>"
					strXML += "<Month>" & Row.Item("Month") & "</Month>"
					strXML += "<Year>" & Row.Item("Year") & "</Year>"
                    strXML += "</cases>"
                Next
                strXML += "</CaseStatus>"
            Else
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus><Error>No leads found</Error></CaseStatus>"
            End If
            dsCache = Nothing
            Response.ContentType = "text/xml"
            Response.Write(strXML)       
    End Sub

End Class