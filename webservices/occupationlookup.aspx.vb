﻿Imports Config, Common

Partial Class OccupationLookup
    Inherits System.Web.UI.Page

    Private strSearch As String = HttpContext.Current.Request("text")

    Public Sub occupationLookup()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        Response.ContentType = "text/xml"
        Dim strXML As String = ""
        Dim strSQL As String = "SELECT OccupationDescription FROM tbloccupationcodes " & _
            "WHERE OccupationActive = 1 "
        If (checkValue(strSearch)) Then
            strSQL += "AND OccupationDescription LIKE '%" & strSearch & "%' "
        End If
        strSQL += "ORDER BY OccupationDescription"
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
            strXML += "<Occupations>"
            For Each Row As DataRow In dsCache.Rows
                strXML += "<Occupation><Name>" & Replace(Row.Item("OccupationDescription"), "& ", "&amp; ") & "</Name></Occupation>"
            Next
            strXML += "</Occupations>"
        End If
        dsCache = Nothing
        Response.Write(strXML)
    End Sub

End Class
