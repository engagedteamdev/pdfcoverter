﻿Imports Config, Common

Partial Class MediaCampaignLookup
    Inherits System.Web.UI.Page

    Private strSearch As String = HttpContext.Current.Request("text")

    Public Sub mediaCampaignLookup()
        Dim strJSON As String = ""
        Dim strSQL As String = "SELECT MC.MediaCampaignID, MC.MediaCampaignFriendlyNameInbound FROM tblmediacampaigns MC INNER JOIN tblmediacampaigntelephonenumbers TEL ON TEL.MediaCampaignTelephoneNumberID = MC.MediaCampaignTelephoneNumberIDInbound " & _
            "WHERE NOT MediaCampaignFriendlyNameInbound IS NULL "
        If (checkValue(strSearch)) Then
            strSQL += "AND MediaCampaignFriendlyNameInbound LIKE '%" & strSearch & "%' OR MediaCampaignTelephoneNumberFriendlyName LIKE '%" & strSearch & "%' OR MediaCampaignTelephoneNumber LIKE '%" & strSearch & "%' "
        End If
        strSQL += "AND MC.MediaCampaignActive = 1 AND MC.CompanyID = '" & CompanyID & "' ORDER BY MC.MediaCampaignName"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblmediacampaigns")
        Dim dsMediaCampaigns As DataTable = objDataSet.Tables("tblmediacampaigns")
        Dim x As Integer = 0
        If (dsMediaCampaigns.Rows.Count > 0) Then
            For Each Row As DataRow In dsMediaCampaigns.Rows
                If (x > 0) Then
                    strJSON += ",{""" & Row.Item("MediaCampaignID") & """:""" & Row.Item("MediaCampaignFriendlyNameInbound") & """}"
                Else
                    strJSON += "{""" & Row.Item("MediaCampaignID") & """:""" & Row.Item("MediaCampaignFriendlyNameInbound") & """}"
                End If
                x += 1
            Next
        End If
        dsMediaCampaigns.Clear()
        dsMediaCampaigns = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Response.Write(strJSON)
    End Sub

End Class
