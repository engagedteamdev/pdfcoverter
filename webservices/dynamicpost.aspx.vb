﻿Imports Config
Imports Common

Partial Class DynamicSocialPost
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private UserID As String = HttpContext.Current.Request("UserID"), SocialPostID As String = HttpContext.Current.Request("SocialPostID"), ParentPostID As String = HttpContext.Current.Request("ParentSocialPostID"), strSocialPost As String = HttpContext.Current.Request("frmSocialPost"), strDelete As String = HttpContext.Current.Request("strDelete")
	Private UserSuperAdmin As String = HttpContext.Current.Request("UserSuperAdmin"), strUserName As String = "", intCompanyID As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub saveDynamicSocialPost()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        If (strDelete = "Y") Then
            Dim strQry As String = "UPDATE tblsocialposts SET SocialPostActive = 0 WHERE SocialPostID = '" & SocialPostID & "'"
            executeNonQuery(strQry)
            Response.Write("1")
        Else
			If (UserSuperAdmin = "True") Then
				intCompanyID = 0
			Else
				intCompanyID = CompanyID
			End If
            Dim strQry As String = _
                "INSERT INTO tblsocialposts (CompanyID, ParentSocialPostID, SocialPostText, SocialPostUserID) " & _
                "VALUES (" & formatField(intCompanyID, "N", 0) & ", " & _
                formatField(ParentPostID, "N", 0) & ", " & _
                formatField(Replace(strSocialPost, vbLf, "<br />"), "", "") & ", " & _
                formatField(UserID, "N", getSystemUser()) & ") "
            SocialPostID = executeIdentityQuery(strQry)
			Dim strUserImage As String = "user"
			If (getUserAccessLevel(UserID, "Make Posts")) Then
				If (UserSuperAdmin = "True") Then
					strUserName = "Engaged Solutions"
					strUserImage = "engaged-solutions"
				Else
					strUserName = getAnyFieldCached(Cache, "UserFullName", "tblusers", "UserID", UserID)
					strUserImage = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))				
				End If
            End If
            'Social.postToTwitter("status=" & strSocialPost & "&include_entities=true", "XbRbWaQ4o6FkHCiPiolK0PWtf")
            Response.Write("1|" & strUserName & "|" & ddmmyyhhmmss2ddmmhhmm(Config.DefaultDateTime) & "|" & UCase(MonthName(Config.DefaultDate.Month, True)) & "|" & padZeros(Config.DefaultDate.Day, 2) & "|" & strUserImage & "|" & SocialPostID)
        End If
    End Sub

End Class
