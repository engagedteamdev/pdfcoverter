﻿Imports Config, Common

Partial Class Messages
    Inherits System.Web.UI.Page

    Private intMessageToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkMessages()
        Dim strReminders As String = "<Messages>"
        Dim strQry As String = "SELECT COUNT(*) FROM tblmessages WHERE CompanyID = '" & CompanyID & "' AND MessageToUserID = '" & intMessageToUserID & "' AND MessageReadDate IS NULL"
        Dim strCount As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
        strReminders += "<Unread>" & strCount & "</Unread>"
        strQry = "SELECT TOP 10 tblmessages.*, tblusers.UserFullName FROM tblmessages INNER JOIN tblusers ON tblusers.UserID = tblmessages.MessageFromUserID WHERE tblmessages.CompanyID = '" & CompanyID & "' AND MessageToUserID = '" & intMessageToUserID & "' AND MessageReadDate IS NULL AND DATEDIFF(MI, MessageUpdatedDate, GETDATE()) >= 10 ORDER BY MessageUpdatedDate ASC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                'If (x = 0) Then
                executeNonQuery("UPDATE tblmessages SET MessageUpdatedDate = GETDATE() WHERE MessageID = " & Row.Item("MessageID"))
                'End If
                strReminders += "<Message><From>" & Row.Item("UserFullName") & "</From><Subject>" & Row.Item("MessageSubject") & "</Subject></Message>"
                x += 1
            Next
        End If
        strReminders += "</Messages>"
        Response.ContentType = "text/xml"
        Response.Write(strReminders)
    End Sub

End Class
