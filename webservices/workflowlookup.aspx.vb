﻿Imports Config
Imports Common

Partial Class WorkflowLookup
    Inherits System.Web.UI.Page

    Private intWorkflowID As String = HttpContext.Current.Request("frmWorkflowID")

    Public Sub workflowLookup()
        Dim intRecordsWaiting As String = ""
		Dim intRecords As String = ""
		
		If (Not checkValue(intWorkflowID)) Then
			intRecordsWaiting = "0"
			intRecords = "0"
        Else
		
			Dim strWorkflowQuery As String = getAnyField("WorkflowQuery", "tblworkflows", "WorkflowID", intWorkflowID)
			
            Dim strSQL As String = "SELECT Count(AppID) FROM vwworkflow WHERE (" & strWorkflowQuery & ") AND NextCallDate < GETDATE() AND CompanyID = '" & CompanyID & "' "
			Dim objDatabase As DatabaseManager = New DatabaseManager
			Dim objResult As Object = objDatabase.executeScalar(strSQL)
			If (objResult IsNot Nothing) Then
				intRecordsWaiting = objResult.ToString
			Else
				intRecordsWaiting = "0"
			End If
			objResult = Nothing
			objDatabase = Nothing

            Dim strSQL2 As String = "SELECT Count(AppID) FROM vwworkflow WHERE (" & strWorkflowQuery & ") AND CompanyID = '" & CompanyID & "' "
			Dim objDatabase2 As DatabaseManager = New DatabaseManager
			Dim objResult2 As Object = objDatabase2.executeScalar(strSQL2)
			If (objResult2 IsNot Nothing) Then
				intRecords = objResult2.ToString
			Else
				intRecords = "0"
			End If
			objResult2 = Nothing
			objDatabase2 = Nothing				

		End If
		
		Response.Write(intRecordsWaiting & "|" & intRecords)

    End Sub

End Class
