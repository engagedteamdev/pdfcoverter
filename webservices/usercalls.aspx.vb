﻿Imports Config, Common

Partial Class Calls
    Inherits System.Web.UI.Page

    Private intReminderAssignedToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkActiveUsers()
        Dim strCalls As String = "<CallUsers>"
        Dim strQry As String = "SELECT * FROM vwcallactiveusers WHERE CompanyID = '" & CompanyID & "' ORDER BY TotalDuration DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                strCalls += "<CallUser><UserID>" & Row.Item("UserID") & "</UserID><UserName>" & Row.Item("UserFullName") & "</UserName><InboundCalls>" & Row.Item("InboundCalls") & "</InboundCalls><OutboundCalls>" & Row.Item("OutboundCalls") & "</OutboundCalls><TotalDuration>" & ss2hhmmss(Row.Item("TotalDuration")) & "</TotalDuration></CallUser>"
            Next
        End If
        strCalls += "</CallUsers>"
        Response.ContentType = "text/xml"
        Response.Write(strCalls)
    End Sub

End Class
