﻿Imports Config
Imports Common

Partial Class DynamicNote
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID"), UserID As String = HttpContext.Current.Request("UserID"), NoteID As String = HttpContext.Current.Request("NoteID"), strNote As String = HttpContext.Current.Request("frmNote"), strDelete As String = HttpContext.Current.Request("strDelete"), strSticky As String = HttpContext.Current.Request("frmSticky")

    Public Sub saveDynamicNote()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        'If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        'End If
        If (strDelete = "Y") Then
            If (HttpContext.Current.Request("strBusiness") = "Y") Then
                Dim strQry As String = "UPDATE tblbusinessobjectnotes SET NoteActive = 0 WHERE NoteID = '" & NoteID & "'"
                executeNonQuery(strQry)
                Response.Write("1")
            Else
                Dim strQry As String = "UPDATE tblnotes SET NoteActive = 0 WHERE NoteID = '" & NoteID & "'"
                executeNonQuery(strQry)
                Response.Write("1")
            End If
        Else
            If (Len(AppID) < 7) Then
                If (strSticky = "1") Then
                    Dim intRowsAffected As Integer = 0
                    Dim strQry As String = _
                        "UPDATE tblbusinessobjectnotes SET " & _
                            "Note           = " & formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                            "CreatedDate    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                            "CreatedUserID  = " & formatField(UserID, "N", getSystemUser()) & " " & _
                            "WHERE BusinessObjectID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteType = 1 AND NoteActive = 1"
                    intRowsAffected = executeRowsAffectedQuery(strQry)
                    NoteID = New Caching(Nothing, "SELECT NoteID FROM tblbusinessobjectnotes WHERE BusinessObjectID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteType = 1 AND NoteActive = 1", "", "", "").returnCacheString
                    If (intRowsAffected = 0) Then
                        strQry = _
                        "INSERT INTO tblbusinessobjectnotes(CompanyID, BusinessObjectID, Note, NoteType, CreatedDate, CreatedUserID) " & _
                        "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
                        formatField(AppID, "N", 0) & ", " & _
                        formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                        formatField(1, "N", 1) & ", " & _
                        formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ") "
                        NoteID = executeIdentityQuery(strQry)
                    End If
                Else
                    Dim strQry As String = _
                        "INSERT INTO tblbusinessobjectnotes(CompanyID, BusinessObjectID, Note, CreatedDate, CreatedUserID) " & _
                        "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
                        formatField(AppID, "N", 0) & ", " & _
                        formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                        formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ") "
                    NoteID = executeIdentityQuery(strQry)
                End If
            Else
                If (strSticky = "1") Then
                    Dim intRowsAffected As Integer = 0
                    Dim strQry As String = _
                        "UPDATE tblnotes SET " & _
                            "Note           = " & formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                            "CreatedDate    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                            "CreatedUserID  = " & formatField(UserID, "N", getSystemUser()) & " " & _
                            "WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteType = 1 AND NoteActive = 1"
                    intRowsAffected = executeRowsAffectedQuery(strQry)
                    NoteID = New Caching(Nothing, "SELECT NoteID FROM tblnotes WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteType = 1 AND NoteActive = 1", "", "", "").returnCacheString
                    If (intRowsAffected = 0) Then
                        strQry = _
                        "INSERT INTO tblnotes (CompanyID, AppID, Note, NoteType, CreatedDate, CreatedUserID) " & _
                        "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
                        formatField(AppID, "N", 0) & ", " & _
                        formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                        formatField(1, "N", 1) & ", " & _
                        formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ") "
                        NoteID = executeIdentityQuery(strQry)
                    End If
                Else
                    Dim strQry As String = _
                        "INSERT INTO tblnotes (CompanyID, AppID, Note, CreatedDate, CreatedUserID) " & _
                        "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
                        formatField(AppID, "N", 0) & ", " & _
                        formatField(Replace(strNote, vbLf, "<br />"), "", "") & ", " & _
                        formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ") "
                    NoteID = executeIdentityQuery(strQry)
                End If
            End If
            Response.Write("1|" & getAnyFieldCached(Cache, "UserFullName", "tblusers", "UserID", UserID) & "|" & ddmmyyhhmmss2ddmmhhmm(Config.DefaultDateTime) & "|" & NoteID)
        End If
    End Sub

End Class
