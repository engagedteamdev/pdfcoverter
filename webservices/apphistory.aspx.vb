﻿Imports Config, Common

Partial Class AppHistory
    Inherits System.Web.UI.Page

    Private intReminderAssignedToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub appHistory()
        Dim strReminders As String = "<Applications>"
        Dim strQry As String = "SELECT TOP 10 * FROM vwapphistory WHERE CompanyID = '" & CompanyID & "' AND LastAccessedUserID = '" & Config.DefaultUserID & "' ORDER BY LastAccessed DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strReminders += "<Application><AppID>" & Row.Item("AppID") & "</AppID><ProductType>" & Row.Item("ProductType") & "</ProductType><StatusCode>" & Row.Item("StatusCode") & "</StatusCode><SubStatusCode>" & Row.Item("SubStatusCode") & "</SubStatusCode><App1FirstName>" & Row.Item("App1FirstName") & "</App1FirstName><App1Surname>" & Row.Item("App1Surname") & "</App1Surname><LastAccessed>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("LastAccessed")) & "</LastAccessed></Application>"
            Next
        End If
        strReminders += "</Applications>"
        Response.ContentType = "text/xml"
        Response.Write(Replace(strReminders, "&nbsp;", " "))
    End Sub

End Class
