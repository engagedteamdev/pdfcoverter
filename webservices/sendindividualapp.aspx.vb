﻿Imports Config, Common, CommonSave
Imports System.IO

Partial Class SendIndividualApp
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignIDOutbound As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
    Private strMailTo As String = HttpContext.Current.Request("MediaCampaignDeliveryEmailAddress")
	Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private intActionType As String = HttpContext.Current.Request("intActionType")
	Private intHotkeyUserID As String = HttpContext.Current.Request("HotkeyUserID")

    Private Enum ActionType
        Email = 3
        CSVEmail = 4
        EmailTextOnly = 8
    End Enum

    Public Sub sendIndividualApp()
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        Dim strResult As String = ""
		Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
		Dim strFriendlyName As String = getAnyFieldByCompanyID("MediaCampaignFriendlyNameOutbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strFriendlyName)) Then strCompanyName = strFriendlyName
        Select Case intActionType
            Case ActionType.Email
                Dim strApplication As String = _
                "<html>" & vbCrLf & _
                "<head>" & vbCrLf & _
                "<style>.h1 { font-family: Georgia, Arial, Helvetica, Sans-Serif; font-size: 16px;color: #000000; text-align: left; } .h2 { font-family: Georgia, Arial, Helvetica, Sans-Serif; font-size: 15px;color: #000000; text-align: left; } .row1 { width:99%; font-size: 11px; color: #000000; font-family: 'Trebuchet MS' , Arial, Helvetica, sans-serif; background-color: #F1F1F1; padding: 5px 5px 5px 5px; } .row2 { width:99%; font-size: 11px; color: #000000; font-family: 'Trebuchet MS' , Arial, Helvetica, sans-serif; background-color: #FFFFFF; padding: 5px 5px 5px 5px; }</style>" & vbCrLf & _
                "</head>" & vbCrLf & _
                "<body>" & vbCrLf & _
                "<div class=""h1"">Please find below a new application from " & strCompanyName & "</div><br />" & vbCrLf & _
                "<div class=""h2"">Ref: " & AppID & "</div><br />" & vbCrLf

                Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1WorkTelephone, App1EmailAddress, App1EmploymentStatus, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressLine2, AddressLine3, AddressCounty, AddressPostCode, AddressLivingArrangement, CallBackDate, PropertyValue, MortgageBalance, PropertyLTV FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                Dim strRowClass As String = "row1"
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        For Each Column As DataColumn In dsCache.Columns
                            If (checkValue(Row(Column).ToString)) Then
                                If (strRowClass = "row1") Then
                                    strRowClass = "row2"
                                Else
                                    strRowClass = "row1"
                                End If
                                strApplication += "<div class=""" & strRowClass & """><strong>" & Regex.Replace(Column.ColumnName.ToString, "([A-Z0-9])", " $1") & ":</strong> " & Row(Column).ToString & "</div>" & vbCrLf
                            End If
                        Next
                    Next
                End If
                dsCache = Nothing

                strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                            If (strRowClass = "row1") Then
                                strRowClass = "row2"
                            Else
                                strRowClass = "row1"
                            End If
                            strApplication += "<div class=""" & strRowClass & """><strong>" & Regex.Replace(Row.Item("StoredDataName").ToString, "([A-Z0-9])", " $1") & ":</strong> " & Row.Item("StoredDataValue").ToString & "</div>" & vbCrLf
                        End If
                    Next
                End If
                dsCache = Nothing

                strApplication += _
                "</body>" & vbCrLf & _
                "</html>" & vbCrLf
                'Response.Write(strApplication)
				incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
                strResult = postEmailWithMediaCampaignID("", strMailTo, "New lead from " & strCompanyName, strApplication, True, "")
            Case ActionType.CSVEmail
                Dim strHeaderRow As String = "", strDataRow As String = "", strComma As String = ""
                Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1WorkTelephone, App1EmailAddress, App1EmploymentStatus, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressLine2, AddressLine3, AddressCounty, AddressPostCode, AddressLivingArrangement, CallBackDate, PropertyValue, MortgageBalance, PropertyLTV FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim strRowClass As String = "row1"
                    Dim intCols As Integer = 0
                    For Each Row As DataRow In dsCache.Rows
                        intCols = 0
                        For Each Column As DataColumn In dsCache.Columns
                            If intCols = 0 Then strComma = "" Else strComma = ","
                            intCols += 1
                            strHeaderRow += strComma & Column.ColumnName
                            strDataRow += strComma & Row(Column).ToString
                        Next
                    Next
                End If
                dsCache = Nothing

                strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                            strHeaderRow += "," & Row.Item("StoredDataName").ToString
                            strDataRow += "," & Row.Item("StoredDataValue").ToString
                        End If
                    Next
                End If
                dsCache = Nothing

                Dim strTimeStamp As String = Year(Today) & padZeros(Month(Today), 2) & padZeros(Day(Today), 2) & padZeros(TimeOfDay.Hour, 2) & padZeros(TimeOfDay.Minute, 2) & padZeros(TimeOfDay.Second, 2) & padZeros(TimeOfDay.Millisecond, 2)
                Dim strFileName As String = Server.MapPath("/csvfiles/export/" & AppID & "_" & strTimeStamp & ".csv")
                Dim objFile As StreamWriter = New StreamWriter(strFileName)
                objFile.WriteLine(strHeaderRow)
                objFile.WriteLine(strDataRow)
                objFile.Close()
				incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
                strResult = postEmailWithMediaCampaignID("", strMailTo, "New lead from " & strCompanyName, "Please find attached a new application from " & strCompanyName, True, AppID & "_" & strTimeStamp & ".csv")
            Case ActionType.EmailTextOnly
                Dim strApplication As String = _
                "Please find below a new application from " & strCompanyName & vbCrLf & vbCrLf & _
                "Ref: " & AppID & vbCrLf & vbCrLf

                Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1WorkTelephone, App1EmailAddress, App1EmploymentStatus, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressLine2, AddressLine3, AddressCounty, AddressPostCode, AddressLivingArrangement, CallBackDate, PropertyValue, MortgageBalance, PropertyLTV FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        For Each Column As DataColumn In dsCache.Columns
                            If (checkValue(Row(Column).ToString)) Then
                                strApplication += Regex.Replace(Column.ColumnName.ToString, "([A-Z0-9])", " $1") & ": " & Row(Column).ToString & vbCrLf
                            End If
                        Next
                    Next
                End If
                dsCache = Nothing

                strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                            strApplication += Regex.Replace(Row.Item("StoredDataName").ToString, "([A-Z0-9])", " $1") & ": " & Row.Item("StoredDataValue").ToString & vbCrLf
                        End If
                    Next
                End If
                dsCache = Nothing

                'Response.Write(strApplication)
				incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
                strResult = postEmailWithMediaCampaignID("", strMailTo, "New lead from " & strCompanyName, strApplication, False, "")
        End Select
        If (strResult = "1") Then
            setTransferState(AppID, strMediaCampaignIDOutbound, "", "", intHotkeyUserID, strMediaCampaignScheduleID)
            HttpContext.Current.Response.Write(1 & "|" & encodeURL("Email transfer successful"))
        Else
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Email transfer not successful"))
        End If
    End Sub

End Class
