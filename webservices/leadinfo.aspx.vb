﻿Imports Config, Common

Partial Class LeadStatus
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID")
    Private UpdatedDate As String = HttpContext.Current.Request("UpdatedDate")
    Private CompanyID As String = "0"
    Private MediaID As String = HttpContext.Current.Request("MediaID")

    Public Sub leadStatus()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        '      If (checkValue(AppID)) Then
        '          Dim strXML As String = ""
        '          Dim strSQL As String = "SELECT * FROM vwleadstatus"
        '          strSQL += " WHERE (AppID IN ("
        '          Dim arrAppIDs As Array = AppID.Split(",")
        '          For x As Integer = 0 To UBound(arrAppIDs)
        '              If (x = 0) Then
        '                  strSQL += arrAppIDs(x)
        '              Else
        '                  strSQL += "," & arrAppIDs(x)
        '              End If
        '          Next  
        'strSQL += "))"

        If (checkValue(UpdatedDate)) Then
            Dim strXML As String = ""
            Dim strSQL As String = "SELECT * FROM vwleadinfo"
            strSQL += " WHERE (UpdatedDate >= " & formatField(UpdatedDate & " 00:00:00", "DTTM", Now) & " AND CompanyID = '" & CompanyID & "')"

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<LeadInfo>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<Lead>"
                    strXML += "<AppID>" & Row.Item("AppID") & "</AppID>"
                    strXML += "<StatusCode>" & Row.Item("StatusCode") & "</StatusCode>"
                    strXML += "<SubStatusCode>" & Row.Item("SubStatusCode") & "</SubStatusCode>"
                    strXML += "<Amount>" & Row.Item("Amount") & "</Amount>"
                    strXML += "<CreatedDate>" & Row.Item("CreatedDate") & "</CreatedDate>"
                    strXML += "<UpdatedDate>" & Row.Item("UpdatedDate") & "</UpdatedDate>"
                    strXML += "<ProductPurpose>" & Row.Item("ProductPurpose") & "</ProductPurpose>"
                    strXML += "<ProductType>" & Row.Item("ProductType") & "</ProductType>"
                    strXML += "<App1HomeTelephone>" & Row.Item("App1HomeTelephone") & "</App1HomeTelephone>"
                    strXML += "<App1MobileTelephone>" & Row.Item("App1MobileTelephone") & "</App1MobileTelephone>"
                    strXML += "<App1WorkTelephone>" & Row.Item("App1WorkTelephone") & "</App1WorkTelephone>"
                    strXML += "<App1EmailAddress>" & Row.Item("App1EmailAddress") & "</App1EmailAddress>"
                    strXML += "<App1Sex>" & Row.Item("App1Sex") & "</App1Sex>"
                    strXML += "<App1FirstName>" & Row.Item("App1FirstName") & "</App1FirstName>"
                    strXML += "<App1MiddleNames>" & Row.Item("App1MiddleNames") & "</App1MiddleNames>"
                    strXML += "<App1Surname>" & Row.Item("App1Surname") & "</App1Surname>"
                    strXML += "<App1MaidenName>" & Row.Item("App1MaidenName") & "</App1MaidenName>"
                    strXML += "<App1PreviousName>" & Row.Item("App1PreviousName") & "</App1PreviousName>"
                    strXML += "<App1DOB>" & Row.Item("App1DOB") & "</App1DOB>"
                    strXML += "<AddressLine1>" & Row.Item("AddressLine1") & "</AddressLine1>"
                    strXML += "<AddressLine2>" & Row.Item("AddressLine2") & "</AddressLine2>"
                    strXML += "<AddressLine3>" & Row.Item("AddressLine3") & "</AddressLine3>"
                    strXML += "<AddressLine4>" & Row.Item("AddressLine4") & "</AddressLine4>"
                    strXML += "<AddressCounty>" & Row.Item("AddressCounty") & "</AddressCounty>"
                    strXML += "<AddressPostCode>" & Row.Item("AddressPostCode") & "</AddressPostCode>"
                    strXML += "<AddressHouseName>" & Row.Item("AddressHouseName") & "</AddressHouseName>"
                    strXML += "<AddressHouseNumber>" & Row.Item("AddressHouseNumber") & "</AddressHouseNumber>"
                    strXML += "<AddressLivingArrangement>" & Row.Item("AddressLivingArrangement") & "</AddressLivingArrangement>"
                    strXML += "<AddressYears>" & Row.Item("AddressYears") & "</AddressYears>"
                    strXML += "<AddressMonths>" & Row.Item("AddressMonths") & "</AddressMonths>"
                    strXML += "<AddressYears>" & Row.Item("AddressYears") & "</AddressYears>"
                    strXML += "<App1NoDependants>" & Row.Item("App1NoDependants") & "</App1NoDependants>"
                    strXML += "<App1EmploymentStatus>" & Row.Item("App1EmploymentStatus") & "</App1EmploymentStatus>"
                    strXML += "<App1EmployerYears>" & Row.Item("App1EmployerYears") & "</App1EmployerYears>"
                    strXML += "<App1EmployerMonths>" & Row.Item("App1EmployerMonths") & "</App1EmployerMonths>"
                    strXML += "<PropertyPurchasePrice>" & Row.Item("PropertyPurchasePrice") & "</PropertyPurchasePrice>"
					strXML += "<MediaCampaignName>" & Row.Item("MediaCampaignName") & "</MediaCampaignName>"
					strXML += "<LenderName>" & Row.Item("LenderName") & "</LenderName>"
					strXML += "<BrokerFee>" & Row.Item("BrokerFee") & "</BrokerFee>"
					strXML += "<Note>" & Replace(Row.Item("Note"),"&","&amp;") & "</Note>"
					strXML += "<ProcurationFee>" & Row.Item("ProcurationFee") & "</ProcurationFee>"
					strXML += "<AgentName>" & Row.Item("AgentName") & "</AgentName>"
					strXML += "<Funded>" & Row.Item("Funded") & "</Funded>"
					strXML += "<SoldAtPartner>" & Row.Item("SoldAtPartner") & "</SoldAtPartner>"
					strXML += "<Fulfilment>" & Row.Item("Fulfilment") & "</Fulfilment>"
					strXML += "<Packaging>" & Row.Item("Packaging") & "</Packaging>"
					strXML += "<TransferedToPartner>" & Row.Item("TransferedToPartner") & "</TransferedToPartner>"
					strXML += "<OutboundCampaign>" & Row.Item("OutboundCampaign") & "</OutboundCampaign>"
					
                    strXML += "</Lead>"
                Next
                strXML += "</LeadInfo>"
            Else
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<LeadInfo><Error>No leads found</Error></LeadInfo>"
            End If
            dsCache = Nothing
            Response.ContentType = "text/xml"
            Response.Write(strXML)
        Else
            Dim strXML As String = ""
            strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
            strXML += "<LeadInfo><Error>No date specified</Error></LeadInfo>"
            Response.ContentType = "text/xml"
            Response.Write(strXML)
        End If
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
              "FROM tblmediacampaigns INNER JOIN " & _
              "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
              "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
              "WHERE (tblmediacampaigns.MediaID = '" & MediaID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR '" & ip & "' = '109.234.207.186') " & _
              "AND (ISNULL(WebServiceActive,1) = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Dim strXML As String = ""
            strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
            strXML += "<LeadInfo><Error>Not authorised</Error></LeadInfo>"
            Response.ContentType = "text/xml"
            Response.Write(strXML)
            responseEnd()
        End If
    End Sub

End Class