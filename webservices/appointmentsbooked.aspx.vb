﻿Imports Config, Common

Partial Class AppointmentsBooked
    Inherits System.Web.UI.Page
	
    Public Sub AppointmentsBooked()
		
      
            Dim strXML As String = ""
            Dim strSQL As String = "select * from vwappointments"
           

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<cases>"
                    strXML += "<AppointmentsBooked>" & Row.Item("Apps") & "</AppointmentsBooked>"
                    strXML += "<BrokerSupportUser>" & Row.Item("Broker Support User") & "</BrokerSupportUser>"	
					 strXML += "<Month>" & Row.Item("Month") & "</Month>"	
					  strXML += "<Year>" & Row.Item("Year") & "</Year>"	
                    strXML += "</cases>"
                Next
                strXML += "</CaseStatus>"
            Else
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus><Error>No leads found</Error></CaseStatus>"
            End If
            dsCache = Nothing
            Response.ContentType = "text/xml"
            Response.Write(strXML)       
    End Sub

End Class