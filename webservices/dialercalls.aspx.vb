﻿Imports Config, Common

Partial Class DialerCalls
    Inherits System.Web.UI.Page

    Private intCallAssignedToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkDialerCalls()
        Dim strClickToDialAccountNumber As String = getAnyFieldFromUserStore("ClickToDialAccountNumber", intCallAssignedToUserID)
        Dim strReminders As String = "<Calls>"
        Dim strQry As String = "SELECT TOP 1 * FROM tbldialercalls WHERE CompanyID = '" & CompanyID & "' AND DialerTelephoneNumberTo = '" & strClickToDialAccountNumber & "' AND DialerCallActive = 1 ORDER BY DialerCallDate DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                'If (x = 0) Then
                executeNonQuery("UPDATE tbldialercalls SET DialerCallActive = 0 WHERE DialerCallID = " & Row.Item("DialerCallID"))
                'End If
                strReminders += "<Call><AppID>" & Row.Item("DialerAppID") & "</AppID><Name>" & Row.Item("DialerTelephoneName") & "</Name><TelNo>" & Row.Item("DialerTelephoneNumberFrom") & " </TelNo><Ignore>" & Row.Item("DialerIgnore") & "</Ignore></Call>"
                x += 1
            Next
        End If
        strReminders += "</Calls>"
        Response.ContentType = "text/xml"
        Response.Write(strReminders)
    End Sub

End Class
