﻿Imports Config, Common

Partial Class LeadStatus
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strUpsell As String = HttpContext.Current.Request("frmUpsell"), strDownsell As String = HttpContext.Current.Request("frmDownsell")
	Private strDecline As String = HttpContext.Current.Request("frmDecline")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub leadStatus()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
		If (checkValue(AppID)) Then
			Dim strXML As String = ""
			Dim strSQL As String = "SELECT TOP 1 * FROM vwleadstatus"
			If (checkValue(AppID)) Then
                strSQL += " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
			End If
			Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
			If (dsCache.Rows.Count > 0) Then
				strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
				strXML += "<LeadStatus>"
				For Each Row As DataRow In dsCache.Rows
					strXML += "<AppID>" & Row.Item("AppID") & "</AppID>"
					strXML += "<Salutation>" & Row.Item("App1Salutation").ToString & "</Salutation>"
					strXML += "<StatusCode>" & Row.Item("StatusCode") & "</StatusCode>"
					strXML += "<ParnerRef>" & Row.Item("ClientReferenceOutbound").ToString & "</ParnerRef>"
					strXML += "<PartnerName>" & Replace(Row.Item("MediaCampaignFriendlyNameOutbound").ToString, "& ", "&amp; ") & "</PartnerName>"
					strXML += "<ParnerTelNo>" & Row.Item("MediaCampaignTelephoneNumberOutbound").ToString & "</ParnerTelNo>"
					strXML += "<AmountOriginal>" & Row.Item("AmountOriginal") & "</AmountOriginal>"
					strXML += "<Amount>" & Row.Item("Amount") & "</Amount>"
                    strXML += "<MaxAmount>" & Row.Item("MediaCampaignMaxAmount").ToString & "</MaxAmount>"
					strXML += "<ProductTerm>" & Row.Item("ProductTerm").ToString & "</ProductTerm>"
					strXML += "<MediaCampaignGradeName>" & Row.Item("MediaCampaignGradeName").ToString & "</MediaCampaignGradeName>"
					strXML += "<TotalRepayment>" & Row.Item("UnderwritingTotalRepayment").ToString & "</TotalRepayment>"
					strXML += "<CurrentRepayment>" & Row.Item("UnderwritingRepaymentFees").ToString & "</CurrentRepayment>"
					strXML += "<APR>" & Row.Item("UnderwritingAPR").ToString & "</APR>"			
					If (strUpsell = "Y") Then
						executeNonQuery("UPDATE tblapplications SET Amount = '" & Row.Item("MediaCampaignMaxAmount") & "' WHERE AppID = '" & Row.Item("AppID") & "' AND CompanyID = '" & CompanyID & "'")
						executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & Row.Item("AppID") & "'")
						CommonSave.saveNote(Row.Item("AppID"), Config.DefaultUserID, "Up-sell: " & displayCurrency(Row.Item("MediaCampaignMaxAmount")))
					End If
                    If (Row.Item("StatusCode") <> "APP" And Row.Item("StatusCode") <> "ACC" And Row.Item("StatusCode") <> "POT" And Row.Item("StatusCode") <> "PRQ" And Row.Item("StatusCode") <> "FPB" And Row.Item("StatusCode") <> "CPB") Then
                        If (strDownsell = "Y" And CDec(Row.Item("Amount")) >= 100) Then
                            executeNonQuery("UPDATE tblapplications SET Amount = (Amount - 50) WHERE Amount >= 100 AND AppID = '" & Row.Item("AppID") & "' AND CompanyID = '" & CompanyID & "'")
                            'responseWrite("UPDATE tblapplications SET Amount = (Amount - 50) WHERE Amount >= 100 AND AppID = '" & Row.Item("AppID") & "' AND CompanyID = '" & CompanyID & "'")
                            executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & Row.Item("AppID") & "'")
                            CommonSave.saveNote(Row.Item("AppID"), Config.DefaultUserID, "Automatic down-sell: " & displayCurrency(CDec(Row.Item("Amount")) - 50))
                        End If
                        If (strDecline = "Y") Then
                            CommonSave.saveStatus(Row.Item("AppID"), Config.DefaultUserID, "DEC", "")
                            CommonSave.saveNote(Row.Item("AppID"), Config.DefaultUserID, "Automatic decline: No plan matched")
                        End If
                    End If
				Next
				strXML += "</LeadStatus>"
			End If
            dsCache = Nothing
            If (strUpsell = "Y") Then
                Response.Write(1)
            Else
                Response.ContentType = "text/xml"
                Response.Write(strXML)
            End If
		End If
    End Sub

End Class