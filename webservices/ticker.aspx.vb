﻿Imports Config, Common

Partial Class Ticker
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkTicker()
        Response.ContentType = "text/xml"
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT StatisticName, StatisticText, StatisticSQL, StatisticCachedMinutes, StatisticShowTickerMS FROM vwdashboardpanel DSH INNER JOIN tbldashboardmappings MAP ON MAP.DashboardID = DSH.DashboardID WHERE DashboardTicker = 1 AND MAP.UserID = '" & Config.DefaultUserID & "' AND DSH.CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
            .WriteLine("<?xml version=""1.0"" encoding=""UTF-8""?>")
            .WriteLine("<Ticker>")
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strStatSQL As String = "", strStatValue As String = ""
                    strStatSQL = Row.Item("StatisticSQL").ToString
                    strStatSQL = Replace(strStatSQL, "{CompanyID}", CompanyID)
                    If (Row.Item("StatisticCachedMinutes") > 0) Then
                        strStatValue = New Caching(Cache, strStatSQL, "", Config.DefaultDateTime.AddMinutes(Row.Item("StatisticCachedMinutes")), "").returnCacheString
                    Else
                        strStatValue = New Caching(Nothing, strStatSQL, "", "", "").returnCacheString
                    End If
                    If (checkValue(strStatValue)) Then
                        .WriteLine("<TickerItem><Name>" & Row.Item("StatisticName") & "</Name><Item>" & Replace(Row.Item("StatisticText").ToString, "{StatValue}", strStatValue) & "</Item><Time>" & Row.Item("StatisticShowTickerMS") & "</Time></TickerItem>")
                    End If
                Next
            End If
            .WriteLine("</Ticker>")
            dsCache = Nothing
        End With
        HttpContext.Current.Response.Write(objStringWriter.ToString)
    End Sub

End Class
