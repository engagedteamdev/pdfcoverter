﻿Imports Config, Common

Partial Class LenderLookup
    Inherits System.Web.UI.Page

    Private strSearch As String = HttpContext.Current.Request("text")
	Private strLenderType As String = HttpContext.Current.Request("frmLenderType")

    Public Sub lenderLookup()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        Response.ContentType = "text/xml"
        Dim strXML As String = ""
        Dim strSQL As String = "SELECT LenderName FROM tbllenders " & _
            "WHERE LenderActive = 1"
        If (checkValue(strSearch)) Then
            strSQL += " AND LenderName LIKE '%" & strSearch & "%' "
        End If
        If (checkValue(strLenderType)) Then
            strSQL += " AND ("
            Dim arrLenderType As Array = Split(strLenderType, ",")
            For x As Integer = 0 To UBound(arrLenderType)
                If (x = 0) Then
                    strSQL += "LenderType = '" & arrLenderType(x) & "'"
                Else
                    strSQL += " OR LenderType = '" & arrLenderType(x) & "'"
                End If
            Next
            strSQL += ")"
        End If
        strSQL += " AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
            strXML += "<Lenders>"
            For Each Row As DataRow In dsCache.Rows
                strXML += "<Lender><Name>" & Replace(Row.Item("LenderName"), "& ", "&amp; ") & "</Name></Lender>"
            Next
            strXML += "</Lenders>"
        End If
        dsCache = Nothing
        Response.Write(strXML)
    End Sub

End Class