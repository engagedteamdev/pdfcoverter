﻿Imports Config, Common

Partial Class Reminders
    Inherits System.Web.UI.Page

    Private intReminderAssignedToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkReminders()
        Dim strReminders As String = "<Reminders>"
		Dim strQry As String = "SELECT COUNT(*) FROM tblreminders WHERE CompanyID = '" & CompanyID & "' AND ReminderAssignedToUserID = '" & intReminderAssignedToUserID & "' AND ReminderDateTime <= GETDATE()"
		Dim strCount As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
		strReminders += "<Due>" & strCount & "</Due>"
        strQry = "SELECT TOP 6 * FROM tblreminders WHERE CompanyID = '" & CompanyID & "' AND ReminderAssignedToUserID = '" & intReminderAssignedToUserID & "' AND ReminderDateTime <= GETDATE() AND DATEDIFF(MI, ReminderUpdated, GETDATE()) >= 10 ORDER BY ReminderUpdated ASC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                'If (x = 0) Then
                    executeNonQuery("UPDATE tblreminders SET ReminderUpdated = GETDATE() WHERE ReminderID = " & Row.Item("ReminderID"))
                'End If
                strReminders += "<Reminder><AppID>" & Row.Item("AppID") & "</AppID><Text>" & Row.Item("ReminderDescription") & "</Text></Reminder>"
                x += 1
            Next
        End If
        strReminders += "</Reminders>"
        Response.ContentType = "text/xml"
        Response.Write(strReminders)
    End Sub

End Class
