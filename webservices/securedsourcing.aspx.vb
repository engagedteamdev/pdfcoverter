﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.brokertoolbox.www

Partial Class securedsourcing
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID"), UserID As String = HttpContext.Current.Request("UserID"), XMLReceivedID As String = HttpContext.Current.Request("XMLReceivedID"), QuoteID As String = HttpContext.Current.Request("QuoteID"), strViewDocuments As String = HttpContext.Current.Request("frmViewDocuments"), strSaveDocuments As String = HttpContext.Current.Request("frmSaveDocuments"), intNoQuotes As String = HttpContext.Current.Request("frmNoQuotes")
    Private boolViewDocuments As Boolean = False, boolSaveDocuments As Boolean = False
    Private intSOAPRequestNo As String = getIndexNumber()
    Private objAuthXMLDoc As XmlDocument = New XmlDocument, objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strXMLURL As String = "", strXMLSOAPAction As String = "", strXMLFile As String = "", strXMLView As String = "", strApplicationSOAPAction As String = "", strApplicationFile As String = "", strApplicationView As String = "", strDocumentsSOAPAction As String = "", strDocumentsFile As String = "", strXMLNamespace As String = "", intCompanyID As String = "", intSiteID As String = "", strUserID As String = "", strAPIKey As String = ""
    Private intMode As String = HttpContext.Current.Request("intMode"), strRateType As String = HttpContext.Current.Request("frmRateType"), strTerm As String = HttpContext.Current.Request("frmProductTerm"), strLenderCodes As String = HttpContext.Current.Request("frmLenderCodes"), strFeeFree As String = HttpContext.Current.Request("frmFeeFree"), strAmount As String = HttpContext.Current.Request("frmAmount")
    Private strErrorMessage As String = ""
	Private BrokerFee As Integer = 0
	Private LenderFee As Integer = 0
	Private ProcurationFee As Integer = 0
	Private App1DOB as date = getAnyField("App1DOB", "tblapplications", "AppID", AppID)	

	
	


	

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If checkValue((getAnyFieldFromDataStore("underwritingbrokerFee", AppID))) Then
            BrokerFee = getAnyFieldFromDataStore("underwritingbrokerFee", AppID)
        End If
    End Sub
   


    Public Sub getSourcing()

        Dim strXML As String = ""
        Dim strSQL As String = "SELECT TOP (100)percent dbo.getage(CONVERT(DATETIME, dbo.tblapplications.App1DOB,102))as App1Age, dbo.tblsecuredproducts.producttype,dbo.tblsecuredproducts.networkid,  dbo.tblsecuredproducts.PolicyName, dbo.tblsecuredproducts.exclusive, dbo.tblsecuredproducts.commission, dbo.tbllendercriteria.overpaymentNote, dbo.tblsecuredproducts.Lender,"& _
                "CASE WHEN dbo.tblsecuredproducts.loanratetype = 'tracker' THEN dbo.tblsecuredproducts.annualrate + 0.5 ELSE dbo.tblsecuredproducts.annualrate END as AnnualRate, dbo.tblsecuredproducts.maxbrokerfeeamount,dbo.tblsecuredproducts.maxbrokerfeepercent, dbo.tbllendercriteria.TTFee, dbo.tblsecuredproducts.loanratetype,dbo.tblsecuredproducts.maxbrokerfeeamount, dbo.tblsecuredproducts.MinTerm, dbo.tblsecuredproducts.LenderAdminCap, dbo.tblsecuredproducts.MaxTerm, dbo.tblsecuredproducts.LTI, dbo.tblsecuredproducts.MinNetLoan,  dbo.tblsecuredproducts.MinPropertyValue, dbo.tblsecuredproducts.MaxPropertyValue, dbo.tblsecuredproducts.MaxNetLoan, dbo.tblsecuredproducts.MinLTV,dbo.tblsecuredproducts.MaxLTV,"& _
                "CASE WHEN CONVERT (int , dbo.tblapplications.amount) >= 5000 AND CONVERT (int , dbo.tblapplications.amount) <= 14999 THEN dbo.tblapplications.amount * 0.12 WHEN CONVERT (int , dbo.tblapplications.amount) >= 15000 AND CONVERT (int , dbo.tblapplications.amount) <= 29999 THEN dbo.tblapplications.amount * 0.09 WHEN CONVERT (int , dbo.tblapplications.amount) >= 30000 AND CONVERT (int , dbo.tblapplications.amount) <= 44999 THEN CONVERT (int , dbo.tblapplications.amount) * 0.06 WHEN CONVERT (int , dbo.tblapplications.amount) >= 45000 AND CONVERT (int , dbo.tblapplications.amount) <= 59999 THEN dbo.tblapplications.amount * 0.05 WHEN CONVERT (int , dbo.tblapplications.amount) >= 60000 THEN dbo.tblapplications.amount * 0.04 ELSE 1 END AS RecomendedBrokerFee," & _			
				" CASE WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 50000 AND CONVERT(int,dbo.tblapplications.propertyvalue) <= 199999 THEN 220 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 200000 AND CONVERT(int," & _	 
                      "dbo.tblapplications.propertyvalue) <= 399999 THEN 250 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 400000 AND CONVERT(int, " & _	
                      "dbo.tblapplications.propertyvalue) <= 499999 THEN 275 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 599999 THEN 350 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 600000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 699999 THEN 500 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 700000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 799999 THEN 600 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 800000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 899999 THEN 700 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 900000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 999999 THEN 800 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1000000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1099999 THEN 1000 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1100000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1199999 THEN 1050 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1200000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1299999 THEN 1100 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1300000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1399999 THEN 1200 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1400000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1499999 THEN 1300 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1599999 THEN 1400 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1600000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1699999 THEN 1500 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1700000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1799999 THEN 1600 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1800000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1899999 THEN 1700 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 1900000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 1999999 THEN 1800 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 2000000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 2249999 THEN 1900 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 2250000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 3249999 THEN 2600 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 3250000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 4499999 THEN 3400 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 4500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 5499999 THEN 4250 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 5500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 6499999 THEN 5000 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 6500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 7499999 THEN 5800 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 7500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 8499999 THEN 6600 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 8500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 9499999 THEN 7400 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 9500000 AND CONVERT(int," & _	
					   "dbo.tblapplications.propertyvalue) <= 10000000 THEN 8200 WHEN CONVERT(int,dbo.tblapplications.propertyvalue) >= 10000001 THEN 8200 END AS EstimatedValuationFee," & _	
					 " CONVERT(int, dbo.tblapplications.amount) AS Amount,"& _ 
                "CASE WHEN CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) < '1' AND (dbo.tblapplications.amount * CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) <= CONVERT (float , dbo.tblsecuredproducts.lenderadmincap) THEN (dbo.tblapplications.amount *  CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) WHEN CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) < '10' AND (dbo.tblapplications.amount * CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) > CONVERT (float , dbo.tblsecuredproducts.lenderadmincap) THEN CONVERT (float , dbo.tblsecuredproducts.LenderAdmincap) ELSE CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) END As LenderFee " & _
			    "FROM  dbo.tblsecuredproducts Inner Join dbo.tblapplications ON dbo.tblapplications.ProductType = dbo.tblSecuredProducts.producttype Inner join tbllendercriteria on dbo.tblsecuredproducts.lender = dbo.tbllendercriteria.lendername " & _
				"WHERE(dbo.tblsecuredproducts.MinNetLoan IS NULL OR dbo.tblapplications.amount >= dbo.tblsecuredproducts.MinNetLoan)" & _
				"AND (dbo.tblsecuredproducts.MaxNetLoan IS NULL OR  dbo.tblapplications.amount <= dbo.tblsecuredproducts.MaxNetLoan)" & _
				"AND (dbo.tblsecuredproducts.MinTerm IS NULL OR dbo.tblapplications.ProductTerm / 12 >= dbo.tblsecuredproducts.MinTerm)"& _
				"AND (dbo.tblsecuredproducts.MaxTerm IS NULL OR dbo.tblapplications.ProductTerm / 12 <= dbo.tblsecuredproducts.MaxTerm)"& _			
			    "AND (dbo.tblsecuredproducts.MaxLTV IS NULL OR   convert(float,dbo.tblsecuredproducts.MaxLTV) >= dbo.tblapplications.PropertyLTV)" & _
				"AND (dbo.tblsecuredproducts.MinLTV IS NULL OR convert(float,dbo.tblsecuredproducts.MinLTV) <= dbo.tblapplications.PropertyLTV)" & _
			    "AND (dbo.tblsecuredproducts.LTI IS NULL OR dbo.tblsecuredproducts.LTI >= dbo.tblapplications.LTI)" & _
				"AND (dbo.tblsecuredproducts.employmentstatus IS NULL OR tblsecuredproducts.employmentstatus LIKE N'%|' + dbo.tblapplications.App1EmploymentStatus + '|%') " & _						
				"AND ((dbo.tblsecuredproducts.MinPropertyValue IS NULL) OR (convert(integer,dbo.tblsecuredproducts.MinPropertyValue) <= dbo.tblapplications.propertyvalue))" & _	
				"AND ((dbo.tblsecuredproducts.MaxPropertyValue IS NULL) OR (convert(integer,dbo.tblsecuredproducts.MaxPropertyValue) >= dbo.tblapplications.propertyvalue))" 
				
		if (checkvalue(getAnyField("productpurpose", "tblapplications", "AppID", AppID)))
			strSQL +="AND (dbo.tbllendercriteria.loanpurpose IS NULL OR tbllendercriteria.loanpurpose LIKE N'%|' + dbo.tblapplications.ProductPurpose + '|%')"
		End if
				
        If (getAnyFieldFromDataStore("RateType", AppID) <> "no pref") Then
            If (getAnyFieldFromDataStore("RateType", AppID) = "Variable,Tracker") Then
                strSQL += "AND (dbo.tblsecuredproducts.loanRateType is NULL OR tblsecuredproducts.loanRateType in ('Variable','Tracker'))"
            Else
                strSQL += "AND (dbo.tblsecuredproducts.loanRateType is NULL OR tblsecuredproducts.loanRateType Like N'%" & getAnyFieldFromDataStore("RateType", AppID) & "%')"
            End If
        End If
		
        If (getAnyFieldFromDataStore("Overpayments", AppID) = "Yes") Then
            strSQL += "AND (dbo.tbllendercriteria.OverpaymentsSavings is NULL OR tbllendercriteria.OverpaymentsSavings = 1)"       
	    End If
		
        strSQL += "AND (dbo.tblsecuredproducts.producttype is NULL OR tblsecuredproducts.producttype = '" & getAnyField("ProductType", "tblapplications", "AppID", AppID) & "')"
       
	    If (getAnyFieldFromDataStore("RepaymentType", AppID) = "Interest Only") Then
            strSQL += "AND (dbo.tbllendercriteria.InterestOnlyOption is NULL OR tbllendercriteria.InterestOnlyOption = 1)"
        End If       
	 
		strSQL += "AND (dbo.tblSecuredProducts.MaxAgeAtEndOfTerm is null or (dbo.getage(CONVERT(DATETIME, '" & App1DOB.ToString("MM/dd/yyyy") & "', 102)) + " & getAnyField("productterm", "tblapplications", "AppID", AppID) / 12 & ") <= dbo.tblSecuredProducts.MaxAgeAtEndOfTerm)"
        strSQL += "AND (dbo.tblSecuredProducts.MaximumAgeStartOfLoan is null or dbo.getage(CONVERT(DATETIME, '" & App1DOB.ToString("MM/dd/yyyy") & "', 102)) <= dbo.tblSecuredProducts.MaximumAgeStartOfLoan)"
        strSQL += "AND (dbo.tblSecuredProducts.MinimumAge is null or dbo.getage(CONVERT(DATETIME, '" & App1DOB.ToString("MM/dd/yyyy") & "', 102)) >= dbo.tblSecuredProducts.MinimumAge)"
       
	     'If (checkvalue(getAnyField("App2AnnualIncome", "tblapplications", "AppID", AppID))) Then
          '  strSQL += "AND ((dbo.tblsecuredproducts.minimumincomejoint IS NULL) OR (dbo.tblsecuredproducts.minimumincomejoint) <= '" & (getAnyField("App2AnnualIncome", "tblapplications", "AppID", AppID) + getAnyField("App1AnnualIncome", "tblapplications", "AppID", AppID)) & "'))"
         'Else
       		'strSQL += " AND ((dbo.tblsecuredproducts.minimumincomeSingle IS NULL) OR (dbo.tblsecuredproducts.minimumincomesingle <=  '" & getAnyField("App1AnnualIncome", "tblapplications", "AppID", AppID) & "'))"
	    ' End If
	   
	   	strSQL += "AND dbo.tblapplications.AppID = " & AppID 
		strSQL += " ORDER BY AnnualRate"
		
		 'HttpContext.Current.Response.Write(strSQL)
         'HttpContext.Current.Response.End()
		
		
		
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()      
        If (dsCache.Rows.Count > 0) Then
				strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
				strXML += "<SecuredSourcing>"
				For Each Row As DataRow In dsCache.Rows		
                strXML += "<Policy>"
                strXML += "<Lender>" & Row.Item("Lender") & "</Lender>"
                strXML += "<PolicyName>" & Row.Item("PolicyName").ToString & "</PolicyName>"
                strXML += "<AnnualRate>" & Row.Item("AnnualRate") & "</AnnualRate>"
                strXML += "<RecomendedBrokerFee>" & Row.Item("RecomendedBrokerFee") & "</RecomendedBrokerFee>"
                strXML += "<LenderFee>" & Row.Item("LenderFee") & "</LenderFee>"
                strXML += "<MinTerm>" & Row.Item("MinTerm") & "</MinTerm>"
                strXML += "<MaxTerm>" & Row.Item("MaxTerm") & "</MaxTerm>"
                strXML += "<MinNetLoan>" & Row.Item("MinNetLoan") & "</MinNetLoan>"
                strXML += "<MaxNetLoan>" & Row.Item("MaxNetLoan") & "</MaxNetLoan>"
                strXML += "<EstimatedValuationFee>" & Row.Item("EstimatedValuationFee") & "</EstimatedValuationFee>"
                strXML += "<MinPropertyValue>" & Row.Item("MinPropertyValue") & "</MinPropertyValue>"
                strXML += "<MaxPropertyValue>" & Row.Item("MaxPropertyValue") & "</MaxPropertyValue>"
                strXML += "<Amount>" & getAnyField("Amount", "tblapplications", "AppID", AppID) & "</Amount>"
                strXML += "<TTFee>" & Row.Item("TTFee") & "</TTFee>"
                strXML += "<maxbrokerfeepercent>" & Row.Item("maxbrokerfeepercent") & "</maxbrokerfeepercent>"
                strXML += "<maxbrokerfeeamount>" & Row.Item("maxbrokerfeeamount") & "</maxbrokerfeeamount>"
                strXML += "<commission>" & Row.Item("commission") & "</commission>"
                strXML += "<exclusive>" & Row.Item("exclusive") & "</exclusive>"
                strXML += "<ProductTerm>" & getAnyField("productterm","tblapplications", "AppID", AppID) & "</ProductTerm>"
                strXML += "<MaxLTV>" & Row.Item("MaxLTV") & "</MaxLTV>"
                strXML += "<MinLTV>" & Row.Item("MinLTV") & "</MinLTV>"
                strXML += "<LTI>" & Row.Item("LTI") & "</LTI>"
                strXML += "<LenderAdminCap>" & Row.Item("LenderAdminCap") & "</LenderAdminCap>"
                strXML += "<MaxBrokerFee>" & Row.Item("maxbrokerfeeamount") & "</MaxBrokerFee>"
                strXML += "<loanratetype>" & Row.Item("loanratetype") & "</loanratetype>"
                strXML += "<overpaymentnote>" & Row.Item("overpaymentnote") & "</overpaymentnote>"
                strXML += "</Policy>"
				Next
				strXML += "</SecuredSourcing>"		
		Else
				HttpContext.Current.Response.Write(strSQL)
       			HttpContext.Current.Response.End()    
        End If

      
        Dim salesuserid As Integer = Config.DefaultUserID()

        Dim strSQL2 As String = _
        "INSERT INTO tblSecuredXMLs (AppID, CompanyID, XML, SourceDate, BrokerFee, SalesUserID) " & _
        "VALUES (" & formatField(AppID, "N", 0) & ", " & _
        formatField(CompanyID, "N", "") & ", " & _
        formatField(strXML, "", "") & ", " & _
        formatField(DateTime.Now, "DTTM", "") & ", " & _
        formatField(BrokerFee, "N", 0) & ", " & _
        formatField(salesuserid, "N", 0) & ") "
        executeNonQuery(strSQL2)
		
	   HttpContext.Current.Response.ContentType = "text/xml"
       HttpContext.Current.Response.Write(strXML)
       HttpContext.Current.Response.End()
          
       HttpContext.Current.Response.Write(strXML)
       HttpContext.Current.Response.End()
        
    End Sub
    

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    

 

End Class
