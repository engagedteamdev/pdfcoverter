﻿Imports Config, Common
Imports System.IO

Partial Class Export
    Inherits System.Web.UI.Page

    Private strExportString As String = HttpContext.Current.Request("strExportString")

    Public Sub export()
        strExportString = Replace(strExportString, "ampersand;", "&")
        strExportString = Replace(strExportString, "lt;", "<")
        strExportString = Replace(strExportString, "gt;", ">")
        Dim strTimeStamp As String = Year(Today) & padZeros(Month(Today), 2) & padZeros(Day(Today), 2) & padZeros(TimeOfDay.Hour, 2) & padZeros(TimeOfDay.Minute, 2) & padZeros(TimeOfDay.Second, 2)
        Dim strFileName As String = Server.MapPath("/net/csvfiles/export/" & strTimeStamp & ".xls")
        Dim objFile As StreamWriter = New StreamWriter(strFileName, False, System.Text.Encoding.UTF8)
        objFile.Write(strFileName)
        objFile.Close()
        Response.Write(strTimeStamp & ".xls")
    End Sub

End Class
