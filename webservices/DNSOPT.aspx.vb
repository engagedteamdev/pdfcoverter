﻿Imports System.Data
Imports System.Globalization
Imports Config, Common
Imports System.Net


Partial Class TransferLead
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    


    Dim CoverGroupEmailList As String = "crmsupport@engagedcrm.co.uk;"
    Dim SparkyEmailList As String = "crmsupport@engagedcrm.co.uk;"
    Dim EsEmailList As String = "crmsupport@engagedcrm.co.uk;"
    Dim ESCSCEmailList As String = "crmsupport@engagedcrm.co.uk;"
    Dim CoreCoverEmailList As String = "crmsupport@engagedcrm.co.uk;"
    Dim HealthCoreEmailList As String = "crmsupport@engagedcrm.co.uk;"



    Dim LifeCoverEmailList As String = ""
    Dim RemortgageEmailList As String = ""
    Dim AcceptedEmailList As String = ""





    Dim AppID As String = HttpContext.Current.Request("AppID")
    Dim CustomerID As String = HttpContext.Current.Request("CustomerID")
    Dim CRM As String = HttpContext.Current.Request("CRM")

    Dim HomeTel As String = HttpContext.Current.Request("HomeTel") 
    Dim Mobile As String = HttpContext.Current.Request("Mobile") 
    Dim Work As String = HttpContext.Current.Request("Work") 
    Dim Email As String = HttpContext.Current.Request("Email") 
    Dim Surname As String = HttpContext.Current.Request("Surname") 







    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub optOut()

        Dim DBList As String = ""

       
        If(CRM = "Sparky")Then
            RemoveSparkyRecord(AppID)
            DBList = "ESCSC|LifeCover|Accepted|Remortgages|HealthCore"
        
        Else If (CRM = "Remortgages")Then
            RemoveRemortgagesRecord(AppID, CustomerID)
            DBList = "ESCSC|Sparky|ES|CoverGroup|LifeCover|Accepted"
        
        Else If (CRM = "LifeCover")Then
            RemoveLifeCoverRecord(AppID)
            DBList = "ESCSC|Sparky|ES|CoverGroup|HealthCore|Remortgages"
       
        Else If (CRM = "HealthCore")Then
            RemoveHealthCoreRecord(AppID)
            DBList = "ESCSC|Sparky|ES|CoverGroup|LifeCover"
        
        Else If (CRM = "ESCSC")Then
            RemoveESCSCRecord(AppID)
            DBList = "ES|Sparky|CoverGroup|Remortgages|Accepted|LifeCover|HealthCore"
        
        Else If (CRM = "ES")Then
            RemoveESRecord(AppID)
            DBList = "ESCSC|LifeCover|Accepted|Remortgages|HealthCore"
        
        Else If (CRM = "CoverGroup")Then
            RemoveCoverGroupRecord(AppID)
            DBList = "ESCSC|LifeCover|Accepted|Remortgages|HealthCore"
        
        Else If (CRM = "Accepted")Then
            RemoveAcceptedRecord(AppID, CustomerID)
            DBList = "ESCSC|Sparky|ES|CoverGroup|LifeCover|Remortgages"
        
        Else If (CRM = "CoreCover")Then
            RemoveCoreCoverRecord(AppID)
            ''' ????????????????
        End If



     
        Dim strCRMArray As Array = DBList.Split("|")
       
        For i As Integer = 0 To UBound(strCRMArray)

               

            Dim CRMName As String = i.ToString()
            Dim ConnectionString As String = getConnectionString(strCRMArray(i))

            


            Dim strLookupQry As String = ""
            If(strCRMArray(i) = "Accepted" Or strCRMArray(i) = "Remortgages")Then
                strLookupQry = "SELECT Distinct(App.AppID), Cust.CustomerID FROM tblApplications as App left outer join tblApplicants as Applicant on App.AppID = Applicant.AppID Left outer join tblCustomers as Cust on Applicant.CustomerID = Cust.CustomerID left outer join tbladdressinfo as CustAdd on Cust.CustomerID = CustAdd.CustomerID and CustAdd.CurrentAddress = '1' and CustAdd.Active = '1' WHERE App.AppID IS NOT NULL"
            
                If(checkValue(HomeTel))Then
                    strLookupQry += " And HomeTelephone = '"& HomeTel &"'"
                End If
            
                If(checkValue(Mobile))Then
                    strLookupQry += " And MobileTelephone = '"& Mobile &"'"
                End If
            
                If(checkValue(Work))Then
                    strLookupQry += " And WorkTelephone = '"& Work &"'"
                End If
            
                If(checkValue(Email))Then
                    strLookupQry += " And EmailAddress = '"& Email &"'"
                End If
            
                If(checkValue(Surname))Then
                    strLookupQry += " And CustomerSurname = '"& Surname &"'"
                End If
            
            Else
                strLookupQry = "SELECT App.AppID, '' as CustomerID FROM tblApplications as App LEFT OUTER JOIN tblApplicationStatus as AppStat on App.AppId = AppStat.AppID WHERE AppStat.Active = '1'"

                If(checkValue(HomeTel))Then
                    strLookupQry += " And App1HomeTelephone = '"& HomeTel &"'"
                End If
            
                If(checkValue(Mobile))Then
                    strLookupQry += " And App1MobileTelephone = '"& Mobile &"'"
                End If
            
                If(checkValue(Work))Then
                    strLookupQry += " And App1WorkTelephone = '"& Work &"'"
                End If
            
                If(checkValue(Email))Then
                    strLookupQry += " And App1EmailAddress = '"& Email &"'"
                End If
            
                If(checkValue(Surname))Then
                    strLookupQry += " And App1Surname = '"& Surname &"'"
                End If
            End If

            Dim ResultingAppID As String = "" 
            Dim CustomerID As String = ""

            Dim dtbAppIDLookup As New DataTable
            using DBConnectionNew As New SqlConnection(ConnectionString)
                DBConnectionNew.open()
                Using dadAppIDLookup As New SqlDataAdapter(strLookupQry, DBConnectionNew)
                    dadAppIDLookup.Fill(dtbAppIDLookup)
                End Using
                DBConnectionNew.close()
            End Using
            If (dtbAppIDLookup.Rows.Count > 0) Then
                For Each Row As DataRow In dtbAppIDLookup.Rows
                    ResultingAppID = Row.Item("AppID")
                    CustomerID = Row.Item("CustomerID")

                     If(strCRMArray(i) = "Sparky")Then
                        RemoveSparkyRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "Remortgages")Then
                        RemoveRemortgagesRecord(ResultingAppID, CustomerID)
                    Else If (strCRMArray(i) = "LifeCover")Then
                        RemoveLifeCoverRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "HealthCore")Then
                        RemoveHealthCoreRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "ESCSC")Then
                        RemoveESCSCRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "ES")Then
                        RemoveESRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "CoverGroup")Then
                        RemoveCoverGroupRecord(ResultingAppID)
                    Else If (strCRMArray(i) = "Accepted")Then
                        RemoveAcceptedRecord(ResultingAppID, CustomerID)
                    Else If (strCRMArray(i) = "CoreCover")Then
                        RemoveCoreCoverRecord(ResultingAppID)
                    End If
                Next
            End If

           
        Next

   









        


    
    End Sub








    Public Function removeFromDialler(AppID As String) As Boolean
        Dim Status As Boolean = False



        Dim ObjResult As HttpWebResponse = getWebRequest("https://integrations.engagedcrm.co.uk/webservices/outbound/connex/default.aspx?AppID="& AppID &"&RequestType=UpdateCustomer")
        Dim objReader As New StreamReader(ObjResult.GetResponseStream())
        Dim strResponseText As String = objReader.ReadToEnd()
                   
        Dim strArray As Array = strResponseText.Split("|")

        If(strArray(0).ToString() = "1")Then

        Else

        End If




        objReader.Close()
        objReader = Nothing



     

        Return Status
    End Function




    Public function getConnectionString(CRMName As String) As String

        Dim Connection As String = ""



        If(CRMName = "Sparky")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-Sparkybeta;Persist Security Info=True;User ID=crm-Spark;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "Remortgages")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-remortgages.com;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;"
        Else If (CRMName = "LifeCover")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-LifeCoverJan20;Persist Security Info=True;User ID=crm-lifecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "HealthCore")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-hcpmi;Persist Security Info=True;User ID=crm-hcpmi;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "ESCSC")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-escscnew_restore;Persist Security Info=True;User ID=crm-escsc;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "ES")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-esbeta;Persist Security Info=True;User ID=crm-esbeta;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "CoverGroup")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-covergrouptest;Persist Security Info=True;User ID=crm-covergroup;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        Else If (CRMName = "Accepted")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-remortgages;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;"
        Else If (CRMName = "CoreCover")Then
            Connection = "Data Source=172.28.27.24;Initial Catalog=crm-corecover;Persist Security Info=True;User ID=crm-corecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;"
        End If






        Return Connection
    End function





    Public Function RemoveSparkyRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-Sparkybeta;Persist Security Info=True;User ID=crm-Spark;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressTown = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",SparkyEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function


    Public Function RemoveESRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-esbeta;Persist Security Info=True;User ID=crm-esbeta;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressTown = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",EsEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function

    

    Public Function RemoveCoverGroupRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-covergrouptest;Persist Security Info=True;User ID=crm-covergroup;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressTown = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",CoverGroupEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function


    Public Function RemoveESCSCRecord (AppID As string) As Boolean
        Dim Result As Boolean = False

            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-escscnew_restore;Persist Security Info=True;User ID=crm-escsc;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressLine3 = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",ESCSCEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function




    Public Function RemoveHealthCoreRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-hcpmi;Persist Security Info=True;User ID=crm-hcpmi;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressLine3 = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",HealthCoreEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function


    Public Function RemoveCoreCoverRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionSparky As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-corecover;Persist Security Info=True;User ID=crm-corecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionSparky.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressLine3 = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionSparky)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionSparky)
			    Command2.ExecuteNonQuery()

                DBConnectionSparky.Close()
                End Using
                Result = True
                postEmail("",CoreCoverEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try
        Return Result
    End Function



    Public Function RemoveLifeCoverRecord (AppID As string) As Boolean
        Dim Result As Boolean = False
            Try
                using DBConnectionLC As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-LifeCoverJan20;Persist Security Info=True;User ID=crm-lifecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
                DBConnectionLC.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplications SET App1HomeTelephone = NULL, App1MobileTelephone = NULL, App1EmailAddress = NULL, AddressPostcode = NULL, AddressHouseNumber = NULL, AddressHouseName = NULL, AddressLine1 = NULL, AddressLine2 = NULL, AddressLine3 = NULL, AddressCounty = NULL WHERE AppID = '"& AppID &"'"

                Dim ApplicationStatusUpdate As String = "UPDATE tblApplicationStatus set StatusCode = 'DNS', SubStatusCode = 'OPT' where AppID = '" & AppID &"'"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionLC)
                Command1.ExecuteNonQuery()

                Dim Command2 as New SqlCommand(ApplicationStatusUpdate, DBConnectionLC)
			    Command2.ExecuteNonQuery()

                DBConnectionLC.Close()


                Dim strQry As String = "select ID from tblPolicy where AppID = '"& AppID &"'"
				Dim dtbPolicies As New DataTable
				using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-LifeCoverJan20;Persist Security Info=True;User ID=crm-lifecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
				DBConnection.open()
				Using dadPolicies As New SqlDataAdapter(strQry,DBConnection)
				dadPolicies.Fill(dtbPolicies)
				End Using
				DBConnection.close()
				End Using					
				If (dtbPolicies.Rows.Count > 0) Then           	 
				    For Each Row As DataRow In dtbPolicies.Rows 	
					        using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-LifeCoverJan20;Persist Security Info=True;User ID=crm-lifecover;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=50;")
						    DBConnection.open()


                            Dim objCmd as New SqlCommand("UPDATE tblPolicyStatus SET STATUSCODE = 'DNS', SubStatusCode = 'OPT' where PolicyID = '"& Row.Item("ID") &"'", DBConnection)
								 
						    objCmd.ExecuteNonQuery()
								 
						    DBConnection.close()
						    End Using	 	
					    Next               
	 				End If	
                
                End Using
                Result = True
                postEmail("",LifeCoverEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            

            removeFromDialler(AppID)
            Catch ex As Exception
                Result = False
            End Try

        Return Result
    End Function

    Public Function RemoveRemortgagesRecord (AppID As string, CustomerID As String) As Boolean

        Dim Result As Boolean = False
             Try
                using DBConnectionLC As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages.com;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
                DBConnectionLC.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplicants SET HomeTelephone = NULL, MobileTelephone = NULL, EmailAddress = NULL WHERE AppID = '"& AppID &"'"

                Dim AddressUpdate As String = "UPDATE tbladdressinfo SET AddressLine1 = NULL, AddressLine2 = NULL, Town = NULL, County = NULL, PostCode = NULL  WHERE AppID = '"& AppID &"' and CurrentAddress = 1"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionLC)
                Command1.ExecuteNonQuery()

                Dim Command2 As New SqlCommand(AddressUpdate, DBConnectionLC)
                Command2.ExecuteNonQuery()

                DBConnectionLC.Close()


                Dim strQry As String = "select ProductID from tblproducts where AppID = '"& AppID &"'"
				Dim dtbPolicies As New DataTable
				using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages.com;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
				DBConnection.open()
				Using dadPolicies As New SqlDataAdapter(strQry,DBConnection)
				dadPolicies.Fill(dtbPolicies)
				End Using
				DBConnection.close()
				End Using					
				If (dtbPolicies.Rows.Count > 0) Then           	 
				    For Each Row As DataRow In dtbPolicies.Rows 	
					        using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages.com;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
						    DBConnection.open()


                            Dim objCmd as New SqlCommand("UPDATE tblproductstatus SET STATUSCODE = 'DNS', SubStatusCode = 'OPT' where ProductID = '"& Row.Item("ProductID") &"'", DBConnection)
								 
						    objCmd.ExecuteNonQuery()
								 
						    DBConnection.close()
						    End Using	 	
					    Next               
	 				End If	
                
                End Using
                Result = True
                postEmail("",LifeCoverEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try


        Return Result

    End Function


  

    Public Function RemoveAcceptedRecord (AppID As string, CustomerID As String) As Boolean

        Dim Result As Boolean = False
        Try
                using DBConnectionLC As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
                DBConnectionLC.open()

                Dim ApplicationsUpdate As String = "UPDATE tblApplicants SET HomeTelephone = NULL, MobileTelephone = NULL, EmailAddress = NULL WHERE AppID = '"& AppID &"'"

                Dim AddressUpdate As String = "UPDATE tbladdressinfo SET AddressLine1 = NULL, AddressLine2 = NULL, Town = NULL, County = NULL, PostCode = NULL  WHERE AppID = '"& AppID &"' and CurrentAddress = 1"

                Dim Command1 As New SqlCommand(ApplicationsUpdate, DBConnectionLC)
                Command1.ExecuteNonQuery()

                Dim Command2 As New SqlCommand(AddressUpdate, DBConnectionLC)
                Command2.ExecuteNonQuery()

                DBConnectionLC.Close()


                Dim strQry As String = "select ProductID from tblproducts where AppID = '"& AppID &"'"
				Dim dtbPolicies As New DataTable
				using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
				DBConnection.open()
				Using dadPolicies As New SqlDataAdapter(strQry,DBConnection)
				dadPolicies.Fill(dtbPolicies)
				End Using
				DBConnection.close()
				End Using					
				If (dtbPolicies.Rows.Count > 0) Then           	 
				    For Each Row As DataRow In dtbPolicies.Rows 	
					        using DBConnection As New SqlConnection("Data Source=172.28.27.24;Initial Catalog=crm-remortgages;Persist Security Info=True;User ID=crm-cls;Password=c7Uf5Ke4RUragEBr;MultipleActiveResultSets=True;Pooling=True;Min Pool Size=5;Max Pool Size=500;")
						    DBConnection.open()


                            Dim objCmd as New SqlCommand("UPDATE tblproductstatus SET STATUSCODE = 'DNS', SubStatusCode = 'OPT' where ProductID = '"& Row.Item("ProductID") &"'", DBConnection)
								 
						    objCmd.ExecuteNonQuery()
								 
						    DBConnection.close()
						    End Using	 	
					    Next               
	 				End If	
                
                End Using
                Result = True
                postEmail("",LifeCoverEmailList,"Application Marked as DNS OPT","Application: "& AppID &" has been marked as DNS OPT", True, "", True)
            Catch ex As Exception
                Result = False
            End Try


        Return Result

    End Function

    


    

End Class