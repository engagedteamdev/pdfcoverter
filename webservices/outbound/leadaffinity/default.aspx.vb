﻿Imports Config, Common
Imports System.Xml
Imports System.Net

' ** Revision history **
'
' 09/09/2013    - File created
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID"), MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strXMLURL As String = "", strReference As String = "", strOutboundCampaignID As String = ""
    Private strPost As String = "", strErrorMessage As String = ""
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Server.ScriptTimeout = 180
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub httpPost()
        If (strEnvironment = "live") Then
            strXMLURL = "http://www.leadaffinity.com/leadpost.aspx"
        Else
            strXMLURL = "http://www.leadaffinity.com/leadpost.aspx"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%LeadAffinity%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "LeadAffinityRef") Then strReference = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LeadAffinityMediaCampaignID") Then strOutboundCampaignID = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        strPost += "lsid=" & strReference

        Dim strSQL As String = "SELECT * FROM vwxmlleadaffinity WHERE AppID = " & AppID
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 1) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If
                        strPost += "&" & arrName(UBound(arrName)) & "=" & Row(Column).ToString
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then

        Else
            ' Post the SOAP message.
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To LeadAffinity", strPost)
            Dim objResponse As HttpWebResponse = postLAWebRequest(strXMLURL, strPost)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (checkResponse(objResponse, "")) Then

                Dim strStatus As String = "", strMessage As String = "", strURL As String = "", strPrice As String = ""
                Dim objStatus As XmlNode = objOutputXMLDoc.SelectSingleNode("//status")
                Dim objMessage As XmlNode = objOutputXMLDoc.SelectSingleNode("//message")
                Dim objURL As XmlNode = objOutputXMLDoc.SelectSingleNode("//redirecturl")
                Dim objPrice As XmlNode = objOutputXMLDoc.SelectSingleNode("//price")
                If (Not objStatus Is Nothing) Then
                    strStatus = objStatus.InnerText
                End If
                If (Not objMessage Is Nothing) Then
                    strMessage = objMessage.InnerText
                End If
                If (Not objURL Is Nothing) Then
                    strURL = objURL.InnerText
                End If
                If (Not objPrice Is Nothing) Then
                    strPrice = objPrice.InnerText
                End If

                Select Case strStatus
                    Case "1"
                        'CommonSave.saveStatus(AppID, Config.DefaultUserID, "PAR", "")
                        'CommonSave.saveUpdatedDate(AppID, Config.DefaultUserID, "TransferredToPartner")
                        'updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignIDOutbound", "N", strOutboundCampaignID, 0)
                        'updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignCostOutbound", "N", strPrice, 0)
                        updateDataStoreField(AppID, "PartnerRedirectURL", strURL, "", "")
                        Dim strResults As String = "<Referral><Status>1</Status><RedirectURL>" & strURL & "</RedirectURL></Referral>"
                        postEmail("", "ben.snaize@engaged-solutions.co.uk;chris.holmes@engaged-solutions.co.uk", "Lead Affinity Referral - Accepted", "Application " & AppID & " was accepted by Lead Affinity", True, "")
                        SOAPSuccessfulMessage("Data Successfully Sent To LeadAffinity", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                    Case Else
                        'CommonSave.saveStatus(AppID, Config.DefaultUserID, "PAR", "REJ")
                        'CommonSave.saveUpdatedDate(AppID, Config.DefaultUserID, "TransferredToPartner")
                        'updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignIDOutbound", "N", strOutboundCampaignID, 0)					
                        Dim strResults As String = "<Referral><Status>-5</Status><Reason>" & strMessage & "</Reason><RedirectURL></RedirectURL></Referral>"
                        postEmail("", "ben.snaize@engaged-solutions.co.uk;chris.holmes@engaged-solutions.co.uk", "Lead Affinity Referral - Rejected", "Application " & AppID & " was rejected by Lead Affinity, reason: " & strMessage, True, "")
                        SOAPSuccessfulMessage("Data Unsuccessfully Sent To LeadAffinity", -5, AppID, strResults, objOutputXMLDoc.InnerXml)
                End Select

                objOutputXMLDoc = Nothing
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|Data error")
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing

        End If

    End Sub
	
    Shared Function postLAWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 180000
                .ReadWriteTimeout = 180000
                .ContentType = "application/x-www-form-urlencoded"
            End With
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
