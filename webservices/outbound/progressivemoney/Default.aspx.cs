using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProgressiveMoney;
using EvoCRMService;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];
	

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	public void httpPost()
	{
	
		AnchorProposalClient client = new AnchorProposalClient();
		client.Endpoint.Binding.SendTimeout = new TimeSpan(1, 0, 0);
		client.Endpoint.Binding.OpenTimeout = new TimeSpan(1, 0, 0);
		client.Endpoint.Binding.CloseTimeout = new TimeSpan(1, 0, 0);
		ServiceModel model = new ServiceModel();
		List<Customer> listOfCustomers = new List<Customer>();
		List<Address> listOfCustomerAddresses = new List<Address>();

		EnquiryServiceClient clientcrm = new EnquiryServiceClient();

	
		List<string> valid = new List<string>();
		string productLenderType = "Quote";
	

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		try
		{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwxmlprogressive where AppId = {0}", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{


				DateTime? app1DOB = null;
				DateTime? AddressStartDate = null;
				decimal app1Salary = 0;

				int appId = 0,
					principal = 0,
					proposalTerm = 0;



				 
				if (!string.IsNullOrEmpty(reader["App1DOB"].ToString()))
				{
					app1DOB = DateTime.Parse(reader["App1DOB"].ToString());
				}

				if (!string.IsNullOrEmpty(reader["AddressStartDate"].ToString()))
				{
					AddressStartDate = DateTime.Parse(reader["AddressStartDate"].ToString());
				}



				int.TryParse(reader["AppID"].ToString(), out appId);
			
				int.TryParse(reader["Proposal/Principal"].ToString(), out principal);
				int.TryParse(reader["Proposal/Term"].ToString(), out proposalTerm);
				


				decimal.TryParse(reader["App1Salary"].ToString(), out app1Salary);


				if (app1DOB != null)
				{
					model = new ServiceModel()
					{
					
						
						AppID = appId,
						Proposal_Product_DefID = 2,
						Proposal_Principal = principal,
						Proposal_Term = proposalTerm,
					

					
						//First Applicant
						App1Customer_Title = reader["App1Title"].ToString(),
						App1Customer_Forename = reader["App1FirstName"].ToString(),
						App1Customer_Surname = reader["App1Surname"].ToString().Trim('\r', '\n'),
						App1App1DOB = app1DOB,

						App1Customer_MobileNumber = reader["App1MobileTelephone"].ToString(),
						App1Customer_Email = reader["App1EmailAddress"].ToString(),
						App1Customer_Occupation = reader["App1Occupation"].ToString(),
						App1Customer_OccupationType = reader["App1OccupationType"].ToString(),
						App1Customer_Salary = app1Salary,
						App1ResidentStatus = reader["App1ResidentStatus"].ToString(),
					

					
						//App 1 Customer Address
						Customer_Address_Line1 = reader["AddressLine1"].ToString(),
						Customer_Address_PostCode = reader["AddressPostCode"].ToString(),
						

					};

					

				}
				else
				{
					if (app1DOB == null)
					{
						valid.Add("App 1 DOB is empty");
					}
				}

			

			}

			
			connection.Close();
			
			

			if (!valid.Any())
			{
				valid = ValidationCheck(model);
			}



		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}
	

		if (!valid.Any())
		{

			listOfCustomers = BuildCustomerList(model, ref valid,AppID);
			CreateProposalResult createProposal = null;

			if (!valid.Any())
			{

				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
				try
				{
					List<CustomData> proposalCustomList = CreateCustomProposalData(model, productLenderType);

					createProposal = client.CreateProposal(new CreateProposalRequest()
					{
						Credentials = new SimpleCredentials()
						{
							Account = "EngSoCCUnsecXml",
							Password = "39g5033nu",
							Username = "EngSoCCUnsecXml"
						},
						Proposal = new Proposal()
						{
							Customers = listOfCustomers.ToArray(),
							ProductDefinitionID = (int)model.Proposal_Product_DefID,
							Principal = (int)model.Proposal_Principal,
							Term = (int)model.Proposal_Term,
							ExternalReference = model.AppID.ToString(),
							ProposalPurpose = model.Proposal_Purpose,
							PersonNumberSupplied = false,
							SalesmanNumberSupplied = false,
							SalesAuthoritySupplied = false,
							BranchSupplied = false,
							DealerNumber = "INT121",
							DealerNumberSupplied = true,
							CustomData = proposalCustomList.ToArray()
						}

					});

			
				}
				catch (Exception e)
				{
					throw new Exception(string.Format("Problem: {0}", e.Message));
				}

				


				string details = "";

				foreach (var item in createProposal.Messages)
				{
					details += string.Format("{0} ", item.Text);
				}


				if (string.IsNullOrEmpty(details))
				{



					//XmlDocument myXml = new XmlDocument();
					//XPathNavigator xNav = myXml.CreateNavigator();
					//XmlSerializer xn = new XmlSerializer(createProposal.Proposal.CustomData.GetType());
					//using (var xs = xNav.AppendChild())
					//{
					//	xn.Serialize(xs, createProposal.Proposal.CustomData);
					//}
					//HttpContext.Current.Response.ContentType = "text/xml";
					//HttpContext.Current.Response.Write(myXml.OuterXml);
					//HttpContext.Current.Response.End();

					foreach (var item in createProposal.Proposal.CustomData)
					{
						details += string.Format("{0} {1}", item.Name, item.Value);

						if (item.Name == "ExternalReference2")
						{
							switch (item.Value)
							{
								case "Quote":
								case "Unsecured Loan":

								
									var result = createProposal.Proposal.CustomData;

									foreach (var customData in result)
									{
										if (customData.Name == "Result")
										{
											string returnedResult = customData.Value;

											switch (returnedResult)
											{
												case "Potential To Down Sell":
													//Common.incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1);
													break;
												case "Being Considered for a Secured Loan":
													//Common.incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1);
													break;
												case "Being Considered for an Unsecured Loan":
													//Common.incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1);
												case "Sorry, we probably can't help":
													//Common.incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1);
													break;
												case "Sorry, Can't Make Auto Decn Try Hotkey":
													//Common.incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1);
													break;
												default:
													try
													{

														var newenquiry = clientcrm.TransmitEnquiry(new Enquiry()
														{
															UserId = "Engaged Solutions",
															Password = "3ng4g3d50XML",
															Source = "Engaged Solutions Unsecured Loan",
															FirstName = model.App1Customer_Forename,
															Surname = model.App1Customer_Surname,
															Title = model.App1Customer_Title,
															DateOfBirth = (DateTime)model.App1App1DOB,
															MobileTelephone = model.App1Customer_MobileNumber,
															EmailAddress = model.App1Customer_Email,
															AddressLine1 = model.Customer_Address_Line1,
															Postcode = model.Customer_Address_PostCode,
															ResidentialStatus = model.App1ResidentStatus,
															LoanAmount = model.Proposal_Principal,

														});

														//XmlDocument myXml = new XmlDocument();
														//XPathNavigator xNav = myXml.CreateNavigator();
														//XmlSerializer xn = new XmlSerializer(newenquiry.GetType());
														//using (var xs = xNav.AppendChild())
														//{
														//	xn.Serialize(xs, newenquiry);
														//}
														//HttpContext.Current.Response.ContentType = "text/xml";
														//HttpContext.Current.Response.Write(myXml.OuterXml);
														//HttpContext.Current.Response.End();

													}
													catch (Exception e)
													{

													}
											
													break;
											}
										}
									}

							
									break;
							}
						}
					}
				}
				else
				{
					Response.Write(string.Format("0|{0}", details));
				}
			}
			else
			{
				string errors = "0| ";
				foreach (var item in valid)
				{
					errors += string.Format("{0} ", item);
				}
				Response.Write(errors);
			}
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("0| ");

			foreach (var item in valid)
			{
				sb.Append(string.Format("{0}, ", item));
			}
			Response.Write(sb.ToString());
		}

	}



	private static List<CustomData> CreateCustomProposalData(ServiceModel model, string type)
	{
		List<CustomData> listOfCustomData = new List<CustomData>();


		listOfCustomData.Add(new CustomData()
		{
			Name = "ExternalReference2",
			Value = type
		});



		return listOfCustomData;
	}


	private static List<Customer> BuildCustomerList(ServiceModel model, ref List<string> valid, string AppID)
	{
	
		GenderType gender;

		List<Customer> listOfCustomers = new List<Customer>();
		List<Address> listOfCustomerAddresses = new List<Address>();
		BuildCustomData bcd = new BuildCustomData(model);
	
	

	
		listOfCustomerAddresses = GenerateAddressHistory(model,AppID);

		gender = GetGenderOfCustomer(model.App1Customer_Title);

		listOfCustomers.Add(new Customer()
		{
			CustomerType = CustomerType.Primary,
			Title = model.App1Customer_Title,
			Forename = model.App1Customer_Forename,
			Surname = model.App1Customer_Surname,
			DOB = (DateTime)model.App1App1DOB,
			MobileNumber = model.App1Customer_MobileNumber,
			Email = model.App1Customer_Email,
			Occupation = model.App1Customer_Occupation,
			OccupationType = model.App1Customer_OccupationType,
			Salary = (decimal)model.App1Customer_Salary,
			ExternalReference = model.AppID.ToString(),
			Addresses = listOfCustomerAddresses.ToArray(),
			
		});


	

		return listOfCustomers;
	}

	private static List<Address> GenerateAddressHistory(ServiceModel model, string AppID)
	{

		DateTime AddressStartDate = Config.DefaultDate;

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwxmlevomoneycsharp where AppId = {0}", AppID), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{

			if (!string.IsNullOrEmpty(reader["AddressStartDate"].ToString()))
			{
				AddressStartDate = DateTime.Parse(reader["AddressStartDate"].ToString());
			}
		}

		List<Address> listOfCustomerAddresses = new List<Address>();
		

		listOfCustomerAddresses.Add(new Address()
		{
			AddressType = AddressType.Current,
			PostCode = model.Customer_Address_PostCode,
			Line1 = model.Customer_Address_Line1,
			ResidentStatus = model.App1ResidentStatus,
			AtAddressFrom = AddressStartDate,
			PhoneNumber = model.App1Customer_MobileNumber
		});

		listOfCustomerAddresses.Add(new Address()
		{
			AddressType = AddressType.EmployerCurrent,
			Line1 = "x",
			AtAddressFrom = DateTime.Parse("2009-08-21"),
		});




		return listOfCustomerAddresses;
	}

	private static GenderType GetGenderOfCustomer(string gender)
	{
		switch (gender)
		{
			case "Mr":
				return GenderType.Male;
			default:
				return GenderType.Female;
		}
	}

	private static List<string> ValidationCheck(ServiceModel model)
	{
		List<string> RequiredPropertyList = new List<string>();

		if (model.Proposal_Product_DefID == null)
			RequiredPropertyList.Add("Product Definition ID missing or not valid.");

		if (model.Proposal_Principal == null || model.Proposal_Principal <= 0)
			RequiredPropertyList.Add("Proposal Principal missing or not valid.");

		if (model.Proposal_Term == null)
			RequiredPropertyList.Add("Term missing or not valid.");

		if (string.IsNullOrEmpty(model.App1Customer_Title))
			RequiredPropertyList.Add("Customer Title missing or not valid.");

		if (string.IsNullOrEmpty(model.App1Customer_Forename))
			RequiredPropertyList.Add("Customer Forename missing or not valid.");

		if (string.IsNullOrEmpty(model.App1Customer_Surname))
			RequiredPropertyList.Add("Customer Surname missing or not valid.");

		if (model.App1App1DOB == null)
			RequiredPropertyList.Add("Customer DOB missing or not valid.");

	


		if (string.IsNullOrEmpty(model.App1Customer_Occupation))
			RequiredPropertyList.Add("Customer Occupation missing or not valid");

		if (string.IsNullOrEmpty(model.App1Customer_OccupationType))
			RequiredPropertyList.Add("Customer Occupation Type missing or not valid");

		if (model.App1Customer_Salary == null || model.App1Customer_Salary <= 0)
			RequiredPropertyList.Add("Customer Salary missing or not valid ");

		if (string.IsNullOrEmpty(model.Customer_Address_Line1))
			RequiredPropertyList.Add("Customer Address Line 1 missing or not valid");

		if (string.IsNullOrEmpty(model.Customer_Address_PostCode))
			RequiredPropertyList.Add("Customer Postcode missing or not valid");

	


		return RequiredPropertyList;
	}

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		//string strQry = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES(" + common.formatField(ip, "", "") + ", " + common.formatField(soapid, "N", 0) + ", " + common.formatField(result, "N", 0) + ", " + common.formatField(msg, "", "") + ", " + common.formatField(strPost, "", "") + ", " + common.formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) + ") ";
		//Common.executeNonQuery(strQry);
	}

	class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}

	class ServiceModel
	{
		//Main Information
		public int? CompanyID { get; set; }
		public int? AppID { get; set; }
		public int? Proposal_Product_DefID { get; set; }
		public int? Proposal_Principal { get; set; }
		public int? Proposal_Term { get; set; }
		public string Proposal_Purpose { get; set; }

		//First Applicant Details
		public string App1Customer_Title { get; set; }
		public string App1Customer_Forename { get; set; }
		public string App1Customer_Surname { get; set; }
		public DateTime? App1App1DOB { get; set; }
		public string App1Customer_MartitalStatus { get; set; }
		public string App1Customer_MobileNumber { get; set; }
		public string App1Customer_Email { get; set; }
		public string App1Customer_Occupation { get; set; }
		public string App1Customer_OccupationType { get; set; }
		public string App1Customer_Address_PhoneNumber { get; set; }
		public decimal? App1Customer_Salary { get; set; }
		public string App1EmployerName { get; set; }
		public string App1ResidentStatus { get; set; }



		//App1 Main Address
		public string Customer_Address_Line1 { get; set; }
		public string Customer_Address_Line2 { get; set; }
		public string Customer_Address_Town { get; set; }
		public string Customer_Address_County { get; set; }
		public string Customer_Address_PostCode { get; set; }
		public string Customer_Address_Tenure { get; set; }
		public int? AddressYears { get; set; }
		public int? AddressMonths { get; set; }
		public DateTime? AddressStartDate { get; set; }

		//App1 Employer Address
		public string Employer_Address_Line1 { get; set; }
		public string Employer_Address_Line2 { get; set; }
		public string Employer_Address_Town { get; set; }
		public string Employer_Address_County { get; set; }
		public string Employer_Address_PostCode { get; set; }
		public int? EmployerAddressYears { get; set; }
		public int? EmployerAddressMonths { get; set; }



		//App2 Main Address
		public string App2_Address_Line1 { get; set; }
		public string App2_Address_Line2 { get; set; }
		public string App2_Address_Town { get; set; }
		public string App2_Address_County { get; set; }
		public string App2_Address_PostCode { get; set; }
		public string App2_Address_Tenure { get; set; }
		public int? App2_AddressYears { get; set; }
		public int? App2_AddressMonths { get; set; }

		//Other Details        
		public decimal? MortgageOutstanding { get; set; }
		public decimal? PropertyValue { get; set; }
		public DateTime? PropertyPurchaseDate { get; set; }
		public decimal? MortgageMonthlyPayment { get; set; }
		public bool CorrespondenceAddressSame { get; set; }
	}
}