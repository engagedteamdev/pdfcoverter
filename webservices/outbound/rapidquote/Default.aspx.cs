using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;
using System.Linq;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public partial class Default : Page
{

	private string AppID = HttpContext.Current.Request["AppID"];




	class theQuote
	{
		public string cover_date { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string email { get; set; }
		public string birthdate { get; set; }
		public int age { get; set; }
		public string postcode { get; set; }
		public bool smoking { get; set; }
		public int bmi { get; set; }
		public string occupation { get; set; }
		public string hospital_rate { get; set; }
		public string aviva_hospital_rate { get; set; }
		public int underwriting { get; set; }
		public bool ncd_uplift { get; set; }
		public int? years_insured { get; set; }
		public int? years_claim_free { get; set; }
		public int? number_of_claims { get; set; }
		public bool? treatment_consulted { get; set; }
		public bool? planned { get; set; }
		public bool? heart_condition { get; set; }
		public bool? mental_condition { get; set; }
		public bool? diabetes_condition { get; set; }
		public bool? cancer_condition { get; set; }
		public bool? stroke_condition { get; set; }
		public bool quote_for_vitality { get; set; }
		public string vitality_current_insurer { get; set; }
		public string vitality_product_type { get; set; }
		public string vitality_hospital_list { get; set; }
		public double? vitality_current_premium { get; set; }
		public int? vitality_current_excess { get; set; }
		public string vitality_renewal_date { get; set; }
		public double? vitality_renewal_premium { get; set; }
		public bool? vitality_full_op { get; set; }
		public bool? vitality_treatment { get; set; }
		public bool? vitality_cancer { get; set; }
		public bool? vitality_psychiatric { get; set; }
		public bool? vitality_travel { get; set; }
		public bool? vitality_medical_issues { get; set; }
		public int? vitality_condition_cancer { get; set; }
		public int? vitality_condition_heart { get; set; }
		public int? vitality_condition_prostate { get; set; }
		public int? vitality_condition_diabetes { get; set; }
		public int? vitality_condition_mental { get; set; }
		public int? vitality_condition_backpain { get; set; }
		public int? vitality_condition_arthritis { get; set; }
		public int? vitality_condition_admission { get; set; }
		public int? vitality_condition_other { get; set; }
		public int coverage { get; set; }
		//public int op_level { get; set; }
		public bool full_diagnostics { get; set; }
		public bool heart_and_cancer { get; set; }
		public bool treatment { get; set; }
		public bool psychiatric { get; set; }
		public bool dental { get; set; }
		public bool worldwide { get; set; }
		public bool travel { get; set; }
		public bool six_week { get; set; }
		public bool pncd { get; set; }
		public bool extended_cover { get; set; }
		public bool pre_existing_condition { get; set; }
		public int vitality_mental { get; set; }
		public int vitality_dental { get; set; }
		public int? travel_type { get; set; }
		public int excess_type { get; set; }
		public int excess_level { get; set; }
		public string payment_mode { get; set; }
		public bool display_discount { get; set; }
		public bool axa_consent { get; set; }
		public int company_ncd_level { get; set; }
		public theMembers[] members { get; set; }
	}

	class theMembers
    {
		public string relationship { get; set; }
		public string birthdate { get; set; }
		public int? years_insured { get; set; }
		public int? years_claim_free { get; set; }
		public int? number_of_claims { get; set; }
		public bool smoking { get; set; }
	}






	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void retrieveOccupationList()
	{

		string GetURL = "http://uat-api.rapid-quote.co.uk/v1/engine/occupation";

		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(GetURL);
		client.DefaultRequestHeaders.Add("RQ-API-KEY", "c1olr0lNsPkIRD7k25iwScUt5fxddQOn"); 

		var res = client.GetAsync(GetURL).Result;

		var responseContent = res.Content.ReadAsStringAsync().Result;

		Response.Write(res.Content.ReadAsStringAsync().Result);


		//var respdocuments = JObject.Parse(responseContent);







	}




	public void Run()
	{

		//try
		//{

			theQuote data = new theQuote();


			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringHeathCrawford"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception e)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
			}

			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwrapidquote where AppID = '{0}'", AppID), connection);

			var reader = myCommand.ExecuteReader();


			string SwitchProduct = "";
			while (reader.Read())
			{
				SwitchProduct = reader["RPSwitchProduct"].ToString();
				data.cover_date = reader["PolicyStartDate"].ToString();
				data.first_name = reader["App1FirstName"].ToString();
				data.last_name = reader["App1Surname"].ToString();
				data.email = reader["App1EmailAddress"].ToString();
				data.birthdate = reader["App1DOB"].ToString();
				if (reader["App1Age"].ToString() != "")
				{
					data.age = Convert.ToInt32(reader["App1Age"].ToString());
				}
				data.postcode = reader["AddressPostCode"].ToString();
				if(reader["App1Smoker"].ToString() == "Yes")
                {
					data.smoking = true;
				}
                else
                {
					data.smoking = false;
				}
				if (reader["RPBMI"].ToString() != "")
				{
					data.bmi = Convert.ToInt32(reader["RPBMI"].ToString());
				}
				data.occupation = reader["App1Occupation"].ToString();
				data.hospital_rate = reader["RPHospitalRate"].ToString();
				data.aviva_hospital_rate = reader["RPAvivaHospitalRate"].ToString();
				if (reader["RPUnderwriting"].ToString() != "")
				{
					data.underwriting = Convert.ToInt32(reader["RPUnderwriting"].ToString());
				}
				if (reader["RPNCDUpLift"].ToString() == "Yes")
				{
					data.ncd_uplift = true;
				}
				else
				{
					data.ncd_uplift = false;
				}
				if(reader["RPSwitchProduct"].ToString() == "Yes") {
					if (reader["RPYearsInsured"].ToString() != "")
					{
						data.years_insured = Convert.ToInt32(reader["RPYearsInsured"].ToString());
					}
					if (reader["RPYearsClaimFree"].ToString() != "")
					{
						data.years_claim_free = Convert.ToInt32(reader["RPYearsClaimFree"].ToString());
					}
					if (reader["RPNoOfClaims"].ToString() != "")
					{
						data.number_of_claims = Convert.ToInt32(reader["RPNoOfClaims"].ToString());
					}
					if (reader["RPTreatmentConsulted"].ToString() == "Yes")
					{
						data.treatment_consulted = true;
					}
					else
					{
						data.treatment_consulted = false;
					}
					if (reader["RPPlanned"].ToString() == "Yes")
					{
						data.planned = true;
					}
					else
					{
						data.planned = false;
					}
					if (reader["RPHeartCondition"].ToString() == "Yes")
					{
						data.heart_condition = true;
					}
					else
					{
						data.heart_condition = false;
					}
					if (reader["RPMentalCondition"].ToString() == "Yes")
					{
						data.mental_condition = true;
					}
					else
					{
						data.mental_condition = false;
					}
					if (reader["RPDiabetesCondition"].ToString() == "Yes")
					{
						data.diabetes_condition = true;
					}
					else
					{
						data.diabetes_condition = false;
					}
					if (reader["RPCancerCondition"].ToString() == "Yes")
					{
						data.cancer_condition = true;
					}
					else
					{
						data.cancer_condition = false;
					}
					if (reader["RPStrokeCondition"].ToString() == "Yes")
					{
						data.stroke_condition = true;
					}
					else
					{
						data.stroke_condition = false;
					}
				}


			data.quote_for_vitality = true;

			if (reader["RPQuoteForVitality"].ToString() == "Yes")
				{
					data.quote_for_vitality = true;
					data.vitality_current_insurer = reader["RPVitalityCurrentInsurer"].ToString();
					data.vitality_product_type = reader["RPVitalityProductType"].ToString();
					data.vitality_hospital_list = reader["RPVitalityHospitalList"].ToString();

					if (reader["RPVitalityCurrentPremium"].ToString() != "")
					{
						data.vitality_current_premium = Convert.ToDouble(reader["RPVitalityCurrentPremium"].ToString());
					}
					if (reader["RPVitalityCurrentExcess"].ToString() != "")
					{
						data.vitality_current_excess = Convert.ToInt32(reader["RPVitalityCurrentExcess"].ToString());
					}
					if (reader["RPVitalityRenewalDate"].ToString() != "")
					{
						data.vitality_renewal_date = reader["RPVitalityRenewalDate"].ToString();
					}
					if (reader["RPVitalityRenewalPremium"].ToString() != "")
					{
						data.vitality_renewal_premium = Convert.ToDouble(reader["RPVitalityRenewalPremium"].ToString());
					}


					if (reader["RPVitalityFullOp"].ToString() == "Yes")
					{
						data.vitality_full_op = true;
					}
					else
					{
						data.vitality_full_op = false;
					}
					if (reader["RPVitalityTreatment"].ToString() == "Yes")
					{
						data.vitality_treatment = true;
					}
					else
					{
						data.vitality_treatment = false;
					}
					if (reader["RPVitalityCancer"].ToString() == "Yes")
					{
						data.vitality_cancer = true;
					}
					else
					{
						data.vitality_cancer = false;
					}
					if (reader["RPVitalityPsychiatric"].ToString() == "Yes")
					{
						data.vitality_psychiatric = true;
					}
					else
					{
						data.vitality_psychiatric = false;
					}
					if (reader["RPVitalityTravel"].ToString() == "Yes")
					{
						data.vitality_travel = true;
					}
					else
					{
						data.vitality_travel = false;
					}
					if (reader["RPVitalityMedicalIssues"].ToString() == "Yes")
					{
						data.vitality_medical_issues = true;
					}
					else
					{
						data.vitality_medical_issues = false;
					}
					if (reader["RPVitalityConditionCancer"].ToString() != "")
					{
						data.vitality_condition_cancer = Convert.ToInt32(reader["RPVitalityConditionCancer"].ToString());
					}
					if (reader["RPVitalityConditionHeart"].ToString() != "")
					{
						data.vitality_condition_heart = Convert.ToInt32(reader["RPVitalityConditionHeart"].ToString());
					}
					if (reader["RPVitalityConditionProstate"].ToString() != "")
					{
						data.vitality_condition_prostate = Convert.ToInt32(reader["RPVitalityConditionProstate"].ToString());
					}
					if (reader["RPVitalityConditionDiabetes"].ToString() != "")
					{
						data.vitality_condition_diabetes = Convert.ToInt32(reader["RPVitalityConditionDiabetes"].ToString());
					}
					if (reader["RPVitalityConditionMental"].ToString() != "")
					{
						data.vitality_condition_mental = Convert.ToInt32(reader["RPVitalityConditionMental"].ToString());
					}
					if (reader["RPVitalityConditionBackPain"].ToString() != "")
					{
						data.vitality_condition_backpain = Convert.ToInt32(reader["RPVitalityConditionBackPain"].ToString());
					}
					if (reader["RPVitalityConditionArthritis"].ToString() != "")
					{
						data.vitality_condition_arthritis = Convert.ToInt32(reader["RPVitalityConditionArthritis"].ToString());
					}
					if (reader["RPVitalityConditionAdmission"].ToString() != "")
					{
						data.vitality_condition_admission = Convert.ToInt32(reader["RPVitalityConditionAdmission"].ToString());
					}
					if (reader["RPVitalityConditionOther"].ToString() != "")
					{
						data.vitality_condition_other = Convert.ToInt32(reader["RPVitalityConditionOther"].ToString());
					}

				}
				else
				{
					data.quote_for_vitality = false;
				
				}
				if (reader["RPCoverage"].ToString() != "")
				{
					data.coverage = Convert.ToInt32(reader["RPCoverage"].ToString());
				}
				if (reader["RPOpLevel"].ToString() != "")
				{
					//data.op_level = Convert.ToInt32(reader["RPOpLevel"].ToString());
				}
				if (reader["RPFullDiagnostics"].ToString() == "Yes")
				{
					data.full_diagnostics = true;
				}
				else
				{
					data.full_diagnostics = false;
				}
				if (reader["RPHeartCancer"].ToString() == "Yes")
				{
					data.heart_and_cancer = true;
				}
				else
				{
					data.heart_and_cancer = false;
				}
				if (reader["RPTreatment"].ToString() == "Yes")
				{
					data.treatment = true;
				}
				else
				{
					data.treatment = false;
				}
				if (reader["RPPsychiatric"].ToString() == "Yes")
				{
					data.psychiatric = true;
				}
				else
				{
					data.psychiatric = false;
				}
				if (reader["RPDental"].ToString() == "Yes")
				{
					data.dental = true;
				}
				else
				{
					data.dental = false;
				}
				if (reader["RPWorldwide"].ToString() == "Yes")
				{
					data.worldwide = true;
				}
				else
				{
					data.worldwide = false;
				}
				if (reader["RPTravel"].ToString() == "Yes")
				{
					data.travel = true;
				}
				else
				{
					data.travel = false;
				}
				if (reader["RPSixweeks"].ToString() == "Yes")
				{
					data.six_week = true;
				}
				else
				{
					data.six_week = false;
				}
				if (reader["RPpncd"].ToString() == "Yes")
				{
					data.pncd = true;
				}
				else
				{
					data.pncd = false;
				}
				if (reader["RPExtendedCover"].ToString() == "Yes")
				{
					data.extended_cover = true;
				}
				else
				{
					data.extended_cover = false;
				}
				if (reader["RPPreExistingCondition"].ToString() == "Yes")
				{
					data.pre_existing_condition = true;
				}
				else
				{
					data.pre_existing_condition = false;
				}
				if (reader["RPVitalityMental"].ToString() != "")
				{
					data.vitality_mental = Convert.ToInt32(reader["RPVitalityMental"].ToString());
				}
				if (reader["RPVitalityDental"].ToString() != "")
				{
					data.vitality_dental = Convert.ToInt32(reader["RPVitalityDental"].ToString());
				}
				if (reader["RPTravelType"].ToString() != null && reader["RPTravel"].ToString() == "Yes")
				{
					data.travel_type = Convert.ToInt32(reader["RPTravelType"].ToString());
				}
				if (reader["RPExcessType"].ToString() != "")
				{
					data.excess_type = Convert.ToInt32(reader["RPExcessType"].ToString());
				}
				if (reader["RPExcessLevel"].ToString() != "")
				{
					data.excess_level = Convert.ToInt32(reader["RPExcessLevel"].ToString());
				}
				data.payment_mode = reader["RPPaymentMode"].ToString();
				if (reader["RPDisplayDiscount"].ToString() == "Yes")
				{
					data.display_discount = true;
				}
				else
				{
					data.display_discount = false;
				}
				if (reader["RPAxaConsent"].ToString() == "Yes")
				{
					data.axa_consent = true;
				}
				else
				{
					data.axa_consent = false;
				}
				if (reader["RPCompanyNCDLevel"].ToString() != "")
				{
					data.company_ncd_level = Convert.ToInt32(reader["RPCompanyNCDLevel"].ToString());
				}
				

			}



			SqlCommand myCommandMembers = new SqlCommand(string.Format("Select * FROM vwrapidquotemembers where AppID = '{0}'", AppID), connection);

			var readerMembers = myCommandMembers.ExecuteReader();

			List<theMembers> members = new List<theMembers>();
		string Relationship = "";
			while (readerMembers.Read())
			{

				if (readerMembers["Type"].ToString() == "Dependant")
				{
					Relationship = "son";
				}
				else
				{
					Relationship = "spouse_partner";
				}

				if (SwitchProduct == "Yes")
				{

					members.Add(
					new theMembers
					{
						relationship = Relationship,
						birthdate = readerMembers["DependantDOBAGE"].ToString(),
						years_insured = Convert.ToInt32(readerMembers["DependantYearsInsured"].ToString()),
						years_claim_free = Convert.ToInt32(readerMembers["DependantYearsClaimFree"].ToString()),
						number_of_claims = Convert.ToInt32(readerMembers["DependantNoOfClaims"].ToString()),
						smoking = false
					}
					);
                }
                else
                {
					members.Add(
					new theMembers
					{
						relationship = Relationship,
						birthdate = readerMembers["DependantDOBAGE"].ToString(),
						smoking = false
					}
					);
				}

			}
			data.members = members.ToArray();

			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;

			string json = JsonConvert.SerializeObject(data, settings);


		string PostURL = "http://uat-api.rapid-quote.co.uk/v1/engine/quote";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(PostURL);
			client.DefaultRequestHeaders.Add("RQ-API-KEY", "c1olr0lNsPkIRD7k25iwScUt5fxddQOn");


			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(PostURL, contentBody).Result;

			var responseContent = res.Content.ReadAsStringAsync().Result;

		

		if (res.ReasonPhrase.ToString() != "OK")
		{
			Response.Write("0|" + res.ReasonPhrase + "");
		}
        else { 

		var resp = JObject.Parse(responseContent);

		

		var errorcode = "";

		try
		{
			errorcode = resp["error_code"].ToString();

		}
		catch
		{
			errorcode = "";

		}

		if(errorcode != "")
        {
			var errormessage = "";

			errormessage = resp["errors"][0].ToString();
			Response.Write("0|" + errormessage);
        }
        else
        {
			SqlCommand update = new SqlCommand();
			update.Connection = connection;
			update.CommandText = string.Format("delete from tblrapidquoteresults where AppID = '" + AppID + "'");
			update.ExecuteNonQuery();

			JArray items = (JArray)resp["quote_result"];

			for (int i = 0; i < items.Count; i++)
			{
				string Logo = "", CompanyName = "", ProductName = "", Price = "", Excess = "";
				int ProviderID = 0;
				var item = (JObject)items[i];
				foreach (JProperty property in item.Properties())
				{

					

					if(property.Name == "logo")
                    {
						Logo = property.Value.ToString(); 

					}

					if (property.Name == "company_name")
					{
						CompanyName = property.Value.ToString();

					}

					if (property.Name == "product")
					{
						ProductName = property.Value.ToString();

					}

					if (property.Name == "provider_id")
					{
						ProviderID = Convert.ToInt32(property.Value);

					}

					if (property.Name == "price")
					{

						var respPrice = JObject.Parse(property.Value.ToString());

						Price = respPrice["base_rate"].ToString();

					}

					if (property.Name == "cover_details")
					{
						var respExcess = JObject.Parse(property.Value.ToString());

						Excess = respExcess["compulsory_excess"].ToString();

					}




				}


			

				SqlCommand updatequotes = new SqlCommand();
				updatequotes.Connection = connection;
				updatequotes.CommandText = string.Format("insert into tblrapidquoteresults (AppID,ProviderID,ProviderLogo,ProviderName,ProviderProductName,ProviderPrice,ProviderExcess) values('" + AppID + "','" + ProviderID + "','" + Logo + "','" + CompanyName + "','" + ProductName + "','" + Price + "','" + Excess + "')");
				updatequotes.ExecuteNonQuery();


				



			}

			Response.Write("1|Success");
			}
		}


		client.Dispose();

       // }
       // catch (Exception e)
       // {
		//	Response.Write(e.Message);
		//}


	}

}
