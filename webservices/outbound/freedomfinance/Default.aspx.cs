using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.ServiceModel;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;

public partial class Default : Page
{

	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strPostURL = "";

	class leadoverall
	{
		public leadinner lead { get; set; }
	}

	class leadinner
	{
		public string lead_source { get; set; }
		public int loan_amount { get; set; }
		public int loan_term { get; set; }
		public string loan_purpose { get; set; }
		public string reference { get; set; }
		public string resume_online { get; set; }
		public theapplicant applicant { get; set; }
		
	}

	class theapplicant
	{
		public string title { get; set; }
		public string forename { get; set; }
		public string surname { get; set; }
		public string dob { get; set; }
		public string mobile_phone { get; set; }
		public string home_phone { get; set; }
		public string email { get; set; }
		public string residential_status { get; set; }
		public theaddress address { get; set; }
	}

	class theaddress
	{
		
		public string house_number { get; set; }
		public string house_name { get; set; }
		public string postcode { get; set; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{

		ServicePointManager.Expect100Continue = true;
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		leadoverall leaddata = new leadoverall();
		leadinner lead = new leadinner();
		theapplicant appdata = new theapplicant();
		theaddress addressdata = new theaddress();

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringES"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwfreedomfinance where AppID = '{0}'", strAppID), connection);

		var readersql = myCommand.ExecuteReader();

		while (readersql.Read())
		{

			int Amount = 0;
			int.TryParse(readersql["Amount"].ToString(), out Amount);
			lead.loan_amount = Amount;

			int ProductTerm = 0;
			int.TryParse(readersql["ProductTerm"].ToString(), out ProductTerm);
			lead.loan_term = ProductTerm * 12;

			lead.lead_source = "THLW";
			lead.resume_online = "Y";
			lead.loan_purpose = readersql["ProductPurpose"].ToString();

			lead.reference = strAppID;

			appdata.title = readersql["App1Title"].ToString();
			appdata.forename = readersql["App1FirstName"].ToString();
			appdata.surname = readersql["App1Surname"].ToString();
			appdata.dob = readersql["App1DOB"].ToString();
			appdata.mobile_phone = readersql["App1MobileTelephone"].ToString();
			appdata.home_phone = readersql["App1HomeTelephone"].ToString();
			if (appdata.home_phone == "")
			{
				appdata.home_phone = null;
			}

			appdata.email = readersql["App1EmailAddress"].ToString();
			appdata.residential_status = readersql["AddressLivingArrangement"].ToString();

			addressdata.house_name = readersql["AddressHouseName"].ToString();
			addressdata.house_number = readersql["AddressHouseNumber"].ToString();
			addressdata.postcode = readersql["AddressPostCode"].ToString();

			if (addressdata.house_name == "")
			{
				addressdata.house_name = null;
			}

			if (addressdata.house_number == "")
			{
				addressdata.house_number = null;
			}
		}


		try
		{
			string json = "";

			lead.applicant = appdata;
			appdata.address = addressdata;
			leaddata.lead = lead;

			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;

			json = JsonConvert.SerializeObject(leaddata, settings);

			
			//HttpContext.Current.Response.Write(json);
			//HttpContext.Current.Response.Flush();
			//HttpContext.Current.Response.SuppressContent = true;
			//HttpContext.Current.ApplicationInstance.CompleteRequest();

			strPostURL = "https://api.freedomfinance.co.uk/v1/lead";
			HttpClient client = new HttpClient();
			byte[] cred = UTF8Encoding.UTF8.GetBytes("trustedhomeloans@freedomfinance.com:DpdWM6uWunSK@KD^");
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
			client.BaseAddress = new Uri(strPostURL);

			HttpContent contentBody = new StringContent(json);

			var res = client.PostAsync(strPostURL, contentBody).Result;
			var resString = res.Content.ReadAsStringAsync();
			
	
			if (res.IsSuccessStatusCode)
			{
				var parsed = JObject.Parse(resString.Result.ToString());

				var appURL = parsed.SelectToken("app_url").Value<string>();

				HttpContext.Current.Response.Write("1|" + appURL);
				
			}
			else
			{
				HttpContext.Current.Response.Write("0|" + res.IsSuccessStatusCode + " " + res.ReasonPhrase);
			}


			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();

		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write("0|Error");
			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

	}

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringES"].ToString());
		connection.Open();
		SqlCommand updatecmd2 = new SqlCommand();
		updatecmd2.Connection = connection;
		updatecmd2.CommandText = string.Format("INSERT INTO tblxmlreceived(XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES('" + ip + "', '" + strAppID + "', 1, '" + msg + "', '" + strPost + "', getdate()) ");
		updatecmd2.ExecuteNonQuery();
		connection.Close();
	}



}
