﻿<%@ Page Language="VB" AutoEventWireup="true" EnableViewState="false" CodeFile="hotkey.aspx.vb" Inherits="Hotkey"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%  If (intWorkflowID > 0) Then
        Response.Write(Common.simpleHeader(objLeadPlatform.Config.CompanyName, False, strTitle, "", "", "setToolbarHeight();", "", True, False, False))
    ElseIf (strView = "small") Then
        Response.Write(Common.simpleHeader(objLeadPlatform.Config.CompanyName, False, strTitle, "", "", "setToolbarHeight();", "", True, False, False))
    Else
        Response.Write(Common.simpleHeader(objLeadPlatform.Config.CompanyName, False, strTitle, "", "", "", "", True, True, False))
    End If
%>
<body<%= Common.strBodyTag%>>
    <%  If (intWorkflowID > 0) Then
            Response.Write(Common.bannerHeader(True, False))
        ElseIf (strView = "small") Then
            Response.Write(Common.bannerHeader(False, False))
        Else
            Response.Write(Common.bannerHeader(True, True))
        End If
    %>
    <script type="text/javascript" src="/net/scripts/hotkey.aspx"></script>
    <script type="text/javascript" src="/net/scripts/jquery/js/jquery.alerts.js"></script>
    <link href="/net/styles/alerts/jquery.alerts.css" rel="stylesheet" type="text/css">
    <div id="applicationBox">
        <span class="status floatRight">
            <%= Common.caseLockImage(strLockUserName)%><%= Common.statusCodes(AppID)%></span>
        <h1>
            <%=strTitle%><span class="small">:
                <%= AppID%></span></h1>
        <h3>
            <%= Common.applicationPages(AppID, -1)%></h3>
        <% If (strView <> "small") Then%>
        <% = objLeadPlatform.ToolBar.drawCallTools(AppID, strTelephoneNumber, "")%>
        <% End If%>
        <div id="tabs" style="float: left; width: 82%">
            <ul>
                <li><a href="#tabs-1">Options -
                    <%= Common.highLevelDetails(AppID)%></a></li>
                <li><a href="#tabs-2" onclick="location.href='/net/prompts/rules.aspx?frmAppID=<%=AppID%>&frmView2=<%=strView%>';">
                    Rules</a></li>
            </ul>
            <div id="tabs-1">
                <form name="form1" id="form1" method="get" action="" onsubmit="return dataValidationHotkey();"
                runat="server">
                <table border="0" cellpadding="" cellspacing="" class="table" width="100%">
                    <%=getHotkeyOptions()%>
                </table>
                </form>
            </div>
            <div id="tabs-2">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            try {
                $("#tabs").tabs({ selected: 0 });
                setHotkeyWidth();
            }
            catch (e) { }
        });
        $(document).ready(function () {
            try {
                setToolbarHeight();
            }
            catch (e) { }
        });
    </script>
    <%
        If (intWorkflowID > 0) Then
            Response.Write(Common.simpleFooter(objLeadPlatform.Config.DefaultUserFullName, False))
        ElseIf (strView = "small") Then
            Response.Write(Common.simpleFooter(objLeadPlatform.Config.DefaultUserFullName, False))
        Else
            Response.Write(Common.simpleFooter(objLeadPlatform.Config.DefaultUserFullName, True))
        End If
    %>
</body>
</html>
