﻿Imports SSL, Config, Common, CallInterface, LogonCheck

Partial Class Hotkey
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = Nothing
    Public strTitle As String = HttpContext.Current.Request("frmTitle")
    Private strSuccessMessage As String = HttpContext.Current.Request("strSuccessMessage")
    Private strFailureMessage As String = HttpContext.Current.Request("strFailureMessage")
    Public AppID As String = HttpContext.Current.Request("AppID")
    Private MediaCampaignID As String = getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID).ToString
    Private intDialerGrade As String = getAnyField("MediaCampaignDialerGradeID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID).ToString
    Private intDialerLoadType As String = getAnyField("MediaCampaignDialerLoadTypeID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID).ToString
    Private intSubStatusGroupID As String = getAnyField("MediaCampaignSubStatusGroupID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID).ToString
    Public strCallTelephoneNumber As String = HttpContext.Current.Request("TelephoneNumber")
    Public strTelephoneNumber As String = ""
    Public intWorkflowTypeID As String = ""
    Public intWorkflowID As String = ""
    Private strAction As String = HttpContext.Current.Request("Action")
    Public strLockUserName As String = ""
    Public strView As String = HttpContext.Current.Request("frmView")
    Private strClass As String = "row1"

    Private Enum DeliveryType
        IncomingData = 1
        XMLTransfer = 2
        Email = 3
        EmailCSV = 4
        VoiceOnly = 5
        XMLLenderScoring = 6
        IssueDocuments = 7
        EmailTextOnly = 8
    End Enum

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        checkSSL()
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise(True)
        objLeadPlatform.LogonCheck.checkLogon(objLeadPlatform.Config.DefaultUserID, "UserWorkflows", False)
        If (strTitle = "") Then strTitle = "Hotkey"
        If (checkKickOut(objLeadPlatform.Config.DefaultUserID, True)) Then
            Response.Redirect("/net/workflow/workflow.aspx?intWorkflowTypeID=" & intWorkflowTypeID)
        End If
        If (Not checkValue(MediaCampaignID) Or Not checkValue(AppID)) Then
            strFailureMessage = "Invalid AppID found"
        End If
        callLogging(AppID, strAction, strCallTelephoneNumber, objLeadPlatform.Config.DefaultUserID)
        strTelephoneNumber = getAnyField("UserActiveWorkflowTelephoneNumber", "tblusers", "UserID", objLeadPlatform.Config.DefaultUserID)
        intWorkflowTypeID = getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", objLeadPlatform.Config.DefaultUserID)
        intWorkflowID = getAnyField("UserActiveWorkflowID", "tblusers", "UserID", objLeadPlatform.Config.DefaultUserID)
        strLockUserName = checkCaseLock(objLeadPlatform.Config.DefaultUserID, AppID)
        'unlockCases(objLeadPlatform.Config.DefaultUserID, AppID)
        lockCase(objLeadPlatform.Config.DefaultUserID, AppID)
    End Sub

    Public Function getHotkeyOptions() As String
        Dim boolHotkeyed As Boolean = False
        Dim strOptions As String = ""
        If (checkValue(strSuccessMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><img src=""/net/images/star_gold.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:green;font-weight:bold;"">" & strSuccessMessage & "<br /></span></td>" & vbCrLf & _
            "</tr>"
        End If
        If (checkValue(strFailureMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><img src=""/net/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">" & strFailureMessage & "<br /></span></td>" & vbCrLf & _
            "</tr>"
        End If
        Dim strSQL As String = "SELECT MC.MediaCampaignID, MC.MediaCampaignName, MC.MediaCampaignTelephoneNumberOutbound, MED.MediaName, APP.MediaCampaignIDOutbound, APP.ClientReferenceOutbound " & _
                                "FROM tblmediacampaigns MC " & _
                                "INNER JOIN tblmedia MED ON MED.MediaID = MC.MediaID " & _
                                "LEFT JOIN tblapplications APP ON APP.MediaCampaignIDOutbound = MC.MediaCampaignID " & _
                                "INNER JOIN tblapplicationstatus APS ON APS.AppID = APP.AppID " & _
                                "WHERE (MC.MediaCampaignDeliveryBatch = 0 AND APP.MediaCampaignIDOutbound > 0 AND StatusCode = 'TFR') AND (APP.AppID = '" & AppID & "') AND tblmediacampaigns.CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblmediacampaigns")
        Dim dsHotkeyExisting As DataTable = objDataSet.Tables("tblmediacampaigns")
        If (dsHotkeyExisting.Rows.Count > 0) Then
            boolHotkeyed = True
            Dim x As Integer = 0
            Dim strRowClass As String = "row1"
            For Each Row As DataRow In dsHotkeyExisting.Rows
                If (strRowClass = "row1") Then
                    strRowClass = "row2"
                Else
                    strRowClass = "row1"
                End If
                If (x = 0) Then
                    strOptions += _
                    "<tr class=""tablehead"">" & vbCrLf & _
                    "<td class=""smlc"">Partner</td>" & vbCrLf & _
                    "<td class=""smlc"">Telephone Number</td>" & vbCrLf & _
                    "<td class=""smlc"" width=""200"">Result</td>" & vbCrLf & _
                    "<td class=""smlc"">Reference</td>" & vbCrLf & _
                    "</tr>"
                End If
                strOptions += _
                "<tr class=""" & strRowClass & """>" & vbCrLf & _
                "<td class=""smlc"">" & Row.Item("MediaName") & "<br />" & Row.Item("MediaCampaignName") & "</td>" & vbCrLf & _
                "<td class=""smlc"">" & Row.Item("MediaCampaignTelephoneNumberOutbound") & "</td>" & vbCrLf & _
                "<td class=""smlc""><span style=""color:green"">Actions complete</span></td>" & vbCrLf
                strOptions += "<td class=""smlc""><span style=""color:green; font-weight: bold;"">" & shortenShowTitle(Row.Item("ClientReferenceOutbound").ToString, 10) & "</span></td>" & vbCrLf
                strOptions += "</tr>"
                x = x + 1
            Next
            strOptions += _
                "<tr class=""" & strRowClass & """>" & vbCrLf & _
                "<td colspan=""4"" class=""smlc"">" & _
                "<a href=""/net/inc/processingsave.aspx?strReturnUrl=" & encodeURL("/net/webservices/outbound/hotkey.aspx?AppID=" & AppID & "&frmView=" & strView) & "&frmAppID=" & AppID & "&strThisPg=hotkey&strFrmAction=cancel"" onClick=""if (confirm('Are you sure you want to cancel this transfer?')){return true;} else {return false;}""><img src=""/net/images/icons/hotkey_cancel.gif"" width=""16"" height=""16"" title=""Cancel Transfer"" border=""0"" /></a> Cancel Transfer"
            If (objLeadPlatform.Config.Reports Or objLeadPlatform.Config.SuperAdmin) Then
                strOptions += "&nbsp;<a href=""/net/inc/processingsave.aspx?strReturnUrl=" & encodeURL("/net/webservices/outbound/hotkey.aspx?AppID=" & AppID & "&frmView=" & strView) & "&frmAppID=" & AppID & "&strThisPg=hotkey&strFrmAction=return"" onClick=""if (confirm('Are you sure you want to return this transfer?')){return true;} else {return false;}""><img src=""/net/images/icons/hotkey_return.gif"" width=""16"" height=""16"" title=""Return Transfer"" border=""0"" /></a> Return Transfer"
            End If
            strOptions += _
                "</td>" & vbCrLf & _
                "</tr>"
        Else
            boolHotkeyed = False
        End If
        dsHotkeyExisting.Clear()
        dsHotkeyExisting = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        If (Not boolHotkeyed) Then
            If ((Not Common.checkValue(strTelephoneNumber) And intWorkflowID > 0) Or checkValue(strLockUserName)) Then
                strOptions += _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4"" class=""smlc"">Hotkey unavailable.</td>" & vbCrLf & _
                    "</tr>"
            Else
                If (intDialerGrade <> "7" And intDialerGrade <> "8" And intDialerGrade <> "9") Then
                    strSQL = "SELECT MediaCampaignID, MediaID, MediaCampaignName, MediaCampaignIDOutbound, MediaCampaignCampaignReference, MediaCampaignClientReferenceRequired, ClientReferenceOutbound, MediaCampaignTelephoneNumberOutbound, MediaCampaignScheduleID, MediaName, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress, MediaCampaignDeliveryToUser, MediaCampaignMaxCostInbound, MediaCampaignCostInbound " & _
                                "FROM vwdistributionsearch " & _
                                "WHERE (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') AND CompanyID = '" & CompanyID & "' ORDER BY MediaCampaignScheduleRequiredRate DESC"
                    objDatabase = New DatabaseManager
                    objDataSet = objDatabase.executeReader(strSQL, "vwdistributionsearch")
                    Dim dsHotkey As DataTable = objDataSet.Tables("vwdistributionsearch")
                    If (dsHotkey.Rows.Count > 0) Then
                        strOptions += _
                            "<tr>" & vbCrLf & _
                            "<td colspan=""4""><h2>Please select a company to transfer the call to:</h2></td>" & vbCrLf & _
                            "</tr>"
                        Dim y As Integer = 0
                        Dim strRowClass As String = "row1"
                        For Each Row As DataRow In dsHotkey.Rows
                            If (strRowClass = "row1") Then
                                strRowClass = "row2"
                            Else
                                strRowClass = "row1"
                            End If
                            If (y = 0) Then
                                strOptions += _
                                "<tr class=""tablehead"">" & vbCrLf & _
                                "<td class=""smlc"">Partner</td>" & vbCrLf & _
                                "<td class=""smlc"">Telephone Number</td>" & vbCrLf & _
                                "<td class=""smlc"">Action</td>" & vbCrLf & _
                                "<td class=""smlc"">Reference</td>" & vbCrLf & _
                                "</tr>"
                            End If
                            strOptions += _
                            "<tr class=""" & strRowClass & """>" & vbCrLf & _
                            "<td class=""smlc"">" & Row.Item("MediaName") & "<br />" & Row.Item("MediaCampaignName")
                            If (checkValue(Row.Item("MediaCampaignMaxCostInbound").ToString)) Then
                                If (Row.Item("MediaCampaignCostInbound") > Row.Item("MediaCampaignMaxCostInbound")) Then
                                    strOptions += "<div class=""alert""><img src=""/net/images/script_reminder.gif"" width=""16"" height=""16"" class=""floatLeft"" /><span class=""red"">&nbsp;Overflow only</span></div>"
                                End If
                            End If
                            strOptions += _
                            "</td>" & vbCrLf & _
                            "<td class=""smlc"">" & Row.Item("MediaCampaignTelephoneNumberOutbound") & "</td>" & vbCrLf & _
                            "<td class=""smlc""><span id=""MessageContainer" & y & """>"
                            If (Row.Item("MediaCampaignClientReferenceRequired")) Then
                                strOptions += "<input id=""txtClientReferenceOutbound" & y & """ name=""txtClientReferenceOutbound" & y & """ type=""text"" value=""Ref No."" class=""smaller"" onfocus=""checkDisplay('Ref No.',this)"" onblur=""populateDisplay('Ref No.',this)"" />" & _
                                    "<br /><span style=""float:right;margin-top:5px;""><img id=""imgClientReferenceOutbound" & y & """ src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please input a Ref No."" class=""hidden"" /></span>"
                            End If
                            Select Case Row.Item("MediaCampaignDeliveryTypeID")
                                Case DeliveryType.XMLTransfer
                                    If (Row.Item("MediaCampaignDeliveryToUser") = "1") Then
                                        strOptions += _
                                        "<select id=""ddTransferUserID" & y & """ class=""small"">" & hotkeyUserList(Cache, "--User--", Row.Item("MediaID"), "") & "</select>"
                                    End If
                                    strOptions += _
                                    "<br />Transfer Customer&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','" & Row.Item("MediaCampaignDeliveryXMLPath") & "','','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & Row.Item("MediaCampaignID") & "','" & Row.Item("MediaCampaignCampaignReference") & "','','" & strView & "')""><img src=""/net/images/hotkey.png"" alt=""Transfer Customer"" border=""0"" /></a>" & _
                                    "<span style=""float:right;margin-top:5px;""><img id=""imgTransferUserID" & y & """ src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please select a User to Hotkey to"" class=""hidden"" /></span>"
                                Case DeliveryType.XMLLenderScoring
                                    strOptions += _
                                    "Awaiting Score&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','" & Row.Item("MediaCampaignDeliveryXMLPath") & "','','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & Row.Item("MediaCampaignID") & "','" & Row.Item("MediaCampaignCampaignReference") & "','','" & strView & "')""><img src=""/net/images/hotkey.png"" title=""Awaiting Score"" border=""0"" /></a>"
                                Case DeliveryType.Email, DeliveryType.EmailCSV, DeliveryType.EmailTextOnly
                                    If (Row.Item("MediaCampaignDeliveryToUser") = "1") Then
                                        strOptions += _
                                        "<select id=""ddTransferUserID" & y & """ class=""small"">" & hotkeyUserList(Cache, "--User--", Row.Item("MediaID"), "") & "</select>"
                                    End If
                                    strOptions += _
                                    "<br />Transfer Customer&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','" & Row.Item("MediaCampaignDeliveryEmailAddress") & "','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & Row.Item("MediaCampaignID") & "','','','" & strView & "')""><img src=""/net/images/hotkey.png"" title=""Transfer Customer"" border=""0"" /></a>" & _
                                    "<span style=""float:right;margin-top:5px;""><img id=""imgTransferUserID" & y & """ src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please select a User to Hotkey to"" class=""hidden"" /></span>"
                                Case DeliveryType.VoiceOnly
                                    strOptions += _
                                    "Transfer Customer&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & Row.Item("MediaCampaignID") & "','','','" & strView & "')""><img src=""/net/images/hotkey.png"" title=""Transfer Customer"" border=""0"" /></a>"
                                Case DeliveryType.IssueDocuments
                                    strOptions += _
                                    "Issue Documents&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & Row.Item("MediaCampaignID") & "','','','" & strView & "')""><img src=""/net/images/hotkey.png"" title=""Issue Documents"" border=""0"" /></a>"
                            End Select
                            strOptions += _
                            "</span></td>" & vbCrLf & _
                            "<td class=""smlc""><span style=""color:green""><strong>" & Row.Item("ClientReferenceOutbound") & "</strong></span></td>" & vbCrLf & _
                            "</tr>"
                            y = y + 1
                        Next
                        strOptions += "<input type=""hidden"" id=""NumOptions"" value=""" & y & """ />"
                    Else
                        strOptions += _
                        "<tr>" & vbCrLf & _
                        "<td colspan=""4"" class=""smlc"">No hotkey options found.</td>" & vbCrLf & _
                        "</tr>"
                    End If
                    dsHotkey.Clear()
                    dsHotkey = Nothing
                    objDataSet = Nothing
                    objDatabase = Nothing
                Else
                    If (Not checkSurveyDate() And Not checkTurnDown()) Then
                        strOptions += _
                        "<tr>" & vbCrLf & _
                        "<td colspan=""3""><h2>Customer survey complete, please confirm.</h2></td>" & vbCrLf & _
                        "<td class=""smlc""><span id=""MessageContainer0"">Survey Complete&nbsp;<a href=""#"" onclick=""changeStatus('" & AppID & "','Survey','SVY','','" & objLeadPlatform.Config.DefaultUserID & "','" & strView & "')""><img src=""/net/images/hotkey.png"" title=""Survey Complete"" border=""0"" /></a></td>" & vbCrLf & _
                        "</tr>"
                    Else
                        strOptions += _
                        "<tr>" & vbCrLf & _
                        "<td colspan=""4"" class=""smlc"">Survey already complete.</td>" & vbCrLf & _
                        "</tr>"
                    End If
                End If
                If (Not checkTurnDown() And objLeadPlatform.Config.Reports) Then
                    strOptions += _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4""><hr /><h2>Or select turn down type:</h2></td>" & vbCrLf & _
                    "</tr>" & vbCrLf & _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4"" class=""smlc"">" & vbCrLf & _
                    "<select id=""ddStatusCode"" class=""medium"" onchange=""toggleSubStatusDropdown(this.value);"">" & vbCrLf & _
                    "<option value="""" selected=""selected"">--Select Reason--</option>" & vbCrLf & _
                    "<option value=""WTD"">We Turned Down</option>" & vbCrLf & _
                    "<option value=""TUD"">Turned Us Down</option>" & vbCrLf & _
                    "<option value=""INV"">Invalid Application</option>" & vbCrLf & _
                    "</select>&nbsp;" & vbCrLf & _
                    "<select id=""ddWTDSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(Cache, "--WTD Reason--", "5", "") & "</select>" & vbCrLf & _
                    "<select id=""ddTUDSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(Cache, "--TUD Reason--", "6", "") & "</select>" & vbCrLf & _
                    "<select id=""ddINVSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(Cache, "--INV Reason--", "3", "") & "</select>" & vbCrLf & _
                    "&nbsp;<a id=""lnkTurnDownReason"" href=""#"" onclick=""turnDown('" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & strView & "')"" class=""hidden""><img src=""/net/images/turndown.gif"" width=""16"" height=""16"" title=""Turn Down application"" border=""0"" /></a>" & _
                    "&nbsp;<img id=""imgTurnDownReason"" src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please select a reason"" class=""hidden"" />" & vbCrLf & _
                    "</td>" & vbCrLf & _
                    "</tr>" & vbCrLf & _
                    "<tr>" & vbCrLf
                    If (intDialerLoadType = "2") Then
                        strOptions += "<td colspan=""4""><hr /><h2>Arrange Call Back:</h2></td>" & vbCrLf & _
                        "</tr>" & vbCrLf
                        If (checkCallBack()) Then
                            strOptions += _
                            "<tr>" & vbCrLf & _
                            "<td colspan=""4"" class=""smlc"">Call Back arranged: <strong>" & getAnyField("CallBackDate", "tblapplicationstatus", "AppID", AppID) & "</strong></td>" & vbCrLf & _
                            "</tr>" & vbCrLf
                        End If
                        strOptions += _
                        "<tr>" & vbCrLf & _
                        "<td colspan=""4"" class=""smlc"">" & vbCrLf & _
                        "Date:&nbsp;<input type=""text"" name=""txtCallBackDate"" id=""txtCallBackDate"" class=""small"" value=""" + Config.DefaultDate + """ /><span style=""position: absolute; margin: 5px 0px 0px -20px;""><img id=""chkCallBackDate"" src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please specify a valid date"" class=""hidden"" /></span>" & _
                        "&nbsp;Time:&nbsp;<input type=""text"" name=""txtCallBackTime"" id=""txtCallBackTime"" class=""small"" value=""" + Left(Config.DefaultTime, 5) + """ /><span style=""position: absolute; margin: 5px 0px 0px -20px;""><img id=""chkCallBackTime"" src=""/net/images/invalid.png"" width=""12"" height=""12"" title=""Please specify a valid time"" class=""hidden"" /></span>" & _
                        "&nbsp;<a href=""#"" onclick=""arrangeCallBack('" & AppID & "','" & objLeadPlatform.Config.DefaultUserID & "','" & strView & "');""><img src=""/net/images/callback.gif"" width=""16"" height=""16"" title=""Arrange Call Back"" border=""0"" /></a>" & _
                        "</td>" & vbCrLf & _
                        "</tr>"
                    End If
                Else
                    strOptions += _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4""><hr /><h2>Or select a Turn Down reason:</h2></td>" & vbCrLf & _
                    "</tr>" & vbCrLf & _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4"" class=""smlc"">Turn Down unavailable.</td>" & vbCrLf & _
                    "</tr>" & vbCrLf
                End If
            End If
        End If
        Return strOptions
    End Function

    Private Function checkTurnDown() As Boolean
        Dim boolTurnedDown As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID " & _
                                "WHERE tblapplicationstatus.AppID = '" & AppID & "' AND tblapplicationstatus.CompanyID = '" & CompanyID & "' AND (tblapplicationstatusdates.ApplicationStatusDateName = 'WTD' OR tblapplicationstatusdates.ApplicationStatusDateName = 'TUD' OR tblapplicationstatusdates.ApplicationStatusDateName = 'INV')"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolTurnedDown = False
            Next
        Else
            boolTurnedDown = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolTurnedDown
    End Function

    Private Function checkCallBack() As Boolean
        Dim boolCallBack As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus " & _
                                "WHERE AppID = '" & AppID & "' AND (CallBackDate IS NULL) AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolCallBack = False
            Next
        Else
            boolCallBack = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolCallBack
    End Function

    Private Function checkSurveyDate() As Boolean
        Dim boolSurveyComplete As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID " & _
                                "WHERE tblapplicationstatus.AppID = '" & AppID & "' AND tblapplicationstatus.CompanyID = '" & CompanyID & "' AND (tblapplicationstatusdates.ApplicationStatusDateName = 'Survey')"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolSurveyComplete = False
            Next
        Else
            boolSurveyComplete = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolSurveyComplete
    End Function

    Private Function hotkeyUserList(ByVal cache As Cache, ByVal txt As String, ByVal MediaID As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT MediaUserID, MediaUserName FROM tblmediausers WHERE MediaCompanyID = '" & MediaID & "' AND MediaUserActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY MediaUserName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("MediaUserID") & """")
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaUserID")) Then
                        objStringBuilder.Append(" selected=""selected""")
                    End If
                End If
                objStringBuilder.Append(">" & Row.Item("MediaUserName") & "</option>")
            Next
        End If
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

    Private Function subStatusList(ByVal cache As Cache, ByVal txt As String, ByVal StatusID As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT SST.SubStatusCode, SST.SubStatusDescription FROM tblsubstatuses SST " & _
                            "INNER JOIN tblstatusmapping MAP ON MAP.SubStatusID = SST.SubStatusID " & _
                            "WHERE SST.CompanyID = '" & CompanyID & "' AND MAP.StatusID = '" & StatusID & "' AND SubStatusActive = 1 AND " & _
                            "EXISTS (SELECT SubStatusID FROM tblsubstatusgroupmapping GRM WHERE GRM.SubStatusID = SST.SubStatusID AND GRM.SubStatusGroupID = '" & intSubStatusGroupID & "' AND CRM.CompanyID = '" & CompanyID & "') " & _
                            "ORDER BY SubStatusDescription"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("SubStatusCode") & """")
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        objStringBuilder.Append(" selected=""selected""")
                    End If
                End If
                objStringBuilder.Append(">" & Row.Item("SubStatusDescription") & "</option>")
            Next
        End If
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

End Class
