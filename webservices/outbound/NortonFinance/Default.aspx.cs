using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.ServiceModel;
using NortonFinance;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];
	private string strUserID = "";
	private string strUserPassword = "";
	private string strErrors = "";

	private string strWriteRequest = HttpContext.Current.Request["Request"];
	private string strWriteResponse = HttpContext.Current.Request["Response"];
	private string strCRMInstance = HttpContext.Current.Request["CRMInstance"];

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	public void httpPost()
	{
		ServicePointManager.Expect100Continue = true;
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

		if (strMediaCampaignReference == "F00202" || strMediaCampaignReference == "F00204" || strMediaCampaignReference == "F00205" || strMediaCampaignReference == "F00206")
		{

			strUserID = "engagedsol2";
			strUserPassword = "lynch02";

		}
		else
		{
			strUserID = "engagedsol";
			strUserPassword = "lynch01";
		}

		ServiceModel model = new ServiceModel();
		SqlConnection connection = new SqlConnection();

		if (strCRMInstance == "ES")
		{
			connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringES"].ToString());
		}
		else if (strCRMInstance == "Sparky")
		{
			connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringSparky"].ToString());
		}
		else if (strCRMInstance == "CoverGroup")
		{
			connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringCoverGroup"].ToString());
		}
		else
		{
			HttpContext.Current.Response.Write("0| No Database Connection");
			HttpContext.Current.Response.End();
		}

		try
		{
			connection.Open();
		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write("0| " + ex.Message + " ");
			HttpContext.Current.Response.End();
		}



		try
		{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwxmlnortonnew where AppId = {0}", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{

				decimal decAmount = 0,
						decMortgageBalance = 0;
				decimal.TryParse(reader["Amount"].ToString(), out decAmount);
				decimal.TryParse(reader["MortgageBalance"].ToString(), out decMortgageBalance);

				int intProductPurpose = 0,
					intAddressLivingArrangement = 0;

				int.TryParse(reader["ProductPurpose"].ToString(), out intProductPurpose);

				int.TryParse(reader["AddressLivingArrangement"].ToString(), out intAddressLivingArrangement);

				model = new ServiceModel()
				{
					AppID = reader["AppID"].ToString(),
					Amount = decAmount,
					ProductPurpose = intProductPurpose,
					App1HomeNumberStd = reader["App1HomeNumberStd"].ToString(),
					App1HomeNumberLocal = reader["App1HomeNumberLocal"].ToString(),
					App1MobileNumberStd = reader["App1MobileNumberStd"].ToString(),
					App1MobileNumberLocal = reader["App1MobileNumberLocal"].ToString(),
					App1EmailAddress = reader["App1EmailAddress"].ToString(),
					App1Title = reader["App1Title"].ToString(),
					App1FirstName = reader["App1FirstName"].ToString(),
					App1Surname = reader["App1Surname"].ToString(),
					AddressLine1 = reader["AddressLine1"].ToString(),
					AddressLine2 = reader["AddressLine2"].ToString(),
					AddressCounty = reader["AddressCounty"].ToString(),
					AddressPostCode = reader["AddressPostCode"].ToString(),
					AddressNameNumber = reader["AddressNameNumber"].ToString(),
					AddressLivingArrangement = intAddressLivingArrangement,
					App1SelfEmployed = reader["App1SelfEmployed"].ToString(),
					MortgageBalance = decMortgageBalance



				};

			}

			
			if (model.Amount <= 0)
			{
				strErrors += "Loan Amount Required, ";
			}

			if (model.ProductPurpose <= 0)
			{
				strErrors += "Product Purpose Required, ";
			}

			if (model.App1HomeNumberLocal == "")
			{
				model.App1HomeNumberStd = model.App1MobileNumberStd;
				model.App1HomeNumberLocal = model.App1MobileNumberLocal;
			}

		

			if (model.App1HomeNumberLocal == "" && model.App1MobileNumberLocal == "")
			{
				strErrors += "Home Telephone Required, ";
			}

			if (model.App1Title == "")
			{
				strErrors += "Customer Title Required, ";
			}

			if (model.App1Surname == "")
			{
				strErrors += "Customer Surname Required, ";
			}

			if (model.AddressPostCode == "")
			{
				strErrors += "PostCode Required, ";
			}

			if (model.AddressNameNumber == "")
			{
				strErrors += "House Name Or Number Required, ";
			}

			if (model.AddressLivingArrangement <= 0)
			{
				strErrors += "Address Living Arrangement Required, ";
			}
			if (model.App1MobileNumberLocal != "")
			{
				if (model.App1MobileNumberStd.Substring(0, 2) != "07")
				{
					model.App1MobileNumberStd = "";
					model.App1MobileNumberLocal = "";
				}
			}

			

		}
		catch (Exception e)
		{
			HttpContext.Current.Response.Write("0|" + e.Message);
			HttpContext.Current.Response.End();
		}


		try
		{

			XmlDocument myXml = new XmlDocument();
			XPathNavigator xNav = myXml.CreateNavigator();
			XmlSerializer xn = new XmlSerializer(model.GetType());
			using (var xs = xNav.AppendChild())
			{
				xn.Serialize(xs, model);
			}
			

			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Nortons Request','" + myXml.OuterXml.Replace("'","''") + "',getDate())");
			insertXML.ExecuteNonQuery();
		}
		catch (Exception e)
		{
			HttpContext.Current.Response.Write("0|" + e.Message);
			HttpContext.Current.Response.End();
		}


		if (strWriteRequest == "Y")
		{
			XmlDocument myXml = new XmlDocument();
			XPathNavigator xNav = myXml.CreateNavigator();
			XmlSerializer xn = new XmlSerializer(model.GetType());
			using (var xs = xNav.AppendChild())
			{
				xn.Serialize(xs, model);
			}
			HttpContext.Current.Response.ContentType = "text/xml";
			HttpContext.Current.Response.Write(myXml.OuterXml);
			HttpContext.Current.Response.End();
		}



		if (strErrors == "")
		{

			var LeadDevSoapClient = new LeadDevSoapClient();

			var executeSoapSimple = LeadDevSoapClient.ExecuteSoapSimple(strUserID, strUserPassword, AppID, model.Amount, model.ProductPurpose, model.App1HomeNumberStd, model.App1HomeNumberLocal, model.App1MobileNumberStd, model.App1MobileNumberLocal, model.App1EmailAddress, strMediaCampaignReference, model.App1Title, model.App1FirstName, model.App1Surname, model.AddressLine1, model.AddressLine2, "", model.AddressCounty, model.AddressPostCode, model.AddressNameNumber, model.AddressLivingArrangement, model.App1SelfEmployed, 0, 0, model.MortgageBalance, 0, 0, "N");

			try
			{
				XmlDocument myXmlResponse = new XmlDocument();
				XPathNavigator xNavResponse = myXmlResponse.CreateNavigator();
				XmlSerializer xnResponse = new XmlSerializer(executeSoapSimple.GetType());
				using (var xsResponse = xNavResponse.AppendChild())
				{
					xnResponse.Serialize(xsResponse, executeSoapSimple);
				}
				

				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Nortons Response','" + myXmlResponse.OuterXml.Replace("'","''") + "',getDate())");
				insertXML.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				HttpContext.Current.Response.Write("0|" + e.Message);
				HttpContext.Current.Response.End();
			}

			if (strWriteResponse == "Y")
			{
				XmlDocument myXml = new XmlDocument();
				XPathNavigator xNav = myXml.CreateNavigator();
				XmlSerializer xn = new XmlSerializer(executeSoapSimple.GetType());
				using (var xs = xNav.AppendChild())
				{
					xn.Serialize(xs, executeSoapSimple);
				}
				HttpContext.Current.Response.ContentType = "text/xml";
				HttpContext.Current.Response.Write(myXml.OuterXml);
				HttpContext.Current.Response.End();
			}

			if (executeSoapSimple.status == "1")
			{
				HttpContext.Current.Response.Write("1|" + executeSoapSimple.no + "| Norton Finance");
				HttpContext.Current.Response.End();

			}
			else
			{
				HttpContext.Current.Response.Write("0|" + executeSoapSimple.message + "| Norton Finance");
				HttpContext.Current.Response.End();
			}

		}
		else
		{
			
			HttpContext.Current.Response.Write("0 | " + strErrors);
			HttpContext.Current.Response.End();
		}

		connection.Close();

	}
		

	




class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}

	public class ServiceModel
	{
		public string AppID { get; set; }
		public decimal Amount { get; set; }
		public int? ProductPurpose { get; set; }
		public string App1HomeNumberStd { get; set; }
		public string App1HomeNumberLocal { get; set; }
		public string App1MobileNumberStd { get; set; }
		public string App1MobileNumberLocal { get; set; }
		public string App1EmailAddress { get; set; }
		public string App1Title { get; set; }
		public string App1FirstName { get; set; }
		public string App1Surname { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string AddressCounty { get; set; }
		public string AddressPostCode { get; set; }
		public string AddressNameNumber { get; set; }
		public int AddressLivingArrangement { get; set; }
		public string App1SelfEmployed { get; set; }
		public decimal MortgageBalance { get; set; }
	}

}
