﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.optimumcredit

Partial Class XMLOptimumCredit
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strBrokerID As String = "", strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strRequestType As String = HttpContext.Current.Request("RequestType")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""
    Private bDebug As Boolean = False
	Private bTest As Boolean = False

    Public Sub generateXml()

        Try
            bDebug = (HttpContext.Current.Request("debug") = "Y")
        Catch ex As Exception
        End Try

        Try
            bTest = (HttpContext.Current.Request("test") = "Y")
        Catch ex As Exception
        End Try

        If (strEnvironment = "live") AND (bTest = False) Then
           strXMLURL = "http://broker.optimumcredit.co.uk/EBankingServicesExternal/OriginationService.asmx"
        Else
           'strXMLURL = "http://82.110.64.162:81/EBankingServicesExternal/OriginationService.asmx"
			strXMLURL = "https://uatbroker.optimumcredit.co.uk/EBankingServicesExternal/OriginationService.asmx"
        End If


        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%OptimumCredit%' AND CompanyID = '" & CompanyID & "'"

        If bDebug Then
            'responseWrite(strSQL & "<br>")
        End If

		' get the broker id from the system config as this is generic for each account
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "OptimumCreditBrokerID") Then strBrokerID = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

		If bTest Then
			' TEST =======================================
			' a Positive account
			'strUserName = "gabbie.hunt@positivelending.co.uk"
			'strPassword = "Sebi030810"
			' a SHF account
			'strBrokerID = "mortgage.review@optimumcredit.co.uk"
			'strUserName = "dan.jarvis@sensiblehomefinance.co.uk"
			'strPassword = "Asdfgh12"
			' the Positive test account
			strUserName = "positive.loans@optimumcredit.co.uk"
			strPassword = "Positive1!"
		Else
			strUserName = Common.getAnyFieldFromUserStore("OptimumUsername", Config.DefaultUserID)
			strPassword = Common.getAnyFieldFromUserStore("OptimumPassword", Config.DefaultUserID)
		End If

		' according to Optimum support the broker id is the same as the username, so force it here
		strBrokerID = strUserName

        If checkValue(AppID) Then
            HttpContext.Current.Server.ScriptTimeout = 180
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/optimumcredit/decision.xml"))

        'writeToNode(objInputXMLDoc, "y", "LoginInfo", "ChannelIdentifier", strLenderID)
        'writeToNode(objInputXMLDoc, "y", "SubmitDecision/credentials", "Username", strUserName)
        'writeToNode(objInputXMLDoc, "y", "SubmitDecision/credentials", "Password", strPassword)
        writeToNode(objInputXMLDoc, "n", "SubmissionRoute", "BrokerId", strBrokerID)
        'writeAttribute(objInputXMLDoc, "y", "Request", "applicationRef", AppID)
        writeAttribute(objInputXMLDoc, "y", "Request", "accountCategory", "SecondCharge")

		If bTest Then
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "Title", "Mrs")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "FirstName", "Christine")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "LastName", "Kirk")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "DateOfBirth", "01/03/1964")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "Gender", "Female")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]", "MaritalStatus", "Married")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[1]", "AddressLine1", "127 High Street")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[1]", "Town", "Westbury")
			writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[1]", "Postcode", "BA13 3BN")
		Else
			Dim strSQL As String = "SELECT * FROM vwxmloptimumcredit WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
			Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
			Dim strApp1Title As String = "", strApp1FirstName As String = "", strApp1MiddleNames As String = "", strApp1OtherIncomeItem As String = ""
			Dim strApp1EmployerMonths As String = "", strApp1EmploymentHistoryStatus As String = "", strApp1EmploymentHistoryEmployerName As String = "", strApp1EmploymentHistoryOccupation As String = "", strApp1EmploymentHistoryYears As String = ""
			Dim strAddressHistoryHouseName As String = "", strAddressHistoryHouseNumber As String = "", strAddressHistoryLine1 As String = "", strAddressHistoryLine2 As String = "", strAddressHistoryLine3 As String = "", strAddressHistoryCounty As String = "", strAddressHistoryPostCode As String = "", strAddressHistoryYears As String = "", strAddressHistoryMonths As String = ""
			Dim strAddressMonths As String = "", strPreviousAddressYears As String = "", strPreviousAddressMonths As String = ""
			Dim strApp2FirstName As String = "", strApp2Title As String = "", strApp2MiddleNames As String = "", strApp2OtherIncomeItem As String = ""
			Dim strApp2EmployerMonths As String = "", strApp2EmploymentHistoryStatus As String = "", strApp2EmploymentHistoryEmployerName As String = "", strApp2EmploymentHistoryOccupation As String = "", strApp2EmploymentHistoryYears As String = ""
			If (dsCache.Rows.Count > 0) Then
				For Each Row As DataRow In dsCache.Rows
					For Each Column As DataColumn In dsCache.Columns
						If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
							If (Column.ColumnName.ToString = "App1Title") Then
								strApp1Title = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1FirstName") Then
								strApp1FirstName = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1MiddleNames") Then
								strApp1MiddleNames = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1PreviousName") Then
								createElement(objInputXMLDoc, "Applicant[1]/Aliases", "Alias", "Alias")
								writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[1]", "Title", splitFullName(Row(Column).ToString, "title"))
								writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[1]", "FirstName", splitFullName(Row(Column).ToString, "firstname"))
								writeToNode(objInputXMLDoc, "n", "Applicant[1]/Aliases/Alias[1]", "MiddleName", splitFullName(Row(Column).ToString, "middlenames"))
								writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[1]", "LastName", splitFullName(Row(Column).ToString, "surname"))
							ElseIf (Column.ColumnName.ToString = "App1MaidenName") Then
								Dim objAlias As XmlElement = objInputXMLDoc.SelectSingleNode("//Applicant[1]/Aliases/Alias[1]")
								Dim intNodeNumber As Integer = 1
								If (Not objAlias Is Nothing) Then
									intNodeNumber = 2
								End If
								createElement(objInputXMLDoc, "Applicant[1]/Aliases", "Alias[" & intNodeNumber & "]", "Alias")
								If (strApp1Title = "Mr") Then
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[" & intNodeNumber & "]", "Title", strApp1Title)
								Else
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[" & intNodeNumber & "]", "Title", "Miss")
								End If
								writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[" & intNodeNumber & "]", "FirstName", strApp1FirstName)
								writeToNode(objInputXMLDoc, "n", "Applicant[1]/Aliases/Alias[" & intNodeNumber & "]", "MiddleName", strApp1MiddleNames)
								writeToNode(objInputXMLDoc, "y", "Applicant[1]/Aliases/Alias[" & intNodeNumber & "]", "LastName", Row(Column).ToString)
							ElseIf (Column.ColumnName.ToString = "PreviousAddressLine1") Then
								createElement(objInputXMLDoc, "Applicant[1]/AddressHistory", "Address[2]", "Address")
							ElseIf (Column.ColumnName.ToString = "App1EmploymentStatus") Then
								Select Case Row(Column).ToString
									Case "Employed", "Part Time"
										createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "EmployedPermanent", "EmployedPermanent")
										writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent", "IsPrimaryJob", "true")
										writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent", "IsCurrent", "true")
									Case "Self Employed Audited Accounts", "Self Employed Non Audited"
										createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "SelfEmployedSoleTraderShareholder", "SelfEmployedSoleTraderShareholder")
										writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder", "IsPrimaryJob", "true")
										writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder", "IsCurrent", "true")
								End Select
							ElseIf (Column.ColumnName.ToString = "App1OtherIncomeItem") Then
								strApp1OtherIncomeItem = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1OtherIncomeValue") Then
								Dim arrIncomeItems As Array = Split(strApp1OtherIncomeItem, ",")
								Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")
								For x As Integer = 0 To UBound(arrIncomeItems)
									createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "OtherIncome[" & x + 1 & "]", "OtherIncome")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IsPrimaryJob", "true")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IsCurrent", "true")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "EmploymentStatus", "PartTime")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "OtherIncome", arrIncomeValues(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IncomeNotes", "IncomeSupport")
								Next
							ElseIf (Column.ColumnName.ToString = "App1DependantAges") Then
								Dim arrDepends As Array = Split(Row(Column).ToString, ",")
								For x As Integer = 0 To UBound(arrDepends)
									createElement(objInputXMLDoc, "Applicant[1]/Dependents", "Dependent[" & x + 1 & "]", "Dependent")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/Dependents/Dependent[" & x + 1 & "]", "Age", arrDepends(x))
								Next
							ElseIf (Column.ColumnName.ToString = "AddressMonths") Then
								strAddressMonths = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "PreviousAddressYears") Then
								strPreviousAddressYears = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "PreviousAddressMonths") Then
								strPreviousAddressMonths = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryHouseName") Then
								strAddressHistoryHouseName = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryHouseNumber") Then
								strAddressHistoryHouseNumber = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryLine1") Then
								strAddressHistoryLine1 = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryLine2") Then
								strAddressHistoryLine2 = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryLine3") Then
								strAddressHistoryLine3 = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryCounty") Then
								strAddressHistoryCounty = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryPostCode") Then
								strAddressHistoryPostCode = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryYears") Then
								strAddressHistoryYears = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "AddressHistoryMonths") Then
								Dim arrName As Array = Split(strAddressHistoryHouseName, ",")
								Dim arrNumber As Array = Split(strAddressHistoryHouseNumber, ",")
								Dim arrLine1 As Array = Split(strAddressHistoryLine1, ",")
								Dim arrLine2 As Array = Split(strAddressHistoryLine2, ",")
								Dim arrLine3 As Array = Split(strAddressHistoryLine3, ",")
								Dim arrCounty As Array = Split(strAddressHistoryCounty, ",")
								Dim arrPostCode As Array = Split(strAddressHistoryPostCode, ",")
								Dim arrYears As Array = Split(strAddressHistoryYears, ",")
								Dim arrMonths As Array = Split(Row(Column).ToString, ",")
								Dim intMonths As Integer = CInt(strAddressMonths) + CInt(strPreviousAddressMonths) + CInt(strPreviousAddressYears) * 12
								Dim dteEnd As Date, dteStart As Date
								For x As Integer = 0 To UBound(arrName)
									dteEnd = Config.DefaultDateTime.AddMonths(-intMonths)
									intMonths += CInt(arrMonths(x) + CInt(arrYears(x) * 12))
									dteStart = Config.DefaultDateTime.AddMonths(-intMonths)
									Dim objAddress As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[1]/AddressHistory/Address")
									createElement(objInputXMLDoc, "Applicant[1]/AddressHistory", "Address[" & objAddress.Count + 1 & "]", "Address")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "AddressLine1", arrLine1(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "Country", "UK")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "District", arrLine2(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "HouseNumber", arrNumber(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "Postcode", arrPostCode(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "Town", arrLine3(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "ResidentialStatus", "TenantPrivate")
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "DateFrom", ddmmyyhhmmss2utc(dteStart, False, False))
									writeToNode(objInputXMLDoc, "y", "Applicant[1]/AddressHistory/Address[" & objAddress.Count + 1 & "]", "DateTo", ddmmyyhhmmss2utc(dteEnd, False, False))
								Next
							ElseIf (Column.ColumnName.ToString = "App1EmployerMonths") Then
								strApp1EmployerMonths = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1EmploymentHistoryStatus") Then
								strApp1EmploymentHistoryStatus = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1EmploymentHistoryEmployerName") Then
								strApp1EmploymentHistoryEmployerName = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1EmploymentHistoryOccupation") Then
								strApp1EmploymentHistoryOccupation = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1EmploymentHistoryYears") Then
								strApp1EmploymentHistoryYears = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App1EmploymentHistoryMonths") Then
								Dim arrStatus As Array = Split(strApp1EmploymentHistoryStatus, ",")
								Dim arrNames As Array = Split(strApp1EmploymentHistoryEmployerName, ",")
								Dim arrOccupations As Array = Split(strApp1EmploymentHistoryOccupation, ",")
								Dim arrYears As Array = Split(strApp1EmploymentHistoryYears, ",")
								Dim arrMonths As Array = Split(Row(Column).ToString, ",")
								Dim intMonths As Integer = CInt(strApp1EmployerMonths)
								Dim dteEnd As Date, dteStart As Date
								For x As Integer = 0 To UBound(arrStatus)
									dteEnd = Config.DefaultDateTime.AddMonths(-intMonths)
									intMonths += CInt(arrMonths(x) + CInt(arrYears(x) * 12))
									dteStart = Config.DefaultDateTime.AddMonths(-intMonths)
									Select Case arrStatus(x)
										Case "Employed", "Part Time"
											Dim objEmployed As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[1]/EmploymentHistory/EmployedPermanent")
											createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "EmployedPermanent[" & objEmployed.Count + 1 & "]", "EmployedPermanent")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsPrimaryJob", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsCurrent", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "StartDate", ddmmyyhhmmss2utc(dteStart, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "EndDate", ddmmyyhhmmss2utc(dteEnd, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "Occupation", arrOccupations(x))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "CompanyName", arrNames(x))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "BasicSalary", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "ShiftAllowance", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "Overtime", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "GuaranteedOBC", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "RegularOBC", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "BonusCommission", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsContinuousEmployment", "true")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "OnProbation", "false")
										Case "Self Employed Audited Accounts", "Self Employed Non Audited"
											Dim objSelfEmployed As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder")
											createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "SelfEmployedSoleTraderShareholder")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "IsPrimaryJob", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "IsCurrent", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "StartDate", ddmmyyhhmmss2utc(dteStart, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "EndDate", ddmmyyhhmmss2utc(dteEnd, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "PercentageShareholding", 100)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear1", dteEnd.Year & "-" & padZeros(dteEnd.Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear1", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear2", DateAdd(DateInterval.Year, -1, dteEnd).Year & "-" & padZeros(DateAdd(DateInterval.Year, -1, dteEnd).Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear2", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear3", DateAdd(DateInterval.Year, -2, dteEnd).Year & "-" & padZeros(DateAdd(DateInterval.Year, -2, dteEnd).Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear3", 0)
									End Select
								Next
								'Dim objSelfEmployed As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder")
								'For x As Integer = 0 To UBound(arrStatus)
								'    Select Case arrStatus(x)
								'        Case "Self Employed Audited Accounts", "Self Employed Non Audited"
								'            createElement(objInputXMLDoc, "Applicant[1]/EmploymentHistory", "SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "SelfEmployedSoleTraderShareholder")
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "IsPrimaryJob", "false")
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "IsCurrent", "false")
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "StartDate", "")
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "Occupation", arrOccupations(x))
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "CompanyName", arrNames(x))
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "BasicSalary", 0)
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "IsContinuousEmployment", "true")
								'            writeToNode(objInputXMLDoc, "y", "Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + x + 1 & "]", "OnProbation", "false")
								'    End Select
								'Next
							ElseIf (Column.ColumnName.ToString = "App2FirstName") Then
								strApp2FirstName = Row(Column).ToString
								Dim objApplicant1 As XmlElement = objInputXMLDoc.SelectSingleNode("//Applicant[1]")
								Dim objApplicant2 As XmlNode = objApplicant1.CloneNode(True)
								objInputXMLDoc.SelectSingleNode("//Applicants").InsertAfter(objApplicant2, objApplicant1)
								Dim objMiddleName As XmlElement = objInputXMLDoc.SelectSingleNode("//Applicant[2]/MiddleName")
								objMiddleName.InnerText = ""
								Dim objAliases As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[2]/Alias")
								For Each objAlias As XmlNode In objAliases
									objApplicant2.RemoveChild(objAlias)
								Next
								objApplicant2 = objInputXMLDoc.SelectSingleNode("//Applicant[2]")
								Dim objHistory As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicant[2]/EmploymentHistory")
								objApplicant2.RemoveChild(objHistory)
								Dim objMortgage As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicant[2]/Commitments/CurrentMortgageCommitment")
								objApplicant2.SelectSingleNode("Commitments").RemoveChild(objMortgage)
								createElement(objInputXMLDoc, "Applicant[2]", "EmploymentHistory", "EmploymentHistory")
							ElseIf (Column.ColumnName.ToString = "App2Title") Then
								strApp2Title = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2MiddleNames") Then
								strApp2MiddleNames = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2PreviousName") Then
								createElement(objInputXMLDoc, "Applicant[2]/Aliases", "Alias", "Alias")
								writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[1]", "Title", splitFullName(Row(Column).ToString, "title"))
								writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[1]", "FirstName", splitFullName(Row(Column).ToString, "firstname"))
								writeToNode(objInputXMLDoc, "n", "Applicant[2]/Aliases/Alias[1]", "MiddleName", splitFullName(Row(Column).ToString, "middlenames"))
								writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[1]", "LastName", splitFullName(Row(Column).ToString, "surname"))
							ElseIf (Column.ColumnName.ToString = "App2MaidenName") Then
								Dim objAlias As XmlElement = objInputXMLDoc.SelectSingleNode("//Applicant[2]/Aliases/Alias[1]")
								Dim intNodeNumber As Integer = 1
								If (Not objAlias Is Nothing) Then
									intNodeNumber = 2
								End If
								createElement(objInputXMLDoc, "Applicant[2]/Aliases", "Alias[" & intNodeNumber & "]", "Alias")
								If (strApp2Title = "Mr") Then
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[" & intNodeNumber & "]", "Title", strApp2Title)
								Else
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[" & intNodeNumber & "]", "Title", "Miss")
								End If
								writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[" & intNodeNumber & "]", "FirstName", strApp2FirstName)
								writeToNode(objInputXMLDoc, "n", "Applicant[2]/Aliases/Alias[" & intNodeNumber & "]", "MiddleName", strApp2MiddleNames)
								writeToNode(objInputXMLDoc, "y", "Applicant[2]/Aliases/Alias[" & intNodeNumber & "]", "LastName", Row(Column).ToString)
							ElseIf (Column.ColumnName.ToString = "App2EmploymentStatus") Then
								Select Case Row(Column).ToString
									Case "Employed", "Part Time"
										createElement(objInputXMLDoc, "Applicant[2]/EmploymentHistory", "EmployedPermanent", "EmployedPermanent")
										writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent", "IsPrimaryJob", "true")
										writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent", "IsCurrent", "true")
									Case "Self Employed Audited Accounts", "Self Employed Non Audited"
										createElement(objInputXMLDoc, "Applicant[2]/EmploymentHistory", "SelfEmployedSoleTraderShareholder", "SelfEmployedSoleTraderShareholder")
										writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder", "IsPrimaryJob", "true")
										writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder", "IsCurrent", "true")
								End Select
							ElseIf (Column.ColumnName.ToString = "App2OtherIncomeItem") Then
								strApp2OtherIncomeItem = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2OtherIncomeValue") Then
								Dim arrIncomeItems As Array = Split(strApp2OtherIncomeItem, ",")
								Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")
								For x As Integer = 0 To UBound(arrIncomeItems)
									createElement(objInputXMLDoc, "Applicant[2]/EmploymentHistory", "OtherIncome[" & x + 1 & "]", "OtherIncome")
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IsPrimaryJob", "true")
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IsCurrent", "true")
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "EmploymentStatus", "PartTime")
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "OtherIncome", arrIncomeValues(x))
									writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/OtherIncome[" & x + 1 & "]", "IncomeNotes", "IncomeSupport")
								Next
							ElseIf (Column.ColumnName.ToString = "App2EmployerMonths") Then
								strApp2EmployerMonths = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2EmploymentHistoryStatus") Then
								strApp2EmploymentHistoryStatus = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2EmploymentHistoryEmployerName") Then
								strApp2EmploymentHistoryEmployerName = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2EmploymentHistoryOccupation") Then
								strApp2EmploymentHistoryOccupation = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2EmploymentHistoryYears") Then
								strApp2EmploymentHistoryYears = Row(Column).ToString
							ElseIf (Column.ColumnName.ToString = "App2EmploymentHistoryMonths") Then
								Dim arrStatus As Array = Split(strApp2EmploymentHistoryStatus, ",")
								Dim arrNames As Array = Split(strApp2EmploymentHistoryEmployerName, ",")
								Dim arrOccupations As Array = Split(strApp2EmploymentHistoryOccupation, ",")
								Dim arrYears As Array = Split(strApp2EmploymentHistoryYears, ",")
								Dim arrMonths As Array = Split(Row(Column).ToString, ",")
								Dim intMonths As Integer = CInt(strApp2EmployerMonths)
								Dim dteEnd As Date, dteStart As Date
								For x As Integer = 0 To UBound(arrStatus)
									dteEnd = Config.DefaultDateTime.AddMonths(-intMonths)
									intMonths += CInt(arrMonths(x) + CInt(arrYears(x) * 12))
									dteStart = Config.DefaultDateTime.AddMonths(-intMonths)
									Select Case arrStatus(x)
										Case "Employed", "Part Time"
											Dim objEmployed As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[2]/EmploymentHistory/EmployedPermanent")
											createElement(objInputXMLDoc, "Applicant[2]/EmploymentHistory", "EmployedPermanent[" & objEmployed.Count + 1 & "]", "EmployedPermanent")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsPrimaryJob", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsCurrent", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "StartDate", ddmmyyhhmmss2utc(dteStart, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "EndDate", ddmmyyhhmmss2utc(dteEnd, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "Occupation", arrOccupations(x))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "CompanyName", arrNames(x))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "BasicSalary", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "ShiftAllowance", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "Overtime", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "GuaranteedOBC", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "RegularOBC", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "BonusCommission", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "IsContinuousEmployment", "true")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/EmployedPermanent[" & objEmployed.Count + 1 & "]", "OnProbation", "false")
										Case "Self Employed Audited Accounts", "Self Employed Non Audited"
											Dim objSelfEmployed As XmlNodeList = objInputXMLDoc.SelectNodes("//Applicant[1]/EmploymentHistory/SelfEmployedSoleTraderShareholder")
											createElement(objInputXMLDoc, "Applicant[2]/EmploymentHistory", "SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "SelfEmployedSoleTraderShareholder")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "IsPrimaryJob", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "IsCurrent", "false")
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "StartDate", ddmmyyhhmmss2utc(dteStart, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "EndDate", ddmmyyhhmmss2utc(dteEnd, False, False))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "PercentageShareholding", 100)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear1", dteEnd.Year & "-" & padZeros(dteEnd.Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear1", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear2", DateAdd(DateInterval.Year, -1, dteEnd).Year & "-" & padZeros(DateAdd(DateInterval.Year, -1, dteEnd).Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear2", 0)
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "YearMonthEndingYear3", DateAdd(DateInterval.Year, -2, dteEnd).Year & "-" & padZeros(DateAdd(DateInterval.Year, -2, dteEnd).Month, 2))
											writeToNode(objInputXMLDoc, "y", "Applicant[2]/EmploymentHistory/SelfEmployedSoleTraderShareholder[" & objSelfEmployed.Count + 1 & "]", "NetProfitYear3", 0)
									End Select
								Next
							Else
								Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
								Dim strParent As String = ""
								If (UBound(arrName) > 0) Then
									For y As Integer = 0 To UBound(arrName) - 1
										If (y = 0) Then
											strParent += arrName(y)
										Else
											strParent += "/" & arrName(y)
										End If
									Next
								Else
									strParent = arrName(0)
								End If

								writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
							End If

						End If
					Next
				Next
			End If
			dsCache = Nothing
		End If

        If (bDebug) Then
            'responseWrite(strErrorMessage)
        End If

        If (Request("showApplicationDebug") = "y") Then
            HttpContext.Current.Response.ContentType = "text/xml"
            HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
            HttpContext.Current.Response.End()
        End If

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("OptimumCredit Unsuccessfully Retrieved", -5, AppID, "<OptimumCredit><Status>0</Status><Reason>Validation error " & strErrorMessage & "</Reason></OptimumCredit>", "")
        Else
            ' Post the SOAP message.	

            If (bDebug) Then
                responseWrite("&lt;Credentials&gt;&lt;Username&gt;" & strUserName & "&lt;/Username&gt;&lt;Password&gt;" & strPassword & "&lt;/Password&gt;&lt;/Credentials&gt;")
                responseEnd()
            End If

            Dim oService As broker.Service1 = New broker.Service1
            Dim strResult As String = oService.SubmitDecision("<Credentials><Username>" & strUserName & "</Username><Password>" & strPassword & "</Password></Credentials>", objInputXMLDoc.InnerXml)

            'Dim objResponse As HttpWebResponse = postOptimumWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            'Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(strResult)
            'objReader.Close()
            'objReader = Nothing

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            'If (objResponse.StatusCode.ToString = "OK") Then

            ' Parse the XML document.
            Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//Response")
            Dim strStatus As String = ""
            Dim objStatus As XmlElement = objApplication.SelectSingleNode("//Result")
            If (Not objStatus Is Nothing) Then
                strStatus = objStatus.InnerText
            End If
            Dim objMessageList As XmlNodeList = objApplication.SelectNodes("//Errors/Error")
            Dim strMessage As String = ""
            For Each node As XmlNode In objMessageList
                strMessage += node.InnerText & "<br />"
            Next
            Dim objNotes As XmlNodeList = objApplication.SelectNodes("//Notes/Note")
            For Each node As XmlNode In objNotes
                strMessage += Replace(node.InnerText, "'", "") & "<br />"
            Next
            Select Case strStatus
                Case "Accept"
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Optimum Credit: " & strMessage))
                    updateDataStoreField(AppID, "UnderwritingLenderName", "Optimum Credit", "", "")
                    updateDataStoreField(AppID, "UnderwritingLenderRef", objApplication.SelectSingleNode("//ApplicationRef").InnerText, "", "")
                    updateDataStoreField(AppID, "UnderwritingInitialMonthlyPayment", formatNumber(objOutputXMLDoc.SelectSingleNode("//InitialMonthlyRepayment").InnerText, 2), "", "")
                    updateDataStoreField(AppID, "UnderwritingInitialInterestRate", objApplication.SelectSingleNode("//InitialInterestRate").InnerText, "", "")
                    updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("//ApplicationRef").InnerText, "NULL")
                    updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "OPTACC", "NULL")
                    saveNote(AppID, intHotkeyUserID, "Application No. " & objApplication.SelectSingleNode("//ApplicationRef").InnerText & " Accepted by Optimum Credit")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Accepted - " & strMessage, objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Accepted - " & strMessage, objOutputXMLDoc.InnerXml)
                Case "Decline"
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Optimum Credit: " & strMessage))
                    updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("//ApplicationRef").InnerText, "NULL")
                    updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "OPTDEC", "NULL")
                    saveNote(AppID, intHotkeyUserID, "Application No. " & objApplication.SelectSingleNode("//ApplicationRef").InnerText & " Declined by Optimum Credit - " & strMessage)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Declined - " & strMessage, objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Declined - " & strMessage, objOutputXMLDoc.InnerXml)
                Case "Refer"
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Referred by Optimum Credit: " & strMessage))
                    updateDataStoreField(AppID, "UnderwritingLenderName", "Optimum Credit", "", "")
                    updateDataStoreField(AppID, "UnderwritingLenderRef", objApplication.SelectSingleNode("//ApplicationRef").InnerText, "", "")
                    'updateDataStoreField(AppID, "UnderwritingInitialMonthlyPayment", formatNumber(objApplication.SelectSingleNode("//InitialMonthlyRepayment").InnerText, 2), "", "")
                    'updateDataStoreField(AppID, "UnderwritingInitialInterestRate", objApplication.SelectSingleNode("//InitialInterestRate").InnerText, "", "")
                    updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("//ApplicationRef").InnerText, "NULL")
                    updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "OPTREF", "NULL")
                    saveNote(AppID, intHotkeyUserID, "Application No. " & objApplication.GetAttribute("ApplicationNumber") & " Referred by Optimum Credit - " & strMessage)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Referred - " & strMessage, objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Optimum Credit Referred - " & strMessage, objOutputXMLDoc.InnerXml)
                Case Else
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Failed by Optimum Credit: " & strMessage))
                    saveNote(AppID, intHotkeyUserID, "Application Failed by Optimum Credit - " & strMessage)
                    updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "AWT", "NULL")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Optimum Credit Failed - " & strMessage, objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Optimum Credit Failed - " & strMessage, objOutputXMLDoc.InnerXml)
            End Select
        End If
        'Else
        'SOAPUnsuccessfulMessage("LendProtect Unsuccessfully Retrieved", -5, AppID, "<LendProtect><Status>0</Status><Reason>Data error</Reason></LendProtect>", "")
        'End If
        'objResponse = Nothing

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Function splitFullName(ByVal strFullName As String, ByVal returnType As String) As String
        Dim boolTitleSet As Boolean = False
        Dim strTitle As String = "", strFirstName As String = "", strMiddleNames As String = "", strSurname As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                strSurname = arrFullName(i)
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                strTitle = arrFullName(i)
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                strFirstName = arrFullName(i)
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                strFirstName = arrFullName(i)
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        Select Case returnType
            Case "title"
                Return strTitle
            Case "firstname"
                Return strFirstName
            Case "middlenames"
                Return strMiddleNames
            Case "surname"
                Return strSurname
            Case Else
                Return strFullName
        End Select
    End Function

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function insertElement(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertAfter(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function insertElementBefore(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertBefore(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = doc.CreateElement(fld)
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function postOptimumWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Headers.Add("SOAPAction", "http://www.dpr.co.uk/eBanking/SubmitDecision")
                .Timeout = 60000
                .ReadWriteTimeout = 60000
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'responseWrite("host: " & url & "<br>")
            'responseWrite(post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
