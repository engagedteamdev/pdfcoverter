﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net

Partial Class PayoutLoans
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strApp1FullName As String = getAnyField("App1FullName", "vwexportapplication", "AppID", AppID)
    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private strXMLURL As String = "", strAccount As String = "", strUserName As String = "", strSecret As String = "", strEmailAddress As String = ""
    Private strErrorMessage As String = "", intMode As String = HttpContext.Current.Request("frmMode")
    Private strGuid As String = System.Guid.NewGuid().ToString

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
        If (strEnvironment = "live") Then
            'strXMLURL = "https://raven.deepcovelabs.com/realtime"
        Else
            strXMLURL = "https://raven.deepcovelabs.com/realtime"
        End If
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Raven%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "RavenUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenAccountCode2") Then strAccount = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenSharedSecret") Then strSecret = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenEmailAddress") Then strEmailAddress = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
    End Sub

    Public Sub payoutLoan()
        If checkValue(AppID) Then
            If (intMode = "1") Then
                Dim strSQL As String = "SELECT AppID, App1FirstName, App1Surname, CONVERT(DECIMAL(18,2),Amount) AS Amount, BankAccountName, BankSortCode, BankAccountNumber FROM vwpaymentinfo WHERE StatusCode = 'MAN' AND (BankBatchID IS NULL OR BankBatchID = '') AND NOT CardTemplateNumber IS NULL AND PaidOut IS NULL AND FasterPaymentBounced IS NULL AND CompanyID ='" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        requestPayment(Row.Item("AppID"), Row.Item("App1FirstName"), Row.Item("App1Surname"), Row.Item("Amount"), Left(Row.Item("BankAccountName").ToString, 18), Row.Item("BankSortCode").ToString, Row.Item("BankAccountNumber").ToString)
                    Next
                End If
                dsCache = Nothing
            ElseIf (intMode = "2") Then
                Dim strSQL As String = "SELECT AppID, Amount, ProductTerm, BankBatchID FROM vwpaymentinfo WHERE StatusCode = 'PRQ' AND PaidOut IS NULL AND NOT BankBatchID IS NULL AND CompanyID ='" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        queryPayment(Row.Item("AppID"), Row.Item("BankBatchID"), Row.Item("Amount"), Row.Item("ProductTerm").ToString)
                    Next
                End If
                dsCache = Nothing
            End If

        End If
    End Sub

    Private Sub requestPayment(ByVal theAppID As String, ByVal firstName As String, ByVal surname As String, ByVal amount As String, ByVal accountName As String, ByVal sortCode As String, ByVal accountNo As String)
        If (checkValue(firstName) And checkValue(surname) And checkValue(amount) And checkValue(accountName) And checkValue(sortCode) And checkValue(accountNo)) Then

            Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
            Dim strPaymentType As String = "fp_credit"
            Dim strCurrency As String = "GBP"
            Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency)
            Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&Currency=" & strCurrency & "&PRN=" & strAccount & "&Amount=" & Replace(amount, ".", "") & "&BankNumber=" & sortCode & "&AccountNumber=" & accountNo & "&AccountName=" & accountName

            'responseWrite(strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency & "<br>")
            'HttpContext.Current.Response.Write(strPost)
            'HttpContext.Current.Response.End()

            If (strErrorMessage <> "") Then
                SOAPUnsuccessfulMessage("Raven Payment Unsuccessfully Generated", 1, theAppID, "<Payment><Status>-5</Status><Message>" & strErrorMessage & "</Message></Payment>", strPost)
            Else
                ' Post the SOAP message.
                Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/submit", strPost)
                'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()

                Dim strStatus As String = "", strMessage As String = "", strTrackingNumber As String = ""
                Dim arrResponse As Array = Split(strResponse, "&")
                For x As Integer = 0 To UBound(arrResponse)
                    Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                    If (arrKeyValues(0) = "Status") Then
                        strStatus = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "Message") Then
                        strMessage = decodeURL(arrKeyValues(1))
                    ElseIf (arrKeyValues(0) = "TrackingNumber") Then
                        strTrackingNumber = arrKeyValues(1)
                    End If
                Next

                Select Case strStatus
                    Case "InProgress"
                        updateDataStoreField(theAppID, "BankBatchID", strTrackingNumber, "", "")
                        saveNote(theAppID, Config.DefaultUserID, "Payment request " & strStatus & " - " & displayCurrency(amount))
                        saveStatus(theAppID, Config.DefaultUserID, "PRQ", "") ' Mark as Paid Out to make it impossible to pay again
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "PaymentRequested")
                        postEmail("", strEmailAddress, "Pacnet Payment #" & strTrackingNumber & " " & strStatus & " - " & strApp1FullName & " - " & AppID, "A payment (#" & strTrackingNumber & ") to " & strApp1FullName & " for " & displayCurrency(amount) & " is " & strStatus, False, "")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Payment Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Payment Successfully Retrieved", 1, theAppID, "<Payment><Status>PENDING</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
                    Case Else
                        Dim paymentID As String = insertPaymentReturnID(AppID, -CDec(amount), "Loan", 3, Config.DefaultDateTime)
                        updatePayment(paymentID, 4)
                        saveNote(theAppID, Config.DefaultUserID, strMessage)
                        postEmail("", strEmailAddress, "Pacnet Payment #" & strTrackingNumber & " " & strStatus & " - " & strApp1FullName & " - " & AppID, "A payment (#" & strTrackingNumber & ") to " & strApp1FullName & " for " & displayCurrency(amount) & " is " & strStatus, False, "")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, -5, "Raven Payment Unsuccessfully Generated", strPost)
                        SOAPUnsuccessfulMessage("Raven Payment Unsuccessfully Retrieved", -5, theAppID, "<Payment><Status>" & strStatus & "</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
                End Select
                objReader = Nothing
                'End If
                objResponse = Nothing
            End If

        End If
    End Sub

    Private Sub queryPayment(ByVal theAppID As String, ByVal batchID As String, ByVal amount As String, ByVal term As String)

        Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
        Dim strPaymentType As String = "fp_credit"
        Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid)
        Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&PRN=" & strAccount & "&TrackingNumber=" & batchID

        'responseWrite(strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency & "<br>")
        'HttpContext.Current.Response.Write(strPost)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("Raven Payment Check Unsuccessfully Generated", 1, theAppID, "<Payment><Status>-5</Status><Message>" & strErrorMessage & "</Message></Payment>", strPost)
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/status", strPost)
            'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()

            'HttpContext.Current.Response.Write(strResponse)
            'HttpContext.Current.Response.End()

            Dim strStatus As String = "", strMessage As String = "", strTrackingNumber As String = ""
            Dim arrResponse As Array = Split(strResponse, "&")
            For x As Integer = 0 To UBound(arrResponse)
                Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                If (arrKeyValues(0) = "Status") Then
                    strStatus = arrKeyValues(1)
                ElseIf (arrKeyValues(0) = "Message") Then
                    strMessage = decodeURL(arrKeyValues(1))
                End If
            Next

            Select Case strStatus
                Case "Submitted", "Cleared"
                    If (checkValue(term)) Then
                        If (term > 0) Then
                            Dim dtePaymentDue As Date = Config.DefaultDateTime.AddDays(term)
                            saveUpdatedDate(AppID, Config.DefaultUserID, "PaymentDue", dtePaymentDue)
                        End If
                    End If
                    Dim paymentID As String = insertPaymentReturnID(AppID, -CDec(amount), "Loan", 3, Config.DefaultDateTime)
                    updatePaymentReference(paymentID, batchID)
                    updatePayment(paymentID, 1)
                    saveNote(theAppID, Config.DefaultUserID, "Payment status: " & strStatus & " " & strMessage)
                    saveStatus(theAppID, Config.DefaultUserID, "POT", "") ' Mark as Paid Out to make it impossible to pay again
                    saveUpdatedDate(theAppID, Config.DefaultUserID, "PaidOut")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Payment Check Successfully Generated", strPost)
                    SOAPSuccessfulMessage("Raven Payment Check Successfully Retrieved", 1, theAppID, "<Payment><Status>" & strStatus & "</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
                Case "Rejected"
                    Dim paymentID As String = insertPaymentReturnID(AppID, -CDec(amount), "Loan", 3, Config.DefaultDateTime)
                    updatePaymentReference(paymentID, batchID)
                    updatePayment(paymentID, 2)
                    saveNote(theAppID, Config.DefaultUserID, "Payment status: " & strStatus & " " & strMessage)
                    saveStatus(theAppID, Config.DefaultUserID, "FPB", "") ' Mark as FP Bounced to make it impossible to pay again
                    saveUpdatedDate(theAppID, Config.DefaultUserID, "FasterPaymentBounced")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Payment Check Successfully Generated", strPost)
                    SOAPSuccessfulMessage("Raven Payment Check Successfully Retrieved", 1, theAppID, "<Payment><Status>" & strStatus & "</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
                Case "Voided"
                    Dim paymentID As String = insertPaymentReturnID(AppID, -CDec(amount), "Loan", 3, Config.DefaultDateTime)
                    updatePaymentReference(paymentID, batchID)
                    updatePayment(paymentID, 4)
                    saveNote(theAppID, Config.DefaultUserID, "Payment status: " & strStatus & " " & strMessage)
                    saveStatus(theAppID, Config.DefaultUserID, "FPV", "") ' Mark as FP Bounced to make it impossible to pay again
                    saveUpdatedDate(theAppID, Config.DefaultUserID, "FasterPaymentVoided")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Payment Check Successfully Generated", strPost)
                    SOAPSuccessfulMessage("Raven Payment Check Successfully Retrieved", 1, theAppID, "<Payment><Status>" & strStatus & "</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
                Case Else
                    saveNote(theAppID, Config.DefaultUserID, "Payment status: " & strStatus & " " & strMessage)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Payment Check Successfully Generated", strPost)
                    SOAPSuccessfulMessage("Raven Payment Check Successfully Retrieved", 1, theAppID, "<Payment><Status>" & strStatus & "</Status><Message>" & strMessage & "</Message></Payment>", strResponse)
            End Select

        objReader = Nothing
        'End If
        objResponse = Nothing
        End If

    End Sub

    Private Function hashString(ByVal key As String, ByVal text As String) As String
        Dim objEncoder As New System.Text.UTF8Encoding
        Dim bKey() As Byte = objEncoder.GetBytes(key)
        Dim bText() As Byte = objEncoder.GetBytes(text)
        Dim objHash As New System.Security.Cryptography.HMACSHA1(bKey, True)
        Dim bHash As Byte() = objHash.ComputeHash(bText)
        Dim hash As String = Replace(BitConverter.ToString(bHash), "-", "")
        Return hash.ToLower
    End Function

       Private Function postRavenWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = True
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .ContentType = "application/x-www-form-urlencoded"
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            'responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        xml = regexReplace("&CardNumber=([0-9]{0,16})", "&CardNumber=****************", xml)
        xml = regexReplace("&CVV2=([0-9]{0,16})", "&CVV2=***", xml)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
