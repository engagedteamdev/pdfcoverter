﻿Imports Config, Common, CommonSave
Imports System.Net
Imports System.Security.Cryptography.SHA1
Imports System.Xml

Partial Class DirectDebit
    Inherits System.Web.UI.Page

    Private AppIDs As String = HttpContext.Current.Request("AppIDs")
    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private strUserName As String = "", strPassword As String = "", strWebFolder As String = "", strRoutingNumber As String = ""
    Private strErrorMessage As String = "", intMode As String = HttpContext.Current.Request("frmMode")
    Private strGuid As String = System.Guid.NewGuid().ToString

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Raven%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "RavenWebFolder") Then strWebFolder = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenWebFolderUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenWebFolderPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenAccountCode") Then strRoutingNumber = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        Select Case intMode
            Case 1
                setupPayments()
            Case 2
                takePayments()
            Case 3
                readPayments()
        End Select

    End Sub

    Public Sub setupPayments()
        Dim strSQL As String = "SELECT AppID, ProductType, BankAccountNumber, BankSortCode, BankAccountName FROM vwpaymentinfo WHERE CompanyID = '" & CompanyID & "' AND AppID IN (" & AppIDs & ")"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim strFile As String = _
                "RavenPaymentFile_v1.0" & vbCrLf & _
                "PaymentRoutingNumber,PymtType,Amount,BankNumber,AccountNumber,AccountName,CurrencyCode,InstructionReference,Description" & vbCrLf
            For Each Row As DataRow In dsCache.Rows
                strFile += strRoutingNumber & ",bacs_setup_debit,0," & Row.Item("BankSortCode").ToString & "," & Row.Item("BankAccountNumber").ToString & "," & Row.Item("BankAccountName").ToString & ",GBP," & Row.Item("AppID") & "," & objLeadPlatform.Config.CompanyName & " " & Row.Item("AppID") & vbCrLf
            Next
            strFile += "RavenFooter," & dsCache.Rows.Count & ",0"
            Dim strTimeStamp As String = Year(Config.DefaultDateTime) & Month(Config.DefaultDateTime) & Day(Config.DefaultDateTime) & Hour(Config.DefaultDateTime) & Minute(Config.DefaultDateTime) & Second(Config.DefaultDateTime) & Config.DefaultDateTime.Millisecond
            Dim objResponse As HttpWebResponse = putRavenWebRequest(strWebFolder & "/payments/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & "-dd-" & strTimeStamp & ".txt", strFile)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            Dim strResponseCode As String = objResponse.StatusCode
            If (strResponseCode = "201") Then
                successfulMessage("Raven Direct Debit Setup Submitted", 1, strGuid, "<DirectDebit><Status>1</Status><Message>201 Created</Message></DirectDebit>", strResponse)
            Else
                unsuccessfulMessage("Raven Direct Debit Setup Not Submitted", 1, strGuid, "<DirectDebit><Status>0</Status><Message>" & encodeURL(strResponse) & "</Message></DirectDebit>", strResponse)
            End If
            objReader = Nothing
            objResponse = Nothing
        End If
        dsCache = Nothing
    End Sub

    Public Sub takePayments()
        Dim strSQL As String = "SELECT AppID, PaymentHistoryID, ProductType, PaymentAmount, BankAccountNumber, BankSortCode, BankAccountName FROM vwpaymentschedule WHERE PaymentHistoryStatus = 0 AND CompanyID = '" & CompanyID & "' AND AppID IN (" & AppIDs & ")"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strFile As String = ""
        Dim intTotalPayment As Decimal = 0.0
        If (dsCache.Rows.Count > 0) Then
            strFile = _
                "RavenPaymentFile_v1.0" & vbCrLf & _
                "PaymentRoutingNumber,PymtType,Amount,BankNumber,AccountNumber,AccountName,CurrencyCode,InstructionReference,Description" & vbCrLf
            For Each Row As DataRow In dsCache.Rows
                strFile += strRoutingNumber & ",bacs_debit," & Replace(Row.Item("PaymentAmount"), ".", "") & "," & Row.Item("BankSortCode").ToString & "," & Row.Item("BankAccountNumber").ToString & "," & Row.Item("BankAccountName").ToString & ",GBP," & Row.Item("AppID") & "-" & Row.Item("PaymentHistoryID") & "," & objLeadPlatform.Config.CompanyName & " " & Row.Item("AppID") & vbCrLf
                intTotalPayment += Row.Item("PaymentAmount")
                saveStatus(Row.Item("AppID"), Config.DefaultUserID, "PPE", "")
            Next
            strFile += "RavenFooter," & dsCache.Rows.Count & "," & Replace(intTotalPayment, ".", "")
        End If
        dsCache = Nothing
        Dim strTimeStamp As String = Year(Config.DefaultDateTime) & Month(Config.DefaultDateTime) & Day(Config.DefaultDateTime) & Hour(Config.DefaultDateTime) & Minute(Config.DefaultDateTime) & Second(Config.DefaultDateTime) & Config.DefaultDateTime.Millisecond
        Dim objResponse As HttpWebResponse = putRavenWebRequest(strWebFolder & "/payments/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & "-bacs-" & strTimeStamp & ".txt", strFile)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        Dim strResponseCode As String = objResponse.StatusCode
        If (strResponseCode = "201") Then
            successfulMessage("Raven Direct Debit Payment Submitted", 1, strGuid, "<DirectDebit><Status>1</Status><Message>201 Created</Message></DirectDebit>", strResponse)
        Else
            unsuccessfulMessage("Raven Direct Debit Payment Not Submitted", 1, strGuid, "<DirectDebit><Status>0</Status><Message>" & encodeURL(strResponse) & "</Message></DirectDebit>", strResponse)
        End If
        objReader = Nothing
        objResponse = Nothing
    End Sub

    Public Sub readPayments()
        Dim objResponse As HttpWebResponse = findRavenWebRequest(strWebFolder & "/reports/")
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse.Close()
        objResponse = Nothing
        If (checkValue(strResponse)) Then
            Dim objXMLDoc As XmlDocument = New XmlDocument
            Dim objNSM As XmlNamespaceManager = New XmlNamespaceManager(objXMLDoc.NameTable)
            objNSM.AddNamespace("D", "DAV:")
            objNSM.AddNamespace("lp1", "DAV:")
            objXMLDoc.LoadXml(strResponse)
            Dim objNodeList As XmlNodeList = objXMLDoc.SelectNodes("(//D:href)[position()>1]", objNSM)
            For Each node As XmlNode In objNodeList
                Dim strFileName As String = Replace(node.InnerText, "/" & strUserName, "")
                If (InStr(strFileName, "RESULT") > 0 Or InStr(strFileName, "REJECTED") > 0) Then
                    Dim objResponse2 As HttpWebResponse = getRavenWebRequest(strWebFolder & strFileName)
                    Dim objReader2 As New StreamReader(objResponse2.GetResponseStream())
                    Dim strFile As String = objReader2.ReadToEnd()

                    If (Not InStr(strFile, "FILE REJECTED")) Then

                        Dim arrData As Array = Split(strFile, "RavenFooter")
                        Dim arrLines As Array = Split(arrData(0), vbCrLf)

                        For x As Integer = 2 To UBound(arrLines)

                            If (checkValue(arrLines(x))) Then

                                Dim arrFields As Array = Split(arrLines(x), ",")

                                Dim strType As String = arrFields(1)
                                Dim arrReference As Array = Split(arrFields(7), "-")
                                Dim intAppID As String = arrReference(0)
                                Dim intPaymentReference As String = arrReference(1)
                                Dim strStatus As String = arrFields(9)

                                Dim arrStatus As Array = Split(strStatus, ":")
                                If (arrStatus.Length = 2) Then
                                    Select Case arrStatus(0)
                                        Case "Invalid", "Rejected", "ConfigError", "UnexpectedResponse", "Invalid", "Return"
                                            saveNote(intAppID, Config.DefaultUserID, arrStatus(0) & " - " & arrStatus(1))
                                            Select Case strType
                                                Case "bacs_debit"
                                                    saveStatus(intAppID, Config.DefaultUserID, "PFA", "")
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "PaymentFailed")
                                                    updatePayment(intPaymentReference, "2")
                                                Case "bacs_setup_debit"
                                                    'saveStatus(intAppID, Config.DefaultUserID, "DDF", "")
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "DirectDebitFailed")
                                            End Select
                                            deleteRavenWebRequest(strWebFolder & strFileName)
                                    End Select
                                Else
                                    saveNote(intAppID, Config.DefaultUserID, arrStatus(0))
                                    Select Case arrStatus(0)
                                        Case "InProgress"

                                        Case "Submitted"
                                            Select Case strType
                                                Case "bacs_debit"
                                                    saveStatus(intAppID, Config.DefaultUserID, "POT", "")
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "PaymentSubmitted")
                                                    ' If logic to handle full repayment

                                                    ' End If
                                                    updatePayment(intPaymentReference, "1")
                                                Case "bacs_setup_debit"
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "DirectDebitSetup")
                                            End Select
                                            deleteRavenWebRequest(strWebFolder & strFileName)
                                        Case "Voided"
                                            Select Case strType
                                                Case "bacs_debit"
                                                    saveStatus(intAppID, Config.DefaultUserID, "PVO", "")
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "PaymentVoided")
                                                    updatePayment(intPaymentReference, "4")
                                                Case "bacs_setup_debit"
                                                    saveUpdatedDate(intAppID, Config.DefaultUserID, "DirectDebitVoided")
                                            End Select
                                            deleteRavenWebRequest(strWebFolder & strFileName)
                                    End Select
                                End If
                            End If
                        Next

                    Else
                        postEmail("", "ben.snaize@engagedcrm.co.uk", "Raven Payment File Rejected", objLeadPlatform.Config.CompanyName & " payment file " & strFileName & " was rejected" & vbCrLf & vbCrLf & strFile, False, "", True)
                    End If

                    responseWrite(vbCrLf & vbCrLf)

                    objReader2.Close()
                    objReader2 = Nothing
                    objResponse2.Close()
                    objResponse2 = Nothing
                End If
            Next
            objXMLDoc = Nothing
        End If
    End Sub

    Private Function putRavenWebRequest(ByVal url As String, ByVal file As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Put
                .KeepAlive = True
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(file)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("file: " & file & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(file)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function getRavenWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Get
                .KeepAlive = True
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            'responseWrite(objRequest.Method & " " & objRequest.RequestUri.AbsolutePath & " HTTP/" & objRequest.ProtocolVersion.ToString & vbCrLf)
            'responseWrite("Host: " & objRequest.RequestUri.Host)
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & vbCrLf)
            'Next
            'responseEnd()
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function deleteRavenWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = "DELETE"
                .KeepAlive = True
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            'responseWrite(objRequest.Method & " " & objRequest.RequestUri.AbsolutePath & " HTTP/" & objRequest.ProtocolVersion.ToString & vbCrLf)
            'responseWrite("Host: " & objRequest.RequestUri.Host)
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & vbCrLf)
            'Next
            'responseEnd()
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function findRavenWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = "PROPFIND"
                .KeepAlive = True
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Credentials = New NetworkCredential(strUserName, strPassword)
                .Headers.Add("Depth", "1")
            End With
            'responseWrite(objRequest.Method & " " & objRequest.RequestUri.AbsolutePath & " HTTP/" & objRequest.ProtocolVersion.ToString & vbCrLf)
            'responseWrite("Host: " & objRequest.RequestUri.Host)
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & vbCrLf)
            'Next
            'responseEnd()
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub successfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<Result>" & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append(resp)
            .Append("</Response>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub unsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<Result>" & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append(resp)
            .Append("</Response>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        xml = regexReplace("&CardNumber=([0-9]{0,16})", "&CardNumber=****************", xml)
        xml = regexReplace("&CVV2=([0-9]{0,16})", "&CVV2=***", xml)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "", "") & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
