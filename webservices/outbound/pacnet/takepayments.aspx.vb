﻿Imports Config, Common, CommonSave
Imports System.Net
Imports System.Security.Cryptography.SHA1

Partial Class TakePayments
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID")
    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private strXMLURL As String = "", strUserName As String = "", strAccount As String = "", strSecret As String = ""
    Private strErrorMessage As String = "", intMode As String = HttpContext.Current.Request("frmMode")
    Private strGuid As String = System.Guid.NewGuid().ToString
    Private strNoStatusChange As String = HttpContext.Current.Request("frmNoStatusChange")
    Private intManualPaymentAmount As String = HttpContext.Current.Request("frmAmount")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
        If (strEnvironment = "live") Then
            'strXMLURL = "https://raven.deepcovelabs.com/realtime"
        Else
            strXMLURL = "https://raven.deepcovelabs.com/realtime"
        End If
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Raven%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "RavenUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenAccountCode") Then strAccount = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "RavenSharedSecret") Then strSecret = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
    End Sub

    Public Sub takePayments()
        If checkValue(AppID) Then
            If (intMode = "1") Then ' Pre-auth
                Dim strSQL As String = "SELECT TOP 1 AppID, BankCardNumber, BankCardCV2, BankCardExpiry, BankCardHolder, AddressLine1, AddressPostCode FROM vwpaymentinfo WHERE CompanyID = '" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        authorisePayment(Row.Item("AppID"), "1", getCardDetails("CardNumber"), getCardDetails("CardCV2"), Row.Item("BankCardExpiry").ToString, Row.Item("BankCardHolder").ToString, Row.Item("AddressLine1").ToString, Row.Item("AddressPostCode").ToString)
                    Next
                End If
                dsCache = Nothing
            ElseIf (intMode = "2") Then ' Due & bounced payments
                Dim strSQL As String = "SELECT AppID, PaymentHistoryID, PaymentAmount, PacnetCardTemplateNumber FROM vwpaymentschedule WHERE NOT PacnetCardTemplateNumber IS NULL AND StatusCode IN ('POT') AND NOT PaidOut IS NULL AND Settled IS NULL AND CardAttempts < 3 AND DATEDIFF(DD, GETDATE(), PaymentHistoryDate) = 0 AND PaymentHistoryMethod = 1 AND PaymentHistoryStatus = 0 AND CompanyID ='" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        requestPayment(Row.Item("AppID"), Row.Item("PaymentAmount"), Row.Item("PacnetCardTemplateNumber").ToString, Row.Item("PaymentHistoryID"))
                    Next
                End If
                dsCache = Nothing
            ElseIf (intMode = "3") Then ' Manual payments
                Dim strSQL As String = "SELECT TOP 1 AppID, UnderwritingTotalRepayment, PacnetCardTemplateNumber FROM vwpaymentinfo WHERE NOT PacnetCardTemplateNumber IS NULL AND StatusCode IN ('POT') AND NOT PaidOut IS NULL AND Settled IS NULL AND PaymentHistoryMethod = 1 AND CompanyID ='" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If (checkValue(intManualPaymentAmount)) Then
                            requestPayment(Row.Item("AppID"), intManualPaymentAmount, Row.Item("PacnetCardTemplateNumber").ToString, "", "MPS")
                        Else
                            requestPayment(Row.Item("AppID"), Row.Item("UnderwritingTotalRepayment"), Row.Item("PacnetCardTemplateNumber").ToString, "", "MPS")
                        End If
                    Next
                End If
                dsCache = Nothing
                'ElseIf (intMode = "4") Then ' Payment Plans
                '    Dim strSQL As String = "SELECT AppID, PaymentPlanMonthlyPayment, CardTemplateNumber FROM vwschedulepayments WHERE NOT CardTemplateNumber IS NULL AND StatusCode IN ('COL','PAY','CPB') AND NOT PaymentPlanMonthlyPayment IS NULL AND NOT PaidOut IS NULL AND PaymentReceived IS NULL AND PaymentPlanMonths < PaymentPlanTerm AND CompanyID ='" & CompanyID & "'"
                '    If (checkValue(AppID)) Then
                '        strSQL += " AND AppID = '" & AppID & "'"
                '    End If
                '    Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                '    If (dsCache.Rows.Count > 0) Then
                '        For Each Row As DataRow In dsCache.Rows
                '            requestPayment(Row.Item("AppID"), Row.Item("PaymentPlanMonthlyPayment"), Row.Item("CardTemplateNumber").ToString)
                '        Next
                '    End If
                '    dsCache = Nothing
            ElseIf (intMode = "5") Then ' Check payment status
                Dim strSQL As String = "SELECT AppID, PaymentHistoryReference, PaymentHistoryID FROM vwpaymentschedule WHERE StatusCode IN ('CPS','MPS') AND NOT PaidOut IS NULL AND Settled IS NULL AND PaymentHistoryMethod = 1 AND PaymentHistoryStatus = 5 AND CompanyID ='" & CompanyID & "'"
                If (checkValue(AppID)) Then
                    strSQL += " AND AppID = '" & AppID & "'"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        'Dim intOverdueDays As Integer = 0
                        'If (checkValue(Row.Item("OverdueDays").ToString)) Then
                        '    intOverdueDays = CInt(Row.Item("OverdueDays"))
                        'End If
                        'If (intOverdueDays > 0 And intOverdueDays <= 60 And Row.Item("StatusCode") = "CPS" And Not checkValue(Row.Item("ExtensionPaymentDue").ToString)) Then
                        '    Dim intPercentage As Integer = getPercentage(intOverdueDays) '((-1 * CDec(Row.Item("CurrentBalance")) / CDec(Row.Item("UnderwritingRepaymentFees")))) * 100
                        '    If (intPercentage > 100) Then intPercentage = 100
                        '    'Dim intPayment As Decimal = (intPercentage / 100) * CDec(Row.Item("UnderwritingRepaymentFees"))
                        '    'If (intPayment > CDec(Row.Item("UnderwritingRepaymentFees"))) Then intPayment = CDec(Row.Item("UnderwritingRepaymentFees"))
                        checkPaymentStatus(Row.Item("AppID"), Row.Item("PaymentHistoryReference").ToString, Row.Item("PaymentHistoryID"))
                        'Else
                        '    If (Row.Item("StatusCode") = "EPS") Then
                        '        checkExtensionPaymentStatus(Row.Item("AppID"), Row.Item("CardTrackingNumber").ToString, CDec(Row.Item("Amount")), CDec(Row.Item("UnderwritingInitialInterestRate")), CDate(Row.Item("PaymentDue").ToString))
                        '    Else
                        '        Dim intPercentage As Integer = 100
                        '        If (intOverdueDays > 0) Then
                        '            intPercentage = CDec(Row.Item("ManualPaymentAmount")) / CDec(Row.Item("CurrentBalance")) * 100
                        '            If (intPercentage > 100) Then intPercentage = 100
                        '        End If
                        '        checkPaymentStatus(Row.Item("AppID"), Row.Item("CardTrackingNumber").ToString, Row.Item("PaymentPlanMonthlyPayment").ToString, Row.Item("PaymentPlanTerm").ToString, Row.Item("PaymentPlanMonths").ToString, intPercentage)
                        '    End If
                        'End If
                    Next
                End If
                dsCache = Nothing
                'ElseIf (intMode = "6") Then ' Delinquency manual dips
                '    Dim strSQL As String = "SELECT AppID, CurrentBalance, CardTemplateNumber, DATEDIFF(DD, PaymentDue, GETDATE()) AS OverdueDays FROM vwschedulepayments WHERE NOT CardTemplateNumber IS NULL AND StatusCode IN ('COL','POT') AND (SubStatusCode <> 'CBK' OR SubStatusCode IS NULL)  AND NOT PaidOut IS NULL AND PaymentReceived IS NULL AND DATEDIFF(D,PaymentDue,GETDATE()) > 0 AND DATEDIFF(D,PaymentDue,GETDATE()) <= 60 AND Extension IS NULL AND CompanyID = '" & CompanyID & "'"
                '    If (checkValue(AppID)) Then
                '        strSQL += " AND AppID = '" & AppID & "'"
                '    End If
                '    Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                '    If (dsCache.Rows.Count > 0) Then
                '        For Each Row As DataRow In dsCache.Rows
                '            Dim intOverdueDays As Integer = CInt(Row.Item("OverdueDays"))
                '            Dim intPercentage As Integer = getPercentage(intOverdueDays)
                '            If (intPercentage > 100) Then intPercentage = 100
                '            Dim intPayment As Decimal = (intPercentage / 100) * (-1 * CDec(Row.Item("CurrentBalance")))
                '            If (intPayment > (-1 * CDec(Row.Item("CurrentBalance")))) Then intPayment = -1 * CDec(Row.Item("CurrentBalance"))
                '            requestPayment(Row.Item("AppID"), formatNumber(intPayment, 2), Row.Item("CardTemplateNumber").ToString)
                '        Next
                '    End If
                '    dsCache = Nothing
                'ElseIf (intMode = "7") Then ' Extension payments
                '    Dim strSQL As String = "SELECT AppID, CurrentBalance, CardTemplateNumber, UnderwritingTotalInterestPayment, TransmissionFee FROM vwschedulepayments WHERE NOT CardTemplateNumber IS NULL AND StatusCode IN ('CPB') AND (SubStatusCode <> 'CBK' OR SubStatusCode IS NULL)  AND NOT PaidOut IS NULL AND PaymentReceived IS NULL AND Extension IS NULL AND CompanyID = '" & CompanyID & "'"
                '    If (checkValue(AppID)) Then
                '        strSQL += " AND AppID = '" & AppID & "'"
                '    End If
                '    Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                '    If (dsCache.Rows.Count > 0) Then
                '        For Each Row As DataRow In dsCache.Rows
                '            Dim intPayment As Decimal = CDec(Row.Item("UnderwritingTotalInterestPayment")) + CDec(Row.Item("TransmissionFee"))
                '            requestPayment(Row.Item("AppID"), formatNumber(intPayment, 2), Row.Item("CardTemplateNumber").ToString, "EPS")
                '        Next
                '    End If
                '    dsCache = Nothing
            End If

        End If
    End Sub

    Private Sub authorisePayment(ByVal theAppID As String, ByVal amount As String, cardNumber As String, ByVal cv2 As String, ByVal expiry As String, ByVal accountHolder As String, ByVal line1 As String, ByVal postCode As String)
        If (checkValue(amount) And checkValue(cardNumber) And checkValue(cv2) And checkValue(expiry)) Then

            Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
            Dim strPaymentType As String = "cc_preauth"
            Dim strCurrency As String = "GBP"
            Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency)
            Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&Currency=" & strCurrency & "&PRN=" & strAccount & "&Amount=" & Replace(amount, ".", "") & "&CardNumber=" & cardNumber & "&Expiry=" & expiry & "&CVV2=" & cv2 & "&AccountName=" & accountHolder & "&BillingAddressLine1=" & line1 & "&BillingPostal=" & postCode

            'responseWrite(strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency & "<br>")
            'HttpContext.Current.Response.Write(strPost)
            'HttpContext.Current.Response.End()

            If (strErrorMessage <> "") Then
                SOAPUnsuccessfulMessage("Raven Pre-Auth Unsuccessfully Generated", 1, theAppID, "<Repayment><Status>-5</Status><Message>" & strErrorMessage & "</Message><Tracking></Tracking></Repayment>", strPost)
            Else
                ' Post the SOAP message.
                Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/submit", strPost)
                'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()

                Dim strStatus As String = "", strMessage As String = "", strTrackingNumber As String = ""
                Dim arrResponse As Array = Split(strResponse, "&")
                For x As Integer = 0 To UBound(arrResponse)
                    Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                    If (arrKeyValues(0) = "Status") Then
                        strStatus = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "Message") Then
                        strMessage = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "TrackingNumber") Then
                        strTrackingNumber = arrKeyValues(1)
                    End If
                Next

                Select Case strStatus
                    Case "Approved"
                        updateDataStoreField(theAppID, "PacnetCardTemplateNumber", strTrackingNumber, "", "")
                        updateDataStoreField(theAppID, "CardAttempts", 0, "N", 0)
                        saveNote(theAppID, Config.DefaultUserID, "Card pre-auth approved: token #" & strTrackingNumber)
                        If (strNoStatusChange <> "Y") Then
                            saveStatus(AppID, Config.DefaultUserID, "CAS", "")
                        End If
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardApproved")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Pre-Auth Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Pre-Auth Successfully Retrieved", 1, theAppID, "<Repayment><Status>1</Status><Message>" & strMessage & "</Message><Tracking>" & strTrackingNumber & "</Tracking></Repayment>", strResponse)
                    Case Else
                        If (strNoStatusChange <> "Y") Then
                            saveStatus(AppID, Config.DefaultUserID, "CAF", "")
                        End If
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardNotApproved")
                        saveNote(theAppID, Config.DefaultUserID, "Card pre-auth not approved - " & Regex.Replace(strStatus, "([A-Z0-9])", " $1") & ": " & decodeURL(strMessage))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Pre-Auth Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Pre-Auth Successfully Retrieved", 1, theAppID, "<Repayment><Status>0</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                End Select
                objReader = Nothing
                'End If
                objResponse = Nothing
            End If

        End If
    End Sub

    Private Sub requestPayment(ByVal theAppID As String, ByVal amount As String, ByVal templateNo As String, ByVal paymentID As String, Optional ByVal st As String = "CPS")
        If (checkValue(amount) And checkValue(templateNo)) Then

            Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
            Dim strPaymentType As String = "cc_debit"
            Dim strCurrency As String = "GBP"
            Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency)
            Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&Currency=" & strCurrency & "&PRN=" & strAccount & "&Amount=" & Replace(amount, ".", "") & "&TemplateNumber=" & templateNo

            'responseWrite(strUserName & strTimeStamp & strGuid & strPaymentType & Replace(amount, ".", "") & strCurrency & "<br>")
            'HttpContext.Current.Response.Write(strPost)
            'HttpContext.Current.Response.End()

            If (strErrorMessage <> "") Then
                SOAPUnsuccessfulMessage("Raven Request Unsuccessfully Generated", 1, theAppID, "<Repayment><Status>-5</Status><Message>" & strErrorMessage & "</Message><Tracking></Tracking></Repayment>", strPost)
            Else
                ' Post the SOAP message.
                Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/submit", strPost)
                'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()

                'responseWrite(strResponse)
                'responseEnd()

                Dim strStatus As String = "", strMessage As String = "", strTrackingNumber As String = ""
                Dim arrResponse As Array = Split(strResponse, "&")
                For x As Integer = 0 To UBound(arrResponse)
                    Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                    If (arrKeyValues(0) = "Status") Then
                        strStatus = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "Message") Then
                        strMessage = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "TrackingNumber") Then
                        strTrackingNumber = arrKeyValues(1)
                    End If
                Next

                Select Case strStatus
                    Case "Approved"
                        incrementDataStoreField(theAppID, "CardAttempts", 1)
                        'updateDataStoreField(theAppID, "CardTrackingNumber", strTrackingNumber, "", "")
                        saveNote(theAppID, Config.DefaultUserID, "Card payment for " & displayCurrency(amount) & " submitted")
                        saveStatus(theAppID, Config.DefaultUserID, st, "")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardPaymentSubmitted")
                        If (st = "MPS") Then
                            paymentID = insertPaymentReturnID(AppID, amount, "Manual Loan Repayment", 1, Config.DefaultDateTime)
                        End If
                        updatePaymentReference(paymentID, strTrackingNumber)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Request Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Request Successfully Retrieved", 1, theAppID, "<Repayment><Status>1</Status><Message>" & strMessage & "</Message><Tracking>" & strTrackingNumber & "</Tracking></Repayment>", strResponse)
                    Case Else
                        incrementDataStoreField(theAppID, "CardAttempts", 1)
                        Dim strNote As String = "Card payment for " & displayCurrency(amount) & " bounced: " & Regex.Replace(strStatus, "([A-Z0-9])", " $1")
                        If (checkValue(strMessage)) Then
                            strNote += " - " & decodeURL(strMessage)
                        End If
                        saveNote(theAppID, Config.DefaultUserID, strNote)
                        saveStatus(theAppID, Config.DefaultUserID, "CPB", "")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardBounced")
                        updatePaymentReference(paymentID, strTrackingNumber)
                        If (st = "MPS") Then
                            paymentID = insertPaymentReturnID(AppID, amount, "Manual Loan Repayment", 1, Config.DefaultDateTime)
                            updatePayment(paymentID, 2)
                        End If
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Request Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Request Successfully Retrieved", 1, theAppID, "<Repayment><Status>0</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                End Select
                objReader = Nothing
                'End If
                objResponse = Nothing
            End If

        End If
    End Sub

    Private Sub checkPaymentStatus(ByVal theAppID As String, ByVal tracking As String, ByVal paymentID As String)

        If (checkValue(tracking)) Then

            Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
            Dim strPaymentType As String = "cc_debit"
            Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid)
            Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&PRN=" & strAccount & "&TrackingNumber=" & tracking

            'responseWrite(strUserName & strTimeStamp & strGuid & "<br>")
            'HttpContext.Current.Response.Write(strPost)
            'HttpContext.Current.Response.End()

            If (strErrorMessage <> "") Then
                SOAPUnsuccessfulMessage("Raven Repayment Unsuccessfully Generated", 1, theAppID, "<Repayment><Status>-5</Status><Message>" & strErrorMessage & "</Message><Tracking></Tracking></Repayment>", strPost)
            Else
                ' Post the SOAP message.
                Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/status", strPost)
                'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()

                'responseWrite(strResponse)
                'responseEnd()

                Dim strStatus As String = "", strMessage As String = "", intAmount As String = ""
                Dim arrResponse As Array = Split(strResponse, "&")
                For x As Integer = 0 To UBound(arrResponse)
                    Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                    If (arrKeyValues(0) = "Status") Then
                        strStatus = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "Message") Then
                        strMessage = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "FormattedAmount") Then
                        intAmount = decodeURL(arrKeyValues(1))
                    End If
                Next

                Select Case strStatus
                    Case "Approved"
                        saveNote(theAppID, Config.DefaultUserID, "Card payment successful")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "PaymentSubmitted")
                        saveStatus(theAppID, Config.DefaultUserID, "POT", "")
                        updatePayment(paymentID, "1")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Repayment Successfully Generated", strPost)
                        ' If logic to handle full repayment

                        ' End If
                        SOAPSuccessfulMessage("Raven Repayment Successfully Retrieved", 1, theAppID, "<Repayment><Status>1</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                    Case Else
                        Dim strNote As String = "Card payment bounced: " & Regex.Replace(strStatus, "([A-Z0-9])", " $1")
                        If (checkValue(strMessage)) Then
                            strNote += " - " & decodeURL(strMessage)
                        End If
                        saveNote(theAppID, Config.DefaultUserID, strNote)
                        saveStatus(theAppID, Config.DefaultUserID, "CPB", "")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardBounced")
                        updatePayment(tracking, "3")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Repayment Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Repayment Successfully Retrieved", 1, theAppID, "<Repayment><Status>0</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                End Select
                objReader = Nothing
                objResponse = Nothing
            End If

        End If

    End Sub

    Private Sub checkExtensionPaymentStatus(ByVal theAppID As String, ByVal tracking As String, ByVal loanAmount As Decimal, ByVal rate As Decimal, ByVal paymentDue As Date)

        If (checkValue(tracking)) Then

            Dim strTimeStamp As String = ddmmyyhhmmss2utcz(Config.DefaultDateTime, False)
            Dim strPaymentType As String = "cc_debit"
            Dim strSignature As String = hashString(strSecret, strUserName & strTimeStamp & strGuid)
            Dim strPost As String = "RAPIVersion=2&UserName=" & strUserName & "&Timestamp=" & strTimeStamp & "&RequestID=" & strGuid & "&Signature=" & strSignature & "&PymtType=" & strPaymentType & "&PRN=" & strAccount & "&TrackingNumber=" & tracking

            'responseWrite(strUserName & strTimeStamp & strGuid & "<br>")
            'HttpContext.Current.Response.Write(strPost)
            'HttpContext.Current.Response.End()

            If (strErrorMessage <> "") Then
                SOAPUnsuccessfulMessage("Raven Extension Unsuccessfully Generated", 1, theAppID, "<Repayment><Status>-5</Status><Message>" & strErrorMessage & "</Message><Tracking></Tracking></Repayment>", strPost)
            Else
                ' Post the SOAP message.
                Dim objResponse As HttpWebResponse = postRavenWebRequest(strXMLURL & "/status", strPost)
                'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()

                Dim strStatus As String = "", strMessage As String = "", intAmount As String = ""
                Dim arrResponse As Array = Split(strResponse, "&")
                For x As Integer = 0 To UBound(arrResponse)
                    Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                    If (arrKeyValues(0) = "Status") Then
                        strStatus = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "Message") Then
                        strMessage = arrKeyValues(1)
                    ElseIf (arrKeyValues(0) = "FormattedAmount") Then
                        intAmount = decodeURL(arrKeyValues(1))
                    End If
                Next

                Select Case strStatus
                    Case "Approved"
                        Dim intInterestPayment As Decimal = loanAmount * rate * 30
                        updateDataStoreField(theAppID, "ExtensionTotalInterestPayment", intInterestPayment, "M", 0)
                        updateDataStoreField(theAppID, "ExtensionTotalRepayment", intInterestPayment + loanAmount, "M", 0)
                        updateDataStoreField(theAppID, "ExtensionFee", loanAmount, "M", 0)
                        updateDataStoreField(theAppID, "ExtensionTerm", 30, "N", 0)
                        saveStatus(theAppID, Config.DefaultUserID, "EXT", "")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "Extension", Config.DefaultDateTime)
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "ExtensionPaymentDue", paymentDue.AddDays(30))
                        saveNote(theAppID, Config.DefaultUserID, "Extension payment successful")
                        logExtensionPayment(theAppID, intAmount, tracking, True)
                        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
                        If (IsNumeric(strValue)) Then
                            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + " & CDbl(intAmount) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                            executeNonQuery(strUpdate)
                        End If
                        executeNonQuery("EXECUTE spcalculateextensionapr @TheAppID = '" & AppID & "'")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Extension Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Extension Payment Successfully Retrieved", 1, theAppID, "<Repayment><Status>1</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                    Case Else
                        Dim strNote As String = "Extension payment bounced: " & Regex.Replace(strStatus, "([A-Z0-9])", " $1")
                        If (checkValue(strMessage)) Then
                            strNote += " - " & decodeURL(strMessage)
                        End If
                        saveNote(theAppID, Config.DefaultUserID, strNote)
                        saveStatus(theAppID, Config.DefaultUserID, "CPB", "")
                        saveUpdatedDate(theAppID, Config.DefaultUserID, "CardBounced")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), theAppID, 1, "Raven Extension Successfully Generated", strPost)
                        SOAPSuccessfulMessage("Raven Extension Payment Successfully Retrieved", 1, theAppID, "<Repayment><Status>0</Status><Message>" & strMessage & "</Message><Tracking></Tracking></Repayment>", strResponse)
                End Select
                objReader = Nothing
                'End If
                objResponse = Nothing
            End If

        End If

    End Sub

    Private Function hashString(ByVal key As String, ByVal text As String) As String
        Dim objEncoder As New System.Text.UTF8Encoding
        Dim bKey() As Byte = objEncoder.GetBytes(key)
        Dim bText() As Byte = objEncoder.GetBytes(text)
        Dim objHash As New System.Security.Cryptography.HMACSHA1(bKey, True)
        Dim bHash As Byte() = objHash.ComputeHash(bText)
        Dim hash As String = Replace(BitConverter.ToString(bHash), "-", "")
        Return hash.ToLower
    End Function

    Private Sub logPayment(ByVal theAppID As String, ByVal amount As String, ByVal ref As String, ByVal success As Boolean)
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(theAppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(amount, "M", 0) & ", " & _
                        formatField("Repayment", "", "") & ", " & _
                        formatField(ref, "", "") & ", " & _
                        formatField(success, "B", 0) & ") "
        executeNonQuery(strQry)
        If (success) Then incrementDataStoreFieldDecimal(theAppID, "CurrentBalance", CDbl(amount))
    End Sub

    Private Sub logExtensionPayment(ByVal theAppID As String, ByVal amount As String, ByVal ref As String, ByVal success As Boolean)
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(theAppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(amount, "M", 0) & ", " & _
                        formatField("Extension Fee", "", "") & ", " & _
                        formatField(ref, "", "") & ", " & _
                        formatField(success, "B", 0) & ") "
        executeNonQuery(strQry)
        If (success) Then incrementDataStoreFieldDecimal(theAppID, "CurrentBalance", CDbl(amount))
    End Sub

    Private Sub incrementDataStoreFieldDecimal(ByVal AppID As String, ByVal name As String, ByVal incr As Double)
        If (checkValue(AppID)) Then
            Dim strValue As String = getAnyFieldFromDataStore(name, AppID)
            If (IsNumeric(strValue)) Then
                Dim strQry As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(FLOAT,StoredDataValue) + " & incr & " WHERE StoredDataName = '" & name & "' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
        End If
    End Sub

    Private Function getCardDetails(ByVal details As String) As String
        Dim strDetails As String = ""
        Dim strSQL As String = "EXEC spdecryptcarddetails @AppID = " & AppID & ", @CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strDetails = Row.Item(details).ToString
            Next
        End If
        dsCache = Nothing
        Return strDetails
    End Function

    Private Function postRavenWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = True
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .ContentType = "application/x-www-form-urlencoded"
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            'responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        xml = regexReplace("&CardNumber=([0-9]{0,16})", "&CardNumber=****************", xml)
        xml = regexReplace("&CVV2=([0-9]{0,16})", "&CVV2=***", xml)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
