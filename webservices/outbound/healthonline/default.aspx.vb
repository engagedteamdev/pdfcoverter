﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.myhealthonline.staging

Partial Class XMLHealthOnline
    Inherits System.Web.UI.Page

    Private strAuthURL As String = "", strXMLURL As String = ""
    Private strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Public Sub generateXml()

        If (strEnvironment = "live") Then
            strXMLURL = "https://www.myhealth-on-line.co.uk/holws_v1_0_1/service.asmx"
        Else
            strXMLURL = "https://staging.myhealth-on-line.co.uk/holws_v1_0_1/service.asmx"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%HealthOnline%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "HealthOnlineUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "HealthOnlinePassword") Then strPassword = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/healthonline/blank.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("def", "http://www.health-on-line.co.uk/holws_v1_0_1")

        writeToNode(objInputXMLDoc, "y", "def:AddLeadWithMultipleTelNo", "def:strUserName", strUserName, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:AddLeadWithMultipleTelNo", "def:strPassword", strPassword, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:AddLeadWithMultipleTelNo", "def:strReferer", strMediaCampaignCampaignReference, objNSM)

        Dim strSQL As String = "SELECT * FROM vwxmlhealthonline WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                            ' Handle non standard fields
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += "def:" & arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & arrName(UBound(arrName)), Row(Column).ToString, objNSM)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            objNSM2 = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
            objNSM2.AddNamespace("def", "http://www.health-on-line.co.uk/holws_v1_0_1")

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//def:AddLeadWithMultipleTelNoResponse", objNSM2)
                Dim strResult As String = objApplication.SelectSingleNode("//def:AddLeadWithMultipleTelNoResult", objNSM2).InnerText
                Dim strMessage As String = ""
                Select Case strResult > 0
                    Case True
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Health Online"))
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", strResult, "NULL")
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Health Online", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Health Online", objOutputXMLDoc.InnerXml)
                        'saveNote(AppID, Config.DefaultUserID, strMessage)
                    Case Else
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Health Online"))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Health Online", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Health Online", objOutputXMLDoc.InnerXml)
                        'saveNote(AppID, Config.DefaultUserID, strMessage)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
