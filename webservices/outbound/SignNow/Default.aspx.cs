using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Collections.Specialized;
using RestSharp;

public partial class Default : Page
{


	public HttpFileCollection filepath = HttpContext.Current.Request.Files;



	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	class theData
	{
	
		public string username { get; set; }
		public string password { get; set; }
		public string grant_type { get; set; }
		public string scope { get; set; }
	}

	class theDocumentData
	{

		public string file { get; set; }

	}


	public void Run()
	{


		//var client = new RestClient("https://api-eval.signnow.com/oauth2/token");
		//var request = new RestRequest(Method.POST);
		//request.AddHeader("authorization", "Basic {MTEyZmM0NWI5NWU3MDM4ZmFiMGNlOGM3ZGY2YTk1NTA6NmJiZjUzOGEwZDM3YjIwOTUyN2M3ZTY0YWYzODdjOTk=}");
		//request.AddHeader("content-type", "multipart/form-data;");

		//theData data = new theData();
		//data.username = "crmsupport@engagedresource.co.uk";
		//data.password = "Y0rkshire";
		//data.grant_type = "password";
		//data.scope = "*";

		//request.AddJsonBody(data);
		//IRestResponse response = client.Execute(request);

		//var jsonnew = response.Content.ToString();

		//var jo = JObject.Parse(jsonnew);

		//var AccessToken = jo["access_token"].ToString();

		var fullFileName = "remortgages_PDF_Day1.pdf";
		var filepath = @"C:\remortgages_PDF_Day1.pdf";

		var clientUploadDocument = new RestClient("https://api-eval.signnow.com/document");

		var requestUploadDocument = new RestRequest(Method.POST);


		requestUploadDocument.AddHeader("Authorization", "Bearer c3b47009f2a3a70c169da2a1248b8b33dc39e27df5751928380b6e305709bb63");
		requestUploadDocument.AddHeader("Content-Type", "multipart/form-data;");
		requestUploadDocument.AddFile(Path.GetFileNameWithoutExtension(fullFileName), filepath);
		requestUploadDocument.AlwaysMultipartFormData = true;

		IRestResponse responseUploadDocument = clientUploadDocument.Execute(requestUploadDocument);

		var jsonUploadDocument = responseUploadDocument.Content.ToString();

		var joUploadDocument = JObject.Parse(jsonUploadDocument);

		Response.Write(joUploadDocument.ToString());
		Response.End();


		//var clientUploadDocument = new RestClient("https://api-eval.signnow.com/document");
		//var requestUploadDocument = new RestRequest(Method.POST);
		//requestUploadDocument.AddHeader("cache-control", "no-cache");
		//requestUploadDocument.AddHeader("Content-Length", "1389128");
		//requestUploadDocument.AddHeader("Accept-Encoding", "gzip, deflate");
		//requestUploadDocument.AddHeader("Content-Type", "multipart/form-data; boundary=--------------------------128052861532455904001127");
		//requestUploadDocument.AddHeader("Host", "api-eval.signnow.com");
		//requestUploadDocument.AddHeader("Cache-Control", "no-cache");
		//requestUploadDocument.AddHeader("Accept", "*/*");
		//requestUploadDocument.AddHeader("Authorization", "Bearer c3b47009f2a3a70c169da2a1248b8b33dc39e27df5751928380b6e305709bb63");
		//requestUploadDocument.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
		//requestUploadDocument.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"file\"; filename=\"remortgages_PDF_Day1.pdf\"\r\nContent-Type: application/pdf\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);

		//IRestResponse responseUploadDocument = clientUploadDocument.Execute(requestUploadDocument);

		




		//var client = new RestClient("https://api-eval.signnow.com/document");
		//var request = new RestRequest(Method.POST);
		//request.AddHeader("cache-control", "no-cache");
		//request.AddHeader("Connection", "keep-alive");
		//request.AddHeader("Content-Length", "1389128");
		//request.AddHeader("Accept-Encoding", "gzip, deflate");
		//request.AddHeader("Content-Type", "multipart/form-data; boundary=--------------------------128052861532455904001127");
		//request.AddHeader("Host", "api-eval.signnow.com");
		//request.AddHeader("Postman-Token", "93e546eb-b2f4-4b14-8133-40aa7fe785b9,436580a6-0822-469d-9795-d241396db68c");
		//request.AddHeader("Cache-Control", "no-cache");
		//request.AddHeader("Accept", "*/*");
		//request.AddHeader("User-Agent", "PostmanRuntime/7.20.1");
		//request.AddHeader("Authorization", "Bearer c3b47009f2a3a70c169da2a1248b8b33dc39e27df5751928380b6e305709bb63");
		//request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
		//request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"file\"; filename=\"remortgages_PDF_Day1.pdf\"\r\nContent-Type: application/pdf\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
		//IRestResponse response = client.Execute(request);


	}



}
