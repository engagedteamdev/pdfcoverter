﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLNemo
    Inherits System.Web.UI.Page

    Private strUserID As String = "", strPassword As String = "", strXMLURL As String = ""
    Private strSourceBranchNo As String = "", strMediaCode As String = "", strMarketingType As String = "", strMediaType As String = "", strChannel As String = "", strAdvertisementCode As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XMLDocument = New XMLDocument
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        If (strEnvironment = "live") Then
        	strXMLURL = "https://creditapps.nemo-loans.co.uk/creditapps/receiveXML.do"
        Else
        	strXMLURL = "https://creditappsuat.nemo-loans.co.uk/creditappsuat/receiveXML.do"
       	End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%NEMO%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "NEMOUserID") Then strUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOSourceBranchNo") Then strSourceBranchNo = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOMediaCode") Then strMediaCode = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOMarketingType") Then strMarketingType = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOMediaType") Then strMediaType = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOChannel") Then strChannel = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "NEMOAdvertisementCode") Then strAdvertisementCode = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/nemo/blank.xml"))

        Call writeXML("y", "Login", "UserId", strUserID)
        Call writeXML("y", "Login", "Password", strPassword)
        Call writeXML("y", "Application/BrokerDetails", "BrokerId", AppID)

        Call writeXML("y", "Application/InitialDetails", "SourceBranchNo", strSourceBranchNo)
        Call writeXML("y", "Application/InitialDetails", "MediaCode", strMediaCode)
        Call writeXML("y", "Application/InitialDetails", "MarketingType", strMarketingType)
        Call writeXML("y", "Application/InitialDetails", "MediaType", strMediaType)
        Call writeXML("y", "Application/InitialDetails", "Channel", strChannel)
        Call writeXML("y", "Application/InitialDetails", "AdvertisementCode", strAdvertisementCode)

        Dim strSQL As String = "SELECT * FROM vwxmlnemo WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName.ToString = "App1HomeTelephone") Then
                            Dim arrTelephone = splitTelephone(Row(Column).ToString)
                            writeXML("n", "Applicant1", "HomeSTD", arrTelephone(0))
                            writeXML("n", "Applicant1", "HomeNumber", arrTelephone(1))
                        ElseIf (Column.ColumnName.ToString = "App1MobileTelephone") Then
                            Dim arrTelephone = splitTelephone(Row(Column).ToString)
                            writeXML("n", "Applicant1", "MobileSTD", arrTelephone(0))
                            writeXML("n", "Applicant1", "MobileNumber", arrTelephone(1))
                        ElseIf (Column.ColumnName.ToString = "App2MobileTelephone") Then
                            Dim arrTelephone = splitTelephone(Row(Column).ToString)
                            writeXML("n", "Applicant2", "MobileSTD", arrTelephone(0))
                            writeXML("n", "Applicant2", "MobileNumber", arrTelephone(1))
                        Else
                            If (Column.ColumnName.ToString = "Applicant2/Title") Then
                                insertElement("Applicant1", "Application", "Applicant2")
                                insertElement("EmploymentDetails1", "Application", "EmploymentDetails2")
                            End If
                            If (Column.ColumnName.ToString = "PreviousAddress1/HouseNumber") Then
                                insertElement("CurrentAddress", "Application", "PreviousAddress1")
                                createElement("PreviousAddress1", "Address")
                            End If
                            If (Column.ColumnName.ToString = "PreviousAddress1/HouseNameFlatNumber") Then
                                insertElement("CurrentAddress", "Application", "PreviousAddress1")
                                createElement("PreviousAddress1", "Address")
                            End If
                            If (Column.ColumnName.ToString = "PreviousAddress2/HouseNumber") Then
                                insertElement("PreviousAddress1", "Application", "PreviousAddress2")
                                createElement("PreviousAddress2", "Address")
                            End If
                            If (Column.ColumnName.ToString = "PreviousAddress2/HouseNameFlatNumber") Then
                                insertElement("PreviousAddress1", "Application", "PreviousAddress2")
                                createElement("PreviousAddress2", "Address")
                            End If
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeXML("n", "Application/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)
                        End If
                    End If
                Next
            Next
        End If

        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, "xmlString=" & objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//Application")
                Dim objNodeList As XmlNodeList = objOutputXMLDoc.SelectNodes("//UnderwritingMessages")
                Dim strMessage As String = ""
                For Each Child As XmlNode In objNodeList
                    strMessage += Child.InnerText + "<br />"
                Next
                Select Case objApplication.GetAttribute("Status")
                    Case "ACCEPTED"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by NEMO: " & strMessage))
                        updateDataStoreField(AppID, "UnderwritingLenderName", "NEMO Personal Finance", "", "")
                        updateDataStoreField(AppID, "UnderwritingLenderRef", objApplication.GetAttribute("ApplicationNumber"), "", "")
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.GetAttribute("ApplicationNumber"), "NULL")
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "NEMACC", "NULL")
                        'setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveNote(AppID, intHotkeyUserID, "Application No. " & objApplication.GetAttribute("ApplicationNumber") & " Accepted by NEMO")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Accepted - " & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Accepted - " & strMessage, objOutputXMLDoc.InnerXml)
                    Case "DECLINED"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by NEMO: " & strMessage))
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "NEMDEC", "NULL")
                        saveNote(AppID, intHotkeyUserID, "Application Declined by NEMO - " & strMessage)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Declined - " & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Declined - " & strMessage, objOutputXMLDoc.InnerXml)
                    Case "REFERRED"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Referred by NEMO: " & strMessage))
                        updateDataStoreField(AppID, "UnderwritingLenderName", "NEMO Personal Finance", "", "")
                        updateDataStoreField(AppID, "UnderwritingLenderRef", objApplication.GetAttribute("ApplicationNumber"), "", "")
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.GetAttribute("ApplicationNumber"), "NULL")
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "NEMREF", "NULL")
                        'setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveNote(AppID, intHotkeyUserID, "Application No. " & objApplication.GetAttribute("ApplicationNumber") & " Referred by NEMO - " & strMessage)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Referred - " & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "NEMO Referred - " & strMessage, objOutputXMLDoc.InnerXml)
                    Case Else
                        objNodeList = objOutputXMLDoc.SelectNodes("//error_description")
                        For Each Child As XmlNode In objNodeList
                            strMessage += Child.InnerText + "<br />"
                        Next
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Failed by NEMO: " & regexReplace("Error parsing XML: Error on line ([0-9]{1,2}): ", "", strMessage)))
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "AWT", "NULL")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "NEMO Failed - " & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "NEMO Failed - " & strMessage, objOutputXMLDoc.InnerXml)
                End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, "An error occured in the service", objOutputXMLDoc.InnerXml)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeXML(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub insertElement(ByVal before As String, ByVal parent As String, ByVal name As String)
        Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & name)
        Dim objNodeBefore = objInputXMLDoc.SelectSingleNode("//" & before)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(name)
            objApplication.InsertAfter(objNewNode, objNodeBefore)
        End If
    End Sub

    Private Sub createElement(ByVal parent As String, ByVal name As String)
        Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & name)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(name)
            objApplication.AppendChild(objNewNode)
        End If
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class