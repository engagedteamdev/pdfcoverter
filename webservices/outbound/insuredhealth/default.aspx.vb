﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLEFinity
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strPassword As String = "", strAccountNumber As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private intOffice As String = HttpContext.Current.Request("frmOffice")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, strPost As String = "", objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""

    Private Enum Office
        Sheffield = 1
        Poole = 2
    End Enum

    Public Sub generateXml()
        'If (strEnvironment = "live") Then
        'Select Case intOffice
        '    Case Office.Sheffield
        '        strXMLURL = "http://leads.insureyourhealth.co.uk/ActWebService/WebToLead.ashx"
        '    Case Office.Poole
        '        strXMLURL = "http://213.120.225.146/HGSLeadInterface/WebToLead.ashx"
        'End Select
        'Else
        Select Case intOffice
            Case Office.Sheffield
                strXMLURL = "http://leads.insureyourhealth.co.uk/ActWebService/WebToLead.ashx"
            Case Office.Poole
                strXMLURL = "http://213.120.225.146/HGSLeadInterface/WebToLead.ashx"
        End Select
        'End If
        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        strPost += "order_id=" & strMediaCampaignCampaignReference

        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()
        Dim strView As String = "vwxmlinsuredhealth"
        If (intOffice = Office.Poole) Then
            strView = "vwxmlinsuredhealthpoole"
        End If
        Dim strSQL As String = "SELECT * FROM " & strView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 1) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If
                        strPost += "&" & arrName(UBound(arrName)) & "=" & Row(Column).ToString
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            incrementTransferAttempts(AppID, strMediaCampaignID)
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, strPost, False, False, False)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            Dim strResponse As String = objReader.ReadToEnd()
            Dim strMessage As String = ""

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Select Case strResponse
                    Case "1"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Insured Health"))
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Insured Health", strPost)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Insured Health", strResponse)
                        sendIndividualApp()
                    Case Else
                        Dim arrResponse As Array = Split(strResponse, "Error: ")
                        If (arrResponse.Length = 2) Then
                            strMessage = arrResponse(1)
                        End If
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Failed by Insured Health: " & strMessage))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Insured Health: " & strMessage, strPost)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Insured Health: " & strMessage, strResponse)
                End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Public Sub sendIndividualApp()
        Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
        Dim strDeliveryEmailAddress = getAnyFieldByCompanyID("MediaCampaignDeliveryEmailAddress", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strDeliveryEmailAddress)) Then
            Dim strApplication As String = _
            "Please find below a new application from " & strCompanyName & vbCrLf & vbCrLf & _
            "Ref: " & AppID & vbCrLf & vbCrLf

            Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressPostCode, CallBackDate FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    For Each Column As DataColumn In dsCache.Columns
                        If (checkValue(Row(Column).ToString)) Then
                            strApplication += Regex.Replace(Column.ColumnName.ToString, "([A-Z0-9])", " $1") & ": " & Row(Column).ToString & vbCrLf
                        End If
                    Next
                Next
            End If
            dsCache = Nothing

            strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                        strApplication += Regex.Replace(Row.Item("StoredDataName").ToString, "([A-Z0-9])", " $1") & ": " & Row.Item("StoredDataValue").ToString & vbCrLf
                    End If
                Next
            End If
            dsCache = Nothing

            'Response.Write(strApplication)
            postEmailWithMediaCampaignID("", strDeliveryEmailAddress, "New lead from " & strCompanyName, strApplication, False, "")
        End If

    End Sub

    Private Sub writeAttribute(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = objInputXMLDoc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNodeByNumber(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal lvl As Integer)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNodeList = objInputXMLDoc.GetElementsByTagName(fld)
                If Not (objTest Is Nothing) Then
                    objTest.Item(lvl - 1).InnerText = val
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
