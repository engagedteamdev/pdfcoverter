﻿Imports Config, Common, CommonSave, PDF
Imports System.Xml
Imports System.Net
Imports System.IO

' ** Revision history **
'
' 12/03/2012    - Added functonality to toggle between 1st/2nd life for IP
' 21/03/2012    - Added budget IP quoting
' ** End Revision History **

Partial Class XMLAvelo
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID"), UserID As String = HttpContext.Current.Request("UserID"), QuoteID As String = HttpContext.Current.Request("QuoteID"), XMLReceivedID As String = HttpContext.Current.Request("XMLReceivedID"), strICRN As String = HttpContext.Current.Request("ICRN")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strXMLURL As String = "", strXMLFile As String = "", strView As String = "", strIntegratorID As String = "", strBusinessContractID As String = "", strMessageID As String = System.Guid.NewGuid.ToString, strResponseLocation As String = "", strProfile As String = ""
    Private strUserID As String = "", strUserID2 As String = "" ' use this where you are just supplying one value
    Private strUserName As String = "", strUserName2 As String = "" ' use this plus password where you need to supply 2 references
    Private strPassword As String = "", strPassword2 As String = ""
    Private strSIBNumber As String = "", strCompanyName As String = "", strBranchName As String = "", strCompanyPostCode As String = "", strCompanyIndividual As String = ""
    Private strServiceID As String = HttpContext.Current.Request("strServiceID"), strProductCode As String = HttpContext.Current.Request("strProductCode")
    Private intMode As String = HttpContext.Current.Request("intMode")
    Private strErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " (Live) Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Private Enum ServiceMode
        WOLQuote = 1
        TermQuote = 2
        IPQuote = 3
        NewBusiness = 4
        ViewDocuments = 5
        SaveDocuments = 6
    End Enum

    Public Sub generateXml()
        HttpContext.Current.Server.ScriptTimeout = 120
        'If (strEnvironment = "live") Then
            strXMLURL = "https://services.exchange.uk.com/exchangeserviceslistener/exchangeserviceslistener.aspx?service_id=" & strServiceID
            strXMLFile = getXMLFile(strServiceID)
            strView = getView(strServiceID)
        'Else
            'strXMLURL = "https://ifapreview-services.exchange.uk.com/exchangeserviceslistener/exchangeserviceslistener.aspx?service_id=" & strServiceID
            'strXMLFile = getXMLFile(strServiceID)
            'strView = getView(strServiceID)
        'End If

        strUserName2 = getAnyFieldFromUserStore("AveloUserName", UserID)
        strPassword2 = getAnyFieldFromUserStore("AveloPassword", UserID)
        strUserID2 = getAnyFieldFromUserStore("AveloUserID", UserID)

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Avelo%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "AveloIntegratorID") Then strIntegratorID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloBusinessContractID") Then strBusinessContractID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloUserID") Then strUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloResponseLocation") Then strResponseLocation = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloProfile") Then strProfile = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloSIBNumber") Then strSIBNumber = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloCompanyName") Then strCompanyName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloBranchName") Then strBranchName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloCompanyPostCode") Then strCompanyPostCode = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AveloCompanyIndividual") Then strCompanyIndividual = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            checkMandatory()
            Select Case intMode
                Case ServiceMode.WOLQuote
                    getWOLQuote()
                Case ServiceMode.TermQuote
                    getTermQuote()
                Case ServiceMode.IPQuote
                    getIPQuote()
                Case ServiceMode.NewBusiness
                    getNewBusiness()
                Case ServiceMode.SaveDocuments
                    getDocuments(True, False)
                Case ServiceMode.ViewDocuments
                    getDocuments(False, True)
            End Select

        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"App1Title", "App1Sex", "App1DOB", "App2Sex", "App2DOB"}
        Dim arrNames As String() = {"Client Title", "Client Gender", "Client DOB", "Partner Gender", "Partner DOB"}
        Dim strApp2Title As String = ""
        Dim strSQL As String = "SELECT App1Title, App1Sex, App1DOB, App2Title, App2Sex, App2DOB FROM tblapplications WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (Column.ColumnName = "App2Title") Then
                        strApp2Title = Row(Column).ToString
                    Else
                        For x As Integer = 0 To UBound(arrMandatory)
                            If (arrMandatory(x) = "App2Sex" And Column.ColumnName = "App2Sex") Then
                                If (checkValue(strApp2Title)) Then
                                    If (Not checkValue(Row(Column).ToString)) Then
                                        boolMandatory = False
                                        strMandatoryField = arrNames(x)
                                        Exit For
                                    End If
                                End If
                            ElseIf (arrMandatory(x) = "App2DOB" And Column.ColumnName = "App2DOB") Then
                                If (checkValue(strApp2Title)) Then
                                    If (Not checkValue(Row(Column).ToString)) Then
                                        boolMandatory = False
                                        strMandatoryField = arrNames(x)
                                        Exit For
                                    End If
                                End If
                            ElseIf (arrMandatory(x) = Column.ColumnName) Then
                                If (Not checkValue(Row(Column).ToString)) Then
                                    boolMandatory = False
                                    strMandatoryField = arrNames(x)
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Next
            Next
        End If
        If (Not boolMandatory) Then
            responseWrite("0|Please correct the following mandatory field: " & strMandatoryField)
            responseEnd()
        Else
            dsCache = Nothing
        End If
    End Sub

    Private Sub getWOLQuote()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/" & strXMLFile))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("def", "http://www.texorigoservices.com")

        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_id", strMessageID)
        'writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:service_id", strServiceID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:user_id", strUserID2)

        Dim strNewICRN As String = AppID & getIndexNumber()

        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:control_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:message_id", strMessageID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_orchestration_id", UserID & "_" & strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:user_id", strUserName2 & " " & strPassword2)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:response_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:profile", strProfile)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:return_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:intermediary_case_reference_number", strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:submission_date", ddmmyyyy2yyyymmdd(Config.DefaultDateTime))

        writeToNode(objInputXMLDoc, "n", "esreq:payload/def:message/def:m_content/def:intermediary", "def:sib_number", strSIBNumber)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:company_name", strCompanyName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:branch_name", strBranchName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:agency_address", "def:postcode", strCompanyPostCode)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:contact_details", "def:name", strCompanyIndividual)

        Dim strSQL As String = "SELECT * FROM " & strView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strNote As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim strCoverType As String = "", intAmount As Integer = 0, intProductTerm As Integer = 0, intCICoverAmount As Integer = 0, strWaiverApp1Life As String = "", strWaiverApp2Life As String = ""
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName = "Amount") Then
                            intAmount = Row(Column).ToString
                        ElseIf (Column.ColumnName = "ProductTerm") Then
                            intProductTerm = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyCoverType") Then
                            strCoverType = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyStartDate") Then
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:policy_term", "def:start_date", ddmmyyyy2yyyymmdd(Row(Column).ToString))
                        ElseIf (Column.ColumnName = "PolicyCoverFor") Then
                            If (Row(Column).ToString = "Joint Lives") Then
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                                objLifeAssured = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:life_assured[@personal_client_id='pc1']", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc2")
                                objLifeAssured.SetAttribute("sequence_number", "2")
                            ElseIf (Row(Column).ToString = "First Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", objNSM)
                                If (Not objPC2 Is Nothing) Then
                                    objApplication.RemoveChild(objPC2)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", objNSM)
                                If (Not objBenefits2 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits2)
                                End If
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            ElseIf (Row(Column).ToString = "Second Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", objNSM)
                                If (Not objPC1 Is Nothing) Then
                                    objApplication.RemoveChild(objPC1)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[1]", objNSM)
                                If (Not objBenefits1 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits1)
                                End If
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", "id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "personal_client_id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "sequence_number", "1")
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            End If
                        ElseIf (Column.ColumnName = "PolicyQuotationBasis") Then
                            If (Row(Column).ToString = "Sum Assured") Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:life_assured[last()]", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:lump_sum_benefit", "lump_sum_benefit")
                                Dim objAmount As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:lump_sum_benefit", "def:amount", "amount")
                                objAmount.SetAttribute("currency", "GBP")
                            End If
                        ElseIf (Column.ColumnName = "PolicyPremium") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount", "currency", "GBP")
                        ElseIf (Column.ColumnName = "PolicyWaiverApp1Life") Then
                            strWaiverApp1Life = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyWaiverApp2Life") Then
                            strWaiverApp2Life = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyWaiver") Then
                            If (Row(Column).ToString = "Y") Then
                                Dim objRiskBenefit2 = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']", objNSM)
                                Dim objWaiver As XmlElement = Nothing
                                If (Not objRiskBenefit2 Is Nothing) Then
                                    objWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:risk_benefit", "risk_benefit")
                                    objWaiver.SetAttribute("id", "rb3")
                                    Dim objRiskWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution/def:payment_indexation", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution", "def:waiver", "waiver")
                                    objRiskWaiver.SetAttribute("risk_benefit_id", "rb3")
                                Else
                                    objWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:risk_benefit", "risk_benefit")
                                    objWaiver.SetAttribute("id", "rb2")
                                    Dim objRiskWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution/def:payment_indexation", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution", "def:waiver", "waiver")
                                    objRiskWaiver.SetAttribute("risk_benefit_id", "rb2")
                                End If
                                If (Not objWaiver Is Nothing) Then
                                    objWaiver.SetAttribute("type", "Waiver")
                                    If (strWaiverApp1Life = "Disability" And strWaiverApp2Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp1Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife1 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife1.SetAttribute("personal_client_id", "pc1")
                                        objLife1.SetAttribute("sequence_number", "1")
                                        Dim objLife2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:life_assured[1]", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife2.SetAttribute("personal_client_id", "pc2")
                                        objLife2.SetAttribute("sequence_number", "2")
                                    ElseIf (strWaiverApp1Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp1Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife1 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife1.SetAttribute("personal_client_id", "pc1")
                                        objLife1.SetAttribute("sequence_number", "1")
                                    ElseIf (strWaiverApp2Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp2Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife2.SetAttribute("personal_client_id", "pc2")
                                        objLife2.SetAttribute("sequence_number", "2")
                                    End If
                                    createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_contribution", "risk_contribution")
                                    writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_contribution", "cost_basis", "Funded From Waived Risk Contribution")
                                End If
                            End If
                        ElseIf (Column.ColumnName = "App2Title") Then
                            Dim objApp2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", "esreq:payload/def:message/def:m_content/def:application", "def:personal_client[@id='pc2']", "personal_client")
                            objApp2.SetAttribute("id", "pc2")
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:benefits_required", "benefits_required")
                            Dim objLife2 As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", "def:life_assured", "life_assured")
                            objLife2.SetAttribute("personal_client_id", "pc2")
                            objLife2.SetAttribute("sequence_number", "2")
                        ElseIf (Column.ColumnName = "PolicyCommissionSacrifice") Then
                            'insertElementBefore(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:indemnity_ind", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]", "def:sacrifice", "sacrifice")
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:uplift", "def:percent", 100 - CInt(Row(Column).ToString))
                            'writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:sacrifice/def:percent", "calculation_basis", "Entitlement")
                        ElseIf (Column.ColumnName = "ReviewableRates") Then
                            If (Row(Column).ToString = "Yes") Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[1]", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:product_option", "product_option")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[2]", "type", "Reviewable Rates")
                            End If
                        ElseIf (Column.ColumnName = "Note") Then
                            strNote = Row(Column).ToString
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            'responseWrite(arrName(UBound(arrName)) & "=" & Row(Column) & "<br>")
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & arrName(UBound(arrName)), Row(Column).ToString)

                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objResponse As HttpWebResponse = postAveloWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1")
                objNSM2.AddNamespace("def", "http://www.origoservices.com")

                objReader.Close()
                objReader = Nothing

                Dim boolSuccess As Boolean = True, strErrorMessage2 As String = ""
                Dim objSuccess As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_wol_quote_response/quo:status", objNSM2)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "Success" And objSuccess.InnerText <> "Warning") Then
                        boolSuccess = False
                        Dim objReason As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_wol_quote_response/quo:reason", objNSM2)
                        If Not (objReason Is Nothing) Then
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found, reason = " & objReason.InnerText
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        Else
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found"
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        End If
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage2 = "Data field not found. Field = status"
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strErrorMessage2, objOutputXMLDoc.InnerXml)
                    postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                    responseWrite("0|Data error. Please contact Engaged Solutions.")
                End If

                If (boolSuccess) Then
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1011, strServiceID & " Successfully Generated", objInputXMLDoc.InnerXml)
                    Dim XMLReceivedID As String = saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 101, strServiceID & " Successfully Retrieved", objOutputXMLDoc.InnerXml)
                    saveNote(AppID, UserID, strNote)
                    responseWrite("1|" & XMLReceivedID)
                End If

            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo XML (Live) Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>Connetion Error", True, "")
                responseWrite("0|Connection error. Please contact Engaged Solutions.")
            End If
            objResponse.Close()
            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub getTermQuote()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/" & strXMLFile))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("def", "http://www.texorigoservices.com")

        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_id", strMessageID)
        'writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:service_id", strServiceID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:user_id", strUserID2)

        Dim strNewICRN As String = AppID & getIndexNumber()

        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:control_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:message_id", strMessageID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_orchestration_id", UserID & "_" & strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:user_id", strUserName2 & " " & strPassword2)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:response_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:profile", strProfile)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:return_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:intermediary_case_reference_number", strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:submission_date", ddmmyyyy2yyyymmdd(Config.DefaultDateTime))

        writeToNode(objInputXMLDoc, "n", "esreq:payload/def:message/def:m_content/def:intermediary", "def:sib_number", strSIBNumber)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:company_name", strCompanyName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:branch_name", strBranchName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:agency_address", "def:postcode", strCompanyPostCode)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:contact_details", "def:name", strCompanyIndividual)

        Dim strSQL As String = "SELECT * FROM " & strView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strNote As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim strCoverType As String = "", strCoverFor As String = "", intAmount As Integer = 0, intProductTerm As Integer = 0, intCICoverAmount As Integer = 0, strWaiverApp1Life As String = "", strWaiverApp2Life As String = ""
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName = "Amount") Then
                            intAmount = Row(Column).ToString
                        ElseIf (Column.ColumnName = "ProductTerm") Then
                            intProductTerm = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyCoverType") Then
                            strCoverType = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyCICoverAmount") Then
                            If (strCoverType = "Life Cover With Critical Illness") Then
                                intCICoverAmount = CInt(Row(Column).ToString)
                                If (intCICoverAmount = 0 Or intCICoverAmount = intAmount) Then
                                    Dim objRiskEvent As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event[1]", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:risk_event", "risk_event")
                                    objRiskEvent.SetAttribute("qualifier", "Core")
                                    writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:risk_event[2]", "Critical Illness")
                                ElseIf (intCICoverAmount <> intAmount) Then
                                    'Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                    Dim objRiskBenefit1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", objNSM)
                                    Dim objRiskCover1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", objNSM)
                                    Dim objRiskCover2 As XmlNode = objRiskCover1.CloneNode(True)
                                    Dim objNewRiskCover As XmlElement = objRiskCover2
                                    'objRiskBenefit.SetAttribute("id", "rb2")
                                    objRiskBenefit1.InsertAfter(objNewRiskCover, objRiskCover1)

                                    objRiskCover2 = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover[2]", objNSM)
                                    Dim objCoverIndexation2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover[2]/def:cover_indexation", objNSM)
                                    If (Not objCoverIndexation2 Is Nothing) Then
                                        objRiskCover2.RemoveChild(objCoverIndexation2)
                                    End If

                                    writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover[2]", "def:risk_event", "Critical Illness")
                                    writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover[2]/def:lump_sum_benefit", "def:amount", intCICoverAmount)
                                    writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover[2]/def:risk_event", "qualifier", "Core")

                                    If (strCoverFor = "Joint Lives") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/", "def:cover_basis", "First Event")
                                        'writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']/", "def:cover_basis", "First Event")
                                    Else
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/", "def:cover_basis", "Single Life")
                                        'writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']/", "def:cover_basis", "Single Life")
                                    End If
                                End If
                            End If
                        ElseIf (Column.ColumnName = "PolicyStartDate") Then
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:policy_term", "def:start_date", ddmmyyyy2yyyymmdd(Row(Column).ToString))
                        ElseIf (Column.ColumnName = "PolicyCoverFor") Then
                            strCoverFor = Row(Column).ToString
                            If (Row(Column).ToString = "Joint Lives") Then
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                                objLifeAssured = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:life_assured[@personal_client_id='pc1']", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc2")
                                objLifeAssured.SetAttribute("sequence_number", "2")
                            ElseIf (Row(Column).ToString = "First Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", objNSM)
                                If (Not objPC2 Is Nothing) Then
                                    objApplication.RemoveChild(objPC2)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", objNSM)
                                If (Not objBenefits2 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits2)
                                End If
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            ElseIf (Row(Column).ToString = "Second Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", objNSM)
                                If (Not objPC1 Is Nothing) Then
                                    objApplication.RemoveChild(objPC1)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[1]", objNSM)
                                If (Not objBenefits1 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits1)
                                End If
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", "id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "personal_client_id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "sequence_number", "1")
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            End If
                        ElseIf (Column.ColumnName = "PolicyCoverPeriodBasis") Then
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_basis", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "def:cover_period", "cover_period")
                            If (Row(Column).ToString = "Exact Term") Then
                                createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period", "def:term", "term")
                                createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term", "def:years", "years")
                                'insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term/def:years", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term", "def:years", "months")
                            ElseIf (Row(Column).ToString = "Exact Age") Then
                                Dim objEndAge = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period", "def:end_age", "end_age")
                                objEndAge.SetAttribute("definition", "Attained Last Whole Year")
                            End If
                        ElseIf (Column.ColumnName = "PolicyQuotationBasis") Then
                            If (Row(Column).ToString = "Sum Assured") Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:life_assured[last()]", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:lump_sum_benefit", "lump_sum_benefit")
                                Dim objAmount As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:lump_sum_benefit", "def:amount", "amount")
                                objAmount.SetAttribute("currency", "GBP")
                            End If
                        ElseIf (Column.ColumnName = "RiskBenefitQualifier") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "qualifier", Row(Column).ToString)
                        ElseIf (Column.ColumnName = "PolicyPremium") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount", "currency", "GBP")
                        ElseIf (Column.ColumnName = "PolicyCoverIndexation") Then
                            If (Row(Column).ToString = "Decreasing - Mortgage Protection") Then
                                writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:service_id", "QuotesMortgageProtection")
                                insertElementBefore(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:increment_ind", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:main_purpose", "main_purpose")
                                writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:main_purpose", "Mortgage Protection")
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:policy_term", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:loan_details", "loan_details")
                                createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details", "def:amount", "amount")
                                writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details", "def:amount", intAmount)
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details/def:amount", "currency", "GBP")
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details/def:amount", "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details", "def:interest_rate", "interest_rate")
                                'writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:loan_details", "def:interest_rate", "7.5")
                                'writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:illustration_basis", "def:assumed_interest_rate", "7.5")
                                Dim objBasis As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:illustration_basis", objNSM)
                                Dim objRequiredYears As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:illustration_basis/def:required_year_value", objNSM)
                                If (Not objRequiredYears Is Nothing) Then
                                    objBasis.RemoveChild(objRequiredYears)
                                End If
                                Dim objCommission1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[@id='CE1']", objNSM)
                                Dim objUplift1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[@id='CE1']/def:uplift", objNSM)
                                If (Not objUplift1 Is Nothing) Then
                                    objCommission1.RemoveChild(objUplift1)
                                End If
                                Dim objCommission2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[@id='CE2']", objNSM)
                                Dim objUplift2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[@id='CE2']/def:uplift", objNSM)
                                If (Not objUplift2 Is Nothing) Then
                                    objCommission2.RemoveChild(objUplift2)
                                End If
                            End If
                        ElseIf (Column.ColumnName = "PolicyIncreasing") Then
                            If (Row(Column).ToString = "Y") Then
                                Dim objBasis As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:illustration_basis", objNSM)
                                Dim objRequiredYears As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:illustration_basis/def:required_year_value", objNSM)
                                If (Not objRequiredYears Is Nothing) Then
                                    objBasis.RemoveChild(objRequiredYears)
                                End If
                            End If
                        ElseIf (Column.ColumnName = "PolicyTPDOptions") Then
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:risk_event", "risk_event")
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:risk_event[2]", "Total And Permanent Disability")
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event[2]", "qualifier", Row(Column).ToString)
                        ElseIf (Column.ColumnName = "PolicyWaiverApp1Life") Then
                            strWaiverApp1Life = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyWaiverApp2Life") Then
                            strWaiverApp2Life = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyWaiver") Then
                            If (Row(Column).ToString = "Y") Then
                                Dim objRiskBenefit2 = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']", objNSM)
                                Dim objWaiver As XmlElement = Nothing
                                If (Not objRiskBenefit2 Is Nothing) Then
                                    objWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb2']", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:risk_benefit", "risk_benefit")
                                    objWaiver.SetAttribute("id", "rb3")
                                    Dim objRiskWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution/def:payment_indexation", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution", "def:waiver", "waiver")
                                    objRiskWaiver.SetAttribute("risk_benefit_id", "rb3")
                                Else
                                    objWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:risk_benefit", "risk_benefit")
                                    objWaiver.SetAttribute("id", "rb2")
                                    Dim objRiskWaiver = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution/def:payment_indexation", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()-1]/def:risk_contribution", "def:waiver", "waiver")
                                    objRiskWaiver.SetAttribute("risk_benefit_id", "rb2")
                                End If
                                If (Not objWaiver Is Nothing) Then
                                    objWaiver.SetAttribute("type", "Waiver")
                                    If (strWaiverApp1Life = "Disability" And strWaiverApp2Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp1Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife1 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife1.SetAttribute("personal_client_id", "pc1")
                                        objLife1.SetAttribute("sequence_number", "1")
                                        Dim objLife2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:life_assured[1]", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife2.SetAttribute("personal_client_id", "pc2")
                                        objLife2.SetAttribute("sequence_number", "2")
                                    ElseIf (strWaiverApp1Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp1Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife1 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife1.SetAttribute("personal_client_id", "pc1")
                                        objLife1.SetAttribute("sequence_number", "1")
                                    ElseIf (strWaiverApp2Life = "Disability") Then
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "cover_basis", "Single Life")
                                        createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_cover", "risk_cover")
                                        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "risk_event", strWaiverApp2Life)
                                        writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "qualifier", "Own Occupation")
                                        Dim objLife2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_cover", "def:life_assured", "life_assured")
                                        objLife2.SetAttribute("personal_client_id", "pc2")
                                        objLife2.SetAttribute("sequence_number", "2")
                                    End If
                                    createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]", "def:risk_contribution", "risk_contribution")
                                    writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[last()]/def:risk_contribution", "cost_basis", "Funded From Waived Risk Contribution")
                                End If
                            End If
                        ElseIf (Column.ColumnName = "PolicyCIBuyBack") Then
                            If (strCoverType <> "Life Cover Only" And Row(Column).ToString = "Y") Then
                                If (strCoverType = "Life Cover With Critical Illness") Then
                                    If (intCICoverAmount = 0) Then
                                        Dim objRBOption As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "def:risk_benefit_option", "risk_benefit_option")
                                        objRBOption.SetAttribute("type", "Critical Illness Reinstatement")
                                    ElseIf (intCICoverAmount <> intAmount) Then
                                        Dim objRBOption As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "def:risk_benefit_option", "risk_benefit_option")
                                        objRBOption.SetAttribute("type", "Critical Illness Reinstatement")
                                    End If
                                Else
                                    Dim objRBOption As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "def:risk_benefit_option", "risk_benefit_option")
                                    objRBOption.SetAttribute("type", "Critical Illness Reinstatement")
                                End If
                            End If
                        ElseIf (Column.ColumnName = "App2Title") Then
                            Dim objApp2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", "esreq:payload/def:message/def:m_content/def:application", "def:personal_client[@id='pc2']", "personal_client")
                            objApp2.SetAttribute("id", "pc2")
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:benefits_required", "benefits_required")
                            Dim objLife2 As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", "def:life_assured", "life_assured")
                            objLife2.SetAttribute("personal_client_id", "pc2")
                            objLife2.SetAttribute("sequence_number", "2")
                            'ElseIf (Column.ColumnName = "App1OccupationCode") Then
                            'writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']/def:employment_contract/def:occupation", "code", Row(Column).ToString)
                            'ElseIf (Column.ColumnName = "App2OccupationCode") Then
                            'insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:tax_rate", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", "def:employment_contract", "employment_contract")
                            'Dim objOccupation As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:employment_contract", "def:occupation", "occupation")
                            'objOccupation.SetAttribute("code", Row(Column).ToString)
                        ElseIf (Column.ColumnName = "App2StateRetirementAge") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:state_retirement_age", "definition", "Attained Years And Months")
                        ElseIf (Column.ColumnName = "PolicyCommissionSacrifice") Then
                            insertElementBefore(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:indemnity_ind", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]", "def:sacrifice", "sacrifice")
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:sacrifice", "def:percent", Row(Column).ToString)
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:sacrifice/def:percent", "calculation_basis", "Entitlement")
                        ElseIf (Column.ColumnName = "ReviewableRates") Then
                            If (Row(Column).ToString = "Yes") Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[1]", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:product_option", "product_option")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[2]", "type", "Reviewable Rates")
                            End If
                        ElseIf (Column.ColumnName = "Note") Then
                            strNote = Row(Column).ToString
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            'responseWrite(arrName(UBound(arrName)) & "=" & Row(Column) & "<br>")
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "esr:p", "esreq:payload"), "def:" & arrName(UBound(arrName)), Row(Column).ToString)

                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objResponse As HttpWebResponse = postAveloWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1")
                objNSM2.AddNamespace("def", "http://www.origoservices.com")

                objReader.Close()
                objReader = Nothing

                Dim boolSuccess As Boolean = True, strErrorMessage2 As String = ""
                Dim objSuccess As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_term_quote_response/quo:status", objNSM2)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "Success" And objSuccess.InnerText <> "Warning") Then
                        boolSuccess = False
                        Dim objReason As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_term_quote_response/quo:reason", objNSM2)
                        If Not (objReason Is Nothing) Then
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found, reason = " & objReason.InnerText
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        Else
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found"
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        End If
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage2 = "Data field not found. Field = status"
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strErrorMessage2, objOutputXMLDoc.InnerXml)
                    postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                    responseWrite("0|Data error. Please contact Engaged Solutions.")
                End If

                If (boolSuccess) Then
                    saveNote(AppID, UserID, strNote)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1021, strServiceID & " Successfully Generated", objInputXMLDoc.InnerXml)
                    Dim XMLReceivedID As String = saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 102, strServiceID & " Successfully Retrieved", objOutputXMLDoc.InnerXml)
                    responseWrite("1|" & XMLReceivedID)
                End If

            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>Connetion Error", True, "")
                responseWrite("0|Connection error. Please contact Engaged Solutions.")
            End If
            objResponse.Close()
            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub getIPQuote()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/" & strXMLFile))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("def", "http://www.texorigoservices.com")

        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:message_id", strMessageID)
        'writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:service_id", strServiceID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc, "y", "esreq:header", "esreq:user_id", strUserID2)

        Dim strNewICRN As String = AppID & getIndexNumber()

        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:control_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:message_id", strMessageID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:initiator_orchestration_id", UserID & "_" & strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:user_id", strUserName2 & " " & strPassword2)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control", "def:response_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:profile", strProfile)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_control/def:browser", "def:return_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:intermediary_case_reference_number", strNewICRN)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:b_control", "def:submission_date", ddmmyyyy2yyyymmdd(Config.DefaultDateTime))

        writeToNode(objInputXMLDoc, "n", "esreq:payload/def:message/def:m_content/def:intermediary", "def:sib_number", strSIBNumber)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:company_name", strCompanyName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary", "def:branch_name", strBranchName)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:agency_address", "def:postcode", strCompanyPostCode)
        writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:intermediary/def:contact_details", "def:name", strCompanyIndividual)

        Dim strSQL As String = "SELECT * FROM " & strView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strNote As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim strCoverType As String = "", intAmount As Integer = 0, intProductTerm As Integer = 0, intCICoverAmount As Integer = 0, strWaiverApp1Life As String = "", strWaiverApp2Life As String = ""
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName = "Amount") Then
                            intAmount = Row(Column).ToString
                        ElseIf (Column.ColumnName = "ProductTerm") Then
                            intProductTerm = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyCoverType") Then
                            strCoverType = Row(Column).ToString
                        ElseIf (Column.ColumnName = "PolicyStartDate") Then
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:policy_term", "def:start_date", ddmmyyyy2yyyymmdd(Row(Column).ToString))
                        ElseIf (Column.ColumnName = "PolicyCoverFor") Then
                            If (Row(Column).ToString = "First Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", objNSM)
                                If (Not objPC2 Is Nothing) Then
                                    objApplication.RemoveChild(objPC2)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits2 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", objNSM)
                                If (Not objBenefits2 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits2)
                                End If
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            ElseIf (Row(Column).ToString = "Second Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application", objNSM)
                                Dim objPC1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", objNSM)
                                If (Not objPC1 Is Nothing) Then
                                    objApplication.RemoveChild(objPC1)
                                End If
                                Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product", objNSM)
                                Dim objBenefits1 As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[1]", objNSM)
                                If (Not objBenefits1 Is Nothing) Then
                                    objProduct.RemoveChild(objBenefits1)
                                End If
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", "id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "personal_client_id", "pc1")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required/def:life_assured", "sequence_number", "1")
                                Dim objLifeAssured As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:risk_event", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover", "def:life_assured", "life_assured")
                                objLifeAssured.SetAttribute("personal_client_id", "pc1")
                                objLifeAssured.SetAttribute("sequence_number", "1")
                            End If
                        ElseIf (Column.ColumnName = "PolicyCoverPeriodBasis") Then
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_basis", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']", "def:cover_period", "cover_period")
                            If (Row(Column).ToString = "Exact Term") Then
                                createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period", "def:term", "term")
                                createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term", "def:years", "years")
                                'insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term/def:years", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period/def:term", "def:years", "months")
                            ElseIf (Row(Column).ToString = "Exact Age") Then
                                Dim objEndAge = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:cover_period", "def:end_age", "end_age")
                                objEndAge.SetAttribute("definition", "Attained Last Whole Year")
                            End If
                        ElseIf (Column.ColumnName = "PolicyMonthlyBenefit") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:amount", "currency", "GBP")
                        ElseIf (Column.ColumnName = "PolicyPremium") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount", "currency", "GBP")
                        ElseIf (Column.ColumnName = "ExcludeBudgetIP") Then
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:frequency", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit", "def:payment_period", "payment_period")
                            createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:payment_period", "def:term", "term")
                            createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:payment_period/def:term", "def:months", "months")
                            createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:tpsdata", "def:benefit_payable_to_end_contract_ind[2]", "benefit_payable_to_end_contract_ind")
                        ElseIf (Column.ColumnName = "PolicyDeferredType") Then
                            Dim objPayPeriod As XmlNode = objInputXMLDoc.SelectSingleNode("//esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:payment_period", objNSM)
                            If (Not objPayPeriod Is Nothing) Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:payment_period", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit", "def:deferred_period", "deferred_period")
                            Else
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit/def:frequency", "esreq:payload/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_cover/def:income_benefit", "def:deferred_period", "deferred_period")
                            End If
                        ElseIf (Column.ColumnName = "App2Title") Then
                            Dim objApp2 As XmlElement = insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']", "esreq:payload/def:message/def:m_content/def:application", "def:personal_client[@id='pc2']", "personal_client")
                            objApp2.SetAttribute("id", "pc2")
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:benefits_required", "benefits_required")
                            Dim objLife2 As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:benefits_required[2]", "def:life_assured", "life_assured")
                            objLife2.SetAttribute("personal_client_id", "pc2")
                            objLife2.SetAttribute("sequence_number", "2")
                        ElseIf (Column.ColumnName = "App1OccupationCode") Then
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc1']/def:employment_contract/def:occupation", "code", Row(Column).ToString)
                        ElseIf (Column.ColumnName = "App2OccupationCode") Then
                            insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:tax_rate", "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']", "def:employment_contract", "employment_contract")
                            Dim objOccupation As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:employment_contract", "def:occupation", "occupation")
                            objOccupation.SetAttribute("code", Row(Column).ToString)
                            Dim objEarnings As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:employment_contract", "def:earnings", "earnings")
                            objEarnings.SetAttribute("type", "Basic Annual Salary")
                            Dim objAmount As XmlElement = createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:employment_contract/def:earnings", "def:amount", "amount")
                            objAmount.SetAttribute("currency", "GBP")
                            createElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:personal_client[@id='pc2']/def:employment_contract/def:earnings", "def:frequency", "frequency")
                        ElseIf (Column.ColumnName = "PolicyCommissionSacrifice") Then
                            insertElementBefore(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:indemnity_ind", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]", "def:sacrifice", "sacrifice")
                            writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:sacrifice", "def:percent", Row(Column).ToString)
                            writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:commission_entitlement[1]/def:sacrifice/def:percent", "calculation_basis", "Entitlement")
                        ElseIf (Column.ColumnName = "ReviewableRates") Then
                            If (Row(Column).ToString = "Yes") Then
                                insertElement(objInputXMLDoc, "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[1]", "esreq:payload/def:message/def:m_content/def:application/def:product", "def:product_option", "product_option")
                                writeAttribute(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[2]", "type", "Reviewable Rates")
                                writeToNode(objInputXMLDoc, "y", "esreq:payload/def:message/def:m_content/def:application/def:product/def:product_option[2]", "def:selected_ind", "Yes")
                            End If
                        ElseIf (Column.ColumnName = "Note") Then
                            strNote = Row(Column).ToString
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            'responseWrite(arrName(UBound(arrName)) & "=" & Row(Column) & "<br>")
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & Replace(Replace(arrName(UBound(arrName)), "(", "["), ")", "]"), Row(Column).ToString)

                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objResponse As HttpWebResponse = postAveloWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1")
                objNSM2.AddNamespace("def", "http://www.origoservices.com")

                objReader.Close()
                objReader = Nothing

                Dim boolSuccess As Boolean = True, strErrorMessage2 As String = ""
                Dim objSuccess As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_ip_quote_response/quo:status", objNSM2)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "Success" And objSuccess.InnerText <> "Warning") Then
                        boolSuccess = False
                        Dim objReason As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:get_ip_quote_response/quo:reason", objNSM2)
                        If Not (objReason Is Nothing) Then
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found, reason = " & objReason.InnerText
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        Else
                            boolSuccess = False
                            strErrorMessage2 = "Inconsistent data found"
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc.InnerXml)
                            postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                            responseWrite("0|Inconsistent data found. Please contact Engaged Solutions.")
                        End If
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage2 = "Data field not found. Field = status"
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strErrorMessage2, objOutputXMLDoc.InnerXml)
                    postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>" & strErrorMessage2, True, "")
                    responseWrite("0|Data error. Please contact Engaged Solutions.")
                End If

                If (boolSuccess) Then
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1031, strServiceID & " Successfully Generated", objInputXMLDoc.InnerXml)
                    Dim XMLReceivedID As String = saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 103, strServiceID & " Successfully Retrieved", objOutputXMLDoc.InnerXml)
                    saveNote(AppID, UserID, strNote)
                    responseWrite("1|" & XMLReceivedID)
                End If

            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strServiceID & " Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                postEmail("", "itsupport@engaged-solutions.co.uk", "Avelo (Live) XML Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>ServiceID: " & strServiceID & "<br>Connetion Error", True, "")
                responseWrite("0|Connection error. Please contact Engaged Solutions.")
            End If
            objResponse.Close()
            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub getNewBusiness()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/nb_blank.xml"))

        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("def", "http://www.origoservices.com")
        objNSM.AddNamespace("con", "http://www.exchange.co.uk")
        objNSM.AddNamespace("app", "http://www.exchange.co.uk")

        Dim strNewICRN As String = AppID & getIndexNumber()

        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:control_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:message_id", strMessageID)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:initiator_id", strIntegratorID)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:initiator_orchestration_id", UserID & "_" & strNewICRN)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:user_id", strUserName2 & " " & strPassword2)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control", "def:response_location", strResponseLocation)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control/def:browser", "def:profile", strProfile)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_control/def:browser", "def:return_location", strResponseLocation)

        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/con:b_control", "con:service_provider_reference_number", QuoteID)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/con:b_control", "con:intermediary_case_reference_number", strNewICRN)

        writeToNode(objInputXMLDoc, "n", "def:message/def:m_content/app:application/app:intermediary", "app:sib_number", strSIBNumber)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:intermediary", "app:company_name", strCompanyName)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:intermediary", "app:branch_name", strBranchName)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:intermediary/app:agency_address", "app:postcode", strCompanyPostCode)
        writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:intermediary/app:registered_individual", "app:name", strCompanyIndividual)

        Dim strSQL As String = "SELECT * FROM vwxmlavelonb WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim strCoverType As String = "", intAmount As Integer = 0, intProductTerm As Integer = 0, intCICoverAmount As Integer = 0, strWaiverApp1Life As String = "", strWaiverApp2Life As String = ""
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName = "App1Title") Then
                            writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]", "id", "0x" & Replace(System.Guid.NewGuid.ToString, "-", ""))
                            writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]", "type", "Life_1")
                        ElseIf (Column.ColumnName = "App2Title") Then
                            Dim objApp2 As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[1]", "def:message/def:m_content/app:application", "app:personal_client[2]", "personal_client", "app")
                            objApp2.SetAttribute("id", "0x" & Replace(System.Guid.NewGuid.ToString, "-", ""))
                            objApp2.SetAttribute("type", "Life_2")
                        ElseIf (Column.ColumnName = "App1FullAddressLine1") Then
                            Dim objMaidenName As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:personal_client[1]/app:maiden_surname", objNSM)
                            Dim objAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[1]/app:smoker_ind", "def:message/def:m_content/app:application/app:personal_client[1]", "app:home_address", "home_address", "app")
                            objAddress.SetAttribute("address_id", "a1")
                            writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]/app:home_address", "use_for_correspondence", "true", "app")
                            Dim objCorrespondenceAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[1]/app:home_address", "def:message/def:m_content/app:application/app:personal_client[1]", "app:correspondence_address", "correspondence_address", "app")
                            objCorrespondenceAddress.SetAttribute("address_id", "a1")
                        ElseIf (Column.ColumnName = "App2FullAddressLine1") Then
                            Dim objPartnerAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:address[1]", "def:message/def:m_content/app:application", "app:address", "address", "app")
                            objPartnerAddress.SetAttribute("id", "a2")
                            Dim objAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[2]/app:smoker_ind", "def:message/def:m_content/app:application/app:personal_client[2]", "app:home_address", "home_address", "app")
                            objAddress.SetAttribute("address_id", "a2")
                            writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[2]/app:home_address", "use_for_correspondence", "true", "app")
                            Dim objCorrespondenceAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[2]/app:home_address", "def:message/def:m_content/app:application/app:personal_client[2]", "app:correspondence_address", "correspondence_address", "app")
                            objCorrespondenceAddress.SetAttribute("address_id", "a2")
                        ElseIf (Column.ColumnName = "PolicyCoverFor") Then
                            If (Row(Column).ToString = "First Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application", objNSM)
                                Dim objPC2 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:personal_client[2]", objNSM)
                                If (Not objPC2 Is Nothing) Then
                                    objApplication.RemoveChild(objPC2)
                                End If
                                Dim objAddress2 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:address[2]", objNSM)
                                If (Not objAddress2 Is Nothing) Then
                                    objApplication.RemoveChild(objAddress2)
                                End If
                            ElseIf (Row(Column).ToString = "Second Life") Then
                                Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application", objNSM)
                                Dim objPC1 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:personal_client[1]", objNSM)
                                If (Not objPC1 Is Nothing) Then
                                    objApplication.RemoveChild(objPC1)
                                End If
                                writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]", "type", "Life_1")
                                Dim objAddress1 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:address[1]", objNSM)
                                Dim objAddress2 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:address[2]", objNSM)
                                If (Not objAddress1 Is Nothing And Not objAddress2 Is Nothing) Then
                                    objApplication.RemoveChild(objAddress1)
                                    writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:address[1]", "id", "a1")
                                    writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]/app:home_address", "address_id", "a1")
                                    writeAttribute(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[1]/app:correspondence_address", "address_id", "a1")
                                End If
                            ElseIf (Row(Column).ToString = "Joint Lives") Then
                                Dim objAddress2 As XmlNode = objInputXMLDoc.SelectSingleNode("//def:message/def:m_content/app:application/app:address[2]", objNSM)
                                If (objAddress2 Is Nothing) Then
                                    Dim objAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[2]/app:smoker_ind", "def:message/def:m_content/app:application/app:personal_client[2]", "app:home_address", "home_address", "app")
                                    objAddress.SetAttribute("address_id", "a1")
                                    writeToNode(objInputXMLDoc, "y", "def:message/def:m_content/app:application/app:personal_client[2]/app:home_address", "use_for_correspondence", "true", "app")
                                    Dim objCorrespondenceAddress As XmlElement = insertElement(objInputXMLDoc, "def:message/def:m_content/app:application/app:personal_client[2]/app:home_address", "def:message/def:m_content/app:application/app:personal_client[2]", "app:correspondence_address", "correspondence_address", "app")
                                    objCorrespondenceAddress.SetAttribute("address_id", "a1")
                                End If
                            End If
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (InStr(arrName(y), "app:") = 0) Then
                                        If (y = 0) Then
                                            strParent += "def:" & arrName(y)
                                        Else
                                            strParent += "/def:" & arrName(y)
                                        End If
                                    Else
                                        If (y = 0) Then
                                            strParent += arrName(y)
                                        Else
                                            strParent += "/" & arrName(y)
                                        End If
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            'responseWrite(arrName(UBound(arrName)) & "=" & Row(Column) & "<br>")
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "app:" & arrName(UBound(arrName)), Row(Column).ToString, "app")

                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Save provider, policy no., commission
            Dim arrQuoteID As Array = Split(QuoteID, "_")
            Dim strQuoteType As String = getAnyFieldFromDataStore("PolicyQuotationType", AppID), strCoverType As String = getAnyFieldFromDataStore("PolicyCoverType", AppID), strRootNode As String = "", strSchema As String = ""
            Dim strPrintType As String = "", strProvider As String = "", strProductName As String = "", strPolicyNumber As String = "", strPayment As String = "", strCommission As String = "", strSumAssured As String = "", strPremium As String = "", strTerm As String = "", strNote As String = ""
            Select Case strQuoteType
                Case "Whole Life Protection"
                    strRootNode = "get_wol_quote_response"
                    strSchema = "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1"
                Case "Term Protection"
                    strRootNode = "get_term_quote_response"
                    strSchema = "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1"
                Case "Income Protection"
                    strRootNode = "get_ip_quote_response"
                    strSchema = "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1"
            End Select
            Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
            objOutputXMLDoc.XmlResolver = Nothing
            objOutputXMLDoc.LoadXml(strXML)
            objNSM = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
            objNSM.AddNamespace("quo", strSchema)
            objNSM.AddNamespace("def", "http://www.origoservices.com")

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            Dim objQuote As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[@quotesequence='" & arrQuoteID(4) & "']", objNSM)
            If (Not objQuote Is Nothing) Then
                strPrintType = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:print_types_available")
                strProvider = getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID")
                updateDataStoreField(AppID, "PolicyLenderName", strProvider, "T", "")
                strPolicyNumber = strICRN
                updateDataStoreField(AppID, "PolicyNumber", strPolicyNumber, "", "")
                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
                updateDataStoreField(AppID, "PolicyMonthlyPayment", strPayment, "M", 0.0)
                strCommission = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
                updateDataStoreField(AppID, "PolicyCommission", strCommission, "M", 0.0)
                updateDataStoreField(AppID, "PolicyAdjustmentCommission", strCommission, "M", 0.0)
                strNote = "Customer accepted policy no. " & strPolicyNumber & " with " & strProvider & " (" & FormatCurrency(strPayment) & " monthly payment, " & FormatCurrency(strCommission) & " commission)"

                'responseWrite(strProvider & " " & strPolicyNumber & " " & strPayment & " " & strCommission)
                'responseEnd()

                'strProductName = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
                'strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
                'strPremium = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_contribution/def:contribution/def:amount")
                'strTerm = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
                'strNote += "Policy arranged: " & strCoverType & " with " & strProvider & " - " & strProductName & " Ref No: " & strPolicyNumber & ") for "
                'If (checkValue(strSumAssured)) Then
                '    'strNote += displayCurrency(CDec(strSumAssured))
                'ElseIf (checkValue(strPremium)) Then
                '    'strNote += displayCurrency(CDec(strPremium))
                'End If
                'strNote += " over " & strTerm & " months at " & displayCurrency(CDec(strPayment)) & " per month (Commission: " & displayCurrency(CDec(strCommission)) & ")"
                'saveNote(AppID, UserID, strNote)
            End If
            ' Save documentation
            If (arrQuoteID.Length = 5) Then
                Dim strKFIRef As String = arrQuoteID(0) & "_" & arrQuoteID(1) & "_" & arrQuoteID(2) & "_" & arrQuoteID(3)
                Dim strComparisonRef As String = arrQuoteID(0)
                'getQuoteDocuments("Comparison", strComparisonRef, strPrintType, False)
                'getQuoteDocuments("KFI", strKFIRef, strPrintType, False)
                'getDocumentLibrary(strCoverType, False)
                'getAssociatedDocuments(strCoverType, False)
                'saveNote(AppID, UserID, "Policy documentation was saved for this case.")
            End If

            saveNote(AppID, UserID, strNote)
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 106, "ApplicationLaunch Successfully Retrieved", objInputXMLDoc.InnerXml)
            HttpContext.Current.Response.Write("<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""><html xmlns=""http://www.w3.org/1999/xhtml"">")
            HttpContext.Current.Response.Write("<head>")
            HttpContext.Current.Response.Write("<script id=""scrjQuery"" type=""text/javascript"" src=""/js/jquery-1.7.2.min.js""></script>")
            HttpContext.Current.Response.Write("<script id=""scrGeneral"" type=""text/javascript"" src=""/js/general.js""></script>")
            HttpContext.Current.Response.Write("<link id=""cssMain"" href=""/css/slate.css?"" rel=""stylesheet"" type=""text/css"" media=""screen"" />")
            HttpContext.Current.Response.Write("</head>")
            HttpContext.Current.Response.Write("<body class=""no-bg"">")
            HttpContext.Current.Response.Write("<div class=""loading-overlay""><div class=""progress progress-primary progress-striped active"" style=""margin-bottom: 2em;""><div class=""bar"" style=""width: 100%"">Sending Data</div></div></div>")
            HttpContext.Current.Response.Write("<form id=""frmExchange"" name=""frmExchange"" action=""https://exweb.exchange.uk.com/portal/tpinewbusiness/nblaunchapplication.aspx"" method=""POST"" target=""_blank"">")
            HttpContext.Current.Response.Write("<input type=""hidden"" id=""XMLDATA"" name=""XMLDATA"" value=""" & encodeURL(objInputXMLDoc.InnerXml) & """/>")
            HttpContext.Current.Response.Write("</form>")
            HttpContext.Current.Response.Write("<script type=""text/javascript"">")
            HttpContext.Current.Response.Write("$(document).ready(function() {")
            HttpContext.Current.Response.Write("    document.frmExchange.submit();")
            HttpContext.Current.Response.Write("    top.$('#modal-iframe').modal('hide');")
            HttpContext.Current.Response.Write("    top.location.href = '/processing.aspx?AppID=" & AppID & "&strMessage=' + escape('Please continue to work this case in the provider’s system.') + '&strMessageTitle=Continue Case&strMessageType=success';")
            HttpContext.Current.Response.Write("});")
            HttpContext.Current.Response.Write("</script>")
            HttpContext.Current.Response.Write("</body>")
            HttpContext.Current.Response.Write("</html>")
            HttpContext.Current.Response.End()

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing
    End Sub

    Private Sub getDocuments(save As Boolean, view As Boolean)
        Dim arrQuoteID As Array = Split(QuoteID, "_")
        Dim strComparisonRef As String = "", strKFIRef As String = "", strPrintType As String = ""
        Dim strQuoteType As String = getAnyFieldFromDataStore("PolicyQuotationType", AppID), strRootNode As String = "", strSchema As String = ""
        Select Case strQuoteType
            Case "Whole Life Protection"
                strRootNode = "get_wol_quote_response"
                strSchema = "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1"
            Case "Term Protection"
                strRootNode = "get_term_quote_response"
                strSchema = "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1"
            Case "Income Protection"
                strRootNode = "get_ip_quote_response"
                strSchema = "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1"
        End Select
        Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
        objOutputXMLDoc.XmlResolver = Nothing
        objOutputXMLDoc.LoadXml(strXML)
        objNSM = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
        objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
        objNSM.AddNamespace("quo", strSchema)
        objNSM.AddNamespace("def", "http://www.origoservices.com")

        Dim objQuote As XmlNode = objOutputXMLDoc.SelectSingleNode("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[@quotesequence='" & arrQuoteID(4) & "']", objNSM)
        strPrintType = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:print_types_available")

        ' View/Save documentation
        If (save) Then
            If (arrQuoteID.Length = 5) Then
                strKFIRef = arrQuoteID(0) & "_" & arrQuoteID(1) & "_" & arrQuoteID(2) & "_" & arrQuoteID(3)
                strComparisonRef = arrQuoteID(0)
                getQuoteDocuments("Comparison", strComparisonRef, "", False)
                getQuoteDocuments("KFI", strKFIRef, strPrintType, False)
                'getDocumentLibrary(strCoverType, False)
                'getAssociatedDocuments(strCoverType, False)
                'saveNote(AppID, UserID, "Policy documentation was saved for this case.")
            End If
        ElseIf (view) Then
            If (arrQuoteID.Length = 5) Then
                strKFIRef = arrQuoteID(0) & "_" & arrQuoteID(1) & "_" & arrQuoteID(2) & "_" & arrQuoteID(3)
                strComparisonRef = arrQuoteID(0)
                getQuoteDocuments("Comparison", strComparisonRef, "", True)
                getQuoteDocuments("KFI", strKFIRef, strPrintType, True)
                'getDocumentLibrary(strCoverType, True)
                'getAssociatedDocuments(strCoverType, True)
            End If
        End If
    End Sub

    Private Sub getQuoteDocuments(typ As String, ref As String, print As String, Optional view As Boolean = False)

        Dim objInputXMLDoc3 As XmlDocument = New XmlDocument, objOutputXMLDoc3 = New XmlDocument
        Dim objStringWriter As StringWriter = New StringWriter

        objInputXMLDoc3.XmlResolver = Nothing
        Select Case typ
            Case "Comparison"
                objInputXMLDoc3.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/doc_comparison.xml"))
            Case "KFI"
                objInputXMLDoc3.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/doc_kfi.xml"))
        End Select

        objNSM = New XmlNamespaceManager(objInputXMLDoc3.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("rdreq", "http://www.exchange.co.uk/schema/RetrieveDocumentationRequest/v1")
        Dim objNSM2 As XmlNamespaceManager = Nothing

        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:message_id", strMessageID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:orchestration_id", System.Guid.NewGuid.ToString)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:user_id", strUserID2)

        writeToNode(objInputXMLDoc3, "y", "esreq:payload/rdreq:retrieve_documentation_request", "rdreq:document_reference", ref)
        If (typ = "KFI") Then
            writeToNode(objInputXMLDoc3, "y", "esreq:payload/rdreq:retrieve_documentation_request", "rdreq:kfi_type", getKFIType(print))
        End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc3.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postAveloWebRequest("https://services.exchange.uk.com/exchangeserviceslistener/exchangeserviceslistener.aspx?service_id=GetDocumentationV1", objInputXMLDoc3.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc3.LoadXml(objReader.ReadToEnd())

                objReader.Close()
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc3.InnerXml)
                'HttpContext.Current.Response.End()

                objNSM2 = New XmlNamespaceManager(objOutputXMLDoc3.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("rdresp", "http://www.exchange.co.uk/schema/RetrieveDocumentationResponse/v1")

                With objStringWriter
                    .WriteLine("<tr class=""tablehead"">")
                    .WriteLine("<td class=""sml"" colspan=""3""><h5>" & typ & " Documents</h5></td>")
                    .WriteLine("</tr>")
                End With

                Dim objLibrary As XmlNodeList = objOutputXMLDoc3.SelectNodes("//rdresp:retrieve_documentation_response/rdresp:documentation/rdresp:document", objNSM2)
                If (objLibrary.Count > 0) Then
                    Dim strDocumentTitle As String = "", strDocumentBytes As String = "", strDocumentURL As String = "", strClass As String = "row1"
                    For Each objDocument In objLibrary
                        strDocumentTitle = getNodeText(objDocument, objNSM2, "rdresp:document_title")
                        strDocumentBytes = getNodeText(objDocument, objNSM2, "rdresp:document_encoded")
                        Dim objPDF As PDF = New PDF(Cache, AppID, 0, "", 0, 0)
                        If (checkValue(strDocumentBytes)) Then
                            If (view) Then
                                strDocumentURL = objPDF.generateFromBytes(strDocumentBytes, UserID)
                            Else
                                objPDF.generateFromBytes(strDocumentBytes, UserID, strDocumentTitle)
                            End If
                        End If
                        If (view) Then
                            With objStringWriter
								strDocumentURL = Replace(strDocumentURL,"F:\data\attachments","")
                                .WriteLine("<tr class=""" & strClass & """>")
                                .WriteLine("<td class=""smlc""><span class=""label label-info"">pdf</span></td>")
                                .WriteLine("<td class=""sml"">" & strDocumentTitle & "</td>")
                                If (checkValue(strDocumentURL)) Then
                                    .WriteLine("<td class=""smlc""><a href=""#"" onClick=""top.$.lightbox('/prompts/document.aspx?lightbox[iframe]=true&lightbox[width]=1024&lightbox[height]=768&frmFileName=" & strDocumentURL & "');"" title=""View Document""><button class=""btn btn-primary btn-mini"" title=""Back"">Open</button></a></td>")
                                Else
                                    .WriteLine("<td class=""smlc""><span style=""color:red;font-weight:bold;"">Error: document is unavailable</span></td>")
                                End If
                                .WriteLine("</tr>")
                            End With
                        End If
                        strClass = nextClass(strClass)
                    Next
                Else
                    If (view) Then
                        With objStringWriter
                            .WriteLine("<tr class=""row1"">")
                            .WriteLine("<td colspan=""3"" class=""sml"">No documents are available.</td>")
                            .WriteLine("</tr>")
                        End With
                    End If
                End If
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 105, "GetDocumentationV1 Successfully Generated", objInputXMLDoc3.InnerXml)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 105, "GetDocumentationV1 Successfully Retrieved", objOutputXMLDoc3.InnerXml)

            End If
            objResponse.Close()
            objResponse = Nothing
        End If

        objInputXMLDoc3 = Nothing
        objOutputXMLDoc3 = Nothing
        If (view) Then
            responseWrite(objStringWriter.ToString)
        End If

    End Sub

    Private Sub getDocumentLibrary(quote As String, Optional view As Boolean = False)

        Dim objInputXMLDoc2 As XmlDocument = New XmlDocument, objOutputXMLDoc2 = New XmlDocument
        Dim objStringWriter As StringWriter = New StringWriter

        objInputXMLDoc2.XmlResolver = Nothing
        objInputXMLDoc2.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/doc_library.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc2.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("pdllreq", "http://www.exchange.co.uk/schema/ProvideDocumentLibraryListRequest/v1")
        Dim objNSM2 As XmlNamespaceManager = Nothing

        writeToNode(objInputXMLDoc2, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc2, "y", "esreq:header", "esreq:message_id", strMessageID)
        writeToNode(objInputXMLDoc2, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc2, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc2, "y", "esreq:header", "esreq:user_id", strUserID2)

        Dim arrReference As Array = Split(QuoteID, "_")
        Dim strProviderCode As String = ""
        Dim strProductCode As String = ""
        If (arrReference.Length = 5) Then
            strProviderCode = arrReference(1)
            strProductCode = arrReference(2)
        End If

        If (quote = "Income Protection") Then
            writeToNode(objInputXMLDoc2, "n", "esreq:payload/pdllreq:provide_document_library_list_request", "pdllreq:product_category", getProductCategory(quote))
        Else
            writeToNode(objInputXMLDoc2, "n", "esreq:payload/pdllreq:provide_document_library_list_request", "pdllreq:product_type", getProductType(quote))
        End If
        writeToNode(objInputXMLDoc2, "n", "esreq:payload/pdllreq:provide_document_library_list_request", "pdllreq:provider_code", strProviderCode)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc2.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postAveloWebRequest("https://services.exchange.uk.com/exchangeserviceslistener/exchangeserviceslistener.aspx?service_id=ProvideDocumentLibraryListV1", objInputXMLDoc2.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc2.LoadXml(objReader.ReadToEnd())

                objReader.Close()
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc2.InnerXml)
                'HttpContext.Current.Response.End()

                objNSM2 = New XmlNamespaceManager(objOutputXMLDoc2.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("pdllresp", "http://www.exchange.co.uk/schema/ProvideDocumentLibraryListResponse/v1")

                Dim boolSuccess As Boolean = True, strErrorMessage2 As String = ""

                Dim objSuccess As XmlNode = objOutputXMLDoc2.SelectSingleNode("//pdllresp:provide_document_library_list_response/pdllresp:status", objNSM2)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "true") Then
                        boolSuccess = False
                        Dim objReason As XmlNode = objOutputXMLDoc2.SelectSingleNode("//pdllresp:provide_document_library_list_response/pdllresp:reason", objNSM2)
                        If Not (objReason Is Nothing) Then
                            strErrorMessage2 = "Inconsistent data found, reason = " & objReason.InnerText
                        Else
                            strErrorMessage2 = "Inconsistent data found"
                        End If
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc2.InnerXml)
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage2 = "Data field not found. Field = status"
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strErrorMessage2, objOutputXMLDoc2.InnerXml)
                End If

                With objStringWriter
                    .WriteLine("<tr class=""tablehead"">")
                    .WriteLine("<td class=""sml"" colspan=""3""><strong>Document Library</strong></td>")
                    .WriteLine("</tr>")
                End With

                If (boolSuccess) Then
                    Dim objLibrary As XmlNodeList = objOutputXMLDoc2.SelectNodes("//pdllresp:provide_document_library_list_response/pdllresp:document_list/pdllresp:document", objNSM2)
                    If (objLibrary.Count > 0) Then
                        Dim strDocumentTitle As String = "", strDocumentURL As String = "", strClass As String = "row1"
                        For Each objDocument In objLibrary
                            strDocumentTitle = getNodeText(objDocument, objNSM2, "pdllresp:document_title")
                            strDocumentURL = getNodeText(objDocument, objNSM2, "pdllresp:document_url")
                            If (view) Then
                                With objStringWriter
                                    .WriteLine("<tr class=""" & strClass & """>")
                                    .WriteLine("<td class=""smlc""><img src=""/net/images/icons/filetypes/doc_pdf.png"" width=""16"" height=""16"" /></td>")
                                    .WriteLine("<td class=""sml"">" & strDocumentTitle & "</td>")
                                    .WriteLine("<td class=""smlc""><a href=""" & strDocumentURL & """ title=""View Document"" target=""_blank""><img src=""/net/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
                                    .WriteLine("</tr>")
                                End With
                            Else
                                saveDocument(AppID, strDocumentURL, strDocumentTitle & ".pdf", "")
                            End If
                            strClass = nextClass(strClass)
                        Next
                    Else
                        If (view) Then
                            With objStringWriter
                                .WriteLine("<tr class=""row1"">")
                                .WriteLine("<td colspan=""3"" class=""sml"">No documents are available.</td>")
                                .WriteLine("</tr>")
                            End With
                        End If
                    End If
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 105, "ProvideDocumentLibraryListV1 Successfully Generated", objInputXMLDoc2.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 105, "ProvideDocumentLibraryListV1 Successfully Retrieved", objOutputXMLDoc2.InnerXml)
                Else
                    If (view) Then
                        With objStringWriter
                            .WriteLine("<tr class=""row1"">")
                            .WriteLine("<td colspan=""3"" class=""sml"">No documents are available.</td>")
                            .WriteLine("</tr>")
                        End With
                    End If
                End If

            End If
            objResponse.Close()
            objResponse = Nothing
        End If

        objInputXMLDoc2 = Nothing
        objOutputXMLDoc2 = Nothing
        If (view) Then
            responseWrite(objStringWriter.ToString)
        End If

    End Sub

    Private Sub getAssociatedDocuments(quote As String, Optional view As Boolean = False)

        Dim objInputXMLDoc3 As XmlDocument = New XmlDocument, objOutputXMLDoc3 = New XmlDocument
        Dim objStringWriter As StringWriter = New StringWriter

        objInputXMLDoc3.XmlResolver = Nothing
        objInputXMLDoc3.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/avelo/doc_associated.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc3.NameTable)
        objNSM.AddNamespace("esreq", "http://www.exchange.co.uk/schema/ExchangeServicesRequest/v1")
        objNSM.AddNamespace("padlreq", "http://www.exchange.co.uk/schema/ProvideAssociatedDocumentListRequest/v1")
        Dim objNSM2 As XmlNamespaceManager = Nothing

        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:message_timestamp", ddmmyyhhmmss2utc(Config.DefaultDateTime, False))
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:message_id", strMessageID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:integrator_id", strIntegratorID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:business_contract_id", strBusinessContractID)
        writeToNode(objInputXMLDoc3, "y", "esreq:header", "esreq:user_id", strUserID2)

        Dim arrReference As Array = Split(QuoteID, "_")
        Dim strProviderCode As String = ""
        Dim strProductCode As String = ""
        If (arrReference.Length = 5) Then
            strProviderCode = arrReference(1)
            strProductCode = arrReference(2)
        End If

        writeToNode(objInputXMLDoc3, "n", "esreq:payload/padlreq:provide_associated_document_list_request", "padlreq:provider_code", strProviderCode)
        writeToNode(objInputXMLDoc3, "n", "esreq:payload/padlreq:provide_associated_document_list_request", "padlreq:product_code", strProductCode)
        writeToNode(objInputXMLDoc3, "n", "esreq:payload/padlreq:provide_associated_document_list_request", "padlreq:product_type", getProductType(quote))

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc3.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postAveloWebRequest("https://services.exchange.uk.com/exchangeserviceslistener/exchangeserviceslistener.aspx?service_id=ProvideAssociatedDocumentListV1", objInputXMLDoc3.InnerXml, True)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

                objOutputXMLDoc3.LoadXml(objReader.ReadToEnd())

                objReader.Close()
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc3.InnerXml)
                'HttpContext.Current.Response.End()

                objNSM2 = New XmlNamespaceManager(objOutputXMLDoc3.NameTable)
                objNSM2.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM2.AddNamespace("padlresp", "http://www.exchange.co.uk/schema/ProvideAssociatedDocumentListResponse/v1")

                Dim boolSuccess As Boolean = True, strErrorMessage2 As String = ""

                Dim objSuccess As XmlNode = objOutputXMLDoc3.SelectSingleNode("//padlresp:provide_associated_document_list_response/padlresp:status", objNSM2)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "true") Then
                        boolSuccess = False
                        Dim objReason As XmlNode = objOutputXMLDoc3.SelectSingleNode("//padlresp:provide_associated_document_list_response/padlresp:reason", objNSM2)
                        If Not (objReason Is Nothing) Then
                            strErrorMessage2 = "Inconsistent data found, reason = " & objReason.InnerText
                        Else
                            strErrorMessage2 = "Inconsistent data found"
                        End If
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, strErrorMessage2, objOutputXMLDoc3.InnerXml)
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage2 = "Data field not found. Field = status"
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, strErrorMessage2, objOutputXMLDoc3.InnerXml)
                End If

                With objStringWriter
                    .WriteLine("<tr class=""tablehead"">")
                    .WriteLine("<td class=""sml"" colspan=""3""><strong>Associated Documents</strong></td>")
                    .WriteLine("</tr>")
                End With

                If (boolSuccess) Then
                    Dim objLibrary As XmlNodeList = objOutputXMLDoc3.SelectNodes("//padlresp:provide_associated_document_list_response/padlresp:document_list/padlresp:document", objNSM2)
                    If (objLibrary.Count > 0) Then
                        Dim strDocumentTitle As String = "", strDocumentURL As String = "", strClass As String = ""
                        For Each objDocument In objLibrary
                            strDocumentTitle = getNodeText(objDocument, objNSM2, "padlresp:document_title")
                            strDocumentURL = getNodeText(objDocument, objNSM2, "padlresp:document_url")
                            If (view) Then
                                With objStringWriter
                                    .WriteLine("<tr class=""" & strClass & """>")
                                    .WriteLine("<td class=""smlc""><img src=""/net/images/icons/filetypes/doc_pdf.png"" width=""16"" height=""16"" /></td>")
                                    .WriteLine("<td class=""sml"">" & strDocumentTitle & "</td>")
                                    .WriteLine("<td class=""smlc""><a href=""" & strDocumentURL & """ title=""View Document"" target=""_blank""><img src=""/net/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
                                    .WriteLine("</tr>")
                                End With
                            Else
                                saveDocument(AppID, strDocumentURL, strDocumentTitle & ".pdf", "")
                            End If
                            strClass = nextClass(strClass)
                        Next
                    Else
                        If (view) Then
                            With objStringWriter
                                .WriteLine("<tr class=""row1"">")
                                .WriteLine("<td colspan=""3"" class=""sml"">No documents are available.</td>")
                                .WriteLine("</tr>")
                            End With
                        End If
                    End If
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 105, "ProvideAssociatedDocumentListV1 Successfully Generated", objInputXMLDoc3.InnerXml)
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 106, "ProvideAssociatedDocumentListV1 Successfully Retrieved", objOutputXMLDoc3.InnerXml)
                Else
                    If (view) Then
                        With objStringWriter
                            .WriteLine("<tr class=""row1"">")
                            .WriteLine("<td colspan=""3"" class=""sml"">No documents are available.</td>")
                            .WriteLine("</tr>")
                        End With
                    End If
                End If

            End If
            objResponse.Close()
            objResponse = Nothing
        End If

        objInputXMLDoc3 = Nothing
        objOutputXMLDoc3 = Nothing
        If (view) Then
            responseWrite(objStringWriter.ToString)
        End If
    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, objNSM)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & fld, objNSM)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    If (InStr(parent, "esreq:payload") > 0 Or InStr(parent, "message") > 0) Then
                        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
                        Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), objNSM.LookupNamespace(ns))
                        Dim objNewText As XmlText = doc.CreateTextNode(val)
                        objNewNode.AppendChild(objNewText)
                        objApplication.AppendChild(objNewNode)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub writeToNodeByNumber(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal lvl As Integer)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNodeList = doc.GetElementsByTagName(fld)
                If Not (objTest Is Nothing) Then
                    objTest.Item(lvl - 1).InnerText = val
                End If
            End If
        End If
    End Sub

    Private Function insertElement(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String, Optional ns As String = "def") As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before, objNSM)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
        Dim objNewNode As XmlElement = doc.CreateElement(fld, objNSM.LookupNamespace(ns))
        objApplication.InsertAfter(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function insertElementBefore(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String, Optional ns As String = "def") As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before, objNSM)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
        Dim objNewNode As XmlElement = doc.CreateElement(fld, objNSM.LookupNamespace(ns))
        objApplication.InsertBefore(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String, Optional ns As String = "def") As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name, objNSM)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
            Dim objNewNode As XmlElement = doc.CreateElement(fld, objNSM.LookupNamespace(ns))
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Function getXMLFile(ByVal service As String) As String
        Select Case service
            Case "GetWOLQuoteV1"
                Return "wol_blank.xml"
            Case "GetTermQuoteV1"
                Return "term_blank.xml"
            Case "GetIPQuoteV1"
                Return "ip_blank.xml"
            Case Else
                Return ""
        End Select
    End Function

    Private Function getView(ByVal service As String) As String
        Select Case service
            Case "GetWOLQuoteV1"
                Return "vwxmlavelowol"
            Case "GetTermQuoteV1"
                Return "vwxmlaveloterm"
            Case "GetIPQuoteV1"
                Return "vwxmlaveloip"
            Case Else
                Return ""
        End Select
    End Function

    Private Function getProductCategory(ByVal quote As String) As String
        Select Case quote
            Case "Life Cover Only"
                Return "Term Assurance"
            Case "Life Cover With Critical Illness"
                Return "Term Assurance"
            Case "Critical Illness Only"
                Return "Term Assurance"
            Case "Whole Of Life"
                Return "Whole Of Life And Critical Illness"
            Case "Income Protection"
                Return "Income Protection"
            Case Else
                Return ""
        End Select
    End Function

    Private Function getProductType(ByVal quote As String) As String
        Select Case quote
            Case "Life Cover Only"
                Return "Term Assurance"
            Case "Life Cover With Critical Illness"
                Return "Term Assurance"
            Case "Critical Illness Only"
                Return "Term Assurance"
            Case "Whole Of Life"
                Return "Whole Of Life"
            Case "Income Protection"
                Return "Keyperson"
            Case Else
                Return ""
        End Select
    End Function

    Private Function getKFIType(ByVal print As String) As String
        Select Case print
            Case "01"
                Return "KFI - Including Commission"
            Case "02"
                Return "KFI - Excluding Commission"
            Case "03"
                Return "KFI - Advisor Summary"
            Case "04"
                Return "Illustration – Regulated"
            Case "05"
                Return "Illustration – Non Regulated"
            Case "06"
                Return "Illustration - With Commission Disclosure"
            Case "07"
                Return "Illustration - Commission Only"
            Case "08"
                Return "Mortgage Cost Comparison"
            Case "09"
                Return "Mortgage Prot + Monthly Repayment"
            Case "99"
                Return "Provider Default"
            Case Else
                Return "Provider Default"
        End Select
    End Function

    Private Function postAveloWebRequest(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(strUserName2 & ":" & strPassword2)))
            End With
            Try
                Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
                With objWriter
                    .Write(post)
                    .Close()
                End With
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub saveDocument(ByVal AppID As String, ByVal url As String, ByVal pdftitle As String, ByVal note As String)
        Dim strDir As String = "/attachments"
        Dim dteDate As Date = Date.Now
        Dim strFileName As String = Year(dteDate) & Month(dteDate) & Day(dteDate) & Hour(dteDate) & Minute(dteDate) & Second(dteDate)
        Dim objWebClient As WebClient = New WebClient
        Try
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(New Uri(url))
            objRequest.Method = "HEAD"
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            If (objResponse.StatusCode = HttpStatusCode.OK And objResponse.ContentType = "application/pdf") Then
                objWebClient.DownloadFile(url, Server.MapPath(strDir & "/" & strFileName & ".pdf"))
                Dim strQry As String = _
                    "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID) " & _
                    "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(AppID, "N", 0) & ", " & _
                    formatField(strFileName & ".pdf", "", "") & ", " & _
                    formatField(pdftitle, "T", "") & ", " & _
                    formatField(note, "", "") & ", " & _
                    formatField(UserID, "N", 0) & ") "
                executeNonQuery(strQry)
                'saveNote(AppID, UserID, pdftitle & " was saved for this case.")
            End If
            objWebClient = Nothing
            objRequest = Nothing
            objResponse = Nothing
        Catch ex As Exception
            ' Do nothing
        End Try
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Shared Function getIndexNumber() As String
        Return Right(DateDiff(DateInterval.Second, CDate("01/01/1900"), Now), 8)
    End Function

End Class
