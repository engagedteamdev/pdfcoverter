﻿Imports Config, Common, CommonSave
Imports System.Net
Imports System.Xml
Imports ceTe.DynamicPDF.Merger
Imports ceTe.DynamicPDF

Partial Class SignDocument
    Inherits System.Web.UI.Page

    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private UserSessionID As String = HttpContext.Current.Request("UserSessionID")
    Private AppID As String = HttpContext.Current.Request("AppID"), MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strErrorMessage As String = ""

    '    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
    '        Dim exc As Exception = Server.GetLastError
    '        Dim excMessage As String = "", excInnerException As String = ""
    '        If Not exc Is Nothing Then
    '            excMessage = exc.Message.ToString
    '            If Not exc.InnerException Is Nothing Then
    '                excInnerException = exc.InnerException.Message.ToString
    '            End If
    '        Else
    '            excMessage = "No error message"
    '        End If
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.Write("2|An unknown error has occurred")
    '        'sendMail("Click to Dial Make Call Error", "Err14: An unknown response has been received from " & strClickToDialProviderURL & " in synety/default.aspx.<br />" & excMessage & "<br />" & excInnerException)
    '        HttpContext.Current.Response.End()
    '    End Sub

    Private Enum ActionType
        sendDocument = 1
    End Enum

    Public Sub signDocument()
        Select Case intActionType
            Case ActionType.sendDocument
                'responseWrite(1)
                sendDocument()
        End Select
    End Sub

    Private Sub sendDocument()
        Dim strRequestURL As String = "https://secure.echosign.com/services/EchoSignDocumentService17", strUserName As String = "", strPassword As String = "", strAccountID As String = "", strAPIKey As String = ""
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%EchoSign%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "EchoSignAPIKey") Then strAPIKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        Dim objClient As WebClient = New WebClient
        Dim strURL As String = ""
        If (checkValue(Request("PDFID"))) Then
            strURL = "https://beta.engaged-solutions.co.uk/letters/generatepdf.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&PDFID=" & Request("PDFID")
        ElseIf (checkValue(Request("LetterID"))) Then
            strURL = "https://beta.engaged-solutions.co.uk/letters/generateletter.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&LetterID=" & Request("LetterID")
        End If
        Dim bPDF As Byte() = objClient.DownloadData(strURL)
        objClient = Nothing

        'Dim bPDF As Byte() = generatePDFBytes()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/echosign/blank.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("def", "http://api.echosign")
        objNSM.AddNamespace("ns2", "http://dto.api.echosign")
        objNSM.AddNamespace("ns4", "http://dto14.api.echosign")

        Dim strDocumentName As String = "", strFileName As String = ""
        If (checkValue(Request("PDFID"))) Then
            strDocumentName = getAnyField("PDFName", "tblpdfs", "PDFID", Request("PDFID"))
            strFileName = getAnyField("PDFFileName", "tblpdfs", "PDFID", Request("PDFID"))
        ElseIf (checkValue(Request("LetterID"))) Then
            strDocumentName = getAnyField("LetterName", "tbllettertemplates", "LetterID", Request("LetterID"))
            strFileName = LCase(Replace(getAnyField("LetterName", "tbllettertemplates", "LetterID", Request("LetterID")), " ", "")) & ".pdf"
        End If
        Dim strEmailAddress As String = getAnyField("App1EmailAddress", "tblapplications", "AppID", AppID)

        writeToNode(objInputXMLDoc, "y", "def:sendDocument", "def:apiKey", strAPIKey, objNSM)
        writeToNode(objInputXMLDoc, "y", "ns2:FileInfo", "ns2:file", Convert.ToBase64String(bPDF), objNSM)
        writeToNode(objInputXMLDoc, "y", "ns2:FileInfo", "ns2:fileName", strFileName, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:documentCreationInfo", "ns2:name", strDocumentName, objNSM)
        writeToNode(objInputXMLDoc, "y", "ns4:RecipientInfo", "ns4:email", strEmailAddress, objNSM)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        Dim objCall As HttpWebResponse = postESWebRequest(strRequestURL, objInputXMLDoc.InnerXml, True, True)
        Dim objReader As New StreamReader(objCall.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing

        objOutputXMLDoc.LoadXml(strResponse)
        objNSM2 = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
        objNSM2.AddNamespace("ns1", "http://api.echosign")
        objNSM2.AddNamespace("ns2", "http://dto16.api.echosign")

        Dim strDocumentKey As String = objOutputXMLDoc.SelectSingleNode("//ns2:DocumentKey/ns2:documentKey", objNSM2).InnerText
        If (checkValue(strDocumentKey)) Then
            saveNote(AppID, Config.DefaultUserID, strDocumentName & " sent to " & strEmailAddress & " for signature via EchoSign")
            HttpContext.Current.Response.Write(1 & "|Document transmission successful")
        Else
            HttpContext.Current.Response.Write(0 & "|Document transmission failed")
        End If
        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()


    End Sub


    Public Function generatePDFBytes() As Byte()
        Dim objPDF As MergeDocument = Nothing
        objPDF = New MergeDocument
        Document.AddLicense("MER50CPSOBLHCJXjNv87KhAU5Aw2r7tKGnxK8cF3Q3dB66OLdUEL0J26WGbtWQpCeKfoGndaMptE+O+FLjm0102upUV+h+My0Wdw")
        Dim strPDFName As String = "", strPDFFileName As String = "", intPDFLength As String = "", strPDFRoutine As String = "", strQueryString As String = ""
        Dim strSQL As String = "", dsCache As DataTable = Nothing

        strSQL = "SELECT PDFName, PDFFileName, PDFLength, PDFRoutine FROM tblpdfs WHERE PDFID = '" & Request("PDFID") & "' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strPDFName = Row.Item("PDFName").ToString
                objPDF.Title = strPDFName
                strPDFFileName = Row.Item("PDFFileName").ToString
                intPDFLength = Row.Item("PDFLength")
                strPDFRoutine = Row.Item("PDFRoutine").ToString
            Next
        End If
        dsCache = Nothing
        If (checkValue(strPDFFileName)) Then
            objPDF.Append(New PdfDocument(HttpContext.Current.Server.MapPath("/letters/" & strPDFFileName)), 1, intPDFLength, New MergeOptions(True))
            'populateFields()
            If (checkValue(strPDFRoutine)) Then
                Dim arrParams As Object() = {objPDF, AppID}
                executeSub(Me, strPDFRoutine, arrParams)
            End If
        End If
        Return objPDF.Draw
    End Function

    Private Function checkResponse(ByRef objResponse As HttpWebResponse) As Boolean
        Dim boolValid = True
        Dim excInnerException As String = ""
        Try ' Make sure the response is valid
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            If Not (objResponse.StatusCode.ToString = "OK") Then
                boolValid = False
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValid = False
        End Try
        Return boolValid
    End Function

    Shared Function postESWebRequest(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False, Optional ByVal soap As Boolean = False, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
        If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
            Call Certificate.OverrideCertificateValidation()
        End If
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                If (soap) Then
                    .Headers.Add("SOAPAction", "")
                End If
                If (xml) Then
                    .ContentType = "text/xml; charset=utf-8"
                Else
                    .ContentType = "application/x-www-form-urlencoded"
                End If
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub


End Class
