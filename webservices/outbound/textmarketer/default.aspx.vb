﻿Imports Config, Common
Imports System.Net
Imports System.Xml

Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private strTelephoneTo As String = HttpContext.Current.Request("frmTelephoneTo")
    Private strSMSBody As String = HttpContext.Current.Request("frmSMSBody")
	Private strSMSProviderFrom As String = HttpContext.Current.Request("frmSMSFrom")
    Private strSMSProviderURL As String = "", strSMSProviderUserName As String = "", strSMSProviderPassword As String = ""
    Private strUserName As String = ""
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        strSMSProviderURL = objLeadPlatform.Config.SMSProviderURL()
        strSMSProviderUserName = objLeadPlatform.Config.SMSProviderUserName()
        strSMSProviderPassword = objLeadPlatform.Config.SMSProviderPassword()
		If (Not checkValue(strSMSProviderFrom)) Then
        	strSMSProviderFrom = objLeadPlatform.Config.SMSProviderFrom()
		End If
    End Sub

    Public Sub sendSMS()
        ' Testing only
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.Write("0|Success|0")
        'responseEnd()
        strTelephoneTo = "44" & Right(strTelephoneTo, 10)
'		responseWrite(objLeadPlatform.Config.SMSProviderURL() & "?username=" & strSMSProviderUserName & "&password=" & strSMSProviderPassword & "&number=" & strTelephoneTo & "&orig=" & strSMSProviderFrom & "&message=" & strSMSBody & "&option=xml")
'		responseEnd()
        Dim objResponse As HttpWebResponse = getWebRequest(objLeadPlatform.Config.SMSProviderURL() & "?username=" & strSMSProviderUserName & "&password=" & strSMSProviderPassword & "&number=" & strTelephoneTo & "&orig=" & strSMSProviderFrom & "&message=" & strSMSBody & "&option=xml")
        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//response")
            Dim objMessage As XmlElement = objApplication.SelectSingleNode("//response/reason")
            Dim strMessage As String = ""
            If (Not objMessage Is Nothing) Then
                strMessage = objMessage.InnerText
            End If
            Select Case objApplication.GetAttribute("status")
                Case "success" ' Success
					If (Len(strSMSBody) > 160) Then
						incrementSystemConfigurationField("SMSNumberSent", 1)
					End If
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("0|Success|" & objApplication.GetAttribute("id"))
                Case Else ' Display user message
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("1|" & strMessage & "|0")
            End Select
            objOutputXMLDoc = Nothing
            objReader.Close()
            objReader = Nothing
        End If
        If (checkResponse(objResponse, "")) Then
            objResponse.Close()
        End If
        objResponse = Nothing
    End Sub

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailFrom=itsupport@engaged-solutions.co.uk&strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&strAttachment=&UserSessionID=" & HttpContext.Current.Request("UserSessionID"))
    End Sub

End Class
