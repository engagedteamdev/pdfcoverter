using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using LoansWarehouse;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Net;

public partial class Default : Page
{
    private string AppID = HttpContext.Current.Request["AppID"];

    protected void Page_Load(object sender, EventArgs e)
    {
        httpPost();
    }


	public void httpPost()
	{
		
		string strMyXml = "<LoanWarehouse>" +
							"<Login>" +
								"<username></username>" +
								"<password></password>" +
							"</Login>" +
							"<Source>" +
								"<UniqueID>100012</UniqueID>" +
								"<ContactEmail>test@test.com</ContactEmail>" +
								"<ContactTel>01302123123</ContactTel>" +
								"<ContactName>William Worthington</ContactName>" +
								"<CompanyName>Engaged CRM</CompanyName>" +
							"</Source>" +
							"<LoanDetails>" +
								"<LoanAmount>10000</LoanAmount>" +
								"<LoanPurpose>CON</LoanPurpose>" +
								"<LoanTerm>120</LoanTerm>" +
								"<Product>SEC</Product>" +
								"<Notes></Notes>" +
							"</LoanDetails>" +
							"<Application>" +
								"<MainApplication>" +
								"<Title>Mr</Title>" +
								"<FirstName>William</FirstName>" +
								"<Surname>Worthington</Surname>" +
								"<Email>email@test.com</Email>" +
								"<DaytimeTel>01923555555</DaytimeTel>" +
								"<MobileTel>07974400840</MobileTel>" +
								"<ResidencyStatus>H</ResidencyStatus>" +
								"<Address>" +
									"<Postcode>WD1 1XX</Postcode>" +
									"<County>Hertfordshire</County>" +
									"<Town>Watford</Town>" +
									"<Street>St Johns Rd</Street>" +
									"<HouseNoName>10</HouseNoName>" +								
								"</Address>" +
								"</MainApplication>" +
								"<Property>" +
									"<PropType>H</PropType>" +
									"<PropertyValue>150000</PropertyValue>" +
									"<BalanceOutstanding>0</BalanceOutstanding>" +
								"</Property>" +
							"</Application>" +
						  "</LoanWarehouse>";



		LWServiceClient client = new LWServiceClient();

		var submitapp = client.SubmitShortApplication(strMyXml);

		XmlDocument myXml = new XmlDocument();
		XPathNavigator xNav = myXml.CreateNavigator();
		XmlSerializer y = new XmlSerializer(submitapp.GetType());
		using (var xs = xNav.AppendChild())
		{
			y.Serialize(xs, submitapp);
		}
		HttpContext.Current.Response.ContentType = "text/xml";
		HttpContext.Current.Response.Write(myXml.OuterXml);
		HttpContext.Current.Response.End();

	}




}



