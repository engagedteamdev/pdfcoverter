﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization


Partial Class XMLCRM
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAction As String = ""
    Private strMediaID As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
	Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strHI As String = HttpContext.Current.Request("frmHI"), strOtherAsDC As String = HttpContext.Current.Request("frmOtherAsDC")
    Private strErrorMessage As String = ""
	Private strMediaCampID as string = HttpContext.Current.Request.QueryString("MediaCampaignID")

    Public Sub generateXml()
			strXMLURL = "https://heathcrawford.flg360.co.uk/api/APILeadCreateUpdate.php"
            sendXML()
   
    End Sub

    Private Sub sendXML()

		ServicePointManager.Expect100Continue = true
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12



		objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/heathcrawfordes/blank.xml"))

		Dim strSQL As String = "SELECT * FROM vwheathcrawford WHERE AppID = " & AppID & " "
        Dim dtb1 As New DataTable
	    using DBConnection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringES").ConnectionString)					 
	    DBConnection.open()			
	    Using dad As New SqlDataAdapter(strSQL,DBConnection)
	    dad.Fill(dtb1)
	    End Using
	    DBConnection.close()
	    End Using
			 
	    If (dtb1.Rows.Count > 0) Then 
            For Each Row As DataRow In dtb1.Rows
                For Each Column As DataColumn In dtb1.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") Then

                      
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeToNode("n", "data/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)

                       

                    End If
                Next
            Next
        End If
     


		Try 

		

		 Dim objResponse As HttpWebResponse = postWebRequestDP(strXMLURL, objInputXMLDoc.InnerXml)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

                  
		objOutputXMLDoc.LoadXml(objReader.ReadToEnd())


		Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//result")
		
		Dim strmessgae As String = objApplication.SelectSingleNode("//item/message").InnerText

		Dim strid As String = objApplication.SelectSingleNode("//item/id").InnerText


		objReader.Close()
		objReader = Nothing
		If(strmessgae = "OK")Then 
				HttpContext.Current.Response.Clear()
				HttpContext.Current.Response.Write(1 & "|" & strid & "")
				

		Else
				HttpContext.Current.Response.Write("0|Error")
		End If



		 Catch ex As Exception

			HttpContext.Current.Response.Write("0|Error")

		End Try

    End Sub

    Private Sub writeXML(ByVal man As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                Else
                    Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                End If
            End If
        End If
    End Sub

	    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String , ByVal strPost As String)

        Dim strQry As String = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(strPost, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub


	Shared Function postWebRequestDP(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False, Optional ByVal soap As Boolean = False, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
        If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
            Call Certificate.OverrideCertificateValidation()
        End If
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                If (soap) Then
                    .Headers.Add("SOAPAction", url) ' Required for Cisco
                End If
                If (xml) Then
                    .ContentType = "Capplication/xml"
                Else
                    .ContentType = "application/xml"
                End If
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                responseWrite(err.Message)
               ' responseEnd()
                
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
