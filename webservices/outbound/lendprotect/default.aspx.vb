﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLLendProtect
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strLenderID As String = "", strMerchantID As String = "", strStoreID As String = "", strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strRequestType As String = HttpContext.Current.Request("RequestType")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""

    Public Sub generateXml()

        'If (strEnvironment = "live") Then
        'strXMLURL = "https://www.lendprotect.co.uk/webservices/lendprotectrequest.aspx?version=2.1"
        'Else
        strXMLURL = "https://datatest.lendprotect.co.uk/webservices/lendprotectrequest.aspx?version=1.9"
        'End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%LendProtect%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "LendProtectLenderID") Then strLenderID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectMerchantID") Then strMerchantID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectStoreID") Then strStoreID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (checkValue(HttpContext.Current.Request("MediaCampaignID"))) Then
                    Dim strRenewal As String = getAnyFieldFromDataStore("LoanRenewal", AppID)
                    Dim intMediaCampaignGradeID As String = getAnyField("MediaCampaignDialerGradeID", "tblmediacampaigns", "MediaCampaignID", HttpContext.Current.Request("MediaCampaignID"))
                    Dim strMediaCampaignGradeName As String = getAnyField("MediaCampaignGradeName", "tblmediacampaigngrades", "MediaCampaignGradeID", intMediaCampaignGradeID)
                    If (strRenewal = "Y") Then
                        If (Row.Item("SystemConfigurationName") = "LendProtectRenewalStoreID") Then strStoreID = Row.Item("SystemConfigurationValue")
                        If (Row.Item("SystemConfigurationName") = "LendProtectRenewalUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                    ElseIf (InStr(strMediaCampaignGradeName, "Web") > 0) Then
                        If (Row.Item("SystemConfigurationName") = "LendProtectWebStoreID") Then strStoreID = Row.Item("SystemConfigurationValue")
                        If (Row.Item("SystemConfigurationName") = "LendProtectWebUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                    End If
                End If
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            Dim strAssignUserID As String = getAnyField("MediaCampaignAssignUserID", "tblmediacampaigns", "MediaCampaignID", HttpContext.Current.Request("MediaCampaignID"))
            If (checkValue(strAssignUserID)) Then
                Dim strQry2 As String = "SELECT TOP 1 UserID FROM tblusers WHERE UserID IN(" & strAssignUserID & ") AND UserCallCentre = 1 AND UserSuperAdmin = 0 AND UserActive = 1 AND UserLockedOut = 0 AND CompanyID = " & CompanyID & " ORDER BY (UserWeighting/100) * ISNULL((SELECT COUNT(AppID) AS Expr1 FROM dbo.tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID AND ApplicationStatusDateName = 'AssignedToSales' WHERE (CallCentreUserID = dbo.tblusers.UserID) AND (DATEDIFF(DD, ApplicationStatusDate, GETDATE()) = 0)),0)"
                Dim strResult2 As String = New Caching(Nothing, strQry2, "", "", "").returnCacheString
                executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = " & formatField(strResult2, "N", 0) & " WHERE CallCentreUserID = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                saveUpdatedDate(AppID, Config.DefaultUserID, "AssignedToSales")
            End If
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/lendprotect/blank.xml"))

        'writeToNode(objInputXMLDoc, "y", "LoginInfo", "ChannelIdentifier", strLenderID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "MerchantIdentifier", strMerchantID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "StoreIdentifier", strStoreID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "Username", strUserName)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "Password", strPassword)
        If (checkValue(strRequestType)) Then
            'writeToNode(objInputXMLDoc, "y", "Request", "RequestType", strRequestType)
        End If

        Dim strSQL As String = "SELECT * FROM vwxmllendprotect WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToArray = "BankCardNumber") Then
                            writeToNode(objInputXMLDoc, "n", "ApplicationInfo", "CCNumber", getCardDetails("CardNumber"))
                        ElseIf (Column.ColumnName.ToArray = "BankCardCV2") Then
                            writeToNode(objInputXMLDoc, "n", "ApplicationInfo", "CCCV2", getCardDetails("CardCV2"))
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("LendProtect Unsuccessfully Retrieved", -5, AppID, "<LendProtect><Status>0</Status><Reason>Validation error</Reason></LendProtect>", "")
        Else
            ' Post the SOAP message.	
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "LendProtect Request Generated", objInputXMLDoc.InnerXml)
            Dim objResponse As HttpWebResponse = postLPWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            If (objResponse.StatusCode.ToString = "OK") Then
                saveUpdatedDate(AppID, Config.DefaultUserID, "CreditChecked")
                If (HttpContext.Current.Request("Interactive") <> "Y") Then
                    saveStatus(AppID, Config.DefaultUserID, "CRD", "")
                End If
                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//ApplicationInfo")
                Dim strStatus As String = objApplication.SelectSingleNode("//TransactionStatus").InnerText
                Dim objMessages As XmlElement = objOutputXMLDoc.SelectSingleNode("//ResponseCodes/Response/Description")
                Dim strMessage As String = ""
                If (Not objMessages Is Nothing) Then
                    For Each message As XmlNode In objMessages.ChildNodes
                        strMessage += objMessages.InnerText & "<br />"
                    Next
                End If

                Dim strCreditScore As String = getNodeText(objOutputXMLDoc.SelectSingleNode("//ApplicationInfo"), Nothing, "//Scores/RiskScore")
                executeNonQuery("UPDATE tblapplications SET ApplicationCreditScore = " & formatField(strCreditScore, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                Select Case strStatus
                    Case "A"
                        Dim strResults As String = "<LendProtect>" & _
                          "<Status>1</Status>" & _
                          "<CreditScore>" & strCreditScore & "</CreditScore>" & _
                          "</LendProtect>"
                        If (checkValue(strMessage)) Then
                            saveNote(AppID, Config.DefaultUserID, "Credit check approved: score was " & strCreditScore & "<br />" & strMessage)
                        Else
                            saveNote(AppID, Config.DefaultUserID, "Credit check approved: score was " & strCreditScore)
                        End If
                        saveCreditSearch(AppID, "LendProtect")
                        SOAPSuccessfulMessage("LendProtect Request Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                    Case Else
                        saveNote(AppID, Config.DefaultUserID, "Credit check declined: " & strMessage)
                        saveCreditSearch(AppID, "LendProtect")
                        SOAPUnsuccessfulMessage("LendProtect Request Unsuccessfully Retrieved - " & strMessage, 1, AppID, "<LendProtect><Status>1</Status><Reason>" & strMessage & "</Reason></LendProtect>", objOutputXMLDoc.InnerXml)
                End Select
            Else
                SOAPUnsuccessfulMessage("LendProtect Unsuccessfully Retrieved", -1, AppID, "<LendProtect><Status>-1</Status><Reason>Data error</Reason></LendProtect>", "")
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub saveCreditSearch(ByVal AppID As String, ByVal provider As String)
        Dim strSQL As String = "INSERT INTO tblcreditsearches (AppID, CompanyID, CreditSearchData, CreditSearchType) VALUES (" & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField(CompanyID, "N", 0) & ", " & _
                                formatField(objOutputXMLDoc.InnerXml, "", "") & ", " & _
                                formatField(provider, "T", "") & ")"
        executeNonQuery(strSQL)
    End Sub

    Private Function getCardDetails(ByVal details As String) As String
        Dim strDetails As String = ""
        Dim strSQL As String = "EXEC spdecryptcarddetails @AppID = " & AppID & ", @CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strDetails = Row.Item(details).ToString
            Next
        End If
        dsCache = Nothing
        Return strDetails
    End Function

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function postLPWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'responseWrite("host: " & url & "<br>")
            'responseWrite(post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
