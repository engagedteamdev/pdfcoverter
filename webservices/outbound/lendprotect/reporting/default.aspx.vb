﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.lendprotect.datatest

Partial Class XMLReporting
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strLenderID As String = "", strMerchantID As String = "", strStoreID As String = "", strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID"), strType As String = HttpContext.Current.Request("type"), intID As String = HttpContext.Current.Request("id")
    Private loanRequest As uk.co.lendprotect.datatest.Request = New uk.co.lendprotect.datatest.Request
    Private strErrorMessage As String = ""

    Public Sub generateXml()

        Dim loginInfo As uk.co.lendprotect.datatest.LoginDetails = New uk.co.lendprotect.datatest.LoginDetails

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%LendProtect%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "LendProtectLenderID") Then strLenderID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectMerchantID") Then strMerchantID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectStoreID") Then strStoreID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "LendProtectPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                With loginInfo
                    .LenderIdentifier = strLenderID
                    .MerchantIdentifier = strMerchantID
                    .StoreIdentifier = strStoreID
                    .Username = strUserName
                    .Password = strPassword
                End With
            Next
        End If
        dsCache = Nothing

        loanRequest.LoginInfo = loginInfo

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        Dim objService As LoanReporting = New LoanReporting
        Dim loanDetails As uk.co.lendprotect.datatest.LoanDetails = New uk.co.lendprotect.datatest.LoanDetails

        Dim strSQL As String = "SELECT TOP 1 * FROM vwpaymentschedule WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND PaymentHistoryID = '" & intID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                With loanDetails
                    .AppID = Row.Item("AppID")
                    .Balance = Row.Item("LoanBalance")
                    .BankAcct = Row.Item("BankAccountNumber").ToString
                    .DOB = Row.Item("App1DOB").ToString
                    .DueDate = Row.Item("PaymentDue").ToString
                    .Forename = Row.Item("App1FirstName").ToString
                    .LoanDate = Row.Item("PaidOut").ToString
                    .PaymentAmt = Row.Item("PaymentAmount")
                    .LoanID = Row.Item("PaymentHistoryReference").ToString
                    .SortCode = Row.Item("BankSortCode").ToString
                    .Surname = Row.Item("App1Surname").ToString
                    .TranDate = FormatDateTime(CDate(Row.Item("PaymentHistoryDate")), DateFormat.ShortDate)
                    .Type = strType
                End With
            Next
        End If
        dsCache = Nothing

        loanRequest.LoanInfo = loanDetails
        Dim loanResponse As uk.co.lendprotect.datatest.ResponseCode = objService.ReportLoan(loanRequest)

        Dim strMessage As String = loanResponse.Description
        Select Case loanResponse.Code
            Case "true"
                Dim strResults As String = "<CreditReporting>" & _
                  "<Status>1</Status>" & _
                  "</CreditReporting>"
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To LP Reporting", "")
                SOAPSuccessfulMessage("LP Reporting Successfully Retrieved", 1, AppID, strResults, "")
            Case Else
                Dim strResults As String = "<CreditReporting>" & _
                  "<Status>0</Status>" & _
                  "<Reason>" & strMessage & "</Reason>" & _
                  "</CreditReporting>"
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To LP Reporting: " & strMessage, "")
                SOAPSuccessfulMessage("LP Reporting Successfully Retrieved", 1, AppID, strResults, "")
        End Select

        objService = Nothing

    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

End Class
