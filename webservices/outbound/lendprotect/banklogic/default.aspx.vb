﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLBankLogic
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strLenderID As String = "", strMerchantID As String = "", strStoreID As String = "", strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strRequestType As String = HttpContext.Current.Request("RequestType")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        'If (strEnvironment = "live") Then
        'strXMLURL = "https://lendprotect.co.uk/WebServices/BankLogic/BankLogicRequest.aspx"
        'Else
        strXMLURL = "https://datatest.lendprotect.co.uk/WebServices/BankLogic/BankLogicRequest.aspx"
        'End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%BankLogic%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "BankLogicLenderID") Then strLenderID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BankLogicMerchantID") Then strMerchantID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BankLogicStoreID") Then strStoreID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BankLogicUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BankLogicPassword") Then strPassword = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/banklogic/blank.xml"))

        'writeToNode(objInputXMLDoc, "y", "LoginInfo", "ChannelIdentifier", strLenderID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "MerchantIdentifier", strMerchantID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "StoreIdentifier", strStoreID)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "Username", strUserName)
        writeToNode(objInputXMLDoc, "y", "LoginInfo", "Password", strPassword)
        If (checkValue(strRequestType)) Then
            writeToNode(objInputXMLDoc, "y", "Request", "RequestType", strRequestType)
        End If

        Dim strSQL As String = "SELECT * FROM vwxmlbanklogic WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                            ' Extra logic
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("BankLogic Unsuccessfully Retrieved", -5, AppID, "<BankLogic><Status>0</Status><Reason>Validation error</Reason></BankLogic>", "")
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            HttpContext.Current.Response.ContentType = "text/xml"
            HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            HttpContext.Current.Response.End()

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//Response")
                Dim strStatus As String = objApplication.SelectSingleNode("//Success").InnerText
                Dim objMessage As XmlElement = objOutputXMLDoc.SelectSingleNode("//Message")
                Dim objURL As XmlElement = objOutputXMLDoc.SelectSingleNode("//LinkAccountWizardUrl")
                Dim strMessage As String = ""
                If (Not objMessage Is Nothing) Then
                    strMessage = objMessage.InnerText
                End If
                Dim strURL As String = ""
                If (Not objURL Is Nothing) Then
                    strURL = objURL.InnerText
                End If
                Select Case strStatus
                    Case "true"
                        Dim strResults As String = "<BankLogic>" & _
                          "<Status>1</Status>" & _
                          "<URL>" & encodeURL(strURL) & "</URL>" & _
                          "</BankLogic>"
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To BankLogic", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Received From BankLogic", objOutputXMLDoc.InnerXml)
                        SOAPSuccessfulMessage("BankLogic Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                    Case Else
                        Dim strResults As String = "<BankLogic>" & _
                          "<Status>0</Status>" & _
                          "<Reason>" & strMessage & "</Reason>" & _
                          "</BankLogic>"
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To BankLogic: " & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Received From BankLogic: " & strMessage, objOutputXMLDoc.InnerXml)
                        SOAPSuccessfulMessage("BankLogic Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                End Select
            Else
                SOAPUnsuccessfulMessage("BankLogic Unsuccessfully Retrieved", -5, AppID, "<BankLogic><Status>0</Status><Reason>Data error</Reason></BankLogic>", "")
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function postLPWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "application/x-www-form-urlencoded" '"text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'responseWrite("host: " & url & "<br>")
            'responseWrite(post & "<br>")
            'For Each item In objRequest.Headers
            'responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
