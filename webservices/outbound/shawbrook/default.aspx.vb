﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json


Partial Class XMLShawbrook
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strBrokerID As String = "", strUserName As String = "", strPassword As String = ""
    Private AppID As String = HttpContext.Current.Request.QueryString("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strRequestType As String = HttpContext.Current.Request("RequestType")
    Private strXmlID As String = HttpContext.Current.Request("xmlID")
    Private strSelectedPlan As String = HttpContext.Current.Request("selectedPlan")
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = "", strAppDetail2 As String = "", strAddressDetail1 As String = "", strAddressDetail2 As String = ""
    Private strShawbrookCaseID As String = "", xmlResponseID As Integer = 0

    Public Sub processShawbrook()



        If (strEnvironment = "test") Then
            strXMLURL = "https://test-securedlending.shawbrook.co.uk/brokerapi/"
            strUserName = Common.getAnyFieldFromUserStore("shawbrookUsername", "800") 'Config.DefaultUserID)
            strPassword = Common.getAnyFieldFromUserStore("ShawbrookPassword", "800") 'Config.DefaultUserID)
        Else
            strXMLURL = "https://securedlending.shawbrook.co.uk/brokerapi/"
            strUserName = Common.getAnyFieldFromUserStore("shawbrookUsername", Config.DefaultUserID)
            strPassword = Common.getAnyFieldFromUserStore("ShawbrookPassword", Config.DefaultUserID)
        End If

 		'	responseWrite(strUserName)
         '   responseEnd()


        strBrokerID = ""

        If (checkValue(strSelectedPlan)) Then

            selectPlan(strSelectedPlan)

        ElseIf checkValue(AppID) Then

            HttpContext.Current.Server.ScriptTimeout = 300

            generateXML()

           ' responseWrite(objInputXMLDoc.InnerXml)
            'responseEnd()
			  Dim strResponse As String = submitSoftSearch(objInputXMLDoc.InnerXml)
			   'responseWrite(strResponse)
               'responseEnd()
  Try
                ' strResponse = 

            Dim bestPlan As SoftSearchResponseTrimmed = processResponse(strResponse)

            Catch e As WebException

                If e.Status = WebExceptionStatus.ProtocolError Then
                    Dim resp As WebResponse = e.Response
                    Dim sr1 As New StreamReader(resp.GetResponseStream())

                    Dim s As String = sr1.ReadToEnd()
       

                   responseWrite(strResponse)
           		   responseEnd()

                End If

            Catch ex As Exception

             responseWrite(strResponse)
             responseEnd()

            End Try


        Else

            Throw New ApplicationException("Missing input parameters")

        End If

    End Sub

    Private Sub generateXML()

        'column filters
        Dim filterStrings As New List(Of String)

        'used to match up additional income items
        Dim income As New Dictionary(Of String, String)

        income.Add("Additional Income", "Income")
        income.Add("Base for Income", "Income")
        income.Add("Carers Allowance", "CarersAllowance")
        income.Add("Child Tax Credit", "ChildTaxCredit")
        income.Add("Disability Living Allowance", "DisabilityLivingAllowance")
        income.Add("Disposable Income", "Income")
        income.Add("Gross Maintenance", "MaintenanceCSA")
        income.Add("Gross Private Pension", "PrivatePension")
        income.Add("Gross Rental Income", "RentalIncome")
        income.Add("Incapacity Benefit", "IncapacityBenefit")
        income.Add("Private Pension", "PrivatePension")
        income.Add("Rental Income", "RentalIncome")
        income.Add("Second Job", "SecondJobIncome")
        income.Add("State Pension", "StatePensionCredit")
        income.Add("Gross State Pension", "StatePensionCredit")
        income.Add("Gross War Pension", "StatePensionCredit")
        income.Add("Gross Widower Pension", "StatePensionCredit")
        income.Add("Working Tax Credit", "WorkingTaxCredit")

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/shawbrook/decision.xml"))


        Dim strSQL As String = "SELECT * FROM vwxmlshawbrook WHERE AppID = " & AppID & " AND (CompanyID = '" & CompanyID & "' OR 'test' = '" & strEnvironment & "')"


        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()

        Dim strDPAAuthorisationToken As String = "", strSecFFPastRetirement As String = "", strApp1AnnualOtherIncomeItem As String = "", strApp1AnnualOtherIncomeValue As String = ""
        Dim strApp2AnnualOtherIncomeItem As String = "", strApp2AnnualOtherIncomeValue As String = "", strApp1ExpInsurance As String = "", strApp1ExpChildcareMaintenance As String = ""
        Dim strApp1ExpCouncilTax As String = "", strApp1ExpGasElectricity As String = "", strApp1ExpWaterRates As String = ""
        Dim strApp1ExpFoodToiletCleaning As String = "", strApp1ExpTravel As String = "", strApp1ExpPetrol As String = ""
        Dim strApp1ExpEducation As String = "", strApp1ExpClothing As String = "", strApp1ExpHolidaysLeisure As String = ""
        Dim strApp1ExpTelephone As String = "", strApp1ExpOther As String = "", strApp1SuppIncome As String = "", strApp2SuppIncome As String = "", suppIncomeAllApps As Integer = 0
        Dim dPAAuthorisation As DPAAuthorisation = getDPAAuth(), strHouseNameOrNumber As String = "", strPostcode As String = ""


        If (dsCache.Rows.Count > 0) Then

            If (Not (IsDBNull(dsCache.Rows(0).Item("ApplicantDetail(2)/Surname")))) Then
                setAppDetail2()
                If (Not (IsDBNull(dsCache.Rows(0).Item("ApplicantDetail(2)/Addresses/AddressDetail(2)/HouseNo")))) Then
                    setAddress2()
                Else
                    filterStrings.Add("ApplicantDetail(2)/Addresses/AddressDetail(2)")
                End If
            Else
                filterStrings.Add("ApplicantDetail(2)")
                filterStrings.Add("App2AnnualOtherIncomeItem")
                filterStrings.Add("App2AnnualOtherIncomeValue")
            End If

            If (Not (IsDBNull(dsCache.Rows(0).Item("ApplicantDetail(1)/Addresses/AddressDetail(2)/HouseNo")))) Then
                setAddress1()
            Else
                filterStrings.Add("ApplicantDetail(1)/Addresses/AddressDetail(2)")
            End If

            Dim strXmlNew As String = objInputXMLDoc.InnerXml.ToString.Replace("{APPDETAIL2}", strAppDetail2)

            strXmlNew = strXmlNew.Replace("{ADDRDETAIL1}", strAddressDetail1)
            strXmlNew = strXmlNew.Replace("{ADDRDETAIL2}", strAddressDetail2)

            objInputXMLDoc.LoadXml(strXmlNew)
			
			'responseWrite(strXmlNew)
            'responseEnd()


            strDPAAuthorisationToken = dPAAuthorisation.Key.ToString()
            writeToNode(objInputXMLDoc, "y", "DPAAuthorisationToken", "DPAAuthorisationToken", strDPAAuthorisationToken)

            'Response.Write(strXmlNew)

            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") And Not ((filterStrings.Contains(Column.ColumnName.ToString))) Then


                        If (Column.ColumnName.ToString = "SecFFPastRetirement") Then
                            'Do nothing for the moment
                        ElseIf (Column.ColumnName.ToString = "App1AnnualOtherIncomeItem") Then
                            strApp1AnnualOtherIncomeItem = Row(Column).ToString

                        ElseIf (Column.ColumnName.ToString = "App1AnnualOtherIncomeValue") Then

                            'Maintain count of income types used
                            Dim incomeCounts As New Dictionary(Of String, Integer)

                            Dim arrIncomeItems As Array = Split(strApp1AnnualOtherIncomeItem, ",")
                            Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")

                            For x As Integer = 0 To UBound(arrIncomeItems)

                                'Default val
                                Dim val As String = "Income"

                                If income.ContainsKey(arrIncomeItems(x)) Then
                                    val = income.Item(arrIncomeItems(x))
                                End If

                                Dim itemCount As Integer = 0

                                If incomeCounts.ContainsKey(val) Then
                                    itemCount = incomeCounts.Item(val)
                                    itemCount = itemCount + 1
                                    incomeCounts.Item(val) = itemCount
                                Else
                                    itemCount = itemCount + 1
                                    incomeCounts.Add(val, itemCount)
                                End If

                                createElement(objInputXMLDoc, "Applicants/ApplicantDetail[1]/RegularIncome", val + "[" & itemCount & "]", val)
                                writeToNode(objInputXMLDoc, "y", "Applicants/ApplicantDetail[1]/RegularIncome", val + "[" & itemCount & "]", arrIncomeValues(x))

                            Next


                        ElseIf (Column.ColumnName.ToString = "App2AnnualOtherIncomeItem") Then
                            strApp2AnnualOtherIncomeItem = Row(Column).ToString

                        ElseIf (Column.ColumnName.ToString = "App2AnnualOtherIncomeValue") Then

                            'Maintain count of income types used
                            Dim incomeCounts As New Dictionary(Of String, Integer)
                            Dim arrIncomeItems As Array = Split(strApp2AnnualOtherIncomeItem, ",")
                            Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")

                            For x As Integer = 0 To UBound(arrIncomeItems)

                                'Default val
                                Dim val As String = "Income"

                                If income.ContainsKey(arrIncomeItems(x)) Then
                                    val = income.Item(arrIncomeItems(x))
                                End If

                                Dim itemCount As Integer = 0

                                If incomeCounts.ContainsKey(val) Then
                                    itemCount = incomeCounts.Item(val)
                                    itemCount = itemCount + 1
                                    incomeCounts.Item(val) = itemCount
                                Else
                                    itemCount = itemCount + 1
                                    incomeCounts.Add(val, itemCount)
                                End If

                                createElement(objInputXMLDoc, "Applicants/ApplicantDetail[2]/RegularIncome", val + "[" & itemCount & "]", val)
                                writeToNode(objInputXMLDoc, "y", "Applicants/ApplicantDetail[2]/RegularIncome", val + "[" & itemCount & "]", arrIncomeValues(x))
                            Next

                        ElseIf (Column.ColumnName.ToString = "App1ExpInsurance") Then
                            strApp1ExpInsurance = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpInsurance)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "BuildingsAndContentInsurance")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpChildcareMaintenance") Then
                            strApp1ExpChildcareMaintenance = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpChildcareMaintenance)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "MaintenanceOrChildSupport")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpCouncilTax") Then
                            strApp1ExpCouncilTax = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpCouncilTax)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "CouncilTax")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpWaterRates") Then
                            strApp1ExpWaterRates = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpWaterRates)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "GasElectricityFuel")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpGasElectricity") Then
                            strApp1ExpGasElectricity = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpGasElectricity)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Water")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpFoodToiletCleaning") Then
                            strApp1ExpFoodToiletCleaning = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpFoodToiletCleaning)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Shopping")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpTravel") Then
                            strApp1ExpTravel = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpTravel)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Transport")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpPetrol") Then
                            strApp1ExpPetrol = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpPetrol)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Transport")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpEducation") Then
                            strApp1ExpEducation = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpEducation)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Education")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpClothing") Then
                            strApp1ExpClothing = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpClothing)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "ClothingAndFootWear")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpHolidaysLeisure") Then
                            strApp1ExpHolidaysLeisure = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpHolidaysLeisure)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "EntertainmentAndRecreation")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpTelephone") Then
                            strApp1ExpTelephone = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpTelephone)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "Communication")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1ExpOther") Then
                            strApp1ExpOther = Row(Column).ToString
                            Dim nodeCount As Integer = objInputXMLDoc.SelectNodes("//Expenditure/ExpenditureItem").Count()
                            createElement(objInputXMLDoc, "Expenditure", "ExpenditureItem[" & nodeCount + 1 & "]", "ExpenditureItem")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Amount", strApp1ExpOther)
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "Cause", "AnyOtherExpenses")
                            writeToNode(objInputXMLDoc, "y", "ExpenditureItem[" & nodeCount + 1 & "]", "ExplanatoryNote", "")

                        ElseIf (Column.ColumnName.ToString = "App1SuppIncome") Then
                            Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")
                            Dim total As Decimal = 0

                            For x As Integer = 0 To UBound(arrIncomeValues)
                                total += Decimal.Parse(arrIncomeValues(x))
                            Next

                            suppIncomeAllApps = total
                            writeToNode(objInputXMLDoc, "y", "SupplementaryIncomeAllApplicants", "SupplementaryIncomeAllApplicants", suppIncomeAllApps.ToString)

                        ElseIf (Column.ColumnName.ToString = "App2SuppIncome") Then
                            Dim arrIncomeValues As Array = Split(Row(Column).ToString, ",")
                            Dim total As Integer = suppIncomeAllApps

                            For x As Integer = 0 To UBound(arrIncomeValues)
                                total += Decimal.Parse(arrIncomeValues(x))
                            Next

                            suppIncomeAllApps = total
                            If (total > 0) Then
                                writeToNode(objInputXMLDoc, "y", "SupplementaryIncomeAllApplicants", "SupplementaryIncomeAllApplicants", suppIncomeAllApps.ToString)
                            Else
                                Dim strNewXML As String = objInputXMLDoc.InnerXml.ToString
                                strNewXML = strNewXML.Replace("<SupplementaryIncomeAllApplicants>0</SupplementaryIncomeAllApplicants>", "")
                                objInputXMLDoc.LoadXml(strNewXML)


                            End If

                        ElseIf (Column.ColumnName.ToString = "LookupHouseNo") Then
                            strHouseNameOrNumber = Row(Column).ToString
							'response.write(strHouseNameOrNumber)
							'response.End()

                        ElseIf (Column.ColumnName.ToString = "LookupPostcode") Then
                            strPostcode = Row(Column).ToString

                            Dim addressMatches As AddressMatchResponse = getAddressMatchResponse(strHouseNameOrNumber, strPostcode)
							'response.write(addressMatches.MatchingAddresses.Count)
							'response.End()
                            Dim strLookupCode As String = ""

                            If (addressMatches.MatchingAddresses.Count = 0) Then
                                response.write("address")
								response.End()
                            ElseIf (addressMatches.MatchingAddresses.Count = 1) Then
                                strLookupCode = addressMatches.MatchingAddresses(0).PtcAbsCode
                            Else

                                For Each address As AddressDetail In addressMatches.MatchingAddresses
							'response.write(replace(address.Postcode, " ", "") & " - " & strPostcode)
							'response.End()
                                    If (replace(address.Postcode, " ", "") = strPostcode And address.HouseNo = strHouseNameOrNumber) Then
                                        strLookupCode = address.PtcAbsCode
                                    Else
                                response.write("address")
								response.End()
                                    End If

                                Next


                            End If

                            writeToNode(objInputXMLDoc, "y", "Applicants/ApplicantDetail[1]/Addresses/AddressDetail[1]", "PtcAbsCode", strLookupCode)
                            writeToNode(objInputXMLDoc, "n", "Applicants/ApplicantDetail[2]/Addresses/AddressDetail[1]", "PtcAbsCode", strLookupCode)
                            writeToNode(objInputXMLDoc, "n", "SecurityAddress", "PtcAbsCode", strLookupCode)

                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
		Else
			response.Write("no")
			response.End()
        
		
		End If
        dsCache = Nothing

        Dim strXML As String = objInputXMLDoc.InnerXml
        strXML = strXML.Replace("<QuotationSoftSearchDetail", "<QuotationSoftSearchDetail xmlns=""http://schemas.datacontract.org/2004/07/Shawbrook.Secured.BrokerApi.Service.Models.Domain""")
        objInputXMLDoc = Nothing

        objInputXMLDoc = New XmlDocument

        objInputXMLDoc.LoadXml(strXML)

        If (Request("debug") = "y") Then
            HttpContext.Current.Response.ContentType = "text/xml"
            HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
            HttpContext.Current.Response.End()
        End If

    End Sub

    Private Function processResponse(strResponse As String) As SoftSearchResponseTrimmed

        Dim xmlResponse As XmlDocument = New XmlDocument
        Dim bestPlan As New SoftSearchResponseTrimmed

        xmlResponse.LoadXml(strResponse.Replace("xmlns=""http://schemas.datacontract.org/2004/07/Shawbrook.Secured.BrokerApi.Service.Models.Domain""", ""))

        If (Request("debugResponse") = "y") Then
            HttpContext.Current.Response.ContentType = "text/xml"
            HttpContext.Current.Response.Write(xmlResponse.InnerXml)
            HttpContext.Current.Response.End()
        End If

        Dim nsmgr = New XmlNamespaceManager(xmlResponse.NameTable)
        nsmgr.AddNamespace("d2p1", "http://schemas.datacontract.org/2004/07/Shawbrook.Secured.Entities")

        'Dim ndlEligiblePlans As XmlNodeList = xmlResponse.SelectNodes("//EligiblePlanDetails/ProductCalculatedDetails")
        Dim ndlEligiblePlans As XmlNodeList = xmlResponse.SelectNodes("/SoftSearchResponse/EligiblePlanDetails/d2p1:ProductCalculatedDetails", nsmgr)

        strShawbrookCaseID = xmlResponse.SelectSingleNode("/SoftSearchResponse/ShawbrookCaseId").InnerText

        If (IsNothing(ndlEligiblePlans) Or ndlEligiblePlans.Count = 0) Then

            HttpContext.Current.Response.Clear()
            updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", strShawbrookCaseID, "NULL")
            updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "SHWDEC", "NULL")
            saveNote(AppID, intHotkeyUserID, "Application No. " & strShawbrookCaseID & " Declined by Shawbrook - No eligible plans.")
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Shawbrook Declined - No eligible plans.", objInputXMLDoc.InnerXml)
            xmlResponseID = saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Shawbrook Declined - No eligible plans.", xmlResponse.InnerXml)
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Shawbrook: No eligible plans.") & "|" & xmlResponseID.ToString)

        Else

            Dim EligablePlans As List(Of SoftSearchResponseTrimmed) = New List(Of SoftSearchResponseTrimmed)

            For Each ndPlan As XmlNode In ndlEligiblePlans

                Dim softSearchResponseTrimmed As New SoftSearchResponseTrimmed
                Dim strLTVRangeID As String = ndPlan.SelectSingleNode("d2p1:LtvRangeId", nsmgr).InnerText
                Dim decAnnualRate As Decimal = Decimal.Parse(ndPlan.SelectSingleNode("d2p1:FixedAnnualRate", nsmgr).InnerText)

                softSearchResponseTrimmed.AnnualRate = decAnnualRate
                softSearchResponseTrimmed.LTVRangeID = strLTVRangeID

                EligablePlans.Add(softSearchResponseTrimmed)


            Next

            Dim annualRate As Decimal = 90000000000000000
            Dim intPosSel As Int16 = -1
            Dim intPos As Int16 = 0

            For Each plan As SoftSearchResponseTrimmed In EligablePlans

                If (plan.AnnualRate < annualRate) Then
                    annualRate = plan.AnnualRate
                    intPosSel = intPos
                End If

                intPos = intPos + 1
            Next


            bestPlan = EligablePlans(intPosSel)


            Response.Clear()
            updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", strShawbrookCaseID, "NULL")
            saveNote(AppID, intHotkeyUserID, "Application No. " & strShawbrookCaseID + " " + ndlEligiblePlans.Count.ToString + " plans returned by Shawbrook.")
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, ndlEligiblePlans.Count.ToString + " plans returned by Shawbrook.", objInputXMLDoc.InnerXml)
            xmlResponseID = saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, ndlEligiblePlans.Count.ToString + " plans returned by Shawbrook.", xmlResponse.InnerXml)
            HttpContext.Current.Response.Write(99 & "|" & encodeURL("Accepted by Shawbrook.") & "|" & xmlResponseID.ToString & "|" & bestPlan.LTVRangeID)



        End If

        Return bestPlan

    End Function

    Private Function submitSoftSearch(strXml As String) As String

        Dim url As String = strXMLURL & "api/quotation/quotationsoftsearched"


        Dim message As String = strXml

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(message)
                .ContentType = "text/xml"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(strUserName & ":" & strPassword)))
                .Accept = "application/xml"
            End With
            Try
                Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
                With objWriter
                    .Write(message)
                    .Close()
                End With
            Catch err As Exception
                Throw err
            End Try
          '  Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Dim respStream As Stream = objResponse.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(respStream, Encoding.ASCII)
                Dim strResponse = reader.ReadToEnd

                'Dim dPAAuthorisation As DPAAuthorisation = DirectCast(JavaScriptConvert.DeserializeObject(strResponse, GetType(DPAAuthorisation)), DPAAuthorisation)

                Return strResponse
           ' Catch err As Exception
               ' Throw err
            'End Try
        Catch e As System.UriFormatException
            Throw e
        End Try
    End Function

    Private Function getDPAAuth() As DPAAuthorisation

        Dim url As String = strXMLURL & "api/consentforms"

        Dim message As String = "<RequestForConsentForm xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://schemas.datacontract.org/2004/07/Shawbrook.Secured.BrokerApi.Service.Models.Domain"">" & vbCrLf
        message += "<BrokerUserEmail>" & strUserName & "</BrokerUserEmail>" & vbCrLf
        message += "</RequestForConsentForm>"

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(message)
                .ContentType = "text/xml"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(strUserName & ":" & strPassword)))
                .Accept = "application/json"
            End With
            Try
                Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
                With objWriter
                    .Write(message)
                    .Close()
                End With
            Catch err As Exception
                Throw err
            End Try
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Dim respStream As Stream = objResponse.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(respStream, Encoding.ASCII)
                Dim strResponse = reader.ReadToEnd

                Dim dPAAuthorisation As DPAAuthorisation = DirectCast(JavaScriptConvert.DeserializeObject(strResponse, GetType(DPAAuthorisation)), DPAAuthorisation)

                Return dPAAuthorisation
            Catch err As Exception
                Throw err
            End Try
        Catch e As System.UriFormatException
            Throw e
        End Try

    End Function

    Private Function getAddressMatchResponse(strHouseNumber As String, strPostCode As String) As AddressMatchResponse

        Dim url As String = strXMLURL & "api/helpers/addressmatch"

        Dim message As String = "<AddressMatchDetail xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://schemas.datacontract.org/2004/07/Shawbrook.Secured.BrokerApi.Service.Models.Domain"">" & vbCrLf
        message += "<HouseNameOrNumber>" & strHouseNumber & "</HouseNameOrNumber>" & vbCrLf
        message += "<PostCode>" & strPostCode & "</PostCode>" & vbCrLf
        message += "</AddressMatchDetail>"

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(message)
                .ContentType = "text/xml"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(strUserName & ":" & strPassword)))
                .Accept = "application/json"
            End With
            Try
                Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
                With objWriter
                    .Write(message)
                    .Close()
                End With
            Catch err As Exception
                Throw err
            End Try
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Dim respStream As Stream = objResponse.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(respStream, Encoding.ASCII)
                Dim strResponse = reader.ReadToEnd

                Dim addressMatchResponse As AddressMatchResponse = DirectCast(JavaScriptConvert.DeserializeObject(strResponse, GetType(AddressMatchResponse)), AddressMatchResponse)

                Return addressMatchResponse
            Catch err As Exception
                Throw err
            End Try
        Catch e As System.UriFormatException
            Throw e
        End Try

    End Function

    Private Function splitFullName(ByVal strFullName As String, ByVal returnType As String) As String
        Dim boolTitleSet As Boolean = False
        Dim strTitle As String = "", strFirstName As String = "", strMiddleNames As String = "", strSurname As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                strSurname = arrFullName(i)
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                strTitle = arrFullName(i)
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                strFirstName = arrFullName(i)
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                strFirstName = arrFullName(i)
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        Select Case returnType
            Case "title"
                Return strTitle
            Case "firstname"
                Return strFirstName
            Case "middlenames"
                Return strMiddleNames
            Case "surname"
                Return strSurname
            Case Else
                Return strFullName
        End Select
    End Function

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)

        Dim whereami As String = ""

        Try
            If (man = "y") And (Not checkValue(val)) Then
                strErrorMessage += fld & ","
            Else
                If (man = "y") Or ((man = "n") And (val <> "")) Then
                    val = regexReplace("<", "&lt;", val)
                    val = regexReplace(">", "&gt;", val)
                    val = regexReplace(" & ", " and ", val)
                    val = regexReplace(" ' ", "&apos;", val)
                    val = regexReplace("""", "&quot;", val)

                    Dim objTest As XmlNode = Nothing

                    If (parent = fld) Then
                        objTest = doc.SelectSingleNode("//" & fld)
                    Else
                        objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                    End If

                    If Not (objTest Is Nothing) Then
                        objTest.InnerText = val
                    Else

                        Dim objApplication As XmlNode = Nothing
                        objApplication = doc.SelectSingleNode("//" & parent)
                        Dim objNewNode As XmlElement = doc.CreateElement(fld)
                        Dim objNewText As XmlText = doc.CreateTextNode(val)
                        objNewNode.AppendChild(objNewText)
                        objApplication.AppendChild(objNewNode)


                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function insertElement(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertAfter(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function insertElementBefore(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertBefore(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Try
            Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name)
            If (objTest Is Nothing) Then
                Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
                Dim objNewNode As XmlElement = doc.CreateElement(fld)
                objApplication.AppendChild(objNewNode)
                Return objNewNode
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Response.Clear()
            ' responseWrite(parent + " " + name + " " + fld)
            '  responseEnd()

            Return Nothing
        End Try


    End Function

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & "VALUES(" & formatField(CompanyID, "N", 0) & ", " & formatField(ip, "", "") & ", " & formatField(soapid, "N", 0) & ", " & formatField(result, "N", 0) & ", " & formatField(msg, "", "") & ", " & formatField(xml, "", "") & ", " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)

    End Function


    Private Sub setAppDetail2()

        Dim tr As IO.TextReader = New IO.StreamReader(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/shawbrook/appdetailnode.txt"))
        strAppDetail2 = tr.ReadToEnd

    End Sub

    Private Sub setAddress1()

        Dim tr As IO.TextReader = New IO.StreamReader(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/shawbrook/addressdetailnode.txt"))
        strAddressDetail1 = tr.ReadToEnd

    End Sub

    Private Sub setAddress2()

        Dim tr As IO.TextReader = New IO.StreamReader(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/shawbrook/addressdetailnode.txt"))
        strAddressDetail2 = tr.ReadToEnd

    End Sub

    Public Sub SetBasicAuthHeader(request As WebRequest, userName As [String], userPassword As [String])
        Dim authInfo As String = Convert.ToString(userName) & ":" & Convert.ToString(userPassword)
        authInfo = Convert.ToBase64String(Encoding.[Default].GetBytes(authInfo))
        request.Headers("Authorization") = "Basic " & authInfo
    End Sub

#Region "Classes"

    Public Class AddressDetail
        Public Property HouseNo() As String
            Get
                Return m_HouseNo
            End Get
            Set(value As String)
                m_HouseNo = value
            End Set
        End Property
        Private m_HouseNo As String
        Public Property Street1() As String
            Get
                Return m_Street1
            End Get
            Set(value As String)
                m_Street1 = value
            End Set
        End Property
        Private m_Street1 As String
        Public Property Street2() As String
            Get
                Return m_Street2
            End Get
            Set(value As String)
                m_Street2 = value
            End Set
        End Property
        Private m_Street2 As String
        Public Property District() As String
            Get
                Return m_District
            End Get
            Set(value As String)
                m_District = value
            End Set
        End Property
        Private m_District As String
        Public Property PostTown() As String
            Get
                Return m_PostTown
            End Get
            Set(value As String)
                m_PostTown = value
            End Set
        End Property
        Private m_PostTown As String
        Public Property County() As String
            Get
                Return m_County
            End Get
            Set(value As String)
                m_County = value
            End Set
        End Property
        Private m_County As String
        Public Property Postcode() As String
            Get
                Return m_Postcode
            End Get
            Set(value As String)
                m_Postcode = value
            End Set
        End Property
        Private m_Postcode As String
        Public Property ResidentialStatusId() As EnumResidentialStatuses
            Get
                Return m_ResidentialStatusId
            End Get
            Set(value As EnumResidentialStatuses)
                m_ResidentialStatusId = value
            End Set
        End Property
        Private m_ResidentialStatusId As EnumResidentialStatuses
        Public Property TimeAtAddressInMonths() As Integer
            Get
                Return m_TimeAtAddressInMonths
            End Get
            Set(value As Integer)
                m_TimeAtAddressInMonths = value
            End Set
        End Property
        Private m_TimeAtAddressInMonths As Integer
        Public Property Type() As EnumAddressTypes
            Get
                Return m_Type
            End Get
            Set(value As EnumAddressTypes)
                m_Type = value
            End Set
        End Property
        Private m_Type As EnumAddressTypes
        Public Property PtcAbsCode() As String
            Get
                Return m_PtcAbsCode
            End Get
            Set(value As String)
                m_PtcAbsCode = value
            End Set
        End Property
        Private m_PtcAbsCode As String
    End Class

    Private Class AddressMatchResponse
        Public Property HouseNameOrNumber() As String
            Get
                Return m_HouseNameOrNumber
            End Get
            Set(value As String)
                m_HouseNameOrNumber = value
            End Set
        End Property
        Private m_HouseNameOrNumber As String
        Public Property PostCode() As String
            Get
                Return m_PostCode
            End Get
            Set(value As String)
                m_PostCode = value
            End Set
        End Property
        Private m_PostCode As String
        Public Property NumberOfMatches() As Integer
            Get
                Return m_NumberOfMatches
            End Get
            Set(value As Integer)
                m_NumberOfMatches = value
            End Set
        End Property
        Private m_NumberOfMatches As Integer
        Public Property Match1PtcAbsCode() As String
            Get
                Return m_Match1PtcAbsCode
            End Get
            Set(value As String)
                m_Match1PtcAbsCode = value
            End Set
        End Property
        Private m_Match1PtcAbsCode As String
        Public Property MatchingAddresses() As List(Of AddressDetail)
            Get
                Return m_MatchingAddresses
            End Get
            Set(value As List(Of AddressDetail))
                m_MatchingAddresses = value
            End Set
        End Property
        Private m_MatchingAddresses As List(Of AddressDetail)
    End Class

    Private Class DPAAuthorisation

        Private _DateIssued As DateTime
        Public Property DateIssued() As DateTime
            Get
                Return _DateIssued
            End Get
            Set(ByVal value As DateTime)
                _DateIssued = value
            End Set
        End Property

        Private _Key As Guid
        Public Property Key() As Guid
            Get
                Return _Key
            End Get
            Set(ByVal value As Guid)
                _Key = value
            End Set
        End Property

        Private _Script As String
        Public Property Script() As String
            Get
                Return _Script
            End Get
            Set(ByVal value As String)
                _Script = value
            End Set
        End Property

    End Class

    Public Class ProductSelectionDetail
        ''' <summary>
        ''' Shawbrooks unique case identifier for the application just created
        ''' </summary>
        Public Property ShawbrookCaseId() As Integer
            Get
                Return m_ShawbrookCaseId
            End Get
            Set(value As Integer)
                m_ShawbrookCaseId = value
            End Set
        End Property
        Private m_ShawbrookCaseId As Integer

        ''' <summary>
        ''' ltv range id for the selected plan - available from eligible and ineligible plans returned in soft search
        ''' </summary>
        Public Property LtvRangeId() As Integer
            Get
                Return m_LtvRangeId
            End Get
            Set(value As Integer)
                m_LtvRangeId = value
            End Set
        End Property
        Private m_LtvRangeId As Integer

    End Class

    Public Enum EnumAddressTypes
        Unset = 0
        Security = 1
        Permanent = 2
        Previous = 3
        Employer = 4
        BankBuildingSociety = 5
        LandRegistry = 6
        Mortgage = 7
        LandRegistryForBtlHome = 8
        LandRegistryLinked = 9
        LandRegistryLinkedForBtlHome = 10
    End Enum

    Public Enum EnumResidentialStatuses
        OwnerOccupied = 1
        BuyToLet = 2
    End Enum

    Public Class SoftSearchResponseTrimmed

        Private _LTVRangeID As String
        Public Property LTVRangeID() As String
            Get
                Return _LTVRangeID
            End Get
            Set(ByVal value As String)
                _LTVRangeID = value
            End Set
        End Property

        Private _AnnualRate As Decimal
        Public Property AnnualRate() As Decimal
            Get
                Return _AnnualRate
            End Get
            Set(ByVal value As Decimal)
                _AnnualRate = value
            End Set
        End Property



    End Class

#End Region

    Private Sub selectPlan(strPlanID As String)

        Dim url As String = strXMLURL & "api/quotation/quotationselectproduct"

        Dim strSQL As String = "SELECT [XMLReceived]" & "FROM [tblxmlreceived] " & "WHERE [XMLReceivedID] = " & strXmlID

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()

        If (dsCache.Rows.Count > 0) Then

            Dim strSavedResponse As String = dsCache.Rows(0).Item(0).ToString
            Dim xmlSavedResponse As XmlDocument = New XmlDocument

            xmlSavedResponse.LoadXml(strSavedResponse)

            Dim nsmgr = New XmlNamespaceManager(xmlSavedResponse.NameTable)
            nsmgr.AddNamespace("d2p1", "http://schemas.datacontract.org/2004/07/Shawbrook.Secured.Entities")

            strShawbrookCaseID = xmlSavedResponse.SelectSingleNode("/SoftSearchResponse/ShawbrookCaseId").InnerText

        End If

        Dim message As String = "<ProductSelectionDetail xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://schemas.datacontract.org/2004/07/Shawbrook.Secured.BrokerApi.Service.Models.Domain"">"
        message += "<LtvRangeId>" & strPlanID & "</LtvRangeId>"
        message += "<ShawbrookCaseId>" & strShawbrookCaseID & "</ShawbrookCaseId>"
        message += "</ProductSelectionDetail>"

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Put
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(message)
                .ContentType = "text/xml"
                .Timeout = 60000
                .ReadWriteTimeout = 30000
                .Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(strUserName & ":" & strPassword)))
                .Accept = "application/xml"
            End With
            Try
                Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
                With objWriter
                    .Write(message)
                    .Close()
                End With
            Catch err As Exception
                Throw err
            End Try
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Dim respStream As Stream = objResponse.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(respStream, Encoding.ASCII)
                Dim strResponse = reader.ReadToEnd

                HttpContext.Current.Response.Clear()
                saveNote(AppID, intHotkeyUserID, "Automatically selected Plan ID:" & strPlanID & " for Shawbrook Case ID: " & strShawbrookCaseID + ".")
                updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "SHWACC", "NULL")
                saveNote(AppID, intHotkeyUserID, "Application No. " & strShawbrookCaseID & " Accepted by Shawbrook.")
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Selected Plan ID:" & strPlanID & " for Shawbrook Case ID: " & strShawbrookCaseID + ".", strResponse)
                HttpContext.Current.Response.Write(1 & "|" & encodeURL("Accepted by Shawbrook.") & "|" & strXmlID & "|" & strPlanID)


            Catch err As Exception
                Throw err
            End Try
        Catch e As System.UriFormatException
            Throw e
        End Try



    End Sub

    Private Function formatField(ByVal val As String, ByVal typ As String, ByVal dflt As String) As String
        Dim strTemp As String = "", strValClean As String = "", strFinished As String = "n", strPrev As String = ""
        If (Not checkValue(val)) Then
            If (dflt = "NULL") Then
                Return dflt
                strFinished = "y"
            Else
                val = Trim(dflt)
            End If
        End If
        If (strFinished = "n") Then
            strValClean = Replace(val, "'", "''")
            Select Case typ
                Case "U"
                    Return "'" & Trim(UCase(strValClean)) & "'"
                Case "L"
                    Return "'" & Trim(LCase(strValClean)) & "'"
                Case "T"
                    Dim i As Integer
                    For i = 1 To Len(strValClean)
                        If (i = 1) Or (strPrev = " ") Or (strPrev = "-") Then
                            strTemp = strTemp & UCase(Mid(strValClean, i, 1))
                        Else
                            strTemp = strTemp & LCase(Mid(strValClean, i, 1))
                        End If
                        strPrev = Mid(strValClean, i, 1)
                    Next
                    Return "'" & Trim(strTemp) & "'"
                Case "M"
                    Return "CONVERT(money," & val & ")"
                Case "N"
                    Return Trim(strValClean)
                Case "B"
                    If (strValClean = "True") Or (UCase(strValClean) = "Y") Or (UCase(strValClean) = "YES") Or (strValClean = "1") Or (strValClean = "on") Then
                        Return "1"
                    Else
                        Return "0"
                    End If
                Case "TM"
                    Return "'" & TimeValue(strValClean) & "'"
                Case "DT"
                    If (checkValue(strValClean)) Then
                        Return "CONVERT(DateTime, '" & strValClean & "',103)"
                    Else
                        Return "NULL"
                    End If
                Case "DTISO"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & "'"
                    Else
                        Return "NULL"
                    End If
                Case "DTTM"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & " " & TimeValue(val) & "'"
                    Else
                        Return "NULL"
                    End If
                Case Else
                    Return "'" & Trim(strValClean) & "'"
            End Select
        Else
            Return ""
        End If
    End Function







End Class
