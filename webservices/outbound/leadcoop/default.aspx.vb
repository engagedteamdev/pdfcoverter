﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.leadcoop.webservice

Partial Class XMLLeadCoop
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        incrementTransferAttempts(AppID, strMediaCampaignID)
        Common.postEmail("", "itsupport@engaged-solutions.co.uk", "Lead Coop Data Error", strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub generateXml()

        'If (strEnvironment = "live") Then
        '	strXMLURL = "http://webservice.leadcoop.co.uk/mortgage.asmx/SubmitLead"
        'Else
        strXMLURL = "http://webservice.leadcoop.co.uk/mortgage.asmx/SubmitLead"
        'End If

        Dim strBrokerGUID As String = findBroker()
        If checkValue(AppID) Then
            If (checkValue(strBrokerGUID)) Then
                sendXML(strBrokerGUID)
            End If
        End If
    End Sub

    Private Function findBroker() As String
        Dim objService As Mortgage = New Mortgage
        Dim strBrokerGUID As String = "", strFormProductCategory As String = "", intMortgageSize As String = "", intPropertyValue As String = "", intFTB As String = "", intBadCredit As String = "", strPostCode As String = "", strIP As String = ""
        Dim strSQL As String = "SELECT * FROM vwxmlleadcoop WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strFormProductCategory = Row.Item("FormProductCategory")
                intMortgageSize = Row.Item("MortgageSize")
                intPropertyValue = Row.Item("PropertyValue")
                intFTB = Row.Item("FirstTimeBuyer")
                intBadCredit = Row.Item("BadCredit")
                strPostCode = Row.Item("Postcode")
                strIP = Row.Item("IPAddress")
            Next
        End If
        dsCache = Nothing
        Dim objResponse As WSLeadResponse = objService.FindBroker(strMediaCampaignCampaignReference, strFormProductCategory, intMortgageSize, intPropertyValue, intFTB, intBadCredit, strPostCode, strIP)
        Select Case objResponse.intReturnCode
            Case "1"
                strBrokerGUID = objResponse.strFindBrokerRequestGUID
            Case Else
                saveNote(AppID, Config.DefaultUserID, "Application Declined by Lead Coop: " & objResponse.strReturnMessage)
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Lead Coop: " & objResponse.strReturnMessage))
                incrementTransferAttempts(AppID, strMediaCampaignID)
        End Select
        Return strBrokerGUID
    End Function

    Private Sub sendXML(ByVal brokerGUID As String)
        Dim objService As Mortgage = New Mortgage
        Dim strFormProductCategory As String = "", intMortgageSize As String = "", intPropertyValue As String = "", intFTB As String = "", intBadCredit As String = "", strPostCode As String = "", strIP As String = ""
        Dim strTitle As String = "", strFirstName As String = "", strSurname As String = "", intAge As String = "", strHomePhone As String = "", strWorkPhone As String = "", strMobilePhone As String = "", strEmail As String = ""
        Dim strAdd1 As String = "", strAdd2 As String = "", strTown As String = "", strCounty As String = "", intCampaignID As String = ""
        Dim strSQL As String = "SELECT * FROM vwxmlleadcoop WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strFormProductCategory = Row.Item("FormProductCategory").ToString
                intMortgageSize = Row.Item("MortgageSize")
                intPropertyValue = Row.Item("PropertyValue")
                intFTB = Row.Item("FirstTimeBuyer")
                intBadCredit = Row.Item("BadCredit")
                strPostCode = Row.Item("Postcode").ToString
                strIP = Row.Item("IPAddress").ToString
                strTitle = Row.Item("Title").ToString
                strFirstName = Row.Item("FirstName").ToString
                strSurname = Row.Item("Surname").ToString
                intAge = Row.Item("Age").ToString
                strHomePhone = Row.Item("HomePhone").ToString
                strWorkPhone = Row.Item("WorkPhone").ToString
                strMobilePhone = Row.Item("MobilePhone").ToString
                strEmail = Row.Item("Email").ToString
                strAdd1 = Row.Item("Address1").ToString
                strAdd2 = Row.Item("Address1").ToString
                strTown = Row.Item("Address1").ToString
                strCounty = Row.Item("Address1").ToString
                intCampaignID = Row.Item("CampaignID").ToString
            Next
        End If
        dsCache = Nothing
        Dim objResponse As WSLeadResponse = objService.SubmitLead(strMediaCampaignCampaignReference, strFormProductCategory, intMortgageSize, intPropertyValue, intFTB, intBadCredit, brokerGUID, strTitle, strFirstName, strSurname, 0, 0, 0, intAge, strHomePhone, strWorkPhone, strMobilePhone, strEmail, strAdd1, strAdd2, strTown, strCounty, strPostCode, 0, AppID, intCampaignID, strIP, "", "")
        Select Case objResponse.intReturnCode
            Case "1"
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Lead Coop"))
                updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objResponse.intLeadId, "NULL")
                updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignCostOutbound", "", objResponse.dblLeadCost, "NULL")
                setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Received From Lead Coop", "")
            Case Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Lead Coop: " & objResponse.strReturnMessage))
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Received From Lead Coop: " & objResponse.strReturnMessage, "")
                incrementTransferAttempts(AppID, strMediaCampaignID)
        End Select
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
