﻿Imports Config, Common
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Xml.Xsl

Partial Class XMLCrafty
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAccessKey As String = ""
    Private strPostCode As String = HttpContext.Current.Request("postcode")
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Public Sub generateXml()
        If (strEnvironment = "live") Then
        	strXMLURL = "http://pcls1.craftyclicks.co.uk/xml/rapidaddress?postcode=" & strPostCode
        Else
        	strXMLURL = "http://pcls1.craftyclicks.co.uk/xml/rapidaddress?postcode=" & strPostCode
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Crafty%' AND CompanyID = 0"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "CraftyAccessKey") Then strAccessKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        sendXML()
    End Sub

    Private Sub sendXML()

        ' Post the SOAP message.
        Dim objResponse As HttpWebResponse = getWebRequest(strXMLURL & "&key=" & strAccessKey)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

        objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

        'responseWrite(objOutputXMLDoc.InnerXml)
        'responseEnd()

        objReader.Close()
        objReader = Nothing

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<?xml version=""1.0"" encoding=""UTF-8""?>")
            If (objResponse.StatusCode.ToString = "OK") Then
                ' Parse the XML document.

                Dim strErrorMessage As String = getNodeText(objOutputXMLDoc, Nothing, "//error_msg")
                Dim strStreetName As String = getNodeText(objOutputXMLDoc, Nothing, "//thoroughfare_name") & " " & getNodeText(objOutputXMLDoc, Nothing, "//thoroughfare_descriptor")
                Dim strLocality As String = getNodeText(objOutputXMLDoc, Nothing, "//dependent_locality")
                Dim strTown As String = getNodeText(objOutputXMLDoc, Nothing, "//town")
                Dim strCounty As String = getNodeText(objOutputXMLDoc, Nothing, "//postal_county")
                Dim strPostCode As String = getNodeText(objOutputXMLDoc, Nothing, "//postcode")
                Dim objAddresses As XmlNodeList = objOutputXMLDoc.SelectNodes("//delivery_point")

                If (checkValue(strErrorMessage)) Then
                    .WriteLine("<Addresses>")
                    .WriteLine("<Error>" & strErrorMessage & "</Error>")
                    .WriteLine("</Addresses>")
                Else
                    .WriteLine("<Addresses>")
                    For Each objAddress As XmlNode In objAddresses
                        Dim strBuildingName As String = getNodeText(objAddress, Nothing, "building_name")
                        Dim strSubBuildingName As String = getNodeText(objAddress, Nothing, "sub_building_name")
                        Dim strHouseName As String = strSubBuildingName
                        If (checkValue(strBuildingName)) Then
                            strHouseName += " " & strBuildingName
                        End If
                        .WriteLine("<Address>")
                        .WriteLine("<ID>" & getNodeText(objAddress, Nothing, "udprn") & "</ID>")
                        .WriteLine("<Organisation>" & getNodeText(objAddress, Nothing, "organisation_name") & "</Organisation>")                        
						.WriteLine("<BuildingName>" & Trim(strHouseName) & "</BuildingName>")
                        .WriteLine("<BuildingNumber>" & getNodeText(objAddress, Nothing, "building_number") & "</BuildingNumber>")
                        .WriteLine("<StreetName>" & strStreetName & "</StreetName>")
                        .WriteLine("<Locality>" & strLocality & "</Locality>")
                        .WriteLine("<Town>" & strTown & "</Town>")
                        .WriteLine("<County>" & strCounty & "</County>")
                        .WriteLine("<PostCode>" & strPostCode & "</PostCode>")
                        .WriteLine("</Address>")
                    Next
                    .WriteLine("</Addresses>")
                    Dim strSQL As String = "UPDATE tblsystemconfiguration SET SystemConfigurationValue = SystemConfigurationValue + 1 WHERE SystemConfigurationName = 'AddressLookupAttempts' AND CompanyID = '" & CompanyID & "' "
                    executeNonQuery(strSQL)
                End If
            Else
                .WriteLine("<Addresses></Addresses")
            End If
        End With

        objResponse = Nothing
        objOutputXMLDoc = Nothing

        'Dim strInput As String = Replace(objStringWriter.ToString, "&", "&amp;")
        'Dim objXML As XmlDocument = New XmlDocument
        'objXML.LoadXml(strInput)

        'Dim objXSL As XslCompiledTransform = New XslCompiledTransform
        'objXSL.Load(Server.MapPath("sort.xsl"))

        'Dim objSortedXML As XmlDocument = New XmlDocument(objXML.CreateNavigator().NameTable())
        'Using objWriter As XmlWriter = objSortedXML.CreateNavigator().AppendChild()
            'objXSL.Transform(New XmlNodeReader(objXML), objWriter)
        'End Using

        HttpContext.Current.Response.ContentType = "text/xml"
        responseWrite(objStringWriter.ToString())
        objStringWriter = Nothing

    End Sub

End Class
