﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.paymex.leadprocessor

Partial Class XMLBainesErnst
    Inherits System.Web.UI.Page

    Private strAuthURL As String = "", strXMLURL As String = ""
    Private strUserName As String = "", strPassword As String = "", strSource As String = "", strCustomerEmailID As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Public Sub generateXml()

        'If (strEnvironment = "live") Then
        'strXMLURL = "https://leadprocessor.paymex.co.uk/Leadprocessor.asmx"
        'Else
        strXMLURL = "https://leadprocessor.paymex.co.uk/Leadprocessor.asmx" '"https://leadprocessor.paymex.co.uk:8843/Leadprocessor.asmx"
        'End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%BainesErnst%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "BainesErnstUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BainesErnstPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BainesErnstSource") Then strSource = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        strCustomerEmailID = getAnyField("MediaCampaignOutboundLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/bainesernst/blank.xml"))
        'objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        'objNSM.AddNamespace("def", "http://bainesandernst.co.uk/webservices")

        writeToNode(objInputXMLDoc, "y", "Lead", "Source", strSource)
        writeToNode(objInputXMLDoc, "y", "Lead", "SubSource", strMediaCampaignCampaignReference)

        Dim strSQL As String = "SELECT * FROM vwxmlbainesernst WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                            ' Handle non standard fields
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	

            Dim objService As LeadProcessor = New LeadProcessor
            Dim strResponse As String = objService.SubmitLead(strUserName, strPassword, objInputXMLDoc.InnerXml)

            objOutputXMLDoc.LoadXml(strResponse)

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            ' Parse the XML document.
            Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//Response")
            Dim strMessage As String = ""
            Select Case objApplication.SelectSingleNode("ReturnCode", objNSM2).InnerText
                Case "0"
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Baines & Ernst"))
                    updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("InternalReference").InnerText, "NULL")
                    setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                    If (checkValue(strCustomerEmailID)) Then
                        sendEmails(AppID, strCustomerEmailID, CompanyID)
                    End If
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Baines & Ernst", objInputXMLDoc.InnerXml)
					saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Baines & Ernst", objOutputXMLDoc.InnerXml)
                Case "3", "6", "7"
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Baines & Ernst: Duplicate"))
                    saveUpdatedDate(AppID, Config.DefaultUserID, "PartnerDuplicate")
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Baines & Ernst", objInputXMLDoc.InnerXml)
					saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Baines & Ernst", objOutputXMLDoc.InnerXml)
                    incrementTransferAttempts(AppID, strMediaCampaignID)
                    incrementTransferAttempts(AppID, strMediaCampaignID)
                    incrementTransferAttempts(AppID, strMediaCampaignID)
                Case Else
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Baines & Ernst"))
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Baines & Ernst", objInputXMLDoc.InnerXml)
					incrementTransferAttempts(AppID, strMediaCampaignID)
            End Select

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
