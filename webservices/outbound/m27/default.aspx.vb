﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.brokertoolbox.www

Partial Class XMLM27
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID"), UserID As String = HttpContext.Current.Request("UserID"), XMLReceivedID As String = HttpContext.Current.Request("XMLReceivedID"), QuoteID As String = HttpContext.Current.Request("QuoteID"), strViewDocuments As String = HttpContext.Current.Request("frmViewDocuments"), strSaveDocuments As String = HttpContext.Current.Request("frmSaveDocuments"), intNoQuotes As String = HttpContext.Current.Request("frmNoQuotes")
    Private boolViewDocuments As Boolean = False, boolSaveDocuments As Boolean = False
    Private intSOAPRequestNo As String = getIndexNumber()
    Private objAuthXMLDoc As XmlDocument = New XmlDocument, objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strXMLURL As String = "", strXMLSOAPAction As String = "", strXMLFile As String = "", strXMLView As String = "", strApplicationSOAPAction As String = "", strApplicationFile As String = "", strApplicationView As String = "", strDocumentsSOAPAction As String = "", strDocumentsFile As String = "", strXMLNamespace As String = "", intCompanyID As String = "", intSiteID As String = "", strUserID As String = "", strAPIKey As String = ""
    Private intMode As String = HttpContext.Current.Request("intMode"), strRateType As String = HttpContext.Current.Request("frmRateType"), strTerm As String = HttpContext.Current.Request("frmProductTerm"), strLenderCodes As String = HttpContext.Current.Request("frmLenderCodes"), strFeeFree As String = HttpContext.Current.Request("frmFeeFree"), strAmount As String = HttpContext.Current.Request("frmAmount")
    Private strErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
        If (strViewDocuments = "Y") Then
            boolViewDocuments = True
        End If
        If (strSaveDocuments = "Y") Then
            boolSaveDocuments = True
        End If
        If (Not checkValue(intNoQuotes)) Then
            intNoQuotes = "5"
        End If
        If (Not checkValue(strRateType)) Then
            strRateType = "Fixed"
        End If
    End Sub

    Private Enum ServiceMode
        ' *** Use 30[x] for XML logging codes e.g. Sourcing success = 301 *** '
        Sourcing = 1
        Application = 2
        ViewDocuments = 3
        PreviousQuotes = 4
        GetLenders = 5
    End Enum

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " (Test) Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Sourcing Unsuccessfully Generated", objInputXMLDoc.InnerXml)
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub generateXml()
        If (strEnvironment = "live") Then
           strXMLURL = "http://www.brokertoolbox.co.uk/ServiceSourcing.asmx" 
            strXMLSOAPAction = "http://tempuri.org/SourceMortgage"
            strXMLFile = "factfind.xml"
            strXMLView = "vwxmlm27"
            'strApplicationSOAPAction = ""
            'strApplicationFile = ""
            'strApplicationView = ""
            'strDocumentsSOAPAction = ""
            'strDocumentsFile = ""
            strXMLNamespace = "http://tempuri.org"
        Else
            strXMLURL = "http://www.brokertoolbox.co.uk/ServiceSourcing.asmx" 
            strXMLSOAPAction = "http://tempuri.org/SourceMortgage"
            strXMLFile = "factfind.xml"
            strXMLView = "vwxmlm27"
            'strApplicationSOAPAction = ""
            'strApplicationFile = ""
            'strApplicationView = ""
            'strDocumentsSOAPAction = ""
            'strDocumentsFile = ""
            strXMLNamespace = "http://tempuri.org"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%M27%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "M27CompanyID") Then intCompanyID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27SiteID") Then intSiteID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27UserID") Then strUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27APIKey") Then strAPIKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            'checkMandatory()
            'getAuthorisation()
            Select Case intMode
                Case ServiceMode.Sourcing
                    getSourcing()
                Case ServiceMode.Application
                    'getApplication()
                Case ServiceMode.ViewDocuments
                    'getQuoteDocuments("Quote", boolViewDocuments, boolSaveDocuments)
                Case ServiceMode.PreviousQuotes
                    'getPreviousQuotes()
                Case ServiceMode.GetLenders
                    getLenders()
            End Select
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"App1Title", "App1DOB", "App2DOB", "PropertyType"}
        Dim arrNames As String() = {"Client Title", "Client DOB", "Partner DOB", "Property Type"}
        Dim strApp2Title As String = ""
        Dim strSQL As String = "SELECT App1Title, App1DOB, App2Title, App2DOB, PropertyType FROM tblapplications WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (Column.ColumnName = "App2Title") Then
                        strApp2Title = Row(Column).ToString
                    Else
                        For x As Integer = 0 To UBound(arrMandatory)
                            If (arrMandatory(x) = "App2DOB" And Column.ColumnName = "App2DOB") Then
                                If (checkValue(strApp2Title)) Then
                                    If (Not checkValue(Row(Column).ToString)) Then
                                        boolMandatory = False
                                        strMandatoryField = arrNames(x)
                                        Exit For
                                    End If
                                End If
                            ElseIf (arrMandatory(x) = Column.ColumnName) Then
                                If (Not checkValue(Row(Column).ToString)) Then
                                    boolMandatory = False
                                    strMandatoryField = arrNames(x)
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Next
            Next
        End If
        If (Not boolMandatory) Then
            SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo, "<Quotes><Error>Mandatory Data Missing: " & strMandatoryField & "</Error></Quotes>", objInputXMLDoc.InnerXml)
            responseEnd()
        Else
            dsCache = Nothing
        End If
    End Sub

    Private Sub getLenders()

        Dim objService As New ServiceSourcing
        Dim intNoOfLendersReturned As Integer
        Dim strResults As String = objService.GetListOfLenders(intSiteID, intNoOfLendersReturned)

        objService = Nothing

        objOutputXMLDoc.LoadXml(strResults)
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
        HttpContext.Current.Response.End()
    End Sub

    Private Sub getSourcing()

        Dim strXML As String = ""
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/m27/" & strXMLFile))

        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "CompanyID", intCompanyID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "SiteID", intSiteID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "UserID", strUserID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "APIKey", strAPIKey)

        Dim strSQL As String = "SELECT * FROM " & strXMLView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim intNumItems As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 0) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If
                        writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        createElement(objInputXMLDoc, "MortgageFactfind/Filters", strRateType, strRateType)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "Show", "Show_With_Feature")
        If (strRateType = "Variable" Or strRateType = "Capped") Then
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodFromMonths", -1)
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodToMonths", -1)
        Else
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodFromMonths", -1)
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodToMonths", -1)
        End If

        Dim strMortgageType As String = getAnyFieldFromDataStore("SourcingM27MortgageType", AppID)
        If (strMortgageType = "First Time Buyer") Then
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "MortgageType", "Standard")
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "ApplicantsDetail", "ApplicantsDetail")
            createElement(objInputXMLDoc, "MortgageFactfind/ApplicantsDetail", "Applicants", "Applicants")
            createElement(objInputXMLDoc, "MortgageFactfind/ApplicantsDetail/Applicants", "MortgageFactfindApplicant", "MortgageFactfindApplicant")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/ApplicantsDetail/Applicants/MortgageFactfindApplicant", "ApplicantType", lookupValue(strMortgageType, "M27", "tblsourcing"))
        End If

        If checkValue(strLenderCodes) Then
            Dim arrLenderCodes As Array = Split(strLenderCodes, ",")
            createElement(objInputXMLDoc, "MortgageFactfind/Filters", "Lenders", "Lenders")
            For x As Integer = 0 To UBound(arrLenderCodes)
                If (checkValue(arrLenderCodes(x))) Then
                    createElement(objInputXMLDoc, "MortgageFactfind/Filters/Lenders", "Lender[" & x + 1 & "]", "Lender")
                    writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/Lenders/Lender[" & x + 1 & "]", "Code", arrLenderCodes(x))
                End If
            Next
        End If

        If (strFeeFree = "Y") Then
            createElement(objInputXMLDoc, "MortgageFactfind/Filters", "Fees", "Fees")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/Fees", "FreeMainLenderFees", "Show_With_Feature")
        End If

        If checkValue(strTerm) Then

            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "Term", strTerm)

        End If

        If checkValue(strAmount) Then

            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "LoanRequired", strAmount)

        End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objService As New ServiceSourcing
            Dim intNumProducts As Integer, boolInputError As Boolean
            Dim strResults As String = objService.SourceMortgage(objInputXMLDoc.InnerXml, intNumProducts, boolInputError)
            objService = Nothing

            If (intNumProducts > 0) Then
                objOutputXMLDoc.LoadXml(strResults)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 301, "Mortgage 27 Sourcing Successfully Generated", objInputXMLDoc.InnerXml)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 301, "Mortgage 27 Sourcing Successfully Retrieved", objOutputXMLDoc.InnerXml)
                HttpContext.Current.Response.ContentType = "text/xml"
                HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                HttpContext.Current.Response.End()
            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 3011, "Mortgage 27 Sourcing Unsuccessfully Generated", objInputXMLDoc.InnerXml)
            End If

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    'Private Sub getApplication()

    '    Dim strXML As String = ""
    '    objInputXMLDoc.XmlResolver = Nothing
    '    objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/net/webservices/xmlfiles/paymentshield/" & strApplicationFile))
    '    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '    objNSM.AddNamespace("def", strXMLNamespace)

    '    writeToNode(objInputXMLDoc, "n", "def:SubmitHouseholdApplication/def:appInfo", "def:QuoteID", QuoteID, objNSM)
    '    writeToNode(objInputXMLDoc, "y", "def:SubmitHouseholdApplication", "def:LoginToken", strLoginToken, objNSM)

    '    Dim strSQL As String = "SELECT * FROM " & strApplicationView & " WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
    '    Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '    Dim strNote As String = ""
    '    If (dsCache.Rows.Count > 0) Then
    '        For Each Row As DataRow In dsCache.Rows
    '            For Each Column As DataColumn In dsCache.Columns
    '                If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
    '                    If (Column.ColumnName = "App2Title") Then
    '                        insertElement(objInputXMLDoc, "def:SubmitHouseholdApplication/def:appInfo/def:Applicant1", "def:SubmitHouseholdApplication/def:appInfo", "def:Applicant2", "Applicant2", objNSM)
    '                    ElseIf (Column.ColumnName = "PolicyStartDate") Then
    '                        writeToNode(objInputXMLDoc, "n", "def:appInfo", "def:PolicyStartDate", ddmmyyyy2yyyymmdd(Row(Column).ToString), objNSM)
    '                    Else
    '                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
    '                        Dim strParent As String = ""
    '                        If (UBound(arrName) > 0) Then
    '                            For y As Integer = 0 To UBound(arrName) - 1
    '                                If (y = 0) Then
    '                                    strParent += "def:" & arrName(y)
    '                                Else
    '                                    strParent += "/def:" & arrName(y)
    '                                End If
    '                            Next
    '                        Else
    '                            strParent = arrName(0)
    '                        End If
    '                        writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & arrName(UBound(arrName)), Row(Column).ToString, objNSM)
    '                    End If

    '                End If
    '            Next
    '        Next
    '    End If
    '    dsCache = Nothing

    '    'HttpContext.Current.Response.ContentType = "text/xml"
    '    'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
    '    'HttpContext.Current.Response.End()

    '    If (strErrorMessage <> "") Then
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.Write(strErrorMessage)
    '        HttpContext.Current.Response.End()
    '    Else
    '        ' Post the SOAP message.

    '        Dim objResponse As HttpWebResponse = postPaymentShieldWebRequest(strXMLURL, objInputXMLDoc.InnerXml, strApplicationSOAPAction)

    '        'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
    '        If (checkResponse(objResponse, "AppID=" & AppID)) Then
    '            Dim objReader As New StreamReader(objResponse.GetResponseStream())

    '            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
    '            objReader.Close()
    '            objReader = Nothing

    '            Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
    '            objNSM2.AddNamespace("def", strXMLNamespace)

    '            'HttpContext.Current.Response.ContentType = "text/xml"
    '            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
    '            'HttpContext.Current.Response.End()

    '            Dim strApplication As String = ""

    '            Dim objErrors As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:SubmitHouseholdApplicationResponse/def:SubmitHouseholdApplicationResult/def:AppErrors", objNSM)
    '            If (Not objErrors Is Nothing) Then
    '                strApplication = "<Application><Error>Could not proceed: " & regexReplace("Error [A-Z0-9]{0,20}: ", "", objErrors.InnerText) & "</Error></Application>"
    '                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 2021, "Application unsuccessfully generated", objInputXMLDoc.InnerXml)
    '                SOAPUnsuccessfulMessage(objErrors.InnerText, -5, AppID, strApplication, objOutputXMLDoc.InnerXml)
    '            Else
    '                Dim objReceipt As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:SubmitHouseholdApplicationResponse/def:SubmitHouseholdApplicationResult/def:AppReceiptNo", objNSM)
    '                If (Not objReceipt Is Nothing) Then
    '                    strApplication = "<Application><PolicyRef>" & objReceipt.InnerText & "</PolicyRef></Application>"
    '                End If
    '                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 2021, "Application successfully generated", objInputXMLDoc.InnerXml)
    '                SOAPSuccessfulMessage("Application successfully retrieved", 202, AppID, strApplication, objOutputXMLDoc.InnerXml)
    '            End If

    '        Else
    '            SOAPUnsuccessfulMessage("Connection error", -1, AppID, "<Quotes><Error>Connection error. Please contact Engaged Solutions</Error></Quotes>", objInputXMLDoc.InnerXml)
    '        End If
    '        objResponse.Close()
    '        objResponse = Nothing

    '    End If

    '    objInputXMLDoc = Nothing
    '    objOutputXMLDoc = Nothing

    'End Sub

    'Private Sub getQuoteDocuments(typ As String, view As Boolean, save As Boolean)

    '    Dim strXML As String = ""
    '    Dim objStringWriter As StringWriter = New StringWriter

    '    objInputXMLDoc.XmlResolver = Nothing
    '    objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/net/webservices/xmlfiles/paymentshield/" & strDocumentsFile))

    '    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '    objNSM.AddNamespace("def", strXMLNamespace)

    '    writeToNode(objInputXMLDoc, "n", "def:GetDocument", "def:quoteID", QuoteID, objNSM)
    '    writeToNode(objInputXMLDoc, "y", "def:GetDocument", "def:typeOfDocument", typ, objNSM)
    '    writeToNode(objInputXMLDoc, "y", "def:GetDocument", "def:LoginToken", strLoginToken, objNSM)

    '    'HttpContext.Current.Response.ContentType = "text/xml"
    '    'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
    '    'HttpContext.Current.Response.End()

    '    If (strErrorMessage <> "") Then
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.Write(strErrorMessage)
    '        HttpContext.Current.Response.End()
    '    Else
    '        ' Post the SOAP message.
    '        Dim objResponse As HttpWebResponse = postPaymentShieldWebRequest(strXMLURL, objInputXMLDoc.InnerXml, strDocumentsSOAPAction)

    '        'If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
    '        If (checkResponse(objResponse, "AppID=" & AppID)) Then
    '            Dim objReader As New StreamReader(objResponse.GetResponseStream())

    '            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
    '            objReader.Close()
    '            objReader = Nothing

    '            Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
    '            objNSM2.AddNamespace("def", strXMLNamespace)

    '            'HttpContext.Current.Response.ContentType = "text/xml"
    '            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
    '            'HttpContext.Current.Response.End()

    '            Dim strDocuments As String = ""

    '            Dim objErrors As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetDocumentResponse/def:GetDocumentResult/def:Errors", objNSM2)
    '            If (Not objErrors Is Nothing) Then
    '                strDocuments = "<Documents><Error>Could not get documents: " & regexReplace("Error GenBC[A-Z0-9]{0,20}: ", "", objErrors.InnerText) & "</Error></Documents>"
    '                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 2051, "Documents unsuccessfully generated", objInputXMLDoc.InnerXml)
    '                SOAPUnsuccessfulMessage(objErrors.InnerText, -5, AppID, strDocuments, objOutputXMLDoc.InnerXml)
    '            Else
    '                Dim strDocumentTitle As String = "", strDocumentBytes As String = "", strDocumentURL As String = ""
    '                Dim objDocument As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetDocumentResponse/def:GetDocumentResult/def:DocumentContent", objNSM2)
    '                Dim objDocumentTitle As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetDocumentResponse/def:GetDocumentResult/def:FileName", objNSM2)
    '                If (Not objDocument Is Nothing) Then
    '                    strDocumentBytes = objDocument.InnerText
    '                End If
    '                If (Not objDocumentTitle Is Nothing) Then
    '                    strDocumentTitle = "Payment Shield " & typ
    '                End If
    '                Dim objPDF As PDF = New PDF(Cache, AppID, 0, 0, 0)
    '                If (checkValue(strDocumentBytes)) Then
    '                    If (view And save) Then
    '                        strDocumentURL = objPDF.generateFromBytes(strDocumentBytes, UserID, strDocumentTitle)
    '                        saveNote(AppID, UserID, strDocumentTitle & " was saved for this case")
    '                    ElseIf (view) Then
    '                        strDocumentURL = objPDF.generateFromBytes(strDocumentBytes, UserID)
    '                    ElseIf (save) Then
    '                        objPDF.generateFromBytes(strDocumentBytes, UserID, strDocumentTitle)
    '                        saveNote(AppID, UserID, strDocumentTitle & " was saved for this case")
    '                    End If
    '                End If
    '                strDocuments = "<Documents><Document><Title>" & strDocumentTitle & "</Title><URL>" & strDocumentURL & "</URL></Document></Documents>"
    '                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 2031, "Documents successfully generated", objInputXMLDoc.InnerXml)
    '                If (view And save) Then
    '                    SOAPSuccessfulMessage("Documents successfully retrieved", 203, AppID, strDocuments, objOutputXMLDoc.InnerXml)
    '                ElseIf (view) Then
    '                    SOAPSuccessfulMessage("Documents successfully retrieved", 203, AppID, strDocuments, objOutputXMLDoc.InnerXml)
    '                ElseIf (save) Then
    '                    SOAPSuccessfulMessage("Documents successfully retrieved", 203, AppID, "", objOutputXMLDoc.InnerXml)
    '                End If
    '            End If
    '        Else
    '            SOAPUnsuccessfulMessage("Connection error", -1, AppID, "<Quotes><Error>Connection error. Please contact Engaged Solutions</Error></Quotes>", objInputXMLDoc.InnerXml)
    '        End If
    '        objResponse.Close()
    '        objResponse = Nothing
    '    End If

    '    objInputXMLDoc = Nothing
    '    objOutputXMLDoc = Nothing

    'End Sub

    'Private Sub getPreviousQuotes()
    '    If (checkValue(XMLReceivedID)) Then
    '        Dim strQuotes As String = "", strPolicyContentsCoverOffered As String = "", strPolicyBuildingsCoverOffered As String = "", intPolicyExcess As String = ""
    '        Dim strSQL As String = "SELECT XMLReceived, XMLReceivedDate FROM tblxmlreceived WHERE XMLReceivedID = '" & XMLReceivedID & "' AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
    '        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '        If (dsCache.Rows.Count > 0) Then
    '            For Each Row As DataRow In dsCache.Rows

    '                Dim strDateMin = ddmmyyyy2yyyymmdd(CDate(Row.Item("XMLReceivedDate")).AddSeconds(-2)) & " " & CDate(Row.Item("XMLReceivedDate")).AddSeconds(-2).TimeOfDay.ToString
    '                Dim strDateMax = ddmmyyyy2yyyymmdd(CDate(Row.Item("XMLReceivedDate")).AddSeconds(2)) & " " & CDate(Row.Item("XMLReceivedDate")).AddSeconds(2).TimeOfDay.ToString
    '                Dim strSQL2 As String = "SELECT TOP 1 XMLReceived FROM tblxmlreceived WHERE (XMLReceivedDate >= '" & strDateMin & "' AND XMLReceivedDate <= '" & strDateMax & "') AND XMLReceivedID <> '" & XMLReceivedID & "' AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
    '                Dim strXMLReceived As String = New Caching(Nothing, strSQL2, "", "", "").returnCacheString()

    '                If (checkValue(strXMLReceived)) Then
    '                    objInputXMLDoc = New XmlDocument
    '                    objInputXMLDoc.XmlResolver = Nothing
    '                    objInputXMLDoc.LoadXml(strXMLReceived)
    '                    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '                    objNSM.AddNamespace("def", strXMLNamespace)
    '                    Dim objPolicyExcess As XmlNode = objInputXMLDoc.SelectSingleNode("//def:GetHouseholdQuote/def:bcInfo/def:PolicyExcess", objNSM)
    '                    If (Not objPolicyExcess Is Nothing) Then
    '                        'intPolicyExcess = getPolicyExcess(objPolicyExcess.InnerText)
    '                    End If
    '                    Dim objBECO As XmlNode = objInputXMLDoc.SelectSingleNode("//def:GetHouseholdQuote/def:bcInfo/def:BuildingsExtraCoverOption", objNSM)
    '                    If (Not objBECO Is Nothing) Then
    '                        If (objBECO.InnerText = "Included") Then
    '                            strPolicyBuildingsCoverOffered = "300,001 to 500,000"
    '                        Else
    '                            strPolicyBuildingsCoverOffered = "Up to 300,0000"
    '                        End If
    '                    End If
    '                    Dim objCECO As XmlNode = objInputXMLDoc.SelectSingleNode("//def:GetHouseholdQuote/def:bcInfo/def:ContentsExtraCoverOption", objNSM)
    '                    If (Not objCECO Is Nothing) Then
    '                        If (objCECO.InnerText = "Included") Then
    '                            strPolicyContentsCoverOffered = "40,001 to 60,000"
    '                        Else
    '                            strPolicyContentsCoverOffered = "Up to 40,0000"
    '                        End If
    '                    End If
    '                    Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//def:GetHouseholdQuote/def:ProductToQuote", objNSM)
    '                    If (Not objProduct Is Nothing) Then
    '                        'strProductType = objProduct.InnerText
    '                    End If
    '                End If

    '                objOutputXMLDoc = New XmlDocument
    '                objOutputXMLDoc.XmlResolver = Nothing
    '                objOutputXMLDoc.LoadXml(Row.Item("XMLReceived").ToString)
    '                objNSM = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
    '                objNSM.AddNamespace("def", strXMLNamespace)
    '                strQuotes = "<Sourcing>"

    '                Dim strApplyURL As String = Request.ServerVariables("SCRIPT_NAME") & "?AppID=" & AppID & "&intMode=2&UserID=" & UserID & "&UserSessionID=" & UserSessionID
    '                Dim strDocumentsURL As String = Request.ServerVariables("SCRIPT_NAME") & "?AppID=" & AppID & "&intMode=3&frmViewDocuments=" & strViewDocuments & "&frmSaveDocuments=" & strSaveDocuments & "&UserID=" & UserID & "&UserSessionID=" & UserSessionID

    '                strQuotes += "<SourcingItem>"
    '                strQuotes += "<ApplyURL>" & Replace(strApplyURL, "&", "&amp;") & "</ApplyURL>"
    '                strQuotes += "<DocumentsURL>" & Replace(strDocumentsURL, "&", "&amp;") & "</DocumentsURL>"
    '                Dim objQuoteRef As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:QuoteReference", objNSM)
    '                If (Not objQuoteRef Is Nothing) Then
    '                    strQuotes += "<QuoteID>" & objQuoteRef.InnerText & "</QuoteID>"
    '                End If
    '                Dim objInsurer As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HouseholdInsurerName", objNSM)
    '                If (Not objInsurer Is Nothing) Then
    '                    strQuotes += "<Provider>" & Replace(objInsurer.InnerText, "&", "&amp;") & "</Provider>"
    '                End If
    '                Dim objInsurerCode As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HouseholdInsurerCode", objNSM)
    '                If (Not objInsurerCode Is Nothing) Then
    '                    strQuotes += "<ProviderCode>" & Replace(objInsurerCode.InnerText, "&", "&amp;") & "</ProviderCode>"
    '                End If
    '                Dim objEmergencyInsurer As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HomeEmergencyInsurerName", objNSM)
    '                If (Not objEmergencyInsurer Is Nothing) Then
    '                    strQuotes += "<EmergencyProvider>" & Replace(objEmergencyInsurer.InnerText, "&", "&amp;") & "</EmergencyProvider>"
    '                End If
    '                Dim objLegalInsurer As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:LegalCoverInsurerName", objNSM)
    '                If (Not objLegalInsurer Is Nothing) Then
    '                    strQuotes += "<LegalProvider>" & Replace(objLegalInsurer.InnerText, "&", "&amp;") & "</LegalProvider>"
    '                End If
    '                'strQuotes += "<Product>" & strProductType & "</Product>"
    '                Dim objProductRef As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:ProductReferenceNumber", objNSM)
    '                If (Not objProductRef Is Nothing) Then
    '                    strQuotes += "<ProductRef>" & Replace(objProductRef.InnerText, "&", "&amp;") & "</ProductRef>"
    '                End If
    '                If (checkValue(strPolicyBuildingsCoverOffered)) Then
    '                    strQuotes += "<SumInsuredBuildings>" & strPolicyBuildingsCoverOffered & "</SumInsuredBuildings>"
    '                End If
    '                If (checkValue(strPolicyContentsCoverOffered)) Then
    '                    strQuotes += "<SumInsuredContents>" & strPolicyContentsCoverOffered & "</SumInsuredContents>"
    '                End If
    '                If (checkValue(intPolicyExcess)) Then
    '                    strQuotes += "<ExcessBuildings>" & intPolicyExcess & "</ExcessBuildings>"
    '                    strQuotes += "<ExcessContents>" & intPolicyExcess & "</ExcessContents>"
    '                End If
    '                Dim objBuildingsPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:BuildingsPremium", objNSM)
    '                If (Not objBuildingsPremium Is Nothing) Then
    '                    strQuotes += "<BuildingsPremium>" & objBuildingsPremium.InnerText & "</BuildingsPremium>"
    '                End If
    '                Dim objBuildingsPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:BuildingsPremiumExIPT", objNSM)
    '                If (Not objBuildingsPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<BuildingsPremiumExIPT>" & objBuildingsPremiumExIPT.InnerText & "</BuildingsPremiumExIPT>"
    '                End If
    '                Dim objBuildingsContentsPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:BuildingsContentsPremium", objNSM)
    '                If (Not objBuildingsContentsPremium Is Nothing) Then
    '                    strQuotes += "<BuildingsContentsPremium>" & objBuildingsContentsPremium.InnerText & "</BuildingsContentsPremium>"
    '                End If
    '                Dim objBuildingsContentsPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:BuildingsContentsPremiumExIPT", objNSM)
    '                If (Not objBuildingsContentsPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<BuildingsContentsPremiumExIPT>" & objBuildingsContentsPremiumExIPT.InnerText & "</BuildingsContentsPremiumExIPT>"
    '                End If
    '                Dim objContentsPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:ContentsPremium", objNSM)
    '                If (Not objContentsPremium Is Nothing) Then
    '                    strQuotes += "<ContentsPremium>" & objContentsPremium.InnerText & "</ContentsPremium>"
    '                End If
    '                Dim objContentsPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:ContentsPremiumExIPT", objNSM)
    '                If (Not objContentsPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<ContentsPremiumExIPT>" & objContentsPremiumExIPT.InnerText & "</ContentsPremiumExIPT>"
    '                End If
    '                Dim objHomeEmergencyPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HomeEmergencyPremium", objNSM)
    '                If (Not objHomeEmergencyPremium Is Nothing) Then
    '                    strQuotes += "<HomeEmergencyPremium>" & objHomeEmergencyPremium.InnerText & "</HomeEmergencyPremium>"
    '                End If
    '                Dim objLegalCoverPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:LegalCoverPremium", objNSM)
    '                If (Not objLegalCoverPremium Is Nothing) Then
    '                    strQuotes += "<LegalCoverPremium>" & objLegalCoverPremium.InnerText & "</LegalCoverPremium>"
    '                End If
    '                Dim objHomeEmergencyPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HomeEmergencyPremiumExIPT", objNSM)
    '                If (Not objHomeEmergencyPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<HomeEmergencyPremiumExIPT>" & objHomeEmergencyPremiumExIPT.InnerText & "</HomeEmergencyPremiumExIPT>"
    '                End If
    '                Dim objLegalCoverPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:LegalCoverPremiumExIPT", objNSM)
    '                If (Not objLegalCoverPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<LegalCoverPremiumExIPT>" & objLegalCoverPremiumExIPT.InnerText & "</LegalCoverPremiumExIPT>"
    '                End If
    '                Dim objTotalPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:TotalPremium", objNSM)
    '                If (Not objTotalPremium Is Nothing) Then
    '                    strQuotes += "<TotalPremium>" & objTotalPremium.InnerText & "</TotalPremium>"
    '                End If
    '                Dim objTotalPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:TotalPremiumExIPT", objNSM)
    '                If (Not objTotalPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<TotalPremiumExIPT>" & objTotalPremiumExIPT.InnerText & "</TotalPremiumExIPT>"
    '                End If
    '                Dim objIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:IPT", objNSM)
    '                If (Not objIPT Is Nothing) Then
    '                    strQuotes += "<IPT>" & objIPT.InnerText & "</IPT>"
    '                End If
    '                Dim objAnnualIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:objAnnualIPT", objNSM)
    '                If (Not objAnnualIPT Is Nothing) Then
    '                    strQuotes += "<AnnualIPT>" & objAnnualIPT.InnerText & "</AnnualIPT>"
    '                End If
    '                Dim objAnnualPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:AnnualPremium", objNSM)
    '                If (Not objAnnualPremium Is Nothing) Then
    '                    strQuotes += "<AnnualPremium>" & objAnnualPremium.InnerText & "</AnnualPremium>"
    '                End If
    '                Dim objAnnualPremiumExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:AnnualPremiumExIPT", objNSM)
    '                If (Not objAnnualPremiumExIPT Is Nothing) Then
    '                    strQuotes += "<AnnualPremiumExIPT>" & objAnnualPremiumExIPT.InnerText & "</AnnualPremiumExIPT>"
    '                End If
    '                Dim objPolicyFee As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:PolicyFee", objNSM)
    '                If (Not objPolicyFee Is Nothing) Then
    '                    strQuotes += "<PolicyFee>" & objPolicyFee.InnerText & "</PolicyFee>"
    '                End If
    '                Dim objPolicyFeeExIPT As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:PolicyFeeExIPT", objNSM)
    '                If (Not objPolicyFeeExIPT Is Nothing) Then
    '                    strQuotes += "<PolicyFeeExIPT>" & objPolicyFeeExIPT.InnerText & "</PolicyFeeExIPT>"
    '                End If
    '                strQuotes += "</SourcingItem>"
    '                strQuotes += "</Sourcing>"
    '            Next
    '        End If
    '        dsCache = Nothing
    '        SOAPSuccessfulMessage("Sourcing successfully retrieved", 305, AppID, strQuotes, "")
    '    Else
    '        Dim strQuotes As String = ""
    '        Dim strSQL As String = "SELECT TOP " & intNoQuotes & " XMLReceivedID, XMLReceivedDate, XMLReceived FROM tblxmlreceived WHERE XMLReceivedResult = 201 AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY XMLReceivedDate DESC"
    '        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '        If (dsCache.Rows.Count > 0) Then
    '            For Each Row As DataRow In dsCache.Rows
    '                Dim strProvider As String = "", strProviderCode As String = "", strLowestAnnualPremium As String = ""
    '                objOutputXMLDoc = New XmlDocument
    '                objOutputXMLDoc.XmlResolver = Nothing
    '                objOutputXMLDoc.LoadXml(Row.Item("XMLReceived").ToString)
    '                objNSM = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
    '                objNSM.AddNamespace("def", strXMLNamespace)
    '                Dim objInsurer As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HouseholdInsurerName", objNSM)
    '                If (Not objInsurer Is Nothing) Then
    '                    strProvider = Replace(objInsurer.InnerText, "&", "&amp;")
    '                End If
    '                Dim objInsurerCode As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:HouseholdInsurerCode", objNSM)
    '                If (Not objInsurerCode Is Nothing) Then
    '                    strProviderCode = Replace(objInsurerCode.InnerText, "&", "&amp;")
    '                End If
    '                Dim objAnnualPremium As XmlNode = objOutputXMLDoc.SelectSingleNode("//def:GetHouseholdQuoteResponse/def:GetHouseholdQuoteResult/def:AnnualPremium", objNSM)
    '                If (Not objAnnualPremium Is Nothing) Then
    '                    strLowestAnnualPremium = objAnnualPremium.InnerText
    '                End If

    '                Dim strDateMin = ddmmyyyy2yyyymmdd(CDate(Row.Item("XMLReceivedDate")).AddSeconds(-2)) & " " & CDate(Row.Item("XMLReceivedDate")).AddSeconds(-2).TimeOfDay.ToString
    '                Dim strDateMax = ddmmyyyy2yyyymmdd(CDate(Row.Item("XMLReceivedDate")).AddSeconds(2)) & " " & CDate(Row.Item("XMLReceivedDate")).AddSeconds(2).TimeOfDay.ToString
    '                Dim strSQL2 As String = "SELECT TOP 1 XMLReceived FROM tblxmlreceived WHERE (XMLReceivedDate >= '" & strDateMin & "' AND XMLReceivedDate <= '" & strDateMax & "') AND XMLReceivedID <> '" & XMLReceivedID & "' AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
    '                Dim strXMLReceived As String = New Caching(Nothing, strSQL2, "", "", "").returnCacheString()

    '                If (checkValue(strXMLReceived)) Then
    '                    objInputXMLDoc = New XmlDocument
    '                    objInputXMLDoc.XmlResolver = Nothing
    '                    objInputXMLDoc.LoadXml(strXMLReceived)
    '                    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '                    objNSM.AddNamespace("def", strXMLNamespace)
    '                    Dim objProduct As XmlNode = objInputXMLDoc.SelectSingleNode("//def:GetHouseholdQuote/def:ProductToQuote", objNSM)
    '                    If (Not objProduct Is Nothing) Then
    '                        'strProductType = objProduct.InnerText
    '                    End If
    '                End If

    '                objInputXMLDoc = Nothing
    '                Dim strURL As String = Request.ServerVariables("SCRIPT_NAME") & "?AppID=" & AppID & "&intMode=4&frmViewDocuments=" & strViewDocuments & "&frmSaveDocuments=" & strSaveDocuments & "&UserID=" & UserID & "&UserSessionID=" & UserSessionID
    '                strQuotes += "<Sourcing ID=""" & Row.Item("XMLReceivedID") & """ SourcingDate=""" & Row.Item("XMLReceivedDate") & """ Provider=""" & strProvider & """ Product="""""" ProviderCode=""" & strProviderCode & """ LowestAnnualPremium=""" & strLowestAnnualPremium & """ URL=""" & Replace(strURL, "&", "&amp;") & """></Sourcing>"
    '            Next
    '        End If
    '        dsCache = Nothing
    '        SOAPSuccessfulMessage("Sourcing history retrieved", 205, AppID, strQuotes, "")
    '    End If
    'End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function insertElement(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertAfter(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function insertElementBefore(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertBefore(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = doc.CreateElement(fld)
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Function postM27WebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("SOAPAction", soapaction)
            End With
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As System.Net.WebException
                If (err.Status = WebExceptionStatus.Timeout) Then
                    reportErrorMessage(err)
                    Return Nothing
                Else
                    Return Nothing
                End If
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, quotes As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crmtest.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(quotes)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, quotes As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crmtest.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(quotes)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        postEmail("", "itsupport@engaged-solutions.co.uk", "Mortgage 27 XML (Test) Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

End Class
