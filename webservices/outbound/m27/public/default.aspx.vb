﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.brokertoolbox.www

Partial Class XMLM27
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID"), UserID As String = HttpContext.Current.Request("UserID"), intNoQuotes As String = HttpContext.Current.Request("frmNoQuotes")
    Private intSOAPRequestNo As String = getIndexNumber()
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strXMLURL As String = "", strXMLSOAPAction As String = "", strXMLFile As String = "", strXMLView As String = "", strXMLNamespace As String = "", intCompanyID As String = "", intSiteID As String = "", strUserID As String = "", strAPIKey As String = ""
    Private intMode As String = HttpContext.Current.Request("intMode"), strRateType As String = HttpContext.Current.Request("frmRateType"), strTerm As String = HttpContext.Current.Request("frmProductTerm"), strLenderCodes As String = HttpContext.Current.Request("frmLenderCodes"), strFeeFree As String = HttpContext.Current.Request("frmFeeFree"), strAmount As String = HttpContext.Current.Request("frmAmount")
    Private strPropertyValue As String = HttpContext.Current.Request("frmPropertyValue"), strMortgageReason As String = HttpContext.Current.Request("frmMortgageReason"), strRepaymentType As String = HttpContext.Current.Request("frmRepaymentType"), strMortgageType As String = HttpContext.Current.Request("frmMortgageType")
    Private strErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not checkValue(intNoQuotes)) Then
            intNoQuotes = "20"
        End If
        If (Not checkValue(strRateType)) Then
            strRateType = "Fixed"
        End If
        Response.Headers.Add("Access-Control-Allow-Origin", "*")
        Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST")
        Response.Headers.Add("Access-Control-Max-Age", "1000")
        Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With")
    End Sub

    Private Enum ServiceMode
        ' *** Use 30[x] for XML logging codes e.g. Sourcing success = 301 *** '
        Sourcing = 1
        GetLenders = 5
        GetLenderImage = 6
    End Enum

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", "Mortgage Sourcing (Test) Error", strMessage, True, "")
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Sourcing Unsuccessfully Generated", objInputXMLDoc.InnerXml)
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub generateXml()
        If (strEnvironment = "live") Then
            strXMLURL = "http://www.brokertoolbox.co.uk/ServiceSourcing.asmx"
            strXMLSOAPAction = "http://tempuri.org/SourceMortgage"
            strXMLFile = "factfind.xml"
            strXMLView = "vwxmlm27"
            strXMLNamespace = "http://tempuri.org"
        Else
            strXMLURL = "http://brokertoolbox.antest.co.uk/ServiceSourcing.asmx"
            strXMLSOAPAction = "http://tempuri.org/SourceMortgage"
            strXMLFile = "factfind.xml"
            strXMLView = "vwxmlm27"
            strXMLNamespace = "http://tempuri.org"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%M27%' AND CompanyID = 1127"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "M27CompanyID") Then intCompanyID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27SiteID") Then intSiteID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27UserID") Then strUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "M27APIKey") Then strAPIKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        Select Case intMode
            Case ServiceMode.Sourcing
                getSourcing()
            Case ServiceMode.GetLenders
                getLenders()
            Case ServiceMode.GetLenderImage
                getLenderImage()
        End Select
    End Sub

    Private Sub getLenders()
        Dim objService As New ServiceSourcing
        Dim intNoOfLendersReturned As Integer
        Dim strResults As String = objService.GetListOfLenders(intSiteID, intNoOfLendersReturned)

        objService = Nothing

        objOutputXMLDoc.LoadXml(strResults)
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
        HttpContext.Current.Response.End()
    End Sub

    Private Sub getLenderImage()
        Dim objClient As WebClient = New WebClient()
        Dim bImg As Byte() = objClient.DownloadData("http://m27toolbox.antest.co.uk/images/" & Request("size") & "-logos/" & Request("code") & ".gif")
        Response.ContentType = "image/gif"
        Response.BinaryWrite(bImg)
    End Sub

    Private Sub getSourcing()

        Dim strXML As String = ""
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath(strXMLFile))

        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "CompanyID", intCompanyID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "SiteID", intSiteID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "UserID", strUserID)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "APIKey", strAPIKey)
        createElement(objInputXMLDoc, "MortgageFactfind/Filters", strRateType, strRateType)
        writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "Show", "Show_With_Feature")
        If (strRateType = "Variable" Or strRateType = "Capped") Then
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodFromMonths", -1)
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodToMonths", -1)
        Else
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodFromMonths", -1)
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/" & strRateType, "PeriodToMonths", -1)
        End If

        If checkValue(strLenderCodes) Then
            Dim arrLenderCodes As Array = Split(strLenderCodes, ",")
            createElement(objInputXMLDoc, "MortgageFactfind/Filters", "Lenders", "Lenders")
            For x As Integer = 0 To UBound(arrLenderCodes)
                If (checkValue(arrLenderCodes(x))) Then
                    createElement(objInputXMLDoc, "MortgageFactfind/Filters/Lenders", "Lender[" & x + 1 & "]", "Lender")
                    writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/Lenders/Lender[" & x + 1 & "]", "Code", arrLenderCodes(x))
                End If
            Next
        End If

        If (strFeeFree = "Y") Then
            createElement(objInputXMLDoc, "MortgageFactfind/Filters", "Fees", "Fees")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind/Filters/Fees", "FreeMainLenderFees", "Show_With_Feature")
        End If

        If checkValue(strPropertyValue) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "ExpectedValuation", "ExpectedValuation")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "ExpectedValuation", strPropertyValue)
        End If

        If checkValue(strAmount) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "LoanRequired", "LoanRequired")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "LoanRequired", strAmount)
        End If

        If checkValue(strTerm) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "Term", "Term")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "Term", strTerm)
        End If

        If checkValue(strMortgageReason) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "ReasonForMortgage", "ReasonForMortgage")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "ReasonForMortgage", strMortgageReason)
        End If

        If checkValue(strRepaymentType) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "PaymentMethod", "PaymentMethod")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "PaymentMethod", lookupValue(strRepaymentType, "M27", "tblsourcing"))
        End If

        If checkValue(strMortgageType) Then
            insertElementBefore(objInputXMLDoc, "MortgageFactfind/Filters", "MortgageFactfind", "MortgageType", "MortgageType")
            writeToNode(objInputXMLDoc, "y", "MortgageFactfind", "MortgageType", lookupValue(strMortgageType, "M27", "tblsourcing"))
        End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objService As New ServiceSourcing
            Dim intNumProducts As Integer, boolInputError As Boolean
            Dim strResults As String = objService.SourceMortgage(objInputXMLDoc.InnerXml, intNumProducts, boolInputError)
            objService = Nothing

            'responseWrite(intNumProducts)
            'responseEnd()

            If (intNumProducts > 0) Then
                objOutputXMLDoc.LoadXml(strResults)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 301, "Mortgage Sourcing Successfully Generated", objInputXMLDoc.InnerXml)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 301, "Mortgage Sourcing Successfully Retrieved", objOutputXMLDoc.InnerXml)
                HttpContext.Current.Response.ContentType = "text/xml"
                HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                HttpContext.Current.Response.End()
            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 3011, "Mortgage Sourcing Unsuccessfully Generated", objInputXMLDoc.InnerXml)
            End If

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function insertElement(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertAfter(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function insertElementBefore(ByVal doc As XmlDocument, ByVal before As String, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objNodeBefore = doc.SelectSingleNode("//" & before)
        'If (objTest Is Nothing) Then
        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
        Dim objNewNode As XmlElement = doc.CreateElement(fld)
        objApplication.InsertBefore(objNewNode, objNodeBefore)
        Return objNewNode
        'Else
        'Return Nothing
        'End If
    End Function

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String) As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = doc.CreateElement(fld)
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Function postM27WebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("SOAPAction", soapaction)
            End With
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As System.Net.WebException
                If (err.Status = WebExceptionStatus.Timeout) Then
                    reportErrorMessage(err)
                    Return Nothing
                Else
                    Return Nothing
                End If
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, quotes As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crmtest.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(quotes)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, quotes As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crmtest.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(quotes)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        postEmail("", "itsupport@engaged-solutions.co.uk", "Mortgage 27 XML (Test) Error From " & getAnyField("UserFullName", "tblusers", "UserID", UserID), "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

End Class
