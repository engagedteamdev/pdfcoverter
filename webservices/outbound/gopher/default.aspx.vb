﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLGopher
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAction As String = ""
    Private strMediaID As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
	Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strCustomerEmailID As String = ""
    Private strDC As String = HttpContext.Current.Request("frmDC"), strOtherAsDC As String = HttpContext.Current.Request("frmOtherAsDC")
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        If (strEnvironment = "live") Then
            'strMediaID = "99000971"
            strXMLURL = "https://api.themoneyportal.co.uk/webscr.php"
            strAction = "cmd=_RequestInsertNewLead&Live=TRUE&Debug=FALSE"
        Else
            'strMediaID = "99000971"
            strXMLURL = "https://api.themoneyportal.co.uk/webscr.php"
			'strXMLURL = "http://crmtest.engaged-solutions.co.uk/net/webservices/inbound/test/gopher/"
            strAction = "cmd=_RequestInsertNewLead&Live=FALSE&Debug=TRUE"
        End If

        strCustomerEmailID = getAnyField("MediaCampaignOutboundLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/gopher/blank.xml"))

        Call writeAttribute("y", "Affiliate", "mediaid", strMediaCampaignCampaignReference)
        Call writeAttribute("y", "Affiliate", "leadinnumber", AppID)

        Dim strSQL As String = "SELECT * FROM vwxmlgopher WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
         Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Left(Column.ColumnName.ToString, 11) = "AddressLine") Then
                            writeToNodeByNumber("n", "Affilate/ClientDetails/Address", "AddressLine", Row(Column).ToString, Right(Column.ColumnName.ToString, 1))
                        ElseIf (Column.ColumnName.ToString = "ProductType") Then
                            writeAttribute("y", "Affiliate/ClientDetails/Loan", "type", Row(Column).ToString)
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            writeToNode("n", "Affiliate/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)

                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing
		
		If (strDC = "Y") Then
			writeToNode("n", "Affiliate/Loan", "Purpose", "Consolidation")
        End If
        If (strOtherAsDC = "Y") Then
            Select Case Right(AppID, 1)
                Case 0, 3, 5, 8
                    writeToNode("n", "Affiliate/Loan", "Purpose", "Consolidation")
            End Select
        End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, strAction & "&XMLData=" & objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//Response")
                Dim objNodeList As XmlNodeList = objApplication.SelectNodes("//Error/ErrorDescription")
                Dim strMessage As String = ""
                For Each Child As XmlNode In objNodeList
                    strMessage += Child.InnerText + "<br />"
                Next
                Select Case objApplication.SelectSingleNode("Status").InnerText
                    Case "Success"
                        If (objApplication.SelectSingleNode("CustomerID").InnerText = "Not Inserted") Then
                            saveStatus(AppID, Config.DefaultUserID, "INV", "DUP")
                            saveUpdatedDate(AppID, Config.DefaultUserID, "INV")
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Gopher: Not Inserted"))
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Gopher", objInputXMLDoc.InnerXml)
							saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Gopher", objOutputXMLDoc.InnerXml)
							incrementTransferAttempts(AppID, strMediaCampaignID)
                        Else
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Gopher"))
                            updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("CustomerID").InnerText, "NULL")
                            setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                            If (checkValue(strCustomerEmailID)) Then
                                sendEmails(AppID, strCustomerEmailID, CompanyID)
                            End If
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Gopher", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Gopher", objOutputXMLDoc.InnerXml)
                            End If
                    Case "Failed"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Gopher: " & strMessage))
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Gopher", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Gopher", objOutputXMLDoc.InnerXml)
                            incrementTransferAttempts(AppID, strMediaCampaignID)
			    End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                End If
            End If
        End If
    End Sub

    Private Sub writeToNodeByNumber(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal lvl As Integer)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNodeList = objInputXMLDoc.GetElementsByTagName(fld)
                If Not (objTest Is Nothing) Then
                    objTest.Item(lvl - 1).InnerText = val
                End If
            End If
        End If
    End Sub
	
    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function	

End Class
