using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EvoService;
using EvoCRMService;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];


	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	public void httpPost()
	{


			string Title = "", Surname = "", Forenames = "", SmokerFlag = "", HomeTelephone = "", MobileTelephone = "", EmailAddress = "";
			
			DateTime utcTime = DateTime.UtcNow;
			DateTime.UtcNow.ToUniversalTime();

			string serviceurl = "https://beta.finplanportal.com/api/NewLead";


			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception e)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
			}

			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwhealthandprotect where AppId = {0}", AppID), connection);

			var readersql = myCommand.ExecuteReader();

			while (readersql.Read())
			{

				Title = readersql["App1Title"].ToString();
				Forenames = readersql["App1FirstName"].ToString();
				Surname = readersql["App1Surname"].ToString();
				SmokerFlag = readersql["App1Smoker"].ToString();
				HomeTelephone = readersql["App1HomeTelephone"].ToString();
				MobileTelephone = readersql["App1MobileTelephone"].ToString();
				EmailAddress = readersql["App1EmailAddress"].ToString();

			

				if (!string.IsNullOrEmpty(readersql["App1DOB"].ToString()))
				{
						utcTime = DateTime.Parse(readersql["App1DOB"].ToString());
				}

			}

			if (SmokerFlag == "Y")
			{
				SmokerFlag = "Yes";
			}
			else
			{
				SmokerFlag = "No";
			}

			string post = "{\"Database\":\"lmshealthprotection\",\"AuthenticationToken\":\"7Ju/kd/aL9i9LHLUbgEZyKNITb+2m7ISjLW/3i8Wqgw=\",\"Records\": { \"LEAD\": { \"LeadId\": \"" + AppID + "\", \"LeadTime\": \"" + DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss+00:00") + "\", \"SourceBusiness\": \"Online Application\", \"LeadNumber\": \"" + AppID + "\", \"LeadSource\": \"Engaged Solutions\", \"Title\": \"" + Title + "\", \"Forenames\": \"" + Forenames + "\",  \"Surname\": \"" + Surname + "\", \"SmokerFlag\": \"" + SmokerFlag + "\", \"DateOfBirth\": \"" + utcTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss+00:00") + "\", \"HomeTelephone\": \"" + HomeTelephone + "\", \"MobileTelephone\": \"" + MobileTelephone + "\", \"Email_Address\": \"" + EmailAddress + "\" } }, \"Tables\": { \"OBJECTIVES\": [{ \"ObjectiveType\": \"Life Cover\" } ] } }";
			
			//HttpContext.Current.Response.Write(post);
			//HttpContext.Current.Response.End(); 

			
			WebRequest request = WebRequest.Create(serviceurl);
			request.Method = "POST";
			byte[] byteArray = Encoding.UTF8.GetBytes(post); 
			request.ContentType = "application/json";
			request.ContentLength = byteArray.Length;
			Stream dataStream = request.GetRequestStream();
			dataStream.Write(byteArray, 0, byteArray.Length);
			dataStream.Close();
			WebResponse response = request.GetResponse();
			Console.WriteLine(((HttpWebResponse)response).StatusDescription);
			dataStream = response.GetResponseStream();
			StreamReader reader = new StreamReader(dataStream);
			string responseFromServer = reader.ReadToEnd();

			HttpContext.Current.Response.Write(responseFromServer);
			HttpContext.Current.Response.End(); 	

			if (responseFromServer.Contains("true"))
			{
				
			}
			else
			{
				
			}

			reader.Close();
			dataStream.Close();
			response.Close();





	}











	}

public class Lead
{
	public string Email { get; set; }
	public bool Active { get; set; }
	public DateTime CreatedDate { get; set; }
	public IList<string> Roles { get; set; }

}

public class LEAD
{
	public string Test { get; set; }
	public string Name { get; set; }

}