using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;

public partial class Default : Page
{

	private string RequestType = HttpContext.Current.Request["RequestType"];
	private string AppID = HttpContext.Current.Request["AppID"];
	private string App1FirstName = "", App1Surname = "", App1MobileTelephone = "", App1EmailAddress = "", Adviser = "";

	public HttpFileCollection filepath = HttpContext.Current.Request.Files;


	class theData
	{

		public string adviser { get; set; }
		public string forename { get; set; }
		public string surname { get; set; }
		public string email { get; set; }
		public string sms { get; set; }
		public string reference { get; set; }
	}



	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	public void Run()
	{


		var stPostURL = "https://adviser.ai/api/auth";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(stPostURL);


		HttpContent contentBody = new StringContent("email=crmsupport@engagedcrm.co.uk&password=Y0rkshire");
		contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

		var res = client.PostAsync(stPostURL, contentBody).Result;		

		var responseContent = res.Content;		

		var jsonauth = responseContent.ReadAsStringAsync().Result;
		
		var joauth = Newtonsoft.Json.Linq.JObject.Parse(jsonauth);

		var token = joauth["token"].ToString();

		


		if (RequestType == "CampaignDead")
		{
			CampaignCallDead(token);
		}
		else if (RequestType == "CampaignLive")
		{
			CampaignCallLive(token);
		}
		else if (RequestType == "FactFindAdviser")
		{
			FactFindAdviser(token);
		}


	}

	public void CampaignCallDead(string token)
	{

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwAdviserAIDead"), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			AppID = reader["AppID"].ToString();
			App1FirstName = reader["App1FirstName"].ToString();
			App1Surname = reader["App1Surname"].ToString();
			App1MobileTelephone = reader["App1MobileTelephone"].ToString();
			App1EmailAddress = reader["App1EmailAddress"].ToString();
			Adviser = reader["UserFullName"].ToString();

			var stPostURL = "https://adviser.ai/api/v1/campaign/add/campaign";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(stPostURL);
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

			theData data = new theData();					
			data.adviser = "Allocate";
			data.forename = App1FirstName;
			data.surname = App1Surname;
			data.email = App1EmailAddress;
			data.sms = App1MobileTelephone;
			data.reference = AppID;

			var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

			HttpContent contentBody = new StringContent("data=[" + json + "]&schedule=weekly&assign=allocate");
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

			var res = client.PostAsync(stPostURL, contentBody).Result;

			var responseContent = res.Content;

			var jsoncampaign = responseContent.ReadAsStringAsync().Result;

			var jocampaign = Newtonsoft.Json.Linq.JObject.Parse(jsoncampaign);


			var campaign = jocampaign["campaign_id"].ToString();

			var results = jocampaign["results"].ToString();



			if (results != "[]")
			{
				SqlCommand saveCustomerID = new SqlCommand();
				saveCustomerID.Connection = connection;
				saveCustomerID.CommandText = string.Format("update tblapplications set AdviserAILiveLead = '1' where AppID = '" + AppID + "'");
				saveCustomerID.ExecuteNonQuery();

				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Response From Adviser AI','" + campaign.ToString() + "',getDate())");
				insertXML.ExecuteNonQuery();

			}
			else
			{

				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Response From Adviser AI','" + campaign.ToString() + "',getDate())");
				insertXML.ExecuteNonQuery();


			}


			Response.Write(jocampaign);

		}






	}

	public void CampaignCallLive(string token)
	{

		try
		{


			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwAdviserAILive"), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			AppID = reader["AppID"].ToString();
			App1FirstName = reader["App1FirstName"].ToString();
			App1Surname = reader["App1Surname"].ToString();
			App1MobileTelephone = reader["App1MobileTelephone"].ToString();
			App1EmailAddress = reader["App1EmailAddress"].ToString();
			Adviser = reader["UserFullName"].ToString();

			var stPostURL = "https://adviser.ai/api/v1/campaign/add/oneoff";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(stPostURL);
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

			theData data = new theData();			
			data.adviser = "Allocate";
			data.forename = App1FirstName;
			data.surname = App1Surname;
			data.email = App1EmailAddress;
			data.sms = App1MobileTelephone;
			data.reference = AppID;

			var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

			HttpContent contentBody = new StringContent("data=[" + json + "]&assign=allocate");
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

			var res = client.PostAsync(stPostURL, contentBody).Result;

			var responseContent = res.Content;

			var jsoncampaign = responseContent.ReadAsStringAsync().Result;

			var jocampaign = Newtonsoft.Json.Linq.JObject.Parse(jsoncampaign);

			var campaign = jocampaign["campaign_id"].ToString();

			var results = jocampaign["results"].ToString();



			if (results != "[]")
			{
				SqlCommand saveCustomerID = new SqlCommand();
				saveCustomerID.Connection = connection;
				saveCustomerID.CommandText = string.Format("update tblapplications set AdviserAILiveLead = '1' where AppID = '" + AppID + "'");
				saveCustomerID.ExecuteNonQuery();

				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Response From Adviser AI','" + campaign.ToString() + "',getDate())");
				insertXML.ExecuteNonQuery();

			}
			else
			{

				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Response From Adviser AI','" + campaign.ToString() + "',getDate())");
				insertXML.ExecuteNonQuery();


			}

			Response.Write(jocampaign);

			}

		



		}
		catch (Exception e)
		{

			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception y)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", y.Message));
			}


			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Error Response From Adviser AI','" + e.Message + "',getDate())");
			insertXML.ExecuteNonQuery();

			Response.Write(e.Message);
		}





	}

	public void FactFindAdviser(string token)
	{

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwAdviserAI where AppID = '{0}'", AppID), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			App1FirstName = reader["App1FirstName"].ToString();
			App1Surname = reader["App1Surname"].ToString();
			App1MobileTelephone = reader["App1MobileTelephone"].ToString();
			App1EmailAddress = reader["App1EmailAddress"].ToString();
			Adviser = reader["UserFullName"].ToString();
		}

		var stPostURL = "https://adviser.ai/api/v1/factfinder/add/advisersingle";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(stPostURL);
		client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

		HttpContent contentBody = new StringContent("adviser=" + Adviser + "&forename=" + App1FirstName + "&surname=" + App1Surname + "&email=" + App1EmailAddress + "&sms=" + App1MobileTelephone + "&reference=" + AppID + "");
		contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

		var res = client.PostAsync(stPostURL, contentBody).Result;

		var responseContent = res.Content;

		var jsonfactfindadviser = responseContent.ReadAsStringAsync().Result;

		var jocampaignfactfindadviser = Newtonsoft.Json.Linq.JObject.Parse(jsonfactfindadviser);

		Response.Write(jocampaignfactfindadviser);

	



	}

}
