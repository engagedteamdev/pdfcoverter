using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Collections.Specialized;

public partial class Default : Page
{

	private string strType = HttpContext.Current.Request["Type"];
	private string strBankID = HttpContext.Current.Request["BankID"];
	private string strCompanyName = HttpContext.Current.Request["CompanyName"];
	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strAccessID = HttpContext.Current.Request["AccessID"];


	private string strHooyuURL = "";

	//Class object for the Institution json
	class theData
	{
		public string name { get; set; }
		public string scope { get; set; }
		public string phone { get; set; }
		public string country { get; set; }
		public string email { get; set; }
		public string environment { get; set; }
		public string reference { get; set; }
		public string callback_uri { get; set; }
	}

	

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{

			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{

		System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringMetroTest"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		theData data = new theData();
		string json = "";

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwhooyu where AppID = '{0}'", strAppID), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			data.name = reader["ClientName"].ToString();
			data.phone = reader["App1MobileTelephone"].ToString();
			data.email = reader["App1EmailAddress"].ToString();
			data.scope = "documents";
			data.country = "gb";
			data.environment = "sandbox";
			data.callback_uri = "https://integrations.engagedcrm.co.uk/webservices/outbound/hooyu/defaultcallback.aspx";

		}

			json = JsonConvert.SerializeObject(data);
			//HttpContext.Current.Response.Write(json + "</br>");
			//HttpContext.Current.Response.End();

			strHooyuURL = "https://www.hooyu.com/api/1.6/request/send";
			HttpClient client = new HttpClient();
			byte[] cred = UTF8Encoding.UTF8.GetBytes("e73c1523-bf50-459d-9126-76848591aecb:16ec79058076605dfb479c2237fdd7ce");
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(cred));
			client.BaseAddress = new Uri(strHooyuURL);
			
			
			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strHooyuURL, contentBody).Result;

			var responseContent = res.Content;

			Response.Write("" + responseContent.ReadAsStringAsync().Result + "");
			HttpContext.Current.Response.End();

			client.Dispose();

	}


	
}
