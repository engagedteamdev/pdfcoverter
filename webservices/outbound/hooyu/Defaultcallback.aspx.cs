using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.IO.Compression;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Collections.Specialized;
using System.Net.Http;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];




	class theCRMJsonPost
	{
		public string AppID { get; set; }
		public string PDFData { get; set; }
		public string PDFFileName { get; set; }
		public string ReportDebits { get; set; }
	}


	public void httpPost()
	{


		Common.postEmail("", "william.worthington@engagedcrm.co.uk", "Hooyu Hit", "Hooyu Hit", true, "", true);

		String data = new System.IO.StreamReader(Context.Request.InputStream).ReadToEnd();



		var json = JObject.Parse(data);

		Common.postEmail("", "william.worthington@engagedcrm.co.uk", "Hooyu Hit Json", json.ToString(), true, "", true);
	


		var Images = System.Convert.FromBase64String(json["images"]["pdf"].ToString());



		var Reference = json["reference"].ToString();

	

		//PDF
		byte[] bytes = Images;



		Stream resFilestream = new MemoryStream(bytes);
		FileStream stream = new FileStream(@"C:\HooyuTemp\Documents-" + Reference + ".pdf", FileMode.Create);

		BinaryWriter bw = new BinaryWriter(stream);
		byte[] ba = new byte[resFilestream.Length];
		resFilestream.Read(ba, 0, ba.Length);
		bw.Write(ba);
		bw.Close();
		resFilestream.Close();


		File.Delete(@"C:\HooyuTemp\Documents-" + Reference + ".pdf");



		var strContent = System.Convert.ToBase64String(bytes);

		theCRMJsonPost CRMPostdata = new theCRMJsonPost();
		CRMPostdata.AppID = Reference;
		CRMPostdata.PDFData = strContent;
		CRMPostdata.PDFFileName = "Hooyu Report";
		string url = "https://metrofinancebeta.engagedcrm.co.uk/webservices/outbound/HooyuDocuments/default.aspx";
		HttpClient crmdataclient = new HttpClient();
		crmdataclient.BaseAddress = new Uri(url);

		var jsontwo = JsonConvert.SerializeObject(CRMPostdata);
		HttpContent contentBodyCRM = new StringContent(jsontwo);
		contentBodyCRM.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var rescrm = crmdataclient.PostAsync(url, contentBodyCRM).Result;

		var responseContentCRM = rescrm.Content;

		crmdataclient.Dispose();

		try
		{

		//Selfie
		byte[] SelfieImages = System.Convert.FromBase64String(json["images"]["selfie"].ToString());


		var strContentSelfie = System.Convert.ToBase64String(SelfieImages);

		theCRMJsonPost CRMPostdataSelfie = new theCRMJsonPost();
		CRMPostdataSelfie.AppID = Reference;
		CRMPostdataSelfie.PDFData = strContentSelfie;
		CRMPostdataSelfie.PDFFileName = "Hooyu Selfie";
		HttpClient crmdataclientSelfie = new HttpClient();
		crmdataclientSelfie.BaseAddress = new Uri(url);

		var jsontwoSelfie = JsonConvert.SerializeObject(CRMPostdataSelfie);
		HttpContent contentBodyCRMSelfie = new StringContent(jsontwoSelfie);
		contentBodyCRMSelfie.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var rescrmSelfie = crmdataclientSelfie.PostAsync(url, contentBodyCRMSelfie).Result;

		crmdataclientSelfie.Dispose();

		}
		catch
		{

		}


		try
		{

		//Passport
		byte[] PassportImages = System.Convert.FromBase64String(json["images"]["passport"].ToString());

		var strContentPassport = System.Convert.ToBase64String(PassportImages);

		theCRMJsonPost CRMPostdataPassport = new theCRMJsonPost();
		CRMPostdataPassport.AppID = Reference;
		CRMPostdataPassport.PDFData = strContentPassport;
		CRMPostdataPassport.PDFFileName = "Hooyu Passport";
		HttpClient crmdataclientPassport = new HttpClient();
		crmdataclientPassport.BaseAddress = new Uri(url);

		var jsontwoPassport = JsonConvert.SerializeObject(CRMPostdataPassport);
		HttpContent contentBodyCRMPassport = new StringContent(jsontwoPassport);
		contentBodyCRMPassport.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var rescrmPassport = crmdataclientPassport.PostAsync(url, contentBodyCRMPassport).Result;

		crmdataclientPassport.Dispose();

		}
		catch
		{

		}



		try
		{
		//DrivingFront
		byte[] DrivingFrontImages = System.Convert.FromBase64String(json["images"]["driving"]["front"].ToString());

		var strContentDrivingFront = System.Convert.ToBase64String(DrivingFrontImages);

		theCRMJsonPost CRMPostdataDrivingFront = new theCRMJsonPost();
		CRMPostdataDrivingFront.AppID = Reference;
		CRMPostdataDrivingFront.PDFData = strContentDrivingFront;
		CRMPostdataDrivingFront.PDFFileName = "Hooyu Driving Front";
		HttpClient crmdataclientDrivingFront = new HttpClient();
		crmdataclientDrivingFront.BaseAddress = new Uri(url);

		var jsontwoDrivingFront = JsonConvert.SerializeObject(CRMPostdataDrivingFront);
		HttpContent contentBodyCRMDrivingFront = new StringContent(jsontwoDrivingFront);
		contentBodyCRMDrivingFront.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var rescrmDrivingFront = crmdataclientDrivingFront.PostAsync(url, contentBodyCRMDrivingFront).Result;

		crmdataclientDrivingFront.Dispose();

		}
		catch
		{

		}

		try
		{

		//DrivingBack
		byte[] DrivingBackImages = System.Convert.FromBase64String(json["images"]["driving"]["back"].ToString());


			var strContentDrivingBack = System.Convert.ToBase64String(DrivingBackImages);

			theCRMJsonPost CRMPostdataDrivingBack = new theCRMJsonPost();
			CRMPostdataDrivingBack.AppID = Reference;
			CRMPostdataDrivingBack.PDFData = strContentDrivingBack;
			CRMPostdataDrivingBack.PDFFileName = "Hooyu Driving Back";
			HttpClient crmdataclientDrivingBack = new HttpClient();
			crmdataclientDrivingBack.BaseAddress = new Uri(url);

			var jsontwoDrivingBack = JsonConvert.SerializeObject(CRMPostdataDrivingBack);
			HttpContent contentBodyCRMDrivingBack = new StringContent(jsontwoDrivingBack);
			contentBodyCRMDrivingBack.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var rescrmDrivingBack = crmdataclientDrivingBack.PostAsync(url, contentBodyCRMDrivingBack).Result;

			crmdataclientDrivingBack.Dispose();


		}
		catch
		{

		}

	}
}

