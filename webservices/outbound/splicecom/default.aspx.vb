﻿Imports Config, Common
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class ClickToDial
    Inherits System.Web.UI.Page

    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private strTelephoneTo As String = HttpContext.Current.Request("TelephoneTo")
    Private UserSessionID As String = HttpContext.Current.Request("UserSessionID")
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strClickToDialProviderURL As String = "", strClickToDialProviderURL2 As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""
    Private strUserID As String = "", strUserName As String = ""

'    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
'        Dim exc As Exception = Server.GetLastError
'        Dim excMessage As String = "", excInnerException As String = ""
'        If Not exc Is Nothing Then
'            excMessage = exc.Message.ToString
'            If Not exc.InnerException Is Nothing Then
'                excInnerException = exc.InnerException.Message.ToString
'            End If
'        Else
'            excMessage = "No error message"
'        End If
'        HttpContext.Current.Response.Clear()
'        HttpContext.Current.Response.Write("2|An unknown error has occurred")
'        'sendMail("Click to Dial Make Call Error", "Err14: An unknown response has been received from " & strClickToDialProviderURL & " in splicecom/default.aspx.<br />" & excMessage & "<br />" & excInnerException)
'        HttpContext.Current.Response.End()
'    End Sub

    Private Enum ActionType
        makeCall = 1
        endCall = 2
    End Enum

    Public Sub clickToDial()
        Dim strResult As String = ""
        strClickToDialProviderURL = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "ClickToDialProviderURL")
        strClickToDialProviderURL2 = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "ClickToDialProviderURL2")
        strUserID = getAnyFieldByCompanyID("UserID", "tblusers", "UserSessionID", UserSessionID)
        If checkValue(strClickToDialProviderURL) Then
            Select Case intActionType
                Case ActionType.makeCall
                    strResult = makeCall(strClickToDialProviderURL)
                    Dim arrResult As Array = Split(strResult, "|")
                    If (arrResult(0) <> "0") Then
                        strResult = makeCall(strClickToDialProviderURL2)
                        Dim arrResult2 As Array = Split(strResult, "|")
                        Select Case arrResult2(0)
                            Case "0"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("0|Success")
                            Case "1"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("1|" & arrResult2(1))
                            Case "2"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("2|An unknown error has occurred")
                        End Select
                    End If
                Case ActionType.endCall
                    strResult = endCall(strClickToDialProviderURL)
                    Dim arrResult As Array = Split(strResult, "|")
                    If (arrResult(0) <> "0") Then
                        strResult = endCall(strClickToDialProviderURL2)
                        Dim arrResult2 As Array = Split(strResult, "|")
                        Select Case arrResult2(0)
                            Case "0"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("0|Success")
                            Case "1"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("1|" & arrResult2(1))
                            Case "2"
                                HttpContext.Current.Response.Clear()
                                HttpContext.Current.Response.Write("2|An unknown error has occurred")
                        End Select
                    End If
            End Select
        End If
    End Sub

    Private Function makeCall(url As String) As String
        Dim strPostData As String = "cmd=newcall&force=1"
        Dim strSpliceComExtension As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialExtension" Then strSpliceComExtension = Row.Item("StoredUserValue").ToString
            Next
        End If
        dsCache = Nothing

        strPostData += "&src=" & strSpliceComExtension & "&dest=" & strTelephoneTo

        If Not checkValue(strSpliceComExtension) Or Not checkValue(strTelephoneTo) Then ' Error in data before sending
            Return "2|An error has occurred"
            sendMail("Click to Dial Make Call Error", "Err15: A default value is not present in splicecom/default.aspx: " & strErrorMessage)
        Else
            Dim objResponse As HttpWebResponse = postWebRequest(strClickToDialProviderURL, strPostData, False, False, False)
            Dim boolValid As Boolean = checkResponse(objResponse)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse = objReader.ReadToEnd()
            objReader = Nothing
            objResponse = Nothing

            If (boolValid = False) Then
                Return "2|An error has occurred"
            Else
                Dim strResult As String = "0", strError As String = ""
                Dim arrValuePairs As String() = Split(strResponse.ToString, "&")
                For i As Integer = 0 To UBound(arrValuePairs)
                    Dim arrValues As String() = Split(arrValuePairs(i), "=")
                    If arrValues(0) = "result" Then strResult = arrValues(1)
                    If arrValues(0) = "error" Then strError = arrValues(1)
                Next

                '				responseWrite(strError)
                '				responseEnd()
                Select Case strResult
                    Case "1" ' Success
                        Return ("0|Success")
                    Case "0" ' Call failure error
                        Return "1|" & strError
                        sendMail("Click to Dial Make Call Error", "Err16: An unknown response has been received from " & strClickToDialProviderURL & " in splicecom/default.aspx.<br />" & strError)
                    Case Else
                        Return "2|An unknown error has occurred"
                        sendMail("Click to Dial Make Call Error", "Err16: An unknown response has been received from " & strClickToDialProviderURL & " in splicecom/default.aspx.<br />" & strError)
                End Select
            End If
        End If
    End Function

    Private Function endCall(url As String) As String
        Dim strPostData As String = "cmd=endallcalls&force=1"
        Dim SpliceComLineNumber As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialExtension" Then SpliceComLineNumber = Row.Item("StoredUserValue").ToString
            Next
        End If
        dsCache = Nothing

        strPostData += "&src=" & SpliceComLineNumber

        If Not checkValue(SpliceComLineNumber) Then ' Error in data before sending
            Return "2|An error has occurred"
            sendMail("Click to Dial End Call Error", "Err15: A default value is not present in splicecom/default.aspx: " & strErrorMessage)
        Else
            Dim objResponse As HttpWebResponse = postWebRequest(url, strPostData, False, False, False)
            Dim boolValid As Boolean = checkResponse(objResponse)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse = objReader.ReadToEnd()
            objReader = Nothing
            objResponse = Nothing

            If (boolValid = False) Then
                Return "2|An error has occurred"
            Else
                Dim strResult As String = "0", strError As String = ""
                Dim arrValuePairs As String() = Split(strResponse.ToString, "&")
                For i As Integer = 0 To UBound(arrValuePairs)
                    Dim arrValues As String() = Split(arrValuePairs(i), "=")
                    If arrValues(0) = "result" Then strResult = arrValues(1)
                    If arrValues(0) = "error" Then strError = arrValues(1)
                Next

                Select Case strResult
                    Case "1" ' Success
                        Return "0|Success"
                    Case "0" ' Call failure error
                        Return "1|" & strError
                    Case Else
                        Return "2|An unknown error has occurred"
                        sendMail("Click to Dial End Call Error", "Err16: An unknown response has been received from " & strClickToDialProviderURL & " in splicecom/default.aspx.<br />" & strError)
                End Select
            End If
        End If
    End Function

    Private Function checkResponse(ByRef objResponse As HttpWebResponse) As Boolean
        Dim boolValid = True
        Dim excInnerException As String = ""
        Try ' Make sure the response is valid
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            If Not (objResponse.StatusCode.ToString = "OK") Then
                sendMail("Click to Dial Call Error", "Err19: An unknown status code has been received from " & strClickToDialProviderURL & " in splicecom/default.aspx.")
                boolValid = False
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValid = False
        End Try
        Return boolValid
    End Function

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        'getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=False&strAttachment=&UserSessionID=" & UserSessionID)
    End Sub

End Class
