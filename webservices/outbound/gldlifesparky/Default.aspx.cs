using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Collections.Specialized;

public partial class Default : Page
{

	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strPostURL = "";

	class ServiceModel
	{
		public string App1FirstName { get; set; }
		public string App1Surname { get; set; }
		public string App1EmailAddress { get; set; }
		public string App1MobileTelehone { get; set; }
		public string App1HomeTelehone { get; set; }
		public string App1DOB { get; set; }
		public int Amount { get; set; }
		public string AddressPostCode { get; set; }
		public string UTMTerm { get; set; }
		public string IPAddress { get; set; }
	}

	class postData
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string LeadSource { get; set; }
		public string Sub_Source__c { get; set; }
		public string Birthdate__c { get; set; }
		public DateTime Partner_time_captured__c { get; set; }
		public int Quote_Enquiry_Amount__c { get; set; }
		//public string Salutation { get; set; }
		//public string Address_line_1__c { get; set; }
		//public string Address_line_2__c { get; set; }
		//public string Address_line_3__c { get; set; } 
		//public string Address_line_4__c { get; set; }
		//public string Address_line_5__c { get; set; }
		public string Postalcode { get; set; }
		//public int Age_range__c { get; set; }
		//public string Gender__c { get; set; }
		public string Source_IP__c { get; set; }
		public string Source_product__c { get; set; }
		public string Partner_site__c { get; set; }
		public string Partner_ID_1__c { get; set; }
		public string Partner_ID_2__c { get; set; }

	}

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{

		ServicePointManager.Expect100Continue = true;
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		ServiceModel model = new ServiceModel();

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringSparky"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwgldlife where AppID = '{0}'", strAppID), connection);

		var readersql = myCommand.ExecuteReader();

		while (readersql.Read())
		{

			int Amount = 0;
			int.TryParse(readersql["Amount"].ToString(), out Amount);

			model.App1FirstName = readersql["App1FirstName"].ToString();
			model.App1Surname = readersql["App1Surname"].ToString();
			model.App1MobileTelehone = readersql["App1MobileTelephone"].ToString();
			model.App1HomeTelehone = readersql["App1HomeTelephone"].ToString();
			model.App1EmailAddress = readersql["App1EmailAddress"].ToString();
			model.App1DOB = readersql["App1DOB"].ToString();
			model.Amount = Amount;
			model.AddressPostCode = readersql["AddressPostCode"].ToString();
			model.UTMTerm = readersql["UTMTerm"].ToString();
			model.IPAddress = readersql["ApplicationIPAddress"].ToString();
		}


		try
		{
			string json = "";

			
			string rt = "";

			try
			{

				//strPostURL = "https://test.salesforce.com/services/oauth2/token?grant_type=refresh_token&refresh_token=5Aep8612epSmlVl3FchpD_bAjZuspk.vSfiZKwmXmBMO6mvgPTMNTbOpW3lEsPeP08G5pBOKzA2dxyCgvxTs_Xg&client_id=3MVG9lcxCTdG2VbtWAnBLcHTY.68GuUoLtGrvmBFRjFl1VfhTy3JSvtSxkqpuH_f10Ms3ForeAClJw1iJ3kne&client_secret=3986963500954330943";

				strPostURL = "https://login.salesforce.com/services/oauth2/token?grant_type=refresh_token&refresh_token=5Aep861k56jbBM_YiGT4iqXQ8gLL5m6ZGfdwlLznYtxKrGv5xgTJtcZzS57MTeRQXl97FKlfrw0PVt4HyZ1UJf7&client_id=3MVG9HxRZv05HarRGNDDeMPazu7vJFkXc_Q6O4BbtCmxY4677oQXFcfLhz.ogo4EveD.1HcB04BPmamQhApMu&client_secret=4696725430643431661";

				

				WebRequest request = WebRequest.Create(strPostURL);
				request.Method = "Post";
				WebResponse response = request.GetResponse();

				Stream dataStream = response.GetResponseStream();

				StreamReader reader = new StreamReader(dataStream);

				rt = reader.ReadToEnd();
			}
			catch (Exception ex)
			{
				HttpContext.Current.Response.Write("Auth Request" + ex.Message);
				HttpContext.Current.Response.Flush();
				HttpContext.Current.Response.SuppressContent = true;
				HttpContext.Current.ApplicationInstance.CompleteRequest();

			}




			var jo = JObject.Parse(rt);


			var AccessToken = jo["access_token"].ToString();

			var InstanceURL = jo["instance_url"].ToString();

			//Response.Write("Accesss Token:" + AccessToken + " Instance URL:" + InstanceURL);
			//HttpContext.Current.Response.End();

			postData data = new postData();
			data.FirstName = model.App1FirstName;
			data.LastName = model.App1Surname;
			data.Email = model.App1EmailAddress;
			if (model.App1MobileTelehone != "")
			{
				data.Phone = model.App1MobileTelehone;
			}
			else
			{
				data.Phone = model.App1HomeTelehone;
			}
			data.LeadSource = "3rd Party Funeral Site";
			data.Sub_Source__c = "Engaged Solutions";
			data.Partner_time_captured__c = DateTime.UtcNow;
			data.Birthdate__c = model.App1DOB;
			data.Quote_Enquiry_Amount__c = model.Amount;
			data.Postalcode = model.AddressPostCode;
			data.Partner_ID_1__c = strAppID;
			data.Partner_ID_2__c = model.UTMTerm;
			data.Partner_site__c = "lifecover.com";
			data.Source_product__c = "OFP";
			data.Source_IP__c = model.IPAddress;

			json = JsonConvert.SerializeObject(data);
			//HttpContext.Current.Response.ContentType = "application/json";
			//HttpContext.Current.Response.Write(json);
			//HttpContext.Current.Response.Flush();
			//HttpContext.Current.Response.SuppressContent = true;  
			//HttpContext.Current.ApplicationInstance.CompleteRequest();

			strPostURL = InstanceURL + "/services/data/v37.0/sobjects/Lead/";

			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(strPostURL);
			client.DefaultRequestHeaders.Add("Authorization", "Bearer " + AccessToken);

			HttpContent contentBody = new StringContent(json);

			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strPostURL, contentBody).Result;
			
			var responseContent = res.Content;

		

			var responseJson = JObject.Parse(responseContent.ReadAsStringAsync().Result);

			var ResponseID = responseJson["id"].ToString();
			


			var ResponseStatus = responseJson["success"].ToString();

			if (ResponseStatus == "True")
			{
				HttpContext.Current.Response.Write("1|" + ResponseID);
				
			}
			else
			{
				var Error = jo["error"][0].ToString();

				HttpContext.Current.Response.Write("0|" + Error);
			}
			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();

		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write("0|" + ex.Message);
			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

	}


	
}
