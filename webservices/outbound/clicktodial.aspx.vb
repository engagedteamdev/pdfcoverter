﻿Imports Config, Common
Imports System.Net
Imports System.IO

Partial Class ClickToDial
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = Nothing
    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private strTelephoneTo As String = HttpContext.Current.Request("TelephoneTo")
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strClickToDialProvider As String = "", strUserName As String = ""
	
    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        Dim exc As Exception = Server.GetLastError
        Dim excMessage As String = "" ' exc.Message.ToString
        Dim excInnerException As String = "" ' exc.InnerException.Message.ToString

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Write("2|An unknown error has occurred")
        'sendMail("Click to Dial Error", "An error has occured in clicktodial.aspx. The response from " & strClickToDialProvider & " is unknown.<br />" & excMessage & "<br />" & excInnerException)
        HttpContext.Current.Response.End()
    End Sub	

    Public Sub clickToDial()
        strClickToDialProvider = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "ClickToDialProvider")
        If checkValue(strClickToDialProvider) Then
            Call sendCommand()
        End If
    End Sub

    Private Sub sendCommand()
        'responseWrite(Config.ApplicationURL & "/webservices/outbound/" & strClickToDialProvider & "/?AppID=" & AppID & "&TelephoneTo=" & strTelephoneTo & "&intActionType=" & intActionType & "&UserSessionID=" & UserSessionID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/" & strClickToDialProvider & "/?AppID=" & AppID & "&TelephoneTo=" & strTelephoneTo & "&intActionType=" & intActionType & "&UserSessionID=" & UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

        Dim strResponse As String = objReader.ReadToEnd()
        Dim arrResponse = Split(strResponse, "|")
        Select Case arrResponse(0)
            Case "0" ' Success
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("0|Success")
            Case "1" ' Display user message
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("1|" & decodeURL(arrResponse(1)))
            Case "2" ' System error
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("2|" & decodeURL(arrResponse(1)))
                'sendMail("Click to Dial Error", "An error has occured in clicktodial.aspx. The response from " & strClickToDialProvider & " is " & arrResponse(1))
            Case Else ' Failure
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("2|An unknown error has occurred")
                'sendMail("Click to Dial Error", "An error has occured in clicktodial.aspx. The response from " & strClickToDialProvider & " is unknown.")
        End Select

    End Sub

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&strAttachment=&UserSessionID=" & UserSessionID)
    End Sub

End Class
