using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.ServiceModel;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;

public partial class Default : Page
{

	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strCRMInstance = HttpContext.Current.Request["CRMInstance"];
	private string strPostURL = "";


	class leadoverall
	{
	
		public int productCode { get; set; }
		public int term { get; set; }
		public int principal { get; set; }
		public string brokerId { get; set; }
		public thecustomers[] customers { get; set; }
	}

	class thecustomers
	{
		public string title { get; set; }
		public string gender { get; set; }
		public string forename { get; set; }
		public string surname { get; set; }
		public string DOB { get; set; }
		public int numberOfDependants { get; set; }
		public string maritalStatus { get; set; }
		public string homeTelephone { get; set; }
		public string mobileNumber { get; set; }
		public string email { get; set; }
		public int salary { get; set; }
		public string employerName { get; set; }
		public string jobTitle { get; set; }
		public string occupationType { get; set; }
		public string bankSortCode { get; set; }
		public string bankAccountNumber { get; set; }
		public string bankAccountName { get; set; }
		public string residentialStatus { get; set; }
		public themortgageData mortgageData { get; set; }
		public theaddresses[] addresses { get; set; }

	}

	class themortgageData
	{
		public int mortgageMonthlyPayment { get; set; }
		public int mortgagePropertyValuation { get; set; }
		public int mortgageBalanceOutstanding { get; set; }
		public string mortgageLoanCompany { get; set; }

	}

	class theaddresses
	{
		
		public string addressType { get; set; }
		public string atAddressFrom { get; set; }
		public string atAddressTo { get; set; }
		public string addressLine1 { get; set; }
		public string addressLine2 { get; set; }
		public string town { get; set; }
		public string county { get; set; }
		public string postCode { get; set; }
		public string propertyType { get; set; }
		public string bedrooms { get; set; }

	}

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{

		ServicePointManager.Expect100Continue = true;
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		leadoverall lead = new leadoverall();
		thecustomers[] customers = new thecustomers[1];
		thecustomers acustomer = new thecustomers();
		themortgageData MortgageData = new themortgageData();
		theaddresses[] addressdata = new theaddresses[2];
		theaddresses anaddress = new theaddresses();
		theaddresses empanaddress = new theaddresses();

		SqlConnection connection = new SqlConnection();

		if (strCRMInstance == "ES")
		{
			connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());
		}
		else if (strCRMInstance == "Sparky")
		{
			connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringSparkyBeta"].ToString());
		}
		else
		{
			HttpContext.Current.Response.Write("0| No Database Connection");
			HttpContext.Current.Response.End();
		}

		try
		{
			connection.Open();
		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write("0| " + ex.Message + " ");
			HttpContext.Current.Response.End();
		}


		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwfirststop where AppID = '{0}'", strAppID), connection);

		var readersql = myCommand.ExecuteReader();

		while (readersql.Read())
		{

			lead.productCode = 26;

			int Term = 0;
			int.TryParse(readersql["ProductTerm"].ToString(), out Term);
			lead.term = Term;

			int Amount = 0;
			int.TryParse(readersql["Amount"].ToString(), out Amount);
			lead.principal = Amount;

			lead.brokerId = "70167";

			acustomer.title = readersql["App1Title"].ToString();
			if (acustomer.title == "")
			{
				acustomer.title = null;
			}
			acustomer.gender = readersql["App1Gender"].ToString();
			if (acustomer.gender == "")
			{
				acustomer.gender = null;
			}
			acustomer.forename = readersql["App1Firstname"].ToString();
			if (acustomer.forename == "")
			{
				acustomer.forename = null;
			}
			acustomer.surname = readersql["App1Surname"].ToString();
			if (acustomer.surname == "")
			{
				acustomer.surname = null;
			}
			acustomer.DOB = readersql["App1DOB"].ToString();
			if (acustomer.DOB == "")
			{
				acustomer.DOB = null;
			}

			int NoOfDependants = 0;
			int.TryParse(readersql["App1NoOfDependents"].ToString(), out NoOfDependants);
			acustomer.numberOfDependants = NoOfDependants;
			
			acustomer.maritalStatus = readersql["App1MaritalStatus"].ToString();
			if (acustomer.maritalStatus == "")
			{
				acustomer.maritalStatus = null;
			}
			acustomer.homeTelephone = readersql["App1HomeTelephone"].ToString();
			if (acustomer.homeTelephone == "")
			{
				acustomer.homeTelephone = null;
			}
			acustomer.mobileNumber = readersql["App1MobileTelephone"].ToString();
			if (acustomer.mobileNumber == "")
			{
				acustomer.mobileNumber = null;
			}
			acustomer.email = readersql["App1EmailAddress"].ToString();
			if (acustomer.email == "")
			{
				acustomer.email = null;
			}

			int App1Salary = 0;
			int.TryParse(readersql["App1Salary"].ToString(), out App1Salary);
			acustomer.salary = App1Salary;

			acustomer.employerName = readersql["App1EmployerName"].ToString();
			if (acustomer.employerName == "")
			{
				acustomer.employerName = null;
			}
			acustomer.jobTitle = readersql["App1Occupation"].ToString();
			if (acustomer.jobTitle == "")
			{
				acustomer.jobTitle = null;
			}
			acustomer.occupationType = readersql["App1OccupationType"].ToString();
			if (acustomer.occupationType == "")
			{
				acustomer.occupationType = null;
			}
			acustomer.bankSortCode = readersql["App1BankSortCode"].ToString();
			if (acustomer.bankSortCode == "")
			{
				acustomer.bankSortCode = null;
			}
			acustomer.bankAccountNumber = readersql["App1BankAccountNumber"].ToString();
			if (acustomer.bankAccountNumber == "")
			{
				acustomer.bankAccountNumber = null;
			}
			acustomer.bankAccountName = readersql["App1BankName"].ToString();
			if (acustomer.bankAccountName == "")
			{
				acustomer.bankAccountName = null;
			}
			acustomer.residentialStatus = readersql["AddressLivingArrangement"].ToString();
			if (acustomer.residentialStatus == "")
			{
				acustomer.residentialStatus = null;
			}

			int MortgageMonthlyPayment = 0;
			int.TryParse(readersql["MortgageMonthlyPayment"].ToString(), out MortgageMonthlyPayment);
			MortgageData.mortgageMonthlyPayment = MortgageMonthlyPayment;

			int MortgagePropertyValuation = 0;
			int.TryParse(readersql["MortgagePropertyValuation"].ToString(), out MortgagePropertyValuation);
			MortgageData.mortgagePropertyValuation = MortgagePropertyValuation;

			int MortgageBalanceOutstanding = 0;
			int.TryParse(readersql["MortgageBalanceOutstanding"].ToString(), out MortgageBalanceOutstanding);
			MortgageData.mortgageBalanceOutstanding = MortgageBalanceOutstanding;
			MortgageData.mortgageLoanCompany = readersql["MortgageLoanCompany"].ToString();

			anaddress.addressType = "Current";
			anaddress.atAddressTo = readersql["AddressMoveOutDate"].ToString();
			if (anaddress.atAddressTo == "")
			{
				anaddress.atAddressTo = null;
			}
			anaddress.atAddressFrom = readersql["AddressMoveInDate"].ToString();
			if (anaddress.atAddressFrom == "")
			{
				anaddress.atAddressFrom = null;
			}
			anaddress.addressLine1 = readersql["AddressLine1"].ToString();
			if (anaddress.addressLine1 == "")
			{
				anaddress.addressLine1 = null;
			}
			anaddress.addressLine2 = readersql["AddressLine2"].ToString();
			if (anaddress.addressLine2 == "")
			{
				anaddress.addressLine2 = null;
			}
			anaddress.town = readersql["AddressTown"].ToString();
			if (anaddress.town == "")
			{
				anaddress.town = null;
			}
			anaddress.county = readersql["AddressCounty"].ToString();
			if (anaddress.county == "")
			{
				anaddress.county = null;
			}
			anaddress.postCode = readersql["AddressPostcode"].ToString();
			if (anaddress.postCode == "")
			{
				anaddress.postCode = null;
			}
			anaddress.propertyType = "4";

			anaddress.bedrooms = readersql["NoOfBedrooms"].ToString();
			if (anaddress.bedrooms == "")
			{
				anaddress.bedrooms = null;
			}

			empanaddress.addressType = "EmployerCurrent";
			empanaddress.atAddressFrom = readersql["App1EmploymentStartDate"].ToString();
			if (empanaddress.atAddressFrom == "")
			{
				anaddress.atAddressFrom = null;
			}

			empanaddress.postCode = readersql["App1EmployerPostCode"].ToString();
			if (empanaddress.postCode == "")
			{
				anaddress.postCode = null;
			}

			empanaddress.addressLine1 = readersql["App1EmployerAddressLine1"].ToString();
			if (empanaddress.addressLine1 == "")
			{
				anaddress.addressLine1 = null;
			}

			empanaddress.town = readersql["App1EmploymentTown"].ToString();
			if (empanaddress.town == "")
			{
				anaddress.town = null;
			}


		}

		
		try
		{
			string json = "";
			customers[0] = acustomer;
			lead.customers = customers;

			addressdata[0] = anaddress;
			addressdata[1] = empanaddress;
			customers[0].addresses = addressdata;
			customers[0].mortgageData = MortgageData;

			


			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;
		
			json = JsonConvert.SerializeObject(lead, settings);

			//HttpContext.Current.Response.Write(json);
			//HttpContext.Current.Response.Flush();
			//HttpContext.Current.Response.SuppressContent = true;
			//HttpContext.Current.ApplicationInstance.CompleteRequest();

			strPostURL = "https://api-test.1stsg.co.uk/proposals/createproposal/json";
			HttpClient client = new HttpClient();
			client.DefaultRequestHeaders.Add("apiKey", "e861180b-fe38-4f74-b360-cf0ff3d25a34");
			client.BaseAddress = new Uri(strPostURL);

			HttpContent contentBody = new StringContent(json);

			contentBody.Headers.ContentType = new MediaTypeHeaderValue("application/json");


			var res = client.PostAsync(strPostURL, contentBody).Result;

			var resString = res.Content.ReadAsStringAsync();

	
			//HttpContext.Current.Response.Write(resString.Result.ToString());
			//HttpContext.Current.Response.Flush();
			//HttpContext.Current.Response.SuppressContent = true;
			//HttpContext.Current.ApplicationInstance.CompleteRequest();



			if (res.IsSuccessStatusCode)
			{
				var parsed = JObject.Parse(resString.Result.ToString());

				var resultCode = parsed.SelectToken("result").Value<string>();

				var resultID = parsed.SelectToken("id").Value<string>();

				HttpContext.Current.Response.Write("1|" + resultID);
				
			}
			else
			{
				HttpContext.Current.Response.Write("0|" + res.IsSuccessStatusCode + " " + res.ReasonPhrase);
			}


			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();

		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write(ex.Message.ToString());
			HttpContext.Current.Response.Flush();
			HttpContext.Current.Response.SuppressContent = true;
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

	}

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());
		connection.Open();
		SqlCommand updatecmd2 = new SqlCommand();
		updatecmd2.Connection = connection;
		updatecmd2.CommandText = string.Format("INSERT INTO tblxmlreceived(XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES('" + ip + "', '" + strAppID + "', 1, '" + msg + "', '" + strPost + "', getdate()) ");
		updatecmd2.ExecuteNonQuery();
		connection.Close();
	}



}
