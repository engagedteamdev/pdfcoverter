﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLG2F
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strUserName As String = "", strPassword As String = "", strBroker As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objAuthXMLDoc As XmlDocument = New XmlDocument, objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Public Sub generateXml()
		Server.ScriptTimeout = 90
        If (strEnvironment = "live") Then
        	strXMLURL = "https://www.gateway2finance.co.uk/_advances/Service/Transfer.asmx"
        Else
        	strXMLURL = "https://uat.gateway2finance.co.uk/_advances/Service/Transfer.asmx"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%G2F%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "G2FUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "G2FPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "G2FBroker") Then strBroker = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/g2f/blank.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("def", "http://tbred.co.uk/")

        writeToNode(objInputXMLDoc, "y", "def:AuthenticationInfo", "def:Login", strUserName, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:AuthenticationInfo", "def:Password", strPassword, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:AuthenticationInfo", "def:Broker", strBroker, objNSM)

       ' HttpContext.Current.Response.ContentType = "text/xml"
'        HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
'        HttpContext.Current.Response.End()

        Dim strSQL As String = "SELECT * FROM vwxmlg2f WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strAddressHouseName As String = "", strAddressHouseNumber As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                            writeToNode(objInputXMLDoc, "y", "def:application/def:Applicants/def:Applicant[1]", "def:Dob", ddmmyyhhmmss2utc(Row(Column).ToString, False, False), objNSM)
                        ElseIf (Column.ColumnName.ToString = "AddressHouseName") Then
                            strAddressHouseName = Row(Column).ToString
                        ElseIf (Column.ColumnName.ToString = "AddressHouseNumber") Then
                            strAddressHouseNumber = Row(Column).ToString
                        ElseIf (Column.ColumnName.ToString = "AddressLine1") Then
                            If (checkValue(strAddressHouseName) Or checkValue(strAddressHouseNumber)) Then
                                writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:HouseName", strAddressHouseName, objNSM)
                                writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:Number", strAddressHouseNumber, objNSM)
                                writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:Street", Row(Column).ToString, objNSM)
                            Else
                                Dim arrAddressLine1 As Char() = Row(Column).ToString.ToCharArray
                                Dim strNewAddressHouseNumber As String = "", strAddressLine1 As String = ""
                                Dim strLastChar As String = "", intPos As String = ""
                                For x As Integer = 0 To UBound(arrAddressLine1)
                                    If (Not IsNumeric(arrAddressLine1(x))) Then
                                        If (IsNumeric(strLastChar)) Then
                                            intPos = x
                                            Exit For
                                        End If
                                    End If
                                    strLastChar = arrAddressLine1(x)
                                Next
                                If (checkValue(intPos)) Then
                                    strNewAddressHouseNumber = Left(Row(Column).ToString, CInt(intPos))
                                    strAddressLine1 = Mid(Row(Column).ToString, CInt(intPos) + 2)
                                Else
                                    strAddressLine1 = Row(Column).ToString
                                End If
								If (IsNumeric(strNewAddressHouseNumber)) Then
								writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:Number", strNewAddressHouseNumber, objNSM)
								Else
								writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:HouseName", strNewAddressHouseNumber, objNSM)
								End If
                                writeToNode(objInputXMLDoc, "n", "def:application/def:Applicants/def:Applicant[1]/def:CurrentAddress/def:Address", "def:Street", strAddressLine1, objNSM)
                            End If
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += "def:" & arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & arrName(UBound(arrName)), Row(Column).ToString, objNSM)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postG2FWebRequest(strXMLURL, objInputXMLDoc.InnerXml, "http://tbred.co.uk/SubmitApplication")
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            objNSM2 = New XmlNamespaceManager(objOutputXMLDoc.NameTable)
            objNSM2.AddNamespace("def", "http://tbred.co.uk/")

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//def:SubmitApplicationResult", objNSM2)
                Dim objNodeList As XmlNodeList = objApplication.SelectNodes("//def:SubmitApplicationResult/def:SelectionInfo/def:SelectionInfo/def:Message", objNSM2) ' [def:Code = 'EDL' or def:Code = 'FLM']
                Dim strMessage As String = ""
                For Each Child As XmlNode In objNodeList
                    strMessage += Child.InnerText + "<br />"
                Next
                Select Case objApplication.SelectSingleNode("//def:SubmitApplicationResult/def:Status", objNSM2).InnerText
                    Case "Success", "Match"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Gateway2Finance:<br />" & strMessage))
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("def:Application", objNSM2).InnerText, "NULL")
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Gateway2Finance", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Gateway2Finance", objOutputXMLDoc.InnerXml)
                        saveNote(AppID, Config.DefaultUserID, strMessage)
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "G2FACC", "NULL")
                    Case Else
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Gateway2Finance:<br />" & strMessage))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Gateway2Finance:<br />" & strMessage, objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Gateway2Finance", objOutputXMLDoc.InnerXml)
                        saveNote(AppID, Config.DefaultUserID, strMessage)
                        updateSingleDatabaseField(AppID, "tblapplicationstatus", "LastLenderDecisionCode", "U", "G2FDEC", "NULL")
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Function postG2FWebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 90000
                .ReadWriteTimeout = 90000
                .Headers.Add("SOAPAction", soapaction)
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            '            For Each item In objRequest.Headers
            '                responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            '            Next
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
