﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLaspire
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strUserName As String = "", strPassword As String = "", strBroker As String = "", strCustomerEmailID As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objAuthXMLDoc As XmlDocument = New XmlDocument, objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        If (strEnvironment = "live") Then
        	strXMLURL = "https://brokerfeed.secureloanportal.co.uk/XMLReceive.asmx/receiveXML"
        Else
        	strXMLURL = "https://brokerfeed.secureloanportal.co.uk/TestSite/XMLReceive.asmx/receiveXML"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Aspire%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "AspireUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AspirePassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "AspireBroker") Then strBroker = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        strCustomerEmailID = getAnyField("MediaCampaignOutboundLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/aspire/blank.xml"))

        writeToNode(objInputXMLDoc, "y", "Applications", "broker_id", strMediaCampaignCampaignReference)

        Dim strSQL As String = "SELECT * FROM vwxmlaspire WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
							
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postAspireWebRequest(strXMLURL, "xmlDoc=" & objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//VALID")
                Dim objNodeList As XmlNodeList = objApplication.SelectNodes("//FailedErrorReasons/ErrorReason")
                Dim strMessage As String = ""
                For Each Child As XmlNode In objNodeList
                    strMessage += Child.InnerText + "<br />"
                Next
                objNodeList = objApplication.SelectNodes("//DuplicateRecords/web_app_id")
                Dim strDuplicates As String = ""
                For Each Child As XmlNode In objNodeList
                    strDuplicates += Child.InnerText + "<br />"
                Next
                Select Case checkValue(strMessage)
                    Case False
                        If (checkValue(strDuplicates)) Then
                            HttpContext.Current.Response.Clear()
                            'saveStatus(AppID, Config.DefaultUserID, "INV", "DUP")
                            saveUpdatedDate(AppID, Config.DefaultUserID, "PartnerDuplicate")
                            saveNote(AppID, Config.DefaultUserID, "Application Declined by Aspire: Duplicate")
                            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Aspire: Duplicate"))
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Aspire: Duplicate", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Aspire: Duplicate", objOutputXMLDoc.InnerXml)
                            incrementTransferAttempts(AppID, strMediaCampaignID)
                        Else
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Aspire"))
                            updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("//web_app_id").InnerText, "NULL")
                            setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                            If (checkValue(strCustomerEmailID)) Then
                                sendEmails(AppID, strCustomerEmailID, CompanyID)
                            End If
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Aspire", objInputXMLDoc.InnerXml)
                            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Aspire", objOutputXMLDoc.InnerXml)
                        End If
                    Case Else
                        saveNote(AppID, Config.DefaultUserID, "Application Declined by Aspire: " & strMessage)
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Aspire: " & strMessage))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Aspire: " & strMessage, objInputXMLDoc.InnerXml)
						saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Aspire: " & strMessage, objOutputXMLDoc.InnerXml)
						incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Function postAspireWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "application/x-www-form-urlencoded" '"text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                '.Headers.Add("SOAPAction", "http://tempuri.org/XMLBrokersWebService/XMLReceive/receiveXML")
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'responseWrite("host: " & url & "<br>")
            'responseWrite(post & "<br>")
            'For Each item In objRequest.Headers
                'responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
