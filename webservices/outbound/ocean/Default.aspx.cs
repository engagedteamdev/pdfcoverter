using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using OceanReferences;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];


	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	public void httpPost()
	{

		
		bool thirdPartyConsent = false;
		DateTime App1DOB;

		ServiceModel model = new ServiceModel();

		List<string> valid = new List<string>();


		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		//try
		//{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwocean where AppId = {0}", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{






				DateTime? EmploymentStartDate = null;
				if (!string.IsNullOrEmpty(reader["App1EmploymentStartDate"].ToString()))
				{
					EmploymentStartDate = DateTime.Parse(reader["App1EmploymentStartDate"].ToString());
				}

				decimal PropertyValue = 0;
				decimal.TryParse(reader["PropertyValue"].ToString(), out PropertyValue);

				decimal MortgageBalance = 0;
				decimal.TryParse(reader["MortgageBalance"].ToString(), out MortgageBalance);


				bool IsFormerCouncilProperty = false;
				string CouncilProperty = reader["IsFormerCouncilProperty"].ToString();
				if (CouncilProperty == "True")
				{
					IsFormerCouncilProperty = true;
				}
				else
				{
					IsFormerCouncilProperty = false;
				}

				bool IsPurchasedFromCouncil = false;
				string PurchasedFromCouncil = reader["IsPurchasedFromCouncil"].ToString();
				if (PurchasedFromCouncil == "True")
				{
					IsPurchasedFromCouncil = true;
				}
				else
				{
					IsPurchasedFromCouncil = false;
				}

				bool IsSharedOwnership = false;
				string SharedOwnership = reader["IsSharedOwnership"].ToString();
				if (SharedOwnership == "True")
				{
					IsSharedOwnership = true;
				}
				else
				{
					IsSharedOwnership = false;
				}

				bool IsPurchasedViaGovernment = false;
				string GovernmentPurchase = reader["IsPurchasedViaGovernment"].ToString();
				if (GovernmentPurchase == "True")
				{
					IsPurchasedViaGovernment = true;
				}
				else
				{
					IsPurchasedViaGovernment = false;
				}

				int proposalTerm = 0;
				int.TryParse(reader["ProductTerm"].ToString(), out proposalTerm);

				int customerSalary = 0;
				int.TryParse(reader["App1Salary"].ToString(), out customerSalary);



				decimal monthlyMortgageAmount = 0;
				decimal.TryParse(reader["MonthlyMortgageAmount"].ToString(), out monthlyMortgageAmount);

				decimal homeownerLoanBalance = 0;
				decimal.TryParse(reader["HomeownerLoanBalance"].ToString(), out homeownerLoanBalance);

				decimal homeOwnerLoanMonthlyAmount = 0;
				decimal.TryParse(reader["HomeownerLoanRepaymentAmount"].ToString(), out homeOwnerLoanMonthlyAmount);

				decimal App1Salary = 0;
				decimal.TryParse(reader["App1Salary"].ToString(), out App1Salary);

				decimal Proposal_Amount = 0;
				decimal.TryParse(reader["Amount"].ToString(), out Proposal_Amount);

				decimal MonthlyHomeownerLoanRepaymentAmount = 0;
				decimal.TryParse(reader["MonthlyLoanRepaymentAmount"].ToString(), out MonthlyHomeownerLoanRepaymentAmount);

				decimal MonthlyMortgageRepaymentAmonut = 0;
				decimal.TryParse(reader["MonthlyMortgageAmount"].ToString(), out MonthlyMortgageRepaymentAmonut);


				DateTime addressstartdate = DateTime.Now;
				if (!string.IsNullOrEmpty(reader["AddressStartDate"].ToString()))
				{
					addressstartdate = DateTime.Parse(reader["AddressStartDate"].ToString());
				}






				string ThirdyPartyConsent = reader["ThirdPartyConsent"].ToString();
				if (ThirdyPartyConsent == "True")
				{
					thirdPartyConsent = true;
				}
				else
				{
					thirdPartyConsent = false;
				}

				bool hasHomeownerLoan = false;
				string HasHomeownerLoan = reader["HasHomeownerLoan"].ToString();
				if (HasHomeownerLoan == "True")
				{
					hasHomeownerLoan = true;
				}
				else
				{
					hasHomeownerLoan = false;
				}

				bool consolidatehomeownerLoan = false;
				string ConsolidatehomeownerLoan = reader["ConsolidateHomeownerLoan"].ToString();
				if (ConsolidatehomeownerLoan == "True")
				{
					consolidatehomeownerLoan = true;
				}
				else
				{
					consolidatehomeownerLoan = false;
				}

				model = new ServiceModel()
				{
					App1Customer_Forename = reader["App1Firstname"].ToString(),
					Customer_Address_Country = "United Kingdom",
					Customer_Address_County = reader["AddressCounty"].ToString(),
					Customer_Address_HouseName = reader["AddressHouseName"].ToString(),
					Customer_Address_HouseNumber = reader["AddressHouseNumber"].ToString(),
					MoveInDate = addressstartdate,
					Customer_Address_PostCode = reader["AddressPostcode"].ToString(),
					Customer_Address_Line1 = reader["AddressLine1"].ToString(),
					Customer_Address_Town = reader["AddressTown"].ToString(),

					App1DOB = DateTime.Parse(reader["App1DOB"].ToString()),
					App1Customer_Email = reader["App1EmailAddress"].ToString(),
					EmploymentStartDate = EmploymentStartDate,
					EmploymentStatus = reader["App1EmploymentStatus"].ToString(),

					App1Customer_Salary = App1Salary,
					HasActiveInsolvency = false,
					App1Customer_MartitalStatus = reader["App1MaritalStatus"].ToString(),
					App1Customer_MiddleNames = reader["App1MiddleNames"].ToString(),
					App1Customer_PreferredContactNumber = reader["App1PreferredContactNumber"].ToString(),
					SelfEmployedAnnualNetProfit = reader["App1SelfEmployedAnnualNetProfit"].ToString(),
					SelfEmployedAnyOtherIncome = reader["App1SelfEmployedAnyOtherIncome"].ToString(),
					SelfEmployedBusinessType = reader["App1SelfEmployedBusinessType"].ToString(),
					SelfEmployedOtherIncome = reader["App1SelfEmployedOtherIncome"].ToString(),
					SelfEmployedSalary = reader["App1SelfEmployedSalary"].ToString(),
					App1Customer_Surname = reader["App1Surname"].ToString(),
					App1Customer_Title = reader["App1Title"].ToString(),

					IsMarketingConsented = thirdPartyConsent,
					Proposal_PurposeOtherDescription = reader["PurposeOtherDescription"].ToString(),
					Proposal_Purpose = reader["ProductPurpose"].ToString(),
					Proposal_Term = proposalTerm,
					IsFormerCouncilProperty = IsFormerCouncilProperty,
					IsPurchasedFromCouncil = IsPurchasedFromCouncil,
					IsSharedOwnership = IsSharedOwnership,
					IsPurchasedViaGovernment = IsPurchasedViaGovernment,

					MortgageBalance = MortgageBalance,
					PropertyType = reader["PropertyType"].ToString(),
					Proposal_Amount = Proposal_Amount,
					PropertyValue = PropertyValue,
					App1Customer_EmploymentStatus = reader["App1EmploymentStatus"].ToString(),
					AddressStartDate = addressstartdate,
					ThirdPartyConsent = thirdPartyConsent,
					MortgageMonthlyPayment = monthlyMortgageAmount,
					HasHomeownerLoan = hasHomeownerLoan,
					ConsolidateHomeownerLoan = consolidatehomeownerLoan,
					HomeOwnerLoanBalance = homeownerLoanBalance,
					HomeOwnerLoanMonthlyAmount = homeOwnerLoanMonthlyAmount,
				};

			}
			//connection.Close();

			SecuredBrokingServiceAffiliateClient client = new SecuredBrokingServiceAffiliateClient();
			
			AffiliateSecuredApplication applicationnew = new AffiliateSecuredApplication();

            var checkAddress = client.AddressCheck(4376, "EngagedCrm_Test", "bb-7B8ZB", "IV44 8TZ", "1", "");




		//XmlDocument myXml = new XmlDocument();
		//XPathNavigator xNav = myXml.CreateNavigator();
		//XmlSerializer y = new XmlSerializer(checkAddress.GetType());
		//using (var xs = xNav.AppendChild())
		//{
		//	y.Serialize(xs, checkAddress);
		//}
		//HttpContext.Current.Response.ContentType = "text/xml";
		//HttpContext.Current.Response.Write(myXml.OuterXml);
		//HttpContext.Current.Response.End();





		var MarketingConsent = (new MarketingConsents()
            {
                ConsentMarketingSms = thirdPartyConsent,
                ConsentMarketingEmail = thirdPartyConsent,
                ConsentMarketingNewsletter = thirdPartyConsent,
                ConsentMarketingPhone = thirdPartyConsent,
                ConsentMarketingPost = thirdPartyConsent


            });

            SecuredApplicationAddress[] theaddresses = new SecuredApplicationAddress[1];

            SecuredApplicationAddress anaddress = new SecuredApplicationAddress();

            anaddress.Postcode = "IV44 8TZ";
            anaddress.MovedInDate = model.MoveInDate;
			anaddress.PTCABSCode = checkAddress.Address.FirstOrDefault().PTCABSCode;
			anaddress.StreetName = model.Customer_Address_Line1;
			anaddress.Town = model.Customer_Address_Town;
			theaddresses[0] = anaddress;

            var SecuredApplicationApplicant = (new SecuredApplicationApplicant()
            {
                Title = model.App1Customer_Title,
                Forename = model.App1Customer_Forename,
                Surname = model.App1Customer_Surname,
                DateOfBirth = model.App1DOB,
                PreferredContactNumber = model.App1Customer_PreferredContactNumber,
                EmailAddress = model.App1Customer_Email,
                MaritalStatus = GetMaritalStatus(model.App1Customer_MartitalStatus),
                EmploymentStatus = GetEmploymentStatus(model.EmploymentStatus),
                EmploymentStartDate = model.EmploymentStartDate,
                GrossHouseholdAnnualIncome = model.App1Customer_Salary,
                Addresses = theaddresses

            });

            var PropertyInformation = (new SecuredApplicationPropertyInformation()
			{
				PropertyValue = model.PropertyValue,
				MortgageBalance = model.MortgageBalance,
				MonthlyMortgageRepaymentAmount = model.MonthlyMortgageRepaymentAmonut,
				HasHomeownerLoan = model.HasHomeownerLoan,
				ConsolidateHomeownerLoan = model.ConsolidateHomeownerLoan,
				HomeownerLoanBalance = model.HomeOwnerLoanBalance,
				MonthlyHomeownerLoanRepaymentAmount = model.MonthlyHomeownerLoanRepaymentAmount,
				PropertyType = GetPropertyType(model.PropertyType),
				IsFormerCouncilProperty = model.IsFormerCouncilProperty,
				IsPurchasedFromCouncil = model.IsPurchasedFromCouncil,
				IsSharedOwnership = model.IsSharedOwnership,
				IsPurchasedViaGovernmentScheme = model.IsPurchasedViaGovernment
			});
			
			
            SecuredLoanPurpose[] purpose = new SecuredLoanPurpose[1];
			
            SecuredLoanPurpose apurpose = new SecuredLoanPurpose();
            apurpose = SecuredLoanPurpose.HomeImprovements;
            purpose[0] = apurpose;


            var LoanDetails = (new AffiliateSecuredApplication()
			{
				LoanAmount = model.Proposal_Amount,
				LoanTermMonths = model.Proposal_Term,
				SourceId = 4376,
				UserName = "EngagedCrm_Test",
				Password = "bb-7B8ZB",
				LoanPurposes = purpose,
				MarketingConsents = MarketingConsent,
				Applicant = SecuredApplicationApplicant,
				PropertyInformation = PropertyInformation,
				

			});




		var result = client.ApplyAll(LoanDetails);

		XmlDocument myXml = new XmlDocument();
		XPathNavigator xNav = myXml.CreateNavigator();
		XmlSerializer y = new XmlSerializer(result.GetType());
		using (var xs = xNav.AppendChild())
		{
			y.Serialize(xs, result);
		}
		HttpContext.Current.Response.ContentType = "text/xml";
		HttpContext.Current.Response.Write(myXml.OuterXml);
		HttpContext.Current.Response.End();


		client.Close();


		//}
		//catch (Exception e)
		//{
		//	throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		//}

	}



	class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}

	class ServiceModel
	{
		//Main Information
		public int? CompanyID { get; set; }
		public int? AppID { get; set; }
		public int? Proposal_Product_DefID { get; set; }
		public int? Proposal_Principal { get; set; }
		public int Proposal_Term { get; set; }
		public string Proposal_Purpose { get; set; }
		public string Proposal_PurposeOtherDescription { get; set; }
		public decimal Proposal_Amount { get; set; }
		public decimal MonthlyHomeownerLoanRepaymentAmount { get; set; }
		public decimal MonthlyMortgageRepaymentAmonut { get; set; }


		//First Applicant Details
		public string App1Customer_Title { get; set; }
		public string App1Customer_Forename { get; set; }
		public string App1Customer_MiddleNames { get; set; }
		public string App1Customer_Surname { get; set; }
		public DateTime App1DOB { get; set; }
		public string App1Customer_MartitalStatus { get; set; }
		public string App1Customer_MobileNumber { get; set; }
		public string App1Customer_Email { get; set; }
		public string App1Customer_Occupation { get; set; }
		public string App1Customer_OccupationType { get; set; }
		public string App1Customer_Address_PhoneNumber { get; set; }
		public decimal App1Customer_Salary { get; set; }
		public string App1EmployerName { get; set; }
		public string App1ResidentStatus { get; set; }
		public string App1Customer_EmploymentStatus { get; set; }
		public string App1Customer_PreferredContactNumber { get; set; }
		public bool HasActiveInsolvency { get; set; }
		public string SelfEmployedAnnualNetProfit { get; set; }
		public string SelfEmployedAnyOtherIncome { get; set; }
		public string SelfEmployedBusinessType { get; set; }
		public string SelfEmployedOtherIncome { get; set; }
		public string SelfEmployedSalary { get; set; }



		//App1 Main Address
		public string Customer_Address_HouseName { get; set; }
		public string Customer_Address_HouseNumber { get; set; }
		public string Customer_Address_Line1 { get; set; }
		public string Customer_Address_Line2 { get; set; }
		public string Customer_Address_Town { get; set; }
		public string Customer_Address_County { get; set; }
		public string Customer_Address_Country { get; set; }
		public string Customer_Address_PostCode { get; set; }
		public string Customer_Address_Tenure { get; set; }
		public int? AddressYears { get; set; }
		public int? AddressMonths { get; set; }
		public DateTime? AddressStartDate { get; set; }
		public string PropertyType { get; set; }
		public DateTime MoveInDate { get; set; }
		public DateTime? MoveOutDate { get; set; }

		//App1 Employer Address
		public string Employer_Address_Line1 { get; set; }
		public string Employer_Address_Line2 { get; set; }
		public string Employer_Address_Town { get; set; }
		public string Employer_Address_County { get; set; }
		public string Employer_Address_PostCode { get; set; }
		public int? EmployerAddressYears { get; set; }
		public int? EmployerAddressMonths { get; set; }
		public DateTime? EmploymentStartDate { get; set; }
		public string EmploymentStatus { get; set; }

		//Other Details        
		public decimal? MortgageOutstanding { get; set; }
		public decimal MortgageBalance { get; set; }
		public decimal PropertyValue { get; set; }
		public DateTime? PropertyPurchaseDate { get; set; }
		public decimal? MortgageMonthlyPayment { get; set; }
		public bool CorrespondenceAddressSame { get; set; }
		public bool ThirdPartyConsent { get; set; }
		public bool HasHomeownerLoan { get; set; }
		public bool ConsolidateHomeownerLoan { get; set; }
		public decimal HomeOwnerLoanBalance { get; set; }
		public decimal HomeOwnerLoanMonthlyAmount { get; set; }
		public bool IsFormerCouncilProperty { get; set; }
		public bool IsPurchasedFromCouncil { get; set; }
		public bool IsSharedOwnership { get; set; }
		public bool IsPurchasedViaGovernment { get; set; }
		public string PTCABSCode { get; set; }
		public bool IsMarketingConsented { get; set; }

	}



 
    private ClientMaritalStatus GetMaritalStatus(string MaritalStatus)
	{
		switch (MaritalStatus)
		{
			case "Other":
				return ClientMaritalStatus.Unspecified;
			case "Divorced":
				return ClientMaritalStatus.Divorced;
			case "Common Law":
				return ClientMaritalStatus.Unspecified;
			case "Separated":
				return ClientMaritalStatus.Separated;
			case "Single":
				return ClientMaritalStatus.Single;
			case "Married":
				return ClientMaritalStatus.Married;
			default:
				return ClientMaritalStatus.Unspecified;


		}
	}

	private ClientEmploymentType GetEmploymentStatus(string EmploymentStatus)
	{
		switch (EmploymentStatus)
		{
			case "FullTimeEmployment":
				return ClientEmploymentType.Employed;
			case "SelfEmployed":
				return ClientEmploymentType.SelfEmployed;
			case "Student":
				return ClientEmploymentType.Student;
			case "HousePerson":
				return ClientEmploymentType.Houseperson;
			case "Retired":
				return ClientEmploymentType.Retired;
			case "TemporaryEmployment":
				return ClientEmploymentType.Employed;
			case "Unemployed":
				return ClientEmploymentType.Unemployed;
			default:
				return ClientEmploymentType.Employed;


		}
	}

	private PropertyType GetPropertyType(string strPropertyType)
	{
		switch (strPropertyType)
		{
			case "Bungalow":
				return PropertyType.Bungalow;
			case "Terraced":
				return PropertyType.Terraced;
			case "SemiDetached":
				return PropertyType.SemiDetached;
			case "Detached":
				return PropertyType.Detached;
			case "Flat":
				return PropertyType.Flat;
			case "Other":
				return PropertyType.Other;
			default:
				return PropertyType.Other;


		}
	}


}




//private static byte getContactType(string type)
//{

//    byte TypeID = 0;

//    if (type == "Home Number")
//    {
//        TypeID = 1;
//    }
//    else if (type == "Mobile Number")
//    {
//        TypeID = 16;
//    }
//    else if (type == "Work Number")
//    {
//        TypeID = 2;
//    }
//    else if (type == "Email Address")
//    {
//        TypeID = 5;
//    }
//    else if (type == "Not Known")
//    {
//        TypeID = 3;
//    }

//    else if (type == "New Home Number")
//    {
//        TypeID = 4;
//    }
//    else if (type == "Fax")
//    {
//        TypeID = 14;
//    }
//    else if (type == "Twitter")
//    {
//        TypeID = 17;
//    }
//    else if (type == "Facebook")
//    {
//        TypeID = 18;
//    }
//    else if (type == "Google+")
//    {
//        TypeID = 19;
//    }
//    else if (type == "LinkedIn")
//    {
//        TypeID = 20;
//    }
//    else if (type == "Other")
//    {
//        TypeID = 15;
//    }

//    return TypeID;

//}