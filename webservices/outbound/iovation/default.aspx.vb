﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.antest.brokertoolbox

' ** Revision history **
'
' 18/06/2013 - File created - BSE
'
' ** End Revision History **

Partial Class XMLIovation
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strXMLURL As String = "", intUserID As String = "", strAccountCode As String = "", strPassword As String = ""
    Private strErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub xml()
        If (strEnvironment = "live") Then
            strXMLURL = "https://soap.iovation.com/api/CheckTransactionDetails"
        Else
            strXMLURL = "https://ci-snare.iovation.com/api/CheckTransactionDetails"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Iovation%' AND CompanyID = '" & Config.CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "IovationUserID") Then intUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "IovationAccountCode") Then strAccountCode = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "IovationPassword") Then strPassword = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            If (Not checkDateField(AppID, "FraudChecked")) Then
                generateXML()
            Else
                SOAPSuccessfulMessage("Iovation Check Skipped", 1, AppID, "<Validation><Status>1</Status><Reason>Iovation Check Skipped</Reason></Validation>", "")
            End If
        End If
    End Sub

    Private Sub generateXML()

        Dim strXML As String = ""
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/iovation/blank.xml"))

        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("ns1", "http://www.iesnare.com/dra/api/CheckTransactionDetails")

        writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails", "ns1:subscriberid", intUserID)
        writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails", "ns1:subscriberaccount", strAccountCode)
        writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails", "ns1:subscriberpasscode", strPassword)

        Dim strSQL As String = "SELECT * FROM vwxmliovation WHERE AppID = " & AppID & " AND CompanyID = '" & Config.CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim intNumItems As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        If (Column.ColumnName = "UPC") Then
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[ns1:name/text() = 'UPC']", "ns1:value", "0" & Row(Column).ToString(), "ns1")
                        ElseIf (Column.ColumnName = "App1EmailAddress") Then
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[ns1:name/text() = 'Email']", "ns1:value", Row(Column).ToString(), "ns1")
                        ElseIf (Column.ColumnName = "Amount") Then 
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[ns1:name/text() = 'ValueAmount']", "ns1:value", Row(Column).ToString(), "ns1")
                        ElseIf (Column.ColumnName = "App1HomeTelephone") Then
                            Dim strTelephone As String = "+44" & Right(Row(Column).ToString, Len(Row(Column).ToString()) - 1)
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "HomePhoneNumber", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", strTelephone, "ns1")
                        ElseIf (Column.ColumnName = "App1MobileTelephone") Then
                            Dim strTelephone As String = "+44" & Right(Row(Column).ToString, Len(Row(Column).ToString()) - 1)
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "MobilePhoneNumber", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", strTelephone, "ns1")
                        ElseIf (Column.ColumnName = "App1WorkTelephone") Then
                            Dim strTelephone As String = "+44" & Right(Row(Column).ToString, Len(Row(Column).ToString()) - 1)
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "OfficePhoneNumber", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", strTelephone, "ns1")
                        ElseIf (Column.ColumnName = "FullAddressLine1") Then
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "BillingStreet", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", Row(Column).ToString, "ns1")
                        ElseIf (Column.ColumnName = "AddressPostCode") Then
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "BillingPostalCode", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", Row(Column).ToString, "ns1")
                        ElseIf (Column.ColumnName = "AddressCountry") Then
                            Dim x As Integer = objInputXMLDoc.SelectNodes("//ns1:property", objNSM).Count
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties", "ns1:property[" & x + 1 & "]", "ns1:property", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "ns1:name", "ns1")
                            createElement(objInputXMLDoc, "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", "ns1:value", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:name", "BillingCountry", "ns1")
                            writeToNode(objInputXMLDoc, "y", "ns1:CheckTransactionDetails/ns1:txn_properties/ns1:property[" & x + 1 & "]", "ns1:value", Row(Column).ToString, "ns1")
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(strErrorMessage)
            HttpContext.Current.Response.End()
        Else
            ' Post the SOAP message.

            Dim objResponse As HttpWebResponse = postIovationWebRequest(strXMLURL, objInputXMLDoc.InnerXml, "")
            If (checkResponse(objResponse, "")) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
                objReader.Close()
                objReader = Nothing

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM2.AddNamespace("namesp1", "http://www.iesnare.com/dra/api/CheckTransactionDetails")

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//namesp1:CheckTransactionDetailsResponse", objNSM2)
                If (Not objApplication Is Nothing) Then
                    Dim strMessage As String = objOutputXMLDoc.SelectSingleNode("//namesp1:reason", objNSM2).InnerText
                    Dim strStatus As String = objOutputXMLDoc.SelectSingleNode("//namesp1:result", objNSM2).InnerText

                    Select Case strStatus
                        Case "A"
                            saveStatus(AppID, Config.DefaultUserID, "CHK", "PAS")
                        Case "R"
                            saveStatus(AppID, Config.DefaultUserID, "CHK", "REF")
                            saveNote(AppID, Config.DefaultUserID, strMessage)
                            saveUpdatedDate(AppID, Config.DefaultUserID, "FraudReferred")
                        Case "D"
                            saveStatus(AppID, Config.DefaultUserID, "CHK", "FAI")
                            saveNote(AppID, Config.DefaultUserID, strMessage)
                            saveUpdatedDate(AppID, Config.DefaultUserID, "FraudDeclined")
                    End Select
                    saveUpdatedDate(AppID, Config.DefaultUserID, "FraudChecked")

                    updateDataStoreField(AppID, "IovationStatus", strStatus, "", "")
                    updateDataStoreField(AppID, "IovationReason", strMessage, "", "")
                    Dim strResults As String = "<Validation>" & _
                        "<Status>" & strStatus & "</Status>" & _
                        "<Reason>" & strMessage & "</Reason>" & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.isp", "IPAddressISP", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.loc.country", "IPAddressCountry", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.loc.countrycode", "IPAddressCountryCode", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.loc.lat", "IPAddressLatitude", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.loc.lng", "IPAddressLongitude", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ipaddress.org", "IPAddressOriginator", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress", "RealIPAddress", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.isp", "RealIPAddressISP", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.loc.country", "RealIPAddressCountry", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.loc.countrycode", "RealIPAddressCountryCode", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.loc.lat", "RealIPAddressLatitude", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.loc.lng", "RealIPAddressLongitude", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "realipaddress.org", "RealIPAddressOriginator", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "ruleset.score", "RulesetScore", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.alias", "DeviceAlias", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.browser.lang", "DeviceLanguage", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.browser.type", "BrowserType", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.browser.version", "BrowserVersion", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.browser.lang", "DeviceLanguage", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.os", "DeviceOS", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.screen", "DeviceScreen", objNSM2) & _
                        getXMLValue(objOutputXMLDoc, "device.type", "DeviceType", objNSM2) & _
                        "</Validation>"

                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Iovation Check Successfully Generated", objInputXMLDoc.InnerXml)
                    SOAPSuccessfulMessage("Iovation Check Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                Else
                    Dim strMessage As String = objOutputXMLDoc.SelectSingleNode("//faultstring").InnerText
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Iovation Check Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                    SOAPSuccessfulMessage("Iovation Check Unsuccessfully Retrieved", -5, AppID, "<Validation><Status>0</Status><Reason>" & strMessage & "</Reason></Validation>", objOutputXMLDoc.InnerXml)
                End If
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|Data error")
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Function getXMLValue(ByVal doc As XmlDocument, ByVal search As String, ByVal fld As String, ByVal nsm As XmlNamespaceManager) As String
        Dim objStringWriter As New StringWriter
        With objStringWriter
            Dim strValue = doc.SelectSingleNode("//namesp1:detail[namesp1:name/text() = '" & search & "']/namesp1:value", nsm).InnerText()
            .WriteLine("<" & fld & ">" & strValue & "</" & fld & ">")
            updateDataStoreField(AppID, fld, strValue, "", "")
        End With
        Return objStringWriter.ToString
    End Function


    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & fld, objNSM)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    If (InStr(parent, "esreq:payload") > 0 Or InStr(parent, "message") > 0) Then
                        Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
                        Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), objNSM.LookupNamespace(ns))
                        Dim objNewText As XmlText = doc.CreateTextNode(val)
                        objNewNode.AppendChild(objNewText)
                        objApplication.AppendChild(objNewNode)
                    End If
                End If
            End If
        End If
    End Sub

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String, Optional ns As String = "def") As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name, objNSM)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, objNSM)
            Dim objNewNode As XmlElement = doc.CreateElement(fld, objNSM.LookupNamespace(ns))
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Function postIovationWebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                '.Headers.Add("SOAPAction", soapaction)
            End With
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            'responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()			
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As System.Net.WebException
                If (err.Status = WebExceptionStatus.Timeout) Then
                    reportErrorMessage(err)
                    Return Nothing
                Else
                    Return Nothing
                End If
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
