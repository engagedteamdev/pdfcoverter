﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.Configuration;


public partial class Demo : System.Web.UI.Page
{

    const string IntegrationId = "Arc";
    const string iPipelineUrl = "https://solutionbuilderservices.ipipeline.uk.com/MessageReceiver/Receiver";

	string AppID = HttpContext.Current.Request["AppID"];
	string ModelID = HttpContext.Current.Request["ModelID"];
	string InternalClientID = HttpContext.Current.Request["InternalClientID"];
	string QuoteID = HttpContext.Current.Request["QuoteID"];
	string ComparisonId = HttpContext.Current.Request["ComparisonId"];
	string UserID = HttpContext.Current.Request["UserID"];

	string App1Title = "", App1FirstName = "", App1Surname = "", App1Sex = "", App1DOB = "", App1EmploymentStatus = "", App1Smoker = "";
	string App2Title = "", App2FirstName = "", App2Surname = "", App2Sex = "", App2DOB = "", App2EmploymentStatus = "", App2Smoker = "";
	string CoverFor = "", ProductTerm = "", PolicyCoverType = "", TimeStamp = "", PolicyTPD = "";
	decimal PolicyInterestRate = 0, Amount = 0, App1AnnualIncome = 0, App2AnnualIncome = 0;

	string strErrors = "Missing Required Data ";

	protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Response.Clear();
            string tpiMessage = GetMessage();
            // this doesnt work as the page ends up with relative paths
            //var sPostDataString = String.Format("{0}={1}", IntegrationId, HttpUtility.UrlEncode(tpiMessage));
            //Redirect(sPostDataString, url);

            var htmlPage = TpiPostPage(tpiMessage); // dont encode - page does it
           ClientSideRedirect(htmlPage);



		}
    }


    protected string TpiPostPage(string message)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append (
            @"<html><body onload=""submitForm()"">
                <FORM name=""Form1"" method=post action=""{0}"" >
                    <TEXTAREA  ROWS=""30"" COLS=""80"" name=""{1}"" style=""display:none;"">
                        {2}
                    </TEXTAREA>
                </FORM ></body>
                <SCRIPT LANGUAGE=JavaScript>
                function submitForm()
                {
                    document.forms['Form1'].submit()
                }
                </SCRIPT>

            </html>
            ");
        sb.Replace("{0}", iPipelineUrl);
        sb.Replace("{1}", IntegrationId);
        sb.Replace("{2}", message);
        string msg = sb.ToString();
        return msg;
    }

	protected string GetMessage()
	{

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("SELECT * FROM vwipipeline where AppID = {0} ", AppID), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			
			TimeStamp = reader["TimeStamp"].ToString();
	
		}

		string StoredUsername = "";
		string Username = "";
		string Password = "";

		SqlConnection connectionNew = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
		connectionNew.Open();

		SqlCommand myCommandUser = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ipipeline%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connectionNew);

		var readerUser = myCommandUser.ExecuteReader();

		while (readerUser.Read())
		{




			StoredUsername = readerUser["StoredUserName"].ToString();

			if (StoredUsername == "ipipelineuserid")
			{

				Username = readerUser["StoredUserValue"].ToString();

			}

			if (StoredUsername == "ipipelinepassword")
			{
				Password = readerUser["StoredUserValue"].ToString();
			}

		}

		string message = "";
		message =
				string.Format(@"<tpi><authentication>
                    <portal_id>Arc</portal_id>
                    <service_id>2</service_id>
                    <control_timestamp>{0}</control_timestamp>
                </authentication>
                <identification>
                    <user_id>{3}</user_id>
                    <certificate>
                        <user_password>{4}</user_password>
                    </certificate>
                </identification>
                  <application_data>
						<applydetails>
						   <modelid>{1}</modelid>
						   <quoteid>{2}</quoteid>
					  </applydetails>
                     </application_data>
                   </tpi>", TimeStamp, ModelID, QuoteID, Username, Password);

		
//		XmlDocument myXml = new XmlDocument();
//		XPathNavigator xNav = myXml.CreateNavigator();
//		XmlSerializer x = new XmlSerializer(message.GetType());
//		using (var xs = xNav.AppendChild())
//		{
//			x.Serialize(xs, message);
//		}
//		HttpContext.Current.Response.ContentType = "text/xml";
//		HttpContext.Current.Response.Write(myXml.OuterXml);
//		HttpContext.Current.Response.End();

		try
		{
			saveXMLReceived("80.244.190.16", AppID, 1, "Data Sent to IpipeLine", message.ToString());
			updateipipelinetable();
		}
		catch (Exception e)
		{
			HttpContext.Current.Response.Write(e.Message);
		
		}

		return message;
	

	}


		

    


    private void ClientSideRedirect(string sPostDataString)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.Write(sPostDataString);
        Response.Flush();
        Response.End();
    }

    protected void Redirect(string message)
    {
        ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallback;

        WebRequest wrRequest = WebRequest.Create(iPipelineUrl);
        wrRequest.Method = "POST";
        byte[] byteArray = Encoding.UTF8.GetBytes(message);
        wrRequest.ContentLength = byteArray.Length;
        wrRequest.ContentType = "application/x-www-form-urlencoded";

        using (Stream writedataStream = wrRequest.GetRequestStream())
        {
            writedataStream.Write(byteArray, 0, byteArray.Length);
            writedataStream.Close();
        }
        using (var wrResponse = wrRequest.GetResponse())
        {
            using (var readDataStream = wrResponse.GetResponseStream())
            {
                if (readDataStream != null)
                {
                    var srReader = new StreamReader(readDataStream);
                    string sResponseFromServer = srReader.ReadToEnd();
                    Response.Buffer = true;
                    Response.Write(sResponseFromServer);
                    srReader.Close();
                    readDataStream.Close();
                    Response.Flush();
                }
            }
            wrResponse.Close();
        }
    }

    /// <summary>
    /// Deal with cert errors in testing
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="certificate"></param>
    /// <param name="chain"></param>
    /// <param name="sslPolicyErrors"></param>
    /// <returns></returns>
    public bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true;
    }

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());
		connection.Open();
		SqlCommand updatecmd2 = new SqlCommand();
		updatecmd2.Connection = connection;
		updatecmd2.CommandText = string.Format("INSERT INTO tblxmlreceived(XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES('" + ip + "', '" + AppID + "', 1, '" + msg + "', '" + strPost + "', getdate()) ");
		updatecmd2.ExecuteNonQuery();
		connection.Close();
	}


	private void updateipipelinetable()
	{
		

		try
		{
			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception exp)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", exp.Message));
			}


			SqlCommand insertproductInfo = new SqlCommand();
			insertproductInfo.Connection = connection;
			insertproductInfo.CommandText = string.Format("insert into tblipipelineids (AppID,ModelID,InternalClientID,QuoteID,ComparisonID) values('{0}','{1}','{2}','{3}','{4}')", AppID, ModelID, InternalClientID, QuoteID, ComparisonId);
			insertproductInfo.ExecuteNonQuery();

			
			connection.Close();
		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write(ex.Message);
			HttpContext.Current.Response.End();
		}
	}


}