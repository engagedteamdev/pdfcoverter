﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolutionBuilderIntegration;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Demo : System.Web.UI.Page
	{

	public string tablerow = "";

	public string CurrentAppID = HttpContext.Current.Request["AppID"];
	public string UserID = HttpContext.Current.Request["UserID"];

	protected void Page_Load(object sender, EventArgs e)
		{
			httpPost();
			


		}

	//This method checks for any events that are present, in your code you will want to also check against an External Client Reference otherwise
	//it could bring back too much data.
	public void httpPost()
		{
		try
		{


			string StoredUsername = "";
			string Username = "";
			string Password = "";

			SqlConnection connectionNew = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
			connectionNew.Open();

			SqlCommand myCommandUser = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ipipeline%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connectionNew);

			var readerUser = myCommandUser.ExecuteReader();

			while (readerUser.Read())
			{




				StoredUsername = readerUser["StoredUserName"].ToString();

				if (StoredUsername == "ipipelineuserid")
				{

					Username = readerUser["StoredUserValue"].ToString();

				}

				if (StoredUsername == "ipipelinepassword")
				{
					Password = readerUser["StoredUserValue"].ToString();
				}

			}

			var client = new GetClientDataClient();
			client.ClientCredentials.UserName.UserName = Username;
			client.ClientCredentials.UserName.Password = Password;

			var req = new GetEventNotificationsRequestMessage();
			req.Header = new Header1() { ApplicationName = "Arc" };

			req.GetEventNotificationsRequest = new GetEventNotificationsRequest();

			req.GetEventNotificationsRequest.EarliestNotificationDate = DateTime.Now.AddDays(-30);

			var resp = client.GetEventNotifications(req);
			int Notifications = resp.GetEventNotificationsResponse.Notifications.Count();


			if (Notifications > 0)
			{
				var iCount = Notifications;

				for (int i = 0; i <= iCount - 1; i++)
				{
					string modelID = resp.GetEventNotificationsResponse.Notifications[i].Event.Key.ToString();
					string internalClientID = resp.GetEventNotificationsResponse.Notifications[i].Identification.Client.InternalClientReference.ToString();
					string AppID = resp.GetEventNotificationsResponse.Notifications[i].Identification.Client.ExternalClientReference.ToString();
					if (AppID == CurrentAppID)
					{
						getClientData(modelID, internalClientID, CurrentAppID);
					}
					else
					{
						HttpContext.Current.Response.Write("No Data Found");
						HttpContext.Current.Response.Flush();
						HttpContext.Current.Response.SuppressContent = true;
						HttpContext.Current.ApplicationInstance.CompleteRequest();
						HttpContext.Current.Response.End();
					}
				}

			}
			else
			{
				HttpContext.Current.Response.Write("No Data Found");
				HttpContext.Current.Response.End();
			}



		}
		catch (Exception ex)
		{
				HttpContext.Current.Response.Write(ex.Message);
				HttpContext.Current.Response.End();

		}
		}


	public void getClientData(string ModelID, string ClientID, string AppID)
	{
		try
		{

			string StoredUsername = "";
			string Username = "";
			string Password = "";

			SqlConnection connectionNew = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
			connectionNew.Open();

			SqlCommand myCommandUser = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ipipeline%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connectionNew);

			var readerUser = myCommandUser.ExecuteReader();

			while (readerUser.Read())
			{




				StoredUsername = readerUser["StoredUserName"].ToString();

				if (StoredUsername == "ipipelineuserid")
				{

					Username = readerUser["StoredUserValue"].ToString();

				}

				if (StoredUsername == "ipipelinepassword")
				{
					Password = readerUser["StoredUserValue"].ToString();
				}

			}

			var client = new GetClientDataClient();
			client.ClientCredentials.UserName.UserName = Username;
			client.ClientCredentials.UserName.Password = Password;
			var req = new GetLikesAndAppliesRequestMessage();
			req.GetLikesAndAppliesRequest = new GetLikesAndAppliesRequest();


			if (!String.IsNullOrEmpty(ModelID))
			{
				req.GetLikesAndAppliesRequest.ModelId = int.Parse(ModelID);
			}
			else
			{
				req.GetLikesAndAppliesRequest.EarliestModelDate = DateTime.Now.AddDays(-30);
			}
			req.GetLikesAndAppliesRequest.InternalClientReference = int.Parse(ClientID);
			req.GetLikesAndAppliesRequest.ExternalClientReference = AppID;
			var resp = client.RetrieveGetLikesAndApplys(req);

			int Models = resp.Models.Count();

			try
			{
				saveXMLReceived("80.244.190.16", AppID, 1, "Quotes Recieved from IpipeLine", resp.ToString());
			}
			catch (Exception e)
			{
				HttpContext.Current.Response.Write(e.Message);

			}

			//			XmlDocument myXml = new XmlDocument();
			//			XPathNavigator xNav = myXml.CreateNavigator();
			//			XmlSerializer x = new XmlSerializer(resp.GetType());
			//			using (var xs = xNav.AppendChild())
			//			{
			//				x.Serialize(xs, resp);
			//			}
			//			HttpContext.Current.Response.ContentType = "text/xml";
			//			HttpContext.Current.Response.Write(myXml.OuterXml);
			//			HttpContext.Current.Response.Flush();
			//			HttpContext.Current.Response.SuppressContent = true;
			//			HttpContext.Current.ApplicationInstance.CompleteRequest();
			//			HttpContext.Current.Response.End();


			if (Models > 0)
			{
				var iCount = Models;

				for (int i = 0; i <= iCount - 1; i++)
				{
					string LikesModelID = resp.Models[i].ModelId.ToString();
					string QuoteID = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].QuoteId.ToString();
					string CommissionAmount = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].CommissionAmount.ToString();
					string ExpiryDate = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].ExpiryDate.ToString();
					string Premium = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].Premium.ToString();
					string ProductName = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].ProductName.ToString();
					string ProviderName = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].ProviderName.ToString();
					string QuoteDate = resp.Models[i].Likes[i].Comparisons[i].Quotes[i].QuoteDate.ToString();
					string ComparisonId = resp.Models[i].Likes[i].Comparisons[i].ComparisonId.ToString();


					string button = "<a class=\"btn btn-success btn-medium ui-tooltip\" onclick=\"[window.open('/webservices/outbound/ipipeline/apply.aspx?InternalClientID=" + ClientID + "&QuoteID=" + QuoteID + "&ModelID=" + LikesModelID + "&AppID=" + AppID + "&ComparisonId=" + ComparisonId + "&UserID=" + UserID + "', 'Quotes', 'height=800px,width=1000px,scrollbars=1', 'true')]\">Apply</a> <a class=\"btn btn-success btn-medium ui-tooltip\" href=\"https://services.engagedcrm.co.uk/webservices/outbound/ipipeline/getdocuments.aspx?QuoteType=ComparisonReport&ComparisonID=" + ComparisonId + "&AppID=" + CurrentAppID + "&UserID=" + UserID + "\">CR <i class=\"far fa-file-alt\"></i></a> <a class=\"btn btn-success btn-medium ui-tooltip\" href=\"https://services.engagedcrm.co.uk/webservices/outbound/ipipeline/getdocuments.aspx?AppID=" + CurrentAppID + "&QuoteID=" + QuoteID + "&UserID=" + UserID + "\">Illustration <i class=\"far fa-file-alt\"></i></a>";
					tablerow = string.Format("<tr><td>{0}</td><td>{1}</td><td>£{2}</td><td>£{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>",ProviderName, ProductName, Premium, CommissionAmount, QuoteDate, ExpiryDate, button);
					
}

			}
			else
			{
				HttpContext.Current.Response.Write("No Data Found");
				HttpContext.Current.Response.End();
			}


			//XmlDocument myXml = new XmlDocument();
			//XPathNavigator xNav = myXml.CreateNavigator();
			//XmlSerializer x = new XmlSerializer(resp.GetType());
			//using (var xs = xNav.AppendChild())
			//{
			//	x.Serialize(xs, resp);
			//}
			//HttpContext.Current.Response.ContentType = "text/xml";
			//HttpContext.Current.Response.Write(myXml.OuterXml);
			//HttpContext.Current.Response.Flush();
			//HttpContext.Current.Response.SuppressContent = true;
			//HttpContext.Current.ApplicationInstance.CompleteRequest();
			//HttpContext.Current.Response.End();

			

		}
		catch (Exception ex)
		{

			HttpContext.Current.Response.Write(ex.Message);
		
		}


	}


	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());
		connection.Open();
		SqlCommand updatecmd2 = new SqlCommand();
		updatecmd2.Connection = connection;
		updatecmd2.CommandText = string.Format("INSERT INTO tblxmlreceived(XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES('" + ip + "', '" + soapid + "', 1, '" + msg + "', '" + strPost + "', getdate()) ");
		updatecmd2.ExecuteNonQuery();
		connection.Close();
	}
}
