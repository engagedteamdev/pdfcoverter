﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="getevents.aspx.cs" Inherits="Demo" ValidateRequest="false" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- Styles -->
    <link href="https://arcbeta.engaged-solutions.co.uk/css/bootstrap.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/css/bootstrap-overrides.css" rel="stylesheet">

    <link href="https://arcbeta.engaged-solutions.co.uk/css/ui-lightness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/js/plugins/msgGrowl/css/msgGrowl.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/js/plugins/msgAlert/css/msgAlert.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet">

    <link href="https://arcbeta.engaged-solutions.co.uk/css/slate.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/css/slate-responsive.css" rel="stylesheet">

    <link href="https://arcbeta.engaged-solutions.co.uk/css/pages/dashboard.css" rel="stylesheet">
    <link href="https://arcbeta.engaged-solutions.co.uk/css/pages/reports.css" rel="stylesheet">

    <!-- Javascript -->
    <script src="https://arcbeta.engaged-solutions.co.uk/js/jquery-1.7.2.min.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/jquery.ui.touch-punch.min.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/bootstrap.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/plugins/msgGrowl/js/msgGrowl.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/plugins/msgAlert/js/msgAlert.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/plugins/lightbox/jquery.lightbox.js"></script>


    <script src="https://arcbeta.engaged-solutions.co.uk/js/Slate.js"></script>

    <script src="https://arcbeta.engaged-solutions.co.uk/js/general.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/crm.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/reminders.aspx"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/messages.aspx"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/ticker.aspx"></script>

    <script src="https://arcbeta.engaged-solutions.co.uk/js/applicationform.js"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/processing.aspx"></script>
    <script src="https://arcbeta.engaged-solutions.co.uk/js/bridgingform.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>

<body class="no-bg">

	<table border="0" class="table sortable table-bordered table-striped table-highlight">
    <thead>
        <tr class="tablehead" style="background:#003F5E;">
        	<th class="smlc sort-alpha">Provider Name</th>
            <th class="smlc sort-alpha">Product Name</th>
            <th class="smlc sort-alpha">Premium</th>
            <th class="smlc sort-alpha">Commission Amount</th>
            <th class="smlc sort-alpha">Quote Date</th>
			<th class="smlc sort-alpha">Expiry Date</th>
			<th class="smlc sort-alpha" style="width:230px;"></th>
        </tr>
    </thead>
            <%=tablerow%>
    </table>


</body>

</html>