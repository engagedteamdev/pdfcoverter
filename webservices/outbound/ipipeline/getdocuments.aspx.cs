﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReAPTermComparisonService;
using DocumentationServiceNew;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public partial class Demo : System.Web.UI.Page
	{

	public string tablerow = "";
	string QuoteType = HttpContext.Current.Request["QuoteType"];
	public string AppID = HttpContext.Current.Request["AppID"];
	public string ComparisonID = HttpContext.Current.Request["ComparisonID"];
	public string QuoteID = HttpContext.Current.Request["QuoteID"];
	public string UserID = HttpContext.Current.Request["UserID"];

	protected void Page_Load(object sender, EventArgs e)
		{
			httpPost();
		}

	//This method checks for any events that are present, in your code you will want to also check against an External Client Reference otherwise
	//it could bring back too much data.
	public void httpPost()
		{


		if (QuoteType == "ComparisonReport")
		{

			getComparisonReport();

		}
		else
		{
			getIllustrationDocument();
		}

	}



	public void getComparisonReport()
	{

		ReAPTermComparisonService.EnhancedProtectionComparisonServiceClient client = null;

		client = new ReAPTermComparisonService.EnhancedProtectionComparisonServiceClient("EnhancedProtectionComparisonWSHttp_Cert");


		string StoredUsername = "";
		string Username = "";
		string Password = "";

		SqlConnection connectionNew = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
		connectionNew.Open();

		SqlCommand myCommandUser = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ipipeline%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connectionNew);

		var readerUser = myCommandUser.ExecuteReader();

		while (readerUser.Read())
		{

			


			StoredUsername = readerUser["StoredUserName"].ToString();

			if (StoredUsername == "ipipelineuserid")
			{

				Username = readerUser["StoredUserValue"].ToString();

			}

			if (StoredUsername == "ipipelinepassword")
			{
				Password = readerUser["StoredUserValue"].ToString();
			}

		}

		
		client = new ReAPTermComparisonService.EnhancedProtectionComparisonServiceClient("EnhancedProtectionComparisonWSHttp_UNP");
		client.ClientCredentials.UserName.UserName = Username;
		client.ClientCredentials.UserName.Password = Password;

		connectionNew.Close();


		try
		{
			
			ReAPTermComparisonService.RetrieveEnhancedProtectionComparisonQuoteReportRequestMessage req = new ReAPTermComparisonService.RetrieveEnhancedProtectionComparisonQuoteReportRequestMessage();
			req.Header = new ReAPTermComparisonService.Header();
			req.Header.Application = "Arc";
			Guid ComparisonId = new Guid(ComparisonID);
			req.ComparisonQuoteReportRequest = new ReAPTermComparisonService.ComparisonQuoteReportRequest();
			req.ComparisonQuoteReportRequest.ComparisonId = ComparisonId;
			req.ComparisonQuoteReportRequest.ReportType = ReAPTermComparisonService.EnhancedProtectionComparisonReportType.SummaryWithCommission;

			XmlSerializer Serializer = new XmlSerializer(req.GetType());

			TextWriter sw = new StringWriter();

			Serializer.Serialize(sw, req);
			sw.Close();
			string xx = sw.ToString().Replace("utf-16", "utf-8");


			// now make the call
			ReAPTermComparisonService.RetrieveComparisonQuoteReportResponseMessage resp = null;
			resp = client.RetrieveComparisonQuoteReport(req);
			try
			{
				SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
				connection.Open();

				DateTime.Now.Year.ToString();
				SqlCommand insertESIS = new SqlCommand();
				insertESIS.Connection = connection;

				var cal = new CultureInfo("tr-TR", false).Calendar;

				string woy2 = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, System.DayOfWeek.Monday).ToString();
				int number = Convert.ToInt32(woy2);
				//number += 1;
				string newWOY = number.ToString();

				string filedate = (DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString());
				insertESIS.CommandText = string.Format("insert into tblPDFFiles(AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate, PDFHistoryFileName) values ('" + AppID + "', 'Comparison Document', 'application/pdf', @Data, '', '', '" + DateTime.Now + "','/arc-protect/" + newWOY + "-" + DateTime.Now.Year.ToString() + "/" + filedate + ".pdf')");
				insertESIS.Parameters.Add("@Data", SqlDbType.VarBinary).Value = resp.ComparisonQuoteReport.Report;
				insertESIS.ExecuteNonQuery();
				databaseFileRead(AppID, "E:\\data\\attachments\\arc-protect\\" + newWOY + "-" + DateTime.Now.Year.ToString() + "\\" + filedate + ".pdf");
				connection.Close();
				
				HttpContext.Current.Response.Write("Comparison Report Downloaded please see document history");
				HttpContext.Current.Response.End();
				HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + "ComparisonReport.pdf");
				HttpContext.Current.Response.ContentType = "application/pdf";
				HttpContext.Current.Response.BinaryWrite(resp.ComparisonQuoteReport.Report);
				HttpContext.Current.Response.End();
				HttpContext.Current.Response.Redirect("", false);

			}
			catch
			{

			}


			Serializer = new XmlSerializer(resp.GetType());
			sw = new StringWriter();
			Serializer.Serialize(sw, resp);
			sw.Close();
			
		}
		catch (Exception ex)
		{

			HttpContext.Current.Response.Write(ex.ToString());
			HttpContext.Current.Response.End();
		
		}
		finally
		{
			client.Close();
		}
	}


	public void getIllustrationDocument()
	{


		DocumentationServiceNew.DocumentationServiceClient client = null;

		client = new DocumentationServiceNew.DocumentationServiceClient("DocumentationWSHttp_Cert");

		string StoredUsername = "";
		string Username = "";
		string Password = "";

		SqlConnection connectionNewOne = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
		connectionNewOne.Open();

		SqlCommand myCommandUserNew = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ipipeline%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connectionNewOne);

		var readerUserNew = myCommandUserNew.ExecuteReader();

		while (readerUserNew.Read())
		{
			StoredUsername = readerUserNew["StoredUserName"].ToString();

			if (StoredUsername == "ipipelineuserid")
			{

				Username = readerUserNew["StoredUserValue"].ToString();

			}

			if (StoredUsername == "ipipelinepassword")
			{
				Password = readerUserNew["StoredUserValue"].ToString();
			}

		}


		client = new DocumentationServiceNew.DocumentationServiceClient("DocumentationWSHttp_UNP");
		client.ClientCredentials.UserName.UserName = Username;
		client.ClientCredentials.UserName.Password = Password;

		connectionNewOne.Close();


		try
		{
			DocumentationServiceNew.RetrieveClientIllustrationsListRequestMessage req = new DocumentationServiceNew.RetrieveClientIllustrationsListRequestMessage();

			req.Header = new DocumentationServiceNew.Header();
			req.Header.Application = "Arc";
			req.RetrieveClientIllustrationsListRequest = new DocumentationServiceNew.RetrieveClientIllustrationsListRequest();
			Guid QuoteId = new Guid(QuoteID);
			req.RetrieveClientIllustrationsListRequest.QuoteId = QuoteId;

			//##TRANSACTIONID##
			XmlSerializer Serializer = new XmlSerializer(req.GetType());

			TextWriter sw = new StringWriter();

			Serializer.Serialize(sw, req);
			sw.Close();
			string xx = sw.ToString().Replace("utf-16", "utf-8");


			// now make the call
			DocumentationServiceNew.RetrieveClientIllustrationsListResponseMessage resp = null;
			resp = client.RetrieveClientIllustrations(req);

			try
			{
				SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString());
				connection.Open();

				DateTime.Now.Year.ToString();
				SqlCommand insertESIS = new SqlCommand();
				insertESIS.Connection = connection;

				var cal = new CultureInfo("tr-TR", false).Calendar;

				string woy2 = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, System.DayOfWeek.Monday).ToString();
				int number = Convert.ToInt32(woy2);
				//number += 1;
				string newWOY = number.ToString();

				int docs = resp.RetrieveClientIllustrationsListResponse.IllustrationDocuments.Count();
				
				var iCount = docs;
				for (int i = 0; i <= iCount - 1; i++)
				{

					
					string filedate = (DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString());
					insertESIS.CommandText = string.Format("insert into tblPDFFiles(AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate, PDFHistoryFileName) values ('" + AppID + "', 'Illustration Document', 'application/pdf', @Data, '', '', '" + DateTime.Now + "','/arc-protect/" + newWOY + "-" + DateTime.Now.Year.ToString() + "/" + filedate + ".pdf')");

					insertESIS.Parameters.Add("@Data", SqlDbType.VarBinary).Value = resp.RetrieveClientIllustrationsListResponse.IllustrationDocuments[i].Data;
					insertESIS.ExecuteNonQuery();
					//databaseFileRead(AppID, "E:\\data\\attachments\\arc-protect\\" + newWOY + "-" + DateTime.Now.Year.ToString() + "\\" + filedate + ".pdf");
					connection.Close();

					HttpContext.Current.Response.Write("Illustration Document Downloaded please see document history");
					HttpContext.Current.Response.End();
				
					HttpContext.Current.Response.Redirect("https://arcbeta.engaged-solutions.co.uk/prompts/letterhistory.aspx?AppID=" + AppID + "&strMessageTitle=Documents%20Downloaded&strMessage=Documents%20Downloaded&strMessageType=success");
				}

				

			}
			catch (Exception ex)
			{
				HttpContext.Current.Response.Write(ex.ToString());
				HttpContext.Current.Response.End();
			}

			Serializer = new XmlSerializer(resp.GetType());
			sw = new StringWriter();
			Serializer.Serialize(sw, resp);
			sw.Close();


			//MessageBox.Show("Done");
		}
		catch (Exception ex)
		{
			HttpContext.Current.Response.Write(ex.ToString());
			HttpContext.Current.Response.End();
		}
		finally
		{
			client.Close();
		}

	}



	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());
		connection.Open();
		SqlCommand updatecmd2 = new SqlCommand();
		updatecmd2.Connection = connection;
		updatecmd2.CommandText = string.Format("INSERT INTO tblxmlreceived(XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES('" + ip + "', '" + soapid + "', 1, '" + msg + "', '" + strPost + "', getdate()) ");
		updatecmd2.ExecuteNonQuery();
		connection.Close();
	}

	public static void databaseFileRead(string AppID, string varPathToNewLocation)
	{
		using (var varConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringArcBeta"].ToString()))
		using (var sqlQuery = new SqlCommand(@"SELECT Data FROM [dbo].[tblPDFFiles] WHERE [AppID] = @varID order by CreatedDate desc", varConnection))
		{
			varConnection.Open();
			sqlQuery.Parameters.AddWithValue("@varID", AppID);
			using (var sqlQueryResult = sqlQuery.ExecuteReader())
				if (sqlQueryResult != null)
				{
					sqlQueryResult.Read();
					var blob = new Byte[(sqlQueryResult.GetBytes(0, 0, null, 0, int.MaxValue))];
					sqlQueryResult.GetBytes(0, 0, blob, 0, blob.Length);
					using (var fs = new FileStream(varPathToNewLocation, FileMode.Create, FileAccess.Write))
						fs.Write(blob, 0, blob.Length);
				}
		}

	}
}
