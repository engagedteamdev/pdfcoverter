﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolutionBuilderIntegration;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Demo : System.Web.UI.Page
{

	public string ModelID = HttpContext.Current.Request["ModelID"];
	public string ClientID = HttpContext.Current.Request["ClientID"];


	protected void Page_Load(object sender, EventArgs e)
	{
		httpPost();
	}


	//This method then goes on to retrieve the response for a given client and model.
	//It only retrieves the client name, so you will want to parse the response object and then extract the data
	//you need to populate in your CRM.
	public void httpPost()
	{
			try
			{

				var client = new GetClientDataClient();
				client.ClientCredentials.UserName.UserName = "web64837";
				client.ClientCredentials.UserName.Password = "shaM219tort";
				var req = new GetLikesAndAppliesRequestMessage();
				req.GetLikesAndAppliesRequest = new GetLikesAndAppliesRequest();


				if (!String.IsNullOrEmpty(ModelID))
				{
					req.GetLikesAndAppliesRequest.ModelId = int.Parse(ModelID);
				}
				else
				{
					req.GetLikesAndAppliesRequest.EarliestModelDate = DateTime.Now.AddDays(-30);
				}
				req.GetLikesAndAppliesRequest.InternalClientReference = int.Parse(ClientID);
				//req.GetLikesAndAppliesRequest.ExternalClientReference = this.txtClientId.Text;
				var resp = client.RetrieveGetLikesAndApplys(req);

				XmlDocument myXml = new XmlDocument();
				XPathNavigator xNav = myXml.CreateNavigator();
				XmlSerializer x = new XmlSerializer(resp.GetType());
				using (var xs = xNav.AppendChild())
				{
					x.Serialize(xs, resp);
				}
				HttpContext.Current.Response.ContentType = "text/xml";
				HttpContext.Current.Response.Write(myXml.OuterXml);
				HttpContext.Current.Response.Flush();
				HttpContext.Current.Response.SuppressContent = true;
				HttpContext.Current.ApplicationInstance.CompleteRequest();
				HttpContext.Current.Response.End();

			//string responseDetails = "";
			//responseDetails = "Details for Model ID: " + resp.Models[0].ModelId + Environment.NewLine;
			//responseDetails = responseDetails + "Get Client Data records found =" + resp.Models.Count + Environment.NewLine;
			//responseDetails = responseDetails + "Client Name: " + resp.Models[0].ClientSummary.FirstLife.Forenames + " " + resp.Models[0].ClientSummary.FirstLife.Surname + Environment.NewLine;
			//txtOutput.Text = responseDetails;

		}
			catch (Exception ex)
			{

				HttpContext.Current.Response.Write(ex.Message);
				HttpContext.Current.Response.End();
		}
		}
	}

