﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLReassured
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strPassword As String = "", strAccountNumber As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, strPost As String = "", objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        'If (strEnvironment = "live") Then
        strXMLURL = "https://www.reassuredpensions.co.uk/lead_egs_providers"
        'Else
        'strXMLURL = "http://test-leads.reassuredpensions.co.uk/lead_egs_providers"
        'End If


        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        strPost += "MediaCampaignID=" & strMediaCampaignCampaignReference & "&AppID=" & AppID


        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()

        Dim strSQL As String = "SELECT * FROM vwxmlreassured WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 1) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If
                        strPost += "&" & arrName(UBound(arrName)) & "=" & Row(Column).ToString
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, strPost, False, False, False)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            Dim strResponse As String = objReader.ReadToEnd()
            Dim strMessage As String = ""
			
			

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then

                ' Parse the XML document.
                Dim XMLDoc As XmlDocument = New XmlDocument
                XMLDoc.LoadXml(strResponse)
                Dim strResponseStatus As String = XMLDoc.SelectSingleNode("//status").InnerXml
                Dim strResponseError As String = XMLDoc.SelectSingleNode("//error").InnerXml

                Select Case strResponseStatus
                    Case "1"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Reasured"))
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", XMLDoc.SelectSingleNode("//ref").InnerText, "NULL")
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Reasured", strPost)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Reasured", XMLDoc.InnerXml)

                    Case Else
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Failed by Reasured: " & strResponseError))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To Reasured: " & strResponseError, strPost)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Reasured: " & strResponseError, XMLDoc.InnerXml)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select

            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode("//" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = objInputXMLDoc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNodeByNumber(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal lvl As Integer)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNodeList = objInputXMLDoc.GetElementsByTagName(fld)
                If Not (objTest Is Nothing) Then
                    objTest.Item(lvl - 1).InnerText = val
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
