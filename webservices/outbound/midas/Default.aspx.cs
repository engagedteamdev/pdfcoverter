using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using MidasIntegration;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;

public partial class DefaultSecuredLoan : Page
{

    string AppID = HttpContext.Current.Request["AppID"];
    string UserID = HttpContext.Current.Request["UserID"];

    string TestAppID = HttpContext.Current.Request["AppID"];

	public void Main()
    {


		

		string strErrors = "0|";

        // Advisor Details

        int MidasAdvisorID = 0;
        string AdvisorName = "";
        int ReferrerID = 0;
        int SourceID = 0;
        string SourceName = "";

        //ContactDetails

        String App1ContactType = "", App1Contact = "";
        bool App1Consent = false, App1Preferred = false;
        String App2ContactType = "", App2Contact = "";
        bool App2Consent = false, App2Preferred = false;


        //Client Details 
        string App1EmailAddress = "", App1FirstName = "", App1Surname = "", App1Gender = "", App1HomeTelephone = "", App1MaritalStatus = "";
        string App1MobileTelephone = "", App1Title = "", App1Nationality = "";
        int RetirementAge = 0;
        decimal App1GrossAnnualIncome = 0;
        DateTime App1DOB = new DateTime();
        bool App1UkResident = true, App1RightToReside = true, App1SalarySacrifices = true;

        string App2EmailAddress = "", App2FirstName = "", App2Surname = "", App2Gender = "", App2HomeTelephone = "", App2MaritalStatus = "";
        string App2MobileTelephone = "", App2Title = "", App2Nationality = "";
        int App2RetirementAge = 0;
        decimal App2GrossAnnualIncome = 0;
        DateTime App2DOB = new DateTime();
        bool App2UkResident = true, App2RightToReside = true, App2SalarySacrifices = true;

        //DependantDetails
        string DependantName = "";
        string DependantClient = "";
        DateTime DependantDOB = new DateTime();

        //Employment Details 

        string EmploymentStatus = "", EmployerName = "", EmployerAddress = "", Occupation = "", JobTitle = "";
        DateTime StartDate = new DateTime();
        decimal NetMonthlyIncome = 0;
        int EmploymentYears = 0, EmploymentMonths = 0;
        string employmentclient = "";
        //bool MainEmployment = true;

        //Employment Incomes
        string IncomeType = "";
        decimal IncomeAmount = 0;

        //Mortgage Requirements & Preferences
        decimal PurchasePrice = 0, LoanRequired = 0, Deposit = 0;
        string SourceOfDepositId = "";

        bool OffsetPayments = true, FixPayments = true, CapPayments = true, InitialTerm = true, NoHigherCharge = true;
        bool AbilityToAddFees = true, FlexiblePayments = true, Portability = true, FreeLegals = true, ValuationFees = true;
        bool NoArrangement = true, MaximumBooking = true, NoERC = true, Cashback = true, OtherPreference = true, PayBeforeTerm = true;

        string OffsetPaymentDetails = "", FixPaymentsDetails = "", CapPaymentsDetails = "", InitialTermDetails = "", NoHigherChargeDetails = "";
        string AbilityToAddFeesDetails = "", FlexiblePaymentsDetails = "", PortabilityDetails = "", FreeLegalsDetails = "", ValuationFeesDetails = "";
        string NoArrangementDetails = "", MaximumBookingDetails = "", NoERCDetails = "", CashbackDetails = "", OtherPreferenceDetails = "", PayBeforeTermDetails = "", AdverseCreditDetails = "";

        bool LTDBTL = true, ConsumerBTL = true, RegulatedBTL = true, FTB = true, FTBPurchase = true, AdverseCredit = true, ConsolidateDebt = true;

        string PurchaseType = "", PurchaseSubType = "", DepositSource = "", MostImportantToClient = "";

        int MortgageTermYears = 0;
        
        byte MortgageTermMonths = 0;

        //Budget Planner
        decimal MortBudget = 0, ProtBudget = 0, MortRent = 0, Utilities = 0, Food = 0, LoanPayments = 0, LifeInsurance = 0, Pension = 0, Maintenance = 0, Transport = 0, Essentials = 0, GoingOut = 0, Holidays = 0, Satellite = 0, Savings = 0, Luxuries = 0;
        string EssentialsReason = "", LuxuriesReason = "";

        //Personal Finance Details

        string SavType = "", SavProvider = "", SavObjective = "";
        decimal SavCurrentBalance = 0;
        int SavApplicantNo = 0;

        string InvestmentType = "", InvestmentObjective = "", InvestmentMortgageRepaymentVehicle = "", InvestmentProvider = "";
        decimal InvestmentCurrentValue = 0;
        int InvestmentApplicantNo = 0;
		bool boolInvestmentMortgageRepaymentVehicle = true;

		string AssetType = "";
        decimal AssetValue = 0;

        string PensionType = "", PensionProvider = "", PensionObjective = "", PensionMortgageRepaymentVehicle = "";
		bool boolPensionMortgageRepaymentVehicle = true;

		int PensionApplicantNo = 0;

        string BankName = "", AccountType = "", BankAddress = "";
        int BankYears = 0, BankMonths = 0;
        int BankApplicantNo = 0;
		decimal PropertyValue = 0, OutstandingAmount = 0;
		string MortgageType = "";
		bool boolRepaymentCharges = true, boolCompletingAfterERC = true, boolRepaymentChargesPreparedToPay = true, boolInitialTieInPeriod = true;
		string RepaymentChargesDetails = "", RepaymentCharges = "", CompletingAfterERC = "", RepaymentChargesPreparedToPay = "", InitialTieInPeriod = "";
		bool boolPortable = true;
		string Portable = "", OtherLenderDescription = "", MortgageLender = "";
		byte RemainingTermYears = 0, RemainingTermMonths = 0;
		decimal CurrentMonthlyPayment = 0;
		string Rate = "", RepaymentType = "", MortgageProductType = "";
		bool boolPortingMortgage = true;
		string PortingMortgage = "", AccountNumber = "", CurrentDeal = "", RepaymentPlanDetails = ""; 

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringFreedom365"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }


        SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwmidas where AppId = '" + AppID + "'"), connection);

        var reader = myCommand.ExecuteReader();

        while (reader.Read())
        {
            
            
            //Client Details
            App1Title = reader["App1Title"].ToString();
            App1FirstName = reader["App1FirstName"].ToString();
            App1Surname = reader["App1Surname"].ToString();
            DateTime.TryParse(reader["App1DOB"].ToString(), out App1DOB);
            App1EmailAddress = reader["App1EmailAddress"].ToString();
            App1Gender = reader["App1Sex"].ToString();
            App1HomeTelephone = reader["App1HomeTelephone"].ToString();
            App1MobileTelephone = reader["App1MobileTelephone"].ToString();
            App1MaritalStatus = reader["App1MaritalStatus"].ToString();
            App1Nationality = reader["App1Nationality"].ToString();
            bool.TryParse(reader["App1UKResident"].ToString(), out App1UkResident);
            bool.TryParse(reader["App1RightToReside"].ToString(), out App1RightToReside);
            bool.TryParse(reader["App1SalarySacrifices"].ToString(), out App1SalarySacrifices);
            int.TryParse(reader["App1RetirementAge"].ToString(), out RetirementAge);
            decimal.TryParse(reader["App1AnnualIncome"].ToString(), out App1GrossAnnualIncome);

            App2Title = reader["App2Title"].ToString();
            App2FirstName = reader["App2FirstName"].ToString();
            App2Surname = reader["App2Surname"].ToString();
            DateTime.TryParse(reader["App2DOB"].ToString(), out App2DOB);
            App2EmailAddress = reader["App2EmailAddress"].ToString();
            App2Gender = reader["App2Sex"].ToString();
            App2HomeTelephone = reader["App2HomeTelephone"].ToString();
            App2MobileTelephone = reader["App2MobileTelephone"].ToString();
            App2MaritalStatus = reader["App2MaritalStatus"].ToString();
            App2Nationality = reader["App2Nationality"].ToString();
            bool.TryParse(reader["App2UKResident"].ToString(), out App2UkResident);
            bool.TryParse(reader["App2RightToReside"].ToString(), out App2RightToReside);
            bool.TryParse(reader["App2SalarySacrifices"].ToString(), out App2SalarySacrifices);
            int.TryParse(reader["App2RetirementAge"].ToString(), out App2RetirementAge);
            decimal.TryParse(reader["App2AnnualIncome"].ToString(), out App2GrossAnnualIncome);


            //Mortgage Details
            decimal.TryParse(reader["PropertyPurchasePrice"].ToString(), out PurchasePrice);
            decimal.TryParse(reader["Amount"].ToString(), out LoanRequired);
            decimal.TryParse(reader["MortgageDeposit"].ToString(), out Deposit);
            SourceOfDepositId = reader["SourceOfDeposit"].ToString();



            //Mortgage Preferences
		try{
            bool.TryParse(reader["offsetPayments"].ToString(), out OffsetPayments);
            OffsetPaymentDetails = reader["OffsetDetails"].ToString();
			if (OffsetPayments == false)
			{
				OffsetPaymentDetails = "N/A";
			}
		}catch{
			OffsetPaymentDetails = "N/A";
		}
		try{
            bool.TryParse(reader["FixedPayments"].ToString(), out FixPayments);
            FixPaymentsDetails = reader["FixedPaymentDetails"].ToString();
			if (FixPayments == false)
			{
				FixPaymentsDetails = "N/A";
			}
		}catch{
			FixPaymentsDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["PaymentCap"].ToString(), out CapPayments);
            CapPaymentsDetails = reader["PaymentCapDetails"].ToString();
			if (CapPayments == false)
			{
				CapPaymentsDetails = "N/A";
			}
		}catch{
			CapPaymentsDetails = "N/A";
		}
		try{
			bool.TryParse(reader["InitialTerm"].ToString(), out InitialTerm);
            InitialTermDetails = reader["InitialTermDetails"].ToString();
			if (InitialTerm == false)
			{
				InitialTermDetails = "N/A";
			}
		}catch{
			InitialTermDetails = "N/A";
		}
		try{
			bool.TryParse(reader["higherLendingCharge"].ToString(), out NoHigherCharge);
            NoHigherChargeDetails = reader["higherLendingChargeDetails"].ToString();
			if (NoHigherCharge == false)
			{
				NoHigherChargeDetails = "N/A";
			}
		}catch{
			NoHigherChargeDetails = "N/A";
		}

		try{
			bool.TryParse(reader["feesAddedToLoan"].ToString(), out AbilityToAddFees);
            AbilityToAddFeesDetails = reader["feesAddedToLoanDetails"].ToString();
			if (AbilityToAddFees == false)
			{
				AbilityToAddFeesDetails = "N/A";
			}
		}catch{
			AbilityToAddFeesDetails = "N/A";
		}
		try{
			bool.TryParse(reader["flexiblePayments"].ToString(), out FlexiblePayments);
            FlexiblePaymentsDetails = reader["flexiblePaymentDetails"].ToString();
			if (FlexiblePayments == false)
			{
				FlexiblePaymentsDetails = "N/A";
			}
		}catch{
			FlexiblePaymentsDetails = "N/A";
		}
		try{
			bool.TryParse(reader["Portability"].ToString(), out Portability);
            PortabilityDetails = reader["PortabilityDetails"].ToString();
			if (Portability == false)
			{
				PortabilityDetails = "N/A";
			}
		}catch{
			PortabilityDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["freeLegals"].ToString(), out FreeLegals);
            FreeLegalsDetails = reader["freeLegalsDetails"].ToString();
			if (FreeLegals == false)
			{
				FreeLegalsDetails = "N/A";
			}
		}catch{
			FreeLegalsDetails = "N/A";
		}
		try{
			bool.TryParse(reader["freeVals"].ToString(), out ValuationFees);
            ValuationFeesDetails = reader["freeValDetails"].ToString();
			if (ValuationFees == false)
			{
				ValuationFeesDetails = "N/A";
			}
		}catch{
			ValuationFeesDetails = "N/A";
		}

		try{
			bool.TryParse(reader["noArrangmentFee"].ToString(), out NoArrangement);
            NoArrangementDetails = reader["noArrangementDetails"].ToString();
			if (NoArrangement == false)
			{
				NoArrangementDetails = "N/A";
			}
		}catch{
			NoArrangementDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["maxArrangmentFee"].ToString(), out MaximumBooking);
            MaximumBookingDetails = reader["maxArrangmentFeeDetails"].ToString();
			if (MaximumBooking == false)
			{
				MaximumBookingDetails = "N/A";
			}
		}catch{
			MaximumBookingDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["noEarlyRepayment"].ToString(), out NoERC);
            NoERCDetails = reader["noEarlyRepaymentDetails"].ToString();
			if (NoERC == false)
			{
				NoERCDetails = "N/A";
			}
		}catch{
			NoERCDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["cashback"].ToString(), out Cashback);
            CashbackDetails = reader["cashbackDetails"].ToString();
			if (Cashback == false)
			{
				CashbackDetails = "N/A";
			}
		}catch{
			CashbackDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["other"].ToString(), out OtherPreference);
            OtherPreferenceDetails = reader["otherDetails"].ToString();
			if (OtherPreference == false)
			{
				OtherPreferenceDetails = "N/A";
			}
		}catch{
			OtherPreferenceDetails = "N/A";
		}
		
		try{
			bool.TryParse(reader["Overpayments"].ToString(), out PayBeforeTerm);
            PayBeforeTermDetails = reader["OverpaymentDetails"].ToString();
			if (PayBeforeTerm == false)
			{
				PayBeforeTermDetails = "N/A";
			}
		}catch{
			PayBeforeTermDetails = "N/A";
		}
			bool.TryParse(reader["LimitedCompanyBTL"].ToString(), out LTDBTL);
            //bool.TryParse(reader["App2UKResident"].ToString(), out ConsumerBTL);
            bool.TryParse(reader["RegulatedBTL"].ToString(), out RegulatedBTL);
            bool.TryParse(reader["PurchaseFirstTimeBuyer"].ToString(), out FTB);
            bool.TryParse(reader["FirstTimeBuyerBTL"].ToString(), out FTBPurchase);
            bool.TryParse(reader["AnyAdverseCredit"].ToString(), out AdverseCredit);
            bool.TryParse(reader["ClientConsolidatingDebt"].ToString(), out ConsolidateDebt);

            AdverseCreditDetails = reader["AdverseCreditDetailsDetails"].ToString();
            PurchaseType = reader["PurchaseType"].ToString();
            PurchaseSubType = reader["PurchaseSubType"].ToString();
            DepositSource = reader["SourceOfDeposit"].ToString();
            MostImportantToClient = reader["costImportance"].ToString();

            int.TryParse(reader["ProductTerm"].ToString(), out MortgageTermYears);

            MortgageTermMonths = 0;

            //Budget Planner
            decimal.TryParse(reader["MortgageBudget"].ToString(), out MortBudget);
            decimal.TryParse(reader["ProtectionBudget"].ToString(), out ProtBudget);
            decimal.TryParse(reader["MortgageRent"].ToString(), out MortRent);
            decimal.TryParse(reader["UtilitiesCouncil"].ToString(), out Utilities);
            decimal.TryParse(reader["FoodClothing"].ToString(), out Food);
            decimal.TryParse(reader["LoanCards"].ToString(), out LoanPayments);
            decimal.TryParse(reader["Insurance"].ToString(), out LifeInsurance);
            decimal.TryParse(reader["Pension"].ToString(), out Pension);
            decimal.TryParse(reader["Childcare"].ToString(), out Maintenance);
            decimal.TryParse(reader["CarTransport"].ToString(), out Transport);
            decimal.TryParse(reader["Essentialother"].ToString(), out Essentials);
            decimal.TryParse(reader["GoingOut"].ToString(), out GoingOut);
            decimal.TryParse(reader["Holidays"].ToString(), out Holidays);
            decimal.TryParse(reader["Satalite"].ToString(), out Satellite);
            decimal.TryParse(reader["Savings"].ToString(), out Savings);
            decimal.TryParse(reader["Lifestyleother"].ToString(), out Luxuries);
            EssentialsReason = reader["EssentialOtherNotes"].ToString();
            LuxuriesReason = reader["LifestyleOtherNotes"].ToString();

        }


        if (string.IsNullOrEmpty(App1Title))
        {
            strErrors += "App1 Title";
        }

        if (string.IsNullOrEmpty(App1FirstName))
        {
            strErrors += "App1 First Name";
        }

        if (string.IsNullOrEmpty(App1Surname))
        {
            strErrors += "App1 Surname";
        }

        if (string.IsNullOrEmpty(App1Gender))
        {
            strErrors += "App1 Gender";
        }

        if (string.IsNullOrEmpty(App1MaritalStatus))
        {
            strErrors += "App1 Marital Status";
        }

        if (string.IsNullOrEmpty(App1HomeTelephone) && string.IsNullOrEmpty(App1MobileTelephone))
        {
            strErrors += "App1 Telephone Number";
        }

        if (string.IsNullOrEmpty(App1EmailAddress))
        {
            strErrors += "App1 Email Address";
        }

        if (string.IsNullOrEmpty(App1Nationality))
        {
            strErrors += "App1 Nationality";
        }

        //if (string.IsNullOrEmpty(Postcode))
        //{
        //    strErrors += "App1 Post Code";
        //}

        //if (string.IsNullOrEmpty(Town))
        //{
        //    strErrors += "App1 Town";
        //}

        //if (string.IsNullOrEmpty(AddressLine1))
        //{
        //    strErrors += "App1 Address Line 1";
        //}

        //if (string.IsNullOrEmpty(AddressLivingArrangement))
        //{
        //    strErrors += "App1 Residential Status";
        //}

        //if (string.IsNullOrEmpty(App1NameofEmployer))
        //{
        //    strErrors += "App1 Employer Name";
        //}

        //if (string.IsNullOrEmpty(App1Occupation))
        //{
        //    strErrors += "App1 Occupation";
        //}

        //if (string.IsNullOrEmpty(App1JobTitle))
        //{
        //    strErrors += "App1 Job Title";
        //}

        //if (App1TimeWithEmployerYears <= 0 && App1TimeWithEmployerMonths <= 0)
        //{
        //    strErrors += "App1 Time with employer";
        //}

        //if (App1NetIncome <= 0)
        //{
        //    strErrors += "App1 Net Income";
        //}

        //if (App1GrossAnnualIncome <= 0)
        //{
        //    strErrors += "App1 Gross Annual Income";
        //}

        //if (string.IsNullOrEmpty(App1EmployerAddressLine1))
        //{
        //    strErrors += "App1 Employer Address Line 1";
        //}


        if (!string.IsNullOrEmpty(App2FirstName))
        {

            if (string.IsNullOrEmpty(App2FirstName))
            {
                strErrors += "App2 First Name";
            }

            if (string.IsNullOrEmpty(App2Surname))
            {
                strErrors += "App2 Surname";
            }

            if (string.IsNullOrEmpty(App2Gender))
            {
                strErrors += "App2 Gender";
            }

            if (string.IsNullOrEmpty(App2MaritalStatus))
            {
                strErrors += "App2 Marital Status";
            }

            if (string.IsNullOrEmpty(App2HomeTelephone) && string.IsNullOrEmpty(App2MobileTelephone))
            {
                strErrors += "App2 Telephone Number";
            }

            if (string.IsNullOrEmpty(App2EmailAddress))
            {
                strErrors += "App2 Email Address";
            }

            if (string.IsNullOrEmpty(App2Nationality))
            {
                strErrors += "App2 Nationality";
            }


            //if (string.IsNullOrEmpty(App2NameofEmployer))
            //{
            //    strErrors += "App2 Employer Name";
            //}

            //if (string.IsNullOrEmpty(App2Occupation))
            //{
            //    strErrors += "App2 Occupation";
            //}

            //if (string.IsNullOrEmpty(App2JobTitle))
            //{
            //    strErrors += "App2 Job Title";
            //}

            //if (App2TimeWithEmployerYears <= 0 && App2TimeWithEmployerMonths <= 0)
            //{
            //    strErrors += "App2 Time with employer";
            //}

            //if (App2NetIncome <= 0)
            //{
            //    strErrors += "App2 Net Income";
            //}

            //if (App2GrossAnnualIncome <= 0)
            //{
            //    strErrors += "App2 Gross Annual Income";
            //}

            //if (string.IsNullOrEmpty(App2EmployerAddressLine1))
            //{
            //    strErrors += "App2 Employer Address Line 1";
            //}

        }

        if (PurchasePrice <= 0)
        {
            strErrors += "Property Purchase Price";
        }

        if (LoanRequired <= 0)
        {
            strErrors += "Requested Amount";
        }

        if (Deposit <= 0)
        {
            strErrors += "Desposit";
        }



        SqlCommand getReferrerCode = new SqlCommand(string.Format("Select UserFullName, MidasAdvisorID FROM tblusers where UserID = '" + UserID + "'"), connection);

        var readerReferrer = getReferrerCode.ExecuteReader();
        while (readerReferrer.Read())
        {
            int.TryParse(readerReferrer["MidasAdvisorID"].ToString(), out MidasAdvisorID);
            AdvisorName = readerReferrer["UserFullName"].ToString();
        }

        int.TryParse(getRefID(AdvisorName).ToString(), out ReferrerID);

        if (ReferrerID == 0)
        {
            strErrors += "Unable to find Referrer ID";
        }


        SqlCommand getLeadSource = new SqlCommand(string.Format("SELECT MidasCampaignName from tblmediacampaigns left outer join tblapplications on tblmediacampaigns.MediaCampaignID = tblapplications.MediaCampaignIDInbound where AppID = '" + AppID + "'"), connection);

        var readerLeadSource = getLeadSource.ExecuteReader();
        while (readerLeadSource.Read())
        {
            SourceName = readerLeadSource["MidasCampaignName"].ToString();
        }

        int.TryParse(getLeadSourceID(SourceName).ToString(), out SourceID);
        if (SourceID == 0)
        {
            strErrors += "Unable to find Lead Source ID";
        }


        if (strErrors == "0|")
        {

            ImportServiceClient client = new ImportServiceClient();

            client.ClientCredentials.UserName.UserName = "freedom365";
            client.ClientCredentials.UserName.Password = "Greenbean@1";

            var Applicant1Contacts = new List<WSLeadImporterContactMethod>();

            SqlCommand getApp1ContactTypes = new SqlCommand(string.Format("Select * FROM tblContactDetails where AppId = '" + AppID + "' AND ContactApplicant = '1' AND ContactActive = '1'"), connection);

            var readerApp1ContactTypes = getApp1ContactTypes.ExecuteReader();

            while (readerApp1ContactTypes.Read())
            {

                App1ContactType = readerApp1ContactTypes["ContactType"].ToString();
                App1Contact = readerApp1ContactTypes["Contact"].ToString();
                bool.TryParse(readerApp1ContactTypes["ContactConsent"].ToString(), out App1Consent);
                bool.TryParse(readerApp1ContactTypes["ContactPreferred"].ToString(), out App1Preferred);

                Applicant1Contacts.Add(new WSLeadImporterContactMethod()
                {
                    ContactMethodTypeId = getContactType(App1ContactType),
                    ContactValue = App1Contact,
                    PreferredMethod = App1Consent,
                    ConsentToContact = App1Preferred,



                });
            }


            var Applicant2Contacts = new List<WSLeadImporterContactMethod>();

            SqlCommand getApp2ContactTypes = new SqlCommand(string.Format("Select * FROM tblContactDetails where AppId = '" + AppID + "' AND ContactApplicant = '2' AND ContactActive = '1'"), connection);

            var readerApp2ContactTypes = getApp2ContactTypes.ExecuteReader();

            while (readerApp2ContactTypes.Read())
            {

                App2ContactType = readerApp2ContactTypes["ContactType"].ToString();
                App2Contact = readerApp2ContactTypes["Contact"].ToString();
                bool.TryParse(readerApp2ContactTypes["ContactConsent"].ToString(), out App2Consent);
                bool.TryParse(readerApp2ContactTypes["ContactPreferred"].ToString(), out App2Preferred);

                Applicant2Contacts.Add(new WSLeadImporterContactMethod()
                {
                    ContactMethodTypeId = getContactType(App2ContactType),
                    ContactValue = App2Contact,
                    PreferredMethod = App2Consent,
                    ConsentToContact = App2Preferred,



                });
            }

            var App1Address = new List<WSLeadImporterAddress>();

            SqlCommand getApp1Address = new SqlCommand(string.Format("Select * FROM tblAddressDetails where AppId = '" + AppID + "' AND Active = '1'"), connection);

            var readerApp1Address = getApp1Address.ExecuteReader();
            while (readerApp1Address.Read())
            {

                App1Address.Add(new WSLeadImporterAddress()
                {
                    Address1 = readerApp1Address["AddressLine1"].ToString(),
                    Address2 = readerApp1Address["AddressLine2"].ToString(),
                    Address3 = "",
                    Address4 = "",
                    Town = readerApp1Address["Town"].ToString(),
                    County = readerApp1Address["County"].ToString(),
                    Postcode = readerApp1Address["PostCode"].ToString(),
                    PropertyStatusId = getPropertyStatus(readerApp1Address["PropertyStatus"].ToString())



                });

            }
				
            try
            {

                var ClientDetails = new List<WSLeadImporterClient>();
                ClientDetails.Add(new WSLeadImporterClient()
                {
                    TitleId = getTitles(App1Title),
                    Firstname = App1FirstName,
                    Lastname = App1Surname,
                    DoB = App1DOB,
                    MaritalStatusId = getMaritalStatus(App1MaritalStatus),
                    GenderId = getGender(App1Gender),
                    ExtClientRef = TestAppID,
                    ContactMethods = Applicant1Contacts.ToArray(),


                });

                if (!string.IsNullOrEmpty(App2FirstName))
                {

                    ClientDetails.Add(new WSLeadImporterClient()
                    {
                        TitleId = getTitles(App2Title),
                        Firstname = App2Title,
                        Lastname = App2Surname,
                        DoB = App2DOB,
                        MaritalStatusId = getMaritalStatus(App2MaritalStatus),
                        GenderId = getGender(App2Gender),
                        ExtClientRef = TestAppID,
                        ContactMethods = Applicant2Contacts.ToArray(),


                    });

                }

                WSLeadImporterLead lead = new WSLeadImporterLead();
                {
                    lead.ExtAgentName = AdvisorName;
                    lead.ExtLeadId = TestAppID;
                    lead.LeadsourceId = SourceID;
                    lead.LeadsourceBranchId = 1;
                    lead.ReferrerId = ReferrerID;
                    lead.PreferredContactMethod = "";
                    lead.PreferredContactDay = "";
                    lead.PreferredContactTime = "";
                    lead.IntegrationId = 10;
                    lead.ExtImportRef = TestAppID;
                    lead.AdviserId = MidasAdvisorID;
                    lead.ExtCaseRef = TestAppID;
                    lead.Addresses = App1Address.ToArray();
                    lead.Clients = ClientDetails.ToArray();

                }
				

                var test = client.ImportLead(lead, false, false);

                //XmlDocument myXml = new XmlDocument();
                //XPathNavigator xNav = myXml.CreateNavigator();
                //XmlSerializer y = new XmlSerializer(test.GetType());
                //using (var xs = xNav.AppendChild())
                //{
                //    y.Serialize(xs, test);
                //}
                //HttpContext.Current.Response.ContentType = "text/xml";
                //HttpContext.Current.Response.Write(myXml.OuterXml);

                //HttpContext.Current.Response.Flush();
                //HttpContext.Current.Response.SuppressContent = true;
                //HttpContext.Current.ApplicationInstance.CompleteRequest();



                int MidasProCaseId = 0;
                string ExternalCaseRef = "";
                int MidasProClientId = 0;
                int App2MidasProClientId = 0;
                string ExternalClientRef = "";
                string MidasProAddressId = "";
                string ExternalAddressRef = "";

                foreach (var item in test)
                {

                    if (item.key.Contains("MidasProCaseId"))
                    {
                        MidasProCaseId = Convert.ToInt32(item.value);
                    }

                    if (item.key.Contains("ExternalCaseRef"))
                    {
                        ExternalCaseRef = item.value.ToString();
                    }

                    if (item.key.Contains("MidasProClientId"))
                    {
                        if (MidasProClientId > 0)
                        {
                            App2MidasProClientId = Convert.ToInt32(item.value);
                        }
                        else
                        {
                            MidasProClientId = Convert.ToInt32(item.value);
                        }
                    }

                    if (item.key.Contains("ExternalClientRef"))
                    {
                        ExternalClientRef = item.value.ToString();
                    }

                    if (item.key.Contains("MidasProAddressId"))
                    {
                        MidasProAddressId = item.value.ToString();
                    }

                    //if (item.key.Contains("ExternalAddressRef"))
                    //{
                    //    ExternalAddressRef = item.value.ToString();
                    //}


                }


                var Dependants = new List<WSLeadImporterFactFindDependant>();

                try
                {



                    SqlCommand getDependants = new SqlCommand(string.Format("Select * FROM tblDependantsInfo where AppId = '" + AppID + "' and Active = '1'"), connection);

                    var readerDependants = getDependants.ExecuteReader();

                    while (readerDependants.Read())
                    {
                        int DepAppNo = 0;

                        DependantClient = readerDependants["ApplicantNo"].ToString();
                        DependantName = readerDependants["DependantName"].ToString();
                        DependantDOB = DateTime.Parse(readerDependants["DependantDOB"].ToString());

                        if (DependantClient == "1")
                        {
                            DepAppNo = MidasProClientId;
                        }
                        else if (DependantClient == "2")
                        {
                            DepAppNo = App2MidasProClientId;
                        }
                        else
                        {
                            DepAppNo = MidasProClientId;
                        }

                        Dependants.Add(new WSLeadImporterFactFindDependant()
                        {
                            Name = DependantName,
                            DateOfBirth = DependantDOB,

                        });

                    }

                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                var App1EmploymentIncome = new List<WSLeadImporterFactFindClientEmploymentDetailEmploymentIncome>();
                try
                {



                    SqlCommand getApp1IncomeVals = new SqlCommand(string.Format("Select * FROM tblIncome where AppId = '" + AppID + "' and ApplicantNo = '1'"), connection);

                    var readerApp1IncomeDetails = getApp1IncomeVals.ExecuteReader();

                    while (readerApp1IncomeDetails.Read())
                    {
                        IncomeType = readerApp1IncomeDetails["IncomeType"].ToString();
                        decimal.TryParse(readerApp1IncomeDetails["IncomeAmount"].ToString(), out IncomeAmount);



                        App1EmploymentIncome.Add(new WSLeadImporterFactFindClientEmploymentDetailEmploymentIncome()
                        {
                            IncomeType = getEmploymentIncomeTypes(IncomeType),
                            GrossAnnualIncome = IncomeAmount
                        });
                    }
                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                var App2EmploymentIncome = new List<WSLeadImporterFactFindClientEmploymentDetailEmploymentIncome>();
                try
                {


                    SqlCommand getApp2IncomeVals = new SqlCommand(string.Format("Select * FROM tblIncome where AppId = '" + AppID + "' and ApplicantNo = '2'"), connection);

                    var readerApp2IncomeDetails = getApp2IncomeVals.ExecuteReader();

                    while (readerApp2IncomeDetails.Read())
                    {
                        IncomeType = readerApp2IncomeDetails["IncomeType"].ToString();
                        decimal.TryParse(readerApp2IncomeDetails["IncomeAmount"].ToString(), out IncomeAmount);



                        App2EmploymentIncome.Add(new WSLeadImporterFactFindClientEmploymentDetailEmploymentIncome()
                        {
                            IncomeType = getEmploymentIncomeTypes(IncomeType),
                            GrossAnnualIncome = IncomeAmount
                        });
                    }
                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                var App1EmploymentDetails = new List<WSLeadImporterFactFindClientEmploymentDetail>();

                try
                {


                    SqlCommand getEmployment = new SqlCommand(string.Format("Select * FROM tblPreviousEmployment where AppId = '" + AppID + "' and ApplicantID = '1' and Active = '1'"), connection);

                    var readerEmploymentDetails = getEmployment.ExecuteReader();

                    while (readerEmploymentDetails.Read())
                    {
                        EmploymentStatus = readerEmploymentDetails["PreviousEmploymentStatus"].ToString();
                        EmployerName = readerEmploymentDetails["PreviousEmployerName"].ToString();
                        EmployerAddress = readerEmploymentDetails["PreviousEmployerBuildingName"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerBuildingNumber"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerAddressLine1"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerAddressLine2"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerAddressLine3"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerCounty"].ToString() + ' ' + readerEmploymentDetails["PreviousEmployerPostcode"].ToString();
                        Occupation = readerEmploymentDetails["PreviousEmploymentOccupation"].ToString();
                        JobTitle = readerEmploymentDetails["PreviousEmploymentJobTitle"].ToString();
                        StartDate = DateTime.Parse(readerEmploymentDetails["PreviousEmploymentStartDate"].ToString());
                        decimal.TryParse(readerEmploymentDetails["PreviousEmploymentIncome"].ToString(), out NetMonthlyIncome);

                        int.TryParse(readerEmploymentDetails["PreviousEmploymentYearsInPositions"].ToString(), out EmploymentYears);
                        int.TryParse(readerEmploymentDetails["PreviousEmploymentMonthsInPositions"].ToString(), out EmploymentMonths);

                        employmentclient = readerEmploymentDetails["ApplicantID"].ToString();

                        App1EmploymentDetails.Add(new WSLeadImporterFactFindClientEmploymentDetail()
                        {
                            EmploymentStatus = getEmploymentStatuses(EmploymentStatus),
                            NameOfEmployer = EmployerName,
                            EmployersAddress = EmployerAddress,
                            //MainEmployment = true,
                            Occupation = Occupation,
                            JobTitle = JobTitle,
                            EmploymentStartDate = StartDate,
                            TimeWithEmployerMonths = EmploymentMonths,
                            TimeWithEmployerYears = EmploymentYears,
                            NetMonthlyIncome = NetMonthlyIncome,
                            EmploymentIncome = App1EmploymentIncome.ToArray(),
                        });


                    }

                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                var App2EmploymentDetails = new List<WSLeadImporterFactFindClientEmploymentDetail>();
                try
                {



                    SqlCommand getApp2Employment = new SqlCommand(string.Format("Select * FROM tblPreviousEmployment where AppId = '" + AppID + "' and ApplicantID = '2' and Active = '1'"), connection);

                    var readerApp2EmploymentDetails = getApp2Employment.ExecuteReader();

                    while (readerApp2EmploymentDetails.Read())
                    {

                        EmploymentStatus = readerApp2EmploymentDetails["PreviousEmploymentStatus"].ToString();
                        EmployerName = readerApp2EmploymentDetails["PreviousEmployerName"].ToString();
                        EmployerAddress = readerApp2EmploymentDetails["PreviousEmployerBuildingName"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerBuildingNumber"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerAddressLine1"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerAddressLine2"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerAddressLine3"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerCounty"].ToString() + ' ' + readerApp2EmploymentDetails["PreviousEmployerPostcode"].ToString();
                        Occupation = readerApp2EmploymentDetails["PreviousEmploymentOccupation"].ToString();
                        JobTitle = readerApp2EmploymentDetails["PreviousEmploymentJobTitle"].ToString();
                        StartDate = DateTime.Parse(readerApp2EmploymentDetails["PreviousEmploymentStartDate"].ToString());
                        decimal.TryParse(readerApp2EmploymentDetails["PreviousEmploymentIncome"].ToString(), out NetMonthlyIncome);

                        int.TryParse(readerApp2EmploymentDetails["PreviousEmploymentYearsInPositions"].ToString(), out EmploymentYears);
                        int.TryParse(readerApp2EmploymentDetails["PreviousEmploymentMonthsInPositions"].ToString(), out EmploymentMonths);

                        employmentclient = readerApp2EmploymentDetails["ApplicantID"].ToString();

                        App2EmploymentDetails.Add(new WSLeadImporterFactFindClientEmploymentDetail()
                        {
                            EmploymentStatus = getEmploymentStatuses(EmploymentStatus),
                            NameOfEmployer = EmployerName,
                            EmployersAddress = EmployerAddress,
                            //MainEmployment = true,
                            Occupation = Occupation,
                            JobTitle = JobTitle,
                            EmploymentStartDate = StartDate,
                            TimeWithEmployerMonths = EmploymentMonths,
                            TimeWithEmployerYears = EmploymentYears,
                            NetMonthlyIncome = NetMonthlyIncome,
                            EmploymentIncome = App2EmploymentIncome.ToArray(),
                        });


                    }

                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }




                var App1OtherIncome = new List<WSLeadImporterFactFindClientEmploymentDetailOtherIncome>();

                try
                {

                    SqlCommand getApp1OtherIncomeVals = new SqlCommand(string.Format("Select * FROM tblOtherIncome where AppId = '" + AppID + "'"), connection);

                    var readerApp1OtherIncomeDetails = getApp1OtherIncomeVals.ExecuteReader();

                    while (readerApp1OtherIncomeDetails.Read())
                    {
                        IncomeType = readerApp1OtherIncomeDetails["IncomeType"].ToString();
                        decimal.TryParse(readerApp1OtherIncomeDetails["IncomeAmount"].ToString(), out IncomeAmount);

                        App1OtherIncome.Add(new WSLeadImporterFactFindClientEmploymentDetailOtherIncome()
                        {
                            IncomeType = GetEmploymentOtherIncomeTypes(IncomeType),
                            MonthlyIncomeAmount = IncomeAmount
                        });

                    }
                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                var App2OtherIncome = new List<WSLeadImporterFactFindClientEmploymentDetailOtherIncome>();
                try
                {

                    SqlCommand getApp2OtherIncomeVals = new SqlCommand(string.Format("Select * FROM tblOtherIncome where AppId = '" + AppID + "'"), connection);

                    var readerApp2OtherIncomeDetails = getApp2OtherIncomeVals.ExecuteReader();

                    while (readerApp2OtherIncomeDetails.Read())
                    {
                        IncomeType = readerApp2OtherIncomeDetails["IncomeType"].ToString();
                        decimal.TryParse(readerApp2OtherIncomeDetails["IncomeAmount"].ToString(), out IncomeAmount);

                        App2OtherIncome.Add(new WSLeadImporterFactFindClientEmploymentDetailOtherIncome()
                        {
                            IncomeType = GetEmploymentOtherIncomeTypes(IncomeType),
                            MonthlyIncomeAmount = IncomeAmount
                        });



                    }
                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }




                var Clients = new List<WSLeadImporterFactFindClient>();

                try
                {

                    Clients.Add(new WSLeadImporterFactFindClient()
                    {
                        DateOfBirth = App1DOB,
                        MidasProClientId = MidasProClientId,
                        //ExternalClientRef = AppID,
                        RetirementAge = 65,
                        ResidentialStatus = getResidentialStatuses("Homeowner"),
                        UKResident = App1UkResident,
                        PermanentRightToReside = App1RightToReside,
                        Nationality = App1Nationality,
                        Smoker = false,
                        Guarantor = false,
                        AppointmentType = getAppointments("Telephone"),
                        SalarySacrifices = App1SalarySacrifices,
                        OtherIncome = App1OtherIncome.ToArray(),
                        EmploymentDetails = App1EmploymentDetails.ToArray(),

                    });

                    if (!string.IsNullOrEmpty(App2FirstName))
                    {

                        Clients.Add(new WSLeadImporterFactFindClient()
                        {
                            DateOfBirth = App2DOB,
                            MidasProClientId = App2MidasProClientId,
                            //ExternalClientRef = AppID,
                            RetirementAge = 65,
                            ResidentialStatus = getResidentialStatuses("Homeowner"),
                            UKResident = App2UkResident,
                            PermanentRightToReside = App2RightToReside,
                            Nationality = App2Nationality,
                            Smoker = false,
                            Guarantor = false,
                            AppointmentType = getAppointments("Telephone"),
                            SalarySacrifices = App2SalarySacrifices,
                            OtherIncome = App2OtherIncome.ToArray(),
                            EmploymentDetails = App2EmploymentDetails.ToArray(),

                        });


                    }

                }
                catch (Exception e)
                {
                    Response.Write(e);
                    Response.End();
                }


                WSLeadImporterFactFind Main = new WSLeadImporterFactFind();
                {

                    Main.MidasProCaseId = MidasProCaseId;
                    Main.ExternalCaseRef = ExternalCaseRef;
                    Main.ExtFactFindId = TestAppID;
                    Main.IDDDeclaration = true;
                    Main.FactFindClients = Clients.ToArray();
                    Main.Dependants = Dependants.ToArray();
                    Main.IntegrationId = 10;
                    Main.ExtImportRef = TestAppID;
                }




                var TestClientID = client.ImportFactFindClients(Main, false);
                int MidasFactFindClientID = 0, App2MidasFactFindClientID = 0;

                int MidasProFactFindID = 0;

                //XmlDocument myXml = new XmlDocument();
                //XPathNavigator xNav = myXml.CreateNavigator();
                //XmlSerializer y = new XmlSerializer(TestClientID.GetType());
                //using (var xs = xNav.AppendChild())
                //{
                //    y.Serialize(xs, TestClientID);
                //}
                //HttpContext.Current.Response.ContentType = "text/xml";
                //HttpContext.Current.Response.Write(myXml.OuterXml);

                //HttpContext.Current.Response.Flush();
                //HttpContext.Current.Response.SuppressContent = true;
                //HttpContext.Current.ApplicationInstance.CompleteRequest();

                foreach (var item in TestClientID)
                {
                    if (item.key.Contains("MidasProFactFindClientId"))
                    {
                        if (MidasFactFindClientID > 0)
                        {
                            App2MidasFactFindClientID = Convert.ToInt32(item.value);
                        }
                        else
                        {
                            MidasFactFindClientID = Convert.ToInt32(item.value);
                        }
                    }

                    if (item.key.Contains("MidasProFactFindId"))
                    {
                        MidasProFactFindID = Convert.ToInt32(item.value);

                    }
                }

                List<int> FactFindClientIDs = new List<int>();
                if (App2MidasFactFindClientID > 0)
                {
                    FactFindClientIDs.Add(App2MidasFactFindClientID);
                }
                FactFindClientIDs.Add(MidasFactFindClientID);

                var AdditionalSourceOfDeposits = new List<WSLeadImporterFactFindMortgageRequirementSourceOfDeposit>();

                WSLeadImporterFactFindMortgageRequirement MortgageDetails = new WSLeadImporterFactFindMortgageRequirement();
                {
                    MortgageDetails.ExtFactFindMortgageRequirementId = TestAppID;
                    MortgageDetails.FactFindClientIdsFinancialRelationships = FactFindClientIDs.ToArray();
                    MortgageDetails.ProFactFindId = MidasProFactFindID;
                    //MortgageDetails.ExtFactFindId = TestAppID;
                    MortgageDetails.IntegrationId = 10;
                    MortgageDetails.ExtImportRef = TestAppID;
                    MortgageDetails.AdverseCredit = AdverseCredit;
                    MortgageDetails.AdverseCreditDetails = AdverseCreditDetails;
                    MortgageDetails.ConsolidatingDebt = ConsolidateDebt;
                    MortgageDetails.ConsolidatingDebtArrears = false;
                    MortgageDetails.ConsolidatingDebtNegotiate = false;
                    MortgageDetails.ConsolidatingDebtNegotiateDetails = "";
                    MortgageDetails.PurchaseTypeId = GetPurchaseTypes(PurchaseType);
                    MortgageDetails.PurchaseSubTypeId = GetPurchaseSubTypes(PurchaseSubType);
                    MortgageDetails.PayOffMortgageBeforeRetirement = false;
                    MortgageDetails.NotPayingOffBeforeRetirementHowWillTheyPayOff = null;
                    MortgageDetails.ReasonForRemortgageId = GetRemortgageReasonTypes("Home Improvements");
                    MortgageDetails.OutstandingLoanAmount = null;
                    MortgageDetails.ConsumerBuyToLet = ConsumerBTL;
                    MortgageDetails.FirstTimeBuyer = FTB;
                    MortgageDetails.PropertyValue = PurchasePrice;
                    MortgageDetails.PurchasePrice = PurchasePrice;
                    MortgageDetails.LoanRequired = LoanRequired;
                    MortgageDetails.Deposit = Deposit;
                    MortgageDetails.SourceOfDepositId = getSourceofDeposit(DepositSource);
                    MortgageDetails.SourceOfDepositOther = "N/A";
                    MortgageDetails.EquityLoan = null;
                    MortgageDetails.ApplicantShareOfValueAmount = null;
                    MortgageDetails.TermYears = 25;
                    MortgageDetails.TermMonths = null;
                    MortgageDetails.RepaymentAmount = LoanRequired;
                    MortgageDetails.RepaymentPlanTypeId = GetRepaymentPlanTypes("Sale of Property");
                    MortgageDetails.RepaymentPlanDetails = "N/A";
                    MortgageDetails.NewPropertyAddressId = Convert.ToInt32(MidasProAddressId);
                    MortgageDetails.NewBuild = false;
                    MortgageDetails.FirstTimeBuyToLetPurchase = FTBPurchase;
                    MortgageDetails.RegulatedBuyToLet = RegulatedBTL;
                    MortgageDetails.LtdCompanyBuyToLet = LTDBTL;
                    MortgageDetails.BuyToLetAnticipatedMonthlyIncome = null;
                    MortgageDetails.MostImportantToClient = null;
                    MortgageDetails.ClientPlanningToPayOffMortgageBeforeFullTerm = PayBeforeTerm;
                    MortgageDetails.ClientPlanningToPayOffMortgageBeforeFullTermHow = PayBeforeTermDetails;
                    MortgageDetails.PreferencesAbilityToOffset = OffsetPayments;
                    MortgageDetails.PreferencesAbilityToOffsetDetails = OffsetPaymentDetails;
                    MortgageDetails.PreferencesAbilityToOffsetReason = null;
                    MortgageDetails.PreferencesAbilityToFixMortgagePayments = FixPayments;
                    MortgageDetails.PreferencesAbilityToFixMortgagePaymentsDetails = FixPaymentsDetails;
					MortgageDetails.PreferencesAbilityToCapMortgagePayments = CapPayments;
                    MortgageDetails.PreferencesAbilityToCapMortgagePaymentsDetails = CapPaymentsDetails;
					MortgageDetails.PreferencesProductInitialTermRequired = InitialTerm;
                    MortgageDetails.PreferencesProductInitialTermRequiredDetails = InitialTermDetails;
					MortgageDetails.PreferencesNoHigherLendingCharge = NoHigherCharge;
                    MortgageDetails.PreferencesNoHigherLendingChargeDetails = NoHigherChargeDetails;
					MortgageDetails.PreferencesAbilityToAddFeesToLoan = AbilityToAddFees;
                    MortgageDetails.PreferencesAbilityToAddFeesToLoanDetails = AbilityToAddFeesDetails;
					MortgageDetails.PreferencesFlexiblePayments = FlexiblePayments;
                    MortgageDetails.PreferencesFlexiblePaymentsDetails = FlexiblePaymentsDetails;
					MortgageDetails.PreferencesPortability = Portability;
                    MortgageDetails.PreferencesPortabilityDetails = PortabilityDetails;
					MortgageDetails.PreferencesFreeLegals = FreeLegals;
                    MortgageDetails.PreferencesFreeLegalsDetails = FreeLegalsDetails;
					MortgageDetails.PreferencesFreeValuationFees = ValuationFees;
                    MortgageDetails.PreferencesFreeValuationFeesDetails = ValuationFeesDetails;
					MortgageDetails.PreferencesNoArrangementFees = NoArrangement;
                    MortgageDetails.PreferencesNoArrangementFeesDetails = NoArrangementDetails;
					MortgageDetails.PreferencesMaximumBookingFees = MaximumBooking;
                    MortgageDetails.PreferencesMaximumBookingFeesDetails = MaximumBookingDetails;
					MortgageDetails.PreferencesNoEarlyRepaymentCharge = NoERC;
                    MortgageDetails.PreferencesNoEarlyRepaymentChargeDetails = NoERCDetails;
					MortgageDetails.PreferencesCashback = Cashback;
                    MortgageDetails.PreferencesCashbackDetails = CashbackDetails;
					MortgageDetails.PreferencesOther = OtherPreference;
                    MortgageDetails.PreferencesOtherDetails = OtherPreferenceDetails;
					MortgageDetails.ValuationOptionRequested = GetValuationOptionTypes("Standard Valuation");
                    MortgageDetails.ValidationOptionsDiscussedWithClient = true;
                    MortgageDetails.AdditionalSourceOfDeposits = AdditionalSourceOfDeposits.ToArray();
					//MortgageDetails.DepositGiftInClientAccount = GetNotPayingOff("Sale of Property");
					MortgageDetails.MostImportantToClient = GetMostImportantTypes(MostImportantToClient);



				}


				

				var ImpMort = client.ImportFactFindMortgageRequirement(MortgageDetails, false);

				//XmlDocument myXml = new XmlDocument();
				//XPathNavigator xNav = myXml.CreateNavigator();
				//XmlSerializer y = new XmlSerializer(ImpMort.GetType());
				//using (var xs = xNav.AppendChild())
				//{
				//	y.Serialize(xs, ImpMort);
				//}
				//HttpContext.Current.Response.ContentType = "text/xml";
				//HttpContext.Current.Response.Write(myXml.OuterXml);

				//HttpContext.Current.Response.Flush();
				//HttpContext.Current.Response.SuppressContent = true;
				//HttpContext.Current.ApplicationInstance.CompleteRequest();



				//XmlDocument myXml = new XmlDocument();
				//XPathNavigator xNav = myXml.CreateNavigator();
				//XmlSerializer y = new XmlSerializer(ImpMort.GetType());
				//using (var xs = xNav.AppendChild())
				//{
				//    y.Serialize(xs, ImpMort);
				//}
				//HttpContext.Current.Response.ContentType = "text/xml";
				//HttpContext.Current.Response.Write(myXml.OuterXml);

				//HttpContext.Current.Response.Flush();
				//HttpContext.Current.Response.SuppressContent = true;
				//HttpContext.Current.ApplicationInstance.CompleteRequest();



				//XmlDocument myXml = new XmlDocument();
				//XPathNavigator xNav = myXml.CreateNavigator();
				//XmlSerializer y = new XmlSerializer(MortgageDetails.GetType());
				//using (var xs = xNav.AppendChild())
				//{
				//    y.Serialize(xs, MortgageDetails);
				//}
				//HttpContext.Current.Response.ContentType = "text/xml";
				//HttpContext.Current.Response.Write(myXml.OuterXml);

				//HttpContext.Current.Response.Flush();
				//HttpContext.Current.Response.SuppressContent = true;
				//HttpContext.Current.ApplicationInstance.CompleteRequest();

				var BudgetPlannerClient = new List<AllocatedClient>();

                BudgetPlannerClient.Add(new AllocatedClient() {
                    Allocated = true,
                    FactFindClientId = MidasFactFindClientID
                });



                WSLeadImporterFactFindBudgetPlanner BudgetPlanner = new WSLeadImporterFactFindBudgetPlanner();
                {
                    BudgetPlanner.ExtFactFindBudgetPlannerId = TestAppID;
                    BudgetPlanner.ProFactFindId = MidasProFactFindID;
                    //BudgetPlanner.ExtFactFindId = TestAppID;
                    BudgetPlanner.IntegrationId = 10;
                    BudgetPlanner.ExtImportRef = TestAppID;
                    BudgetPlanner.MortgageBudget = MortBudget;
                    BudgetPlanner.ProtectionBudget = ProtBudget;
                    BudgetPlanner.MortgageRent = MortRent;
                    BudgetPlanner.UtilitiesCouncilTax = Utilities;
                    BudgetPlanner.Food = Food;
                    BudgetPlanner.LoanCardPayments = LoanPayments;
                    BudgetPlanner.LifeOtherInsurance = LifeInsurance;
                    BudgetPlanner.Pension = Pension;
                    BudgetPlanner.ChildcareMaintenance = Maintenance;
                    BudgetPlanner.CarTransport = Transport;
                    BudgetPlanner.EssentialOther = Essentials;
                    BudgetPlanner.GoingOut = GoingOut;
                    BudgetPlanner.Holidays = Holidays;
                    BudgetPlanner.Satellite = Satellite;
                    BudgetPlanner.Savings = Savings;
                    BudgetPlanner.LuxuriesOther = Luxuries;
                    BudgetPlanner.EssentialOtherReason = EssentialsReason;
                    BudgetPlanner.LuxuriesOtherReason = LuxuriesReason;
                    BudgetPlanner.FactFindClientIds = BudgetPlannerClient.ToArray();
                }



				var ImpBudgetPlanner = client.ImportFactFindBudgetPlanner(BudgetPlanner, false);


				//XmlDocument myXml = new XmlDocument();
				//XPathNavigator xNav = myXml.CreateNavigator();
				//XmlSerializer y = new XmlSerializer(ImpBudgetPlanner.GetType());
				//using (var xs = xNav.AppendChild())
				//{
				//	y.Serialize(xs, ImpBudgetPlanner);
				//}
				//HttpContext.Current.Response.ContentType = "text/xml";
				//HttpContext.Current.Response.Write(myXml.OuterXml);

				//HttpContext.Current.Response.Flush();
				//HttpContext.Current.Response.SuppressContent = true;
				//HttpContext.Current.ApplicationInstance.CompleteRequest();


				var SavingInfo = new List<WSLeadImporterFactFindPersonalFinanceSaving>();


                SqlCommand getSavings = new SqlCommand(string.Format("Select * FROM tblPersonalFinance where AppId = '" + AppID + "' AND FinanceType = '1'"), connection);

                var readerSavings = getSavings.ExecuteReader();

                while (readerSavings.Read())
                    {

                        SavType = readerSavings["SavingsType"].ToString();
                        SavProvider = readerSavings["SavingsProvider"].ToString();
                        SavObjective = readerSavings["SavingsObjective"].ToString();
                        decimal.TryParse(readerSavings["SavingsCurrentBalance"].ToString(), out SavCurrentBalance);
                        int.TryParse(readerSavings["Client"].ToString(), out SavApplicantNo);

                        if (SavApplicantNo == 1)
                        {
                            SavApplicantNo = MidasFactFindClientID;
                        }
                        else if (SavApplicantNo == 2)
                        {
                            SavApplicantNo = App2MidasFactFindClientID;
                        }
                        else
                        {
                            SavApplicantNo = MidasFactFindClientID;
                        }

               


                    }

				SavingInfo.Add(new WSLeadImporterFactFindPersonalFinanceSaving()
				{
					SavingsType = SavType,
					Provider = SavProvider,
					Objective = SavObjective,
					CurrentBalance = SavCurrentBalance,
					AllocatedFactFindClients = getAllocation(SavApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray(),
				});

				WSLeadImporterFactFindPersonalFinanceSavingBase SavingsDetails = new WSLeadImporterFactFindPersonalFinanceSavingBase();
                {

                    SavingsDetails.Items = SavingInfo.ToArray();
                    SavingsDetails.ProFactFindId = MidasProFactFindID;
                    //SavingsDetails.ExtFactFindId = TestAppID;
                    SavingsDetails.IntegrationId = 10;
                    SavingsDetails.ExtImportRef = TestAppID;




                   }


				var ImpSavings = client.ImportFactFindPersonalFinanceSavings(SavingsDetails, false);


				



				var InvestmentInfo = new List<WSLeadImporterFactFindPersonalFinanceInvestment>();


                SqlCommand getInvestments = new SqlCommand(string.Format("Select * FROM tblPersonalFinance where AppId = '" + AppID + "' AND FinanceType = '2'"), connection);

                var readerInvestments = getInvestments.ExecuteReader();

                while (readerInvestments.Read())
                {

                    InvestmentType = readerInvestments["InvestmentType"].ToString();
                    
                    InvestmentObjective = readerInvestments["InvestmentObjective"].ToString();
                    decimal.TryParse(readerInvestments["InvestmentCurrentValue"].ToString(), out InvestmentCurrentValue);
                    int.TryParse(readerInvestments["Client"].ToString(), out InvestmentApplicantNo);

                    if (InvestmentApplicantNo == 1)
                    {
                        InvestmentApplicantNo = MidasFactFindClientID;
                    }
                    else if (InvestmentApplicantNo == 2)
                    {
                        InvestmentApplicantNo = App2MidasFactFindClientID;
                    }
                    else
                    {
                        InvestmentApplicantNo = MidasFactFindClientID;
                    }

					InvestmentMortgageRepaymentVehicle = readerInvestments["MortgageRepaymentVehicle"].ToString();

					if (InvestmentMortgageRepaymentVehicle == "Yes")
					{
						boolInvestmentMortgageRepaymentVehicle = true;
					}
					else
					{
						boolInvestmentMortgageRepaymentVehicle = false;
					}

					InvestmentProvider = readerInvestments["Provider"].ToString();

					InvestmentInfo.Add(new WSLeadImporterFactFindPersonalFinanceInvestment()
                    {
                        InvestmentType = InvestmentType,
                        Objective = InvestmentObjective,
                        CurrentValue = InvestmentCurrentValue,
						Provider = InvestmentProvider,
						MortgageRepaymentVehicle = boolInvestmentMortgageRepaymentVehicle,
                        AllocatedFactFindClients = getAllocation(InvestmentApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray(),
                    });


                }

                WSLeadImporterFactFindPersonalFinanceInvestmentBase InvestmentDetails = new WSLeadImporterFactFindPersonalFinanceInvestmentBase();
                {

                    InvestmentDetails.Items = InvestmentInfo.ToArray();
                    InvestmentDetails.ProFactFindId = MidasProFactFindID;
                    //InvestmentDetails.ExtFactFindId = TestAppID;
                    InvestmentDetails.IntegrationId = 10;
                    InvestmentDetails.ExtImportRef = TestAppID;
					

                }



				var ImpInvestments = client.ImportFactFindPersonalFinanceInvestments(InvestmentDetails, false);



				var AssetInfo = new List<WSLeadImporterFactFindPersonalFinanceAsset>();


                SqlCommand getAssets = new SqlCommand(string.Format("Select * FROM tblPersonalFinance where AppId = '" + AppID + "' AND FinanceType = '3'"), connection);

                var readerAssets = getAssets.ExecuteReader();

                while (readerAssets.Read())
                {

                    AssetType = readerAssets["AssetType"].ToString();
                    decimal.TryParse(readerAssets["AssetValue"].ToString(), out AssetValue);
           
                    AssetInfo.Add(new WSLeadImporterFactFindPersonalFinanceAsset()
                    {
                        AssetType = GetFactFindPersonalFinanceAssetTypes(AssetType),
                        AssetValue = AssetValue,

                    });


                }

                WSLeadImporterFactFindPersonalFinanceAssetBase AssetDetails = new WSLeadImporterFactFindPersonalFinanceAssetBase();
                {

                    AssetDetails.Items = AssetInfo.ToArray();
                    AssetDetails.ProFactFindId = MidasProFactFindID;
                    //AssetDetails.ExtFactFindId = TestAppID;
                    AssetDetails.IntegrationId = 10;
                    AssetDetails.ExtImportRef = TestAppID;

                }


                var ImpAssets = client.ImportFactFindPersonalFinanceAssets(AssetDetails, false);


				var BankInfo = new List<WSLeadImporterFactFindPersonalFinanceBankAccount>();


                SqlCommand getBank = new SqlCommand(string.Format("Select * FROM tblPersonalFinance where AppId = '" + AppID + "' AND FinanceType = '5'"), connection);

                var readerBankDetails = getBank.ExecuteReader();

                while (readerBankDetails.Read())
                {

                    BankName = readerBankDetails["BankName"].ToString();
                    AccountType = readerBankDetails["BankAccountType"].ToString();
                    BankAddress = readerBankDetails["BankAddress"].ToString();
                    int.TryParse(readerBankDetails["BankTimeYears"].ToString(), out BankYears);
                    int.TryParse(readerBankDetails["BankTimeMonths"].ToString(), out BankMonths);
                    int.TryParse(readerBankDetails["Client"].ToString(), out BankApplicantNo);

                    if (BankApplicantNo == 1)
                    {
                        BankApplicantNo = MidasFactFindClientID;
                    }
                    else if (BankApplicantNo == 2)
                    {
                        BankApplicantNo = App2MidasFactFindClientID;
                    }
                    else
                    {
                        BankApplicantNo = MidasFactFindClientID;
                    }

                    BankInfo.Add(new WSLeadImporterFactFindPersonalFinanceBankAccount()
                    {
                        BankName = PensionType,
                        YearsWithBank = BankYears,
                        MonthsWithBank = BankMonths,
                        AccountType = AccountType,
                        BankAddress = BankAddress,
                        AllocatedFactFindClients = getAllocation(BankApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray(),

                    });


                }

                WSLeadImporterFactFindPersonalFinanceBankAccountBase BankDetails = new WSLeadImporterFactFindPersonalFinanceBankAccountBase();
                {

                    BankDetails.Items = BankInfo.ToArray();
                    BankDetails.ProFactFindId = MidasProFactFindID;
                    //BankDetails.ExtFactFindId = TestAppID;
                    BankDetails.IntegrationId = 10;
                    BankDetails.ExtImportRef = TestAppID;

                }


                var ImpBank = client.ImportFactFindPersonalFinanceBankAccounts(BankDetails, false);




				var PensionInfo = new List<WSLeadImporterFactFindPersonalFinancePension>();


                SqlCommand getPension = new SqlCommand(string.Format("Select * FROM tblPersonalFinance where AppId = '" + AppID + "' AND FinanceType = '4'"), connection);

                var readerPension = getPension.ExecuteReader();

                while (readerPension.Read())
                {

                    PensionType = readerPension["PensionType"].ToString();
                    
                    PensionProvider = readerPension["PensionProvider"].ToString();
                    PensionObjective = readerPension["PensionObjective"].ToString();

					PensionMortgageRepaymentVehicle = readerPension["MortgageRepaymentVehicle"].ToString();

					if (PensionMortgageRepaymentVehicle == "Yes")
					{
						boolPensionMortgageRepaymentVehicle = true;
					}
					else
					{
						boolPensionMortgageRepaymentVehicle = false;
					}


					int.TryParse(readerPension["Client"].ToString(), out PensionApplicantNo);

                    if (PensionApplicantNo == 1)
                    {
                        PensionApplicantNo = MidasFactFindClientID;
                    }
                    else if (PensionApplicantNo == 2)
                    {
                        PensionApplicantNo = App2MidasFactFindClientID;
                    }
                    else
                    {
                        PensionApplicantNo = MidasFactFindClientID;
                    }

                    PensionInfo.Add(new WSLeadImporterFactFindPersonalFinancePension()
                    {
                        PensionType = PensionType,
                        Provider = PensionProvider,
                        Objective = PensionObjective,
						MortgageRepaymentVehicle = boolPensionMortgageRepaymentVehicle,
                        AllocatedFactFindClients = getAllocation(PensionApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray(),
                    });


                }

                WSLeadImporterFactFindPersonalFinancePensionBase PensionDetails = new WSLeadImporterFactFindPersonalFinancePensionBase();
                {

                    PensionDetails.Items = PensionInfo.ToArray();
                    PensionDetails.ProFactFindId = MidasProFactFindID;
                    //PensionDetails.ExtFactFindId = TestAppID;
                    PensionDetails.IntegrationId = 10;
                    PensionDetails.ExtImportRef = TestAppID;

                }

				

				var ImpPension = client.ImportFactFindPersonalFinancePensions(PensionDetails, false);


				var CurrentProperties = new List<WSLeadImporterFactFindCurrentProperty>();

				var CurrentPropertyMortgage = new List<WSLeadImporterFactFindCurrentPropertyMortgage>();


				SqlCommand getProperties = new SqlCommand(string.Format("Select * FROM tblExistingProperties where AppId = '" + AppID + "'"), connection);

				var readerProperties = getProperties.ExecuteReader();

				while (readerProperties.Read())
				{


					decimal.TryParse(readerProperties["PropertyValue"].ToString(), out PropertyValue);
					MortgageType = readerProperties["MortgageType"].ToString();
					CurrentDeal = readerProperties["CurrentDeal"].ToString();
					RepaymentCharges = readerProperties["RepaymentCharges"].ToString();


					if (RepaymentCharges == "Yes")
					{
						boolRepaymentCharges = true;
					}
					else if (RepaymentCharges == "No")
					{
						boolRepaymentCharges = false;
					}
					else
					{
						boolRepaymentCharges = false;
					}
					RepaymentChargesDetails = readerProperties["RepaymentChargesDetails"].ToString();

					CompletingAfterERC = readerProperties["CompletingAfterERC"].ToString();
					if (CompletingAfterERC == "Yes")
					{
						boolCompletingAfterERC = true;
					}
					else if (CompletingAfterERC == "No")
					{
						boolCompletingAfterERC = false;
					}
					else
					{
						boolCompletingAfterERC = false;
					}
					RepaymentChargesPreparedToPay = readerProperties["PreparedERC"].ToString();
					if (RepaymentChargesPreparedToPay == "Yes")
					{
						boolRepaymentChargesPreparedToPay = true;
					}
					else if (RepaymentChargesPreparedToPay == "No")
					{
						boolRepaymentChargesPreparedToPay = false;
					}
					else
					{
						boolRepaymentChargesPreparedToPay = false;
					}
					InitialTieInPeriod = readerProperties["InitialTieInPeriod"].ToString();
					if (InitialTieInPeriod == "Yes")
					{
						boolInitialTieInPeriod = true;
					}
					else if (InitialTieInPeriod == "No")
					{
						boolInitialTieInPeriod = false;
					}
					else
					{
						boolInitialTieInPeriod = false;
					}

					Portable = readerProperties["PortingMortgage"].ToString();
					if (Portable == "Yes")
					{
						boolPortable = true;
					}
					else if (Portable == "No")
					{
						boolPortable = false;
					}
					else
					{
						boolPortable = false;
					}
					MortgageLender = readerProperties["Lender"].ToString();
					decimal.TryParse(readerProperties["OutstandingAmount"].ToString(), out OutstandingAmount);
					byte.TryParse(readerProperties["RemainingTermYears"].ToString(), out RemainingTermYears);
					byte.TryParse(readerProperties["RemainingTermMonths"].ToString(), out RemainingTermMonths);
					decimal.TryParse(readerProperties["MonthlyPayment"].ToString(), out CurrentMonthlyPayment);
					Rate = readerProperties["Rate"].ToString();
					RepaymentType = readerProperties["RepaymentType"].ToString();
					PortingMortgage = readerProperties["PortingMortgage"].ToString();
					if (PortingMortgage == "Yes")
					{
						boolPortingMortgage = true;
					}
					else if (PortingMortgage == "No")
					{
						boolPortingMortgage = false;
					}
					else
					{
						boolPortingMortgage = false;
					}
					AccountNumber = readerProperties["MortgageAccount"].ToString();

					OtherLenderDescription = readerProperties["OtherLenderDescription"].ToString();
					if (OtherLenderDescription == "")
					{
						OtherLenderDescription = "N/A";
					}
					RepaymentPlanDetails = readerProperties["RepaymentPlanDetails"].ToString();
					if (RepaymentPlanDetails == "")
					{
						RepaymentPlanDetails = "N/A";
					}

					CurrentPropertyMortgage.Add(new WSLeadImporterFactFindCurrentPropertyMortgage()
					{
						RepaymentCharges = boolRepaymentCharges,
						RepaymentChargesDetails = RepaymentChargesDetails,
						CompletingAfterERC= boolCompletingAfterERC,
						RepaymentChargesPreparedToPay = boolRepaymentChargesPreparedToPay,
						InitialTieInPeriod = boolInitialTieInPeriod,
						Portable = boolPortable,
						OtherLenderDescription = OtherLenderDescription,
						LenderId = GetFactFindCurrentPropertyLenders(MortgageLender),
						OutstandingAmount = OutstandingAmount,
						RemainingTermMonths = RemainingTermMonths,
						RemainingTermYears = RemainingTermYears,
						CurrentMonthlyPayment = CurrentMonthlyPayment,
						Rate = Rate,
						RepaymentType = GetFactFindCurrentPropertyRepaymentTypes(RepaymentType),
						RepaymentPlanDetails = RepaymentPlanDetails,
						PortingMortgage = boolPortingMortgage,
						AccountNumber = AccountNumber,
						MortgageProductTypeId = GetFactFindCurrentPropertyMortgageProductTypes(CurrentDeal),





					});

					CurrentProperties.Add(new WSLeadImporterFactFindCurrentProperty()
					{
						ExtFactFindCurrentPropertyId = TestAppID,
						ProFactFindId = MidasProFactFindID,
						IntegrationId = 10,
						ExtImportRef = TestAppID,
						AddressId = Int32.Parse(MidasProAddressId),
						HouseValue = PropertyValue,
						LinkedFactFindMortgageRequirementId = null,
						MortgageTypeId = GetFactFindCurrentPropertyMortgageTypes(MortgageType),
						BTLRentalIncome = null,
						Mortgages = CurrentPropertyMortgage.ToArray(),
						AllocatedFactFindClients = getAllocation(PensionApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray(),
					});




				}

			

				WSLeadImporterFactFindCurrentProperty CurrentProperty = new WSLeadImporterFactFindCurrentProperty();
				{

					CurrentProperty.Mortgages = CurrentPropertyMortgage.ToArray();
					CurrentProperty.ExtFactFindCurrentPropertyId = TestAppID;
					CurrentProperty.ProFactFindId = MidasProFactFindID;
					CurrentProperty.IntegrationId = 10;
					CurrentProperty.ExtImportRef = TestAppID;
					CurrentProperty.AddressId = Int32.Parse(MidasProAddressId);
					CurrentProperty.HouseValue = PropertyValue;
					CurrentProperty.LinkedFactFindMortgageRequirementId = null;
					CurrentProperty.MortgageTypeId = GetFactFindCurrentPropertyMortgageTypes(MortgageType);
					CurrentProperty.BTLRentalIncome = null;
					CurrentProperty.AllocatedFactFindClients = getAllocation(PensionApplicantNo, MidasFactFindClientID, App2MidasFactFindClientID).ToArray();
					
				}

				



				var CurrProperty = client.ImportFactFindCurrentProperty(CurrentProperty, false);

				//XmlDocument myXml = new XmlDocument();
				//XPathNavigator xNav = myXml.CreateNavigator();
				//XmlSerializer y = new XmlSerializer(CurrProperty.GetType());
				//using (var xs = xNav.AppendChild())
				//{
				//	y.Serialize(xs, CurrProperty);
				//}
				//HttpContext.Current.Response.ContentType = "text/xml";
				//HttpContext.Current.Response.Write(myXml.OuterXml);

				//HttpContext.Current.Response.Flush();
				//HttpContext.Current.Response.SuppressContent = true;
				//HttpContext.Current.ApplicationInstance.CompleteRequest();

				if (ImpMort > 0)
                {
                    SqlCommand updatecmd2 = new SqlCommand();
                    updatecmd2.Connection = connection;
                    updatecmd2.CommandText = string.Format("UPDATE tblapplications set MidasMortgageID = '" + ImpMort + "' where AppID = '" + AppID + "'");
                    updatecmd2.ExecuteNonQuery();
                }

                foreach (var item in TestClientID)
                {

                    if (item.key.Contains("MidasProCaseId"))
                    {
                        MidasProCaseId = Convert.ToInt32(item.value);
                    }

                    if (item.key.Contains("ExternalCaseRef"))
                    {
                        ExternalCaseRef = item.value.ToString();
                    }

                    if (item.key.Contains("MidasProClientId"))
                    {
                        if (MidasProClientId > 0)
                        {
                            App2MidasProClientId = Convert.ToInt32(item.value);
                        }
                        else
                        {
                            MidasProClientId = Convert.ToInt32(item.value);
                        }
                    }

                    if (item.key.Contains("ExternalClientRef"))
                    {
                        ExternalClientRef = item.value.ToString();
                    }

                    if (item.key.Contains("MidasProAddressId"))
                    {
                        MidasProAddressId = item.value.ToString();
                    }

                    if (item.key.Contains("ExternalAddressRef"))
                    {
                        ExternalAddressRef = item.value.ToString();
                    }

                }

                if (MidasProClientId > 0)
                {
                    SqlCommand updatecmd2 = new SqlCommand();
                    updatecmd2.Connection = connection;
                    updatecmd2.CommandText = string.Format("UPDATE tblapplications set App1MidasProClientId = '" + MidasProClientId + "' where AppID = '" + AppID + "'");
                    updatecmd2.ExecuteNonQuery();
                }

                if (App2MidasProClientId > 0)
                {

                    SqlCommand updatecmd3 = new SqlCommand();
                    updatecmd3.Connection = connection;
                    updatecmd3.CommandText = string.Format("UPDATE tblapplications set App2MidasProClientId = '" + App2MidasProClientId + "' where AppID = '" + AppID + "'");
                    updatecmd3.ExecuteNonQuery();
                }

                if (MidasProCaseId > 0)
                {
                    SqlCommand updatecmd = new SqlCommand();
                    updatecmd.Connection = connection;
                    updatecmd.CommandText = string.Format("UPDATE tblapplications set MidasProCaseId = '" + MidasProCaseId + "' where AppID = '" + AppID + "'");
                    updatecmd.ExecuteNonQuery();

                }

                if (!string.IsNullOrEmpty(ExternalCaseRef))
                {
                    SqlCommand updatecmd1 = new SqlCommand();
                    updatecmd1.Connection = connection;
                    updatecmd1.CommandText = string.Format("UPDATE tblapplications set MidasExternalCaseRef = '" + ExternalCaseRef + "' where AppID = '" + AppID + "'");
                    updatecmd1.ExecuteNonQuery();
                }





            }
            catch (FaultException<WebServiceValidationFaultContract> ex)
            {

                foreach (var item in ex.Detail._validationErrors)
                {
                    HttpContext.Current.Response.Write(item.ToString());

                }

            }
            catch (Exception ex)
            {

                HttpContext.Current.Response.Write(ex.Message);
                SqlCommand updatecmd1 = new SqlCommand();
                updatecmd1.Connection = connection;
                updatecmd1.CommandText = string.Format("INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) VALUES('1430','" + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] + "','" + AppID + "','-3','" + ex.Message + "','',GETDATE())");
                updatecmd1.ExecuteNonQuery();
            }

            client.Close();


        }
        else
        {
            HttpContext.Current.Response.Write(strErrors);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

    }

   

        private static byte getTitles(string title)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetTitles();


            var testtitle = dict.FirstOrDefault(x => x.Value == title).Key;

            return testtitle;

        }
	

		private byte getMaritalStatus(string Status)
        {

            if (Status == "Co-habiting")
            {
                Status = "Cohabiting";
            }

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetMaritalStatuses();

            var testtitle = dict.FirstOrDefault(x => x.Value == Status).Key;

            return testtitle;
        }

	private byte GetFactFindCurrentPropertyRepaymentTypes(string RepaymentType)
	{

		

		ImportServiceClient client = new ImportServiceClient();
		client.ClientCredentials.UserName.UserName = "TestUser";
		client.ClientCredentials.UserName.Password = "test";

		Dictionary<byte, string> dict = new Dictionary<byte, string>();
		dict = client.GetFactFindCurrentPropertyRepaymentTypes();

	

		var testtitle = dict.FirstOrDefault(x => x.Value == RepaymentType).Key;

		return testtitle;
	}

	private byte GetFactFindCurrentPropertyMortgageProductTypes(string MortgageType)
	{



		ImportServiceClient client = new ImportServiceClient();
		client.ClientCredentials.UserName.UserName = "TestUser";
		client.ClientCredentials.UserName.Password = "test";

		Dictionary<byte, string> dict = new Dictionary<byte, string>();
		dict = client.GetFactFindCurrentPropertyMortgageProductTypes();

		
		var testtitle = dict.FirstOrDefault(x => x.Value == MortgageType).Key;

		return testtitle;
	}

	private byte GetFactFindCurrentPropertyMortgageTypes(string Mortgage)
	{

		

		ImportServiceClient client = new ImportServiceClient();
		client.ClientCredentials.UserName.UserName = "TestUser";
		client.ClientCredentials.UserName.Password = "test";

		Dictionary<byte, string> dict = new Dictionary<byte, string>();
		dict = client.GetFactFindCurrentPropertyMortgageTypes();

	



		var testtitle = dict.FirstOrDefault(x => x.Value == Mortgage).Key;

		return testtitle;
	}


	private byte getGender(string Gender)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetGenders();
            var testtitle = dict.FirstOrDefault(x => x.Value == Gender).Key;

            return testtitle;

        }
    

        private string getLeadSourceID(string Source)
        {
            var SourceID = "";
            int IntegrationID = 10;
            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            //  Dictionary<int[], string> dict = new Dictionary<int[], string>();

            WSLeadImporterLeadsource[] dict = new WSLeadImporterLeadsource[6];
            dict = client.GetLeadSources(IntegrationID);
            //var testtitle = dict.FirstOrDefault(x => x.Id == IntegrationID).ToString();

            XmlDocument myXml = new XmlDocument();
            XPathNavigator xNav = myXml.CreateNavigator();
            XmlSerializer y = new XmlSerializer(dict.GetType());
            using (var xs = xNav.AppendChild())
            {
                y.Serialize(xs, dict);
            }

           XmlNodeList elementname = myXml.GetElementsByTagName("Description");

            for (int i = 0; i < elementname.Count; i++)
            {

                if (elementname[i].InnerXml == Source)
                {
                    SourceID = elementname[i].NextSibling.InnerXml;

                }

            }

            return SourceID;

        }


        private string getRefID(string Advisor)
        {
            var RefID = "";
            int IntegrationID = 10;
            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            //  Dictionary<int[], string> dict = new Dictionary<int[], string>();

            WSLeadImporterReferrer[] dict = new WSLeadImporterReferrer[100];
            dict = client.GetReferrers(IntegrationID);
            //var testtitle = dict.FirstOrDefault(x => x.Id == IntegrationID).ToString();

            XmlDocument myXml = new XmlDocument();
            XPathNavigator xNav = myXml.CreateNavigator();
            XmlSerializer y = new XmlSerializer(dict.GetType());
            using (var xs = xNav.AppendChild())
            {
                y.Serialize(xs, dict);
            }

			XmlNodeList elementname = myXml.GetElementsByTagName("Description");

            for (int i = 0; i < elementname.Count; i++)
            {

			
			

			if (elementname[i].InnerXml == Advisor)
                {
                    RefID = elementname[i].NextSibling.InnerXml;

                }

            }
			
		return RefID;
        }
        

        private byte getPropertyStatus(string Address)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetPropertyStatuses();
            var testtitle = dict.FirstOrDefault(x => x.Value == Address).Key;

            return testtitle;

        }

        private static byte getAppointments(string appointment)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetAppointmentTypes();



            var testtitle = dict.FirstOrDefault(x => x.Value == appointment).Key;

            return testtitle;

        }

        private static byte getResidentialStatuses(string statuses)
        {

            if (statuses == "Living in Partners Home")
            {
                statuses = "Living in partner's home";
            }
            else if (statuses == "Rented")
            {
                statuses = "Renting";
            }
            else if (statuses == "Living with Family/Friends")
            {
                statuses = "Living with family";
            }
            else if (statuses == "Living With Parents")
            {
                statuses = "Living with family";
            }
            else if (statuses == "Homeowner")
            {
                statuses = "Homeowner";
            }

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetResidentialStatuses();

            var testtitle = dict.FirstOrDefault(x => x.Value == statuses).Key;

            return testtitle;

        }

    
        private static byte getEmploymentStatuses(string statuses)
        {

            if (statuses == "Full Time Permanent")
            {
                statuses = "Full time permanent";
            }
            else if (statuses == "Part Time Permanent")
            {
                statuses = "Part time permanent";
            }
            else if (statuses == "Self-Employed")
            {
                statuses = "Self-employed";
            }
            else if (statuses == "House Person")
            {
                statuses = "Houseperson";
            }

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetEmploymentStatuses();


            var testtitle = dict.FirstOrDefault(x => x.Value == statuses).Key;

            return testtitle;

        }

        private static byte getEmploymentIncomeTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetEmploymentIncomeTypes();

            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte? getEmploymentOtherIncomeTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetEmploymentOtherIncomeTypes();

            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;
           
            return testtitle;


        }

        private static byte getSourceofDeposit(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetSourceOfDepositTypes();

             var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetPurchaseTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetPurchaseTypes();

            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetPurchaseSubTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetPurchaseSubTypes();
            
            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetMostImportantTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetMostImportantTypes();

            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

		private static int GetFactFindCurrentPropertyLenders(string lendername)
		{

			ImportServiceClient client = new ImportServiceClient();
			client.ClientCredentials.UserName.UserName = "TestUser";
			client.ClientCredentials.UserName.Password = "test";

			Dictionary<int, string> dict = new Dictionary<int, string>();
			dict = client.GetFactFindCurrentPropertyLenders();

			foreach (var pair in dict)
			{
			HttpContext.Current.Response.Write(pair.Key + ":" +
					pair.Value.ToString() + Environment.NewLine);
			}
			HttpContext.Current.Response.End();


		var testtitle = dict.FirstOrDefault(x => x.Value == lendername).Key;

			return testtitle;

		}


	//private static int GetLeadSources(int Integration)
	//{
	//    int LeadSource = 0;


	//    ImportServiceClient client = new ImportServiceClient();
	//    client.ClientCredentials.UserName.UserName = "TestUser";
	//    client.ClientCredentials.UserName.Password = "test";


	//    return LeadSource;
	//}

	//private static int GetReferrers(int Integration)
	//{

	//    ImportServiceClient client = new ImportServiceClient();
	//    client.ClientCredentials.UserName.UserName = "TestUser";
	//    client.ClientCredentials.UserName.Password = "test";

	//    int Lead = 0;


	//    return Lead;
	//}





	private static byte GetRepaymentPlanTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetRepaymentPlanTypes();


            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetRemortgageReasonTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetRemortgageReasonTypes();

            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static short GetValuationOptionTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<short, string> dict = new Dictionary<short, string>();
            dict = client.GetValuationOptionTypes();


            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        

        private static byte GetNotPayingOff(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetIntoRetirementTypes();


            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetFactFindPersonalFinanceAssetTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetFactFindPersonalFinanceAssetTypes();


            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte GetEmploymentOtherIncomeTypes(string type)
        {

            ImportServiceClient client = new ImportServiceClient();
            client.ClientCredentials.UserName.UserName = "TestUser";
            client.ClientCredentials.UserName.Password = "test";

            Dictionary<byte, string> dict = new Dictionary<byte, string>();
            dict = client.GetEmploymentOtherIncomeTypes();


            var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

            return testtitle;

        }

        private static byte getContactType(string type)
        {

            byte TypeID = 0;

            if (type == "Home Number")
            {
                TypeID = 1;
            }
            else if (type == "Mobile Number")
            {
                TypeID = 16;
            }
            else if (type == "Work Number")
            {
                TypeID = 2;
            }
            else if (type == "Email Address")
            {
                TypeID = 5;
            }
            else if (type == "Not Known")
            {
                TypeID = 3;
            }

            else if (type == "New Home Number")
            {
                TypeID = 4;
            }
            else if (type == "Fax")
            {
                TypeID = 14;
            }
            else if (type == "Twitter")
            {
                TypeID = 17;
            }
            else if (type == "Facebook")
            {
                TypeID = 18;
            }
            else if (type == "Google+")
            {
                TypeID = 19;
            }
            else if (type == "LinkedIn")
            {
                TypeID = 20;
            }
            else if (type == "Other")
            {
                TypeID = 15;
            }

            return TypeID;

        }


        private static List<FactFindAllocatedClient> getAllocation(int AppNo, int MidasProClientID, int App2MidasProClientID)
        {
            List<FactFindAllocatedClient> AllocatedClients = new List<FactFindAllocatedClient>();

            var AllClient = new FactFindAllocatedClient();
            var AllClient2 = new FactFindAllocatedClient();

            if (AppNo == 1)
            {


                AllClient.FactFindClientId = MidasProClientID;
                AllClient.Allocated = true;
                AllocatedClients.Add(AllClient);
               
                
            }
            else if(AppNo == 2) {
                AllClient.FactFindClientId = MidasProClientID;
                AllClient.Allocated = false;
                AllocatedClients.Add(AllClient);

                AllClient2.FactFindClientId = App2MidasProClientID;
                AllClient2.Allocated = true;
                AllocatedClients.Add(AllClient2);
            }
            else if (AppNo == 3)
            {
                AllClient.FactFindClientId = MidasProClientID;
                AllClient.Allocated = true;
                AllocatedClients.Add(AllClient);

                AllClient2.FactFindClientId = App2MidasProClientID;
                AllClient2.Allocated = true;
                AllocatedClients.Add(AllClient2);
            }
            else
            {
                AllClient.FactFindClientId = MidasProClientID;
                AllClient.Allocated = true;
                AllocatedClients.Add(AllClient);

             

            }


            return AllocatedClients;



        }



    //private static byte getResStatus()
    //{

    //    ImportServiceClient client = new ImportServiceClient();
    //    client.ClientCredentials.UserName.UserName = "TestUser";
    //    client.ClientCredentials.UserName.Password = "test";

    //    Dictionary<byte, string> dict = new Dictionary<byte, string>();
    //    dict = client.GetResidentialStatuses();

    //    //foreach(KeyValuePair<byte, string> kvp in dict){

    //    //    HttpContext.Current.Response.Write(kvp);

    //    //}
    //    //HttpContext.Current.Response.End();

    //    //var testtitle = dict.FirstOrDefault(x => x.Value == type).Key;

    //    return 0;

    //}

}

