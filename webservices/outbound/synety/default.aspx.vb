﻿Imports Config, Common
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class ClickToDial
    Inherits System.Web.UI.Page

    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private strTelephoneTo As String = HttpContext.Current.Request("TelephoneTo")
    Private UserSessionID As String = HttpContext.Current.Request("UserSessionID")
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strClickToDialProviderURL As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""
    Private strUserID As String = "", strUserName As String = ""

'    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
'        Dim exc As Exception = Server.GetLastError
'        Dim excMessage As String = "", excInnerException As String = ""
'        If Not exc Is Nothing Then
'            excMessage = exc.Message.ToString
'            If Not exc.InnerException Is Nothing Then
'                excInnerException = exc.InnerException.Message.ToString
'            End If
'        Else
'            excMessage = "No error message"
'        End If
'        HttpContext.Current.Response.Clear()
'        HttpContext.Current.Response.Write("2|An unknown error has occurred")
'        'sendMail("Click to Dial Make Call Error", "Err14: An unknown response has been received from " & strClickToDialProviderURL & " in synety/default.aspx.<br />" & excMessage & "<br />" & excInnerException)
'        HttpContext.Current.Response.End()
'    End Sub

    Private Enum ActionType
        makeCall = 1
        endCall = 2
    End Enum

    Public Sub clickToDial()
        strClickToDialProviderURL = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "ClickToDialProviderURL")
        strUserID = getAnyFieldByCompanyID("UserID", "tblusers", "UserSessionID", UserSessionID)
        If checkValue(strClickToDialProviderURL) Then
            Select Case intActionType
                Case ActionType.makeCall
                    'responseWrite(1)
                    makeCall()
                Case ActionType.endCall
                    'responseWrite(2)
                    endCall()
                'Case ActionType.getCallRecording
                    'responseWrite(3)
                    'getCallRecording()
            End Select
        End If
    End Sub

    Private Sub makeCall()
        Dim strRequestURL As String = "", strClickToDialAccountNumber As String = "", strClickToDialPassword As String = "", strSynetyLicenseKey As String = "", strClickToDialDDI As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialAccountNumber" Then strClickToDialAccountNumber = Row.Item("StoredUserValue").ToString
                If Row.Item("StoredUserName").ToString = "ClickToDialPassword" Then strClickToDialPassword = Row.Item("StoredUserValue").ToString
                If Row.Item("StoredUserName").ToString = "ClickToDialDDI" Then strClickToDialDDI = Row.Item("StoredUserValue").ToString
            Next
        End If
        strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Synety%' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "SynetyLicenseKey") Then strSynetyLicenseKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If (Not checkValue(strClickToDialPassword) Or Not checkValue(strClickToDialAccountNumber) Or strClickToDialDDI = "0" Or Not checkValue(strClickToDialDDI) Or Not checkValue(strTelephoneTo) Or strTelephoneTo = "0") Then ' Error in data before sending
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("2|An invalid CLI, CLD or Account Number was provided")
            sendMail("Click to Dial Make Call Error", "An invalid CLI, CLD, Password or Account Number was provided")
        Else
            strRequestURL = strClickToDialProviderURL & "/accounts/" & strClickToDialAccountNumber & "/Calls/"
            Dim strJSONPost As String = "{'CLI':'" & strClickToDialDDI & "','CLD':'" & strTelephoneTo & "','AccountID':'" & strClickToDialAccountNumber & "'}"
            Dim objCall As HttpWebResponse = postSynetyWebRequest(strRequestURL, strClickToDialAccountNumber, strClickToDialPassword, strSynetyLicenseKey, strJSONPost)
            Dim objReader As New StreamReader(objCall.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            Dim strSessionID As String = regexFirstMatch("""SessionID"":""([a-z0-9\-]{1,64})""", strResponse)
            updateUserStoreField(strUserID, "ClickToDialSessionID", strSessionID, "", "")
            objReader.Close()
            objReader = Nothing
            'responseWrite(strResponse & "<br><br>")
            'responseWrite("SessionID=" & strSessionID & "<br>")
            'responseEnd()
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("0|Success")
        End If
    End Sub

    Private Sub endCall()
        Dim strRequestURL As String = "", strClickToDialAccountNumber As String = "", strClickToDialPassword As String = "", strSynetyLicenseKey As String = "", strSessionID As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialAccountNumber" Then strClickToDialAccountNumber = Row.Item("StoredUserValue").ToString
                If Row.Item("StoredUserName").ToString = "ClickToDialPassword" Then strClickToDialPassword = Row.Item("StoredUserValue").ToString
                If Row.Item("StoredUserName").ToString = "ClickToDialSessionID" Then strSessionID = Row.Item("StoredUserValue").ToString
            Next
        End If
        strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Synety%' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "SynetyLicenseKey") Then strSynetyLicenseKey = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If (Not checkValue(strClickToDialAccountNumber)) Then ' Error in data before sending
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("2|An error has occurred")
            sendMail("Click to Dial End Call Error", "Err15: A default value is not present in synety/default.aspx: " & strErrorMessage)
        Else
            strRequestURL = strClickToDialProviderURL & "/accounts/" & strClickToDialAccountNumber & "/Calls/CloseCall?SessionID=" & strSessionID
            Dim strJSONPost As String = "{'AccountID':'" & strClickToDialAccountNumber & "'}"
            Dim objCall As HttpWebResponse = postSynetyWebRequest(strRequestURL, strClickToDialAccountNumber, strClickToDialPassword, strSynetyLicenseKey, strJSONPost)
            Dim objReader As New StreamReader(objCall.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing
            'responseWrite(strResponse & "<br><br>")
            'responseEnd()
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("0|Success")
        End If
    End Sub

    Private Function checkResponse(ByRef objResponse As HttpWebResponse) As Boolean
        Dim boolValid = True
        Dim excInnerException As String = ""
        Try ' Make sure the response is valid
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            If Not (objResponse.StatusCode.ToString = "OK") Then
                sendMail("Click to Dial Call Error", "Err19: An unknown status code has been received from " & strClickToDialProviderURL & " in synety/default.aspx.")
                boolValid = False
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValid = False
        End Try
        Return boolValid
    End Function

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        'getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=False&strAttachment=&UserSessionID=" & UserSessionID)
    End Sub

    Private Function postSynetyWebRequest(ByVal url As String, username As String, password As String, licenceKey As String, json As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Accept = "application/json"
                .Method = WebRequestMethods.Http.Post
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(json)
                .Headers.Add("LicenseKey: " & licenceKey)
                .Headers.Add("Username: " & username)
                .Headers.Add("Password: " & password)
                .KeepAlive = False
                .Timeout = 30000
                .ReadWriteTimeout = 30000
            End With
            'responseWrite(url & "<br>")
            'For Each Item In objRequest.Headers
            '    responseWrite(Item & "=" & objRequest.Headers(Item) & "<br>")
            'Next
            'responseWrite("post=" & json)
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(json)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function getSynetyWebRequest(ByVal url As String, username As String, password As String, licenceKey As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Get
                .Headers.Add("LicenseKey: " & licenceKey)
                .Headers.Add("Username: " & username)
                .Headers.Add("Password: " & password)
                .KeepAlive = False
                .Timeout = 30000
                .ReadWriteTimeout = 30000
            End With
            'responseWrite(url & "<br>")
            'For Each Item In objRequest.Headers
            '    responseWrite(Item & "=" & objRequest.Headers(Item) & "<br>")
            'Next
            'responseEnd()
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
