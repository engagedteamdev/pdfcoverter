﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLACF
    Inherits System.Web.UI.Page

    Private strAuthURL As String = "", strXMLURL As String = ""
    Private strUserName As String = "", strUserID As String = "", strCompanyName As String = "", boolLoggedOn As Boolean = False
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        incrementTransferAttempts(AppID, strMediaCampaignID)
        Common.postEmail("", "itsupport@engaged-solutions.co.uk", "ACF Data Error", strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub generateXml()

        'If (strEnvironment = "live") Then
        '	strXMLURL = "https://gateway.secureapplication.co.uk/vault/receive.asp?env=prod&rec=HPnew"
        'Else
        	strXMLURL = "https://gateway.secureapplication.co.uk/vaultUat/receive.asp?env=uat&rec=HPnew"
        'End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%ACF%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "ACFUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ACFUserID") Then strUserID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ACFCompanyName") Then strCompanyName = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/acf/blank.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("def", "http://www.thefundingcorporation.com/BusinessFunctionV1.0")

        writeToNode(objInputXMLDoc, "y", "def:header", "def:thisdocumentdatetime/def:datetime", ddmmyyhhmmss2ddmmmyyyy(Config.DefaultDateTime), objNSM)
        writeToNode(objInputXMLDoc, "y", "def:header", "def:from/def:partnerinformation/def:id", strUserID, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:header", "def:from/def:partnerinformation/def:name", strCompanyName, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:shortproposal", "def:proposaldate", ddmmyyhhmmss2ddmmmyyyy(Config.DefaultDateTime, False), objNSM)
        writeToNode(objInputXMLDoc, "y", "def:introducer", "def:id", strUserID, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:introducer", "def:name", strCompanyName, objNSM)
        writeToNode(objInputXMLDoc, "y", "def:additionalattribute[2]", "def:value", strCompanyName, objNSM)

        Dim strSQL As String = "SELECT * FROM vwxmlacf WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1DOB") Then
                            writeToNode(objInputXMLDoc, "y", "def:customer", "def:dateofbirth", ddmmyyhhmmss2ddmmmyyyy(Row(Column).ToString, False), objNSM)
                        ElseIf (Column.ColumnName.ToString = "App1EmployerStartDate") Then
                            writeToNode(objInputXMLDoc, "y", "def:employmentrecord/def:employment", "def:startdate", ddmmyyhhmmss2ddmmmyyyy(Row(Column).ToString, False), objNSM)
                        ElseIf (Column.ColumnName.ToString = "AddressStartDate") Then
                            writeToNode(objInputXMLDoc, "y", "def:occupancyrecord/def:occupancy", "def:movingindate", ddmmyyhhmmss2ddmmmyyyy(Row(Column).ToString, False), objNSM)
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += "def:" & arrName(y)
                                    Else
                                        strParent += "/def:" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), "def:" & arrName(UBound(arrName)), Row(Column).ToString, objNSM)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
            Dim objResponse As HttpWebResponse = postACFWebRequest(strXMLURL & "&intro=" & strUserName, objInputXMLDoc.InnerXml, "")

            If (objResponse.StatusCode.ToString = "OK") Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResponse As String = objReader.ReadToEnd()
                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objReader.ReadToEnd())
                'HttpContext.Current.Response.End()

                'If (InStr(strResponse, "TFCRepositoryFunction") > 0) Then
                objOutputXMLDoc.LoadXml(strResponse)
                objReader.Close()
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                ' Parse the XML document.
                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//TFCRepositoryFunction")
                Dim strStatus As String = objApplication.SelectSingleNode("//Message").InnerText
                Select Case strStatus
                    Case "Application Received"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by ACF"))
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To ACF", objInputXMLDoc.InnerXml)
						saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Received From ACF", objOutputXMLDoc.InnerXml)
                    Case "Previously received"
                        HttpContext.Current.Response.Clear()
                        'saveStatus(AppID, Config.DefaultUserID, "INV", "DUP")
                        saveUpdatedDate(AppID, Config.DefaultUserID, "PartnerDuplicate")
                        saveNote(AppID, Config.DefaultUserID, "Application Declined by ACF: Duplicate")
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by ACF: Duplicate"))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To ACF: Duplicate", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Received From ACF: Duplicate", objOutputXMLDoc.InnerXml)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                    Case Else
                        saveNote(AppID, Config.DefaultUserID, "Application Declined by ACF: " & strStatus)
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by ACF: " & strStatus))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Generated To ACF", objInputXMLDoc.InnerXml)
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Received From ACF: " & strStatus, objOutputXMLDoc.InnerXml)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select
                'Else
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by ACF: " & strResponse))
                'objReader.Close()
                'objReader = Nothing
                'End If
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If
            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Function postACFWebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .Headers.Add("SOAPAction", soapaction)
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            '            For Each item In objRequest.Headers
            '                responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            '            Next
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function ddmmyyhhmmss2ddmmmyyyy(ByVal tm As String, Optional ByVal includeTime As Boolean = True) As String
        Dim strDateTime As String = padZeros(Day(tm), 2) & "-" & MonthName(Month(tm), True) & "-" & Year(tm)
        If (includeTime) Then
            strDateTime += " " & TimeValue(tm)
        End If
        Return strDateTime
    End Function

End Class
