﻿Imports Config, Common
Imports System.Net

Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strTelephoneTo As String = HttpContext.Current.Request("frmTelephoneTo")
    Private strSMSBody As String = HttpContext.Current.Request("frmSMSBody")
    Private strSMSFrom As String = "", intEmailProfileID As String = HttpContext.Current.Request("frmEmailProfileID")
    Private strUserName As String = ""

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        Dim exc As Exception = Server.GetLastError
        Dim excMessage As String = "" ' exc.Message.ToString
        Dim excInnerException As String = "" ' exc.InnerException.Message.ToString

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Write("2|An unknown error has occured|0")
        'sendMail("SMS Error", "An error has occured in sms.aspx. The response from " & objLeadPlatform.Config.SMSProvider & " is unknown.<br />" & excMessage & "<br />" & excInnerException)
        HttpContext.Current.Response.End()
    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
    End Sub

    Public Sub sendSMS()
        Dim intNumMessages As Integer = 1
        If (Common.checkValue(objLeadPlatform.Config.SMSProviderMaxSMSLength) And objLeadPlatform.Config.SMSProviderMaxSMSLength > 0 And Len(strSMSBody) > objLeadPlatform.Config.SMSProviderMaxSMSLength) Then
            If (objLeadPlatform.Config.SMSProviderConcatenateSMS = "Y") Then
                If (Len(strSMSBody) > objLeadPlatform.Config.SMSProviderMaxSMSLength) Then
                    intNumMessages = Math.Ceiling(Len(strSMSBody) / objLeadPlatform.Config.SMSProviderMaxSMSLength)
                End If
            Else
                strSMSBody = Left(strSMSBody, objLeadPlatform.Config.SMSProviderMaxSMSLength)
            End If
        End If
		
		If (checkValue(intEmailProfileID)) Then
			strSMSFrom = getAnyField("EmailProfileEmailAddress", "tblemailprofiles", "EmailProfileID", intEmailProfileID)
		End If
		If (Not checkValue(strSMSFrom)) Then
			strSMSFrom = "EngagedCRM"
		End If
        'sendMail("SMS", Config.ApplicationURL & "/webservices/outbound/" & objLeadPlatform.Config.SMSProvider & "/?frmTelephoneTo=" & strTelephoneTo & "&frmSMSBody=" & strSMSBody & "&frmNumMessages=" & intNumMessages & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
		'responseWrite(Config.ApplicationURL & "/webservices/outbound/" & objLeadPlatform.Config.SMSProvider & "/?AppID=" & AppID & "&frmTelephoneTo=" & strTelephoneTo & "&frmSMSFrom=" & strSMSFrom & "&frmSMSBody=" & strSMSBody & "&frmNumMessages=" & intNumMessages & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
		'responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/outbound/" & objLeadPlatform.Config.SMSProvider & "/", "AppID=" & AppID & "&frmTelephoneTo=" & strTelephoneTo & "&frmSMSFrom=" & strSMSFrom & "&frmSMSBody=" & strSMSBody & "&frmNumMessages=" & intNumMessages & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            Dim arrResponse = Split(strResponse, "|")
            Select Case arrResponse(0)
                Case "0" ' Success
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("0|Success|" & arrResponse(2))
                    incrementSystemConfigurationField("SMSNumberSent", 1)
                Case "1" ' Display user message
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("1|" & arrResponse(1) & "|0")
                Case Else ' Failure
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("2|An unknown error has occured|0")
                    'sendMail("SMS", "An error has occured in sms.aspx. The response from " & objLeadPlatform.Config.SMSProvider & " is unknown.")
            End Select
            objReader.Close()
            objReader = Nothing
        End If
        If (checkResponse(objResponse, "")) Then
            objResponse.Close()
        End If
        objResponse = Nothing
    End Sub

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&strAttachment=&UserSessionID=" & UserSessionID)
    End Sub

End Class
