﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="default.aspx.vb" Inherits="XMLPrecise" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- Styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="/css/bootstrap-overrides.css" rel="stylesheet">

    <link href="/css/ui-lightness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
    <link href="/js/plugins/msgGrowl/css/msgGrowl.css" rel="stylesheet">
    <link href="/js/plugins/msgAlert/css/msgAlert.css" rel="stylesheet">
    <link href="/js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet">

    <link href="/css/slate.css" rel="stylesheet">
    <link href="/css/slate-responsive.css" rel="stylesheet">

    <link href="/css/pages/dashboard.css" rel="stylesheet">
    <link href="/css/pages/reports.css" rel="stylesheet">

    <!-- Javascript -->
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins/msgGrowl/js/msgGrowl.js"></script>
    <script src="/js/plugins/msgAlert/js/msgAlert.js"></script>
    <script src="/js/plugins/lightbox/jquery.lightbox.js"></script>


    <script src="/js/Slate.js"></script>

    <script src="/js/general.js"></script>
    <script src="/js/crm.js"></script>
    <script src="/js/reminders.aspx"></script>
    <script src="/js/messages.aspx"></script>
    <script src="/js/ticker.aspx"></script>

    <script src="/js/applicationform.js"></script>
    <script src="/js/processing.aspx"></script>
    <script src="/js/bridgingform.js"></script>
    
</head>

<body class="no-bg">

	<% If(strSelectPlan = "Y") Then %>
	<table border="0" cellpadding="" cellspacing="" class="table sortable table-bordered table-striped table-highlight" width="100%">
    <thead>
        <tr class="tablehead">
        	<th class="smlc sort-alpha">Product Code</th>
            <th class="smlc sort-alpha">Product Name</th>
            <th class="smlc sort-alpha">Monthly Payment</th>
            <th class="smlc sort-alpha">Interest Rate</th>
            <th class="smlc sort-alpha"></th>
        </tr>
    </thead>
            <%  generateXml()%>
    </table>
    <% Else %>
   		 <%  generateXml()%>
    <% End If %>


</body>

</html>