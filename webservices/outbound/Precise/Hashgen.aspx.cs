using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;


namespace sha384_key
{
    public partial class Hashgen : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Main();
        }

        public void Main()
        {

            string preciseViewSystemUserName = "PositiveAPI"; //The API Key is case sensitive 
            string preciseViewSystemUserApiKey = "eDDvnVSDnoUcAZp9JIX6YbKss4iBsb";
            string requestDateTime = DateTime.UtcNow.ToString("o");
            string hash = CalculatePreciseViewHash(preciseViewSystemUserName, preciseViewSystemUserApiKey, requestDateTime);

        }



        private static string CalculatePreciseViewHash(string preciseViewSystemUserName, string preciseViewSystemUserApiKey, string requestDateTime)
        {
            using (HashAlgorithm hash = new SHA384Managed())
            {
                string plainText = string.Concat(preciseViewSystemUserName.ToUpperInvariant(), preciseViewSystemUserApiKey, requestDateTime);
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                byte[] hashBytes = hash.ComputeHash(plainTextBytes);
                return Convert.ToBase64String(hashBytes);
            }
        }
    }
}