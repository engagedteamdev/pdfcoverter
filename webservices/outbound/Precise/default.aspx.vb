﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System
Imports System.Security
Imports System.Security.Cryptography
Imports System.Text
Imports System.Globalization
Imports ServiceReferencePreciseDocument
Imports ServiceReferencePreciseDocument.CaesiumDocApiClient
Imports ServiceReference1NEWMCD.CaesiumApiClient
Imports ServiceReference1NEWMCD
Imports ServiceReferencePreciseCreateLoan
Imports ServiceReferencePreciseCreateLoan.CaesiumApiClient
Imports ceTe.DynamicPDF.Merger
Imports System.Web.Caching
Imports System.Diagnostics




Partial Class XMLPrecise
    Inherits System.Web.UI.Page

    Private strUserID As String = "", strPassword As String = "", strXMLURL As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
	Private CompanyID As String = HttpContext.Current.Request("CompanyID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
	
    Public strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
	Public strCreateLoan As String = HttpContext.Current.Request.QueryString("CreateLoan")
	Public strProductCode As String = HttpContext.Current.Request.QueryString("ProductCode")
	Public strMinValue As String = HttpContext.Current.Request.QueryString("MinValue")
	Public strRate As String = HttpContext.Current.Request.QueryString("Rate")
	Public strProductName As String = HttpContext.Current.Request.QueryString("ProductName")
	Public strSelectPlan As String = HttpContext.Current.Request.QueryString("SelectPlan")
	
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strErrorMessage As String = ""
    Private strDocument As String = HttpContext.Current.Request("Document")
    'Private objPDF As MergeDocument = Nothing
    Private objLeadPlatform As LeadPlatform = New LeadPlatform, objCache As Cache


    Public Sub generateXml()
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
		
        If (strEnvironment = "live") Then
            strXMLURL = "https://mcdview.precisemortgages.co.uk"
        Else
            strXMLURL = "https://mcdview.precisemortgages.co.uk"
        End If

        If (strDocument = "Y") Then
            getDocument()
        Else If(strCreateLoan = "Y")
			createLoanApp(strProductCode, strMinValue, strRate)
		Else 
            sendXML()
        End If
		
		
		

    End Sub

    Private Sub sendXML()
        Dim App2Title As String = ""
        If (checkValue(getAnyField("App2Title", "tblapplications", "AppID", AppID))) Then
            App2Title = getAnyField("App2Title", "tblapplications", "AppID", AppID)

        End If

        objInputXMLDoc.XmlResolver = Nothing
        If (App2Title <> "") Then
            objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/Precise/blankaffapp2.xml"))
        Else
            objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/Precise/blankaff.xml"))
        End If
		
        Dim configTest As String = "Y"
        Dim strConntectionString = ""
        If (configTest = "Y") Then
            strConntectionString = ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringNew").ConnectionString
        Else
            strConntectionString = ConfigurationManager.ConnectionStrings("crm.live.ConnectionString").ConnectionString
        End If

        Dim strSQL As String = "SELECT * FROM vwxmlPrecise WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
		'HttpContext.Current.Response.Write(strSQL)
        'HttpContext.Current.Response.End()
		
		 Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim strTitle As String = Column.ColumnName.ToString = "Applicants/Applicant/Title1"
                        If (strTitle <> "False") Then

                            'createNewElement("Applicants", "Applicant2")
                            'createNewElement("Applicant2", "Title1")
                            'createNewElement("Applicant2", "FirstName1")
                            'createNewElement("Applicant2", "MiddleName1")
                            'createNewElement("Applicant2", "Surname1")
                            'createNewElement("Applicant2", "DateOfBirth1")
                            ''createNewElement("Applicant2", "MaritalStatus1")
                            ''createNewElement("Applicant2", "HomeTelephoneNumber1")							
                            ''createNewElement("Applicant2", "OfficeTelephoneNumber1")
                            ''createNewElement("Applicant2", "MobileTelephoneNumber1")
                            ''createNewElement("Applicant2", "EmailAddress1")
                            ''createNewElement("Applicant2", "Nationality1")
                            ''createNewElement("Applicant2", "MaidenSurname1")
                            'createNewElement("Applicant2", "Income2")
                            'createNewElement("Income2", "GrossAnnualIncomeAmount1")
                            'createNewElement("Income2", "MonthlyPermanentSecondJobIncome1")
                            'createNewElement("Income2", "MonthlyOverTimeBonusCommissionAmount1")
                            'createNewElement("Income2", "MonthlyPensionAmount1")
                            'createNewElement("Income2", "MonthlyTaxCreditsAmount1")
                            'createNewElement("Income2", "MonthlyMaintenanceAmount1")
                            'createNewElement("Income2", "MonthlyRentalIncomeAmount1")
                            'createNewElement("Income2", "MonthlyBenefitsAmount1")
                            'createNewElement("Income2", "MonthlyAllowanceAmount1")
                            'createNewElement("Income2", "MonthlyInvestmentIncomeAmount1")

                            Dim oldNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2")
                            Dim newnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Applicant", oldNode.NamespaceURI)
                            newnode.InnerXml = oldNode.InnerXml
                            oldNode.ParentNode.ReplaceChild(newnode, oldNode)




                        End If

                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 1) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If

                        writeToNode("n", "Payload/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)

                    End If
                Next
            Next
        End If
        dsCache = Nothing


        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	






            '			Dim newNode As XmlNode = objInputXMLDoc.CreateElement("Applicant")
            '
            '			newNode.InnerXml = oldNode.InnerXml
            '			oldNode.ParentNode.AppendChild(newNode)
            '			objInputXMLDoc.RemoveChild(oldNode)








            ' Dim oldMaritalNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MaritalStatus1")
            '            Dim newMaritalnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MaritalStatus", oldMaritalNode.NamespaceURI)
            '            newMaritalnode.InnerXml = oldMaritalNode.InnerXml
            '            oldMaritalNode.ParentNode.ReplaceChild(newMaritalnode, oldMaritalNode)

            'Dim oldHomeTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/HomeTelephoneNumber1")
            '            Dim newHomeTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "HomeTelephoneNumber", oldHomeTelNode.NamespaceURI)
            '            newHomeTelnode.InnerXml = oldHomeTelNode.InnerXml
            '            oldHomeTelNode.ParentNode.ReplaceChild(newHomeTelnode, oldHomeTelNode)
            '
            '            Dim oldOfficeTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/OfficeTelephoneNumber1")
            '            Dim newOfficeTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "OfficeTelephoneNumber", oldOfficeTelNode.NamespaceURI)
            '            newOfficeTelnode.InnerXml = oldOfficeTelNode.InnerXml
            '            oldOfficeTelNode.ParentNode.ReplaceChild(newOfficeTelnode, oldOfficeTelNode)
            '
            '            Dim oldMobileTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MobileTelephoneNumber1")
            '            Dim newMobileTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MobileTelephoneNumber", oldMobileTelNode.NamespaceURI)
            '            newMobileTelnode.InnerXml = oldMobileTelNode.InnerXml
            '            oldMobileTelNode.ParentNode.ReplaceChild(newMobileTelnode, oldMobileTelNode)

            '  Dim oldEmailTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/EmailAddress1")
            '            Dim newEmailTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "EmailAddress", oldEmailTelNode.NamespaceURI)
            '            newEmailTelnode.InnerXml = oldEmailTelNode.InnerXml
            '            oldEmailTelNode.ParentNode.ReplaceChild(newEmailTelnode, oldEmailTelNode)
            '
            '            Dim oldNationalityNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Nationality1")
            '            Dim newNationalitynode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Nationality", oldNationalityNode.NamespaceURI)
            '            newNationalitynode.InnerXml = oldNationalityNode.InnerXml
            '            oldNationalityNode.ParentNode.ReplaceChild(newNationalitynode, oldNationalityNode)
            '
            '            Dim oldMaidenNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MaidenSurname1")
            '            Dim newMaidennode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MaidenSurname", oldMaidenNode.NamespaceURI)
            '            newMaidennode.InnerXml = oldMaidenNode.InnerXml
            '            oldMaidenNode.ParentNode.ReplaceChild(newMaidennode, oldMaidenNode)

			'Live
            Dim preciseViewSystemUserName As String = "PositiveAPIUser"
		    'UAT
			'Dim preciseViewSystemUserName As String = "PositiveAPI"
            'UAT
             'Dim preciseViewSystemUserApiKey As String = "eDDvnVSDnoUcAZp9JIX6YbKss5iBsb"
            'LIVE
            Dim preciseViewSystemUserApiKey As String = "ljDHWxpNxAO1rgYVJ7qRhjhzM9nTrt"
            Dim RequestDate As String = DateTime.UtcNow.ToString("o")

            Dim RepaymentType As String = getAnyFieldFromDataStore("RepaymentType", AppID)


            If (RepaymentType = "InterestOnly") Then
                RepaymentType = "Interest Only"
            Else
                RepaymentType = "Capital & Interest"
            End If
			
			


            Dim App2FirstName As String = ""
            Dim App2MiddleNames As String = ""
            Dim App2Surname As String = ""
            Dim App2DOB As Date
            Dim reformatted As String = ""
            Dim App2AnnualIncome As String = ""
            Dim App2MonthylSecondJob As String = ""
            Dim App2MonthylRentalIncome As String = ""
            Dim App2CommissionPA As Decimal = 0.0
            Dim App2BonusPa As Decimal = 0.0
            Dim App1OvertimePA As Decimal = 0.0
            Dim OverTimeBonusCommissionAmount As Decimal = 0.0
            Dim App2StatePensionsPA As Decimal = 0.0
            Dim App2PrivatePensionsPA As Decimal = 0.0
            Dim MonthlyPensionAmount As Decimal = 0.0
            Dim App2ChildTaxCredit As Decimal = 0.0
            Dim App2WorkingTaxCredits As Decimal = 0.0
            Dim MonthlyTaxCreditsAmount As Decimal = 0.0
            Dim App2MaintenanceCSA As Decimal = 0.0
            Dim MonthlyMaintenanceAmount As Decimal = 0.0
            Dim App2AnnBenTotal As Decimal = 0.0
            Dim App2IncapacityBenefit As Decimal = 0.0
            Dim MonthlyBenefitsAmount As Decimal = 0.0
            Dim App2CarersAllowance As Decimal = 0.0
            Dim App2SevereDisAllowance As Decimal = 0.0
            Dim App2DisAllowance As Decimal = 0.0
            Dim MonthlyAllowanceAmount As Decimal = 0.0
            Dim App2InvestmentPA As Decimal = 0.0
            Dim MonthlyInvestmentIncomeAmount As Decimal = 0.0

            objInputXMLDoc.SelectSingleNode("Payload/SecurityProperty/FirstCharge/RepaymentType").InnerText = RepaymentType
            objInputXMLDoc.SelectSingleNode("Payload/SecondCharge/RepaymentType").InnerText = RepaymentType
			
			

            'If (checkValue(getAnyFieldFromDataStore("App1RentalInPA", AppID))) Then
            '    Dim RentalIncome As String = getAnyFieldFromDataStore("App1RentalInPA", AppID)
            '    Dim MonthlyRentalIncome As Integer = RentalIncome / 12
            '    objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/Income/MonthlyRentalIncomeAmount").InnerText = MonthlyRentalIncome
            'End If
            'If (checkValue(getAnyFieldFromDataStore("RentAfterFees", AppID))) Then
            '    Dim SecuirtyMonthlyRentalIncome As String = getAnyFieldFromDataStore("RentAfterFees", AppID)
            '    objInputXMLDoc.SelectSingleNode("Payload/SecurityProperty/MonthlyRentalIncomeAmount").InnerText = SecuirtyMonthlyRentalIncome
            'End If

            If (checkValue(getAnyField("App2FirstName", "tblapplications", "AppID", AppID))) Then
                App2FirstName = getAnyField("App2FirstName", "tblapplications", "AppID", AppID)
            End If
            If (checkValue(getAnyField("App2Surname", "tblapplications", "AppID", AppID))) Then
                App2Surname = getAnyField("App2Surname", "tblapplications", "AppID", AppID)
            End If
			'HttpContext.Current.Response.Write(App2Title)
            'HttpContext.Current.Response.End()

            If (App2Title <> "") Then
                If (checkValue(getAnyField("App2Title", "tblapplications", "AppID", AppID))) Then
                    App2Title = getAnyField("App2Title", "tblapplications", "AppID", AppID)

                End If
                If (checkValue(getAnyField("App2MiddleNames", "tblapplications", "AppID", AppID))) Then
                    App2MiddleNames = getAnyField("App2MiddleNames", "tblapplications", "AppID", AppID)
                End If
                If (checkValue(getAnyField("App2DOB", "tblapplications", "AppID", AppID))) Then
                    App2DOB = getAnyField("App2DOB", "tblapplications", "AppID", AppID)
                    reformatted = App2DOB.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)
                End If
                If (checkValue(getAnyFieldFromDataStore("App2AnnualIncome", AppID))) Then
                    App2AnnualIncome = getAnyFieldFromDataStore("App2AnnualIncome", AppID)
                End If
                If (checkValue(getAnyFieldFromDataStore("App2SecondJobIncome", AppID))) Then
                    App2MonthylSecondJob = getAnyFieldFromDataStore("App2SecondJobIncome", AppID)
                End If
                If (checkValue(getAnyFieldFromDataStore("App2RentaPA", AppID))) Then
                    App2MonthylRentalIncome = getAnyFieldFromDataStore("App2RentaPA", AppID)
                End If
                If (checkValue(getAnyFieldFromDataStore("App2CommissionPA", AppID)) And checkValue(getAnyFieldFromDataStore("App2BonusPa", AppID)) And checkValue(getAnyFieldFromDataStore("App2OvertimePA", AppID))) Then
                    App2CommissionPA = getAnyFieldFromDataStore("App2CommissionPA", AppID)
                    App2BonusPa = getAnyFieldFromDataStore("App2BonusPa", AppID)
                    App1OvertimePA = getAnyFieldFromDataStore("App2OvertimePA", AppID)
                    OverTimeBonusCommissionAmount = (App2CommissionPA + App2BonusPa + App1OvertimePA) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2StatePensionsPA", AppID)) And checkValue(getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID))) Then
                    App2StatePensionsPA = getAnyFieldFromDataStore("App2StatePensionsPA", AppID)
                    App2PrivatePensionsPA = getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID)
                    MonthlyPensionAmount = (App2StatePensionsPA + App2PrivatePensionsPA) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2ChildTaxCredit", AppID)) And checkValue(getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID))) Then
                    App2ChildTaxCredit = getAnyFieldFromDataStore("App2ChildTaxCredit", AppID)
                    App2WorkingTaxCredits = getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID)
                    MonthlyTaxCreditsAmount = (App2ChildTaxCredit + App2WorkingTaxCredits) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2MaintenanceCSA", AppID))) Then
                    App2MaintenanceCSA = getAnyFieldFromDataStore("App2MaintenanceCSA", AppID)
                    MonthlyMaintenanceAmount = (App2MaintenanceCSA) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2AnnBenTotal", AppID)) And checkValue(getAnyFieldFromDataStore("App2IncapacityBenefit", AppID))) Then
                    App2AnnBenTotal = getAnyFieldFromDataStore("App2AnnBenTotal", AppID)
                    App2IncapacityBenefit = getAnyFieldFromDataStore("App2IncapacityBenefit", AppID)
                    MonthlyBenefitsAmount = (App2AnnBenTotal + App2IncapacityBenefit) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2CarersAllowance", AppID)) And checkValue(getAnyFieldFromDataStore("App2SevereDisAllowance", AppID)) And checkValue(getAnyFieldFromDataStore("App2DisAllowance", AppID))) Then
                    App2CarersAllowance = getAnyFieldFromDataStore("App2CarersAllowance", AppID)
                    App2SevereDisAllowance = getAnyFieldFromDataStore("App2SevereDisAllowance", AppID)
                    App2DisAllowance = getAnyFieldFromDataStore("App2DisAllowance", AppID)
                    MonthlyAllowanceAmount = (App2CarersAllowance + App2SevereDisAllowance + App2DisAllowance) / 12
                End If
                If (checkValue(getAnyFieldFromDataStore("App2InvestmentPA", AppID))) Then
                    App2InvestmentPA = getAnyFieldFromDataStore("App2InvestmentPA", AppID)
                    MonthlyInvestmentIncomeAmount = (App2InvestmentPA) / 12
                End If
                Dim objApplicant1 As XmlElement = objInputXMLDoc.SelectSingleNode("//Applicant[1]")
                Dim objApplicant2 As XmlNode = objApplicant1.CloneNode(True)

                'objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Title1").InnerText = App2Title
                'objInputXMLDoc.SelectSingleNode("//Applicant/FirstName1").InnerText = App2FirstName
                ''                objInputXMLDoc.SelectSingleNode("//Applicant/MiddleName1").InnerText = App2MiddleNames
                'objInputXMLDoc.SelectSingleNode("//Applicant/Surname1").InnerText = App2Surname
                'objInputXMLDoc.SelectSingleNode("//Applicant/DateOfBirth1").InnerText = reformatted
                ''objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/GrossAnnualIncomeAmount1").InnerText = 1.00
                ''                                
                ''                     
                ''   



                Dim oldTitleNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Title1")
                Dim newTitlenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Title", oldTitleNode.NamespaceURI)
                newTitlenode.InnerXml = oldTitleNode.InnerXml
                oldTitleNode.ParentNode.ReplaceChild(newTitlenode, oldTitleNode)

                Dim oldFirstNameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/FirstName1")
                Dim newFirstNamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "FirstName", oldFirstNameNode.NamespaceURI)
                newFirstNamenode.InnerXml = oldFirstNameNode.InnerXml
                oldFirstNameNode.ParentNode.ReplaceChild(newFirstNamenode, oldFirstNameNode)

                Dim oldMiddleNameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MiddleName1")
                Dim newMiddleNamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MiddleName", oldMiddleNameNode.NamespaceURI)
                newMiddleNamenode.InnerXml = oldMiddleNameNode.InnerXml
                oldMiddleNameNode.ParentNode.ReplaceChild(newMiddleNamenode, oldMiddleNameNode)

                Dim oldSurnameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Surname1")
                Dim newSurnamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Surname", oldSurnameNode.NamespaceURI)
                newSurnamenode.InnerXml = oldSurnameNode.InnerXml
                oldSurnameNode.ParentNode.ReplaceChild(newSurnamenode, oldSurnameNode)

                Dim oldDOBNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/DateOfBirth1")
                Dim newDOBnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "DateOfBirth", oldDOBNode.NamespaceURI)
                newDOBnode.InnerXml = oldDOBNode.InnerXml
                oldDOBNode.ParentNode.ReplaceChild(newDOBnode, oldDOBNode)

            End If
			
			

            If (App2Title <> "") Then
                'If (IsDBNull(getAnyFieldFromDataStore("App2AnnualIncome", AppID)) Or (getAnyFieldFromDataStore("App2AnnualIncome", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/GrossAnnualIncomeAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2CommissionPA", AppID)) Or (getAnyFieldFromDataStore("App2CommissionPA", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2BonusPa", AppID)) Or (getAnyFieldFromDataStore("App2BonusPa", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2OvertimePA", AppID)) Or (getAnyFieldFromDataStore("App2OvertimePA", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyOverTimeBonusCommissionAmount1").InnerText = 1.0
                'End If


                'If ((IsDBNull(getAnyFieldFromDataStore("App2CarersAllowance", AppID)) Or (getAnyFieldFromDataStore("App2CarersAllowance", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2SevereDisAllowance", AppID)) Or (getAnyFieldFromDataStore("App2SevereDisAllowance", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2DisAllowance", AppID)) Or (getAnyFieldFromDataStore("App2DisAllowance", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyAllowanceAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2StatePensionsPA", AppID)) Or (getAnyFieldFromDataStore("App2StatePensionsPA", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID)) Or (getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyPensionAmount1").InnerText = 1.0
                'End If


                'If (IsDBNull(getAnyFieldFromDataStore("App2InvestmentPA", AppID)) Or (getAnyFieldFromDataStore("App2InvestmentPA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyInvestmentIncomeAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2ChildTaxCredit", AppID)) Or (getAnyFieldFromDataStore("App2ChildTaxCredit", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID)) Or (getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyTaxCreditsAmount1").InnerText = 1.0
                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2MaintenanceCSA", AppID)) Or (getAnyFieldFromDataStore("App2MaintenanceCSA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyMaintenanceAmount1").InnerText = 1.0
                'End If

                'If (((IsDBNull(getAnyFieldFromDataStore("App2AnnBenTotal", AppID))) Or (getAnyFieldFromDataStore("App2AnnBenTotal", AppID) = "")) And ((IsDBNull(getAnyFieldFromDataStore("App2IncapacityBenefit", AppID))) Or (getAnyFieldFromDataStore("App2AnnBenTotal", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyBenefitsAmount1").InnerText = 1.0

                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2RentaPA", AppID)) Or (getAnyFieldFromDataStore("App2RentaPA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyRentalIncomeAmount1").InnerText = 1.0
                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2SecondJobIncome", AppID)) Or (getAnyFieldFromDataStore("App2SecondJobIncome", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2/MonthlyPermanentSecondJobIncome1").InnerText = 1.0
                'End If






                Dim oldIncomeNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2")
                Dim newIncomenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Income", oldIncomeNode.NamespaceURI)
                newIncomenode.InnerXml = oldIncomeNode.InnerXml
                oldIncomeNode.ParentNode.ReplaceChild(newIncomenode, oldIncomeNode)


                Dim oldGrossIncomeNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/GrossAnnualIncomeAmount1")
                Dim newGrossIncomenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "GrossAnnualIncomeAmount", oldGrossIncomeNode.NamespaceURI)
                newGrossIncomenode.InnerXml = oldGrossIncomeNode.InnerXml
                oldGrossIncomeNode.ParentNode.ReplaceChild(newGrossIncomenode, oldGrossIncomeNode)




                'Dim oldBonusNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyOverTimeBonusCommissionAmount1")
                'Dim newBonusnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyOverTimeBonusCommissionAmount", oldBonusNode.NamespaceURI)
                'objInputXMLDoc.CreateAttribute("xsi", "nil", "http://www.w3.org/2001/XMLSchema-instance")
                'newBonusnode.InnerXml = oldBonusNode.InnerXml
                'oldBonusNode.ParentNode.ReplaceChild(newBonusnode, oldBonusNode)


                'Dim oldPensionNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyPensionAmount1")
                'Dim newPensionnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyPensionAmount", oldPensionNode.NamespaceURI)
                'newPensionnode.InnerXml = oldPensionNode.InnerXml
                'oldPensionNode.ParentNode.ReplaceChild(newPensionnode, oldPensionNode)

                'Dim oldBenefitsNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyBenefitsAmount1")
                'Dim newBenefitsnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyBenefitsAmount", oldBenefitsNode.NamespaceURI)
                'newBenefitsnode.InnerXml = oldBenefitsNode.InnerXml
                'oldBenefitsNode.ParentNode.ReplaceChild(newBenefitsnode, oldBenefitsNode)

                'Dim oldRentalNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyRentalIncomeAmount1")
                'Dim newRentalnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyRentalIncomeAmount", oldRentalNode.NamespaceURI)
                'newRentalnode.InnerXml = oldRentalNode.InnerXml
                'oldRentalNode.ParentNode.ReplaceChild(newRentalnode, oldRentalNode)

                'Dim oldAllowanceNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyAllowanceAmount1")
                'Dim newAllowancenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyAllowanceAmount", oldAllowanceNode.NamespaceURI)
                'newAllowancenode.InnerXml = oldAllowanceNode.InnerXml
                'oldAllowanceNode.ParentNode.ReplaceChild(newAllowancenode, oldAllowanceNode)

                'Dim oldInvestmentNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyInvestmentIncomeAmount1")
                'Dim newInvestmentnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyInvestmentIncomeAmount", oldInvestmentNode.NamespaceURI)
                'newInvestmentnode.InnerXml = oldInvestmentNode.InnerXml
                'oldInvestmentNode.ParentNode.ReplaceChild(newInvestmentnode, oldInvestmentNode)

                'Dim oldTaxNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyTaxCreditsAmount1")
                'Dim newTaxnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyTaxCreditsAmount", oldTaxNode.NamespaceURI)
                'newTaxnode.InnerXml = oldTaxNode.InnerXml
                'oldTaxNode.ParentNode.ReplaceChild(newTaxnode, oldTaxNode)

                'Dim oldMaintenanceNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyMaintenanceAmount1")
                'Dim newMaintenancenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyMaintenanceAmount", oldMaintenanceNode.NamespaceURI)
                'newMaintenancenode.InnerXml = oldMaintenanceNode.InnerXml
                'oldMaintenanceNode.ParentNode.ReplaceChild(newMaintenancenode, oldMaintenanceNode)

                'Dim oldSecondJobNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/MonthlyPermanentSecondJobIncome1")
                'Dim newSecondJobnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MonthlyPermanentSecondJobAmount", oldSecondJobNode.NamespaceURI)
                'newSecondJobnode.InnerXml = oldSecondJobNode.InnerXml
                'oldSecondJobNode.ParentNode.ReplaceChild(newSecondJobnode, oldSecondJobNode)
            End If

            Dim TTfree As String = ""

          


            Dim FeeType As String = ""
                Dim strSQL1 As String = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE (AppID = " & AppID & ") AND (CompanyID = '1430') AND (StoredDataName = 'FeeType')"
                Dim dsCache1 As DataTable = New Caching(Nothing, strSQL1, "", "", "").returnCache()
                If (dsCache1.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache1.Rows
                        FeeType = Row.Item("StoredDataValue")
                    Next
                End If
                dsCache = Nothing

                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Precise Affordability Request", objInputXMLDoc.InnerXml)

                Dim Hash As String = hashString384(preciseViewSystemUserName, preciseViewSystemUserApiKey, RequestDate)

                Dim AffordabilityProductSearch As New ServiceReference1NEWMCD.CaesiumApiClient()

                'Try
                    Dim reply = AffordabilityProductSearch.Request(preciseViewSystemUserName, RequestDate, Hash, objInputXMLDoc.InnerXml)

                   ' HttpContext.Current.Response.ContentType = "text/xml"
                   ' HttpContext.Current.Response.Write(reply)
                   ' HttpContext.Current.Response.End()

                    Dim doc = New XmlDocument()
                    doc.LoadXml(reply)

                    

                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Precise Response", reply)


                    Dim nodes As XmlNodeList = doc.DocumentElement.SelectNodes("/ProductMatchReply/Products/Product")
                    Dim product_code As String = "", product_amount As String = "", rate As String = "", months As String = "", product_name As String = ""

                    Dim minvalue As Decimal = 9999999
                    For Each node As XmlNode In nodes

                        Dim value = node.SelectSingleNode("Rates/Rate/PaymentAmount").InnerText
                        rate = node.SelectSingleNode("Rates/Rate/MarginRate").InnerText
                        months = node.SelectSingleNode("Rates/Rate/Months").InnerText
                        minvalue = value
                        product_code = node.SelectSingleNode("ProductCode").InnerText
                        product_name = node.SelectSingleNode("ProductName").InnerText

                        Dim strTableNew = createAffTable(product_code, minvalue, rate, product_name)

                        Response.Write(strTableNew)


                    Next

                If (product_code <> "") Then
                    '	updateDataStoreField(AppID, "UnderwritingLenderName", "Precise", "", "")
                    'updateDataStoreField(AppID, "UnderwritingLenderPlan", product_code, "", "")
                    '	updateDataStoreField(AppID, "UnderwritingRegularMonthlyPayment", minvalue, "", "")
                    'updateDataStoreField(AppID, "UnderwritingRegularInterestRate", rate, "", "")
                    '	createLoanApp(product_code, minvalue, rate)

                    'HttpContext.Current.Response.Write(1 & "|" & encodeURL("Accepted in principle by Precise. Product Code: " & product_code & ", Monthly Payment: " & minvalue & ", Margin Rate: " & rate & " please login to Precise View to accept the plan or select an alterative."))
                Else
                    Throw New Exception("No Products Found.")
                End If
                saveNote(AppID, Config.DefaultUserID, "Lender scoring completed for Precise.")
          '  Catch ex As Exception
          '          Dim strMessage As String = ex.Message.ToString()
          '          executeNonQuery("insert into tblLenderScoringResponse (AppID,ErrorMessage,ErrorDate,LenderName) Values('" & AppID & "', '" & strMessage.Replace("'", " ") & "', GETDATE(),'Precise')")
           '         saveNote(AppID, Config.DefaultUserID, "Lender scoring completed for Precise - " & strMessage.Replace("'", " ") & "")
           '         HttpContext.Current.Response.Write("Application Error -  Please check the error messages </br>")
          '          HttpContext.Current.Response.Write("<a href=""#"" onclick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Error Messages', '/prompts/lenderscoringerrors.aspx?AppID=" & AppID & "&LenderName=Precise', '600', '600');""  class=""btn btn-danger btn-small"">Error Message</a>")
          '  End Try

            End If

            objInputXMLDoc = Nothing
            objOutputXMLDoc = Nothing


    End Sub

    Public Function createAffTable(ByVal ProductCode As String, ByVal MinValue As String, ByVal Rate As String, ByVal ProductName As String) As String

        Dim strTable As String = ""


        strTable += _
                   "<tr>" & vbCrLf & _
                   "<td class=""smlc""><span >" & ProductCode & "</span></td>" & vbCrLf & _
                   "<td class=""smlc""><span >" & ProductName & "</span></td>" & vbCrLf & _
                   "<td class=""smlc""><span >" & MinValue & "</span></td>" & vbCrLf & _
                   "<td class=""smlc""><span >" & Rate & "</span></td>" & vbCrLf & _
                   "<td class=""smlc""><span style=""color:green""><a href=""http://services.engagedcrm.co.uk/webservices/outbound/Precise/?AppID=" & AppID & "&CreateLoan=Y&ProductCode=" & ProductCode & "&MinValue=" & MinValue & "&Rate=" & Rate & "&ProductName=" & ProductName & """ class=""btn btn-danger btn-small"">Select Plan</a></span></td>" & vbCrLf & _
                   "</tr>"



        Return strTable

    End Function



    Private Sub createLoanApp(product_code As String, minvalue As String, rate As String)

        Dim App2Title As String = ""
        Try

            If (checkValue(getAnyField("App2Title", "tblapplications", "AppID", AppID))) Then
                App2Title = getAnyField("App2Title", "tblapplications", "AppID", AppID)

            End If

            objInputXMLDoc.XmlResolver = Nothing
            If (App2Title <> "") Then
                objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/Precise/blankapp2.xml"))
            Else
                objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/Precise/blank.xml"))
            End If

            objInputXMLDoc.XmlResolver = Nothing


            Dim strSQL As String = "SELECT * FROM vwxmlPrecise WHERE AppID = " & AppID & " And CompanyID = '" & CompanyID & "' "
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    For Each Column As DataColumn In dsCache.Columns
                        If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                            Dim strTitle As String = Column.ColumnName.ToString = "Applicants/Applicant/Title1"
                            If (strTitle <> "False") Then
                                'createNewElement("Applicants", "Applicant2")
                                'createNewElement("Applicant2", "Title1")
                                'createNewElement("Applicant2", "FirstName1")
                                'createNewElement("Applicant2", "MiddleName1")
                                'createNewElement("Applicant2", "Surname1")
                                'createNewElement("Applicant2", "DateOfBirth1")
                                'createNewElement("Applicant2", "MaritalStatus1")
                                'createNewElement("Applicant2", "HomeTelephoneNumber1")

                                'createNewElement("Applicant2", "OfficeTelephoneNumber1")
                                'createNewElement("Applicant2", "MobileTelephoneNumber1")
                                'createNewElement("Applicant2", "EmailAddress1")
                                'createNewElement("Applicant2", "MethodOfContact1")
                                'createNewElement("Applicant2", "Nationality1")

                                'createNewElement("Applicant2", "UKResidentDate1")
                                'createNewElement("Applicant2", "UkResidentSinceBirth1")

                                'createNewElement("Applicant2", "MaidenSurname1")

                                'createNewElement("Applicant2", "OptOutSMS1")
                                'createNewElement("Applicant2", "OptOutEMail1")

                                'createNewElement("Applicant2", "Income2")
                                'createNewElement("Income2", "GrossAnnualIncomeAmount1")
                                'createNewElement("Income2", "MonthlyPermanentSecondJobIncome1")
                                'createNewElement("Income2", "MonthlyOverTimeBonusCommissionAmount1")
                                'createNewElement("Income2", "MonthlyPensionAmount1")
                                'createNewElement("Income2", "MonthlyTaxCreditsAmount1")
                                'createNewElement("Income2", "MonthlyMaintenanceAmount1")
                                'createNewElement("Income2", "MonthlyRentalIncomeAmount1")
                                'createNewElement("Income2", "MonthlyBenefitsAmount1")
                                'createNewElement("Income2", "MonthlyAllowanceAmount1")
                                'createNewElement("Income2", "MonthlyInvestmentIncomeAmount1")

                                'createNewElement("Applicant2", "AddressHistories1")

                                'createNewElement("AddressHistories1", "AddressHistory1")

                                'createNewElement("AddressHistory1", "StartDate1")
                                'createNewElement("AddressHistory1", "EndDate1")
                                'createNewElement("AddressHistory1", "ResidentialStatus1")

                                'createNewElement("AddressHistory1", "Address1")

                                'createNewElement("Address1", "FlatNumber1")
                                'createNewElement("Address1", "HouseNumber1")
                                'createNewElement("Address1", "HouseName1")
                                'createNewElement("Address1", "AddressLine11")
                                'createNewElement("Address1", "AddressLine21")
                                'createNewElement("Address1", "Town1")
                                'createNewElement("Address1", "District1")
                                'createNewElement("Address1", "County1")
                                'createNewElement("Address1", "PostCode1")
                                'createNewElement("Address1", "CountryName1")

                            End If

                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 1) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If

                            If (Column.ColumnName.ToString = "SecondCharge/RepaymentType") Then
                                Server.HtmlEncode(Row(Column).ToString())

                            End If
                            If (Column.ColumnName.ToString = "SecurityProperty/FirstCharge/RepaymentType") Then
                                Server.HtmlEncode(Row(Column).ToString())
                            End If

                            If (Column.ColumnName.ToString = "SecondChargeLoanDetail/ProductCode") Then
                                Server.HtmlEncode(product_code)
                            End If


                            writeToNode("n", "Payload/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)

                        End If
                    Next
                Next
            End If
            dsCache = Nothing
        Catch ex As Exception
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Error - Create Loan Build XML"))
        End Try

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	
			Dim RepaymentType As String = getAnyFieldFromDataStore("RepaymentType", AppID)


            If (RepaymentType = "InterestOnly") Then
                RepaymentType = "Interest Only"
            Else
                RepaymentType = "Capital & Interest"
            End If
			
			objInputXMLDoc.SelectSingleNode("Payload/SecondChargeLoanDetail/RepaymentType").InnerText = RepaymentType
            objInputXMLDoc.SelectSingleNode("Payload/SecurityProperty/FirstCharge/RepaymentType").InnerText = RepaymentType

            objInputXMLDoc.SelectSingleNode("Payload/SecondChargeLoanDetail/ProductCode").InnerText = product_code
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/HouseNumber").InnerText = getAnyFieldFromDataStore("AddressHistoryHouseNumber", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/HouseName").InnerText = getAnyFieldFromDataStore("AddressHistoryHouseName", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/AddressLine1").InnerText = getAnyField("AddressLine1", "tblapplications", "AppID", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/AddressLine2").InnerText = getAnyFieldFromDataStore("AddressHistoryLine2", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/Town").InnerText = getAnyFieldFromDataStore("AddressHistoryLine3", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/County").InnerText = getAnyFieldFromDataStore("AddressHistoryCounty", AppID)
            objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/Address/PostCode").InnerText = getAnyField("AddressPostCode", "tblapplications", "AppID", AppID)
            'objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/StartDate").InnerText = getAnyFieldFromDataStore("JointPreviousAddressDatefrom", AppID)
            'objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/AddressHistories/AddressHistory/EndDate").InnerText = getAnyFieldFromDataStore("JointPreviousAddressDateTo", AppID)

            Dim App2FirstName As String = ""
            Dim App2Surname As String = ""

            If (checkValue(getAnyField("App2FirstName", "tblapplications", "AppID", AppID))) Then
                App2FirstName = getAnyField("App2FirstName", "tblapplications", "AppID", AppID)
            End If
            If (checkValue(getAnyField("App2Surname", "tblapplications", "AppID", AppID))) Then
                App2Surname = getAnyField("App2Surname", "tblapplications", "AppID", AppID)
            End If

            If (App2Title <> "") Then
                objInputXMLDoc.SelectSingleNode("Payload/SecondChargeLoanDetail/ProductCode").InnerText = product_code
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/HouseNumber1").InnerText = getAnyFieldFromDataStore("AddressHistoryHouseNumber", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/HouseName1").InnerText = getAnyFieldFromDataStore("AddressHistoryHouseName", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/AddressLine11").InnerText = getAnyField("AddressLine1", "tblapplications", "AppID", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/AddressLine21").InnerText = getAnyFieldFromDataStore("AddressHistoryLine2", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/Town1").InnerText = getAnyFieldFromDataStore("AddressHistoryLine3", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/County1").InnerText = getAnyFieldFromDataStore("AddressHistoryCounty", AppID)
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/PostCode1").InnerText = getAnyField("AddressPostCode", "tblapplications", "AppID", AppID)

                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/Address1/CountryName1").InnerText = "United Kingdom"

                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/StartDate1").InnerText = "2010-01-01"
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/AddressHistories1/AddressHistory1/EndDate1").InnerText = "2013-01-01"

                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/MethodOfContact1").InnerText = "Mobile"
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/UkResidentSinceBirth1").InnerText = 1
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/OptOutSMS1").InnerText = 1
                objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant2/OptOutEMail1").InnerText = 1




                'If (IsDBNull(getAnyFieldFromDataStore("App1RentalInPA", AppID))) Then
                '    Dim RentalIncome As String = getAnyFieldFromDataStore("App1RentalInPA", AppID)
                '    Dim MonthlyRentalIncome As Integer = RentalIncome / 12
                '    objInputXMLDoc.SelectSingleNode("Payload/Applicants/Applicant/Income/MonthlyRentalIncomeAmount").InnerText = MonthlyRentalIncome
                'End If
                'If (IsDBNull(getAnyFieldFromDataStore("RentAfterFees", AppID))) Then
                '    Dim SecuirtyMonthlyRentalIncome As String = getAnyFieldFromDataStore("RentAfterFees", AppID)
                '    objInputXMLDoc.SelectSingleNode("Payload/SecurityProperty/MonthlyRentalIncomeAmount").InnerText = SecuirtyMonthlyRentalIncome
                'End If


                'If (IsDBNull(getAnyFieldFromDataStore("App2AnnualIncome", AppID)) Or (getAnyFieldFromDataStore("App2AnnualIncome", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/GrossAnnualIncomeAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2CommissionPA", AppID)) Or (getAnyFieldFromDataStore("App2CommissionPA", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2BonusPa", AppID)) Or (getAnyFieldFromDataStore("App2BonusPa", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2OvertimePA", AppID)) Or (getAnyFieldFromDataStore("App2OvertimePA", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyOverTimeBonusCommissionAmount1").InnerText = 1.0
                'End If


                'If ((IsDBNull(getAnyFieldFromDataStore("App2CarersAllowance", AppID)) Or (getAnyFieldFromDataStore("App2CarersAllowance", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2SevereDisAllowance", AppID)) Or (getAnyFieldFromDataStore("App2SevereDisAllowance", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2DisAllowance", AppID)) Or (getAnyFieldFromDataStore("App2DisAllowance", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyAllowanceAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2StatePensionsPA", AppID)) Or (getAnyFieldFromDataStore("App2StatePensionsPA", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID)) Or (getAnyFieldFromDataStore("App2PrivatePensionsPA", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyPensionAmount1").InnerText = 1.0
                'End If


                'If (IsDBNull(getAnyFieldFromDataStore("App2InvestmentPA", AppID)) Or (getAnyFieldFromDataStore("App2InvestmentPA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyInvestmentIncomeAmount1").InnerText = 1.0
                'End If

                'If ((IsDBNull(getAnyFieldFromDataStore("App2ChildTaxCredit", AppID)) Or (getAnyFieldFromDataStore("App2ChildTaxCredit", AppID) = "")) And (IsDBNull(getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID)) Or (getAnyFieldFromDataStore("App2WorkingTaxCredits", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyTaxCreditsAmount1").InnerText = 1.0
                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2MaintenanceCSA", AppID)) Or (getAnyFieldFromDataStore("App2MaintenanceCSA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyMaintenanceAmount1").InnerText = 1.0
                'End If

                'If (((IsDBNull(getAnyFieldFromDataStore("App2AnnBenTotal", AppID))) Or (getAnyFieldFromDataStore("App2AnnBenTotal", AppID) = "")) And ((IsDBNull(getAnyFieldFromDataStore("App2IncapacityBenefit", AppID))) Or (getAnyFieldFromDataStore("App2AnnBenTotal", AppID) = ""))) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyBenefitsAmount1").InnerText = 1.0

                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2RentaPA", AppID)) Or (getAnyFieldFromDataStore("App2RentaPA", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyRentalIncomeAmount1").InnerText = 1.0
                'End If

                'If (IsDBNull(getAnyFieldFromDataStore("App2SecondJobIncome", AppID)) Or (getAnyFieldFromDataStore("App2SecondJobIncome", AppID) = "")) Then
                '    objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2/Income2/MonthlyPermanentSecondJobIncome1").InnerText = 1.0
                'End If


                Dim oldNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant2")
                Dim newnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Applicant", oldNode.NamespaceURI)
                newnode.InnerXml = oldNode.InnerXml
                oldNode.ParentNode.ReplaceChild(newnode, oldNode)

                Dim oldTitleNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Title1")
                Dim newTitlenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Title", oldTitleNode.NamespaceURI)
                newTitlenode.InnerXml = oldTitleNode.InnerXml
                oldTitleNode.ParentNode.ReplaceChild(newTitlenode, oldTitleNode)

                Dim oldFirstNameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/FirstName1")
                Dim newFirstNamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "FirstName", oldFirstNameNode.NamespaceURI)
                newFirstNamenode.InnerXml = oldFirstNameNode.InnerXml
                oldFirstNameNode.ParentNode.ReplaceChild(newFirstNamenode, oldFirstNameNode)

                Dim oldMiddleNameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MiddleName1")
                Dim newMiddleNamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MiddleName", oldMiddleNameNode.NamespaceURI)
                newMiddleNamenode.InnerXml = oldMiddleNameNode.InnerXml
                oldMiddleNameNode.ParentNode.ReplaceChild(newMiddleNamenode, oldMiddleNameNode)

                Dim oldSurnameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Surname1")
                Dim newSurnamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Surname", oldSurnameNode.NamespaceURI)
                newSurnamenode.InnerXml = oldSurnameNode.InnerXml
                oldSurnameNode.ParentNode.ReplaceChild(newSurnamenode, oldSurnameNode)

                Dim oldDOBNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/DateOfBirth1")
                Dim newDOBnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "DateOfBirth", oldDOBNode.NamespaceURI)
                newDOBnode.InnerXml = oldDOBNode.InnerXml
                oldDOBNode.ParentNode.ReplaceChild(newDOBnode, oldDOBNode)

                Dim oldMaritalNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MaritalStatus1")
                Dim newMaritalnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MaritalStatus", oldMaritalNode.NamespaceURI)
                newMaritalnode.InnerXml = oldMaritalNode.InnerXml
                oldMaritalNode.ParentNode.ReplaceChild(newMaritalnode, oldMaritalNode)

                Dim oldHomeTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/HomeTelephoneNumber1")
                Dim newHomeTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "HomeTelephoneNumber", oldHomeTelNode.NamespaceURI)
                newHomeTelnode.InnerXml = oldHomeTelNode.InnerXml
                oldHomeTelNode.ParentNode.ReplaceChild(newHomeTelnode, oldHomeTelNode)

                Dim oldOfficeTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/OfficeTelephoneNumber1")
                Dim newOfficeTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "OfficeTelephoneNumber", oldOfficeTelNode.NamespaceURI)
                newOfficeTelnode.InnerXml = oldOfficeTelNode.InnerXml
                oldOfficeTelNode.ParentNode.ReplaceChild(newOfficeTelnode, oldOfficeTelNode)

                Dim oldMobileTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MobileTelephoneNumber1")
                Dim newMobileTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MobileTelephoneNumber", oldMobileTelNode.NamespaceURI)
                newMobileTelnode.InnerXml = oldMobileTelNode.InnerXml
                oldMobileTelNode.ParentNode.ReplaceChild(newMobileTelnode, oldMobileTelNode)

                Dim oldContactNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/EmailAddress1")
                Dim newContactnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "EmailAddress", oldContactNode.NamespaceURI)
                newContactnode.InnerXml = oldContactNode.InnerXml
                oldContactNode.ParentNode.ReplaceChild(newContactnode, oldContactNode)

                Dim oldEmailTelNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MethodOfContact1")
                Dim newEmailTelnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MethodOfContact", oldEmailTelNode.NamespaceURI)
                newEmailTelnode.InnerXml = oldEmailTelNode.InnerXml
                oldEmailTelNode.ParentNode.ReplaceChild(newEmailTelnode, oldEmailTelNode)

                Dim oldNationalityNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Nationality1")
                Dim newNationalitynode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Nationality", oldNationalityNode.NamespaceURI)
                newNationalitynode.InnerXml = oldNationalityNode.InnerXml
                oldNationalityNode.ParentNode.ReplaceChild(newNationalitynode, oldNationalityNode)


                Dim oldResidentDateNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/UKResidentDate1")
                Dim newResidentDatenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "UKResidentDate", oldResidentDateNode.NamespaceURI)
                newResidentDatenode.InnerXml = oldResidentDateNode.InnerXml
                oldResidentDateNode.ParentNode.ReplaceChild(newResidentDatenode, oldResidentDateNode)


                Dim oldResidentBirthNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/UkResidentSinceBirth1")
                Dim newResidentBirthnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "UkResidentSinceBirth", oldResidentBirthNode.NamespaceURI)
                newResidentBirthnode.InnerXml = oldResidentBirthNode.InnerXml
                oldResidentBirthNode.ParentNode.ReplaceChild(newResidentBirthnode, oldResidentBirthNode)


                Dim oldMaidenNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/MaidenSurname1")
                Dim newMaidennode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "MaidenSurname", oldMaidenNode.NamespaceURI)
                newMaidennode.InnerXml = oldMaidenNode.InnerXml
                oldMaidenNode.ParentNode.ReplaceChild(newMaidennode, oldMaidenNode)


                Dim oldSMSNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/OptOutSMS1")
                Dim newSMSnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "OptOutSMS", oldSMSNode.NamespaceURI)
                newSMSnode.InnerXml = oldSMSNode.InnerXml
                oldSMSNode.ParentNode.ReplaceChild(newSMSnode, oldSMSNode)

                Dim oldEmailNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/OptOutEMail1")
                Dim newEmailnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "OptOutEMail", oldEmailNode.NamespaceURI)
                newEmailnode.InnerXml = oldEmailNode.InnerXml
                oldEmailNode.ParentNode.ReplaceChild(newEmailnode, oldEmailNode)



                Dim oldIncomeNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income2")
                Dim newIncomenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Income", oldIncomeNode.NamespaceURI)
                newIncomenode.InnerXml = oldIncomeNode.InnerXml
                oldIncomeNode.ParentNode.ReplaceChild(newIncomenode, oldIncomeNode)


                Dim oldGrossIncomeNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/Income/GrossAnnualIncomeAmount1")
                Dim newGrossIncomenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "GrossAnnualIncomeAmount", oldGrossIncomeNode.NamespaceURI)
                newGrossIncomenode.InnerXml = oldGrossIncomeNode.InnerXml
                oldGrossIncomeNode.ParentNode.ReplaceChild(newGrossIncomenode, oldGrossIncomeNode)




                Dim oldHistoriesNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories1")
                Dim newHistoriesnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "AddressHistories", oldHistoriesNode.NamespaceURI)
                newHistoriesnode.InnerXml = oldHistoriesNode.InnerXml
                oldHistoriesNode.ParentNode.ReplaceChild(newHistoriesnode, oldHistoriesNode)


                Dim oldHistoryNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory1")
                Dim newHistorynode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "AddressHistory", oldHistoryNode.NamespaceURI)
                newHistorynode.InnerXml = oldHistoryNode.InnerXml
                oldHistoryNode.ParentNode.ReplaceChild(newHistorynode, oldHistoryNode)


                Dim oldStartDateNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/StartDate1")
                Dim newStartDatenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "StartDate", oldStartDateNode.NamespaceURI)
                newStartDatenode.InnerXml = oldStartDateNode.InnerXml
                oldStartDateNode.ParentNode.ReplaceChild(newStartDatenode, oldStartDateNode)

                Dim oldEndDateNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/EndDate1")
                Dim newEndDatenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "EndDate", oldEndDateNode.NamespaceURI)
                newEndDatenode.InnerXml = oldEndDateNode.InnerXml
                oldEndDateNode.ParentNode.ReplaceChild(newEndDatenode, oldEndDateNode)

                Dim oldResidentialstatusNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/ResidentialStatus1")
                Dim newResidentialstatusnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "ResidentialStatus", oldResidentialstatusNode.NamespaceURI)
                newResidentialstatusnode.InnerXml = oldResidentialstatusNode.InnerXml
                oldResidentialstatusNode.ParentNode.ReplaceChild(newResidentialstatusnode, oldResidentialstatusNode)

                Dim oldAddressNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address1")
                Dim newAddressnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Address", oldAddressNode.NamespaceURI)
                newAddressnode.InnerXml = oldAddressNode.InnerXml
                oldAddressNode.ParentNode.ReplaceChild(newAddressnode, oldAddressNode)




                Dim oldFlatNoNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/FlatNumber1")
                Dim newFlatNonode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "FlatNumber", oldFlatNoNode.NamespaceURI)
                newFlatNonode.InnerXml = oldFlatNoNode.InnerXml
                oldFlatNoNode.ParentNode.ReplaceChild(newFlatNonode, oldFlatNoNode)

                Dim oldHouseNoNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/HouseNumber1")
                Dim newHouseNonode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "HouseNumber", oldHouseNoNode.NamespaceURI)
                newHouseNonode.InnerXml = oldHouseNoNode.InnerXml
                oldHouseNoNode.ParentNode.ReplaceChild(newHouseNonode, oldHouseNoNode)

                Dim oldHouseNameNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/HouseName1")
                Dim newHouseNamenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "HouseName", oldHouseNameNode.NamespaceURI)
                newHouseNamenode.InnerXml = oldHouseNameNode.InnerXml
                oldHouseNameNode.ParentNode.ReplaceChild(newHouseNamenode, oldHouseNameNode)

                Dim oldAddressLine1Node As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/AddressLine11")
                Dim newAddressLine1node As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "AddressLine1", oldAddressLine1Node.NamespaceURI)
                newAddressLine1node.InnerXml = oldAddressLine1Node.InnerXml
                oldAddressLine1Node.ParentNode.ReplaceChild(newAddressLine1node, oldAddressLine1Node)

                Dim oldAddressLine2Node As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/AddressLine21")
                Dim newAddressLine2node As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "AddressLine2", oldAddressLine2Node.NamespaceURI)
                newAddressLine2node.InnerXml = oldAddressLine2Node.InnerXml
                oldAddressLine2Node.ParentNode.ReplaceChild(newAddressLine2node, oldAddressLine2Node)

                Dim oldTownNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/Town1")
                Dim newTownnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "Town", oldTownNode.NamespaceURI)
                newTownnode.InnerXml = oldTownNode.InnerXml
                oldTownNode.ParentNode.ReplaceChild(newTownnode, oldTownNode)

                Dim oldDistrictNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/District1")
                Dim newDistrictnode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "District", oldDistrictNode.NamespaceURI)
                newDistrictnode.InnerXml = oldDistrictNode.InnerXml
                oldDistrictNode.ParentNode.ReplaceChild(newDistrictnode, oldDistrictNode)

                Dim oldCountyNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/County1")
                Dim newCountynode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "County", oldCountyNode.NamespaceURI)
                newCountynode.InnerXml = oldCountyNode.InnerXml
                oldCountyNode.ParentNode.ReplaceChild(newCountynode, oldCountyNode)

                Dim oldPostcodeNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/PostCode1")
                Dim newPostcodenode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "PostCode", oldPostcodeNode.NamespaceURI)
                newPostcodenode.InnerXml = oldPostcodeNode.InnerXml
                oldPostcodeNode.ParentNode.ReplaceChild(newPostcodenode, oldPostcodeNode)

                Dim oldCountryNode As XmlNode = objInputXMLDoc.SelectSingleNode("//Applicants/Applicant/AddressHistories/AddressHistory/Address/CountryName1")
                Dim newCountrynode As XmlNode = objInputXMLDoc.CreateNode(XmlNodeType.Element, "CountryName", oldCountryNode.NamespaceURI)
                newCountrynode.InnerXml = oldCountryNode.InnerXml
                oldCountryNode.ParentNode.ReplaceChild(newCountrynode, oldCountryNode)

            End If




            Dim preciseViewSystemUserName As String = "PositiveAPIUser"
                'UAT
                ' Dim preciseViewSystemUserApiKey As String = "eDDvnVSDnoUcAZp9JIX6YbKss5iBsb"
                'LIVE
                Dim preciseViewSystemUserApiKey As String = "ljDHWxpNxAO1rgYVJ7qRhjhzM9nTrt"
                Dim RequestDate As String = DateTime.UtcNow.ToString("o")

                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Precise API Create Loan", objInputXMLDoc.InnerXml)

                Dim Hash As String = hashString384(preciseViewSystemUserName, preciseViewSystemUserApiKey, RequestDate)
                Try
                    Dim CreateLoanApplication As New ServiceReferencePreciseCreateLoan.CaesiumApiClient()

                    Dim reply = CreateLoanApplication.Request(preciseViewSystemUserName, RequestDate, Hash, objInputXMLDoc.InnerXml)

                Dim doc = New XmlDocument()
                doc.LoadXml(reply)




                Dim nodes As XmlNodeList = doc.DocumentElement.SelectNodes("/CreateLoanApplication")
                    Dim ref As String = ""
                    Dim quoteNO As String = ""

                    For Each node As XmlNode In nodes
                        ref = node.SelectSingleNode("ApplicationReference").InnerText
                        quoteNO = node.SelectSingleNode("QuoteNumber").InnerText

                    Next
                    If (ref <> "") Then
                        updateDataStoreField(AppID, "UnderwritingLenderRef", ref, "", "")
                        updateDataStoreField(AppID, "PreciseQuoteNo", quoteNO, "", "")
                        updateDataStoreField(AppID, "UnderwritingLenderName", "Precise Mortgages", "", "")
                        updateDataStoreField(AppID, "UnderwritingLenderPlan", product_code, "", "")
                        updateDataStoreField(AppID, "UnderwritingRegularMonthlyPayment", minvalue, "", "")
                        updateDataStoreField(AppID, "UnderwritingRegularInterestRate", rate, "", "")

                    Else
                        Throw New Exception("Problem finding reference.")
                    End If
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Precise Create Loan Response", reply)
                saveNote(AppID, Config.DefaultUserID, "Accepted By Precise. Lender Reference: " & ref & ", Quote Number: " & quoteNO & ", Product Code: " & product_code & ", Monthly Payment: " & minvalue & ", Margin Rate: " & rate & "")
                HttpContext.Current.Response.Write("Accepted  by Precise. Product Code: " & product_code & ", Monthly Payment: " & minvalue & ", Margin Rate: " & rate & ".<br/><a href=""#"" onclick=""window.top.location.reload();""  class=""btn btn-danger btn-small"">Close</a>")

            Catch ex As Exception
                    Dim strMessage As String = ex.Message.ToString()
                    executeNonQuery("insert into tblLenderScoringResponse (AppID,ErrorMessage,ErrorDate,LenderName) Values('" & AppID & "', '" & strMessage.Replace("'", " ") & "', GETDATE(),'Precise')")
                    HttpContext.Current.Response.Write("Application Error -  Please check the error messages Create Loan </br>")
                    HttpContext.Current.Response.Write("<a href=""#"" onclick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Error Messages', '/prompts/lenderscoringerrors.aspx?AppID=" & AppID & "&LenderName=Precise', '600', '600');""  class=""btn btn-danger btn-small"">Error Message</a>")

                End Try




            End If
            objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing


    End Sub

    Private Sub getDocument()

        Try
            objInputXMLDoc.XmlResolver = Nothing
            objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/Precise/document.xml"))

            Dim preciseViewSystemUserName As String = "PositiveAPIUser"
            'UAT
            ' Dim preciseViewSystemUserApiKey As String = "eDDvnVSDnoUcAZp9JIX6YbKss5iBsb"
            'LIVE
            Dim preciseViewSystemUserApiKey As String = "ljDHWxpNxAO1rgYVJ7qRhjhzM9nTrt"
            Dim RequestDate As String = DateTime.UtcNow.ToString("o")

            Dim Hash As String = hashString384(preciseViewSystemUserName, preciseViewSystemUserApiKey, RequestDate)

            Dim CreateDocument As New ServiceReferencePreciseDocument.CaesiumDocApiClient()

            Dim newref = getAnyFieldFromDataStore("UnderwritingLenderRef", AppID)
            Dim newquoteno = getAnyFieldFromDataStore("PreciseQuoteNo", AppID)

            objInputXMLDoc.SelectSingleNode("Payload/Document/ApplicationReference").InnerText = newref
            objInputXMLDoc.SelectSingleNode("Payload/Document/QuoteNumber").InnerText = newquoteno


            Dim reply() As Byte
            reply = CreateDocument.Request(preciseViewSystemUserName, RequestDate, Hash, objInputXMLDoc.InnerXml)

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("crm.live.ConnectionString").ToString())
            Dim insertESIS As New SqlCommand()

            Dim cal = New CultureInfo("tr-TR", False).Calendar
            Dim woy2 As String = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, System.DayOfWeek.Monday).ToString()
            Dim number As Integer = Convert.ToInt32(woy2)
            number += 1
            Dim newWOY As String = number.ToString()

            Dim filedate As String = (DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString())
            connection.Open()
            insertESIS.Connection = connection
            insertESIS.CommandText = String.Format("insert into tblPDFFiles(AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate, PDFHistoryFileName) values ('" & AppID & "', 'Precise ESIS Document', 'application/pdf', @Data, '', '', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','/positive-lending/" + newWOY + "-" + DateTime.Now.Year.ToString() + "/" + filedate + ".pdf')")
            insertESIS.Parameters.Add("@Data", SqlDbType.VarBinary).Value = reply
            insertESIS.ExecuteNonQuery()
            databaseFileRead(AppID, "F:\data\attachments\positive-lending\" + newWOY + "-" + DateTime.Now.Year.ToString() + "\" + filedate + ".pdf")
            HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + "" + filedate + ".pdf")
            HttpContext.Current.Response.ContentType = "application/pdf"
            HttpContext.Current.Response.BinaryWrite(reply)
            HttpContext.Current.Response.Redirect("http://positive.engagedcrm.co.uk/processing.aspx?Action=2&AppID=" & AppID & "&strMessageType=success&strMessageTitle=Document&strMessage=Document Downloaded to Letter History - (Contact -> History)")
			HttpContext.Current.Response.End()

        Catch ex As Exception
            Dim strMessage As String = ex.Message.ToString()

            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Error: " & strMessage))

        End Try

    End Sub


    Public Shared Sub databaseFileRead(AppID As String, varPathToNewLocation As String)
        Using varConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("crm.live.ConnectionString").ToString())
            Using sqlQuery = New SqlCommand("SELECT Data FROM [dbo].[tblPDFFiles] WHERE [AppID] = @varID order by CreatedDate desc", varConnection)
                varConnection.Open()
                sqlQuery.Parameters.AddWithValue("@varID", AppID)
                Using sqlQueryResult = sqlQuery.ExecuteReader()
                    If sqlQueryResult IsNot Nothing Then
                        sqlQueryResult.Read()
                        Dim blob = New [Byte]((sqlQueryResult.GetBytes(0, 0, Nothing, 0, Integer.MaxValue)) - 1) {}
                        sqlQueryResult.GetBytes(0, 0, blob, 0, blob.Length)
                        Using fs = New FileStream(varPathToNewLocation, FileMode.Create, FileAccess.Write)
                            fs.Write(blob, 0, blob.Length)
                        End Using
                    End If
                End Using
            End Using
        End Using

    End Sub

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================


'    Public Function generateFromBytes(bytes As String, UserID As String, Optional title As String = "") As String
'        'HttpContext.Current.Response.ContentType = "application/pdf"
'        Try
'
'            HttpContext.Current.Response.Write(bytes)
'            HttpContext.Current.Response.Flush()
'            HttpContext.Current.Response.SuppressContent = True
'            HttpContext.Current.ApplicationInstance.CompleteRequest()
'
'            objPDF.Append(New PdfDocument(Convert.FromBase64String(bytes)))
'            Dim strTimeStamp As String = Year(Config.DefaultDateTime) & Month(Config.DefaultDateTime) & Day(Config.DefaultDateTime) & Hour(Config.DefaultDateTime) & Minute(Config.DefaultDateTime) & Second(Config.DefaultDateTime) & Config.DefaultDateTime.Millisecond
'            Dim strDir As String = "F:\data\attachments"
'            Dim strCompanyDir As String = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))
'            If (Not Directory.Exists(strDir & "\" & strCompanyDir)) Then
'                Directory.CreateDirectory(strDir & "\" & strCompanyDir)
'            End If
'            Dim strDateDir As String = DatePart(DateInterval.WeekOfYear, Config.DefaultDate) & "-" & Config.DefaultDate.Year
'            If (Not Directory.Exists(strDir & "\" & strCompanyDir & "\" & strDateDir)) Then
'                Directory.CreateDirectory(strDir & "\" & strCompanyDir & "\" & strDateDir)
'            End If
'            Dim strFilePath As String = strDir & "/" & strCompanyDir & "/" & strDateDir & "/" & strTimeStamp & ".pdf"
'            objPDF.Draw(strFilePath)
'            If (checkValue(title)) Then
'                Dim strQry As String = _
'                        "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID) " & _
'                        "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
'                        formatField(AppID, "N", 0) & ", " & _
'                        formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strTimeStamp & ".pdf", "", "") & ", " & _
'                        formatField(title & ".pdf", "T", "") & ", " & _
'                        formatField("", "", "") & ", " & _
'                        formatField(UserID, "N", 0) & ") "
'                executeNonQuery(strQry)
'            End If
'            Return strDir & "/" & strCompanyDir & "/" & strDateDir & "/" & strTimeStamp & ".pdf"
'        Catch ex As Exception
'            HttpContext.Current.Response.Write(ex)
'        End Try
'    End Function

    Private Sub writeXML(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode("/" & parent)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub insertElement(ByVal before As String, ByVal parent As String, ByVal name As String)
        Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & name)
        Dim objNodeBefore = objInputXMLDoc.SelectSingleNode("//" & before)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = objInputXMLDoc.createElement(name)
            objApplication.InsertAfter(objNewNode, objNodeBefore)
        End If
    End Sub

    Private Sub createNewElement(ByVal parent As String, ByVal name As String)
        Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent & "/" & name)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//" & parent)
            Dim objNewNode As XmlElement = objInputXMLDoc.createElement(name)
            objApplication.AppendChild(objNewNode)
        End If
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                End If
            End If
        End If
    End Sub

    Shared Function hashString384(ByVal key As String, ByVal text As String, ByVal newdate As String) As String



        Dim Hash As HashAlgorithm = New SHA384Managed()
        Dim plainText As String = String.Concat(key.ToUpperInvariant(), text, newdate)
        Dim plainTextBytes As Byte() = Encoding.UTF8.GetBytes(plainText)
        Dim hashBytes As Byte() = Hash.ComputeHash(plainTextBytes)
        Return Convert.ToBase64String(hashBytes)

    End Function


    Private Function postPrciseWebRequest(ByVal url As String) As HttpWebResponse

        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .Timeout = 30000
                .ReadWriteTimeout = 30000
            End With
            responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            For Each item In objRequest.Headers
                responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            Next
            responseEnd()
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                Response.Write(err)
                Response.End()
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function




End Class