﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports Everydayloans
Imports System.Xml.Linq

Partial Class XMLCRM
	Inherits System.Web.UI.Page

	Private strXMLURL As String = ""
	Private AppID As String = HttpContext.Current.Request.QueryString("AppID")
	Private strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignIDOutbound")
	Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
	Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
	Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
	Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
	Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
	Private strProductType As String = HttpContext.Current.Request("frmProductType"), strCustomerEmailID As String = ""
	Private strErrorMessage As String = ""
	Private strMediaCampID As String = HttpContext.Current.Request.QueryString("MediaCampaignID")

	Public Sub generateXml()
		strXMLURL = "https://testbrokerapi.everyday-loans.co.uk/Process_Lead_Engaged_Solutions_Limited"
		strCustomerEmailID = ""
		'If checkValue(AppID) Then
		'sendIndividualApp()
		sendXML()
		' End If
	End Sub

	Private Sub sendXML()



		'Dim LeadIncome1 As String = ""
		'      Dim LeadIncome2 As String = ""

		'Dim strIncomeVals as string = "SELECT MediaCampaignCostOutbound, MediaCampaignCostOutboundTwo from tblMediaCampaigns where MediaCampaignID ='"& strMediaCampID &"'"
		'Dim dsCacheVals As DataTable = New Caching(Nothing, strIncomeVals, "", "", "").returnCache()
		'      If (dsCacheVals.Rows.Count > 0) Then
		'	For Each RowVal As DataRow In dsCacheVals.Rows

		'		LeadIncome1 = RowVal.Item("MediaCampaignCostOutbound").toString()
		'		LeadIncome2 = RowVal.Item("MediaCampaignCostOutboundTwo").toString()

		'	Next
		'End If



		objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/everydayloans/blank.xml"))

'		Dim strSQL As String = "SELECT * FROM vweverydayloans WHERE AppID = " & AppID & " "
'		Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
'		If (dsCache.Rows.Count > 0) Then
'			For Each Row As DataRow In dsCache.Rows
'				For Each Column As DataColumn In dsCache.Columns
'					If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppId") Then
'						Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
'						Dim strParent As String = ""
'						If (UBound(arrName) > 0) Then
'							For y As Integer = 0 To UBound(arrName) - 1
'								If (y = 0) Then
'									strParent += "def:" & arrName(y)
'								Else
'									strParent += "/def:" & arrName(y)
'								End If
'							Next
'						Else
'							strParent = arrName(0)
'						End If
'
'						writeToNode("n", "Application/" & strParent, arrName(UBound(arrName)), Row(Column).ToString)
'					End If
'				Next
'			Next
'		End If
'		dsCache = Nothing
'
'	
'		objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
'        objNSM.AddNamespace("Application", "http://tempuri.org/Lead1.xsd")

		'HttpContext.Current.Response.ContentType = "text/xml"
		'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
		'HttpContext.Current.Response.End()

		If (strErrorMessage <> "") Then
			HttpContext.Current.Response.Clear()
			HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
		Else
			' Post the SOAP message.	

			Try
				Dim objResponse As HttpWebResponse = postWebRequestDP(strXMLURL, objInputXMLDoc.InnerXml)
				Dim objReader As New StreamReader(objResponse.GetResponseStream())
				objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
				HttpContext.Current.Response.ContentType = "text/xml"
				HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
				HttpContext.Current.Response.End()


				HttpContext.Current.Response.Write(objOutputXMLDoc.GetElementsByTagName("status_code"))
				HttpContext.Current.Response.End()

				'Dim objApplication As XmlNode = objOutputXMLDoc.SelectSingleNode("//status")
				'		HttpContext.Current.Response.Write(objApplication)
				'		HttpContext.Current.Response.End()

				'Select Case objApplication.SelectSingleNode("status").InnerText
				'               Case "BD"
				'		 HttpContext.Current.Response.Write("here")
				'		HttpContext.Current.Response.End()

				'End Select





				'            If (objResponse.StatusCode.ToString = "OK") Then
				'              	try 
				'            ' Parse the XML document.
				'            Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//def:AddBorrowerResult", objNSM2)
				'           	Dim strMessage As String = ""
				'   Select Case objApplication.SelectSingleNode("def:Successful", objNSM2).InnerText
				'                Case "true"
				'                    HttpContext.Current.Response.Clear()
				'                    HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Every Day Loans"))

				'		' executeNonQuery("update tblapplications set ClientRefOutbound = '" & objApplication.SelectSingleNode("def:executeSoapSimpleNo", objNSM2).InnerText & "' where AppID='" & AppID & "'")
				'			executeNonQuery("update tblapplicationstatus set transferredDate = GETDATE() where AppID='" & AppID & "'")
				'                        executeNonQuery("update tblapplicationstatus set statuscode = 'TFR' where AppID='" & AppID & "'")
				'                        executeNonQuery("update tblapplications set MediaCampaignIDOutbound = '" & strMediaCampID & "' where AppID='" & AppID & "'")
				'                        Dim MediaID As String = getAnyField("MediaID","tblMediaCampaigns","MediaCampaignID",strMediaCampID)
				'                        executeNonQuery("update tblapplications set MediaIdOutbound = '" & MediaID & "' where AppID='" & AppID & "'")
				'                        incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
				'                        executeNonQuery("update tblapplications set LeadIncome1= '" & LeadIncome1 & "', LeadIncome2 = '" & LeadIncome2 & "' where AppID = '" & AppID & "'")
				'			executeNonQuery("update tblMediaCampaignVolumes set DeliveryVolume = DeliveryVolume + 1, CurrentDeliveryVolume = CurrentDeliveryVolume + 1 where MediaCampaignID = '" & strMediaCampID & "'")
				'			 try	
				'			saveXMLReceived(Request.ServerVariables("REMOTE_ADDR"),AppID,1,"Application successfully received",objInputXMLDoc.InnerXml)
				'			catch

				'			end try  
				'	Case Else
				'                    HttpContext.Current.Response.Clear()
				'                    HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Every Day Loans"))
				'                    incrementTransferAttempts(AppID, strMediaCampaignID)
				'		 try	
				'		Dim strEmailText As String = "Lead failed to send reason: " & objApplication.SelectSingleNode("def:ErrorCode", objNSM2).InnerText & ""
				'		postEmail("", "william.worthington@engagedcrm.co.uk", "EveryDayLoans Lead Failed to Send " & AppID & "", objApplication.SelectSingleNode("def:ErrorDetails", objNSM2).InnerText, True, "", True)
				'		saveXMLReceived(Request.ServerVariables("REMOTE_ADDR"),AppID,0,"Application unsuccessfully received",objApplication.SelectSingleNode("def:ErrorDetails", objNSM2).InnerText)
				'		catch

				'		end try 
				'            End Select

				' Catch ex As Exception
				'	       try	
				'			Dim strEmailText As String = "Lead failed to send reason"
				'			postEmail("", "william.worthington@engagedcrm.co.uk", "EveryDayLoans Lead Failed to Send " & AppID & "",  ex.Message , True, "", True)
				'			saveXMLReceived(Request.ServerVariables("REMOTE_ADDR"),AppID,0,"Application unsuccessfully received", ex.Message)
				'			catch

				'			end try 
				'End Try 
				'            Else
				'                HttpContext.Current.Response.Clear()
				'                HttpContext.Current.Response.Write(0)
				'                'sendFailureEmail(AppID, "500 error")
				'                'saveFailureXML(AppID)
				'            End If

				'            objResponse = Nothing

			Catch e As System.UriFormatException
				HttpContext.Current.Response.Write(e)
				HttpContext.Current.Response.End()

			End Try

		End If

		objInputXMLDoc = Nothing
		objOutputXMLDoc = Nothing

	End Sub

	Private Sub writeXML(ByVal man As String, ByVal fld As String, ByVal val As String)
		If (man = "y") And (Not checkValue(val)) Then
			strErrorMessage += fld & ","
		Else
			If (man = "y") Or ((man = "n") And (val <> "")) Then
				val = regexReplace("<", "&lt;", val)
				val = regexReplace(">", "&gt;", val)
				val = regexReplace(" & ", " and ", val)
				val = regexReplace(" ' ", "&apos;", val)
				val = regexReplace("""", "&quot;", val)

				Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
				If Not (objTest Is Nothing) Then
					objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
				Else
					Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//AddBorrower")
					Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
					Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
					objNewNode.AppendChild(objNewText)
					objApplication.AppendChild(objNewNode)
				End If
			End If
		End If
	End Sub
	Private Sub writeToNode(ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
		If (man = "y") And (Not checkValue(val)) Then
			strErrorMessage += fld & ","
		Else
			If (man = "y") Or ((man = "n") And (val <> "")) Then
				val = regexReplace("<", "&lt;", val)
				val = regexReplace(">", "&gt;", val)
				val = regexReplace(" & ", " and ", val)
				val = regexReplace(" ' ", "&apos;", val)
				val = regexReplace("""", "&quot;", val)

				Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
				If Not (objTest Is Nothing) Then
					objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
				End If
			End If
		End If
	End Sub

	Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal strPost As String)

		Dim strQry As String = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
				   "VALUES(" & formatField(ip, "", "") & ", " & _
				   formatField(soapid, "N", 0) & ", " & _
				   formatField(result, "N", 0) & ", " & _
				   formatField(msg, "", "") & ", " & _
				   formatField(strPost, "", "") & ", " & _
				   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
		executeNonQuery(strQry)
	End Sub


	Shared Function postWebRequestDP(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False, Optional ByVal soap As Boolean = False, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
		If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
			Call Certificate.OverrideCertificateValidation()
		End If
		Try
			Dim objURI As New Uri(url)
			Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
			With objRequest
				.Method = WebRequestMethods.Http.Post
				.KeepAlive = False
				.ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
				.Timeout = 30000
				.ReadWriteTimeout = 30000
				If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
					.UnsafeAuthenticatedConnectionSharing = True
				End If
				If (soap) Then
					.Headers.Add("SOAPAction", url) ' Required for Cisco
				End If
				If (xml) Then
					.ContentType = "application/xml"
				Else
					.ContentType = "application/xml"
				End If
			End With
			' responseWrite("url: " & url & "<br>")
			' responseWrite("post: " & post & "<br>")
			' For Each item In objRequest.Headers
			'     responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
			' Next
			'responseEnd()
			Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
			With objWriter
				.Write(post)
				.Close()
			End With
			Try
				Dim objResponse As HttpWebResponse = objRequest.GetResponse()
				Return objResponse
			Catch err As Exception
				responseWrite(err.Message)
				responseEnd()

			End Try
		Catch e As System.UriFormatException
			HttpContext.Current.Response.Write("URL format error")
			HttpContext.Current.Response.End()
			Return Nothing
		End Try
	End Function

End Class
