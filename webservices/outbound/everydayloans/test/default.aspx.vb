﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLCRM
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private AppID As String = HttpContext.Current.Request.QueryString("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XMLDocument = New XMLDocument
    Private strProductType As String = HttpContext.Current.Request("frmProductType"), strCustomerEmailID As String = ""
    Private strErrorMessage As String = ""

    Public Sub generateXml()
        strXMLURL = ApplicationURL & "/webservices/inbound/soappost.aspx"
        strCustomerEmailID = getAnyField("MediaCampaignOutboundLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/crm/blank.xml"))

        writeXML("n", "MediaCampaignID", strMediaCampaignCampaignReference)
        writeXML("n", "SalesUserID", getAnyField("MediaUserRef", "tblmediausers", "MediaUserID", strTransferUserID))

        Dim strSQL As String = "SELECT * FROM vwxmlcrm WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") Then
                        Call writeXML("n", Column.ColumnName.ToString, Row(Column).ToString)
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Call writeXML("n", Row.Item("StoredDataName"), Row.Item("StoredDataValue"))
            Next
        End If
        dsCache = Nothing
		
        strSQL = "SELECT TOP 1 Note FROM tblnotes WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteActive = 1"
        Dim strNote As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        writeXML("n", "Notes", strNote)		
		
		If (checkValue(strProductType)) Then
			Call writeXML("n", "ProductType", strProductType)
		End If

       	'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	

            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            HttpContext.Current.Response.Write(objReader.ReadToEnd())
            HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then
                Dim objApplication As XmlNode = objOutputXMLDoc.SelectSingleNode("//executeSoapResponse")
                Select Case objApplication.SelectSingleNode("executeSoapStatus").InnerText
                    Case "1"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application successfully received: " & objApplication.SelectSingleNode("executeSoapNo").InnerText))
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", objApplication.SelectSingleNode("executeSoapNo").InnerText, "NULL")
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        sendIndividualApp()
                        If (checkValue(strCustomerEmailID)) Then
                            sendEmails(AppID, strCustomerEmailID, CompanyID)
                        End If
                    Case Else
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText)
						incrementTransferAttempts(AppID, strMediaCampaignID)
                        'sendFailureEmail(AppID, Node.selectSingleNode("executeSoapResult").Text)
                        'saveFailureXML(AppID)
                End Select
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
                'sendFailureEmail(AppID, "500 error")
                'saveFailureXML(AppID)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeXML(ByVal man As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                Else
                    Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Public Sub sendIndividualApp()
        Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
        Dim strFriendlyName As String = getAnyFieldByCompanyID("MediaCampaignFriendlyNameOutbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strFriendlyName)) Then strCompanyName = strFriendlyName
        Dim strDeliveryEmailAddress = getAnyFieldByCompanyID("MediaCampaignDeliveryEmailAddress", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strDeliveryEmailAddress)) Then
            Dim strApplication As String = _
            "Please find below a new application from " & strCompanyName & vbCrLf & vbCrLf & _
            "Ref: " & AppID & vbCrLf & vbCrLf

            Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressPostCode, CallBackDate FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    For Each Column As DataColumn In dsCache.Columns
                        If (checkValue(Row(Column).ToString)) Then
                            strApplication += Regex.Replace(Column.ColumnName.ToString, "([A-Z0-9])", " $1") & ": " & Row(Column).ToString & vbCrLf
                        End If
                    Next
                Next
            End If
            dsCache = Nothing

            strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                        strApplication += Regex.Replace(Row.Item("StoredDataName").ToString, "([A-Z0-9])", " $1") & ": " & Row.Item("StoredDataValue").ToString & vbCrLf
                    End If
                Next
            End If
            dsCache = Nothing

            'Response.Write(strApplication)
            postEmailWithMediaCampaignID("", strDeliveryEmailAddress, "New lead from " & strCompanyName, strApplication, False, "")
        End If

    End Sub

    Private Sub postEmailWithMediaCampaignID(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String)
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=False"
        postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&MediaCampaignID=" & strMediaCampaignID)
    End Sub

End Class
