using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EvoService;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string ProductID = HttpContext.Current.Request["ProductID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];

	private string strRequest = HttpContext.Current.Request["Request"];

	private string strLoanPurpose = "", RateType = "", PaymentFrequency = ""; 
	private int LoanAmount = 0, TermInMonths = 0, BrokerFee = 0;

	private string App1Title = "", App1FirstName = "", App1MiddleName = "", App1Surname = "", App1DOB = "", App1MaritalStatus = "", App1MobileTelephone = "", App1HomeTelephone = "", App1EmailAddress = "", ResidentialStatus = "";
	private string App2Title = "", App2FirstName = "", App2MiddleName = "", App2Surname = "", App2DOB = "", App2MaritalStatus = "", App2MobileTelephone = "", App2HomeTelephone = "", App2EmailAddress = "";

	private string App1EmploymentStatus = "", App1Occupation = "", App1EmployedSince = "";
	private double NetMonthlyIncome = 0.00;

	private string App2EmploymentStatus = "", App2Occupation = "", App2EmployedSince = "";
	private double App2NetMonthlyIncome = 0.00;

	private double MortgageBalance = 0.00, PropertyValuation = 0.00;


	private int CaseId = 0;

	private string ExistingGuid = "";

	private int ExistingCaseId = 0;

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	class ServiceModel
	{
		public string ApiKey { get; set; }
		public string Action { get; set; }
		public bool Debug { get; set; }
		public theRequest Request { get; set; }
	}

	class theRequest
	{
		public int? CaseId { get; set; }
		public string CaseGuid { get; set; }
		public theLoan LoanDetails { get; set; }
		public theApplicantDetails ApplicantDetails { get; set; }
		public theSecurityDetails SecurityDetails { get; set; }

	}

	class theLoan
	{
		public string ExternalReference { get; set; }
		public string LoanPurpose { get; set; } //Enum Value
		public bool Packaging { get; set; }
		public int LoanAmount { get; set; }
		public int TermInMonths { get; set; }
		public string RateType { get; set; } //Enum Value
		public string PaymentFrequency { get; set; } //Enum Value
		public bool LendingFeeUpfront { get; set; }
		public bool? ProductFeeUpfront { get; set; }
		public bool BrokerFeeUpfront { get; set; }
		public double BrokerFee { get; set; }

	}

	class theApplicantDetails
	{
		public bool JointApplication { get; set; }
		public theCustomer Customer { get; set; }

	}

	class theCustomer
	{
		public string JointApplication { get; set; }
		public string Title { get; set; } 
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string Surname { get; set; }
		public string DateOfBirth { get; set; }
		public string MaritalStatus { get; set; } 
		public string Mobile { get; set; }
		public string OtherTelephone { get; set; }
		public string EmailAddress { get; set; }
		public string ResidentialStatus { get; set; } 
		public theCustomerTwo Partner { get; set; }
		public theEmployment Employment { get; set; }
		public List<theAddresses> Addresses = new List<theAddresses>();
	}

	class theCustomerTwo
	{
		public string JointApplication { get; set; }
		public string Title { get; set; } 
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string Surname { get; set; }
		public string DateOfBirth { get; set; }
		public string MaritalStatus { get; set; } 
		public string Mobile { get; set; }
		public string OtherTelephone { get; set; }
		public string EmailAddress { get; set; }
		public string ResidentialStatus { get; set; } 
		public theEmployment Employment { get; set; }
		public List<theAddresses> Addresses = new List<theAddresses>();
	}

	class theEmployment
	{
		public string EmploymentStatus { get; set; }
		public string Occupation { get; set; }
		public string EmployedSince { get; set; }
		public double NetMonthlyIncome { get; set; }
	}

	class theAddresses
	{
		public string AddressType { get; set; } 
		public string FlatNumber { get; set; }
		public string HouseNumber { get; set; }
		public string BuildingName { get; set; }
		public string StreetName { get; set; }
		public string Town { get; set; }
		public string County { get; set; }
		public string Postcode { get; set; }
		public string AddressFrom { get; set; }
	}

	class theSecurityDetails
	{
		public Double PropertyValuation { get; set; }
		public Double MortgageBalance { get; set; }
		public bool CurrentAddressIsSecurity { get; set; }
	}


	//Quote Accept


	class ServiceModelAccept
	{
		public string ApiKey { get; set; }
		public string Action { get; set; }
		public bool Debug { get; set; }
		public theRequestAccept Request { get; set; }
	}

	class theRequestAccept
	{
		public int? CaseId { get; set; }
		public string CaseGuid { get; set; }


	}


	public void httpPost()
	{



		System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;


		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringAcceptedMoney"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		try
		{
			
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwxmlevomoney2020 where AppId = '{0}' and ProductID = '{1}'", AppID, ProductID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{
				//loan details 
				strLoanPurpose = reader["ProductPurpose"].ToString();
				Int32.TryParse(reader["ProductAmount"].ToString(), out LoanAmount);
				Int32.TryParse(reader["ProductTerm"].ToString(), out TermInMonths);
				RateType = reader["ProductScheme"].ToString();
				PaymentFrequency = reader["PaymentFrequency"].ToString();
				Int32.TryParse(reader["ProductBrokerFee"].ToString(), out BrokerFee);

				//App1 Detyails 
				App1Title = reader["CustomerTitle"].ToString();
				App1FirstName = reader["CustomerFirstName"].ToString();
				App1MiddleName = reader["CustomerMiddleNames"].ToString();
				App1Surname = reader["CustomerSurname"].ToString();
				App1DOB = reader["CustomerDOB"].ToString();
				App1MaritalStatus = reader["MaritalStatus"].ToString();
				App1MobileTelephone = reader["MobileTelephone"].ToString();
				App1HomeTelephone = reader["HomeTelephone"].ToString();
				App1EmailAddress = reader["EmailAddress"].ToString();
				App1EmploymentStatus = reader["EmploymentStatus"].ToString();
				App1Occupation = reader["JobTitle"].ToString();
				App1EmployedSince = reader["StartDate"].ToString();
				Double.TryParse(reader["ProductMortgageBalance"].ToString(), out MortgageBalance);
				Double.TryParse(reader["ProductPropertyValue"].ToString(), out PropertyValuation);
				App1EmployedSince = reader["StartDate"].ToString();

				Double.TryParse(reader["NetAmount"].ToString(), out NetMonthlyIncome);
				Int32.TryParse(reader["EvolutionCaseID"].ToString(), out ExistingCaseId);
				ExistingGuid = reader["EvolutionCaseGuid"].ToString();


			}

			SqlCommand myCommandApp2 = new SqlCommand(string.Format("Select * FROM vwxmlevomoney2020App2 where AppId = {0}", AppID), connection);

			var readerapp2 = myCommandApp2.ExecuteReader();

			while (readerapp2.Read())
			{

				//App2 Detyails 
				App2Title = reader["CustomerTitle"].ToString();
				App2FirstName = reader["CustomerFirstName"].ToString();
				App2MiddleName = reader["CustomerMiddleNames"].ToString();
				App2Surname = reader["CustomerSurname"].ToString();
				App2DOB = reader["CustomerDOB"].ToString();
				App2MaritalStatus = reader["MaritalStatus"].ToString();
				App2MobileTelephone = reader["MobileTelephone"].ToString();
				App2HomeTelephone = reader["HomeTelephone"].ToString();
				App2EmailAddress = reader["EmailAddress"].ToString();
				App2EmploymentStatus = reader["EmploymentStatus"].ToString();
				App2Occupation = reader["JobTitle"].ToString();
				App2EmployedSince = reader["StartDate"].ToString();

				Double.TryParse(reader["NetAmount"].ToString(), out App2NetMonthlyIncome);


			}


			ServiceModel model = new ServiceModel();
			model.ApiKey = "01acfbraV8YCMbQWLiIvDbhiCfRMqjV12H6O";
			if (ExistingCaseId > 0 && ExistingGuid != null)
			{
				model.Action = "QuoteRecalc";
			}
			else
			{
				model.Action = "Quote";
			}
			model.Debug = true;
			theRequest request = new theRequest();

			if (ExistingCaseId > 0 && ExistingGuid != null)
			{
				request.CaseId = ExistingCaseId;
				request.CaseGuid = ExistingGuid;
			}
			else
			{
				request.CaseId = null;
				request.CaseGuid = null;
			}
			
			theLoan loandetails = new theLoan();
			loandetails.ExternalReference = AppID;
			loandetails.LoanPurpose = strLoanPurpose;
			loandetails.Packaging = false;
			loandetails.LoanAmount = LoanAmount;
			loandetails.TermInMonths = (TermInMonths * 12);
			loandetails.RateType = RateType;
			loandetails.PaymentFrequency = PaymentFrequency;
			loandetails.LendingFeeUpfront = false;
			loandetails.ProductFeeUpfront = false;
			loandetails.BrokerFeeUpfront = false;
			loandetails.BrokerFee = BrokerFee;
			request.LoanDetails = loandetails;

			theApplicantDetails applicantdetails = new theApplicantDetails();
			applicantdetails.JointApplication = false;

			
			theCustomer customer = new theCustomer();
			customer.Title = App1Title;
			customer.FirstName = App1FirstName;
			customer.MiddleName = App1MiddleName;
			customer.Surname = App1Surname;
			customer.DateOfBirth = App1DOB;
			customer.MaritalStatus = App1MaritalStatus;
			customer.Mobile = App1MobileTelephone;
			customer.OtherTelephone = App1HomeTelephone;
			customer.EmailAddress = App1EmailAddress;


			theCustomerTwo customertwo = new theCustomerTwo();
			
			if (App2Title != "")
			{
				
				customertwo.Title = App2Title;
				customertwo.FirstName = App2FirstName;
				customertwo.MiddleName = App2MiddleName;
				customertwo.Surname = App2Surname;
				customertwo.DateOfBirth = App2DOB;
				customertwo.MaritalStatus = App2MaritalStatus;
				customertwo.Mobile = App2MobileTelephone;
				customertwo.OtherTelephone = App2HomeTelephone;
				customertwo.EmailAddress = App2EmailAddress;

				theEmployment employmenttwo = new theEmployment();
				employmenttwo.EmploymentStatus = App2EmploymentStatus;
				employmenttwo.Occupation = App2Occupation;
				employmenttwo.EmployedSince = App2EmployedSince;
				employmenttwo.NetMonthlyIncome = App2NetMonthlyIncome;
				customertwo.Employment = employmenttwo;

				customer.Partner = customertwo;
			
			}
			else
			{
				customer.Partner = null;
		
			}


			theEmployment employment = new theEmployment();
			employment.EmploymentStatus = App1EmploymentStatus;
			employment.Occupation = App1Occupation;
			employment.EmployedSince = App1EmployedSince;
			employment.NetMonthlyIncome = NetMonthlyIncome;
			customer.Employment = employment;


			List<theAddresses> addresses = new List<theAddresses>();


			SqlCommand myCommandAddress = new SqlCommand(string.Format("Select Top(1) * FROM vwxmlevomoney2020addresses where AppId = {0}", AppID), connection);

			var readeraddress = myCommandAddress.ExecuteReader();

			while (readeraddress.Read())
			{
				addresses.Add(
					new theAddresses
					{
						AddressType = "CurrentAddress",
						FlatNumber = null,
						HouseNumber = readeraddress["AddressLine1"].ToString().Substring(0, 3),
						BuildingName = null,
						StreetName = readeraddress["AddressLine1"].ToString(),
						Town = readeraddress["Town"].ToString(),
						County = readeraddress["County"].ToString(),
						Postcode = readeraddress["PostCode"].ToString(),
						AddressFrom = readeraddress["MoveInDate"].ToString()
					}
				);

				ResidentialStatus = readeraddress["ResidentialStatus"].ToString();

			}

			customer.ResidentialStatus = ResidentialStatus;


			if (App2Title != "" || App2Title != null)
			{
				customertwo.ResidentialStatus = ResidentialStatus;
				customertwo.Addresses = addresses;
			}
			customer.Addresses = addresses;

			applicantdetails.Customer = customer;

			if (ExistingCaseId > 0 && ExistingGuid != null)
			{
				
			}
			else
			{
				request.ApplicantDetails = applicantdetails;
			}
			



			theSecurityDetails securitydetails = new theSecurityDetails();
			securitydetails.CurrentAddressIsSecurity = true;
			securitydetails.MortgageBalance = MortgageBalance;
			securitydetails.PropertyValuation = PropertyValuation;

			if (ExistingCaseId > 0 && ExistingGuid != null)
			{

			}
			else
			{
				request.SecurityDetails = securitydetails;
			}
			

			model.Request = request;

			JsonSerializerSettings settings = new JsonSerializerSettings();


			if (ExistingCaseId > 0 && ExistingGuid != null)
			{
				settings.NullValueHandling = NullValueHandling.Ignore;
			}




			string json = JsonConvert.SerializeObject(model, settings);

			if (strRequest == "Y")
			{
				HttpContext.Current.Response.ContentType = "application/json";
				HttpContext.Current.Response.Write(json);
				HttpContext.Current.Response.End();

			}

			string strPostURL = "https://uat.evolutionintroducers.co.uk/api/v1";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(strPostURL);


			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strPostURL, contentBody).Result;

			var responseContent = res.Content;

			var jsonnew = responseContent.ReadAsStringAsync().Result;

			

			var jo = JObject.Parse(jsonnew);

			//HttpContext.Current.Response.Write(jsonnew);
			//HttpContext.Current.Response.End();


			var Error = jo["Error"].ToString();
		
			if (Error == "True")
			{
				var ErrorMessage = jo["Errors"][0]["Element"].ToString();
				var Status = jo["Errors"][0]["Status"].ToString();
				Response.Write("0|" + AppID + "|" + ErrorMessage + "-" + Status + "|||||");
			
			}
			else
			{


				var Decision = jo["Response"]["Case"]["Decision"].ToString();

				if (Decision.Contains("Duplicate") || Decision.Contains("Decline") || Decision.Contains("DecisionFail"))
				{
					Response.Write("0|" + AppID + "|" + Decision + "");
				}
				else
				{
					Int32.TryParse(jo["Response"]["Case"]["CaseId"].ToString(), out CaseId);
	
					var CaseGuid = jo["Response"]["Case"]["CaseGuid"].ToString();

					var Plan = jo["Response"]["Product"]["Plan"].ToString();
					var ProductRange = jo["Response"]["Product"]["ProductRange"].ToString();
					var MaxLoanAmount = jo["Response"]["Product"]["MaximumAvailableAmount"].ToString();
					var MaxTerm = jo["Response"]["Product"]["MaximumAvailableTerm"].ToString();


					Response.Write("1|" + AppID + "|" + Plan + "|" + ProductRange + "|" + MaxLoanAmount + "|" + MaxTerm + "|" + CaseId + "|" + CaseGuid + "");



				}
			}

			connection.Close();

		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}



	}

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		//string strQry = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES(" + common.formatField(ip, "", "") + ", " + common.formatField(soapid, "N", 0) + ", " + common.formatField(result, "N", 0) + ", " + common.formatField(msg, "", "") + ", " + common.formatField(strPost, "", "") + ", " + common.formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) + ") ";
		//Common.executeNonQuery(strQry);
	}

	class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}


}