using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EvoService;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string ProductID = HttpContext.Current.Request["ProductID"];
	private string CaseID = HttpContext.Current.Request["CaseID"];
	private string CaseGuid = HttpContext.Current.Request["CaseGuid"];



	private int CaseId = 0;

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	//Quote Accept


	class ServiceModelAccept
	{
		public string ApiKey { get; set; }
		public string Action { get; set; }
		public bool Debug { get; set; }
		public theRequestAccept Request { get; set; }
	}

	class theRequestAccept
	{
		public int? CaseId { get; set; }
		public string CaseGuid { get; set; }


	}


	public void httpPost()
	{

	

		System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;


		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringAcceptedMoney"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		try
		{
			
		
					ServiceModelAccept modelaccept = new ServiceModelAccept();
					modelaccept.ApiKey = "01acfbraV8YCMbQWLiIvDbhiCfRMqjV12H6O";
					modelaccept.Action = "QuoteAccept";
					modelaccept.Debug = true;
					theRequestAccept requestacept = new theRequestAccept();
					requestacept.CaseGuid = CaseGuid;
					requestacept.CaseId = CaseId;
					modelaccept.Request = requestacept;

					string strPostURLAccept = "https://uat.evolutionintroducers.co.uk/api/v1";
					HttpClient clientAccept = new HttpClient();
					clientAccept.BaseAddress = new Uri(strPostURLAccept);

					modelaccept.Request = requestacept;

					string json = JsonConvert.SerializeObject(modelaccept);

					

					HttpContent contentBodyAccept = new StringContent(json);
					contentBodyAccept.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

					var resaccept = clientAccept.PostAsync(strPostURLAccept, contentBodyAccept).Result;

					var responseContentaccept = resaccept.Content;

					var jsonnewaccpet = responseContentaccept.ReadAsStringAsync().Result;

					var joaccept = JObject.Parse(jsonnewaccpet);

					

					var Decision = joaccept["Response"]["Case"]["Decision"].ToString();

					if (Decision.Contains("Accepted"))
					{
						var FactFindID = joaccept["Response"]["Case"]["FactFindId"].ToString();

						Response.Write("1|" + FactFindID);
	

					}
					else
					{
						Response.Write("0|" + Decision);
					}

			



		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}



	}




}