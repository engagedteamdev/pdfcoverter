﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net

' ** Revision history **
'
' 09/09/2013    - File created
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID"), MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strXMLURL As String = "", strAffiliateID As String = "", strCustomerEmailID As String = ""
    Private strPost As String = "", strErrorMessage As String = ""
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Public Sub httpPost()
        If (strEnvironment = "live") Then
            strXMLURL = "http://poweredby.loanfinder.co.uk/cascadeCustomer.php"
        Else
            strXMLURL = "http://poweredby.loanfinder.co.uk/cascadeCustomer.php"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Ratio%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "RatioAffiliateID") Then strAffiliateID = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        strCustomerEmailID = getAnyField("MediaCampaignOutboundLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()

        strPost += "mode=xml&affiliate=" & strMediaCampaignCampaignReference

        Dim strSQL As String = "SELECT * FROM vwxmlratio WHERE AppID = " & AppID
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                        Dim strParent As String = ""
                        If (UBound(arrName) > 1) Then
                            For y As Integer = 0 To UBound(arrName) - 1
                                If (y = 0) Then
                                    strParent += arrName(y)
                                Else
                                    strParent += "/" & arrName(y)
                                End If
                            Next
                        Else
                            strParent = arrName(0)
                        End If
                        strPost += "&" & arrName(UBound(arrName)) & "=" & Row(Column).ToString
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        'HttpContext.Current.Response.Write(strXMLURL & "?" & strPost)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then

        Else
            ' Post the SOAP message.
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Generated To Ratio", strPost)
            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, strPost)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then

                Dim strStatus As String = "", strMessage As String = "", strReference As String = "", strPrice As String = ""
                Dim objStatus As XmlNode = objOutputXMLDoc.SelectSingleNode("//Result")
                Dim objMessage As XmlNode = objOutputXMLDoc.SelectSingleNode("//RedirectURL")
                Dim objReference As XmlNode = objOutputXMLDoc.SelectSingleNode("//CustomerID")
                Dim objPrice As XmlNode = objOutputXMLDoc.SelectSingleNode("//Commission")
                If (Not objStatus Is Nothing) Then
                    strStatus = objStatus.InnerText
                End If
                If (Not objMessage Is Nothing) Then
                    strMessage = objMessage.InnerText
                End If
                If (Not objReference Is Nothing) Then
                    strReference = objReference.InnerText
                End If
                If (Not objPrice Is Nothing) Then
                    strPrice = objPrice.InnerText
                End If

                Select Case strStatus
                    Case "Accepted"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Ratio"))
                        updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", strReference, "NULL")
                        If (IsNumeric(strPrice)) Then
                            updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignCostOutbound", "M", strPrice, 0)
                        End If
                        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                        If (checkValue(strCustomerEmailID)) Then
                            sendEmails(AppID, strCustomerEmailID, CompanyID)
                        End If
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To Ratio", objOutputXMLDoc.InnerXml)
                    Case Else
                        saveNote(AppID, Config.DefaultUserID, "Application Declined by Ratio: " & strMessage)
                        HttpContext.Current.Response.Clear()
						saveUpdatedDate(AppID, Config.DefaultUserID, "PartnerDuplicate")
                        HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Ratio: " & strMessage))
                        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To Ratio", objOutputXMLDoc.InnerXml)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                        incrementTransferAttempts(AppID, strMediaCampaignID)
                End Select

                objOutputXMLDoc = Nothing
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|Data error")
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing

        End If

    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
