using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OptimumService;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;


public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];
	

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	public void httpPost()
	{


		ServiceInterfaceClient client = new ServiceInterfaceClient();


		ServiceModel model = new ServiceModel();
	
		List<string> valid = new List<string>();

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		try
		{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwxmloptimumcredit where AppId = {0}", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{

				decimal Amount = 0;
				decimal.TryParse(reader["Amount"].ToString(), out Amount);

				decimal PropertyValue = 0;
				decimal.TryParse(reader["PropertyValue"].ToString(), out PropertyValue);

				decimal MortgageBalance = 0;
				decimal.TryParse(reader["MortgageBalance"].ToString(), out MortgageBalance);

				int ProductTerm = 0;
				int.TryParse(reader["ProductTerm"].ToString(), out ProductTerm);

				int AddressMonths = 0;
				int.TryParse(reader["AddressMonths"].ToString(), out AddressMonths);

				int NoOfBedrooms = 0;
				int.TryParse(reader["NoOfBedrooms"].ToString(), out NoOfBedrooms);

				int Address2Months = 0;
				int.TryParse(reader["Address2TimeAtAddress"].ToString(), out Address2Months);

				int Address3Months = 0;
				int.TryParse(reader["Address3TmeAtAddress"].ToString(), out Address3Months);

				DateTime? app1DOB = null;
				DateTime? propertyPurchaseDate = null;
				DateTime? app2DOB = null;

				if (!string.IsNullOrEmpty(reader["App1DOB"].ToString()))
				{
					app1DOB = DateTime.Parse(reader["App1DOB"].ToString());
				}

				if (!string.IsNullOrEmpty(reader["PropertyPurchaseDate"].ToString()))
				{
					propertyPurchaseDate = DateTime.Parse(reader["PropertyPurchaseDate"].ToString());
				}

				if (!string.IsNullOrEmpty(reader["App2DOB"].ToString()))
				{
					app2DOB = DateTime.Parse(reader["App2DOB"].ToString());
				}

				if (app1DOB != null)
				{
					model = new ServiceModel()
					{
						Amount = Amount,
						PrductTerm = ProductTerm,
						AddressHouseName = reader["AddressHouseName"].ToString(),
						AddressHouseNumber = reader["AddressHouseNumber"].ToString(),
						AddressLine1 = reader["AddressLine1"].ToString(),
						AddressPostCode = reader["AddressPostCode"].ToString(),
						AddressMonths = AddressMonths,
						App1DOB = app1DOB,
						App1EmploymentStatus = reader["App1EmploymentStatus"].ToString(),
						App1Title = reader["App1Title"].ToString(),
						App1FirstName = reader["App1Firstname"].ToString(),
						App1Surname = reader["App1Surname"].ToString(),
						App1MaritalStatus = reader["App1MaritalStatus"].ToString(),
						PropertyValue = PropertyValue,
						MortgageBalance = MortgageBalance,
						NoOfBedrooms = NoOfBedrooms,
						PropertyStyle = reader["PropertyStyle"].ToString(),
						PropertyType = reader["PropertyType"].ToString(),
						PropertyPurchaseDate = propertyPurchaseDate,
						App2Title = reader["App2Title"].ToString(),
						App2FirstName = reader["App2FirstName"].ToString(),
						App2Surname = reader["App2Surname"].ToString(),
						App2DOB = app2DOB,
						App2EmploymentStatus = reader["App2EmploymentStatus"].ToString(),
						App1HomeTelephone = reader["App1HomeTelephone"].ToString(),
						App1MobileTelephone = reader["App1MobileTelephone"].ToString(),
						App2HomeTelephone = reader["App2HomeTelephone"].ToString(),
						App2MobileTelephone = reader["App2MobileTelephone"].ToString(),
						App1EmailAddress = reader["App1EmailAddress"].ToString(),
						App2EmailAddress = reader["App2EmailAddress"].ToString(),
						App2MaritalStatus = reader["App2MaritalStatus"].ToString(),
						Address2Line1 = reader["Address2Line1"].ToString(),
						Address2PostCode = reader["Address2PostCode"].ToString(),
						Address2Months = Address2Months,
						Address3Months = Address3Months,
						Address3Line1 = reader["Address3Line1"].ToString(),
						Address3PostCode = reader["Address3PostCode"].ToString(),
						Address2HouseName = reader["Address2HouseName"].ToString(),
						Address2HouseNumber = reader["Address2HouseNumber"].ToString(),
						Address3HouseName = reader["Address3HouseName"].ToString(),
						Address3HouseNumber = reader["Address3HouseNumber"].ToString()

					};

				}
				else
				{
					if (app1DOB == null)
					{
						valid.Add("App 1 DOB is empty");
					}
				}
			}


			
			connection.Close();
			
			

			if (!valid.Any())
			{
				valid = ValidationCheck(model);
			}



		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}
	

		if (!valid.Any())
		{

		

			if (!valid.Any())
			{
				

				try
				{
					

					XMLQuickApplication application = new XMLQuickApplication();
					application.AccountId = 0;
					application.BrokerId = "10047";
					application.LeadSource = "Broker";
					application.ApplicationDetails = new XMLApplicationData()
					{
						Advance = model.Amount,
						Term = model.PrductTerm,
						BrokerFee = 0,
						TypeOfLoan = TypeOfLoan.Residential

					};
					application.QuoteData = new XMLQuoteData()
					{
						AdviceFee = 0,
						AdviceFeePaidUpfront = 1,
						ValuationFee = 0,
						ValuationFeePaidUpfront = 1,
						BrokerFeePaidUpfront = 0,
						ApplicationFee = 0,
						ApplicationFeePaidUpfront = 1,
						OtherFee = 0,
						OtherFeePaidUpfront = 1,
						LenderFeePaidUpfront = 1,
						ProductFeePaidUpfront = 1

					};

					if (model.App2Title == null || model.App2Title == "")
					{
						XMLCustomerData[] thecustomers = new XMLCustomerData[1];

						XMLCustomerData acustomer = new XMLCustomerData();
						acustomer.Title = model.App1Title;
						acustomer.FirstName = model.App1FirstName;
						acustomer.LastName = model.App1Surname;
						acustomer.DOB = model.App1DOB;
						acustomer.MaritalStatus = GetMaritalStatus(model.App1MaritalStatus);
						acustomer.EmploymentStatus = GetEmploymentType(model.App1EmploymentStatus);
						if (!string.IsNullOrEmpty(model.Address2Line1))
						{

							if (!string.IsNullOrEmpty(model.Address3Line1))
							{
								XMLAddressData[] addresshistories = new XMLAddressData[2];

								XMLAddressData addressHistory1 = new XMLAddressData()
								{
									HouseName = model.Address2HouseName,
									HouseNumber = model.Address2HouseNumber,
									AddressLine1 = model.Address2Line1,
									PostCode = model.Address2PostCode,
									TimeAtAddress = model.Address2Months
								};

								XMLAddressData addressHistory2 = new XMLAddressData()
								{
									HouseName = model.Address3HouseName,
									HouseNumber = model.Address3HouseNumber,
									AddressLine1 = model.Address3Line1,
									PostCode = model.Address3PostCode,
									TimeAtAddress = model.Address3Months
								};
								addresshistories[0] = addressHistory1;
								addresshistories[1] = addressHistory2;
								acustomer.AddressHistory = addresshistories;
							}
							else
							{
								XMLAddressData[] addresshistories = new XMLAddressData[1];
								XMLAddressData addressHistory1 = new XMLAddressData()
								{
									HouseName = model.Address2HouseName,
									HouseNumber = model.Address2HouseNumber,
									AddressLine1 = model.Address2Line1,
									PostCode = model.Address2PostCode,
									TimeAtAddress = model.Address2Months
								};
								addresshistories[0] = addressHistory1;
								acustomer.AddressHistory = addresshistories;
							}

						}
						


						thecustomers[0] = acustomer;
						application.Customers = thecustomers;

						XMLAddressData address = new XMLAddressData()
						{
							HouseName = model.AddressHouseName,
							HouseNumber = model.AddressHouseNumber,
							PostCode = model.AddressPostCode,
							AddressLine1 = model.AddressLine1,
							TimeAtAddress = model.AddressMonths
						};
						acustomer.AddressResidential = address;


						

					}
					else
					{
						XMLCustomerData[] thecustomers = new XMLCustomerData[2];

						XMLCustomerData acustomer = new XMLCustomerData();
						acustomer.Title = model.App1Title;
						acustomer.FirstName = model.App1FirstName;
						acustomer.LastName = model.App1Surname;
						acustomer.DOB = model.App1DOB;
						acustomer.MaritalStatus = GetMaritalStatus(model.App1MaritalStatus);
						acustomer.EmploymentStatus = GetEmploymentType(model.App1EmploymentStatus);
						if (!string.IsNullOrEmpty(model.Address2Line1))
						{

							if (!string.IsNullOrEmpty(model.Address3Line1))
							{
								XMLAddressData[] addresshistories = new XMLAddressData[2];

								XMLAddressData addressHistory1 = new XMLAddressData()
								{
									HouseName = model.Address2HouseName,
									HouseNumber = model.Address2HouseNumber,
									AddressLine1 = model.Address2Line1,
									PostCode = model.Address2PostCode,
									TimeAtAddress = model.Address2Months
								};

								XMLAddressData addressHistory2 = new XMLAddressData()
								{
									HouseName = model.Address3HouseName,
									HouseNumber = model.Address3HouseNumber,
									AddressLine1 = model.Address3Line1,
									PostCode = model.Address3PostCode,
									TimeAtAddress = model.Address3Months
								};
								addresshistories[0] = addressHistory1;
								addresshistories[1] = addressHistory2;
								acustomer.AddressHistory = addresshistories;
							}
							else
							{
								XMLAddressData[] addresshistories = new XMLAddressData[1];
								XMLAddressData addressHistory1 = new XMLAddressData()
								{
									HouseName = model.Address2HouseName,
									HouseNumber = model.Address2HouseNumber,
									AddressLine1 = model.Address2Line1,
									PostCode = model.Address2PostCode,
									TimeAtAddress = model.Address2Months
								};
								addresshistories[0] = addressHistory1;
								acustomer.AddressHistory = addresshistories;
							}

						}


						application.Customers = thecustomers;
						
						XMLAddressData address = new XMLAddressData()
						{
							HouseName = model.AddressHouseName,
							HouseNumber = model.AddressHouseNumber,
							PostCode = model.AddressPostCode,
							AddressLine1 = model.AddressLine1,
							TimeAtAddress = model.AddressMonths
						};
						acustomer.AddressResidential = address;

						XMLCustomerData app2customer = new XMLCustomerData();
						app2customer.Title = model.App2Title;
						app2customer.FirstName = model.App2FirstName;
						app2customer.LastName = model.App2Surname;
						app2customer.DOB = model.App2DOB;
						app2customer.MaritalStatus = GetMaritalStatus(model.App2MaritalStatus);
						app2customer.EmploymentStatus = GetEmploymentType(model.App2EmploymentStatus);
						application.Customers = thecustomers;
						app2customer.AddressResidential = address;
						thecustomers[0] = acustomer;
						thecustomers[1] = app2customer;


					}




					application.SecurityAddress = new XMLSecurityAddressData()
					{
						HouseName = model.AddressHouseName,
						HouseNumber = model.AddressHouseNumber,
						PostCode = model.AddressPostCode,
						AddressLine1 = model.AddressLine1,
						PropertyType = GetPropertyType(model.PropertyType),
						PropertyStyle = GetPropertyStyle(model.PropertyStyle),
						NumberOfBedrooms = model.NoOfBedrooms,
						CustomerEstimatedValue = model.PropertyValue,
						FirstMortgageBalance = model.MortgageBalance,
						PropertyPurchaseDate = model.PropertyPurchaseDate,
						TimeAtAddress = model.AddressMonths

					};

					XMLSubBroker[] thesubbroker = new XMLSubBroker[1];

					XMLSubBroker asubbroker = new XMLSubBroker();
					asubbroker.FrnNumber = "08047869";
					asubbroker.Name = "Engaged Solutions Ltd";
					asubbroker.HouseNameNumber = "Sanctum House, Unit 9 Hayfield Business Park";
					asubbroker.AddressLine1 = "Field Lane";
					asubbroker.PostCode = "DN9 3FL";
					asubbroker.WebAddress = "https://engaged-solutions.co.uk/";
					asubbroker.Telephone = "0800 0901 546";
					asubbroker.SubBrokerType = SubBrokerType.Introducer;
					thesubbroker[0] = asubbroker;
					application.SubBrokerDetails = thesubbroker;

					XMLIntroducer[] theintroducer = new XMLIntroducer[1];

					XMLIntroducer anintroducer = new XMLIntroducer();
					anintroducer.Name = "Engaged Solutions Ltd";
					anintroducer.PostCode = "DN9 3FL";
					anintroducer.Telephone = "0800 0901 546";
					anintroducer.SubBrokerType = SubBrokerType.Introducer;
					theintroducer[0] = anintroducer;

					application.IntroducerDetails = theintroducer;

					//ProcessQuickApplication is the Service Reference Method to Post through to the API
					var request = client.ProcessQuickApplicationUsingDataContract(application);

					//XmlDocument myXml = new XmlDocument();
					//XPathNavigator xNav = myXml.CreateNavigator();
					//XmlSerializer y = new XmlSerializer(request.GetType());
					//using (var xs = xNav.AppendChild())
					//{
					//	y.Serialize(xs, request);
					//}
					//HttpContext.Current.Response.ContentType = "text/xml";
					//HttpContext.Current.Response.Write(myXml.OuterXml);
					//HttpContext.Current.Response.End();

					var MonthlyRepayment = request.Quote.First().Product.OrderBy(y => y.LoanRate).First().MonthlyRepayment;
					var LoanTerm = request.Quote.First().Product.OrderBy(y => y.LoanRate).First().LoanTerm;
					var LoanAmount = request.Quote.First().Product.OrderBy(y => y.LoanRate).First().LoanAmount;
					var LoanRate = request.Quote.First().Product.OrderBy(y => y.LoanRate).First().LoanRate;
                    var ProductName = request.Quote.First().Product.OrderBy(y => y.LoanRate).First().ProductName;

					if (request.AccountId > 0)
					{
						HttpContext.Current.Response.Write("1|" + request.AccountId + "|" + MonthlyRepayment + "|" + LoanAmount + "|" + LoanTerm + "|" + String.Format("{0:.##}", LoanRate) + "|" + ProductName);

					}
					else
					{
						HttpContext.Current.Response.Write("0|Failed To Send");
						
					}


				}
				catch (Exception e)
				{
					HttpContext.Current.Response.Write(e.Message);
					HttpContext.Current.Response.End();
				}

				


			}
			else
			{
				string errors = "0| ";
				foreach (var item in valid)
				{
					errors += string.Format("{0} ", item);
				}
				Response.Write(errors);
			}
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("0| ");

			foreach (var item in valid)
			{
				sb.Append(string.Format("{0}, ", item));
			}
			Response.Write(sb.ToString());
		}

	}


	//The code used to Serialize the QuickApplication Object 
	public static XmlElement SerializeToXmlElement(object o)
	{
		XmlDocument doc = new XmlDocument();

	

		using (XmlWriter writer = doc.CreateNavigator().AppendChild())
		{
			new XmlSerializer(o.GetType()).Serialize(writer, o);
		}

		return doc.DocumentElement;


	}



	private static List<string> ValidationCheck(ServiceModel model)
	{
		List<string> RequiredPropertyList = new List<string>();

		return RequiredPropertyList;
	}

	private void saveXMLReceived(string ip, string soapid, int result, string msg, string strPost)
	{
		//string strQry = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " + "VALUES(" + common.formatField(ip, "", "") + ", " + common.formatField(soapid, "N", 0) + ", " + common.formatField(result, "N", 0) + ", " + common.formatField(msg, "", "") + ", " + common.formatField(strPost, "", "") + ", " + common.formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) + ") ";
		//Common.executeNonQuery(strQry);
	}

	class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}

	class ServiceModel
	{
		
		public string App1Customer_Title { get; set; }
		public decimal? App1Customer_Salary { get; set; }
		public bool CorrespondenceAddressSame { get; set; }
		public decimal Amount { get; set; }
		public int PrductTerm { get; set; }
		public string AddressHouseName { get; set; }
		public string AddressHouseNumber { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressPostCode { get; set; }
		public int AddressMonths { get; set; }
		public DateTime? App1DOB { get; set; }
		public string App1EmploymentStatus { get; set; }
		public string App1Title { get; set; }
		public string App1FirstName { get; set; }
		public string App1Surname { get; set; }
		public string App1MaritalStatus { get; set; }
		public decimal PropertyValue { get; set; }
		public decimal MortgageBalance { get; set; }
		public int NoOfBedrooms { get; set; }
		public string PropertyStyle { get; set; }
		public string PropertyType { get; set; }
		public DateTime? PropertyPurchaseDate { get; set; }

		public string App2Title { get; set; }
		public string App2FirstName { get; set; }
		public string App2Surname { get; set; }
		public DateTime? App2DOB { get; set; }
		public string App2EmploymentStatus { get; set; }
		public string App1HomeTelephone { get; set; }
		public string App1MobileTelephone { get; set; }
		public string App2HomeTelephone { get; set; }
		public string App2MobileTelephone { get; set; }
		public string App1EmailAddress { get; set; }
		public string App2EmailAddress { get; set; }
		public string App2MaritalStatus { get; set; }
		public string Address2HouseName { get; set; }
		public string Address2HouseNumber { get; set; }
		public string Address2Line1 { get; set; }
		public string Address2PostCode { get; set; }
		public string Address3HouseName { get; set; }
		public string Address3HouseNumber { get; set; }
		public int Address2Months { get; set; }
		public string Address3Line1 { get; set; }
		public string Address3PostCode { get; set; }
		public int Address3Months { get; set; }

	}

	private MaritalStatus GetMaritalStatus(string strMaritalStatus)
	{
		switch (strMaritalStatus)
		{
			case "Married":
				return MaritalStatus.Married;
			case "Civil Partnership":
				return MaritalStatus.CivilPartnership;
			case "Co habiting":
				return MaritalStatus.Cohabiting;
			case "Single":
				return MaritalStatus.Single;
			case "Divorced":
				return MaritalStatus.Divorced;
			case "Separated":
				return MaritalStatus.Separated;
			case "Widowed":
				return MaritalStatus.Widowed;
			default:
				return MaritalStatus.Single;

		}
	}

	private EmploymentType GetEmploymentType(string strEmploymentType)
	{
		switch (strEmploymentType)
		{
			case "Employed":
				return EmploymentType.Employed;
			case "Unemployed":
				return EmploymentType.NotWorkingUnemployed;
			case "Self Employed":
				return EmploymentType.SelfEmployed;
			case "Retired":
				return EmploymentType.Retired;
			default:
				return EmploymentType.Employed;

		}
	}


	private PropertyType GetPropertyType(string strPropertyType)
	{
		switch (strPropertyType)
		{
			case "Bungalow":
				return PropertyType.Bungalow;
			case "Flat Maisonette":
				return PropertyType.FlatMaisonette;
			case "House":
				return PropertyType.House;
			default:
				return PropertyType.NotKnown;

		}
	}

	private PropertyStyle GetPropertyStyle(string strPropertyStyle)
	{
		switch (strPropertyStyle)
		{
			case "Converted":
				return PropertyStyle.Converted;
			case "Detached":
				return PropertyStyle.Detached;
			case "End Terrace":
				return PropertyStyle.EndTerrace;
			case "Mid Terrace":
				return PropertyStyle.MidTerrace;
			case "Not Known":
				return PropertyStyle.NotKnown;
			case "Purpose Built":
				return PropertyStyle.PurposeBuilt;
			case "Semi Detached":
				return PropertyStyle.SemiDetached;
			case "Terraced":
				return PropertyStyle.Terraced;
			default:
				return PropertyStyle.NotKnown;

		}
	}
}