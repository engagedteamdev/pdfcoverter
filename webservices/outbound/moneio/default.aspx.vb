﻿Imports Config, Common, CommonSave

Partial Class EmailMoneio
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")

    Public Sub generateEmail()
        Dim strEmailBody As String = ""
        Dim strSQL As String = "SELECT * FROM vwxmlmoneio WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        strEmailBody += Column.ColumnName.ToString & ": " & Row(Column).ToString & vbCrLf
                    End If
                Next
            Next
        End If
        dsCache = Nothing
        strEmailBody += "HearFrom: " & strMediaCampaignCampaignReference

		incrementTransferAttempts(AppID, strMediaCampaignID)
        Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
        Dim strDeliveryEmailAddress = getAnyFieldByCompanyID("MediaCampaignDeliveryEmailAddress", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        postEmail("", strDeliveryEmailAddress, "New Lead From " & strCompanyName, strEmailBody, False, "")
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by Moneio"))
        setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
    End Sub

End Class
