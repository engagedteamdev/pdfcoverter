﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class EmailCPC
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAction As String = ""
    Private strMediaID As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strMailTo As String = HttpContext.Current.Request("MediaCampaignDeliveryEmailAddress")

    Public Sub sendEmail()
        If (sendIndividualApp() = "1") Then
            incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(1 & "|" & encodeURL("Email Sent to CPC"))
            setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
        Else
            incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Email Not Sent to CPC"))
        End If
    End Sub

    Public Function sendIndividualApp() As String
        Dim strResult As String = ""
        Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
        Dim strFriendlyName As String = getAnyFieldByCompanyID("MediaCampaignFriendlyNameOutbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strFriendlyName)) Then strCompanyName = strFriendlyName
        Dim strDeliveryEmailAddress = getAnyFieldByCompanyID("MediaCampaignDeliveryEmailAddress", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
        If (checkValue(strDeliveryEmailAddress)) Then
            Dim strApplication As String = _
            "Please find below a new application from " & strCompanyName & vbCrLf & vbCrLf & _
            "Ref: " & AppID & vbCrLf & vbCrLf

            Dim strSQL As String = "SELECT MediaCampaignFriendlyNameInbound, Amount, ProductType, ProductTerm, ProductPurpose, App1Firstname, App1Surname, App1DOB, App1Sex, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, App2Firstname, App2Surname, App2DOB, App2Sex, App2MobileTelephone, App2EmailAddress, AddressHouseNumber, AddressHouseName, AddressLine1, AddressPostCode, CallBackDate FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    For Each Column As DataColumn In dsCache.Columns
                        If (checkValue(Row(Column).ToString)) Then
                            strApplication += Regex.Replace(Column.ColumnName.ToString, "([A-Z0-9])", " $1") & ": " & Row(Column).ToString & vbCrLf
                        End If
                    Next
                Next
            End If
            dsCache = Nothing

            strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE 'PPC%'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (checkValue(Row.Item("StoredDataValue").ToString)) Then
                        strApplication += Regex.Replace(Row.Item("StoredDataName").ToString, "([A-Z0-9])", " $1") & ": " & Row.Item("StoredDataValue").ToString & vbCrLf
                    End If
                Next
            End If
            dsCache = Nothing
            strSQL = "SELECT TOP 1 Note FROM tblnotes WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteActive = 1 AND NoteType = 1"
            Dim strNote As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
            strApplication += "Notes: " & strNote

            'Response.Write(strApplication)
            strResult = postEmailWithMediaCampaignID("", strDeliveryEmailAddress, "New lead from " & strCompanyName, strApplication, False, "")
        End If
        Return strResult
    End Function

End Class


