using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using ServiceReference27TecNew1;

public partial class ESISDocument : Page
{
    string productId = HttpContext.Current.Request["productId"];
    string AppID = HttpContext.Current.Request["AppID"];
    string Plan = HttpContext.Current.Request["Plan"];
    string Lender = HttpContext.Current.Request["Lender"];
    string Payment = HttpContext.Current.Request["Payment"];



    public void DownloadESIS()    
    {
        try
        {
            decimal loanValue = 0, propertyValue = 0, brokerFee = 0, mortbal = 0, brokerfeeflatAm = 0, brokerfeeflatPerc = 0;
            int app1IncomeInt = 0, app2IncomeInt = 0, TotalOutstanding = 0, TotalPropVal = 0;
            int? CalCostNum = 0;
            double term = 0;
            string app1Title = "", app1FirstName = "", app1Surname = "";
            string app2Title = "", app2FirstName = "", app2Surname = "";
            string ExitStrategy = "", PropertyPreviouslyBridged = "", BridgingPropertyUse = "";
            string BridgingLoanPurpose = "", PaymentMethod = "", PropertyTenure = "", RoofCon = "";
            string AddressPostCode = "", UserName = "", SortColunm = "", LoanReason = "", MortgageType = "";
            int MaxLTVbuffer = 5, MinLTVBuffer = 5, MaxLoanBuffer = 5000, MinLoanBuffer = 5000, MaxTermBuffer = 12, MinTermBuffer = 12, MaxValBuffer = 5000, MinValBuffer = 5000;
            bool iPeriod = true, TrueCostFees = true, FessAddedToLoan = true, DeductCachBackVal = true, DeductRefundFeesVal = true;
            string iPeriodString = "", TrueCostFeesString = "", FessAddedToLoanString = "", DeductCachBackValString = "", DeductRefundFeesValString = "";
            string OccupiedBy = "", LimitedCompany = "", ExitStrat = "";



            DateTime app1DOBdate = new DateTime(), app2DOBdate = new DateTime();

            string siteId = "POSITI", tecCompanyId = "POSITI";


            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
            }

            SqlCommand updatecmd = new SqlCommand();
            updatecmd.Connection = connection;
            updatecmd.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Lender + "' where AppID = '" + AppID + "' and storeddataname = 'Underwritinglendername' IF @@ROWCOUNT=0 Insert Into tbldatastore  (AppId,StoredDataName,Storeddatavalue,companyid) values ('" + AppID + "','UnderwritingLenderName','" + Lender + "','1430') ");
            updatecmd.ExecuteNonQuery();

            SqlCommand updatecmd2 = new SqlCommand();
            updatecmd2.Connection = connection;
            updatecmd2.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Plan + "' where AppID = '" + AppID + "' and storeddataname = 'UnderwritingLenderPlan' IF @@ROWCOUNT=0 Insert Into tbldatastore (AppId,StoredDataName,Storeddatavalue,companyid) values (" + AppID + ",'UnderwritingLenderPlan','" + Plan + "','1430')");
            updatecmd2.ExecuteNonQuery();


            SqlCommand updatecmd3 = new SqlCommand();
            updatecmd3.Connection = connection;
            updatecmd3.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Payment + "' where AppID = '" + AppID + "' and storeddataname = 'Underwritingregularmonthlypayment' IF @@ROWCOUNT=0 Insert Into tbldatastore (AppId,StoredDataName,Storeddatavalue,companyid) values (" + AppID + ",'UnderwritingRegularMonthlyPayment','" + Payment + "','1430')");
            updatecmd3.ExecuteNonQuery();



            SqlCommand myCommand = new SqlCommand(string.Format("Select Amount, ProductTerm, PropertyValue, App1Title, App1FirstName, App1Surname, App1DOB, App1AnnualIncome, App2AnnualIncome, App2Title, App2FirstName, App2Surname, App2DOB, MortgageBalance, ProductType FROM tblapplications where AppId = {0}", AppID), connection);

            var reader = myCommand.ExecuteReader();

            while (reader.Read())
            {
                decimal.TryParse(reader["Amount"].ToString(), out loanValue);
                double.TryParse(reader["ProductTerm"].ToString(), out term);
                decimal.TryParse(reader["PropertyValue"].ToString(), out propertyValue);
                decimal.TryParse(reader["MortgageBalance"].ToString(), out mortbal);

                #region App 1
                app1Title = reader["App1Title"].ToString();
                app1FirstName = reader["App1FirstName"].ToString();
                app1Surname = reader["App1Surname"].ToString();
                app1Surname = reader["App1Surname"].ToString();
                DateTime.TryParse(reader["App1DOB"].ToString(), out app1DOBdate);
                int.TryParse(reader["App1AnnualIncome"].ToString(), out app1IncomeInt);
                #endregion

                #region App 2
                app2Title = reader["App2Title"].ToString();
                app2FirstName = reader["App2FirstName"].ToString();
                app2Surname = reader["App2Surname"].ToString();
                app2Surname = reader["App2Surname"].ToString();
                DateTime.TryParse(reader["App2DOB"].ToString(), out app2DOBdate);
                int.TryParse(reader["App2AnnualIncome"].ToString(), out app2IncomeInt);
                #endregion

                MortgageType = reader["ProductType"].ToString();




            }

            SqlCommand myCommandBroker = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'UnderwritingBrokerFee'", AppID), connection);

            var brokerReader = myCommandBroker.ExecuteReader();

            while (brokerReader.Read())
            {
                decimal.TryParse(brokerReader["StoredDataValue"].ToString(), out brokerFee);
            }

            SqlCommand myCommandDatastore = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

            var datastoreReader = myCommandDatastore.ExecuteReader();

            while (datastoreReader.Read())
            {
                ExitStrategy = datastoreReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPrevBridged = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PrevBridged'", AppID), connection);

            var PrevBridgedReader = myCommandPrevBridged.ExecuteReader();

            while (PrevBridgedReader.Read())
            {
                PropertyPreviouslyBridged = PrevBridgedReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyUse = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyUse'", AppID), connection);

            var PropertyUseReader = myCommandPropertyUse.ExecuteReader();

            while (PropertyUseReader.Read())
            {
                BridgingPropertyUse = PropertyUseReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLoanType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingLoanType'", AppID), connection);

            var LoanTypeReader = myCommandLoanType.ExecuteReader();

            while (LoanTypeReader.Read())
            {
                BridgingLoanPurpose = LoanTypeReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandTotalOutstanding = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AdditionalPropertiesOutstanding'", AppID), connection);

            var OutTotalReader = myCommandTotalOutstanding.ExecuteReader();

            while (OutTotalReader.Read())
            {
                int.TryParse(OutTotalReader["StoredDataValue"].ToString(), out TotalOutstanding);
            }

            SqlCommand myCommandTotalPropval = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AdditionalPropertiesValue'", AppID), connection);

            var TotalPropValReader = myCommandTotalPropval.ExecuteReader();

            while (TotalPropValReader.Read())
            {
                int.TryParse(TotalPropValReader["StoredDataValue"].ToString(), out TotalPropVal);
            }

            SqlCommand myCommandPaymentMeth = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPaymentMethod'", AppID), connection);

            var PaymentMethReader = myCommandPaymentMeth.ExecuteReader();

            while (PaymentMethReader.Read())
            {
                PaymentMethod = PaymentMethReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandTenure = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyFreeholdLeasehold'", AppID), connection);

            var TenureReader = myCommandTenure.ExecuteReader();

            while (TenureReader.Read())
            {
                PropertyTenure = TenureReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandRoof = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingConstructionRoof'", AppID), connection);

            var RoofReader = myCommandRoof.ExecuteReader();

            while (RoofReader.Read())
            {
                RoofCon = RoofReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandUser = new SqlCommand(string.Format("Select UserFullName from tblusers users inner join tblapplicationstatus app on users.UserID=app.SalesUserID where app.AppID= '2314632'Select UserFullName from tblusers users inner join tblapplicationstatus app on users.UserID=app.SalesUserID where app.AppID= " + AppID + "", AppID), connection);

            var UserReader = myCommandUser.ExecuteReader();

            while (UserReader.Read())
            {
                UserName = UserReader["UserFullName"].ToString();
            }

            SqlCommand myCommandSortCol = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SortColumn'", AppID), connection);

            var SortColReader = myCommandSortCol.ExecuteReader();

            while (SortColReader.Read())
            {
                SortColunm = SortColReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLoanReason = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ReasonForMortgage'", AppID), connection);

            var LoanReasonReader = myCommandLoanReason.ExecuteReader();

            while (LoanReasonReader.Read())
            {
                LoanReason = LoanReasonReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandIntitialPeriod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverInitialPeriod'", AppID), connection);

            var IntitialPeriod = myCommandIntitialPeriod.ExecuteReader();

            while (IntitialPeriod.Read())
            {
                iPeriodString = IntitialPeriod["StoredDataValue"].ToString();


                if (iPeriodString == "Yes")
                {

                    bool.TryParse(iPeriodString, out iPeriod);
                    iPeriod = true;
                }
                else
                {
                    bool.TryParse(iPeriodString, out iPeriod);
                    iPeriod = false;
                }
            }

            SqlCommand myCommandTrueCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesPayableToTrueCost'", AppID), connection);

            var AddFeesTrueCost = myCommandTrueCost.ExecuteReader();

            while (AddFeesTrueCost.Read())
            {
                TrueCostFeesString = AddFeesTrueCost["StoredDataValue"].ToString();

                if (TrueCostFeesString == "Yes")
                {
                    bool.TryParse(TrueCostFeesString, out TrueCostFees);
                    TrueCostFees = true;
                }
                else
                {
                    bool.TryParse(TrueCostFeesString, out TrueCostFees);
                    TrueCostFees = false;
                }
            }

            SqlCommand myCommandFeesAdded = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesToLoanAllowable'", AppID), connection);

            var FessAdded = myCommandFeesAdded.ExecuteReader();

            while (FessAdded.Read())
            {
                FessAddedToLoanString = FessAdded["StoredDataValue"].ToString();

                if (FessAddedToLoanString == "Yes")
                {

                    bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                    FessAddedToLoan = true;
                }
                else
                {
                    bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                    FessAddedToLoan = false;
                }
            }

            SqlCommand myCommandCashBack = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductCashback'", AppID), connection);

            var CachBack = myCommandCashBack.ExecuteReader();

            while (CachBack.Read())
            {
                DeductCachBackValString = CachBack["StoredDataValue"].ToString();
                bool.TryParse(DeductCachBackValString, out DeductCachBackVal);
                DeductCachBackVal = false;

            }

            SqlCommand myCommandRefundFees = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductRefundedFees'", AppID), connection);

            var RefundFees = myCommandRefundFees.ExecuteReader();

            while (RefundFees.Read())
            {
                DeductRefundFeesValString = RefundFees["StoredDataValue"].ToString();

                if (DeductRefundFeesValString == "Yes")
                {

                    bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                    DeductRefundFeesVal = true;
                }
                else
                {
                    bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                    DeductRefundFeesVal = false;
                }
            }

            SqlCommand myCommandCalCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverMonthsVal'", AppID), connection);

            var CalCost = myCommandCalCost.ExecuteReader();

            while (CalCost.Read())
            {

                //int.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

                NullableInt.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

                if (CalCostNum == 0)
                {
                    CalCostNum = Convert.ToInt32(term);

                }
                else
                {
                    CalCostNum = CalCostNum;
                }


            }

            SqlCommand myCommandOccupied = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyOccupied'", AppID), connection);

            var OccupiedReader = myCommandOccupied.ExecuteReader();

            while (OccupiedReader.Read())
            {
                OccupiedBy = OccupiedReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLimitedCo = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'LimitedCompany'", AppID), connection);

            var LimitedCoReader = myCommandLimitedCo.ExecuteReader();

            while (LimitedCoReader.Read())
            {
                LimitedCompany = LimitedCoReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandBrokerFeeFlatAm = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeFlatAmount'", AppID), connection);

            var brokerFeeFlatAm = myCommandBrokerFeeFlatAm.ExecuteReader();

            while (brokerFeeFlatAm.Read())
            {
                decimal.TryParse(brokerFeeFlatAm["StoredDataValue"].ToString(), out brokerfeeflatAm);


            }

            SqlCommand myCommandBrokerFeeFlatPerc = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeOverPercent'", AppID), connection);

            var brokerFeeFlatPerc = myCommandBrokerFeeFlatPerc.ExecuteReader();

            while (brokerFeeFlatPerc.Read())
            {
                decimal.TryParse(brokerFeeFlatPerc["StoredDataValue"].ToString(), out brokerfeeflatPerc);
            }




            //HttpContext.Current.Response.Write("Loan Reason " + LoanReason);
            //HttpContext.Current.Response.End();

            SourcingClient client = new SourcingClient();
            const string licenceKey = "840eadc1-b401-48ae-b9c1-456c3095ae11";

            SourceInput sourceInput = new SourceInput()
            {
                SiteId = siteId,
                Applicant1 = new Applicant()
                {
                    DateOfBirth = app1DOBdate.ToShortDateString(),
                    ApplicantType = ApplicantTypeEnum.No_Selection,
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app1IncomeInt,
                    },
                    FirstName = app1FirstName,
                    LastName = app1Surname,
                    Title = app1Title,
                    Gender = GetGender(app1Title),
                    ExistingMortgage = new ExistingMortgage()
                    {
                        OutstandingBalance = (int)mortbal
                    }
                },
                CompanyId = tecCompanyId,
                Term = (int)term/12,
                ExpectedValuation = (int)propertyValue,
                LoanRequired = (int)loanValue,

                MortgageType = GetMortgType(MortgageType),
                TermUnit = TermUnitEnum.Years,
                ReasonForMortgage = GetLoanReason(LoanReason),
                SearchMatchingType = SearchMatchingTypeEnum.Include_Near_Misses,

                NearMissesDetails = new NearMissesDetails()
                {
                    MaximumLTVBuffer = MaxLTVbuffer,
                    MinimumLTVBuffer = MinLTVBuffer,
                    MaximumLoanBuffer = MaxLoanBuffer,
                    MinimumLoanBuffer = MinLoanBuffer,
                    MaximumTermBuffer = MaxTermBuffer,
                    MinimumTermBuffer = MinTermBuffer,
                    MaximumValuationBuffer = MaxValBuffer,
                    MinimumValuationBuffer = MinValBuffer

                },
                TrueCostDetails = new TrueCostDetails()
                {
                    CalculateOverNoOfMonths = (int?)CalCostNum,
                    CalculateOverInitialPeriod = iPeriod,
                    AddFeesPayableToTruecostTotal = TrueCostFees,
                    AddFeesThatWillBeAddedToLoan = FessAddedToLoan,
                    DeductCashback = false,
                    DeductRefundedFees = DeductRefundFeesVal
                },

                //PropertyDetails = new PropertyDetails()
                //{
                //    PropertyTenure = GetTenure(PropertyTenure),
                //    PropertyRoof = GetRoof(RoofCon)


                //},

                SecuredLoanDetails = new SecuredLoanDetails()
                {

                   // ExitStrategy = GetExitStrat(ExitStrategy),
                    ChangeOfUseRequired = YesNoEnum.No,
                    ProjectRequiresPlanning = YesNoEnum.No,
                    ProjectPlanningGranted = YesNoEnum.No

                    //BridgingDetails = new BridgingDetails()
                    //{
                    //    BridgingPropertyUse = GetPropUse(BridgingPropertyUse),
                    //    PropertyPreviouslyBridged = GetPrevProp(PropertyPreviouslyBridged),
                    //    BridgingLoanPurpose = GetLoanPur(BridgingLoanPurpose),
                    //    BridgingAdditionalPropertiesTotalOutstanding = TotalOutstanding,
                    //    BridgingAdditionalPropertiesTotalValue = TotalPropVal,
                    //    BridgingPaymentMethod = GetPayM(PaymentMethod),
                    //    OccupiedByClientOrFamilyMember = GetOccupied(OccupiedBy),
                    //    LimitedCompany = GetCompany(LimitedCompany),


                    //},

                },


                Filters = new Filters()
                {
                    //OverpaymentsAllowed = overPayment,
                    SortResultsByColumn = new SortColumn()
                    {
                        Column = GetSortColunm(SortColunm)
                    }
                },


                FeeOverrideDetails = new FeeOverrideDetails()
                {
                    BrokerFeeFlatAmount = brokerfeeflatAm,
                    BrokerFeePercent = brokerfeeflatPerc
                }

            };


            if (app2IncomeInt != 0 && !string.IsNullOrEmpty(app2Title))
            {
                sourceInput.Applicant2 = new Applicant()
                {
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app2IncomeInt
                    },
                    Title = app2Title,
                    FirstName = app2FirstName,
                    LastName = app2Surname,
                    Gender = GetGender(app2Title),
                    DateOfBirth = app2DOBdate.ToShortDateString(),
                };
            }

            
            var test = client.RunSource(licenceKey, sourceInput);

            var testing = client.RunSource(licenceKey, sourceInput);
            var doc = client.GenerateIllustration(licenceKey, new IllustrationInput()
            {
                BrokerCompanyName = "Positive Lending",
                BrokerName = "Positive Lending",
                BrokerTelephoneNumber = "0845 260 7511",
                BrokerAddress = new Address()
                {
                    AddressLine1 = "Arena Business Centre",
                    AddressLine2 = "9 Nimrod Way",
                    AddressLine3 = "Ferndown Industrial Estate",
                    County = "Dorset",
                    Country = "United Kingdom",
                    PostCode = "BH21 7WH",

                },
                IllustrationType = IllustrationType.Esis,
                SourceInput = sourceInput,
                ProductId = productId


            });

            //var doc2 = client.GenerateIllustration(licenceKey, new IllustrationInput()
            //{
            //    IllustrationType = IllustrationType.Esis
            //});
           



            if (doc.Error.Any())
            {
                string errors = "";
                foreach (var item in doc.Error)
                {
                    errors += item + " ";
                }
				
				Response.Write(errors);
				Response.End();
            }

 

            try
            {
                byte[] bytes = Convert.FromBase64String(doc.DocumentData);

                HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + "EoRDocument.pdf");
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.BinaryWrite(bytes);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Redirect("", false);
            }
            catch (Exception)
            {                
                throw new Exception("No data found in document");
            }   

        }
        catch (Exception e)
        {
            throw;
        }
    }

   

    private GenderEnum? GetGender(string title)
    {
        switch (title)
        {
            case "Mr":
                return GenderEnum.Male;
            default:
                return GenderEnum.Female;
        }
    }

    private EnumSortColumns GetSortColunm(string SortColunmVal)
    {
        switch (SortColunmVal)
        {
            case "LenderName":
                return EnumSortColumns.LenderName;
            case "InitialRatePeriodMonths":
                return EnumSortColumns.InitialRatePeriodMonths;
            case "InitialMonthlyPayment":
                return EnumSortColumns.InitialMonthlyPayment;
            case "TrueCost":
                return EnumSortColumns.Truecost;
            case "MaxLTVAvailable":
                return EnumSortColumns.MaxLTVAvailable;
            case "FeesTotal":
                return EnumSortColumns.FeesTotal;
            case "MaxLoanAvailable":
                return EnumSortColumns.MaxLoanAvailable;
            default:
                return EnumSortColumns.InitialMonthlyPayment;
        }
    }

    private ReasonForMortgageEnum GetLoanReason(string ReasonForMortgage)
    {
        switch (ReasonForMortgage)
        {

            case "Remortgage":
                return ReasonForMortgageEnum.Remortgage;
            default:
                return ReasonForMortgageEnum.Remortgage;
        }
    }

    private BridgingPaymentMethodEnum? GetPayM(string BridgingPaymentMethod)
    {
        switch (BridgingPaymentMethod)
        {
            case "Serviced":
                return BridgingPaymentMethodEnum.Serviced;
            case "Rolled Up":
                return BridgingPaymentMethodEnum.RolledUp;
            default:
                return BridgingPaymentMethodEnum.Retained;

        }
    }

    private YesNoEnum? GetPrevProp(string PropertyPreviouslyBridged)
    {
        switch (PropertyPreviouslyBridged)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private YesNoEnum? GetOccupied(string OccupiedBy)
    {
        switch (OccupiedBy)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private YesNoEnum? GetCompany(string LimitedCompany)
    {
        switch (LimitedCompany)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private MortgageTypeEnum GetMortgType(string MortgageType)
    {
        switch (MortgageType)
        {
            case "Secured Loan":
                return MortgageTypeEnum.Secured_Loan;
            case "Secured Loan BTL":
                return MortgageTypeEnum.Secured_Loan_BTL;
            case "Bridging Execution Only":
                return MortgageTypeEnum.Bridging_Loan;
			case "Bridging Packaged":
                return MortgageTypeEnum.Bridging_Loan;
            default:
                return MortgageTypeEnum.Secured_Loan;

        }
    }

   

    public static class NullableInt
    {
        public static bool TryParse(string text, out int? outValue)
        {
            int parsedValue;
            bool success = int.TryParse(text, out parsedValue);
            outValue = success ? (int?)parsedValue : null;
            return success;
        }
    }
}

//public class DocumentModel
//{
//    public int ProductId { get; set; }
//}
