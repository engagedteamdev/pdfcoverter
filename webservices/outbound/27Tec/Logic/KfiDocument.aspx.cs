using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using ServiceReference27TecNew1;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Web.Script.Services;

public partial class KfiDocument : Page
{
    string strAPIKey = HttpContext.Current.Request["apiKey"];
    string strGUID = Guid.NewGuid().ToString();
    string strProductType = HttpContext.Current.Request["frmProductType"];
    string intAmount = HttpContext.Current.Request["frmAmount"];
    string intTerm = HttpContext.Current.Request["frmTerm"];
    string intLTV = HttpContext.Current.Request["frmLTV"];
    string intLTI = HttpContext.Current.Request["frmLTI"];
    string intPropertyVal = HttpContext.Current.Request["frmPropertyValue"];
    string strEmployment = HttpContext.Current.Request["frmEmploymentStatus"];
    string strPurpose = HttpContext.Current.Request["frmProductPurpose"];

    string strPropertyType = HttpContext.Current.Request["frmPropertyType"];
    string strConstructionType = HttpContext.Current.Request["frmConstructionType"];
    string strRateType = HttpContext.Current.Request["frmRateType"];

    #region Options
    string strLender = HttpContext.Current.Request["frmLender"];
    string strRepaymentType = HttpContext.Current.Request["frmRepaymentType"];
    string lenderListCall = HttpContext.Current.Request["lenderListCall"];
    string lenderNameSelect = HttpContext.Current.Request["frmLenderNameSelect"];
    string strOverpayments = HttpContext.Current.Request["frmOverpayment"];
    string strBrokerFee = HttpContext.Current.Request["brokerFee"];
    #endregion

    #region App1
    string app1Title = HttpContext.Current.Request["App1Title"];
    string app1FirstName = HttpContext.Current.Request["App1FirstName"];
    string app1Surname = HttpContext.Current.Request["App1Surname"];
    string app1DOB = HttpContext.Current.Request["frmApp1DateOB"];
    string app1Income = HttpContext.Current.Request["frmApp1annualincome"];
    string app1AddressLine1 = HttpContext.Current.Request["frmAddressLine1"];
    string app1AddressLine2 = HttpContext.Current.Request["frmAddressLine2"];
    string app1AddressLine3 = HttpContext.Current.Request["frmAddressLine3"];
    string app1AddressCounty = HttpContext.Current.Request["frmAddressCounty"];
    string app1AddressPostCode = HttpContext.Current.Request["frmAddressPostCode"];
    #endregion

    #region App2
    string app2Title = HttpContext.Current.Request["App2Title"];
    string app2FirstName = HttpContext.Current.Request["App2FirstName"];
    string app2Surname = HttpContext.Current.Request["App2Surname"];
    string app2DOB = HttpContext.Current.Request["frmApp2DateOB"];
    string app2Income = HttpContext.Current.Request["frmApp2annualincome"];
    #endregion
    string productId = HttpContext.Current.Request["productId"];

    public void DownloadKfi()
    {
        try
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            SourcingClient client = new SourcingClient();
            const string licenceKey = "840eadc1-b401-48ae-b9c1-456c3095ae11";
            string itemsToDisplay = "";

            double loanValue = double.Parse(intAmount), propertyValue = double.Parse(intPropertyVal);
            int? app1IncomeInt = !string.IsNullOrEmpty(app1Income) ? int.Parse(app1Income) : 0;
            int? app2IncomeInt = !string.IsNullOrEmpty(app2Income) ? int.Parse(app2Income) : 0;
            int brokerFee = int.Parse(strBrokerFee);

            FilterEnum overPayment = strOverpayments == "no pref" ? FilterEnum.Ignore : strOverpayments == "Yes" ? FilterEnum.Include : FilterEnum.Ignore;

            DateTime app1DOBdate, app2DOBdate;
            DateTime.TryParse(app1DOB, out app1DOBdate);
            DateTime.TryParse(app2DOB, out app2DOBdate);

            double term = double.Parse(intTerm);
            string siteId = "POSITI", tecCompanyId = "POSITI";



            SourceInput sourceInput = new SourceInput()
            {
                SiteId = siteId,
                Applicant1 = new Applicant()
                {
                    DateOfBirth = app1DOBdate.ToShortDateString(),
                    ApplicantType = ApplicantTypeEnum.No_Selection,
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app1IncomeInt,
                    },
                    FirstName = app1FirstName,
                    LastName = app1Surname,
                    Title = app1Title,
                },

                CompanyId = tecCompanyId,
                Term = (int)term,
                ExpectedValuation = (int)propertyValue,
                LoanRequired = (int)loanValue,
                MortgageType = MortgageTypeEnum.Bridging_Loan,
                TermUnit = TermUnitEnum.Months,
                TrueCostDetails = new TrueCostDetails()
                {
                    CalculateOverNoOfMonths = (int)term,
                    CalculateOverInitialPeriod = true,
                    AddFeesPayableToTruecostTotal = true,
                    //DeductCashback = true,
                    //DeductRefundedFees = true,
                    AddFeesThatWillBeAddedToLoan = true
                },
                Filters = new Filters()
                {
                    OverpaymentsAllowed = overPayment
                },
                FeeOverrideDetails = new FeeOverrideDetails()
                {
                    BrokerFeeFlatAmount = brokerFee
                }
            };

            if (app2IncomeInt != 0 && !string.IsNullOrEmpty(app2Title))
            {
                sourceInput.Applicant2 = new Applicant()
                {
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app2IncomeInt
                    },
                    Title = app2Title,
                    FirstName = app2FirstName,
                    LastName = app2Surname,
                    DateOfBirth = app2DOBdate.ToShortDateString(),
                };
            }


            var test = client.RunSource(licenceKey, sourceInput);



            //foreach (var item in test.Results)
            //{
            //    itemsToDisplay += "Item: " + item.ProductCode + "  ProductCode: " + productId;
            //}

            var doc = client.GenerateKfi(licenceKey, new KfiInput()
            {
                ProductId = productId,
                SourceInput = sourceInput,
                IntroducerName = "Me",
                BrokerCompanyName = "Testing",
                PackagerCompanyName = "Testing",
                BrokerTelephoneNumber = "01302",
                BrokerAddress1 = "Line 1",
                BrokerAddress2 = "Line 2",
                BrokerCounty = "South Yorkshire",
                BrokerName = "Name",
                BrokerPostcode = "DN3 2EZ",
                BrokerCountry = "England"
            });

            //var doc = client.GenerateEoRDocument(licenceKey, new EoRInput()
            //{
            //    AdvisorName = "James Naylor",
            //    SourceInput = sourceInput,
            //    ProductId = productId
            //});

            byte[] bytes = Convert.FromBase64String(doc.Document);

            //HttpContext.Current.Response.Write(string.Format("Amount: {0}, Loan Value: {1}, Property Value: {2}, App1IncomeInt: {3},  App2Income: {4}, BrokerFee: {5}, Overpayment: {6}, DOB1: {7}, DOB2: {8}, Term: {9}", intAmount, loanValue, propertyValue, app1IncomeInt, app2IncomeInt, brokerFee, overPayment, app1DOBdate.ToShortDateString(), app2DOBdate.ToShortDateString(), term));

            HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + "TEST1.pdf");
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.BinaryWrite(bytes);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Redirect("", false);




        }
        catch (Exception e)
        {
            throw;
        }
    }
}