using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using ServiceReference27Tec1;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml.XPath;

public partial class ESISDocument : Page
{
    string productId = HttpContext.Current.Request["productId"];
    string AppID = HttpContext.Current.Request["AppID"];
    string Plan = HttpContext.Current.Request["Plan"];
    string Lender = HttpContext.Current.Request["Lender"];
    string Payment = HttpContext.Current.Request["Payment"];
    string Rate = HttpContext.Current.Request["Rate"];
    string BrokerName = HttpContext.Current.Request["BrokerName"];
    string CompanyName = HttpContext.Current.Request["BrokerCompanyName"];
    string CompanyAddressLine1 = HttpContext.Current.Request["BrokerAddressLine1"];
    string CompanyAddressLine2 = HttpContext.Current.Request["BrokerAddressLine2"];
    string CompanyAddressLine3 = HttpContext.Current.Request["BrokerAddressLine3"];
    string CompanyCounty = HttpContext.Current.Request["BrokerAddressCounty"];
    string CompanyPostCode = HttpContext.Current.Request["BrokerAddressPostCode"];
    string isAdvised = HttpContext.Current.Request["BrokerAdvised"];
    string isAdvisedDeclared = HttpContext.Current.Request["AdverseDeclared"];
    string feewaiver = HttpContext.Current.Request["txtfeewaiver"];
    string complaints = HttpContext.Current.Request["txtcomplaints"];
    string BrokerTelephoneNumber = HttpContext.Current.Request["BrokerTelephoneNumber"];
    string FeeWaiver = HttpContext.Current.Request["txtfeewaiver"];
    string Complaints = HttpContext.Current.Request["txtcomplaints"];
    string BrokerAdvised = HttpContext.Current.Request["BrokerAdvised"];
    string AdverseDeclared = HttpContext.Current.Request["AdverseDeclared"];
    string IsAdverseDeclared = HttpContext.Current.Request["AdverseDeclared"];
    bool BrokerAdvisedYes = true;
    bool IsAdverseDeclaredYes = true;

    string FeeTypeValFee = HttpContext.Current.Request["FeeTypeValFee"];
    string ValFeeAmount = HttpContext.Current.Request["ValFeeAmount"];
    string ValFeeWhenPayable = HttpContext.Current.Request["ValFeeWhenPayable"];
    string ValFeeAddToLoan = HttpContext.Current.Request["ValFeeAddToLoan"];
    string ValFeeAmountRef = HttpContext.Current.Request["ValFeeAmountRef"];
    string ValFeeWhenRef = HttpContext.Current.Request["ValFeeWhenRef"];

    string FeeTypeDeedsFee = HttpContext.Current.Request["FeeTypeDeedsFee"];
    string DeedsFeeAmount = HttpContext.Current.Request["DeedsFeeAmount"];
    string DeedsFeeWhenPayable = HttpContext.Current.Request["DeedsFeeWhenPayable"];
    string DeedsFeeAddToLoan = HttpContext.Current.Request["DeedsFeeAddToLoan"];
    string DeedsFeeAmountRef = HttpContext.Current.Request["DeedsFeeAmountRef"];
    string DeedsFeeWhenRef = HttpContext.Current.Request["DeedsFeeWhenRef"];

    string FeeTypeDisFee = HttpContext.Current.Request["FeeTypeDisFee"];
    string DisFeeAmount = HttpContext.Current.Request["DisFeeAmount"];
    string DisFeeWhenPayable = HttpContext.Current.Request["DisFeeWhenPayable"];
    string DisFeeAddToLoan = HttpContext.Current.Request["DisFeeAddToLoan"];
    string DisFeeAmountRef = HttpContext.Current.Request["DisFeeAmountRef"];
    string DisFeeWhenRef = HttpContext.Current.Request["DisFeeWhenRef"];

    string FeeTypeArrFee = HttpContext.Current.Request["FeeTypeArrFee"];
    string ArrFeeAmount = HttpContext.Current.Request["ArrFeeAmount"];
    string ArrFeeWhenPayable = HttpContext.Current.Request["ArrFeeWhenPayable"];
    string ArrFeeAddToLoan = HttpContext.Current.Request["ArrFeeAddToLoan"];
    string ArrFeeAmountRef = HttpContext.Current.Request["ArrFeeAmountRef"];
    string ArrFeeWhenRef = HttpContext.Current.Request["ArrFeeWhenRef"];

    string FeeTypeBookingFee = HttpContext.Current.Request["FeeTypeBookingFee"];
    string BookingFeeAmount = HttpContext.Current.Request["BookingFeeAmount"];
    string BookingFeeWhenPayable = HttpContext.Current.Request["BookingFeeWhenPayable"];
    string BookingFeeAddToLoan = HttpContext.Current.Request["BookingFeeAddToLoan"];
    string BookingFeeAmountRef = HttpContext.Current.Request["BookingFeeAmountRef"];
    string BookingFeeWhenRef = HttpContext.Current.Request["BookingFeeWhenRef"];

    string FeeTypeChapsFee = HttpContext.Current.Request["FeeTypeChapsFee"];
    string ChapsFeeAmount = HttpContext.Current.Request["ChapsFeeAmount"];
    string ChapsFeeWhenPayable = HttpContext.Current.Request["ChapsFeeWhenPayable"];
    string ChapsFeeAddToLoan = HttpContext.Current.Request["ChapsFeeAddToLoan"];
    string ChapsFeeAmountRef = HttpContext.Current.Request["ChapsFeeAmountRef"];
    string ChapsFeeWhenRef = HttpContext.Current.Request["ChapsFeeWhenRef"];

    string FeeTypeHigherLendingFee = HttpContext.Current.Request["FeeTypeHigherLendingFee"];
    string HigherLendingFeeAmount = HttpContext.Current.Request["HigherLendingFeeAmount"];
    string HigherLendingFeeWhenPayable = HttpContext.Current.Request["HigherLendingFeeWhenPayable"];
    string HigherLendingFeeAddToLoan = HttpContext.Current.Request["HigherLendingFeeAddToLoan"];
    string HigherLendingFeeAmountRef = HttpContext.Current.Request["HigherLendingFeeAmountRef"];
    string HigherLendingFeeWhenRef = HttpContext.Current.Request["HigherLendingFeeWhenRef"];

    string FeeTypeOwnBrokerApplicationFee = HttpContext.Current.Request["FeeTypeOwnBrokerApplicationFee"];
    string OwnBrokerFeeAmount = HttpContext.Current.Request["OwnBrokerFeeAmount"];
    string OwnBrokerFeeWhenPayable = HttpContext.Current.Request["OwnBrokerFeeWhenPayable"];
    string OwnBrokerFeeAddToLoan = HttpContext.Current.Request["OwnBrokerFeeAddToLoan"];
    string OwnBrokerFeeAmountRef = HttpContext.Current.Request["OwnBrokerFeeAmountRef"];
    string OwnBrokerFeeWhenRef = HttpContext.Current.Request["OwnBrokerFeeWhenRef"];

    string FeeTypeBrokerFee = HttpContext.Current.Request["FeeTypeBrokerFee"];
    string BrokerFeeAmount = HttpContext.Current.Request["BrokerFeeAmount"];
    string BrokerFeeWhenPayable = HttpContext.Current.Request["BrokerFeeWhenPayable"];
    string BrokerFeeAddToLoan = HttpContext.Current.Request["BrokerFeeAddToLoan"];
    string BrokerFeeAmountRef = HttpContext.Current.Request["BrokerFeeAmountRef"];
    string BrokerFeeWhenRef = HttpContext.Current.Request["BrokerFeeWhenRef"];

    string FeeType = HttpContext.Current.Request["FeeType"];
    string ProcFee = HttpContext.Current.Request["ProcFee"];
    string ProcFeeCompanyName = HttpContext.Current.Request["ProcFeeCompanyName"];

    string PropertyLocation = HttpContext.Current.Request["PropertyLocation"];
    string PropertyType = HttpContext.Current.Request["PropertyType"];
    string PropertyStyle = HttpContext.Current.Request["PropertyStyle"];
    string PropertyTenure = HttpContext.Current.Request["PropertyTenure"];
    string PropertyConstructionType = HttpContext.Current.Request["PropertyConstructionType"];
    string PropertyRoofConstructionType = HttpContext.Current.Request["PropertyRoofConstructionType"];
    string PropertyUse = HttpContext.Current.Request["PropertyUse"];
    string PropertyYearBuilt = HttpContext.Current.Request["PropertyYearBuilt"];
    string PropertyYearBuiltMonths = HttpContext.Current.Request["PropertyYearBuiltMonths"];
    string PropertyCouncilPurchase = HttpContext.Current.Request["PropertyCouncilPurchase"];
    string PurchaseAddressHouseNumber = HttpContext.Current.Request["PurchaseAddressHouseNumber"];
    string PurchaseAddressHouseName = HttpContext.Current.Request["PurchaseAddressHouseName"];
    string PurchaseAddressLine1 = "";
    string PurchaseAddressLine2 = "";
    string PurchaseAddressLine3 = "";
    string PurchaseAddressCounty = "";
    string PurchaseAddressPostCode = "";
    string CurrentYearsApp1 = "";
    string CurrentYearsApp2 = "";
    string CurrentMonthsApp1 = "";
    string CurrentMonthsApp2 = "";
    string YearsAccountsApp1 = "";
    string YearsAccountsApp2 = "";
    string App1EmploymentStatus = "";
    string App2EmploymentStatus = "";
    string PropertyRemainingLease = "";
    string mortgagereason = "";
    string PropertyFlatFloorNumber = "";







    public void DownloadESIS()
    {
        try
        {


            decimal loanValue = 0, propertyValue = 0, brokerFee = 0, mortbal = 0, brokerfeeflatAm = 0, brokerfeeflatPerc = 0;
            int app1IncomeInt = 0, app2IncomeInt = 0, TotalOutstanding = 0, TotalPropVal = 0;
            int? CalCostNum = 0;
            double term = 0;
            string app1Title = "", app1FirstName = "", app1Surname = "";
            string app2Title = "", app2FirstName = "", app2Surname = "";
            string ExitStrategy = "", PropertyPreviouslyBridged = "", BridgingPropertyUse = "";
            string BridgingLoanPurpose = "", PaymentMethod = "", PropertyTenure = "", RoofCon = "";
            string AddressPostCode = "", UserName = "", SortColunm = "", LoanReason = "", MortgageType = "";
            int MaxLTVbuffer = 5, MinLTVBuffer = 5, MaxLoanBuffer = 5000, MinLoanBuffer = 5000, MaxTermBuffer = 12, MinTermBuffer = 12, MaxValBuffer = 5000, MinValBuffer = 5000;
            bool iPeriod = true, TrueCostFees = true, FessAddedToLoan = true, DeductCachBackVal = true, DeductRefundFeesVal = true, PayBrokerFeeUpFront = true;
            string iPeriodString = "", TrueCostFeesString = "", FessAddedToLoanString = "", DeductCachBackValString = "", DeductRefundFeesValString = "", PayBrokerFeeUpFrontString = "";
            string OccupiedBy = "", LimitedCompany = "", ExitStrat = "";
            string securedpaymentmeth = "", consumerbtl = "";

            string App1ArrearsDateRegistered1 = "", App1ArrearsCleared1 = "";
            string App1ArrearsDateRegistered2 = "", App1ArrearsCleared2 = "";
            string App1ArrearsDateRegistered3 = "", App1ArrearsCleared3 = "";
            string App1ArrearsDateRegistered4 = "", App1ArrearsCleared4 = "";
            string App1ArrearsDateRegistered5 = "", App1ArrearsCleared5 = "";

            string App2ArrearsDateRegistered1 = "", App2ArrearsCleared1 = "";
            string App2ArrearsDateRegistered2 = "", App2ArrearsCleared2 = "";
            string App2ArrearsDateRegistered3 = "", App2ArrearsCleared3 = "";
            string App2ArrearsDateRegistered4 = "", App2ArrearsCleared4 = "";
            string App2ArrearsDateRegistered5 = "", App2ArrearsCleared5 = "";

            string App1CountyDateRegistered1 = "", App1CountyAmount1 = "", App1CountyDateSat1 = "";
            string App1CountyDateRegistered2 = "", App1CountyAmount2 = "", App1CountyDateSat2 = "";
            string App1CountyDateRegistered3 = "", App1CountyAmount3 = "", App1CountyDateSat3 = "";
            string App1CountyDateRegistered4 = "", App1CountyAmount4 = "", App1CountyDateSat4 = "";
            string App1CountyDateRegistered5 = "", App1CountyAmount5 = "", App1CountyDateSat5 = "";

            string App2CountyDateRegistered1 = "", App2CountyAmount1 = "", App2CountyDateSat1 = "";
            string App2CountyDateRegistered2 = "", App2CountyAmount2 = "", App2CountyDateSat2 = "";
            string App2CountyDateRegistered3 = "", App2CountyAmount3 = "", App2CountyDateSat3 = "";
            string App2CountyDateRegistered4 = "", App2CountyAmount4 = "", App2CountyDateSat4 = "";
            string App2CountyDateRegistered5 = "", App2CountyAmount5 = "", App2CountyDateSat5 = "";

            string App1DefaultDateRegistered1 = "", App1DefaultAmount1 = "", App1DefaultDateSat1 = "";
            string App1DefaultDateRegistered2 = "", App1DefaultAmount2 = "", App1DefaultDateSat2 = "";
            string App1DefaultDateRegistered3 = "", App1DefaultAmount3 = "", App1DefaultDateSat3 = "";
            string App1DefaultDateRegistered4 = "", App1DefaultAmount4 = "", App1DefaultDateSat4 = "";
            string App1DefaultDateRegistered5 = "", App1DefaultAmount5 = "", App1DefaultDateSat5 = "";

            string App2DefaultDateRegistered1 = "", App2DefaultAmount1 = "", App2DefaultDateSat1 = "";
            string App2DefaultDateRegistered2 = "", App2DefaultAmount2 = "", App2DefaultDateSat2 = "";
            string App2DefaultDateRegistered3 = "", App2DefaultAmount3 = "", App2DefaultDateSat3 = "";
            string App2DefaultDateRegistered4 = "", App2DefaultAmount4 = "", App2DefaultDateSat4 = "";
            string App2DefaultDateRegistered5 = "", App2DefaultAmount5 = "", App2DefaultDateSat5 = "";

            string App1PayDayLoanRegistered1 = "";
            string App1PayDayLoanRegistered2 = "";
            string App1PayDayLoanRegistered3 = "";
            string App1PayDayLoanRegistered4 = "";
            string App1PayDayLoanRegistered5 = "";

            string App2PayDayLoanRegistered1 = "";
            string App2PayDayLoanRegistered2 = "";
            string App2PayDayLoanRegistered3 = "";
            string App2PayDayLoanRegistered4 = "";
            string App2PayDayLoanRegistered5 = "";

            string App1BankruptciesDate = "";
            string App2BankruptciesDate = "";

            string App1IVADateReg1 = "", App1IVASat1 = "", App1IVADateCom1 = "";
            string App1IVADateReg2 = "", App1IVASat2 = "", App1IVADateCom2 = "";
            string App1IVADateReg3 = "", App1IVASat3 = "", App1IVADateCom3 = "";
            string App1IVADateReg4 = "", App1IVASat4 = "", App1IVADateCom4 = "";
            string App1IVADateReg5 = "", App1IVASat5 = "", App1IVADateCom5 = "";

            string App2IVADateReg1 = "", App2IVASat1 = "", App2IVADateCom1 = "";
            string App2IVADateReg2 = "", App2IVASat2 = "", App2IVADateCom2 = "";
            string App2IVADateReg3 = "", App2IVASat3 = "", App2IVADateCom3 = "";
            string App2IVADateReg4 = "", App2IVASat4 = "", App2IVADateCom4 = "";
            string App2IVADateReg5 = "", App2IVASat5 = "", App2IVADateCom5 = "";



            if (BrokerAdvised == "Y")
            {
                BrokerAdvisedYes = true;

            }
            else
            {
                BrokerAdvisedYes = false;
            }

            if (IsAdverseDeclared == "Y")
            {
                IsAdverseDeclaredYes = true;

            }
            else
            {
                IsAdverseDeclaredYes = false;
            }



            DateTime app1DOBdate = new DateTime(), app2DOBdate = new DateTime();

            string siteId = "POSITI", tecCompanyId = "POSITI";


            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
            }



            SqlCommand updatecmd = new SqlCommand();
            updatecmd.Connection = connection;
            updatecmd.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Lender + "' where AppID = '" + AppID + "' and storeddataname = 'Underwritinglendername' IF @@ROWCOUNT=0 Insert Into tbldatastore  (AppId,StoredDataName,Storeddatavalue,companyid) values ('" + AppID + "','UnderwritingLenderName','" + Lender + "','1430') ");
            updatecmd.ExecuteNonQuery();



            SqlCommand updatecmd2 = new SqlCommand();
            updatecmd2.Connection = connection;
            updatecmd2.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Plan + "' where AppID = '" + AppID + "' and storeddataname = 'UnderwritingLenderPlan' IF @@ROWCOUNT=0 Insert Into tbldatastore (AppId,StoredDataName,Storeddatavalue,companyid) values (" + AppID + ",'UnderwritingLenderPlan','" + Plan + "','1430')");
            updatecmd2.ExecuteNonQuery();



            SqlCommand updatecmd3 = new SqlCommand();
            updatecmd3.Connection = connection;
            updatecmd3.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Payment + "' where AppID = '" + AppID + "' and storeddataname = 'Underwritingregularmonthlypayment' IF @@ROWCOUNT=0 Insert Into tbldatastore (AppId,StoredDataName,Storeddatavalue,companyid) values (" + AppID + ",'UnderwritingRegularMonthlyPayment','" + Payment + "','1430')");
            updatecmd3.ExecuteNonQuery();

            SqlCommand updatecmd4 = new SqlCommand();
            updatecmd4.Connection = connection;
            updatecmd4.CommandText = string.Format("UPDATE tbldatastore set storeddatavalue = '" + Rate + "' where AppID = '" + AppID + "' and storeddataname = 'UnderwritingRegularInterestRate' IF @@ROWCOUNT=0 Insert Into tbldatastore (AppId,StoredDataName,Storeddatavalue,companyid) values (" + AppID + ",'UnderwritingRegularInterestRate','" + Rate + "','1430')");
            updatecmd4.ExecuteNonQuery();



            SqlCommand myCommand = new SqlCommand(string.Format("Select Amount, ProductTerm, PropertyValue, App1Title, App1FirstName, App1Surname, App1DOB, App1AnnualIncome, App2AnnualIncome, App2Title, App2FirstName, App2Surname, App2DOB, MortgageBalance, ProductType, ProductPurpose FROM tblapplications where AppId = {0}", AppID), connection);

            var reader = myCommand.ExecuteReader();

            while (reader.Read())
            {
                decimal.TryParse(reader["Amount"].ToString(), out loanValue);
                double.TryParse(reader["ProductTerm"].ToString(), out term);
                decimal.TryParse(reader["PropertyValue"].ToString(), out propertyValue);
                decimal.TryParse(reader["MortgageBalance"].ToString(), out mortbal);

                #region App 1
                app1Title = reader["App1Title"].ToString();
                app1FirstName = reader["App1FirstName"].ToString();
                app1Surname = reader["App1Surname"].ToString();
                app1Surname = reader["App1Surname"].ToString();
                DateTime.TryParse(reader["App1DOB"].ToString(), out app1DOBdate);
                int.TryParse(reader["App1AnnualIncome"].ToString(), out app1IncomeInt);
                #endregion

                #region App 2
                app2Title = reader["App2Title"].ToString();
                app2FirstName = reader["App2FirstName"].ToString();
                app2Surname = reader["App2Surname"].ToString();
                app2Surname = reader["App2Surname"].ToString();
                DateTime.TryParse(reader["App2DOB"].ToString(), out app2DOBdate);
                int.TryParse(reader["App2AnnualIncome"].ToString(), out app2IncomeInt);
                #endregion

                MortgageType = reader["ProductType"].ToString();
                mortgagereason = reader["ProductPurpose"].ToString();




            }

            SqlCommand myCommandBroker = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'UnderwritingBrokerFee'", AppID), connection);

            var brokerReader = myCommandBroker.ExecuteReader();

            while (brokerReader.Read())
            {
                decimal.TryParse(brokerReader["StoredDataValue"].ToString(), out brokerFee);
            }

            SqlCommand myCommandDatastore = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

            var datastoreReader = myCommandDatastore.ExecuteReader();

            while (datastoreReader.Read())
            {
                ExitStrategy = datastoreReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPrevBridged = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PrevBridged'", AppID), connection);

            var PrevBridgedReader = myCommandPrevBridged.ExecuteReader();

            while (PrevBridgedReader.Read())
            {
                PropertyPreviouslyBridged = PrevBridgedReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyUse = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyUse'", AppID), connection);

            var PropertyUseReader = myCommandPropertyUse.ExecuteReader();

            while (PropertyUseReader.Read())
            {
                BridgingPropertyUse = PropertyUseReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLoanType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingLoanType'", AppID), connection);

            var LoanTypeReader = myCommandLoanType.ExecuteReader();

            while (LoanTypeReader.Read())
            {
                BridgingLoanPurpose = LoanTypeReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandTotalOutstanding = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AdditionalPropertiesOutstanding'", AppID), connection);

            var OutTotalReader = myCommandTotalOutstanding.ExecuteReader();

            while (OutTotalReader.Read())
            {
                int.TryParse(OutTotalReader["StoredDataValue"].ToString(), out TotalOutstanding);
            }

            SqlCommand myCommandTotalPropval = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AdditionalPropertiesValue'", AppID), connection);

            var TotalPropValReader = myCommandTotalPropval.ExecuteReader();

            while (TotalPropValReader.Read())
            {
                int.TryParse(TotalPropValReader["StoredDataValue"].ToString(), out TotalPropVal);
            }

            SqlCommand myCommandPaymentMeth = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPaymentMethod'", AppID), connection);

            var PaymentMethReader = myCommandPaymentMeth.ExecuteReader();

            while (PaymentMethReader.Read())
            {
                PaymentMethod = PaymentMethReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandTenure = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyFreeholdLeasehold'", AppID), connection);

            var TenureReader = myCommandTenure.ExecuteReader();

            while (TenureReader.Read())
            {
                PropertyTenure = TenureReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandRoof = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingConstructionRoof'", AppID), connection);

            var RoofReader = myCommandRoof.ExecuteReader();

            while (RoofReader.Read())
            {
                RoofCon = RoofReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandUser = new SqlCommand(string.Format("Select UserFullName from tblusers users inner join tblapplicationstatus app on users.UserID=app.SalesUserID where app.AppID= '2314632'Select UserFullName from tblusers users inner join tblapplicationstatus app on users.UserID=app.SalesUserID where app.AppID= " + AppID + "", AppID), connection);

            var UserReader = myCommandUser.ExecuteReader();

            while (UserReader.Read())
            {
                UserName = UserReader["UserFullName"].ToString();
            }

            SqlCommand myCommandSortCol = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SortColumn'", AppID), connection);

            var SortColReader = myCommandSortCol.ExecuteReader();

            while (SortColReader.Read())
            {
                SortColunm = SortColReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLoanReason = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ReasonForMortgage'", AppID), connection);

            var LoanReasonReader = myCommandLoanReason.ExecuteReader();

            while (LoanReasonReader.Read())
            {
                LoanReason = LoanReasonReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandIntitialPeriod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverInitialPeriod'", AppID), connection);

            var IntitialPeriod = myCommandIntitialPeriod.ExecuteReader();

            while (IntitialPeriod.Read())
            {
                iPeriodString = IntitialPeriod["StoredDataValue"].ToString();


                if (iPeriodString == "Yes")
                {

                    bool.TryParse(iPeriodString, out iPeriod);
                    iPeriod = true;
                }
                else
                {
                    bool.TryParse(iPeriodString, out iPeriod);
                    iPeriod = false;
                }
            }

            SqlCommand myCommandTrueCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesPayableToTrueCost'", AppID), connection);

            var AddFeesTrueCost = myCommandTrueCost.ExecuteReader();

            while (AddFeesTrueCost.Read())
            {
                TrueCostFeesString = AddFeesTrueCost["StoredDataValue"].ToString();

                if (TrueCostFeesString == "Yes")
                {
                    bool.TryParse(TrueCostFeesString, out TrueCostFees);
                    TrueCostFees = true;
                }
                else
                {
                    bool.TryParse(TrueCostFeesString, out TrueCostFees);
                    TrueCostFees = false;
                }
            }

            SqlCommand myCommandFeesAdded = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesToLoanAllowable'", AppID), connection);

            var FessAdded = myCommandFeesAdded.ExecuteReader();

            while (FessAdded.Read())
            {
                FessAddedToLoanString = FessAdded["StoredDataValue"].ToString();

                if (FessAddedToLoanString == "Yes")
                {

                    bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                    FessAddedToLoan = true;
                }
                else
                {
                    bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                    FessAddedToLoan = false;
                }
            }

            SqlCommand myCommandPayBroker = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PayBrokerFeeUpFront'", AppID), connection);

            var PayBroker = myCommandPayBroker.ExecuteReader();

            while (PayBroker.Read())
            {
                PayBrokerFeeUpFrontString = PayBroker["StoredDataValue"].ToString();

                if (PayBrokerFeeUpFrontString == "Y")
                {

                    bool.TryParse(PayBrokerFeeUpFrontString, out PayBrokerFeeUpFront);
                    PayBrokerFeeUpFront = true;
                }
                else
                {
                    bool.TryParse(PayBrokerFeeUpFrontString, out PayBrokerFeeUpFront);
                    PayBrokerFeeUpFront = false;
                }
            }

            SqlCommand myCommandCashBack = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductCashback'", AppID), connection);

            var CachBack = myCommandCashBack.ExecuteReader();

            while (CachBack.Read())
            {
                DeductCachBackValString = CachBack["StoredDataValue"].ToString();
                bool.TryParse(DeductCachBackValString, out DeductCachBackVal);
                DeductCachBackVal = false;

            }

            SqlCommand myCommandRefundFees = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductRefundedFees'", AppID), connection);

            var RefundFees = myCommandRefundFees.ExecuteReader();

            while (RefundFees.Read())
            {
                DeductRefundFeesValString = RefundFees["StoredDataValue"].ToString();

                if (DeductRefundFeesValString == "Yes")
                {

                    bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                    DeductRefundFeesVal = true;
                }
                else
                {
                    bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                    DeductRefundFeesVal = false;
                }
            }

            SqlCommand myCommandCalCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverMonthsVal'", AppID), connection);

            var CalCost = myCommandCalCost.ExecuteReader();

            while (CalCost.Read())
            {

                //int.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

                NullableInt.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

                if (CalCostNum == 0)
                {
                    CalCostNum = Convert.ToInt32(term);

                }
                else
                {
                    CalCostNum = CalCostNum;
                }


            }

            SqlCommand myCommandOccupied = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyOccupied'", AppID), connection);

            var OccupiedReader = myCommandOccupied.ExecuteReader();

            while (OccupiedReader.Read())
            {
                OccupiedBy = OccupiedReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandLimitedCo = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'LimitedCompany'", AppID), connection);

            var LimitedCoReader = myCommandLimitedCo.ExecuteReader();

            while (LimitedCoReader.Read())
            {
                LimitedCompany = LimitedCoReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandBrokerFeeFlatAm = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeFlatAmount'", AppID), connection);

            var brokerFeeFlatAm = myCommandBrokerFeeFlatAm.ExecuteReader();

            while (brokerFeeFlatAm.Read())
            {
                decimal.TryParse(brokerFeeFlatAm["StoredDataValue"].ToString(), out brokerfeeflatAm);


            }

            SqlCommand myCommandBrokerFeeFlatPerc = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeOverPercent'", AppID), connection);

            var brokerFeeFlatPerc = myCommandBrokerFeeFlatPerc.ExecuteReader();

            while (brokerFeeFlatPerc.Read())
            {
                decimal.TryParse(brokerFeeFlatPerc["StoredDataValue"].ToString(), out brokerfeeflatPerc);
            }


            SqlCommand myCommandemp = new SqlCommand(string.Format("Select App1EmploymentStatus, App2EmploymentStatus FROM tblapplications where AppId = {0}", AppID), connection);

            var readeremp = myCommandemp.ExecuteReader();

            while (readeremp.Read())
            {

                App1EmploymentStatus = readeremp["App1EmploymentStatus"].ToString();
                App2EmploymentStatus = readeremp["App2EmploymentStatus"].ToString();




            }

            SqlCommand myCommandPropertyFlatFloorNumber = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyFlatFloorNumber'", AppID), connection);

            var PropertyFlatFloorNumberReader = myCommandPropertyFlatFloorNumber.ExecuteReader();

            while (PropertyFlatFloorNumberReader.Read())
            {
                PropertyFlatFloorNumber = PropertyFlatFloorNumberReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyAddress1 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine1'", AppID), connection);

            var PA1Reader = myCommandPropertyAddress1.ExecuteReader();

            while (PA1Reader.Read())
            {
                PurchaseAddressLine1 = PA1Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyAddress2 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine2'", AppID), connection);

            var PA2Reader = myCommandPropertyAddress2.ExecuteReader();

            while (PA2Reader.Read())
            {
                PurchaseAddressLine2 = PA2Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyAddress3 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine3'", AppID), connection);

            var PA3Reader = myCommandPropertyAddress3.ExecuteReader();

            while (PA3Reader.Read())
            {
                PurchaseAddressLine3 = PA3Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyCounty = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressCounty'", AppID), connection);

            var PACountyReader = myCommandPropertyCounty.ExecuteReader();

            while (PACountyReader.Read())
            {
                PurchaseAddressCounty = PACountyReader["StoredDataValue"].ToString();
            }


            SqlCommand myCommandPropertyPostcode = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressPostCode'", AppID), connection);

            var PostCodeReader = myCommandPropertyPostcode.ExecuteReader();

            while (PostCodeReader.Read())
            {
                PurchaseAddressPostCode = PostCodeReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandApp1Arrears = new SqlCommand(string.Format("Select Top(1) * FROM vw27tecadverse where AppId = {0}", AppID), connection);

            var ArrearsReader = myCommandApp1Arrears.ExecuteReader();

            while (ArrearsReader.Read())
            {
                App1ArrearsDateRegistered1 = ArrearsReader["ArrearsDateReg1"].ToString();
                App1ArrearsCleared1 = ArrearsReader["ArrearsDateSat1"].ToString();
                App1ArrearsDateRegistered2 = ArrearsReader["ArrearsDateReg2"].ToString();
                App1ArrearsCleared2 = ArrearsReader["ArrearsDateSat2"].ToString();
                App1ArrearsDateRegistered3 = ArrearsReader["ArrearsDateReg3"].ToString();
                App1ArrearsCleared3 = ArrearsReader["ArrearsDateSat3"].ToString();
                App1ArrearsDateRegistered4 = ArrearsReader["ArrearsDateReg4"].ToString();
                App1ArrearsCleared4 = ArrearsReader["ArrearsDateSat4"].ToString();
                App1ArrearsDateRegistered5 = ArrearsReader["ArrearsDateReg5"].ToString();
                App1ArrearsCleared5 = ArrearsReader["ArrearsDateSat5"].ToString();
                App2ArrearsDateRegistered1 = ArrearsReader["App2ArrearsDateReg1"].ToString();
                App2ArrearsCleared1 = ArrearsReader["App2ArrearsDateSat1"].ToString();
                App2ArrearsDateRegistered2 = ArrearsReader["App2ArrearsDateReg2"].ToString();
                App2ArrearsCleared2 = ArrearsReader["App2ArrearsDateSat2"].ToString();
                App2ArrearsDateRegistered3 = ArrearsReader["App2ArrearsDateReg3"].ToString();
                App2ArrearsCleared3 = ArrearsReader["App2ArrearsDateSat3"].ToString();
                App2ArrearsDateRegistered4 = ArrearsReader["App2ArrearsDateReg4"].ToString();
                App2ArrearsCleared4 = ArrearsReader["App2ArrearsDateSat4"].ToString();
                App2ArrearsDateRegistered5 = ArrearsReader["App2ArrearsDateReg5"].ToString();
                App2ArrearsCleared5 = ArrearsReader["App2ArrearsDateSat5"].ToString();

                App1CountyDateRegistered1 = ArrearsReader["CountyCourtDateReg1"].ToString();
                App1CountyAmount1 = ArrearsReader["CountyCourtAmount1"].ToString();
                App1CountyDateSat1 = ArrearsReader["CountyCourtDateSat1"].ToString();
                App1CountyDateRegistered2 = ArrearsReader["CountyCourtDateReg1"].ToString();
                App1CountyAmount2 = ArrearsReader["CountyCourtAmount2"].ToString();
                App1CountyDateSat2 = ArrearsReader["CountyCourtDateSat2"].ToString();
                App1CountyDateRegistered3 = ArrearsReader["CountyCourtDateReg3"].ToString();
                App1CountyAmount3 = ArrearsReader["CountyCourtAmount3"].ToString();
                App1CountyDateSat3 = ArrearsReader["CountyCourtDateSat3"].ToString();
                App1CountyDateRegistered4 = ArrearsReader["CountyCourtDateReg4"].ToString();
                App1CountyAmount4 = ArrearsReader["CountyCourtAmount4"].ToString();
                App1CountyDateSat4 = ArrearsReader["CountyCourtDateSat4"].ToString();
                App1CountyDateRegistered5 = ArrearsReader["CountyCourtDateReg5"].ToString();
                App1CountyAmount5 = ArrearsReader["CountyCourtAmount5"].ToString();
                App1CountyDateSat5 = ArrearsReader["CountyCourtDateSat5"].ToString();
                App2CountyDateRegistered1 = ArrearsReader["App2CountyCourtDateReg1"].ToString();
                App2CountyAmount1 = ArrearsReader["App2CountyCourtAmount1"].ToString();
                App2CountyDateSat1 = ArrearsReader["App2CountyCourtDateSat1"].ToString();
                App2CountyDateRegistered2 = ArrearsReader["App2CountyCourtDateReg1"].ToString();
                App2CountyAmount2 = ArrearsReader["App2CountyCourtAmount2"].ToString();
                App2CountyDateSat2 = ArrearsReader["App2CountyCourtDateSat2"].ToString();
                App2CountyDateRegistered3 = ArrearsReader["App2CountyCourtDateReg3"].ToString();
                App2CountyAmount3 = ArrearsReader["App2CountyCourtAmount3"].ToString();
                App2CountyDateSat3 = ArrearsReader["App2CountyCourtDateSat3"].ToString();
                App2CountyDateRegistered4 = ArrearsReader["App2CountyCourtDateReg4"].ToString();
                App2CountyAmount4 = ArrearsReader["App2CountyCourtAmount4"].ToString();
                App2CountyDateSat4 = ArrearsReader["App2CountyCourtDateSat4"].ToString();
                App2CountyDateRegistered5 = ArrearsReader["App2CountyCourtDateReg5"].ToString();
                App2CountyAmount5 = ArrearsReader["App2CountyCourtAmount5"].ToString();
                App2CountyDateSat5 = ArrearsReader["App2CountyCourtDateSat5"].ToString();

                App1DefaultDateRegistered1 = ArrearsReader["DefaultDateReg1"].ToString();
                App1DefaultAmount1 = ArrearsReader["DefaultAmount1"].ToString();
                App1DefaultDateSat1 = ArrearsReader["DefaultDateSat1"].ToString();
                App1DefaultDateRegistered2 = ArrearsReader["DefaultDateReg2"].ToString();
                App1DefaultAmount2 = ArrearsReader["DefaultAmount2"].ToString();
                App1DefaultDateSat2 = ArrearsReader["DefaultDateSat2"].ToString();
                App1DefaultDateRegistered3 = ArrearsReader["DefaultDateReg3"].ToString();
                App1DefaultAmount3 = ArrearsReader["DefaultAmount3"].ToString();
                App1DefaultDateSat3 = ArrearsReader["DefaultDateSat3"].ToString();
                App1DefaultDateRegistered4 = ArrearsReader["DefaultDateReg4"].ToString();
                App1DefaultAmount4 = ArrearsReader["DefaultAmount4"].ToString();
                App1DefaultDateSat4 = ArrearsReader["DefaultDateSat4"].ToString();
                App1DefaultDateRegistered5 = ArrearsReader["DefaultDateReg5"].ToString();
                App1DefaultAmount5 = ArrearsReader["DefaultAmount5"].ToString();
                App1DefaultDateSat5 = ArrearsReader["DefaultDateSat5"].ToString();
                App2DefaultDateRegistered1 = ArrearsReader["App2DefaultDateReg1"].ToString();
                App2DefaultAmount1 = ArrearsReader["App2DefaultAmount1"].ToString();
                App2DefaultDateSat1 = ArrearsReader["App2DefaultDateSat1"].ToString();
                App2DefaultDateRegistered2 = ArrearsReader["App2DefaultDateReg2"].ToString();
                App2DefaultAmount2 = ArrearsReader["App2DefaultAmount2"].ToString();
                App2DefaultDateSat2 = ArrearsReader["App2DefaultDateSat2"].ToString();
                App2DefaultDateRegistered3 = ArrearsReader["App2DefaultDateReg3"].ToString();
                App2DefaultAmount3 = ArrearsReader["App2DefaultAmount3"].ToString();
                App2DefaultDateSat3 = ArrearsReader["App2DefaultDateSat3"].ToString();
                App2DefaultDateRegistered4 = ArrearsReader["App2DefaultDateReg4"].ToString();
                App2DefaultAmount4 = ArrearsReader["App2DefaultAmount4"].ToString();
                App2DefaultDateSat4 = ArrearsReader["App2DefaultDateSat4"].ToString();
                App2DefaultDateRegistered5 = ArrearsReader["App2DefaultDateReg5"].ToString();
                App2DefaultAmount5 = ArrearsReader["App2DefaultAmount5"].ToString();
                App2DefaultDateSat5 = ArrearsReader["App2DefaultDateSat5"].ToString();

                App1PayDayLoanRegistered1 = ArrearsReader["PayDayDateReg1"].ToString();
                App1PayDayLoanRegistered2 = ArrearsReader["PayDayDateReg2"].ToString();
                App1PayDayLoanRegistered3 = ArrearsReader["PayDayDateReg3"].ToString();
                App1PayDayLoanRegistered4 = ArrearsReader["PayDayDateReg4"].ToString();
                App1PayDayLoanRegistered5 = ArrearsReader["PayDayDateReg5"].ToString();
                App2PayDayLoanRegistered1 = ArrearsReader["App2PayDayDateReg1"].ToString();
                App2PayDayLoanRegistered2 = ArrearsReader["App2PayDayDateReg2"].ToString();
                App2PayDayLoanRegistered3 = ArrearsReader["App2PayDayDateReg3"].ToString();
                App2PayDayLoanRegistered4 = ArrearsReader["App2PayDayDateReg4"].ToString();
                App2PayDayLoanRegistered5 = ArrearsReader["App2PayDayDateReg5"].ToString();

                App1BankruptciesDate = ArrearsReader["App1BankruptciesDate"].ToString();
                App2BankruptciesDate = ArrearsReader["App2BankruptciesDate"].ToString();

                App1IVADateReg1 = ArrearsReader["IVADateReg1"].ToString();
                App1IVASat1 = ArrearsReader["IVASat1"].ToString();
                App1IVADateCom1 = ArrearsReader["IVADateCom1"].ToString();
                App1IVADateReg2 = ArrearsReader["IVADateReg2"].ToString();
                App1IVASat2 = ArrearsReader["IVASat2"].ToString();
                App1IVADateCom2 = ArrearsReader["IVADateCom2"].ToString();
                App1IVADateReg3 = ArrearsReader["IVADateReg3"].ToString();
                App1IVASat3 = ArrearsReader["IVASat3"].ToString();
                App1IVADateCom3 = ArrearsReader["IVADateCom3"].ToString();
                App1IVADateReg4 = ArrearsReader["IVADateReg4"].ToString();
                App1IVASat4 = ArrearsReader["IVASat4"].ToString();
                App1IVADateCom4 = ArrearsReader["IVADateCom4"].ToString();
                App1IVADateReg5 = ArrearsReader["IVADateReg5"].ToString();
                App1IVASat5 = ArrearsReader["IVASat5"].ToString();
                App1IVADateCom5 = ArrearsReader["IVADateCom5"].ToString();
                App2IVADateReg1 = ArrearsReader["App2IVADateReg1"].ToString();
                App2IVASat1 = ArrearsReader["App2IVASat1"].ToString();
                App2IVADateCom1 = ArrearsReader["App2IVADateCom1"].ToString();
                App2IVADateReg2 = ArrearsReader["App2IVADateReg2"].ToString();
                App2IVASat2 = ArrearsReader["App2IVASat2"].ToString();
                App2IVADateCom2 = ArrearsReader["App2IVADateCom2"].ToString();
                App2IVADateReg3 = ArrearsReader["App2IVADateReg3"].ToString();
                App2IVASat3 = ArrearsReader["App2IVASat3"].ToString();
                App2IVADateCom3 = ArrearsReader["App2IVADateCom3"].ToString();
                App2IVADateReg4 = ArrearsReader["App2IVADateReg4"].ToString();
                App2IVASat4 = ArrearsReader["App2IVASat4"].ToString();
                App2IVADateCom4 = ArrearsReader["App2IVADateCom4"].ToString();
                App2IVADateReg5 = ArrearsReader["App2IVADateReg5"].ToString();
                App2IVASat5 = ArrearsReader["App2IVASat5"].ToString();
                App2IVADateCom5 = ArrearsReader["App2IVADateCom5"].ToString();
            }

            SqlCommand myCommandCurrentYears1 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App1CurrentProfessionYears'", AppID), connection);

            var CurrentYears1Reader = myCommandCurrentYears1.ExecuteReader();

            while (CurrentYears1Reader.Read())
            {
                CurrentYearsApp1 = CurrentYears1Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandCurrentYears2 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App2CurrentProfessionYears'", AppID), connection);

            var CurrentYears2Reader = myCommandCurrentYears2.ExecuteReader();

            while (CurrentYears2Reader.Read())
            {
                CurrentYearsApp2 = CurrentYears2Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandCurrentMonths1 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App1CurrentProfessionMonths'", AppID), connection);

            var CurrentMonths1Reader = myCommandCurrentMonths1.ExecuteReader();

            while (CurrentMonths1Reader.Read())
            {
                CurrentMonthsApp1 = CurrentMonths1Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandCurrentMonths2 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App2CurrentProfessionMonths'", AppID), connection);

            var CurrentMonths2Reader = myCommandCurrentMonths2.ExecuteReader();

            while (CurrentMonths2Reader.Read())
            {
                CurrentMonthsApp2 = CurrentMonths2Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandYearsAccounts2 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App2YearsAccountsAvailable'", AppID), connection);

            var YearsAccounts2Reader = myCommandYearsAccounts2.ExecuteReader();

            while (YearsAccounts2Reader.Read())
            {
                YearsAccountsApp2 = YearsAccounts2Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandYearsAccounts1 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'App1YearsAccountsAvailable'", AppID), connection);

            var YearsAccounts1Reader = myCommandYearsAccounts1.ExecuteReader();

            while (YearsAccounts1Reader.Read())
            {
                YearsAccountsApp1 = YearsAccounts1Reader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPropertyRemainingLease = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyRemainingLease'", AppID), connection);

            var PropertyRemainingLeaseReader = myCommandPropertyRemainingLease.ExecuteReader();

            while (PropertyRemainingLeaseReader.Read())
            {
                PropertyRemainingLease = PropertyRemainingLeaseReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandPaymentMethod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SecuredPaymentMethod'", AppID), connection);

            var PaymentMethodReader = myCommandPaymentMethod.ExecuteReader();

            while (PaymentMethodReader.Read())
            {
                securedpaymentmeth = PaymentMethodReader["StoredDataValue"].ToString();
            }

            SqlCommand myCommandConsumerBTL = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ConsumerBuyToLet'", AppID), connection);

            var ConsumerBTLReader = myCommandConsumerBTL.ExecuteReader();

            while (ConsumerBTLReader.Read())
            {
                consumerbtl = ConsumerBTLReader["StoredDataValue"].ToString();
            }



            int? CurrentYearsApp1Int = !string.IsNullOrEmpty(CurrentYearsApp1) ? int.Parse(CurrentYearsApp1) : 0;
            int? CurrentYearsApp2Int = !string.IsNullOrEmpty(CurrentYearsApp2) ? int.Parse(CurrentYearsApp2) : 0;
            int? CurrentMonthsApp1Int = !string.IsNullOrEmpty(CurrentMonthsApp1) ? int.Parse(CurrentMonthsApp1) : 0;
            int? CurrentMonthsApp2Int = !string.IsNullOrEmpty(CurrentMonthsApp2) ? int.Parse(CurrentMonthsApp2) : 0;
            int? YearsAccountsApp2Int = !string.IsNullOrEmpty(YearsAccountsApp2) ? int.Parse(YearsAccountsApp2) : 0;
            int? YearsAccountsApp1Int = !string.IsNullOrEmpty(YearsAccountsApp1) ? int.Parse(YearsAccountsApp1) : 0;

            int? CurrentMonthsTotalApp1 = (CurrentYearsApp1Int * 12) + CurrentMonthsApp1Int;
            int? CurrentMonthsTotalApp2 = (CurrentYearsApp2Int * 12) + CurrentMonthsApp2Int;

            int? PropertyRemainingLeaseint = !string.IsNullOrEmpty(PropertyRemainingLease) ? int.Parse(PropertyRemainingLease) : 0;
            int? PropertyFlatFloorNumberVal = !string.IsNullOrEmpty(PropertyFlatFloorNumber) ? int.Parse(PropertyFlatFloorNumber) : 0;

            int? App1CountyAmount1New = !string.IsNullOrEmpty(App1CountyAmount1) ? int.Parse(App1CountyAmount1) : 0;
            int? App1CountyAmount2New = !string.IsNullOrEmpty(App1CountyAmount2) ? int.Parse(App1CountyAmount2) : 0;
            int? App1CountyAmount3New = !string.IsNullOrEmpty(App1CountyAmount3) ? int.Parse(App1CountyAmount3) : 0;
            int? App1CountyAmount4New = !string.IsNullOrEmpty(App1CountyAmount4) ? int.Parse(App1CountyAmount4) : 0;
            int? App1CountyAmount5New = !string.IsNullOrEmpty(App1CountyAmount5) ? int.Parse(App1CountyAmount5) : 0;
            int? App2CountyAmount1New = !string.IsNullOrEmpty(App2CountyAmount1) ? int.Parse(App2CountyAmount1) : 0;
            int? App2CountyAmount2New = !string.IsNullOrEmpty(App2CountyAmount2) ? int.Parse(App2CountyAmount2) : 0;
            int? App2CountyAmount3New = !string.IsNullOrEmpty(App2CountyAmount3) ? int.Parse(App2CountyAmount3) : 0;
            int? App2CountyAmount4New = !string.IsNullOrEmpty(App2CountyAmount4) ? int.Parse(App2CountyAmount4) : 0;
            int? App2CountyAmount5New = !string.IsNullOrEmpty(App2CountyAmount5) ? int.Parse(App2CountyAmount5) : 0;

            int? App1DefaultAmount1New = !string.IsNullOrEmpty(App1DefaultAmount1) ? int.Parse(App1DefaultAmount1) : 0;
            int? App1DefaultAmount2New = !string.IsNullOrEmpty(App1DefaultAmount2) ? int.Parse(App1DefaultAmount2) : 0;
            int? App1DefaultAmount3New = !string.IsNullOrEmpty(App1DefaultAmount3) ? int.Parse(App1DefaultAmount3) : 0;
            int? App1DefaultAmount4New = !string.IsNullOrEmpty(App1DefaultAmount4) ? int.Parse(App1DefaultAmount4) : 0;
            int? App1DefaultAmount5New = !string.IsNullOrEmpty(App1DefaultAmount5) ? int.Parse(App1DefaultAmount5) : 0;
            int? App2DefaultAmount1New = !string.IsNullOrEmpty(App2DefaultAmount1) ? int.Parse(App2DefaultAmount1) : 0;
            int? App2DefaultAmount2New = !string.IsNullOrEmpty(App2DefaultAmount2) ? int.Parse(App2DefaultAmount2) : 0;
            int? App2DefaultAmount3New = !string.IsNullOrEmpty(App2DefaultAmount3) ? int.Parse(App2DefaultAmount3) : 0;
            int? App2DefaultAmount4New = !string.IsNullOrEmpty(App2DefaultAmount4) ? int.Parse(App2DefaultAmount4) : 0;
            int? App2DefaultAmount5New = !string.IsNullOrEmpty(App2DefaultAmount5) ? int.Parse(App2DefaultAmount5) : 0;

            bool App1IVASat1Val = true;
            bool App1IVASat2Val = true;
            bool App1IVASat3Val = true;
            bool App1IVASat4Val = true;
            bool App1IVASat5Val = true;
            bool App2IVASat1Val = true;
            bool App2IVASat2Val = true;
            bool App2IVASat3Val = true;
            bool App2IVASat4Val = true;
            bool App2IVASat5Val = true;

            if (App1IVASat1 == "Y")
            {

                bool.TryParse(App1IVASat1, out App1IVASat1Val);
                App1IVASat1Val = true;
            }
            else
            {
                bool.TryParse(App1IVASat1, out App1IVASat1Val);
                App1IVASat1Val = false;
            }

            if (App1IVASat2 == "Y")
            {

                bool.TryParse(App1IVASat2, out App1IVASat2Val);
                App1IVASat2Val = true;
            }
            else
            {
                bool.TryParse(App1IVASat2, out App1IVASat2Val);
                App1IVASat2Val = false;
            }

            if (App1IVASat3 == "Y")
            {

                bool.TryParse(App1IVASat3, out App1IVASat3Val);
                App1IVASat3Val = true;
            }
            else
            {
                bool.TryParse(App1IVASat3, out App1IVASat3Val);
                App1IVASat3Val = false;
            }

            if (App1IVASat4 == "Y")
            {

                bool.TryParse(App1IVASat4, out App1IVASat4Val);
                App1IVASat4Val = true;
            }
            else
            {
                bool.TryParse(App1IVASat4, out App1IVASat3Val);
                App1IVASat3Val = false;
            }

            if (App1IVASat5 == "Y")
            {

                bool.TryParse(App1IVASat5, out App1IVASat5Val);
                App1IVASat5Val = true;
            }
            else
            {
                bool.TryParse(App1IVASat5, out App1IVASat5Val);
                App1IVASat5Val = false;
            }

            if (App2IVASat1 == "Y")
            {

                bool.TryParse(App2IVASat1, out App2IVASat1Val);
                App2IVASat1Val = true;
            }
            else
            {
                bool.TryParse(App2IVASat1, out App2IVASat1Val);
                App2IVASat1Val = false;
            }

            if (App2IVASat2 == "Y")
            {

                bool.TryParse(App2IVASat2, out App2IVASat2Val);
                App2IVASat2Val = true;
            }
            else
            {
                bool.TryParse(App2IVASat2, out App2IVASat2Val);
                App2IVASat2Val = false;
            }

            if (App2IVASat3 == "Y")
            {

                bool.TryParse(App2IVASat3, out App2IVASat3Val);
                App2IVASat3Val = true;
            }
            else
            {
                bool.TryParse(App2IVASat3, out App2IVASat3Val);
                App2IVASat3Val = false;
            }

            if (App2IVASat4 == "Y")
            {

                bool.TryParse(App2IVASat4, out App2IVASat4Val);
                App2IVASat4Val = true;
            }
            else
            {
                bool.TryParse(App2IVASat4, out App2IVASat4Val);
                App2IVASat4Val = false;
            }

            if (App2IVASat5 == "Y")
            {

                bool.TryParse(App2IVASat5, out App2IVASat5Val);
                App2IVASat5Val = true;
            }
            else
            {
                bool.TryParse(App2IVASat5, out App2IVASat5Val);
                App2IVASat5Val = false;
            }





            SourcingClient client = new SourcingClient();
            const string licenceKey = "840eadc1-b401-48ae-b9c1-456c3095ae11";

            var arrearsapp1 = new List<Arrear>();
            arrearsapp1.Add(new Arrear()
            {
                DateOfArrear = App1ArrearsDateRegistered1,
                DateCleared = App1ArrearsCleared1

            });
            //arrearsapp1.Add(new Arrear()
            //{
            //    DateOfArrear = App1ArrearsDateRegistered2,
            //    DateCleared = App1ArrearsCleared2
            //});
            //arrearsapp1.Add(new Arrear()
            //{
            //    DateOfArrear = App1ArrearsDateRegistered3,
            //    DateCleared = App1ArrearsCleared3
            //});
            //arrearsapp1.Add(new Arrear()
            //{
            //    DateOfArrear = App1ArrearsDateRegistered4,
            //    DateCleared = App1ArrearsCleared4
            //});
            //arrearsapp1.Add(new Arrear()
            //{
            //    DateOfArrear = App1ArrearsDateRegistered5,
            //    DateCleared = App1ArrearsCleared5
            //});
            var arrearsapp2 = new List<Arrear>();
            arrearsapp2.Add(new Arrear()
            {
                DateOfArrear = App2ArrearsDateRegistered1,
                DateCleared = App2ArrearsCleared1
            });
            arrearsapp2.Add(new Arrear()
            {
                DateOfArrear = App2ArrearsDateRegistered2,
                DateCleared = App2ArrearsCleared2
            });
            arrearsapp2.Add(new Arrear()
            {
                DateOfArrear = App2ArrearsDateRegistered3,
                DateCleared = App2ArrearsCleared3
            });
            arrearsapp2.Add(new Arrear()
            {
                DateOfArrear = App2ArrearsDateRegistered4,
                DateCleared = App2ArrearsCleared4
            });
            arrearsapp2.Add(new Arrear()
            {
                DateOfArrear = App2ArrearsDateRegistered5,
                DateCleared = App2ArrearsCleared5
            });

            var ccjapp1 = new List<CCJ>();
            ccjapp1.Add(new CCJ()
            {
                DateRegistered = App1CountyDateRegistered1,
                DateSatisfied = App1CountyDateSat1,
                Amount = App1CountyAmount1New

            });
            //ccjapp1.Add(new CCJ()
            //{
            //    DateRegistered = App1CountyDateRegistered2,
            //    DateSatisfied = App1CountyDateSat2,
            //    Amount = App1CountyAmount2New

            //});
            //ccjapp1.Add(new CCJ()
            //{
            //    DateRegistered = App1CountyDateRegistered3,
            //    DateSatisfied = App1CountyDateSat3,
            //    Amount = App1CountyAmount3New

            //});
            //ccjapp1.Add(new CCJ()
            //{
            //    DateRegistered = App1CountyDateRegistered4,
            //    DateSatisfied = App1CountyDateSat4,
            //    Amount = App1CountyAmount4New

            //});
            //ccjapp1.Add(new CCJ()
            //{
            //    DateRegistered = App1CountyDateRegistered5,
            //    DateSatisfied = App1CountyDateSat5,
            //    Amount = App1CountyAmount5New

            //});
            var ccjapp2 = new List<CCJ>();
            ccjapp2.Add(new CCJ()
            {
                DateRegistered = App2CountyDateRegistered1,
                DateSatisfied = App2CountyDateSat1,
                Amount = App2CountyAmount1New

            });
            ccjapp2.Add(new CCJ()
            {
                DateRegistered = App2CountyDateRegistered2,
                DateSatisfied = App2CountyDateSat2,
                Amount = App2CountyAmount2New

            });
            ccjapp2.Add(new CCJ()
            {
                DateRegistered = App2CountyDateRegistered3,
                DateSatisfied = App2CountyDateSat3,
                Amount = App2CountyAmount3New

            });
            ccjapp2.Add(new CCJ()
            {
                DateRegistered = App2CountyDateRegistered4,
                DateSatisfied = App2CountyDateSat4,
                Amount = App2CountyAmount4New

            });
            ccjapp2.Add(new CCJ()
            {
                DateRegistered = App2CountyDateRegistered5,
                DateSatisfied = App2CountyDateSat5,
                Amount = App2CountyAmount5New

            });

            var paydayapp1 = new List<PaydayLoan>();
            paydayapp1.Add(new PaydayLoan()
            {
                DateOfLoan = App1PayDayLoanRegistered1,

            });
            paydayapp1.Add(new PaydayLoan()
            {
                DateOfLoan = App1PayDayLoanRegistered2,

            });
            paydayapp1.Add(new PaydayLoan()
            {
                DateOfLoan = App1PayDayLoanRegistered3,

            });
            paydayapp1.Add(new PaydayLoan()
            {
                DateOfLoan = App1PayDayLoanRegistered4,

            });
            paydayapp1.Add(new PaydayLoan()
            {
                DateOfLoan = App1PayDayLoanRegistered5,

            });

            var paydayapp2 = new List<PaydayLoan>();
            paydayapp2.Add(new PaydayLoan()
            {
                DateOfLoan = App2PayDayLoanRegistered1,

            });
            paydayapp2.Add(new PaydayLoan()
            {
                DateOfLoan = App2PayDayLoanRegistered2,

            });
            paydayapp2.Add(new PaydayLoan()
            {
                DateOfLoan = App2PayDayLoanRegistered3,

            });
            paydayapp2.Add(new PaydayLoan()
            {
                DateOfLoan = App2PayDayLoanRegistered4,

            });
            paydayapp2.Add(new PaydayLoan()
            {
                DateOfLoan = App2PayDayLoanRegistered5,

            });

            var bankapp1 = new List<Bankruptcy>();
            bankapp1.Add(new Bankruptcy()
            {
                DateDischarged = App1BankruptciesDate,

            });

            var bankapp2 = new List<Bankruptcy>();
            bankapp2.Add(new Bankruptcy()
            {
                DateDischarged = App2BankruptciesDate,

            });

            var ivaapp1 = new List<IVA>();
            ivaapp1.Add(new IVA()
            {
                SatisfactoryConduct = App1IVASat1Val,
                DateRegistered = App1IVADateReg1,
                DateCompleted = App1IVADateCom1

            });


            SourceInput sourceInput = new SourceInput()
            {
                SiteId = siteId,
                Applicant1 = new Applicant()
                {
                    DateOfBirth = app1DOBdate.ToShortDateString(),
                    ApplicantType = ApplicantTypeEnum.No_Selection,
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app1IncomeInt,
                    },
                    FirstName = app1FirstName,
                    LastName = app1Surname,
                    Title = app1Title,
                    Gender = GetGender(app1Title),
                    ExistingMortgage = new ExistingMortgage()
                    {
                        OutstandingBalance = (int)mortbal
                    },
                    CreditHistoryDetails = new CreditHistoryDetails()
                    {
                        Arrears = arrearsapp1.ToArray(),
                        CCJs = ccjapp1.ToArray(),
                       // PaydayLoans = paydayapp1.ToArray(),
                        Bankruptcies = bankapp1.ToArray(),
                        IVAs = ivaapp1.ToArray()

                    },
                    //SelfEmployedDetails = new SelfEmployedDetails()
                    //{
                    //    NoOfMonthsTrading = CurrentMonthsTotalApp1,
                    //    NoOfYearsAccountsAvailable = YearsAccountsApp1Int


                    //}
                },
                CompanyId = tecCompanyId,
                Term = (int)term/12,
                ExpectedValuation = (int)propertyValue,
                LoanRequired = (int)loanValue,

                MortgageType = GetMortgType(MortgageType),
                TermUnit = TermUnitEnum.Years,
                ReasonForMortgage = GetLoanReason(LoanReason),
                SearchMatchingType = SearchMatchingTypeEnum.Include_Near_Misses,
                PaymentMethod = GetPaymentMethod(securedpaymentmeth),


                NearMissesDetails = new NearMissesDetails()
                {
                    MaximumLTVBuffer = MaxLTVbuffer,
                    MinimumLTVBuffer = MinLTVBuffer,
                    MaximumLoanBuffer = MaxLoanBuffer,
                    MinimumLoanBuffer = MinLoanBuffer,
                    MaximumTermBuffer = MaxTermBuffer,
                    MinimumTermBuffer = MinTermBuffer,
                    MaximumValuationBuffer = MaxValBuffer,
                    MinimumValuationBuffer = MinValBuffer

                },
                TrueCostDetails = new TrueCostDetails()
                {

                    CalculateOverNoOfMonths = (int?)CalCostNum,
                    CalculateOverInitialPeriod = iPeriod,
                    DeductCashback = false,
                    AddFeesPayableToTruecostTotal = FessAddedToLoan,
                    PayBrokerFeeUpFront = PayBrokerFeeUpFront,
                },


                SecuredLoanDetails = new SecuredLoanDetails()
                {

                    // ExitStrategy = GetExitStrat(ExitStrategy),
                    //ChangeOfUseRequired = YesNoEnum.No,
                    //ProjectRequiresPlanning = YesNoEnum.No,
                    //ProjectPlanningGranted = YesNoEnum.No

                    //BridgingDetails = new BridgingDetails()
                    //{
                    //    BridgingPropertyUse = GetPropUse(BridgingPropertyUse),
                    //    PropertyPreviouslyBridged = GetPrevProp(PropertyPreviouslyBridged),
                    //    BridgingLoanPurpose = GetLoanPur(BridgingLoanPurpose),
                    //    BridgingAdditionalPropertiesTotalOutstanding = TotalOutstanding,
                    //    BridgingAdditionalPropertiesTotalValue = TotalPropVal,
                    //    BridgingPaymentMethod = GetPayM(PaymentMethod),
                    //    OccupiedByClientOrFamilyMember = GetOccupied(OccupiedBy),
                    //    LimitedCompany = GetCompany(LimitedCompany),


                    //},

                },


                Filters = new Filters()
                {
                    //OverpaymentsAllowed = overPayment,
                    SortResultsByColumn = new SortColumn()
                    {
                        Column = GetSortColunm(SortColunm)
                    }
                },


                FeeOverrideDetails = new FeeOverrideDetails()
                {
                    BrokerFeeFlatAmount = brokerfeeflatAm,
                    BrokerFeePercent = brokerfeeflatPerc
                },

                PropertyDetails = new PropertyDetails()
                {
                    AddressLine1 = PurchaseAddressLine1,
                    AddressLine2 = PurchaseAddressLine2,
                    Town = PurchaseAddressLine3,
                    County = PurchaseAddressCounty,
                    PostCode = PurchaseAddressPostCode,
                    PropertyType = GetPropertType(PropertyType),
                    PropertyStyle = GetPropertStyle(PropertyStyle),
                    PropertyTenure = GetPropertTenure(PropertyTenure),
                    PropertyWalls = GetPropertWalls(PropertyConstructionType),
                    PropertyRoof = GetPropertRoof(PropertyRoofConstructionType),
                    PropertyUse = GetPropertyUse(PropertyUse),
                    ExCouncil = GetExCouncil(PropertyCouncilPurchase),
                    LeaseholdDetails = new LeaseholdDetails()
                    {
                        LeaseRemainingYears = PropertyRemainingLeaseint,
                    },
                    FlatDetails = new FlatDetails()
                    {

                        NumFloorsInBlock = PropertyFlatFloorNumberVal,
                    },



                },


                BuyToLetDetails = new BuyToLetDetails()
                {
                    ConsumerBuyToLet = GetConsumerbtl(consumerbtl)
                },

                RemortgageDetails = new RemortgageDetails()
                {
                    ReasonForRemortgage = getReasonForRemortgage(mortgagereason),
                },



            };


            if (!string.IsNullOrEmpty(app2Title))
            {
                sourceInput.Applicant2 = new Applicant()
                {
                    EmploymentStatus = GetEmploymentStatus(App2EmploymentStatus),
                    EmployedDetails = new EmployedDetails()
                    {
                        BasicAnnualSalaryGross = app2IncomeInt
                    },
                    Title = app2Title,
                    FirstName = app2FirstName,
                    LastName = app2Surname,
                    Gender = GetGender(app2Title),
                    DateOfBirth = app2DOBdate.ToShortDateString(),
//                    SelfEmployedDetails = new SelfEmployedDetails()
//                    {
//                        NoOfMonthsTrading = CurrentMonthsTotalApp2,
//                        NoOfYearsAccountsAvailable = YearsAccountsApp2Int
//
//
//                    },
//                    CreditHistoryDetails = new CreditHistoryDetails()
//                    {
//                        Arrears = arrearsapp2.ToArray(),
//                        CCJs = ccjapp2.ToArray(),
//                        PaydayLoans = paydayapp2.ToArray(),
//                        Bankruptcies = bankapp2.ToArray(),
//                        IVAs = ivaapp1.ToArray()
//                    }
                };
            }


            var additionalFees = new List<AdditionalFee>();

            decimal ValFeeAmountInt = 0;
            decimal.TryParse(ValFeeAmount, out ValFeeAmountInt);

            decimal ValFeeAmountRefint = 0;
            decimal.TryParse(ValFeeAmountRef, out ValFeeAmountRefint);

            bool valaddtoloannew;

            if (ValFeeAddToLoan == "Y")
            {
                valaddtoloannew = true;
            }
            else
            {
                valaddtoloannew = false;
            }

            if (ValFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = valaddtoloannew,
                    FeeType = FeeTypeEnum.ValuationFee,
                    WhenPayable = WhenPayableEnum.OnApplication,
                    WhenRefundable = GetWhenrefund(ValFeeWhenRef),
                    AmountRefundable = ValFeeAmountRefint,
                    FeeAmount = ValFeeAmountInt,
                });

            }

            decimal ArrFeeAmountRefInt = 0;
            decimal.TryParse(ArrFeeAmountRef, out ArrFeeAmountRefInt);

            decimal ArrFeeAmountInt = 0;
            decimal.TryParse(ArrFeeAmount, out ArrFeeAmountInt);


            bool ArrFeeAddToLoannew;

            if (ArrFeeAddToLoan == "Y")
            {
                ArrFeeAddToLoannew = true;
            }
            else
            {
                ArrFeeAddToLoannew = false;
            }

            if (ArrFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = ArrFeeAddToLoannew,
                    FeeType = FeeTypeEnum.ArrangementFee,
                    WhenPayable = GetPayable(ArrFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(ArrFeeWhenRef),
                    AmountRefundable = ArrFeeAmountRefInt,
                    FeeAmount = ArrFeeAmountInt,
                });
            }

            decimal BrokerFeeAmountInt = 0;
            decimal.TryParse(BrokerFeeAmount, out BrokerFeeAmountInt);

            decimal BrokerFeeAmountRefint = 0;
            decimal.TryParse(BrokerFeeAmountRef, out BrokerFeeAmountRefint);

            bool BrokerFeeAddToLoannew;

            if (BrokerFeeAddToLoan == "Y")
            {
                BrokerFeeAddToLoannew = true;
            }
            else
            {
                BrokerFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = BrokerFeeAddToLoannew,
                    FeeType = FeeTypeEnum.BrokerFee,
                    WhenPayable = GetPayable(BrokerFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(BrokerFeeWhenRef),
                    AmountRefundable = BrokerFeeAmountRefint,
                    FeeAmount = BrokerFeeAmountInt,
                });

            }

            decimal DeedsFeeAmountInt = 0;
            decimal.TryParse(DeedsFeeAmount, out DeedsFeeAmountInt);

            decimal DeedsFeeAmountRefint = 0;
            decimal.TryParse(DeedsFeeAmountRef, out DeedsFeeAmountRefint);

            bool DeedsFeeAddToLoannew;

            if (DeedsFeeAddToLoan == "Y")
            {
                DeedsFeeAddToLoannew = true;
            }
            else
            {
                DeedsFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = DeedsFeeAddToLoannew,
                    FeeType = FeeTypeEnum.DeedsReleaseFee,
                    WhenPayable = WhenPayableEnum.OnMaturity,
                    WhenRefundable = GetWhenrefund(DeedsFeeWhenRef),
                    AmountRefundable = DeedsFeeAmountRefint,
                    FeeAmount = DeedsFeeAmountInt,
                });

            }


            decimal BookingFeeAmountInt = 0;
            decimal.TryParse(BookingFeeAmount, out BookingFeeAmountInt);

            decimal BookingFeeAmountRefint = 0;
            decimal.TryParse(BookingFeeAmountRef, out BookingFeeAmountRefint);

            bool BookingFeeAddToLoannew;

            if (BookingFeeAddToLoan == "Y")
            {
                BookingFeeAddToLoannew = true;
            }
            else
            {
                BookingFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = BookingFeeAddToLoannew,
                    FeeType = FeeTypeEnum.BookingFee,
                    WhenPayable = GetPayable(BookingFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(BookingFeeWhenRef),
                    AmountRefundable = BookingFeeAmountRefint,
                    FeeAmount = BookingFeeAmountInt,
                });

            }


            decimal ChapsFeeAmountInt = 0;
            decimal.TryParse(ChapsFeeAmount, out ChapsFeeAmountInt);

            decimal ChapsFeeAmountRefint = 0;
            decimal.TryParse(ChapsFeeAmountRef, out ChapsFeeAmountRefint);

            bool ChapsFeeAddToLoannew;

            if (ChapsFeeAddToLoan == "Y")
            {
                ChapsFeeAddToLoannew = true;
            }
            else
            {
                ChapsFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = ChapsFeeAddToLoannew,
                    FeeType = FeeTypeEnum.CHAPSFee,
                    WhenPayable = GetPayable(ChapsFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(ChapsFeeWhenRef),
                    AmountRefundable = ChapsFeeAmountRefint,
                    FeeAmount = ChapsFeeAmountInt,
                });

            }

            decimal HigherLendingFeeAmountInt = 0;
            decimal.TryParse(HigherLendingFeeAmount, out HigherLendingFeeAmountInt);

            decimal HigherLendingFeeAmountRefint = 0;
            decimal.TryParse(HigherLendingFeeAmountRef, out HigherLendingFeeAmountRefint);

            bool HigherLendingFeeAddToLoannew;

            if (HigherLendingFeeAddToLoan == "Y")
            {
                HigherLendingFeeAddToLoannew = true;
            }
            else
            {
                HigherLendingFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = HigherLendingFeeAddToLoannew,
                    FeeType = FeeTypeEnum.HlcFee,
                    WhenPayable = GetPayable(HigherLendingFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(HigherLendingFeeWhenRef),
                    AmountRefundable = HigherLendingFeeAmountRefint,
                    FeeAmount = HigherLendingFeeAmountInt,
                });

            }

            decimal DisFeeAmountInt = 0;
            decimal.TryParse(DisFeeAmount, out DisFeeAmountInt);

            decimal DisFeeAmountRefint = 0;
            decimal.TryParse(DisFeeAmountRef, out DisFeeAmountRefint);

            bool DisFeeAddToLoannew;

            if (DisFeeAddToLoan == "Y")
            {
                DisFeeAddToLoannew = true;
            }
            else
            {
                DisFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = DisFeeAddToLoannew,
                    FeeType = FeeTypeEnum.DisbursementFee,
                    WhenPayable = GetPayable(DisFeeWhenRef),
                    WhenRefundable = GetWhenrefund(DisFeeWhenPayable),
                    AmountRefundable = DisFeeAmountRefint,
                    FeeAmount = DisFeeAmountInt,
                });

            }

            decimal OwnBrokerFeeAmountInt = 0;
            decimal.TryParse(OwnBrokerFeeAmount, out OwnBrokerFeeAmountInt);

            decimal OwnBrokerFeeAmountRefint = 0;
            decimal.TryParse(OwnBrokerFeeAmountRef, out OwnBrokerFeeAmountRefint);

            bool OwnBrokerFeeAddToLoannew;

            if (OwnBrokerFeeAddToLoan == "Y")
            {
                OwnBrokerFeeAddToLoannew = true;
            }
            else
            {
                OwnBrokerFeeAddToLoannew = false;
            }

            if (BrokerFeeAmountInt > 0)
            {
                additionalFees.Add(new AdditionalFee()
                {
                    AddToLoan = OwnBrokerFeeAddToLoannew,
                    FeeType = FeeTypeEnum.BrokerApplicationFee,
                    WhenPayable = GetPayable(OwnBrokerFeeWhenPayable),
                    WhenRefundable = GetWhenrefund(OwnBrokerFeeWhenRef),
                    AmountRefundable = OwnBrokerFeeAmountRefint,
                    FeeAmount = OwnBrokerFeeAmountInt,
                });

            }

            decimal ProcFeeint = 0;
            decimal.TryParse(ProcFee, out ProcFeeint);


            var procFees = new List<ProcFee>();

            procFees.Add(new ProcFee()
            {
                ProcFeeType = ProcFeeTypeEnum.Packager,
                FeeAmount = ProcFeeint,
                Company = ProcFeeCompanyName
            });


            if (MortgageType == "Secured Loan Advised MCD")
            {
                CompanyName = "Positive Lending";
                BrokerName = "Positive Lending";
                BrokerTelephoneNumber = "0845 260 7511";
                CompanyAddressLine1 = "Arena Business Centre";
                CompanyAddressLine2 = "9 Nimrod Way";
                CompanyAddressLine3 = "Ferndown Industrial Estate";
                CompanyCounty = "Dorset";
                CompanyPostCode = "BH21 7WH";

             }

            //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(sourceInput));
            //HttpContext.Current.Response.End();
			
//        XmlDocument myXml = new XmlDocument();
//        XPathNavigator xNav = myXml.CreateNavigator();
//        XmlSerializer y = new XmlSerializer(sourceInput.GetType());
//        using (var xs = xNav.AppendChild())
//        {
//            y.Serialize(xs, sourceInput);
//        }
//        HttpContext.Current.Response.ContentType = "text/xml";
//        HttpContext.Current.Response.Write(myXml.OuterXml);
//        HttpContext.Current.Response.End();
            var test = client.RunSource(licenceKey, sourceInput);

            var testing = client.RunSource(licenceKey, sourceInput);
            var doc = client.GenerateIllustration(licenceKey, new IllustrationInput()
            {
                BrokerCompanyName = CompanyName,
                BrokerName = BrokerName,
                BrokerTelephoneNumber = BrokerTelephoneNumber,
                BrokerAddress = new Address()
                {
                    AddressLine1 = CompanyAddressLine1,
                    AddressLine2 = CompanyAddressLine2,
                    AddressLine3 = CompanyAddressLine3,
                    County = CompanyCounty,
                    Country = "United Kingdom",
                    PostCode = CompanyPostCode,

                },
                IllustrationType = IllustrationType.Esis,
                SourceInput = sourceInput,
                ProductId = productId,
                IsSaleAdvised = true,
                FeeWaiverText = FeeWaiver,
                ComplaintsText = Complaints,
                IsAdverseDeclared = true,
                AdditionalFees = additionalFees.ToArray(),
                ProcFees = procFees.ToArray()






            });

            //var doc2 = client.GenerateIllustration(licenceKey, new IllustrationInput()
            //{
            //    IllustrationType = IllustrationType.Esis
            //});




            if (doc.Error.Any())
            {
                string errors = "";
                foreach (var item in doc.Error)
                {
                    errors += item + " ";
                }

                Response.Write(errors);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();

            }



            try
            {

                byte[] bytes = Convert.FromBase64String(doc.DocumentData);

                SqlCommand insertESIS = new SqlCommand();
                insertESIS.Connection = connection;
                var cal = new CultureInfo("tr-TR", false).Calendar;

                string woy2 = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, System.DayOfWeek.Monday).ToString();
                int number = Convert.ToInt32(woy2);
                number += 1;
                string newWOY = number.ToString();

                string filedate = (DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString());
                insertESIS.CommandText = string.Format("insert into tblPDFFiles(AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate, PDFHistoryFileName) values ('" + AppID + "', 'Broker ESIS Document', 'application/pdf', @Data, '" + Plan + "', '" + Lender + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','/positive-lending/" + newWOY + "-" + DateTime.Now.Year.ToString() + "/" + filedate + ".pdf')");
                insertESIS.Parameters.Add("@Data", SqlDbType.VarBinary).Value = bytes;
                insertESIS.ExecuteNonQuery();
                databaseFileRead(AppID, "F:\\data\\attachments\\positive-lending\\" + newWOY + "-" + DateTime.Now.Year.ToString() + "\\" + filedate + ".pdf");
                HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + "" + filedate + ".pdf");
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.BinaryWrite(bytes);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Redirect("", false);




            }
            catch (Exception e)
            {
                HttpContext.Current.Response.Write(e);
            }

        }
        catch (Exception e)
        {

            HttpContext.Current.Response.Write(e);

        }
    }

    public static void databaseFileRead(string AppID, string varPathToNewLocation)
    {
        using (var varConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString()))
        using (var sqlQuery = new SqlCommand(@"SELECT Data FROM [dbo].[tblPDFFiles] WHERE [AppID] = @varID order by CreatedDate desc", varConnection))
        {
            varConnection.Open();
            sqlQuery.Parameters.AddWithValue("@varID", AppID);
            using (var sqlQueryResult = sqlQuery.ExecuteReader())
                if (sqlQueryResult != null)
                {
                    sqlQueryResult.Read();
                    var blob = new Byte[(sqlQueryResult.GetBytes(0, 0, null, 0, int.MaxValue))];
                    sqlQueryResult.GetBytes(0, 0, blob, 0, blob.Length);
                    using (var fs = new FileStream(varPathToNewLocation, FileMode.Create, FileAccess.Write))
                        fs.Write(blob, 0, blob.Length);
                }
        }

    }


    private Boolean InsertUpdateData(SqlCommand cmd)

    {

        String strConnString = System.Configuration.ConfigurationManager

        .ConnectionStrings["conString"].ConnectionString;

        SqlConnection con = new SqlConnection(strConnString);

        cmd.CommandType = CommandType.Text;

        cmd.Connection = con;

        try

        {

            con.Open();

            cmd.ExecuteNonQuery();

            return true;

        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

            return false;

        }

        finally

        {

            con.Close();

            con.Dispose();

        }

    }



    private GenderEnum? GetGender(string title)
    {
        switch (title)
        {
            case "Mr":
                return GenderEnum.Male;
            default:
                return GenderEnum.Female;
        }
    }



    private EnumSortColumns GetSortColunm(string SortColunmVal)
    {
        switch (SortColunmVal)
        {
            case "LenderName":
                return EnumSortColumns.LenderName;
            case "InitialRatePeriodMonths":
                return EnumSortColumns.InitialRatePeriodMonths;
            case "InitialMonthlyPayment":
                return EnumSortColumns.InitialMonthlyPayment;
            case "TrueCost":
                return EnumSortColumns.Truecost;
            case "MaxLTVAvailable":
                return EnumSortColumns.MaxLTVAvailable;
            case "FeesTotal":
                return EnumSortColumns.FeesTotal;
            case "MaxLoanAvailable":
                return EnumSortColumns.MaxLoanAvailable;
            default:
                return EnumSortColumns.InitialMonthlyPayment;
        }
    }

    private ReasonForMortgageEnum GetLoanReason(string ReasonForMortgage)
    {
        switch (ReasonForMortgage)
        {

            case "Remortgage":
                return ReasonForMortgageEnum.Remortgage;
            default:
                return ReasonForMortgageEnum.Remortgage;
        }
    }

    private BridgingPaymentMethodEnum? GetPayM(string BridgingPaymentMethod)
    {
        switch (BridgingPaymentMethod)
        {
            case "Serviced":
                return BridgingPaymentMethodEnum.Serviced;
            case "Rolled Up":
                return BridgingPaymentMethodEnum.RolledUp;
            default:
                return BridgingPaymentMethodEnum.Retained;

        }
    }

    private YesNoEnum? GetPrevProp(string PropertyPreviouslyBridged)
    {
        switch (PropertyPreviouslyBridged)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private YesNoEnum? GetOccupied(string OccupiedBy)
    {
        switch (OccupiedBy)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private YesNoEnum? GetCompany(string LimitedCompany)
    {
        switch (LimitedCompany)
        {
            case "Yes":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private MortgageTypeEnum GetMortgType(string MortgageType)
    {
        switch (MortgageType)
        {
            case "Secured Loan":
                return MortgageTypeEnum.Secured_Loan;
            case "Secured Loan BTL":
                return MortgageTypeEnum.Secured_Loan_BTL;
            case "Bridging Execution Only":
                return MortgageTypeEnum.Bridging_Loan;
			case "Bridging Packaged":
                return MortgageTypeEnum.Bridging_Loan;
            case "Secured Loan BTL Execution Only MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
			case "Secured Loan BTL Advised MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
			case "Secured Loan BTL Packaged MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
            default:
                return MortgageTypeEnum.Secured_Loan;

        }
    }

    private WhenPayableEnum GetPayable(string whenpay)
    {
        switch (whenpay)
        {
            case "OnApplication":
                return WhenPayableEnum.OnApplication;
            case "OnCompletion":
                return WhenPayableEnum.OnCompletion;
            case "UpFront":
                return WhenPayableEnum.UpFront;
            case "OnOffer":
                return WhenPayableEnum.OnOffer;
            case "OnManturity":
                return WhenPayableEnum.OnMaturity;
            default:
                return WhenPayableEnum.OnApplication;
        }
    }

    private FeeTypeEnum GetFeeType(string feetype)
    {
        switch (feetype)
        {
            case "BrokerApplicationFee":
                return FeeTypeEnum.BrokerApplicationFee;
            case "BrokerCompletionFee":
                return FeeTypeEnum.BrokerCompletionFee;
            case "ArrangementFee":
                return FeeTypeEnum.ArrangementFee;
            case "BookingFee":
                return FeeTypeEnum.BookingFee;
            case "ValuationFee":
                return FeeTypeEnum.ValuationFee;
            case "HLCFee":
                return FeeTypeEnum.HlcFee;
            case "InsuranceFee":
                return FeeTypeEnum.InsuranceFee;
            case "ProcessingFee":
                return FeeTypeEnum.ProcessingFee;
            case "AdminFee":
                return FeeTypeEnum.AdminFee;
            case "LegalFee":
                return FeeTypeEnum.LegalFee;
            case "CHAPSFee":
                return FeeTypeEnum.CHAPSFee;
            case "MortgaeDischargeFee":
                return FeeTypeEnum.MortgageDischargeFee;
            case "DeedsReleaseFee":
                return FeeTypeEnum.DeedsReleaseFee;
            case "BrokerFee":
                return FeeTypeEnum.BrokerFee;
            default:
                return FeeTypeEnum.BrokerApplicationFee;
        }
    }

    private WhenRefundableEnum GetWhenrefund(string refund)
    {
        switch (refund)
        {
            case "NotRefundable":
                return WhenRefundableEnum.NotRefundable;
            case "Priortooffer":
                return WhenRefundableEnum.OnOffer;
            case "PriortoValuation":
                return WhenRefundableEnum.OnValuation;
            case "PriortoCompletion":
                return WhenRefundableEnum.OnCompletion;
            default:
                return WhenRefundableEnum.NotRefundable;
        }
    }

    private PropertyTypeEnum? GetPropertType(string proptype)
    {
        switch (proptype)
        {
            case "House":
                return PropertyTypeEnum.House;
            case "Flat":
                return PropertyTypeEnum.Flat;
            case "Maisonette":
                return PropertyTypeEnum.Maisonette;
            case "Bungalow":
                return PropertyTypeEnum.Bungalow;
            default:
                return null;
        }
    }

    private PropertyStyleEnum? GetPropertStyle(string propstyle)
    {
        switch (propstyle)
        {
            case "Detached":
                return PropertyStyleEnum.Detached;
            case "Semi Detached":
                return PropertyStyleEnum.Semi_Detached;
            case "Terraced":
                return PropertyStyleEnum.Terraced;
            case "Purpose Built":
                return PropertyStyleEnum.Purpose_Built;
            case "Above Shop":
                return PropertyStyleEnum.Above_Shop;
            case "Studio":
                return PropertyStyleEnum.Studio;
            case "Conversion":
                return PropertyStyleEnum.Conversion;
            default:
                return null;
        }
    }

    private PropertyTenureEnum? GetPropertTenure(string proptenure)
    {
        switch (proptenure)
        {
            case "Freehold":
                return PropertyTenureEnum.Freehold;
            case "Leasehold":
                return PropertyTenureEnum.Leasehold;
            case "Commonhold":
                return PropertyTenureEnum.Commonhold;
            case "Feudal":
                return PropertyTenureEnum.Feudal;
            default:
                return null;
        }
    }

    private PropertyWallsEnum? GetPropertWalls(string propwalls)
    {
        switch (propwalls)
        {
            case "Concrete":
                return PropertyWallsEnum.Concrete;
            case "Timber Framed":
                return PropertyWallsEnum.Timber_Framed;
            case "Steel Framed":
                return PropertyWallsEnum.Steel_Framed;
            case "Brick":
                return PropertyWallsEnum.Brick;
            default:
                return null;
        }
    }

    private PropertyRoofEnum? GetPropertRoof(string proproof)
    {
        switch (proproof)
        {
            case "Flat":
                return PropertyRoofEnum.Flat_Roof;
            case "Thatched":
                return PropertyRoofEnum.Thatched;
            case "Slate":
                return PropertyRoofEnum.Slate;
            case "Tile":
                return PropertyRoofEnum.Tile;
            default:
                return null;
        }
    }

    private PropertyUseEnum? GetPropertyUse(string propuse)
    {
        switch (propuse)
        {
            case "PrimaryResidence":
                return PropertyUseEnum.Primary_Residence;
            case "SecondHome":
                return PropertyUseEnum.Second_Home;
            case "Holiday Home":
                return PropertyUseEnum.Holiday_Home;
            case "SittingTenant":
                return PropertyUseEnum.Sitting_Tenant;
            default:
                return null;
        }
    }

    private YesNoEnum? GetExCouncil(string excouncil)
    {
        switch (excouncil)
        {
            case "Y":
                return YesNoEnum.Yes;
            case "N":
                return YesNoEnum.No;
            default:
                return null;
        }
    }

    private EmploymentStatusEnum? GetEmploymentStatus(string employmentstatus)
    {
        switch (employmentstatus)
        {
            case "Employed":
                return EmploymentStatusEnum.Employed;
            case "Self Employed":
                return EmploymentStatusEnum.Self_Employed;
            case "Self Employed Non Audited":
                return EmploymentStatusEnum.Self_Employed;
            case "Retired":
                return EmploymentStatusEnum.Retired;
            case "Househusband":
                return EmploymentStatusEnum.Homemaker;
            case "Housewife":
                return EmploymentStatusEnum.Homemaker;
            default:
                return null;
        }

    }


    private PaymentMethodEnum GetPaymentMethod(string securedpaymentmethod)
    {
        switch (securedpaymentmethod)
        {
            case "Repayment":
                return PaymentMethodEnum.Repayment;
            case "Endowment":
                return PaymentMethodEnum.Endowment;
            case "Individual Savings Account":
                return PaymentMethodEnum.Individual_Savings_Account;
            case "Interest Only":
                return PaymentMethodEnum.Interest_Only;
            case "Part Investment":
                return PaymentMethodEnum.Part_Investment;
            case "Pension":
                return PaymentMethodEnum.Pension;
            case "Sale Of Property":
                return PaymentMethodEnum.Sale_Of_Property;
            default:
                return PaymentMethodEnum.Repayment;
        }
    }

    private YesNoEnum? GetConsumerbtl(string consumerbtl)
    {
        switch (consumerbtl)
        {
            case "Y":
                return YesNoEnum.Yes;
            case "N":
                return YesNoEnum.No;
            default:
                return null;
        }
    }

    private ReasonForRemortgageEnum? getReasonForRemortgage(string mortgagereason)
    {
        switch (mortgagereason)
        {
            case "Business Purpose":
                return ReasonForRemortgageEnum.Business_Purposes;
            case "Debt Consolidation":
                return ReasonForRemortgageEnum.Debt_Consolidation;
            case "Home Improvements":
                return ReasonForRemortgageEnum.Home_Improvements;
            case "Other Property Purchase":
                return ReasonForRemortgageEnum.Other_Property_Purchase;
            case "School Fees":
                return ReasonForRemortgageEnum.School_Fees;
            case "Tax Bill":
                return ReasonForRemortgageEnum.Tax_Bill;
            default:
                return null;
        }

    }




    public static class NullableInt
    {
        public static bool TryParse(string text, out int? outValue)
        {
            int parsedValue;
            bool success = int.TryParse(text, out parsedValue);
            outValue = success ? (int?)parsedValue : null;
            return success;
        }
    }
}

//public class DocumentModel
//{
//    public int ProductId { get; set; }
//}
