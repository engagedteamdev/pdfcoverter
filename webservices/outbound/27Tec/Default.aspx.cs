using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using ServiceReference27Tec1;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Net;

public partial class Default : Page
{
    private string AppID = HttpContext.Current.Request["AppID"];

    protected void Page_Load(object sender, EventArgs e)
    {
        httpPost();
    }


    public void httpPost()
    {

        decimal loanValue = 0, propertyValue = 0, mortbal = 0, brokerfeeflatAm = 0, brokerfeeflatPerc = 0, TotalPropVal = 0, TotalOutstanding = 0;
        int app1IncomeInt = 0, app2IncomeInt = 0, MaximumLTVBuffer = 0;
        int? CalCostNum = 0;
        double term = 0;
        string app1Title = "", app1FirstName = "", app1Surname = "", totalpval = "";
        string app2Title = "", app2FirstName = "", app2Surname = "";
        string ExitStrategy = "", PropertyPreviouslyBridged = "", BridgingPropertyUse = "";
        string BridgingLoanPurpose = "", PaymentMethod = "", PropertyTenure = "", RoofCon = "", TrueCostFeesString = "", FessAddedToLoanString = "", DeductCachBackValString = "", DeductRefundFeesValString = "";
        string AddressPostCode = "";
        string iPeriodString = "", SortColunm = "", SearchMatchingType = "", LoanReason = "";
        bool iPeriod = true, TrueCostFees = true, FessAddedToLoan = true, DeductCachBackVal = true, DeductRefundFeesVal = true;
        int MaxLTVbuffer = 5, MinLTVBuffer = 5, MaxLoanBuffer = 5000, MinLoanBuffer = 5000, MaxTermBuffer = 12, MinTermBuffer = 12, MaxValBuffer = 5000, MinValBuffer = 5000;
        string OccupiedBy = "", LimitedCompany ="", ExitStrat = "";

        DateTime app1DOBdate = new DateTime(), app2DOBdate = new DateTime();

        string siteId = "POSITI", tecCompanyId = "POSITI";


        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }


        SqlCommand myCommand = new SqlCommand(string.Format("Select Amount, ProductTerm, PropertyValue, App1Title, App1FirstName, App1Surname, App1DOB, App1AnnualIncome, App2AnnualIncome, App2Title, App2FirstName, App2Surname, App2DOB, MortgageBalance FROM tblapplications where AppId = {0}", AppID), connection);

        var reader = myCommand.ExecuteReader();

        while (reader.Read())
        {
            decimal.TryParse(reader["Amount"].ToString(), out loanValue);
            double.TryParse(reader["ProductTerm"].ToString(), out term);
            decimal.TryParse(reader["PropertyValue"].ToString(), out propertyValue);
            decimal.TryParse(reader["MortgageBalance"].ToString(), out mortbal);

            //HttpContext.Current.Response.Write("Amount " + loanValue);
            //HttpContext.Current.Response.Write("ProductTerm " + term);
            //HttpContext.Current.Response.Write("PropertyValue " + propertyValue);
            //HttpContext.Current.Response.Write("MortgageBalance " + mortbal);
            

            #region App 1
            app1Title = reader["App1Title"].ToString();
            app1FirstName = reader["App1FirstName"].ToString();
            app1Surname = reader["App1Surname"].ToString();
            app1Surname = reader["App1Surname"].ToString();
            DateTime.TryParse(reader["App1DOB"].ToString(), out app1DOBdate);
            int.TryParse(reader["App1AnnualIncome"].ToString(), out app1IncomeInt);
            #endregion

            #region App 2            
            app2Title = reader["App2Title"].ToString();
            app2FirstName = reader["App2FirstName"].ToString();
            app2Surname = reader["App2Surname"].ToString();
            app2Surname = reader["App2Surname"].ToString();
            DateTime.TryParse(reader["App2DOB"].ToString(), out app2DOBdate);
            int.TryParse(reader["App2AnnualIncome"].ToString(), out app2IncomeInt);
            #endregion

         
        }


        SqlCommand myCommandDatastore = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

        var datastoreReader = myCommandDatastore.ExecuteReader();

        while (datastoreReader.Read())
        {
            ExitStrategy = datastoreReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPrevBridged = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PrevBridged'", AppID), connection);

        var PrevBridgedReader = myCommandPrevBridged.ExecuteReader();

        while (PrevBridgedReader.Read())
        {
            PropertyPreviouslyBridged = PrevBridgedReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyUse = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyUse'", AppID), connection);

        var PropertyUseReader = myCommandPropertyUse.ExecuteReader();

        while (PropertyUseReader.Read())
        {
            BridgingPropertyUse = PropertyUseReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLoanType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingLoanPurpose'", AppID), connection);

        var LoanTypeReader = myCommandLoanType.ExecuteReader();

        while (LoanTypeReader.Read())
        {
            BridgingLoanPurpose = LoanTypeReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandTotalOutstanding = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ValueofAdditionalProperties'", AppID), connection);

        var OutTotalReader = myCommandTotalOutstanding.ExecuteReader();

        while (OutTotalReader.Read())
        {
           
            decimal.TryParse(OutTotalReader["StoredDataValue"].ToString(), out TotalOutstanding);


        }


        SqlCommand myCommandTotalPropval = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AmountOutstandingAdditionalProperties'", AppID), connection);

        var TotalPropValReader = myCommandTotalPropval.ExecuteReader();

        while (TotalPropValReader.Read())
        {

            decimal.TryParse(TotalPropValReader["StoredDataValue"].ToString(), out TotalPropVal);


      
        }

        SqlCommand myCommandPaymentMeth = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPaymentMethod'", AppID), connection);

        var PaymentMethReader = myCommandPaymentMeth.ExecuteReader();

        while (PaymentMethReader.Read())
        {
            PaymentMethod = PaymentMethReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandTenure = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyTenure'", AppID), connection);

        var TenureReader = myCommandTenure.ExecuteReader();

        while (TenureReader.Read())
        {
            PropertyTenure = TenureReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandRoof = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingConstructionRoof'", AppID), connection);

        var RoofReader = myCommandRoof.ExecuteReader();

        while (RoofReader.Read())
        {
            RoofCon = RoofReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandCalCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverMonthsVal'", AppID), connection);

        var CalCost = myCommandCalCost.ExecuteReader();

        while (CalCost.Read())
        {

            //int.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

            NullableInt.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

            if (CalCostNum == 0)
            {
                CalCostNum = Convert.ToInt32(term);

            }
            else
            {
                CalCostNum = CalCostNum;
            }


        }

        SqlCommand myCommandIntitialPeriod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverInitialPeriod'", AppID), connection);

        var IntitialPeriod = myCommandIntitialPeriod.ExecuteReader();

        while (IntitialPeriod.Read())
        {
            iPeriodString = IntitialPeriod["StoredDataValue"].ToString();


            if (iPeriodString == "Yes")
            {

                bool.TryParse(iPeriodString, out iPeriod);
                iPeriod = true;

            }
            else
            {
                bool.TryParse(iPeriodString, out iPeriod);
                iPeriod = false;
            }
        }

        SqlCommand myCommandTrueCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesPayableToTrueCost'", AppID), connection);

        var AddFeesTrueCost = myCommandTrueCost.ExecuteReader();

        while (AddFeesTrueCost.Read())
        {
            TrueCostFeesString = AddFeesTrueCost["StoredDataValue"].ToString();

            if (TrueCostFeesString == "Yes")
            {
                bool.TryParse(TrueCostFeesString, out TrueCostFees);
                TrueCostFees = true;
            }
            else
            {
                bool.TryParse(TrueCostFeesString, out TrueCostFees);
                TrueCostFees = false;
            }
        }

        SqlCommand myCommandFeesAdded = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesToLoanAllowable'", AppID), connection);

        var FessAdded = myCommandFeesAdded.ExecuteReader();

        while (FessAdded.Read())
        {
            FessAddedToLoanString = FessAdded["StoredDataValue"].ToString();

            if (FessAddedToLoanString == "Yes")
            {

                bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                FessAddedToLoan = true;
            }
            else
            {
                bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                FessAddedToLoan = false;
            }
        }

        SqlCommand myCommandCashBack = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductCashback'", AppID), connection);

        var CachBack = myCommandCashBack.ExecuteReader();

        while (CachBack.Read())
        {
            DeductCachBackValString = CachBack["StoredDataValue"].ToString();
            bool.TryParse(DeductCachBackValString, out DeductCachBackVal);
            DeductCachBackVal = false;
            
        }

        SqlCommand myCommandRefundFees = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductRefundedFees'", AppID), connection);

        var RefundFees = myCommandRefundFees.ExecuteReader();

        while (RefundFees.Read())
        {
            DeductRefundFeesValString = RefundFees["StoredDataValue"].ToString();

            if (DeductRefundFeesValString == "Yes")
            {

                bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                DeductRefundFeesVal = true;
            }
            else
            {
                bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                DeductRefundFeesVal = false;
            }
        }

        SqlCommand myCommandBrokerFeeFlatAm = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeFlatAmount'", AppID), connection);

        var brokerFeeFlatAm = myCommandBrokerFeeFlatAm.ExecuteReader();

        while (brokerFeeFlatAm.Read())
        {
            decimal.TryParse(brokerFeeFlatAm["StoredDataValue"].ToString(), out brokerfeeflatAm);
             

        }

        SqlCommand myCommandBrokerFeeFlatPerc = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeOverPercent'", AppID), connection);

        var brokerFeeFlatPerc = myCommandBrokerFeeFlatPerc.ExecuteReader();

        while (brokerFeeFlatPerc.Read())
        {
            decimal.TryParse(brokerFeeFlatPerc["StoredDataValue"].ToString(), out brokerfeeflatPerc);
        }

        SqlCommand myCommandSortCol = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SortColumn'", AppID), connection);

        var SortColReader = myCommandSortCol.ExecuteReader();

        while (SortColReader.Read())
        {
            SortColunm = SortColReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandOccupied = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyOccupied'", AppID), connection);

        var OccupiedReader = myCommandOccupied.ExecuteReader();

        while (OccupiedReader.Read())
        {
            OccupiedBy = OccupiedReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLimitedCo = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'LimitedCompany'", AppID), connection);

        var LimitedCoReader = myCommandLimitedCo.ExecuteReader();

        while (LimitedCoReader.Read())
        {
            LimitedCompany = LimitedCoReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandExitStrat = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

        var ExitStratReader = myCommandExitStrat.ExecuteReader();

        while (ExitStratReader.Read())
        {
            ExitStrat = ExitStratReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLoanReason = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ReasonForMortgage'", AppID), connection);

        var LoanReasonReader = myCommandLoanReason.ExecuteReader();

        while (LoanReasonReader.Read())
        {
            LoanReason = LoanReasonReader["StoredDataValue"].ToString();
        }

       // int? TotalOutstandingVal = !string.IsNullOrEmpty(TotalOutstanding) ? int.Parse(TotalOutstanding) : 0;



        //HttpContext.Current.Response.Write(TotalOutstanding);
        //HttpContext.Current.Response.Write(TotalPropVal);
        // HttpContext.Current.Response.End();
        //HttpContext.Current.Response.Write(brokerfeeflatAm);
        //HttpContext.Current.Response.Write("Calculate Cost Over the Initial Period: " + iPeriod);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Calculate Cost Over A Number of Months: " + CalCostNum);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Add Fees Payable To True Cost Total: " + TrueCostFees);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Add Fees To Loan Where Allowable: " + FessAddedToLoan);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Deduct Refund Fees: " + DeductRefundFeesVal);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Deduct Cash Back: " + DeductCachBackVal);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Broker Fee Flat Amount: " + brokerfeeflatAm);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Broker Fee Flat Percent: " + brokerfeeflatPerc);
        //HttpContext.Current.Response.End();



        SourcingClient client = new SourcingClient();
        const string licenceKey = "840eadc1-b401-48ae-b9c1-456c3095ae11";

 
        SourceInput sourceInput = new SourceInput()
        {
            SiteId = siteId,
            Applicant1 = new Applicant()
            {
                DateOfBirth = app1DOBdate.ToShortDateString(),
                ApplicantType = ApplicantTypeEnum.No_Selection,
                EmployedDetails = new EmployedDetails()
                {
                    BasicAnnualSalaryGross = app1IncomeInt,
                },
                FirstName = app1FirstName,
                LastName = app1Surname,
                Title = app1Title,
                Gender = GetGender(app1Title),
                ExistingMortgage = new ExistingMortgage()
                {
                    OutstandingBalance = (int)mortbal
                }
            },
            CompanyId = tecCompanyId,
            Term = (int)term,
            ExpectedValuation = (int)propertyValue,
            LoanRequired = (int)loanValue,

            MortgageType = MortgageTypeEnum.Bridging_Loan,
            TermUnit = TermUnitEnum.Months,
            ReasonForMortgage = GetLoanReason(LoanReason),
            SearchMatchingType = SearchMatchingTypeEnum.Include_Near_Misses,

            NearMissesDetails = new NearMissesDetails()
            {
                MaximumLTVBuffer = MaxLTVbuffer,
                MinimumLTVBuffer = MinLTVBuffer,
                MaximumLoanBuffer = MaxLoanBuffer,
                MinimumLoanBuffer = MinLoanBuffer,
                MaximumTermBuffer = MaxTermBuffer,
                MinimumTermBuffer = MinTermBuffer,
                MaximumValuationBuffer = MaxValBuffer,
                MinimumValuationBuffer = MinValBuffer

            },


            TrueCostDetails = new TrueCostDetails()
            {

                CalculateOverNoOfMonths = (int?)CalCostNum,
                CalculateOverInitialPeriod = iPeriod,
                AddFeesPayableToTruecostTotal = TrueCostFees,
                AddFeesThatWillBeAddedToLoan = FessAddedToLoan,
                DeductCashback = false,
                DeductRefundedFees = DeductRefundFeesVal,
                PayBrokerFeeUpFront = false,
                PayArrangementFeeUpFront = false

            },

            PropertyDetails = new PropertyDetails()
            {
                PropertyTenure = GetTenure(PropertyTenure),
                PropertyRoof = GetRoof(RoofCon)


            },

            SecuredLoanDetails = new SecuredLoanDetails()
            {

               ExitStrategy = GetExitStrat(ExitStrat),

                BridgingDetails = new BridgingDetails()
                {
                    BridgingPropertyUse = GetPropUse(BridgingPropertyUse),
                    PropertyPreviouslyBridged = GetPrevProp(PropertyPreviouslyBridged),
                    BridgingLoanPurpose = GetLoanPur(BridgingLoanPurpose),
                    BridgingAdditionalPropertiesTotalOutstanding = (int?)TotalPropVal,
                    BridgingAdditionalPropertiesTotalValue = (int?)TotalOutstanding,
                    BridgingPaymentMethod = GetPayM(PaymentMethod),
                    OccupiedByClientOrFamilyMember = GetOccupied(OccupiedBy),
                    LimitedCompany = GetCompany(LimitedCompany),


                },
                  
             },

        

            Filters = new Filters()
            {
                //OverpaymentsAllowed = overPayment,
                SortResultsByColumn = new SortColumn()
                {
                    Column = GetSortColunm(SortColunm)
                }
            },


            FeeOverrideDetails = new FeeOverrideDetails()
            {
                BrokerFeeFlatAmount = brokerfeeflatAm,
                BrokerFeePercent = brokerfeeflatPerc
            }

        };


        if (app2IncomeInt != 0 && !string.IsNullOrEmpty(app2Title))
        {
            sourceInput.Applicant2 = new Applicant()
            {
                EmployedDetails = new EmployedDetails()
                {
                    BasicAnnualSalaryGross = app2IncomeInt
                },
                Title = app2Title,
                FirstName = app2FirstName,
                LastName = app2Surname,
                Gender = GetGender(app2Title),
                DateOfBirth = app2DOBdate.ToShortDateString(),
            };
        }
        var lenderList = client.RunSource(licenceKey, sourceInput);
		

        List<PolicyCRM> table = new List<PolicyCRM>();

        foreach (var productCRM in lenderList.Results)
        {


            //decimal grossLoan = (productCRM.TrueCost - productCRM.TotalInterestPayable);
            decimal apr = productCRM.AprLenders;


            //string TTFee = (productCRM.CHAPSFee == null || productCRM.CHAPSFee == 0) ? "0" : productCRM.CHAPSFee.ToString("#.##");
            //string BrokerFee = (productCRM.BrokerFee == null || productCRM.BrokerFee == 0) ? "0" : productCRM.BrokerFee.ToString("#.##");
            //string LenderFee = (productCRM.ArrangementFee == null || productCRM.ArrangementFee == 0) ? "0" : productCRM.ArrangementFee.ToString("#.##");
            //string ProcFee = (productCRM.ProcFee == null || productCRM.ProcFee == 0) ? "0" : productCRM.ProcFee.ToString("#.##");

            table.Add(new PolicyCRM()
            {

                ProductId = productCRM.ProductCode,
                Lender = string.Format("{0}", productCRM.LenderName),
                AnnualRate = string.Format("{0}", productCRM.StandardVariableRate),
                RateType = productCRM.MortgageClass,
                TotalFees = productCRM.FeesTotal.ToString("C"),
                TrueCost = productCRM.TrueCost.ToString("C"),
                MaxLTV = productCRM.MaxLTVAvailable.ToString(),
                ERC = productCRM.EarlyRepaymentChargeAppliesUntil.ToString(),
                APR = apr.ToString(),
                //GrossLoan = string.Format("{0}", grossLoan.ToString("#.##")),
                TotalRepayable = string.Format("{0}", productCRM.TrueCost),
                MonthlyPayment = string.Format("{0}", productCRM.InitialMonthlyPayment.ToString("#.##")),
                Duration = productCRM.InitialRatePeriod,
                Button = string.Format("\"selectProduct('{0}','{1}','{2}','{3}',{5},'{4}')\"", productCRM.LenderName, productCRM.StandardVariableRate, productCRM.InitialMonthlyPayment.ToString("#.##"), productCRM.BrokerFee.ToString("#.##"), productCRM.ProductName, productCRM.ArrangementFee.ToString("#.##")),
                Term = term.ToString(),
                BrokerFee = productCRM.BrokerFee.ToString(),
                LenderFee = productCRM.ArrangementFee.ToString(),
                ProductName = productCRM.ProductName,
                LenderName = productCRM.LenderName,
                ProductMatchStatus = productCRM.ProductMatchStatus,
                RejectReasons = productCRM.RejectReasons,
                Img = GetImage(productCRM.LenderName),
                NetMaxLoan = productCRM.MaxLoanAvailable.ToString("C"),
				ArrangementFee = string.Format("{0}", productCRM.ArrangementFee),
				BookingFee = string.Format("{0}", productCRM.BookingFee),
				ChapsFee = productCRM.CHAPSFee,
				DeedsFee = string.Format("{0}", productCRM.DeedsReleaseFee),
				HigherLending = string.Format("{0}", productCRM.HigherLendingCharge),
				MortgageDischargeFee = string.Format("{0}", productCRM.MortgageDischargeFee),
				ValFee =  string.Format("{0}", productCRM.ValuationFee),
                ProcFee = string.Format("{0}", productCRM.ProcFee),
                DisbursementFee = string.Format("{0}", productCRM.DisbursementFee)

                //TotalInterest = string.Format("{0}", productCRM.TotalInterestPayable.ToString("#.##"))
            });

        }

       var tableToXML = ToXML(table);
        DateTime dateOfPost = DateTime.Now;
        int companyId = 1430;

        SqlCommand updatecmd = new SqlCommand();
        updatecmd.Connection = connection;
        updatecmd.CommandText = string.Format("INSERT INTO  tblQuotes  ([CompanyID], [QuoteXML], [QuoteDate], [AppID], [QuotedID], [ProductType]) VALUES ('{0}', '{1}', '{2}', '{3}', '1', 'Bridging Packaged')", companyId, tableToXML.Replace("'", ""), dateOfPost.ToString("MM/dd/yyyy HH:mm:ss"), AppID);
        updatecmd.ExecuteNonQuery();

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Write(JsonConvert.SerializeObject(table));
       // HttpContext.Current.Response.Write(JsonConvert.SerializeObject(sourceInput));
        HttpContext.Current.Response.End();


        string json = JsonConvert.SerializeObject(table);

        //throw new Exception(string.Format("Term: {0}, Property Value: {1}, Loan Value: {2}", term, propertyValue, loanValue));

//        Response.Clear();
//        Response.Headers.Add("Content-type", "text/json");
//        Response.Headers.Add("Content-type", "application/json");
//          Response.Write(json);
//
//        XmlDocument myXml = new XmlDocument();
//        XPathNavigator xNav = myXml.CreateNavigator();
//        XmlSerializer y = new XmlSerializer(sourceInput.GetType());
//        using (var xs = xNav.AppendChild())
//        {
//            y.Serialize(xs, sourceInput);
//        }
//        HttpContext.Current.Response.ContentType = "text/xml";
//        HttpContext.Current.Response.Write(myXml.OuterXml);
//        HttpContext.Current.Response.End();


        //HttpContext.Current.Response.Clear();
        //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(table));

    }

    static public string ToXML(List<PolicyCRM> results)
    {
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.NewLineHandling = NewLineHandling.None;
        settings.Indent = false;
        StringWriter StringWriter = new StringWriter();
        XmlWriter writer = XmlWriter.Create(StringWriter, settings);
        XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
        namespaces.Add(string.Empty, string.Empty);
        XmlSerializer MySerializer = new XmlSerializer(results.GetType());

        MySerializer.Serialize(writer, results, namespaces);
        string s = StringWriter.ToString();
        return s;
    }

    private PropertyRoofEnum? GetRoof(string RoofCon)
    {
        switch (RoofCon)
        {
            case "Flat Roof":
                return PropertyRoofEnum.Flat_Roof;
            case "Thatched":
                return PropertyRoofEnum.Thatched;
            case "Slate":
                return PropertyRoofEnum.Slate;
            case "Tile":
                return PropertyRoofEnum.Tile;
            default:
                return null;


        }
    }

    private PropertyTenureEnum? GetTenure(string PropertyTenure)
    {
        switch (PropertyTenure)
        {
            case "Free Hold":
                return PropertyTenureEnum.Freehold;
            case "Flying Freehold":
                return PropertyTenureEnum.Flying_Freehold;
            case "Lease Hold":
                return PropertyTenureEnum.Leasehold;
            case "Common Hold":
                return PropertyTenureEnum.Commonhold;
            case "Feudal":
                return PropertyTenureEnum.Feudal;
            default:
                return null;


        }
    }

    private BridgingPaymentMethodEnum? GetPayM(string BridgingPaymentMethod)
    {
        switch (BridgingPaymentMethod)
        {
            case "Serviced":
                return BridgingPaymentMethodEnum.Serviced;
            case "Rolled Up":
                return BridgingPaymentMethodEnum.RolledUp;
            default:
                return BridgingPaymentMethodEnum.Retained;

        }
    }

    private BridgingPropertyUseEnum? GetPropUse(string BridgingPropertyUse)
    {
        switch (BridgingPropertyUse)
        {
            case "Commercial":
                return BridgingPropertyUseEnum.Commercial_Only;
            case "Mixed Residential And Commercial":
                return BridgingPropertyUseEnum.Mixed_Residential_And_Commercial;
            default:
                return null;
        }
    }

    private BridgingLoanPurposeEnum? GetLoanPur(string BridgingLoanPurpose)
    {
        switch (BridgingLoanPurpose)
        {
            case "First Charge":
                return BridgingLoanPurposeEnum.First_Charge;
            case "Second Charge":
                return BridgingLoanPurposeEnum.Second_Charge;
            default:
                return null;
        }
    }

    private YesNoEnum? GetPrevProp(string PropertyPreviouslyBridged)
    {
        switch (PropertyPreviouslyBridged)
        {
            case "YES":
                return YesNoEnum.Yes;
            case "NO":
                return YesNoEnum.No;
            default:
                return null;
        }
    }


    private GenderEnum? GetGender(string title)
    {
        switch (title)
        {
            case "Mr":
                return GenderEnum.Male;
            default:
                return GenderEnum.Female;
        }
    }

    private YesNoEnum? GetOccupied(string OccupiedBy)
    {
        switch (OccupiedBy)
        {
            case "YES":
                return YesNoEnum.Yes;
            case "NO":
                return YesNoEnum.No;
            default:
                return null;
        }
    }

    private YesNoEnum? GetCompany(string LimitedCompany)
    {
        switch (LimitedCompany)
        {
            case "YES":
                return YesNoEnum.Yes;
            case "NO":
                return YesNoEnum.No;
            default:
                return null;
        }
    }



    private EnumSortColumns GetSortColunm(string SortColunmVal)
    {
        switch (SortColunmVal)
        {
            case "Lender Name":
                return EnumSortColumns.LenderName;
            case "Monthly Rate":
                return EnumSortColumns.InitialRatePeriodMonths;
            case "Monthly Payment":
                return EnumSortColumns.InitialMonthlyPayment;
            case "True Cost":
                return EnumSortColumns.Truecost;
            case "Max LTV":
                return EnumSortColumns.MaxLTVAvailable;
            case "Fees Total":
                return EnumSortColumns.FeesTotal;
            case "Net Max Loan":
                return EnumSortColumns.MaxLoanAvailable;
            default:
                return EnumSortColumns.InitialMonthlyPayment;
        }
    }


    private ExitStrategyEnum? GetExitStrat(string ExitStrategy)
    {
        switch (ExitStrategy)
        {
            case "Refinance (Residential)":
                return ExitStrategyEnum.Refinance_Residential;
            case "Refinance (BTL)":
                return ExitStrategyEnum.Refinance_BTL;
            case "Refinance (Other)":
                return ExitStrategyEnum.Refinance_Other;
            case "Sale":
                return ExitStrategyEnum.Sale;
            case "Sale & Refinance":
                return ExitStrategyEnum.Sale_And_Refinance;
            case "Investments":
                return ExitStrategyEnum.No_Selection;
            case "Other":
                return ExitStrategyEnum.No_Selection;
            default:
                return null;
        }
    }

    private ReasonForMortgageEnum GetLoanReason(string ReasonForMortgage)
    {
        switch (ReasonForMortgage)
        {

            case "Purchase":
                return ReasonForMortgageEnum.Purchase;
            default:
                return ReasonForMortgageEnum.Remortgage;
        }
    }

    //internal string CreatePlansTable(List<Policy> listOfPolicies)
    //{
    //    string display = "<tr><th></th><th>Policy Details</th><th>Annual Rate</th><th>Product Rules</th><th>Product Information</th><th>Fees</th><th>Gross Loan</th><th>Interest</th><th>Total Repayable</th><th>Monthly Payment</th><th>Maximum Potential Commision</th></tr>";

    //    foreach (var plan in listOfPolicies)
    //    {
    //        display += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td></tr>",
    //            plan.PolicyDetails,
    //            plan.AnnualRate,
    //            plan.ProductRules,
    //            plan.ProductInformation,
    //            plan.Fees,
    //            plan.GrossLoan,
    //            plan.Interest,
    //            plan.TotalRepayable,
    //            plan.MonthlyPayment,
    //            plan.MaximumPotentialCommission);
    //    }
    //    //display += "</table>";
    //    return display;
    //}

    //internal string CreatePlansTableCRM(List<PolicyCRM> listOfPolicies)
    //{
    //    string display = "";

    //    foreach (var plan in listOfPolicies)
    //    {
    //        string image = string.Format("<img src=\"{0}\">", GetImage(plan.LenderName));
    //        display += string.Format("<tr><td>{9}</td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td><input type='button' class='btn btn-small btn-secondary' onclick={8} value='select' \\></td></tr>",
    //            plan.Lender,
    //            plan.LoanDetails,
    //            plan.AnnualRate,
    //            plan.FeesBreakdown,
    //            plan.GrossLoan,
    //            plan.Interest,
    //            plan.TotalRepayable,
    //            plan.MonthlyPayment,
    //            plan.Button,
    //            image);
    //    }
    //    return display;
    //}

    internal string GetImage(string lenderName)
    {
        string imgSource = "/img/lenders/";

        switch (lenderName)
        {
            case "Aldermore Mortgages":
                imgSource += "alde.jpg";
                break;
            case "Dragonfly Property Finance":
                imgSource += "drag.jpg";
                break;
            case "Greenfield Capital":
                imgSource += "gree.jpg";
                break;
            case "Lancashire Mortgage Corporation":
                imgSource += "lanc.jpg";
                break;
            case "Precise Mortgages":
                imgSource += "prec.jpg";
                break;
            case "West One Loans":
                imgSource += "west.jpg";
                break;
            case "Harpenden BS":
                imgSource += "harp.jpg";
                break;
            case "Affirmative":
                imgSource += "affi.jpg";
                break;
            case "MT Finance":
                imgSource += "mtf.jpg";
                break;
            case "Shawbrook Bank":
                imgSource += "shaw.jpg";
                break;
            case "Funding 365":
                imgSource += "fund.jpg";
                break;
            case "Omni Capital":
                imgSource += "omni.jpg";
                break;
            case "United Trust Bank":
                imgSource += "unit.jpg";
                break;
            case "Together Mortgages":
                imgSource += "toge.jpg";
                break;
            case "Lendinvest":
                imgSource += "lend.jpg";
                break;
        }
        return imgSource;
    }

    //public class Policy
    //{
    //    public string PolicyDetails { get; set; }
    //    public string AnnualRate { get; set; }
    //    public string ProductRules { get; set; }
    //    public string ProductInformation { get; set; }
    //    public string Fees { get; set; }
    //    public string GrossLoan { get; set; }
    //    public string Interest { get; set; }
    //    public string TotalRepayable { get; set; }
    //    public string MonthlyPayment { get; set; }
    //    public string MaximumPotentialCommission { get; set; }
    //}

    public class PolicyCRM
    {
        public string ProductId { get; set; }
        public string Lender { get; set; }
        public string LoanDetails { get; set; }
        public string AnnualRate { get; set; }
        public string RateType { get; set; }
        public string FeesBreakdown { get; set; }
        public string TotalFees { get; set; }
        public string GrossLoan { get; set; }
        public string Interest { get; set; }
        public string TotalRepayable { get; set; }
        public string MonthlyPayment { get; set; }
        public string ProductCode { get; set; }
        public string Button { get; set; }
        public string Term { get; set; }
        public string ProductName { get; set; }
        public string LenderFee { get; set; }
        public string BrokerFee { get; set; }
        public string LenderName { get; set; }
        public string Img { get; set; }
        public string TotalInterest { get; set; }
        public string ProductMatchStatus { get; set; }
        public string RejectReasons { get; set; }
        public string TrueCost { get; set; }
        public string MaxLTV { get; set; }
        public string ERC { get; set; }
        public string APR { get; set; }
        public string Duration { get; set; }
        public string NetMaxLoan { get; set; }
        public string ArrangementFee { get; set; }
        public string BookingFee { get; set; }
        public int ChapsFee { get; set; }
        public string DeedsFee { get; set; }
        public string HigherLending { get; set; }
        public string MortgageDischargeFee { get; set; }
        public string ValFee { get; set; }
        public string DisbursementFee { get; set; }
        public string ProcFee { get; set; }


    }

    //public class PolicyXML
    //{
    //    public string Lender { get; set; }
    //    public string PolicyName { get; set; }
    //    public string AnnualRate { get; set; }
    //    public string RecomendedBrokerFee { get; set; }
    //    public string LenderFee { get; set; }
    //    public string MinTerm { get; set; }
    //    public string MaxTerm { get; set; }
    //    public string MinNetLoan { get; set; }
    //    public string MaxNetLoan { get; set; }
    //    public string EstimatedValuationFee { get; set; }
    //    public string MinPropertyValue { get; set; }
    //    public string MaxPropertyValue { get; set; }
    //    public string Amount { get; set; }
    //    public string TTFee { get; set; }
    //    public string maxbrokerfeepercent { get; set; }
    //    public string maxbrokerfeeamount { get; set; }
    //    public string commission { get; set; }
    //    public string exclusive { get; set; }
    //    public string ProductTerm { get; set; }
    //    public string MaxLTV { get; set; }
    //    public string MinLTV { get; set; }
    //    public string LTI { get; set; }
    //    public string LenderAdminCap { get; set; }
    //    public string MaxBrokerFee { get; set; }
    //    public string loanratetype { get; set; }
    //    public string overpaymentnote { get; set; }
    //}

    public static class NullableInt
    {
        public static bool TryParse(string text, out int? outValue)
        {
            int parsedValue;
            bool success = int.TryParse(text, out parsedValue);
            outValue = success ? (int?)parsedValue : null;
            return success;
        }
    }

}

