using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using ServiceReference27Tec1;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Net;


public partial class DefaultSecuredLoan : Page
{
    public string AppID = HttpContext.Current.Request["AppID"];
    private string BridgingExp = HttpContext.Current.Request["BridgingExp"];

    protected void Page_Load(object sender, EventArgs e)
    {
        httpPost();
    }


    public void httpPost()
    {

        decimal loanValue = 0, propertyValue = 0, mortbal = 0, brokerfeeflatAm = 0, brokerfeeflatPerc = 0;
        int app1IncomeInt = 0, app2IncomeInt = 0, TotalOutstanding = 0, TotalPropVal = 0, MaximumLTVBuffer = 0;
        int? CalCostNum = 0, BridgingNew = 0;
        double term = 0;
        string app1Title = "", app1FirstName = "", app1Surname = "";
        string app2Title = "", app2FirstName = "", app2Surname = "";
        string ExitStrategy = "", PropertyPreviouslyBridged = "", BridgingPropertyUse = "";
        string BridgingLoanPurpose = "", PaymentMethod = "", PropertyTenure = "", RoofCon = "", TrueCostFeesString = "", FessAddedToLoanString = "", DeductCachBackValString = "", DeductRefundFeesValString = "", PayBrokerFeeUpFrontString = "";
        string AddressPostCode = "";
        string iPeriodString = "", SortColunm = "", SearchMatchingType = "", LoanReason = "", selectquoteTwo = "", MortgageType = "";
        bool iPeriod = true, TrueCostFees = true, FessAddedToLoan = true, DeductCachBackVal = true, DeductRefundFeesVal = true, PayBrokerFeeUpFront = true;
        int MaxLTVbuffer = 5, MinLTVBuffer = 5, MaxLoanBuffer = 5000, MinLoanBuffer = 5000, MaxTermBuffer = 12, MinTermBuffer = 12, MaxValBuffer = 5000, MinValBuffer = 5000;
        string OccupiedBy = "", LimitedCompany ="", ExitStrat = "",mortgagereason = "";
        string PurchaseAddressLine1 = "", PurchaseAddressLine2 = "", PurchaseAddressLine3 = "", PurchaseAddressPostCode = "", PurchaseAddressCounty = "", PropertyType = "", PropertyConstructionType = "", PropertyUse = "", PropertyCouncilPurchase = "", PropertyFlatFloorNumber = "", PropertyRemainingLease = "";
        string securedpaymentmeth = "", consumerbtl = "";

        string App1ArrearsDateRegistered1 = "", App1ArrearsCleared1 = "";
        string App1ArrearsDateRegistered2 = "", App1ArrearsCleared2 = "";
        string App1ArrearsDateRegistered3 = "", App1ArrearsCleared3 = "";
        string App1ArrearsDateRegistered4 = "", App1ArrearsCleared4 = "";
        string App1ArrearsDateRegistered5 = "", App1ArrearsCleared5 = "";

        string App2ArrearsDateRegistered1 = "", App2ArrearsCleared1 = "";
        string App2ArrearsDateRegistered2 = "", App2ArrearsCleared2 = "";
        string App2ArrearsDateRegistered3 = "", App2ArrearsCleared3 = "";
        string App2ArrearsDateRegistered4 = "", App2ArrearsCleared4 = "";
        string App2ArrearsDateRegistered5 = "", App2ArrearsCleared5 = "";

        string App1CountyDateRegistered1 = "", App1CountyAmount1 = "", App1CountyDateSat1 = "";
        string App1CountyDateRegistered2 = "", App1CountyAmount2 = "", App1CountyDateSat2 = "";
        string App1CountyDateRegistered3 = "", App1CountyAmount3 = "", App1CountyDateSat3 = "";
        string App1CountyDateRegistered4 = "", App1CountyAmount4 = "", App1CountyDateSat4 = "";
        string App1CountyDateRegistered5 = "", App1CountyAmount5 = "", App1CountyDateSat5 = "";

        string App2CountyDateRegistered1 = "", App2CountyAmount1 = "", App2CountyDateSat1 = "";
        string App2CountyDateRegistered2 = "", App2CountyAmount2 = "", App2CountyDateSat2 = "";
        string App2CountyDateRegistered3 = "", App2CountyAmount3 = "", App2CountyDateSat3 = "";
        string App2CountyDateRegistered4 = "", App2CountyAmount4 = "", App2CountyDateSat4 = "";
        string App2CountyDateRegistered5 = "", App2CountyAmount5 = "", App2CountyDateSat5 = "";

        string App1DefaultDateRegistered1 = "", App1DefaultAmount1 = "", App1DefaultDateSat1 = "";
        string App1DefaultDateRegistered2 = "", App1DefaultAmount2 = "", App1DefaultDateSat2 = "";
        string App1DefaultDateRegistered3 = "", App1DefaultAmount3 = "", App1DefaultDateSat3 = "";
        string App1DefaultDateRegistered4 = "", App1DefaultAmount4 = "", App1DefaultDateSat4 = "";
        string App1DefaultDateRegistered5 = "", App1DefaultAmount5 = "", App1DefaultDateSat5 = "";

        string App2DefaultDateRegistered1 = "", App2DefaultAmount1 = "", App2DefaultDateSat1 = "";
        string App2DefaultDateRegistered2 = "", App2DefaultAmount2 = "", App2DefaultDateSat2 = "";
        string App2DefaultDateRegistered3 = "", App2DefaultAmount3 = "", App2DefaultDateSat3 = "";
        string App2DefaultDateRegistered4 = "", App2DefaultAmount4 = "", App2DefaultDateSat4 = "";
        string App2DefaultDateRegistered5 = "", App2DefaultAmount5 = "", App2DefaultDateSat5 = "";

        string App1PayDayLoanRegistered1 = "";
        string App1PayDayLoanRegistered2 = "";
        string App1PayDayLoanRegistered3 = "";
        string App1PayDayLoanRegistered4 = "";
        string App1PayDayLoanRegistered5 = "";

        string App2PayDayLoanRegistered1 = "";
        string App2PayDayLoanRegistered2 = "";
        string App2PayDayLoanRegistered3 = "";
        string App2PayDayLoanRegistered4 = "";
        string App2PayDayLoanRegistered5 = "";

        string App1BankruptciesDate = "";
        string App2BankruptciesDate = "";

        string App1IVADateReg1 = "", App1IVASat1 = "", App1IVADateCom1 = "";
        string App1IVADateReg2 = "", App1IVASat2 = "", App1IVADateCom2 = "";
        string App1IVADateReg3 = "", App1IVASat3 = "", App1IVADateCom3 = "";
        string App1IVADateReg4 = "", App1IVASat4 = "", App1IVADateCom4 = "";
        string App1IVADateReg5 = "", App1IVASat5 = "", App1IVADateCom5 = "";

        string App2IVADateReg1 = "", App2IVASat1 = "", App2IVADateCom1 = "";
        string App2IVADateReg2 = "", App2IVASat2 = "", App2IVADateCom2 = "";
        string App2IVADateReg3 = "", App2IVASat3 = "", App2IVADateCom3 = "";
        string App2IVADateReg4 = "", App2IVASat4 = "", App2IVADateCom4 = "";
        string App2IVADateReg5 = "", App2IVASat5 = "", App2IVADateCom5 = "";

        DateTime app1DOBdate = new DateTime(), app2DOBdate = new DateTime();

        string siteId = "POSITI", tecCompanyId = "POSITI";


        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }


        SqlCommand myCommand = new SqlCommand(string.Format("Select Amount, ProductTerm, PropertyValue, App1Title, App1FirstName, App1Surname, App1DOB, App1AnnualIncome, App2AnnualIncome, App2Title, App2FirstName, App2Surname, App2DOB, MortgageBalance, ProductType, ProductPurpose FROM tblapplications where AppId = {0}", AppID), connection);

        var reader = myCommand.ExecuteReader();

        while (reader.Read())
        {
            decimal.TryParse(reader["Amount"].ToString(), out loanValue);
            double.TryParse(reader["ProductTerm"].ToString(), out term);
            decimal.TryParse(reader["PropertyValue"].ToString(), out propertyValue);
            decimal.TryParse(reader["MortgageBalance"].ToString(), out mortbal);

            //HttpContext.Current.Response.Write("Amount " + loanValue);
            //HttpContext.Current.Response.Write("ProductTerm " + term);
            //HttpContext.Current.Response.Write("PropertyValue " + propertyValue);
            //HttpContext.Current.Response.Write("MortgageBalance " + mortbal);
            

            #region App 1
            app1Title = reader["App1Title"].ToString();
            app1FirstName = reader["App1FirstName"].ToString();
            app1Surname = reader["App1Surname"].ToString();
            app1Surname = reader["App1Surname"].ToString();
            DateTime.TryParse(reader["App1DOB"].ToString(), out app1DOBdate);
            int.TryParse(reader["App1AnnualIncome"].ToString(), out app1IncomeInt);
            #endregion

            #region App 2            
            app2Title = reader["App2Title"].ToString();
            app2FirstName = reader["App2FirstName"].ToString();
            app2Surname = reader["App2Surname"].ToString();
            app2Surname = reader["App2Surname"].ToString();
            DateTime.TryParse(reader["App2DOB"].ToString(), out app2DOBdate);
            int.TryParse(reader["App2AnnualIncome"].ToString(), out app2IncomeInt);
            #endregion

            MortgageType = reader["ProductType"].ToString();
			mortgagereason = reader["ProductPurpose"].ToString();


        }


        SqlCommand myCommandDatastore = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

        var datastoreReader = myCommandDatastore.ExecuteReader();

        while (datastoreReader.Read())
        {
            ExitStrategy = datastoreReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPrevBridged = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PrevBridged'", AppID), connection);

        var PrevBridgedReader = myCommandPrevBridged.ExecuteReader();

        while (PrevBridgedReader.Read())
        {
            PropertyPreviouslyBridged = PrevBridgedReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyUse = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyUse'", AppID), connection);

        var PropertyUseReader = myCommandPropertyUse.ExecuteReader();

        while (PropertyUseReader.Read())
        {
            BridgingPropertyUse = PropertyUseReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLoanType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingLoanPurpose'", AppID), connection);

        var LoanTypeReader = myCommandLoanType.ExecuteReader();

        while (LoanTypeReader.Read())
        {
            BridgingLoanPurpose = LoanTypeReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandTotalOutstanding = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ValueofAdditionalProperties'", AppID), connection);

        var OutTotalReader = myCommandTotalOutstanding.ExecuteReader();

        while (OutTotalReader.Read())
        {
            int.TryParse(OutTotalReader["StoredDataValue"].ToString(), out TotalOutstanding);
        }

        SqlCommand myCommandTotalPropval = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'AmountOutstandingAdditionalProperties'", AppID), connection);

        var TotalPropValReader = myCommandTotalPropval.ExecuteReader();

        while (TotalPropValReader.Read())
        {
            int.TryParse(TotalPropValReader["StoredDataValue"].ToString(), out TotalPropVal);
        }

        SqlCommand myCommandPaymentMeth = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPaymentMethod'", AppID), connection);

        var PaymentMethReader = myCommandPaymentMeth.ExecuteReader();

        while (PaymentMethReader.Read())
        {
            PaymentMethod = PaymentMethReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandTenure = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingPropertyTenure'", AppID), connection);

        var TenureReader = myCommandTenure.ExecuteReader();

        while (TenureReader.Read())
        {
            PropertyTenure = TenureReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandRoof = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BridgingConstructionRoof'", AppID), connection);

        var RoofReader = myCommandRoof.ExecuteReader();

        while (RoofReader.Read())
        {
            RoofCon = RoofReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandCalCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverMonthsVal'", AppID), connection);

        var CalCost = myCommandCalCost.ExecuteReader();

        while (CalCost.Read())
        {

            //int.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

            NullableInt.TryParse(CalCost["StoredDataValue"].ToString(), out CalCostNum);

            if (CalCostNum == 0)
            {
                CalCostNum = Convert.ToInt32(term);

            }
            else
            {
                CalCostNum = CalCostNum;
            }


        }

        SqlCommand myCommandIntitialPeriod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'CalculateOverInitialPeriod'", AppID), connection);

        var IntitialPeriod = myCommandIntitialPeriod.ExecuteReader();

        while (IntitialPeriod.Read())
        {
            iPeriodString = IntitialPeriod["StoredDataValue"].ToString();


            if (iPeriodString == "Yes")
            {

                bool.TryParse(iPeriodString, out iPeriod);
                iPeriod = true;
            }
            else
            {
                bool.TryParse(iPeriodString, out iPeriod);
                iPeriod = false;
            }
        }

        SqlCommand myCommandTrueCost = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesPayableToTrueCost'", AppID), connection);

        var AddFeesTrueCost = myCommandTrueCost.ExecuteReader();

        while (AddFeesTrueCost.Read())
        {
            TrueCostFeesString = AddFeesTrueCost["StoredDataValue"].ToString();

            if (TrueCostFeesString == "Yes")
            {
                bool.TryParse(TrueCostFeesString, out TrueCostFees);
                TrueCostFees = true;
            }
            else
            {
                bool.TryParse(TrueCostFeesString, out TrueCostFees);
                TrueCostFees = false;
            }
        }

        SqlCommand myCommandFeesAdded = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'FeesToLoanAllowable'", AppID), connection);

        var FessAdded = myCommandFeesAdded.ExecuteReader();

        while (FessAdded.Read())
        {
            FessAddedToLoanString = FessAdded["StoredDataValue"].ToString();

            if (FessAddedToLoanString == "Yes")
            {

                bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                FessAddedToLoan = true;
            }
            else
            {
                bool.TryParse(FessAddedToLoanString, out FessAddedToLoan);
                FessAddedToLoan = false;
            }
        }

        SqlCommand myCommandPayBroker = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PayBrokerFeeUpFront'", AppID), connection);

        var PayBroker = myCommandPayBroker.ExecuteReader();

        while (PayBroker.Read())
        {
            PayBrokerFeeUpFrontString = PayBroker["StoredDataValue"].ToString();

            if (PayBrokerFeeUpFrontString == "Y")
            {

                bool.TryParse(PayBrokerFeeUpFrontString, out PayBrokerFeeUpFront);
                PayBrokerFeeUpFront = true;
            }
            else
            {
                bool.TryParse(PayBrokerFeeUpFrontString, out PayBrokerFeeUpFront);
                PayBrokerFeeUpFront = false;
            }
        }

        SqlCommand myCommandCashBack = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductCashback'", AppID), connection);

        var CachBack = myCommandCashBack.ExecuteReader();

        while (CachBack.Read())
        {
            DeductCachBackValString = CachBack["StoredDataValue"].ToString();
            bool.TryParse(DeductCachBackValString, out DeductCachBackVal);
            DeductCachBackVal = false;
            
        }

        SqlCommand myCommandRefundFees = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'DeductRefundedFees'", AppID), connection);

        var RefundFees = myCommandRefundFees.ExecuteReader();

        while (RefundFees.Read())
        {
            DeductRefundFeesValString = RefundFees["StoredDataValue"].ToString();

            if (DeductRefundFeesValString == "Yes")
            {

                bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                DeductRefundFeesVal = true;
            }
            else
            {
                bool.TryParse(DeductRefundFeesValString, out DeductRefundFeesVal);
                DeductRefundFeesVal = false;
            }
        }

        SqlCommand myCommandBrokerFeeFlatAm = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeFlatAmount'", AppID), connection);

        var brokerFeeFlatAm = myCommandBrokerFeeFlatAm.ExecuteReader();

        while (brokerFeeFlatAm.Read())
        {
            decimal.TryParse(brokerFeeFlatAm["StoredDataValue"].ToString(), out brokerfeeflatAm);
             

        }

        SqlCommand myCommandBrokerFeeFlatPerc = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'BrokerFeeOverPercent'", AppID), connection);

        var brokerFeeFlatPerc = myCommandBrokerFeeFlatPerc.ExecuteReader();

        while (brokerFeeFlatPerc.Read())
        {
            decimal.TryParse(brokerFeeFlatPerc["StoredDataValue"].ToString(), out brokerfeeflatPerc);
        }

        SqlCommand myCommandSortCol = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SortColumn'", AppID), connection);

        var SortColReader = myCommandSortCol.ExecuteReader();

        while (SortColReader.Read())
        {
            SortColunm = SortColReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandOccupied = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyOccupied'", AppID), connection);

        var OccupiedReader = myCommandOccupied.ExecuteReader();

        while (OccupiedReader.Read())
        {
            OccupiedBy = OccupiedReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLimitedCo = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'LimitedCompany'", AppID), connection);

        var LimitedCoReader = myCommandLimitedCo.ExecuteReader();

        while (LimitedCoReader.Read())
        {
            LimitedCompany = LimitedCoReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandExitStrat = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ExitStrategy'", AppID), connection);

        var ExitStratReader = myCommandExitStrat.ExecuteReader();

        while (ExitStratReader.Read())
        {
            ExitStrat = ExitStratReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandLoanReason = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ReasonForMortgage'", AppID), connection);

        var LoanReasonReader = myCommandLoanReason.ExecuteReader();

        while (LoanReasonReader.Read())
        {
            LoanReason = LoanReasonReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyAddress1 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine1'", AppID), connection);

        var PA1Reader = myCommandPropertyAddress1.ExecuteReader();

        while (PA1Reader.Read())
        {
            PurchaseAddressLine1 = PA1Reader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyAddress2 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine2'", AppID), connection);

        var PA2Reader = myCommandPropertyAddress2.ExecuteReader();

        while (PA2Reader.Read())
        {
            PurchaseAddressLine2 = PA2Reader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyAddress3 = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressLine3'", AppID), connection);

        var PA3Reader = myCommandPropertyAddress3.ExecuteReader();

        while (PA3Reader.Read())
        {
            PurchaseAddressLine3 = PA3Reader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyCounty = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressCounty'", AppID), connection);

        var PACountyReader = myCommandPropertyCounty.ExecuteReader();

        while (PACountyReader.Read())
        {
            PurchaseAddressCounty = PACountyReader["StoredDataValue"].ToString();
        }


        SqlCommand myCommandPropertyPostcode = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PurchaseAddressPostCode'", AppID), connection);

        var PostCodeReader = myCommandPropertyPostcode.ExecuteReader();

        while (PostCodeReader.Read())
        {
            PurchaseAddressPostCode = PostCodeReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyType'", AppID), connection);

        var PropertyTypeReader = myCommandPropertyType.ExecuteReader();

        while (PropertyTypeReader.Read())
        {
            PropertyType = PropertyTypeReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyConstructionType = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyConstructionType'", AppID), connection);

        var PropertyConstructionTypeReader = myCommandPropertyConstructionType.ExecuteReader();

        while (PropertyConstructionTypeReader.Read())
        {
            PropertyConstructionType = PropertyConstructionTypeReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyCouncilPurchase = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyCouncilPurchase'", AppID), connection);

        var PropertyCouncilPurchaseReader = myCommandPropertyCouncilPurchase.ExecuteReader();

        while (PropertyCouncilPurchaseReader.Read())
        {
            PropertyCouncilPurchase = PropertyCouncilPurchaseReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyFlatFloorNumber = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyFlatFloorNumber'", AppID), connection);

        var PropertyFlatFloorNumberReader = myCommandPropertyFlatFloorNumber.ExecuteReader();

        while (PropertyFlatFloorNumberReader.Read())
        {
            PropertyFlatFloorNumber = PropertyFlatFloorNumberReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPropertyRemainingLease = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'PropertyRemainingLease'", AppID), connection);

        var PropertyRemainingLeaseReader = myCommandPropertyRemainingLease.ExecuteReader();

        while (PropertyRemainingLeaseReader.Read())
        {
            PropertyRemainingLease = PropertyRemainingLeaseReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandPaymentMethod = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'SecuredPaymentMethod'", AppID), connection);

        var PaymentMethodReader = myCommandPaymentMethod.ExecuteReader();

        while (PaymentMethodReader.Read())
        {
            securedpaymentmeth = PaymentMethodReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandConsumerBTL = new SqlCommand(string.Format("Select StoredDataValue FROM tbldatastore where AppId = {0} AND StoredDataName = 'ConsumerBuyToLet'", AppID), connection);

        var ConsumerBTLReader = myCommandConsumerBTL.ExecuteReader();

        while (ConsumerBTLReader.Read())
        {
            consumerbtl = ConsumerBTLReader["StoredDataValue"].ToString();
        }

        SqlCommand myCommandApp1Arrears = new SqlCommand(string.Format("Select Top(1) * FROM vw27tecadverse where AppId = {0}", AppID), connection);

        var ArrearsReader = myCommandApp1Arrears.ExecuteReader();

        while (ArrearsReader.Read())
        {
               App1ArrearsDateRegistered1 = ArrearsReader["ArrearsDateReg1"].ToString();
               App1ArrearsCleared1 = ArrearsReader["ArrearsDateSat1"].ToString();
            //   App1ArrearsDateRegistered2 = ArrearsReader["ArrearsDateReg2"].ToString();
            //    App1ArrearsCleared2 = ArrearsReader["ArrearsDateSat2"].ToString();
            //    App1ArrearsDateRegistered3 = ArrearsReader["ArrearsDateReg3"].ToString();
            //    App1ArrearsCleared3 = ArrearsReader["ArrearsDateSat3"].ToString();
            //    App1ArrearsDateRegistered4 = ArrearsReader["ArrearsDateReg4"].ToString();
            //    App1ArrearsCleared4 = ArrearsReader["ArrearsDateSat4"].ToString();
            //    App1ArrearsDateRegistered5 = ArrearsReader["ArrearsDateReg5"].ToString();
            //    App1ArrearsCleared5 = ArrearsReader["ArrearsDateSat5"].ToString();
            //    App2ArrearsDateRegistered1 = ArrearsReader["App2ArrearsDateReg1"].ToString();
            //    App2ArrearsCleared1 = ArrearsReader["App2ArrearsDateSat1"].ToString();
            //    App2ArrearsDateRegistered2 = ArrearsReader["App2ArrearsDateReg2"].ToString();
            //    App2ArrearsCleared2 = ArrearsReader["App2ArrearsDateSat2"].ToString();
            //    App2ArrearsDateRegistered3 = ArrearsReader["App2ArrearsDateReg3"].ToString();
            //    App2ArrearsCleared3 = ArrearsReader["App2ArrearsDateSat3"].ToString();
            //    App2ArrearsDateRegistered4 = ArrearsReader["App2ArrearsDateReg4"].ToString();
            //    App2ArrearsCleared4 = ArrearsReader["App2ArrearsDateSat4"].ToString();
            //    App2ArrearsDateRegistered5 = ArrearsReader["App2ArrearsDateReg5"].ToString();
            //    App2ArrearsCleared5 = ArrearsReader["App2ArrearsDateSat5"].ToString();

               App1CountyDateRegistered1 = ArrearsReader["CountyCourtDateReg1"].ToString();
              App1CountyAmount1 = ArrearsReader["CountyCourtAmount1"].ToString();
               App1CountyDateSat1 = ArrearsReader["CountyCourtDateSat1"].ToString();
            //    App1CountyDateRegistered2 = ArrearsReader["CountyCourtDateReg1"].ToString();
            //    App1CountyAmount2 = ArrearsReader["CountyCourtAmount2"].ToString();
            //    App1CountyDateSat2 = ArrearsReader["CountyCourtDateSat2"].ToString();
            //    App1CountyDateRegistered3 = ArrearsReader["CountyCourtDateReg3"].ToString();
            //    App1CountyAmount3 = ArrearsReader["CountyCourtAmount3"].ToString();
            //    App1CountyDateSat3 = ArrearsReader["CountyCourtDateSat3"].ToString();
            //    App1CountyDateRegistered4 = ArrearsReader["CountyCourtDateReg4"].ToString();
            //    App1CountyAmount4 = ArrearsReader["CountyCourtAmount4"].ToString();
            //    App1CountyDateSat4 = ArrearsReader["CountyCourtDateSat4"].ToString();
            //    App1CountyDateRegistered5 = ArrearsReader["CountyCourtDateReg5"].ToString();
            //    App1CountyAmount5 = ArrearsReader["CountyCourtAmount5"].ToString();
            //    App1CountyDateSat5 = ArrearsReader["CountyCourtDateSat5"].ToString();
            //    App2CountyDateRegistered1 = ArrearsReader["App2CountyCourtDateReg1"].ToString();
            //    App2CountyAmount1 = ArrearsReader["App2CountyCourtAmount1"].ToString();
            //    App2CountyDateSat1 = ArrearsReader["App2CountyCourtDateSat1"].ToString();
            //    App2CountyDateRegistered2 = ArrearsReader["App2CountyCourtDateReg1"].ToString();
            //    App2CountyAmount2 = ArrearsReader["App2CountyCourtAmount2"].ToString();
            //    App2CountyDateSat2 = ArrearsReader["App2CountyCourtDateSat2"].ToString();
            //    App2CountyDateRegistered3 = ArrearsReader["App2CountyCourtDateReg3"].ToString();
            //    App2CountyAmount3 = ArrearsReader["App2CountyCourtAmount3"].ToString();
            //    App2CountyDateSat3 = ArrearsReader["App2CountyCourtDateSat3"].ToString();
            //    App2CountyDateRegistered4 = ArrearsReader["App2CountyCourtDateReg4"].ToString();
            //    App2CountyAmount4 = ArrearsReader["App2CountyCourtAmount4"].ToString();
            //    App2CountyDateSat4 = ArrearsReader["App2CountyCourtDateSat4"].ToString();
            //    App2CountyDateRegistered5 = ArrearsReader["App2CountyCourtDateReg5"].ToString();
            //    App2CountyAmount5 = ArrearsReader["App2CountyCourtAmount5"].ToString();
            //    App2CountyDateSat5 = ArrearsReader["App2CountyCourtDateSat5"].ToString();

              App1DefaultDateRegistered1 = ArrearsReader["DefaultDateReg1"].ToString();
                App1DefaultAmount1 = ArrearsReader["DefaultAmount1"].ToString();
                App1DefaultDateSat1 = ArrearsReader["DefaultDateSat1"].ToString();
            //    App1DefaultDateRegistered2 = ArrearsReader["DefaultDateReg2"].ToString();
            //    App1DefaultAmount2 = ArrearsReader["DefaultAmount2"].ToString();
            //    App1DefaultDateSat2 = ArrearsReader["DefaultDateSat2"].ToString();
            //    App1DefaultDateRegistered3 = ArrearsReader["DefaultDateReg3"].ToString();
            //    App1DefaultAmount3 = ArrearsReader["DefaultAmount3"].ToString();
            //    App1DefaultDateSat3 = ArrearsReader["DefaultDateSat3"].ToString();
            //    App1DefaultDateRegistered4 = ArrearsReader["DefaultDateReg4"].ToString();
            //    App1DefaultAmount4 = ArrearsReader["DefaultAmount4"].ToString();
            //    App1DefaultDateSat4 = ArrearsReader["DefaultDateSat4"].ToString();
            //    App1DefaultDateRegistered5 = ArrearsReader["DefaultDateReg5"].ToString();
            //    App1DefaultAmount5 = ArrearsReader["DefaultAmount5"].ToString();
            //    App1DefaultDateSat5 = ArrearsReader["DefaultDateSat5"].ToString();
            //    App2DefaultDateRegistered1 = ArrearsReader["App2DefaultDateReg1"].ToString();
            //    App2DefaultAmount1 = ArrearsReader["App2DefaultAmount1"].ToString();
            //    App2DefaultDateSat1 = ArrearsReader["App2DefaultDateSat1"].ToString();
            //    App2DefaultDateRegistered2 = ArrearsReader["App2DefaultDateReg2"].ToString();
            //    App2DefaultAmount2 = ArrearsReader["App2DefaultAmount2"].ToString();
            //    App2DefaultDateSat2 = ArrearsReader["App2DefaultDateSat2"].ToString();
            //    App2DefaultDateRegistered3 = ArrearsReader["App2DefaultDateReg3"].ToString();
            //    App2DefaultAmount3 = ArrearsReader["App2DefaultAmount3"].ToString();
            //    App2DefaultDateSat3 = ArrearsReader["App2DefaultDateSat3"].ToString();
            //    App2DefaultDateRegistered4 = ArrearsReader["App2DefaultDateReg4"].ToString();
            //    App2DefaultAmount4 = ArrearsReader["App2DefaultAmount4"].ToString();
            //    App2DefaultDateSat4 = ArrearsReader["App2DefaultDateSat4"].ToString();
            //    App2DefaultDateRegistered5 = ArrearsReader["App2DefaultDateReg5"].ToString();
            //    App2DefaultAmount5 = ArrearsReader["App2DefaultAmount5"].ToString();
            //    App2DefaultDateSat5 = ArrearsReader["App2DefaultDateSat5"].ToString();

            //    App1PayDayLoanRegistered1 = ArrearsReader["PayDayDateReg1"].ToString();
            //    App1PayDayLoanRegistered2 = ArrearsReader["PayDayDateReg2"].ToString();
            //    App1PayDayLoanRegistered3 = ArrearsReader["PayDayDateReg3"].ToString();
            //    App1PayDayLoanRegistered4 = ArrearsReader["PayDayDateReg4"].ToString();
            //    App1PayDayLoanRegistered5 = ArrearsReader["PayDayDateReg5"].ToString();
            //    App2PayDayLoanRegistered1 = ArrearsReader["App2PayDayDateReg1"].ToString();
            //    App2PayDayLoanRegistered2 = ArrearsReader["App2PayDayDateReg2"].ToString();
            //    App2PayDayLoanRegistered3 = ArrearsReader["App2PayDayDateReg3"].ToString();
            //    App2PayDayLoanRegistered4 = ArrearsReader["App2PayDayDateReg4"].ToString();
            //    App2PayDayLoanRegistered5 = ArrearsReader["App2PayDayDateReg5"].ToString();

            App1BankruptciesDate = ArrearsReader["App1BankruptciesDate"].ToString();
            //    App2BankruptciesDate = ArrearsReader["App2BankruptciesDate"].ToString();

            App1IVADateReg1 = ArrearsReader["IVADateReg1"].ToString();
            App1IVASat1 = ArrearsReader["IVASat1"].ToString();
            App1IVADateCom1 = ArrearsReader["IVADateCom1"].ToString();
            App1IVADateReg2 = ArrearsReader["IVADateReg2"].ToString();
            App1IVASat2 = ArrearsReader["IVASat2"].ToString();
            App1IVADateCom2 = ArrearsReader["IVADateCom2"].ToString();
            App1IVADateReg3 = ArrearsReader["IVADateReg3"].ToString();
            App1IVASat3 = ArrearsReader["IVASat3"].ToString();
            App1IVADateCom3 = ArrearsReader["IVADateCom3"].ToString();
            App1IVADateReg4 = ArrearsReader["IVADateReg4"].ToString();
            App1IVASat4 = ArrearsReader["IVASat4"].ToString();
            App1IVADateCom4 = ArrearsReader["IVADateCom4"].ToString();
            App1IVADateReg5 = ArrearsReader["IVADateReg5"].ToString();
            App1IVASat5 = ArrearsReader["IVASat5"].ToString();
            App1IVADateCom5 = ArrearsReader["IVADateCom5"].ToString();
            App2IVADateReg1 = ArrearsReader["App2IVADateReg1"].ToString();
            App2IVASat1 = ArrearsReader["App2IVASat1"].ToString();
            App2IVADateCom1 = ArrearsReader["App2IVADateCom1"].ToString();
            App2IVADateReg2 = ArrearsReader["App2IVADateReg2"].ToString();
            App2IVASat2 = ArrearsReader["App2IVASat2"].ToString();
            App2IVADateCom2 = ArrearsReader["App2IVADateCom2"].ToString();
            App2IVADateReg3 = ArrearsReader["App2IVADateReg3"].ToString();
            App2IVASat3 = ArrearsReader["App2IVASat3"].ToString();
            App2IVADateCom3 = ArrearsReader["App2IVADateCom3"].ToString();
            App2IVADateReg4 = ArrearsReader["App2IVADateReg4"].ToString();
            App2IVASat4 = ArrearsReader["App2IVASat4"].ToString();
            App2IVADateCom4 = ArrearsReader["App2IVADateCom4"].ToString();
            App2IVADateReg5 = ArrearsReader["App2IVADateReg5"].ToString();
            App2IVASat5 = ArrearsReader["App2IVASat5"].ToString();
            App2IVADateCom5 = ArrearsReader["App2IVADateCom5"].ToString();
        }






        int? PropertyFlatFloorNumberVal = !string.IsNullOrEmpty(PropertyFlatFloorNumber) ? int.Parse(PropertyFlatFloorNumber) : 0;
        int? PropertyRemainingLeaseint = !string.IsNullOrEmpty(PropertyRemainingLease) ? int.Parse(PropertyRemainingLease) : 0;


 


        int? App1CountyAmount1New = !string.IsNullOrEmpty(App1CountyAmount1) ? int.Parse(App1CountyAmount1) : 0;
        int? App1CountyAmount2New = !string.IsNullOrEmpty(App1CountyAmount2) ? int.Parse(App1CountyAmount2) : 0;
        int? App1CountyAmount3New = !string.IsNullOrEmpty(App1CountyAmount3) ? int.Parse(App1CountyAmount3) : 0;
        int? App1CountyAmount4New = !string.IsNullOrEmpty(App1CountyAmount4) ? int.Parse(App1CountyAmount4) : 0;
        int? App1CountyAmount5New = !string.IsNullOrEmpty(App1CountyAmount5) ? int.Parse(App1CountyAmount5) : 0;
        int? App2CountyAmount1New = !string.IsNullOrEmpty(App2CountyAmount1) ? int.Parse(App2CountyAmount1) : 0;
        int? App2CountyAmount2New = !string.IsNullOrEmpty(App2CountyAmount2) ? int.Parse(App2CountyAmount2) : 0;
        int? App2CountyAmount3New = !string.IsNullOrEmpty(App2CountyAmount3) ? int.Parse(App2CountyAmount3) : 0;
        int? App2CountyAmount4New = !string.IsNullOrEmpty(App2CountyAmount4) ? int.Parse(App2CountyAmount4) : 0;
        int? App2CountyAmount5New = !string.IsNullOrEmpty(App2CountyAmount5) ? int.Parse(App2CountyAmount5) : 0;

        int? App1DefaultAmount1New = !string.IsNullOrEmpty(App1DefaultAmount1) ? int.Parse(App1DefaultAmount1) : 0;
        int? App1DefaultAmount2New = !string.IsNullOrEmpty(App1DefaultAmount2) ? int.Parse(App1DefaultAmount2) : 0;
        int? App1DefaultAmount3New = !string.IsNullOrEmpty(App1DefaultAmount3) ? int.Parse(App1DefaultAmount3) : 0;
        int? App1DefaultAmount4New = !string.IsNullOrEmpty(App1DefaultAmount4) ? int.Parse(App1DefaultAmount4) : 0;
        int? App1DefaultAmount5New = !string.IsNullOrEmpty(App1DefaultAmount5) ? int.Parse(App1DefaultAmount5) : 0;
        int? App2DefaultAmount1New = !string.IsNullOrEmpty(App2DefaultAmount1) ? int.Parse(App2DefaultAmount1) : 0;
        int? App2DefaultAmount2New = !string.IsNullOrEmpty(App2DefaultAmount2) ? int.Parse(App2DefaultAmount2) : 0;
        int? App2DefaultAmount3New = !string.IsNullOrEmpty(App2DefaultAmount3) ? int.Parse(App2DefaultAmount3) : 0;
        int? App2DefaultAmount4New = !string.IsNullOrEmpty(App2DefaultAmount4) ? int.Parse(App2DefaultAmount4) : 0;
        int? App2DefaultAmount5New = !string.IsNullOrEmpty(App2DefaultAmount5) ? int.Parse(App2DefaultAmount5) : 0;

        bool App1IVASat1Val = true;
        bool App1IVASat2Val = true;
        bool App1IVASat3Val = true;
        bool App1IVASat4Val = true;
        bool App1IVASat5Val = true;
        bool App2IVASat1Val = true;
        bool App2IVASat2Val = true;
        bool App2IVASat3Val = true;
        bool App2IVASat4Val = true;
        bool App2IVASat5Val = true;

        if (App1IVASat1 == "Y")
        {

            bool.TryParse(App1IVASat1, out App1IVASat1Val);
            App1IVASat1Val = true;
        }
        else
        {
            bool.TryParse(App1IVASat1, out App1IVASat1Val);
            App1IVASat1Val = false;
        }

        if (App1IVASat2 == "Y")
        {

            bool.TryParse(App1IVASat2, out App1IVASat2Val);
            App1IVASat2Val = true;
        }
        else
        {
            bool.TryParse(App1IVASat2, out App1IVASat2Val);
            App1IVASat2Val = false;
        }

        if (App1IVASat3 == "Y")
        {

            bool.TryParse(App1IVASat3, out App1IVASat3Val);
            App1IVASat3Val = true;
        }
        else
        {
            bool.TryParse(App1IVASat3, out App1IVASat3Val);
            App1IVASat3Val = false;
        }

        if (App1IVASat4 == "Y")
        {

            bool.TryParse(App1IVASat4, out App1IVASat4Val);
            App1IVASat4Val = true;
        }
        else
        {
            bool.TryParse(App1IVASat4, out App1IVASat3Val);
            App1IVASat3Val = false;
        }

        if (App1IVASat5 == "Y")
        {

            bool.TryParse(App1IVASat5, out App1IVASat5Val);
            App1IVASat5Val = true;
        }
        else
        {
            bool.TryParse(App1IVASat5, out App1IVASat5Val);
            App1IVASat5Val = false;
        }

        if (App2IVASat1 == "Y")
        {

            bool.TryParse(App2IVASat1, out App2IVASat1Val);
            App2IVASat1Val = true;
        }
        else
        {
            bool.TryParse(App2IVASat1, out App2IVASat1Val);
            App2IVASat1Val = false;
        }

        if (App2IVASat2 == "Y")
        {

            bool.TryParse(App2IVASat2, out App2IVASat2Val);
            App2IVASat2Val = true;
        }
        else
        {
            bool.TryParse(App2IVASat2, out App2IVASat2Val);
            App2IVASat2Val = false;
        }

        if (App2IVASat3 == "Y")
        {

            bool.TryParse(App2IVASat3, out App2IVASat3Val);
            App2IVASat3Val = true;
        }
        else
        {
            bool.TryParse(App2IVASat3, out App2IVASat3Val);
            App2IVASat3Val = false;
        }

        if (App2IVASat4 == "Y")
        {

            bool.TryParse(App2IVASat4, out App2IVASat4Val);
            App2IVASat4Val = true;
        }
        else
        {
            bool.TryParse(App2IVASat4, out App2IVASat4Val);
            App2IVASat4Val = false;
        }

        if (App2IVASat5 == "Y")
        {

            bool.TryParse(App2IVASat5, out App2IVASat5Val);
            App2IVASat5Val = true;
        }
        else
        {
            bool.TryParse(App2IVASat5, out App2IVASat5Val);
            App2IVASat5Val = false;
        }

        //HttpContext.Current.Response.Write(brokerfeeflatAm);
        //HttpContext.Current.Response.Write("Calculate Cost Over the Initial Period: " + iPeriod);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Calculate Cost Over A Number of Months: " + CalCostNum);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Add Fees Payable To True Cost Total: " + TrueCostFees);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Add Fees To Loan Where Allowable: " + FessAddedToLoan);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Deduct Refund Fees: " + DeductRefundFeesVal);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Deduct Cash Back: " + DeductCachBackVal);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Broker Fee Flat Amount: " + brokerfeeflatAm);
        //HttpContext.Current.Response.Write("<br>");
        //HttpContext.Current.Response.Write("Broker Fee Flat Percent: " + brokerfeeflatPerc);
        //HttpContext.Current.Response.End();



        SourcingClient client = new SourcingClient();
        const string licenceKey = "840eadc1-b401-48ae-b9c1-456c3095ae11";

        if (BridgingExp == "Yes")
        {
            BridgingNew = 1;
        }
        else
        {
            BridgingNew = null;
        }

        var arrearsapp1 = new List<Arrear>();
        arrearsapp1.Add(new Arrear()
        {
            DateOfArrear = App1ArrearsDateRegistered1,
            DateCleared = App1ArrearsCleared1

        });
        //arrearsapp1.Add(new Arrear()
        //{
        //    DateOfArrear = App1ArrearsDateRegistered2,
        //    DateCleared = App1ArrearsCleared2
        //});
        //arrearsapp1.Add(new Arrear()
        //{
        //    DateOfArrear = App1ArrearsDateRegistered3,
        //    DateCleared = App1ArrearsCleared3
        //});
        //arrearsapp1.Add(new Arrear()
        //{
        //    DateOfArrear = App1ArrearsDateRegistered4,
        //    DateCleared = App1ArrearsCleared4
        //});
        //arrearsapp1.Add(new Arrear()
        //{
        //    DateOfArrear = App1ArrearsDateRegistered5,
        //    DateCleared = App1ArrearsCleared5
        //});
        var arrearsapp2 = new List<Arrear>();
        arrearsapp2.Add(new Arrear()
        {
            DateOfArrear = App2ArrearsDateRegistered1,
            DateCleared = App2ArrearsCleared1
        });
        arrearsapp2.Add(new Arrear()
        {
            DateOfArrear = App2ArrearsDateRegistered2,
            DateCleared = App2ArrearsCleared2
        });
        arrearsapp2.Add(new Arrear()
        {
            DateOfArrear = App2ArrearsDateRegistered3,
            DateCleared = App2ArrearsCleared3
        });
        arrearsapp2.Add(new Arrear()
        {
            DateOfArrear = App2ArrearsDateRegistered4,
            DateCleared = App2ArrearsCleared4
        });
        arrearsapp2.Add(new Arrear()
        {
            DateOfArrear = App2ArrearsDateRegistered5,
            DateCleared = App2ArrearsCleared5
        });

        var ccjapp1 = new List<CCJ>();
        ccjapp1.Add(new CCJ()
        {
            DateRegistered = App1CountyDateRegistered1,
            DateSatisfied = App1CountyDateSat1,
            Amount = App1CountyAmount1New

        });
        //ccjapp1.Add(new CCJ()
        //{
        //    DateRegistered = App1CountyDateRegistered2,
        //    DateSatisfied = App1CountyDateSat2,
        //    Amount = App1CountyAmount2New

        //});
        //ccjapp1.Add(new CCJ()
        //{
        //    DateRegistered = App1CountyDateRegistered3,
        //    DateSatisfied = App1CountyDateSat3,
        //    Amount = App1CountyAmount3New

        //});
        //ccjapp1.Add(new CCJ()
        //{
        //    DateRegistered = App1CountyDateRegistered4,
        //    DateSatisfied = App1CountyDateSat4,
        //    Amount = App1CountyAmount4New

        //});
        //ccjapp1.Add(new CCJ()
        //{
        //    DateRegistered = App1CountyDateRegistered5,
        //    DateSatisfied = App1CountyDateSat5,
        //    Amount = App1CountyAmount5New

        //});
        var ccjapp2 = new List<CCJ>();
        ccjapp2.Add(new CCJ()
        {
            DateRegistered = App2CountyDateRegistered1,
            DateSatisfied = App2CountyDateSat1,
            Amount = App2CountyAmount1New

        });
        ccjapp2.Add(new CCJ()
        {
            DateRegistered = App2CountyDateRegistered2,
            DateSatisfied = App2CountyDateSat2,
            Amount = App2CountyAmount2New

        });
        ccjapp2.Add(new CCJ()
        {
            DateRegistered = App2CountyDateRegistered3,
            DateSatisfied = App2CountyDateSat3,
            Amount = App2CountyAmount3New

        });
        ccjapp2.Add(new CCJ()
        {
            DateRegistered = App2CountyDateRegistered4,
            DateSatisfied = App2CountyDateSat4,
            Amount = App2CountyAmount4New

        });
        ccjapp2.Add(new CCJ()
        {
            DateRegistered = App2CountyDateRegistered5,
            DateSatisfied = App2CountyDateSat5,
            Amount = App2CountyAmount5New

        });

        var paydayapp1 = new List<PaydayLoan>();
        paydayapp1.Add(new PaydayLoan()
        {
            DateOfLoan = App1PayDayLoanRegistered1,

        });
        paydayapp1.Add(new PaydayLoan()
        {
            DateOfLoan = App1PayDayLoanRegistered2,

        });
        paydayapp1.Add(new PaydayLoan()
        {
            DateOfLoan = App1PayDayLoanRegistered3,

        });
        paydayapp1.Add(new PaydayLoan()
        {
            DateOfLoan = App1PayDayLoanRegistered4,

        });
        paydayapp1.Add(new PaydayLoan()
        {
            DateOfLoan = App1PayDayLoanRegistered5,

        });

        var paydayapp2 = new List<PaydayLoan>();
        paydayapp2.Add(new PaydayLoan()
        {
            DateOfLoan = App2PayDayLoanRegistered1,

        });
        paydayapp2.Add(new PaydayLoan()
        {
            DateOfLoan = App2PayDayLoanRegistered2,

        });
        paydayapp2.Add(new PaydayLoan()
        {
            DateOfLoan = App2PayDayLoanRegistered3,

        });
        paydayapp2.Add(new PaydayLoan()
        {
            DateOfLoan = App2PayDayLoanRegistered4,

        });
        paydayapp2.Add(new PaydayLoan()
        {
            DateOfLoan = App2PayDayLoanRegistered5,

        });

        var bankapp1 = new List<Bankruptcy>();
        bankapp1.Add(new Bankruptcy()
        {
            DateDischarged = App1BankruptciesDate,

        });

        var bankapp2 = new List<Bankruptcy>();
        bankapp2.Add(new Bankruptcy()
        {
            DateDischarged = App2BankruptciesDate,

        });

        var ivaapp1 = new List<IVA>();
        ivaapp1.Add(new IVA()
        {
            SatisfactoryConduct = App1IVASat1Val,
            DateRegistered = App1IVADateReg1,
            DateCompleted = App1IVADateCom1

        });






        SourceInput sourceInput = new SourceInput()
        {
            SiteId = siteId,
            Applicant1 = new Applicant()
            {
                DateOfBirth = app1DOBdate.ToShortDateString(),
                ApplicantType = ApplicantTypeEnum.No_Selection,
                EmployedDetails = new EmployedDetails()
                {
                    BasicAnnualSalaryGross = app1IncomeInt,
                },
                FirstName = app1FirstName,
                LastName = app1Surname,
                Title = app1Title,
                Gender = GetGender(app1Title),
                ExistingMortgage = new ExistingMortgage()
                {
                    OutstandingBalance = (int)mortbal
                },
                CreditHistoryDetails = new CreditHistoryDetails()
                {
                    Arrears = arrearsapp1.ToArray(),
                    CCJs = ccjapp1.ToArray(),
                    //PaydayLoans = paydayapp1.ToArray(),
                   Bankruptcies = bankapp1.ToArray(),
                   IVAs = ivaapp1.ToArray()

                }
            },
            CompanyId = tecCompanyId,
            Term = (int)term/12,
            ExpectedValuation = (int)propertyValue,
            LoanRequired = (int)loanValue,

            MortgageType = GetMortgType(MortgageType),
            TermUnit = TermUnitEnum.Years,
            ReasonForMortgage = GetLoanReason(LoanReason),
            SearchMatchingType = SearchMatchingTypeEnum.Include_Near_Misses,
            NumberOfItems = BridgingNew,
            PaymentMethod = GetPaymentMethod(securedpaymentmeth),
           

            NearMissesDetails = new NearMissesDetails()
            {
                MaximumLTVBuffer = MaxLTVbuffer,
                MinimumLTVBuffer = MinLTVBuffer,
                MaximumLoanBuffer = MaxLoanBuffer,
                MinimumLoanBuffer = MinLoanBuffer,
                MaximumTermBuffer = MaxTermBuffer,
                MinimumTermBuffer = MinTermBuffer,
                MaximumValuationBuffer = MaxValBuffer,
                MinimumValuationBuffer = MinValBuffer

            },


            TrueCostDetails = new TrueCostDetails()
            {

                CalculateOverNoOfMonths = (int?)CalCostNum,
                CalculateOverInitialPeriod = iPeriod,
                DeductCashback = false,
                AddFeesPayableToTruecostTotal = FessAddedToLoan,
                PayBrokerFeeUpFront = PayBrokerFeeUpFront,
            },

            PropertyDetails = new PropertyDetails()
            {
                AddressLine1 = PurchaseAddressLine1,
                AddressLine2 = PurchaseAddressLine2,
                Town = PurchaseAddressLine3,
                County = PurchaseAddressCounty,
                PostCode = PurchaseAddressPostCode,
                PropertyType = GetPropertType(PropertyType),
                PropertyTenure = GetTenure(PropertyTenure),
                PropertyWalls = GetPropertWalls(PropertyConstructionType),
                PropertyRoof = GetRoof(RoofCon),
                PropertyUse = GetPropertyUse(PropertyUse),
                ExCouncil = GetExCouncil(PropertyCouncilPurchase),
                FlatDetails = new FlatDetails()
                {

                    NumFloorsInBlock = PropertyFlatFloorNumberVal,
                },
                LeaseholdDetails = new LeaseholdDetails()
                {
                    LeaseRemainingYears = PropertyRemainingLeaseint,
                }


            },

            SecuredLoanDetails = new SecuredLoanDetails()
            {
                

                //ExitStrategy = GetExitStrat(ExitStrat),
                //ChangeOfUseRequired = YesNoEnum.No,
                //ProjectRequiresPlanning = YesNoEnum.No,
                //ProjectPlanningGranted = YesNoEnum.No

                //BridgingDetails = new BridgingDetails()
                //{
                //    BridgingPropertyUse = GetPropUse(BridgingPropertyUse),
                //    PropertyPreviouslyBridged = GetPrevProp(PropertyPreviouslyBridged),
                //    BridgingLoanPurpose = GetLoanPur(BridgingLoanPurpose),
                //    BridgingAdditionalPropertiesTotalOutstanding = TotalOutstanding,
                //    BridgingAdditionalPropertiesTotalValue = TotalPropVal,
                //    BridgingPaymentMethod = GetPayM(PaymentMethod),
                //    OccupiedByClientOrFamilyMember = GetOccupied(OccupiedBy),
                //    LimitedCompany = GetCompany(LimitedCompany),


                //},

            },
			
			 RemortgageDetails = new RemortgageDetails()
			{
				ReasonForRemortgage = getReasonForRemortgage(mortgagereason),
			},



            Filters = new Filters()
            {
                //OverpaymentsAllowed = overPayment,
                SortResultsByColumn = new SortColumn()
                {
                    Column = GetSortColunm(SortColunm)
                }
            },


            FeeOverrideDetails = new FeeOverrideDetails()
            {
                BrokerFeeFlatAmount = brokerfeeflatAm,
                BrokerFeePercent = brokerfeeflatPerc
            },

            BuyToLetDetails = new BuyToLetDetails()
            {
                ConsumerBuyToLet = GetConsumerbtl(consumerbtl)
            }




        };
        

        if (app2IncomeInt != 0 && !string.IsNullOrEmpty(app2Title))
        {
            sourceInput.Applicant2 = new Applicant()
            {
                EmployedDetails = new EmployedDetails()
                {
                    BasicAnnualSalaryGross = app2IncomeInt
                },
                Title = app2Title,
                FirstName = app2FirstName,
                LastName = app2Surname,
                Gender = GetGender(app2Title),
                DateOfBirth = app2DOBdate.ToShortDateString(),
            };
        }
        var lenderList = client.RunSource(licenceKey, sourceInput);
		

        List<PolicyCRM> table = new List<PolicyCRM>();

        foreach (var productCRM in lenderList.Results)
        {


            //decimal grossLoan = (productCRM.TrueCost - productCRM.TotalInterestPayable);
            decimal apr = productCRM.AprLenders;


            //string TTFee = (productCRM.CHAPSFee == null || productCRM.CHAPSFee == 0) ? "0" : productCRM.CHAPSFee.ToString("#.##");
            //string BrokerFee = (productCRM.BrokerFee == null || productCRM.BrokerFee == 0) ? "0" : productCRM.BrokerFee.ToString("#.##");
            //string LenderFee = (productCRM.ArrangementFee == null || productCRM.ArrangementFee == 0) ? "0" : productCRM.ArrangementFee.ToString("#.##");
            //string ProcFee = (productCRM.ProcFee == null || productCRM.ProcFee == 0) ? "0" : productCRM.ProcFee.ToString("#.##");

            table.Add(new PolicyCRM()
            {

                ProductId = productCRM.ProductCode,
                Lender = string.Format("{0}", productCRM.LenderName),
                AnnualRate = string.Format("{0}", productCRM.StandardVariableRate),
                RateType = productCRM.MortgageClass,
                TotalFees = productCRM.FeesTotal.ToString("C"),
                TrueCost = productCRM.TrueCost.ToString("C"),
                MaxLTV = productCRM.MaxLTVAvailable.ToString(),
                ERC = productCRM.EarlyRepaymentChargeAppliesUntil.ToString(),
                APR = apr.ToString(),
                //GrossLoan = string.Format("{0}", grossLoan.ToString("#.##")),
                TotalRepayable = string.Format("{0}", productCRM.TrueCost),
                MonthlyPayment = string.Format("{0}", productCRM.InitialMonthlyPayment.ToString("#.##")),
                Duration = productCRM.InitialRatePeriod,
                Button = string.Format("\"selectProduct('{0}','{1}','{2}','{3}',{5},'{4}')\"", productCRM.LenderName, productCRM.StandardVariableRate, productCRM.InitialMonthlyPayment.ToString("#.##"), productCRM.BrokerFee.ToString("#.##"), productCRM.ProductName, productCRM.ArrangementFee.ToString("#.##")),
                Term = term.ToString(),
                BrokerFee = productCRM.BrokerFee.ToString(),
                LenderFee = productCRM.ArrangementFee.ToString(),
                ProductName = productCRM.ProductName,
                LenderName = productCRM.LenderName,
                ProductMatchStatus = productCRM.ProductMatchStatus,
                RejectReasons = productCRM.RejectReasons,
                Img = GetImage(productCRM.LenderName),
                NetMaxLoan = productCRM.MaxLoanAvailable.ToString("C"),
                ArrangementFee = string.Format("{0}", productCRM.ArrangementFee),
                BookingFee = string.Format("{0}", productCRM.BookingFee),
                ChapsFee = productCRM.CHAPSFee,
                DeedsFee = string.Format("{0}", productCRM.DeedsReleaseFee),
                HigherLending = string.Format("{0}", productCRM.HigherLendingCharge),
                MortgageDischargeFee = string.Format("{0}", productCRM.MortgageDischargeFee),
                ValFee = string.Format("{0}", productCRM.ValuationFee),
				InitialPeriod = string.Format("{0}", productCRM.InitialRatePeriod),
                FixedRate = string.Format("{0}", productCRM.InitialPayRate),
                RevertRate = productCRM.StandardVariableRate,
				MonthlyPaymentRevert = productCRM.MonthlyPaymentAfterInitialPeriod.ToString("C"),
                LenderCode = productCRM.LenderProductReference,
                ProcFee = string.Format("{0}", productCRM.ProcFee),
                DisbursementFee = string.Format("{0}", productCRM.DisbursementFee)
                

                //TotalInterest = string.Format("{0}", productCRM.TotalInterestPayable.ToString("#.##"))


            });
            if (BridgingExp == "Yes")
            {


                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID="+AppID+ " and StoredDataName='UnderwritingChapsFee'", connection))
                {
                   
                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.CHAPSFee + " where StoredDataName='UnderwritingChapsFee' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();
                        

                    }
                    else
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingChapsFee', '" + productCRM.CHAPSFee + "', '" + AppID + "', '1430')");
                        insertChapsFee.ExecuteNonQuery();
                    }
                   
                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingRegularMonthlyPayment'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.InitialMonthlyPayment + " where StoredDataName='UnderwritingRegularMonthlyPayment' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingRegularMonthlyPayment', '" + productCRM.InitialMonthlyPayment + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingLenderFee'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.ArrangementFee + " where StoredDataName='UnderwritingLenderFee' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingLenderFee', '" + productCRM.ArrangementFee + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingBrokerFee'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.BrokerFee + " where StoredDataName='UnderwritingBrokerFee' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingBrokerFee', '" + productCRM.BrokerFee + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingValFee'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.ValuationFee + " where StoredDataName='UnderwritingValFee' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingValFee', '" + productCRM.ValuationFee + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingNetMaxLoan'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.MaxLoanAvailable + " where StoredDataName='UnderwritingNetMaxLoan' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingNetMaxLoan', '" + productCRM.MaxLoanAvailable + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from tbldatastore where AppID=" + AppID + " and StoredDataName='UnderwritingRegularInterestRate'", connection))
                {

                    int dataCount = (int)sqlCommand.ExecuteScalar();
                    if (dataCount > 0)
                    {
                        SqlCommand insertChapsFee = new SqlCommand();
                        insertChapsFee.Connection = connection;
                        insertChapsFee.CommandText = string.Format("update tbldatastore set StoredDataValue=" + productCRM.StandardVariableRate + " where StoredDataName='UnderwritingRegularInterestRate' and AppID=" + AppID + "");
                        insertChapsFee.ExecuteNonQuery();


                    }
                    else
                    {
                        SqlCommand updateMonthlyPayment = new SqlCommand();
                        updateMonthlyPayment.Connection = connection;
                        updateMonthlyPayment.CommandText = string.Format("insert into tbldatastore ([StoredDataName], [StoredDataValue], [AppID], [CompanyID]) VALUES ('UnderwritingRegularInterestRate', '" + productCRM.StandardVariableRate + "', '" + AppID + "', '1430')");
                        updateMonthlyPayment.ExecuteNonQuery();
                    }

                }

            }

        }

      

        var tableToXML = ToXML(table);
        DateTime dateOfPost = DateTime.Now;
        int companyId = 1430;

        SqlCommand updatecmd = new SqlCommand();
        updatecmd.Connection = connection;
        updatecmd.CommandText = string.Format("INSERT INTO  tblQuotes  ([CompanyID], [QuoteXML], [QuoteDate], [AppID], [QuotedID], [ProductType]) VALUES ('{0}', '{1}', '{2}', '{3}', '1', 'Bridging Packaged')", companyId, tableToXML.Replace("'", ""), dateOfPost.ToString("MM/dd/yyyy HH:mm:ss"), AppID);
        updatecmd.ExecuteNonQuery();



        HttpContext.Current.Response.Clear();
      //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(table));
    //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(sourceInput));
       //HttpContext.Current.Response.End();


        string json = JsonConvert.SerializeObject(table);

        //throw new Exception(string.Format("Term: {0}, Property Value: {1}, Loan Value: {2}", term, propertyValue, loanValue));

        Response.Clear();
        Response.Headers.Add("Content-type", "text/json");
        Response.Headers.Add("Content-type", "application/json");
       Response.Write(json);

//        XmlDocument myXml = new XmlDocument();
//        XPathNavigator xNav = myXml.CreateNavigator();
//        XmlSerializer y = new XmlSerializer(sourceInput.GetType());
//        using (var xs = xNav.AppendChild())
//        {
//            y.Serialize(xs, sourceInput);
//        }
//        HttpContext.Current.Response.ContentType = "text/xml";
//        HttpContext.Current.Response.Write(myXml.OuterXml);
//        HttpContext.Current.Response.End();

        //HttpContext.Current.Response.Clear();
        //HttpContext.Current.Response.Write(JsonConvert.SerializeObject(table));

    }




    static public string ToXML(List<PolicyCRM> results)
    {
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.NewLineHandling = NewLineHandling.None;
        settings.Indent = false;
        StringWriter StringWriter = new StringWriter();
        XmlWriter writer = XmlWriter.Create(StringWriter, settings);
        XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
        namespaces.Add(string.Empty, string.Empty);
        XmlSerializer MySerializer = new XmlSerializer(results.GetType());

        MySerializer.Serialize(writer, results, namespaces);
        string s = StringWriter.ToString();
        return s;
    }

    private MortgageTypeEnum GetMortgType(string MortgageType)
    {
        switch (MortgageType)
        {
            case "Secured Loan":
                return MortgageTypeEnum.Secured_Loan;
            case "Secured Loan BTL":
                return MortgageTypeEnum.Secured_Loan_BTL;
            case "Secured Loan BTL Execution Only MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
			case "Secured Loan BTL Advised MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
		    case "Secured Loan BTL Packaged MCD":
                return MortgageTypeEnum.Secured_Loan_BTL;
            default:
                return MortgageTypeEnum.Secured_Loan;

        }
    }

    private PropertyRoofEnum? GetRoof(string RoofCon)
    {
        switch (RoofCon)
        {
            case "Flat Roof":
                return PropertyRoofEnum.Flat_Roof;
            case "Thatched":
                return PropertyRoofEnum.Thatched;
            case "Slate":
                return PropertyRoofEnum.Slate;
            case "Tile":
                return PropertyRoofEnum.Tile;
            default:
                return null;


        }
    }

    private PropertyTenureEnum? GetTenure(string PropertyTenure)
    {
        switch (PropertyTenure)
        {
            case "Free Hold":
                return PropertyTenureEnum.Freehold;
            case "Flying Freehold":
                return PropertyTenureEnum.Flying_Freehold;
            case "Lease Hold":
                return PropertyTenureEnum.Leasehold;
            case "Common Hold":
                return PropertyTenureEnum.Commonhold;
            case "Feudal":
                return PropertyTenureEnum.Feudal;
            default:
                return null;


        }
    }

    private BridgingPaymentMethodEnum? GetPayM(string BridgingPaymentMethod)
    {
        switch (BridgingPaymentMethod)
        {
            case "Serviced":
                return BridgingPaymentMethodEnum.Serviced;
            case "Rolled Up":
                return BridgingPaymentMethodEnum.RolledUp;
            default:
                return BridgingPaymentMethodEnum.Retained;

        }
    }

    private BridgingPropertyUseEnum? GetPropUse(string BridgingPropertyUse)
    {
        switch (BridgingPropertyUse)
        {
            case "Commercial":
                return BridgingPropertyUseEnum.Commercial_Only;
            case "Mixed Residential And Commercial":
                return BridgingPropertyUseEnum.Mixed_Residential_And_Commercial;
            default:
                return BridgingPropertyUseEnum.No_Selection;
        }
    }

    private BridgingLoanPurposeEnum? GetLoanPur(string BridgingLoanPurpose)
    {
        switch (BridgingLoanPurpose)
        {
            case "First Charge":
                return BridgingLoanPurposeEnum.First_Charge;
            case "Second Charge":
                return BridgingLoanPurposeEnum.Second_Charge;
            default:
                return BridgingLoanPurposeEnum.No_Selection;
        }
    }

    private YesNoEnum? GetPrevProp(string PropertyPreviouslyBridged)
    {
        switch (PropertyPreviouslyBridged)
        {
            case "YES":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }


    private GenderEnum? GetGender(string title)
    {
        switch (title)
        {
            case "Mr":
                return GenderEnum.Male;
            case "Mrs":
                return GenderEnum.Female;
            default:
                return null;
        }
    }

    private YesNoEnum? GetOccupied(string OccupiedBy)
    {
        switch (OccupiedBy)
        {
            case "YES":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }

    private YesNoEnum? GetCompany(string LimitedCompany)
    {
        switch (LimitedCompany)
        {
            case "YES":
                return YesNoEnum.Yes;
            default:
                return YesNoEnum.No;
        }
    }



    private EnumSortColumns GetSortColunm(string SortColunmVal)
    {
        switch (SortColunmVal)
        {
            case "Lender Name":
                return EnumSortColumns.LenderName;
            case "Monthly Rate":
                return EnumSortColumns.InitialRatePeriodMonths;
            case "Monthly Payment":
                return EnumSortColumns.InitialMonthlyPayment;
            case "True Cost":
                return EnumSortColumns.Truecost;
            case "Max LTV":
                return EnumSortColumns.MaxLTVAvailable;
            case "Fees Total":
                return EnumSortColumns.FeesTotal;
            case "Net Max Loan":
                return EnumSortColumns.MaxLoanAvailable;
            default:
                return EnumSortColumns.InitialMonthlyPayment;
        }
    }


    private ExitStrategyEnum? GetExitStrat(string ExitStrategy)
    {
        switch (ExitStrategy)
        {
            case "Refinance (Residential)":
                return ExitStrategyEnum.Refinance_Residential;
            case "Refinance (BTL)":
                return ExitStrategyEnum.Refinance_BTL;
            case "Refinance (Other)":
                return ExitStrategyEnum.Refinance_Other;
            case "Sale":
                return ExitStrategyEnum.Sale;
            case "Sale & Refinance":
                return ExitStrategyEnum.Sale_And_Refinance;
            case "Investments":
                return ExitStrategyEnum.No_Selection;
            case "Other":
                return ExitStrategyEnum.No_Selection;
            default:
                return ExitStrategyEnum.No_Selection;
        }
    }

    private ReasonForMortgageEnum GetLoanReason(string ReasonForMortgage)
    {
        switch (ReasonForMortgage)
        {

            case "Remortgage":
                return ReasonForMortgageEnum.Remortgage;
            default:
                return ReasonForMortgageEnum.Remortgage;
        }
    }

    //internal string CreatePlansTable(List<Policy> listOfPolicies)
    //{
    //    string display = "<tr><th></th><th>Policy Details</th><th>Annual Rate</th><th>Product Rules</th><th>Product Information</th><th>Fees</th><th>Gross Loan</th><th>Interest</th><th>Total Repayable</th><th>Monthly Payment</th><th>Maximum Potential Commision</th></tr>";

    //    foreach (var plan in listOfPolicies)
    //    {
    //        display += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td></tr>",
    //            plan.PolicyDetails,
    //            plan.AnnualRate,
    //            plan.ProductRules,
    //            plan.ProductInformation,
    //            plan.Fees,
    //            plan.GrossLoan,
    //            plan.Interest,
    //            plan.TotalRepayable,
    //            plan.MonthlyPayment,
    //            plan.MaximumPotentialCommission);
    //    }
    //    //display += "</table>";
    //    return display;
    //}

    //internal string CreatePlansTableCRM(List<PolicyCRM> listOfPolicies)
    //{
    //    string display = "";

    //    foreach (var plan in listOfPolicies)
    //    {
    //        string image = string.Format("<img src=\"{0}\">", GetImage(plan.LenderName));
    //        display += string.Format("<tr><td>{9}</td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td><input type='button' class='btn btn-small btn-secondary' onclick={8} value='select' \\></td></tr>",
    //            plan.Lender,
    //            plan.LoanDetails,
    //            plan.AnnualRate,
    //            plan.FeesBreakdown,
    //            plan.GrossLoan,
    //            plan.Interest,
    //            plan.TotalRepayable,
    //            plan.MonthlyPayment,
    //            plan.Button,
    //            image);
    //    }
    //    return display;
    //}

    internal string GetImage(string lenderName)
    {
        string imgSource = "/img/lenders/";

        switch (lenderName)
        {
            case "Aldermore Mortgages":
                imgSource += "alde.jpg";
                break;
            case "Dragonfly Property Finance":
                imgSource += "drag.jpg";
                break;
            case "Greenfield Capital":
                imgSource += "gree.jpg";
                break;
            case "Lancashire Mortgage Corporation":
                imgSource += "lanc.jpg";
                break;
            case "Precise Mortgages":
                imgSource += "prec.jpg";
                break;
            case "West One Loans":
                imgSource += "west.jpg";
                break;
            case "Harpenden BS":
                imgSource += "harp.jpg";
                break;
            case "Affirmative":
                imgSource += "affi.jpg";
                break;
            case "MT Finance":
                imgSource += "mtf.jpg";
                break;
            case "Shawbrook Bank":
                imgSource += "shaw.jpg";
                break;
            case "Funding 365":
                imgSource += "fund.jpg";
                break;
            case "Omni Capital":
                imgSource += "omni.jpg";
                break;
            case "United Trust Bank":
                imgSource += "unit.jpg";
                break;
            case "Together Mortgages":
                imgSource += "toge.jpg";
                break;
            case "Lendinvest":
                imgSource += "lend.jpg";
                break;
        }
        return imgSource;
    }
	
    private ReasonForRemortgageEnum? getReasonForRemortgage(string mortgagereason)
    {
        switch (mortgagereason)
        {
            case "Business Purpose":
                return ReasonForRemortgageEnum.Business_Purposes;
            case "Debt Consolidation":
                return ReasonForRemortgageEnum.Debt_Consolidation;
            case "Home Improvements":
                return ReasonForRemortgageEnum.Home_Improvements;
            case "Other Property Purchase":
                return ReasonForRemortgageEnum.Other_Property_Purchase;
            case "School Fees":
                return ReasonForRemortgageEnum.School_Fees;
            case "Tax Bill":
                return ReasonForRemortgageEnum.Tax_Bill;
            default:
                return null;
        }

    }

    //public class Policy
    //{
    //    public string PolicyDetails { get; set; }
    //    public string AnnualRate { get; set; }
    //    public string ProductRules { get; set; }
    //    public string ProductInformation { get; set; }
    //    public string Fees { get; set; }
    //    public string GrossLoan { get; set; }
    //    public string Interest { get; set; }
    //    public string TotalRepayable { get; set; }
    //    public string MonthlyPayment { get; set; }
    //    public string MaximumPotentialCommission { get; set; }
    //}

    public class PolicyCRM
    {
        public string ProductId { get; set; }
        public string Lender { get; set; }
        public string LoanDetails { get; set; }
        public string AnnualRate { get; set; }
        public string RateType { get; set; }
        public string FeesBreakdown { get; set; }
        public string TotalFees { get; set; }
        public string GrossLoan { get; set; }
        public string Interest { get; set; }
        public string TotalRepayable { get; set; }
        public string MonthlyPayment { get; set; }
        public string ProductCode { get; set; }
        public string Button { get; set; }
        public string Term { get; set; }
        public string ProductName { get; set; }
        public string LenderFee { get; set; }
        public string BrokerFee { get; set; }
        public string LenderName { get; set; }
        public string Img { get; set; }
        public string TotalInterest { get; set; }
        public string ProductMatchStatus { get; set; }
        public string RejectReasons { get; set; }
        public string TrueCost { get; set; }
        public string MaxLTV { get; set; }
        public string ERC { get; set; }
        public string APR { get; set; }
        public string Duration { get; set; }
        public string NetMaxLoan { get; set; }
        public string ArrangementFee { get; set; }
        public string BookingFee { get; set; }
        public int ChapsFee { get; set; }
        public string DeedsFee { get; set; }
        public string HigherLending { get; set; }
        public string MortgageDischargeFee { get; set; }
        public string ValFee { get; set; }
		public string InitialPeriod { get; set; }
		public decimal RevertRate { get; set; }
		public string MonthlyPaymentRevert { get; set; }
        public string LenderCode { get; set; }
        public string DisbursementFee { get; set; }
        public string ProcFee { get; set; }
        public string FixedRate { get; set; }
      



    }

    //public class PolicyXML
    //{
    //    public string Lender { get; set; }
    //    public string PolicyName { get; set; }
    //    public string AnnualRate { get; set; }
    //    public string RecomendedBrokerFee { get; set; }
    //    public string LenderFee { get; set; }
    //    public string MinTerm { get; set; }
    //    public string MaxTerm { get; set; }
    //    public string MinNetLoan { get; set; }
    //    public string MaxNetLoan { get; set; }
    //    public string EstimatedValuationFee { get; set; }
    //    public string MinPropertyValue { get; set; }
    //    public string MaxPropertyValue { get; set; }
    //    public string Amount { get; set; }
    //    public string TTFee { get; set; }
    //    public string maxbrokerfeepercent { get; set; }
    //    public string maxbrokerfeeamount { get; set; }
    //    public string commission { get; set; }
    //    public string exclusive { get; set; }
    //    public string ProductTerm { get; set; }
    //    public string MaxLTV { get; set; }
    //    public string MinLTV { get; set; }
    //    public string LTI { get; set; }
    //    public string LenderAdminCap { get; set; }
    //    public string MaxBrokerFee { get; set; }
    //    public string loanratetype { get; set; }
    //    public string overpaymentnote { get; set; }
    //}

    public static class NullableInt
    {
        public static bool TryParse(string text, out int? outValue)
        {
            int parsedValue;
            bool success = int.TryParse(text, out parsedValue);
            outValue = success ? (int?)parsedValue : null;
            return success;

        }
    }
	
    private PropertyWallsEnum? GetPropertWalls(string propwalls)
    {
        switch (propwalls)
        {
            case "Concrete":
                return PropertyWallsEnum.Concrete;
            case "Timber Framed":
                return PropertyWallsEnum.Timber_Framed;
            case "Steel Framed":
                return PropertyWallsEnum.Steel_Framed;
            case "Brick":
                return PropertyWallsEnum.Brick;
            default:
                return null;
        }
    }



    private PropertyUseEnum? GetPropertyUse(string propuse)
    {
        switch (propuse)
        {
            case "PrimaryResidence":
                return PropertyUseEnum.Primary_Residence;
            case "SecondHome":
                return PropertyUseEnum.Second_Home;
            case "Holiday Home":
                return PropertyUseEnum.Holiday_Home;
            case "SittingTenant":
                return PropertyUseEnum.Sitting_Tenant;
            default:
                return null;
        }
    }

    private PropertyTypeEnum? GetPropertType(string proptype)
    {
        switch (proptype)
        {
            case "House":
                return PropertyTypeEnum.House;
            case "Flat":
                return PropertyTypeEnum.Flat;
            case "Maisonette":
                return PropertyTypeEnum.Maisonette;
            case "Bungalow":
                return PropertyTypeEnum.Bungalow;
            default:
                return null;
        }
    }

    private YesNoEnum? GetExCouncil(string excouncil)
    {
        switch (excouncil)
        {
            case "Y":
                return YesNoEnum.Yes;
            case "N":
                return YesNoEnum.No;
            default:
                return null;
        }
    }

    private PaymentMethodEnum GetPaymentMethod(string securedpaymentmethod)
    {
        switch (securedpaymentmethod)
        {
            case "Repayment":
                return PaymentMethodEnum.Repayment;
            case "Endowment":
                return PaymentMethodEnum.Endowment;
            case "Individual Savings Account":
                return PaymentMethodEnum.Individual_Savings_Account;
            case "Interest Only":
                return PaymentMethodEnum.Interest_Only;
            case "Part Investment":
                return PaymentMethodEnum.Part_Investment;
            case "Pension":
                return PaymentMethodEnum.Pension;
            case "Sale Of Property":
                return PaymentMethodEnum.Sale_Of_Property;
            default:
                return PaymentMethodEnum.Repayment;
        }
    }

    private YesNoEnum? GetConsumerbtl(string consumerbtl)
    {
        switch (consumerbtl)
        {
            case "Y":
                return YesNoEnum.Yes;
            case "N":
                return YesNoEnum.No;
            default:
                return null;
        }
    }

}

