using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Collections.Specialized;

public partial class Default : Page
{

	private string strType = HttpContext.Current.Request["Type"];
	private string strBankID = HttpContext.Current.Request["BankID"];
	private string strCompanyName = HttpContext.Current.Request["CompanyName"];
	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strAccessID = HttpContext.Current.Request["AccessID"];
	private string strMogoURL = "";

	//Class object for the Institution json
	class theCredentials
	{
		public int providerId { get; set; }
		public string username { get; set; }
		public string password { get; set; }
	}

	//Class object for the Access json
	class theAccessData {
		public int providerId { get; set; }
		public string username { get; set; }
		public string password { get; set; }
		//public int bank { get; set; }
		public string reference { get; set; }
		public int productType { get; set; }
	}

	class theDataRetrieval
	{
		public int providerId { get; set; }
		public string username { get; set; }
		public string password { get; set; }
		public string accessId { get; set; }
	}

	//Service Model to Store information from the database
	class ServiceModel
	{
		public int provider { get; set; }
		public string username { get; set; }
		public string password { get; set; }
		public string accessID { get; set; }
	}

	class theDataResponse
	{
		public string statement { get; set; }

	}

	class theCRMJsonPost
	{
		public string AppID { get; set; }
		public string PDFData { get; set; }
		public string PDFFileName { get; set; }
		public string ReportDebits { get; set; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{

		ServiceModel model = new ServiceModel();
		int BankID = 0;
		int ProviderId = 0;
		string json = "";
		string CRMURL = "";

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM tblmogointegration where CompanyName = '{0}'", strCompanyName), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{
			model.username = reader["Username"].ToString();
			model.password = reader["Password"].ToString();
			int.TryParse(reader["ProviderId"].ToString(), out ProviderId);
			CRMURL = reader["CRMURL"].ToString();
		}


		
		
		if (strType == "Institution")
		{
			//root node for the first json request - this gets the bank list 
			theCredentials data = new theCredentials();
			data.providerId = ProviderId;
			data.username = model.username;
			data.password = model.password;

			//Convert the data object to Json to use later posting to mogo 
			json = JsonConvert.SerializeObject(data);
			//HttpContext.Current.Response.Write(json + "</br>");
			//HttpContext.Current.Response.End();

			//send the request to Mogo
			strMogoURL = "https://secureuk.mogoplus.com/huat/clovelly/bank/list";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(strMogoURL);


			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strMogoURL, contentBody).Result;

			var responseContent = res.Content;

			//Parse the response
			Response.Write("{\"aaData\":" + responseContent.ReadAsStringAsync().Result + "}");
			HttpContext.Current.Response.End();

			//end client call
			client.Dispose();


		}
		else if (strType == "Access")
		{

			//Parse the Bank ID 
			int.TryParse(strBankID, out BankID);
			//root node for the second json request - this returns the URL for the screen pop
			theAccessData accessdata = new theAccessData();
			accessdata.providerId = ProviderId;
			accessdata.username = model.username;
			accessdata.password = model.password;
			//accessdata.bank = BankID;
			accessdata.reference = strAppID;
			accessdata.productType = 19;

			

			json = JsonConvert.SerializeObject(accessdata);

			//HttpContext.Current.Response.Write(json + "</br>");
			//HttpContext.Current.Response.End();

			//send the request to Mogo
			strMogoURL = "https://secureuk.mogoplus.com/huat/clovelly/application/createApplication";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(strMogoURL);

			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strMogoURL, contentBody).Result;

			var responseContent = res.Content;

			//Parse the response
			HttpContext.Current.Response.Write(responseContent.ReadAsStringAsync().Result);
			HttpContext.Current.Response.End();

			//end client call
			client.Dispose();




		}
		else if (strType == "DataRetrieval")
		{

			//root json for the data retrieval method 
			theDataRetrieval data = new theDataRetrieval();
			data.providerId = ProviderId;
			data.username = model.username;
			data.password = model.password;
			data.accessId = strAccessID;
			data.providerId = ProviderId;

			json = JsonConvert.SerializeObject(data);

			//send the request to Mogo
			strMogoURL = "https://secureuk.mogoplus.com/huat/clovelly/collection/mogoReport";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(strMogoURL);

			//HttpContext.Current.Response.Write(json + "</br>");
			//HttpContext.Current.Response.End();


			Common.postEmail("", "william.worthington@engagedcrm.co.uk", json, "", true, "", true);
			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(strMogoURL, contentBody).Result;

			var responseContent = res.Content;

			//HttpContext.Current.Response.Write(responseContent.ReadAsStringAsync().Result);
			//HttpContext.Current.Response.End();

			//Common.postEmail("", "william.worthington@engagedcrm.co.uk", responseContent.ReadAsStringAsync().Result, "", true, "", true);

			//Parse the response
			var jsonnew = responseContent.ReadAsStringAsync().Result;

			var jo = JObject.Parse(jsonnew);
			Common.postEmail("", "william.worthington@engagedcrm.co.uk", jo["statement"].ToString(), "", true, "", true);

			var statements = System.Convert.FromBase64String(jo["statement"].ToString());
			
			var RefAppID = jo["mogoData"]["detailsList"][0]["reportDetails"]["providerReference"].ToString();

			//HttpContext.Current.Response.Write(RefAppID);
			//HttpContext.Current.Response.End();

			byte[] bytes = statements;

			

			Stream resFilestream = new MemoryStream(bytes);
			FileStream stream = new FileStream(@"C:\MogoTemp\Documents-" + RefAppID + ".zip", FileMode.Create); 

			BinaryWriter bw = new BinaryWriter(stream);
			byte[] ba = new byte[resFilestream.Length];
			resFilestream.Read(ba, 0, ba.Length);
			bw.Write(ba);
			bw.Close();
			resFilestream.Close();

			string extractPath = @"C:\MogoTemp\Extracted\" + RefAppID + "";

			System.IO.Compression.ZipFile.ExtractToDirectory(@"C:\MogoTemp\Documents-" + RefAppID + ".zip", extractPath);

			File.Delete(@"C:\MogoTemp\Documents-" + RefAppID + ".zip");

			string[] array1 = Directory.GetFiles(@"C:\MogoTemp\Extracted\" + RefAppID + "");
			

			foreach (string name in array1)
			{
				byte[] extractedbytes = System.IO.File.ReadAllBytes(name);
				var strContent = System.Convert.ToBase64String(extractedbytes);

				theCRMJsonPost CRMPostdata = new theCRMJsonPost();
				CRMPostdata.AppID = RefAppID;
				CRMPostdata.PDFData = strContent;
				CRMPostdata.PDFFileName = Path.GetFileName(name);
				string url = "" + CRMURL + "/webservices/outbound/MogoDocuments/default.aspx";
				HttpClient crmdataclient = new HttpClient();
				crmdataclient.BaseAddress = new Uri(url);

				json = JsonConvert.SerializeObject(CRMPostdata);
				HttpContent contentBodyCRM = new StringContent(json);
				contentBodyCRM.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

				var rescrm = crmdataclient.PostAsync(url, contentBodyCRM).Result;

				var responseContentCRM = rescrm.Content;

				crmdataclient.Dispose();


			}

			System.IO.DirectoryInfo di = new DirectoryInfo(@"C:\MogoTemp\Extracted\" + RefAppID + "");

			foreach (FileInfo file in di.GetFiles())
			{
				file.Delete();
			}

			Directory.Delete(@"C:\MogoTemp\Extracted\" + RefAppID + "");

			//get and post the Mogo Report 
			var report = System.Convert.FromBase64String(jo["mogoReport"].ToString());
			byte[] bytesreport = report;
			var strReportContent = System.Convert.ToBase64String(bytesreport);
			theCRMJsonPost CRMReportPostdata = new theCRMJsonPost();
			CRMReportPostdata.AppID = RefAppID;
			CRMReportPostdata.PDFData = strReportContent;
			CRMReportPostdata.PDFFileName = "Mogo Report.pdf";
			CRMReportPostdata.ReportDebits = jo["mogoData"]["detailsList"][0]["categorySummary"]["debits"].ToString(); 
	
			string Reporturl = "" + CRMURL + "/webservices/outbound/MogoDocuments/default.aspx";
			HttpClient crmdReportclient = new HttpClient();
			crmdReportclient.BaseAddress = new Uri(Reporturl);

			string Reportjson = JsonConvert.SerializeObject(CRMReportPostdata);
			HttpContent contentBodyReportCRM = new StringContent(Reportjson);

			contentBodyReportCRM.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var resReportcrm = crmdReportclient.PostAsync(Reporturl, contentBodyReportCRM).Result;

			var responseReportContentCRM = resReportcrm.Content;

			HttpContext.Current.Response.Write(responseReportContentCRM.ReadAsStringAsync().Result);

			//end client call
			client.Dispose();
			crmdReportclient.Dispose();
		}

	}


	
}
