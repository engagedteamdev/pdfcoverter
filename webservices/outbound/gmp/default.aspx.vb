﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports com.gmpdataservices.api

Partial Class XMLGMP
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private strUserName As String = "", strCompanyName As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    Private strErrorMessage As String = ""

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        incrementTransferAttempts(AppID, strMediaCampaignID)
        Common.postEmail("", "devteam@engagedcrm.co.uk", "GMP Data Error", strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub generateXml()

        'If (strEnvironment = "live") Then
        '	strXMLURL = "http://api.gmpdataservices.com/"
        'Else
        strXMLURL = "http://api.gmpdataservices.com/"
        'End 

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%GMP%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "GMPUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "GMPCompanyName") Then strCompanyName = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If checkValue(AppID) Then
            sendXML()
        End If
    End Sub

    Private Sub sendXML()
        Dim objService As GMPDataServices = New GMPDataServices
        Dim strTitle As String = "", strSurname As String = "", strForename As String = "", strMobile As String = "", strHomeTel As String = "", strWorkTel As String = "", strContactTel As String = "", strEmail As String = ""
        Dim strAddress1 As String = "", strAddress2 As String = "", strTown As String = "", strCounty As String = "", strPostCode As String = "", strCountry As String = "", strContactBy As String = "", strHowDidYouHear As String = "", strBestTime As String = "", strComments As String = ""
        Dim strSQL As String = "SELECT * FROM vwxmlgmp WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strTitle = Row.Item("title").ToString
                strSurname = Row.Item("surname").ToString
                strForename = Row.Item("forename").ToString
                strMobile = Row.Item("mobile").ToString
                strHomeTel = Row.Item("homeTelephone").ToString
                strWorkTel = Row.Item("workTelephone").ToString
                strContactTel = Row.Item("contactTelephone").ToString
                strEmail = Row.Item("emailAddress").ToString
                strAddress1 = Row.Item("addressLine1").ToString
                strAddress2 = Row.Item("addressLine2").ToString
                strTown = Row.Item("town_or_city").ToString
                strCounty = Row.Item("county_or_region_or_state").ToString
                strPostCode = Row.Item("postcode_or_zipcode").ToString
                strCountry = Row.Item("country").ToString
                strContactBy = Row.Item("contactBy").ToString
                strHowDidYouHear = Row.Item("howDidYouHearAboutUs").ToString
                strBestTime = Row.Item("bestTimeToCall").ToString
                strComments = Row.Item("comments").ToString
            Next
        End If
        dsCache = Nothing
        Dim boolStatus As Boolean = objService.CreateNewLead(strUserName, strMediaCampaignCampaignReference, "", strTitle, strSurname, strForename, strMobile, strHomeTel, strWorkTel, strContactTel, strEmail, strAddress1, strAddress2, strTown, strCounty, strPostCode, strCountry, strContactBy, strHowDidYouHear, strBestTime, strComments)
        Select Case boolStatus
            Case True
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(1 & "|" & encodeURL("Application Accepted by GMP"))
                setTransferState(AppID, strMediaCampaignID, strTransferUserID, "", intHotkeyUserID, strMediaCampaignScheduleID)
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Data Successfully Sent To GMP", "")
            Case Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by GMP"))
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "Data Unsuccessfully Sent To GMP", "")
                incrementTransferAttempts(AppID, strMediaCampaignID)
        End Select
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

End Class
