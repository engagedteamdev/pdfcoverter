﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates

Partial Class XMLExperian
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strXMLURL As String = "", strSOAPAction As String = "", strAccount As String = "", strCertFile As String = "", strProfile As String = ""
    Private strErrorMessage As String = "", intTries As Integer = 0

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        'HttpContext.Current.Response.End()
        If (intTries <= 3) Then
            generateXml()
        End If
    End Sub

    Public Sub generateXml()
        intTries += 1
        If (strEnvironment = "live") Then
            strXMLURL = "https://ukid.uk.experian.com/EIHEndpoint"
        Else
            strXMLURL = "https://ukid.uat.uk.experian.com/EIHEndpoint"
        End If
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%ExperianIDHub%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "ExperianIDHubAccount") Then strAccount = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ExperianIDHubCertificateFile") Then strCertFile = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ExperianIDHubProfile") Then strProfile = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        If checkValue(AppID) Then
            Dim strToken As String = getAuthorisation()
            If (checkValue(strToken)) Then
                sendXML(strToken)
            End If
        End If
    End Sub

    Private Function getAuthorisation() As String
        Dim objCert As X509Certificate
        Dim objStore As X509Store = Nothing
        objStore = New X509Store(StoreName.My, StoreLocation.LocalMachine)
        objStore.Open(OpenFlags.ReadOnly)
        Dim objcerts As X509Certificate2Collection = objStore.Certificates.Find(X509FindType.FindBySubjectName, strCertFile, False)
        objCert = objcerts(0)
        Dim objService As ExperianSTSService.TokenService = New ExperianSTSService.TokenService
        objService.ClientCertificates.Add(objCert)
        Dim strApplication As String = "<WASPAuthenticationRequest><ApplicationName>{0}</ApplicationName><AuthenticationLevel>CertificateAuthentication</AuthenticationLevel><AuthenticationParameters/></WASPAuthenticationRequest>"
        Dim strToken As String = objService.STS(String.Format(strApplication, strAccount))
        objService = Nothing
        Return strToken
    End Function

    Private Sub sendXML(ByVal token As String)
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/experian/bwv.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
        objNSM.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd")
        objNSM.AddNamespace("header", "http://schema.uk.experian.com/eih/2011/03/EIHHeader")
        objNSM.AddNamespace("bws", "http://schema.uk.experian.com/eih/2011/03")
        objNSM.AddNamespace("bban", "http://schema.uk.experian.com/eih/2011/03/BankWizard")

        Dim strSortCode As String = "", strAccountNumber As String = ""
        Dim strSQL As String = "SELECT BankSortCode, BankAccountNumber FROM vwschedulepayments WHERE AppID = '" & AppID & "' AND CompanyID ='" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strSortCode = Row.Item("BankSortCode").ToString
                strAccountNumber = Row.Item("BankAccountNumber").ToString
            Next
        End If
        dsCache = Nothing

        writeToNode(objInputXMLDoc, "y", "wsu:Timestamp", "wsu:Created", ddmmyyhhmmss2utcz(Config.DefaultDateTime, False), objNSM)
        writeToNode(objInputXMLDoc, "y", "wsu:Timestamp", "wsu:Expires", ddmmyyhhmmss2utcz(Config.DefaultDateTime.AddHours(2), False), objNSM)
        writeToNode(objInputXMLDoc, "y", "wsse:Security", "wsse:BinarySecurityToken", token, objNSM)
        writeToNode(objInputXMLDoc, "y", "header:EIHHeader", "header:ClientUser", strAccount, objNSM)
        writeToNode(objInputXMLDoc, "y", "header:EIHHeader", "header:ReferenceId", AppID, objNSM)
        writeToNode(objInputXMLDoc, "y", "bws:BWValidateRequest", "bban:BBAN[@index='1']", strSortCode, objNSM)
        writeToNode(objInputXMLDoc, "y", "bws:BWValidateRequest", "bban:BBAN[@index='2']", strAccountNumber, objNSM)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("Bank Check Unsuccessfully Retrieved", -5, AppID, "<BankCheck><Status>-5</Status><Reason>Mandatory field check:" & strErrorMessage & "</Reason></BankCheck>", "")
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postExperianWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            If (checkResponse(objResponse, "")) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM2.AddNamespace("eih", "http://schema.uk.experian.com/eih/2011/03")
                objNSM2.AddNamespace("bws", "http://schema.uk.experian.com/eih/2011/03/BankWizard")

                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//eih:BWValidateResponse", objNSM2)
                Dim objKey As XmlElement = objApplication.SelectSingleNode("bws:dataAccessKey", objNSM2)
                Dim objMessage As XmlElement = objApplication.SelectSingleNode("//bws:condition", objNSM2)
                Dim strMessage As String = ""
                If (Not objMessage Is Nothing) Then
                    strMessage = objMessage.InnerText
                End If
                If (Not objKey Is Nothing) Then
                    Dim strDataKey As String = objKey.InnerText
                    saveNote(AppID, Config.DefaultUserID, "Bank check successfully performed: " & strMessage)
                    getBankDetails(token, strDataKey)
                Else
                    Dim strResults As String = "<BankCheck>" & _
                      "<Status>0</Status>" & _
                      "<Reason>" & strMessage & "</Reason>" & _
                      "</BankCheck>"
                    saveNote(AppID, Config.DefaultUserID, "Bank check unsuccessfully performed: " & strMessage)
                    SOAPSuccessfulMessage("Bank Check Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)
                End If
            Else
                SOAPUnsuccessfulMessage("Bank Check Unsuccessfully Retrieved", -5, AppID, "<BankCheck><Status>0</Status><Reason>Data error</Reason></BankCheck>", "")
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        End If
    End Sub

    Private Sub getBankDetails(ByVal token As String, ByVal dataKey As String)
        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/experian/gbd.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
        objNSM.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd")
        objNSM.AddNamespace("header", "http://schema.uk.experian.com/eih/2011/03/EIHHeader")
        objNSM.AddNamespace("bws", "http://schema.uk.experian.com/eih/2011/03")
        objNSM.AddNamespace("bban", "http://schema.uk.experian.com/eih/2011/03/BankWizard")


        writeToNode(objInputXMLDoc, "y", "wsu:Timestamp", "wsu:Created", ddmmyyhhmmss2utcz(Config.DefaultDateTime, False), objNSM)
        writeToNode(objInputXMLDoc, "y", "wsu:Timestamp", "wsu:Expires", ddmmyyhhmmss2utcz(Config.DefaultDateTime.AddHours(2), False), objNSM)
        writeToNode(objInputXMLDoc, "y", "wsse:Security", "wsse:BinarySecurityToken", token, objNSM)
        writeToNode(objInputXMLDoc, "y", "header:EIHHeader", "header:ClientUser", strAccount, objNSM)
        writeToNode(objInputXMLDoc, "y", "header:EIHHeader", "header:ReferenceId", AppID, objNSM)
        writeToNode(objInputXMLDoc, "y", "bws:BWGetBranchDataRequest", "bban:dataAccessKey", dataKey, objNSM)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("Bank Check Unsuccessfully Retrieved", -5, AppID, "<BankCheck><Status>-5</Status><Reason>Mandatory field check:" & strErrorMessage & "</Reason></BankCheck>", "")
        Else
            ' Post the SOAP message.
            Dim objResponse As HttpWebResponse = postExperianWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            If (objResponse.StatusCode = Net.HttpStatusCode.OK) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Bank Check Successfully Retrieved", objOutputXMLDoc.InnerXml)
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM2.AddNamespace("eih", "http://schema.uk.experian.com/eih/2011/03")
                objNSM2.AddNamespace("bws", "http://schema.uk.experian.com/eih/2011/03/BankWizard")

                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//eih:BWGetBranchDataResponse", objNSM2)
                Dim strBankName As String = objApplication.SelectSingleNode("//bws:institutionName", objNSM2).InnerText
                SOAPSuccessfulMessage("Bank Check Successfully Retrieved", 1, AppID, "<BankCheck><Status>1</Status><BankName>" & strBankName & "</BankName></BankCheck>", "")
            Else
                SOAPUnsuccessfulMessage("Bank Check Unsuccessfully Retrieved", -5, AppID, "<BankCheck><Status>0</Status><Reason>Data error</Reason></BankCheck>", "")
            End If
			If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing
    End Sub

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Function createElement(ByVal doc As XmlDocument, ByVal parent As String, ByVal name As String, ByVal fld As String, nsm As XmlNamespaceManager, Optional ns As String = "def") As XmlElement
        Dim objTest As XmlNode = doc.SelectSingleNode("//" & parent & "/" & name, nsm)
        If (objTest Is Nothing) Then
            Dim objApplication As XmlNode = doc.SelectSingleNode("//" & parent, nsm)
            Dim objNewNode As XmlElement = doc.CreateElement(fld, nsm.LookupNamespace(ns))
            objApplication.AppendChild(objNewNode)
            Return objNewNode
        Else
            Return Nothing
        End If
    End Function

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Function postExperianWebRequest(ByVal url As String, ByVal post As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            Dim objCert As X509Certificate
            Dim objStore As X509Store = Nothing
            objStore = New X509Store(StoreName.My, StoreLocation.LocalMachine)
            objStore.Open(OpenFlags.ReadOnly)
            Dim objcerts As X509Certificate2Collection = objStore.Certificates.Find(X509FindType.FindBySubjectName, "LFBLLifeboatU02U", False)
            objCert = objcerts(0)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .ClientCertificates.Add(objCert)
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'For Each item As X509Certificate In objRequest.ClientCertificates
            '    responseWrite("Name:" & item.GetName & "<br>")
            '    responseWrite("Public Key:" & item.GetPublicKeyString & "<br>")
            '    responseWrite("Data:" & item.GetRawCertDataString & "<br>")
            '    responseWrite("Issuer:" & item.GetIssuerName & "<br>")
            '    responseWrite("Hash:" & item.Subject & "<br>")
            '    responseWrite("Serial:" & item.GetSerialNumberString & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
