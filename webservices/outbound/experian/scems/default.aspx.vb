﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates

Partial Class XMLExperian
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager, objNSM2 As XmlNamespaceManager
    Private strXMLURL As String = "", strSOAPAction As String = "", strUserName As String = "", strPassword As String = "", strAccount As String = "", strCertFile As String = ""
    Private strErrorMessage As String = "", intTries As Integer = 0

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        'HttpContext.Current.Response.End()
        If (intTries <= 3) Then
            generateXml()
        End If
    End Sub

    Public Sub generateXml()
		Server.ScriptTimeout = 120
        intTries += 1
        If (strEnvironment = "live") Then
            strXMLURL = "https://scems.uk.experian.com/experian/wbsv/v100/interactive/interactive.asmx"
            strSOAPAction = "http://www.uk.experian.com/experian/wbsv/peinteractive/interactive"
        Else
            strXMLURL = "https://scems.uat.uk.experian.com/experian/wbsv/v100/interactive.asmx"
            strSOAPAction = "http://www.uk.experian.com/experian/wbsv/peinteractive/interactive"
        End If
        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%ExperianSCEMS%' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "ExperianSCEMSUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ExperianSCEMSPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ExperianSCEMSAccount") Then strAccount = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ExperianSCEMSCertificateFile") Then strCertFile = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        If checkValue(AppID) Then
            Dim strStatusCode As String = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
            Dim strSubStatusCode As String = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
            Dim strDPA1 As String = getAnyFieldFromDataStore("DPA1", AppID)
            If (strDPA1 = "Y" And ((strStatusCode = "TFR") Or (strStatusCode = "IDP"))) Then ' To perform a credit check, case must have DPA1 consent and be manually at sales or pass Iovation checking
                checkPreviousCreditHistory()
                Dim strToken As String = getAuthorisation()
                If (checkValue(strToken)) Then
                    sendXML(strToken)
                End If
            Else
                SOAPUnsuccessfulMessage("SCEMS Credit Check Unsuccessfully Retrieved", -5, AppID, "<CreditCheck><Status>-5</Status><Reason>Credit check cannot be performed</Reason></CreditCheck>", "")
            End If
        End If
    End Sub

    Private Function getCertificate() As X509Certificate
        Dim objCert As X509Certificate
        Dim objStore As X509Store = Nothing
        objStore = New X509Store(StoreName.My, StoreLocation.LocalMachine)
        objStore.Open(OpenFlags.ReadOnly)
        Dim objCerts As X509Certificate2Collection = objStore.Certificates.Find(X509FindType.FindBySubjectName, strCertFile, False)
        objCert = objCerts(0)
        'ResponseWrite(objCert.GetName)
        'responseEnd()
        Return objCert
    End Function

    Private Function getAuthorisation() As String
        Dim objCert As X509Certificate = getCertificate()
        Dim objService As ExperianTokenService.TokenService = New ExperianTokenService.TokenService
        objService.ClientCertificates.Add(objCert)
        Dim strToken As String = objService.LoginWithCertificate(strAccount, True)
        objService = Nothing
        Return strToken
    End Function

    Private Sub sendXML(ByVal token As String)

        objInputXMLDoc.XmlResolver = Nothing
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/experian/scems.xml"))
        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
        objNSM.AddNamespace("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
        writeToNode(objInputXMLDoc, "y", "wsse:Security", "wsse:BinarySecurityToken", Convert.ToBase64String(Encoding.UTF8.GetBytes(token)), objNSM)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        Dim strSQL As String = "SELECT * FROM vwxmlscems WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then

                        If (Column.ColumnName.ToString = "App1HomeTelephone") Then
                            Dim arrTelephone As Array = splitTelephone(Row(Column).ToString)
                            writeToNode(objInputXMLDoc, "n", "ApplicationData/Personal/HomeTelephone", "STDCode", arrTelephone(0))
                            writeToNode(objInputXMLDoc, "n", "ApplicationData/Personal/HomeTelephone", "LocalNumber", arrTelephone(1))
                        ElseIf (Column.ColumnName.ToString = "App1WorkTelephone") Then
                            Dim arrTelephone As Array = splitTelephone(Row(Column).ToString)
                            writeToNode(objInputXMLDoc, "n", "ApplicationData/Employment/WorkTelephone", "STDCode", arrTelephone(0))
                            writeToNode(objInputXMLDoc, "n", "ApplicationData/Employment/WorkTelephone", "LocalNumber", arrTelephone(1))
                        Else
                            Dim arrName As Array = Split(Column.ColumnName.ToString, "/")
                            Dim strParent As String = ""
                            If (UBound(arrName) > 0) Then
                                For y As Integer = 0 To UBound(arrName) - 1
                                    If (y = 0) Then
                                        strParent += arrName(y)
                                    Else
                                        strParent += "/" & arrName(y)
                                    End If
                                Next
                            Else
                                strParent = arrName(0)
                            End If
                            writeToNode(objInputXMLDoc, "n", Replace(Replace(Replace(strParent, "(", "["), ")", "]"), "->", "/"), arrName(UBound(arrName)), Row(Column).ToString)
                        End If

                    End If
                Next
            Next
        End If
        dsCache = Nothing
		
        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()
		
        If (strErrorMessage <> "") Then
            SOAPUnsuccessfulMessage("SCEMS Credit Check Unsuccessfully Retrieved", -5, AppID, "<CreditCheck><Status>-5</Status><Reason>Mandatory field check:" & strErrorMessage & "</Reason></CreditCheck>", "")
        Else
            ' Post the SOAP message.
			saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "SCEMS Credit Check Generated", objInputXMLDoc.InnerXml)
            Dim objResponse As HttpWebResponse = postExperianWebRequest(strXMLURL, objInputXMLDoc.InnerXml, strSOAPAction)
            If (checkResponse(objResponse, "")) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
                objReader.Close()
                objReader = Nothing

                'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
                'HttpContext.Current.Response.End()

                Dim objNSM2 As XmlNamespaceManager = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM2.AddNamespace("def", "http://schema.uk.experian.com/experian/cems/msgs/v1.7/ConsumerData")

                Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//def:Output", objNSM2)
                Dim objError As XmlNode = objApplication.SelectSingleNode("//Error", objNSM2)
                If (objError Is Nothing) Then

                    Dim objScore As XmlNode = objApplication.SelectSingleNode("//Scoring/E5S051", objNSM2)
                    If (Not objScore Is Nothing) Then

                        Dim strCreditScore As String = Trim(objApplication.SelectSingleNode("//Scoring/E5S051").InnerText)
                        executeNonQuery("UPDATE tblapplications SET ApplicationCreditScore = " & formatField(strCreditScore, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intPaymentsMissed As Integer = 0
                        intPaymentsMissed += getNodeValueAsNumber(objApplication, "PDD15")
                        executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(intPaymentsMissed, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intCreditPaymentsMissed As Integer = 0
                        intCreditPaymentsMissed += getNodeValueAsNumber(objApplication, "E1A04,E1B12,HCA05")
                        executeNonQuery("UPDATE tblapplications SET NonMortgagePaymentsMissed12 = " & formatField(intCreditPaymentsMissed, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intDefaults As Integer = 0
                        intDefaults += getNodeValueAsNumber(objApplication, "PDB08,SPNONPDL12,SPNONPD,SPNOEBADSL12,HCA05,HCC01,HCC03")
                        executeNonQuery("UPDATE tblapplications SET DefaultTotal = " & formatField(intDefaults, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intCCJs As Integer = 0
                        intCCJs += getNodeValueAsNumber(objApplication, "E2G01,EA2J01")
                        executeNonQuery("UPDATE tblapplications SET CCJTotal = " & formatField(intCCJs, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intCreditors As Integer = 0
                        intCreditors += getNodeValueAsNumber(objApplication, "PDB10")
                        executeNonQuery("UPDATE tblapplications SET UnsecuredCreditorTotal = " & formatField(intCreditors, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intSearches As Integer = 0
                        intSearches += getNodeValueAsNumber(objApplication, "PDE17")
                        executeNonQuery("UPDATE tblapplications SET ApplicationCreditSearches = " & formatField(intSearches, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim strBankruptcy As String = getNodeText(objApplication, objNSM2, "//EA1C01")
                        If (strBankruptcy = "T") Then strBankruptcy = ""
                        executeNonQuery("UPDATE tblapplications SET DeclaredBankrupt = " & formatField(strBankruptcy, "U", "N") & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        Dim intWorstStatusPDL As Integer
                        intWorstStatusPDL = getNodeValueAsNumber(objApplication, "PDA04") ' Worst Current Status on all PDL Accounts (Active/Defaulted)
                        If (intWorstStatusPDL > 7) Then
                            intWorstStatusPDL += intPaymentsMissed
                            executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(intWorstStatusPDL, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                        End If

                        Dim intTotalCreditors As Integer = 0
                        intTotalCreditors += getNodeValueAsNumber(objApplication, "E1B01") ' Number of CAIS Accounts (3 months)
                        updateDataStoreField(AppID, "TotalCAISAccounts", intTotalCreditors, "N", 0)

                        Dim intMortgageBalance As Integer = 0
                        intMortgageBalance = getNodeValueAsNumber(objApplication, "E1B11") ' Mortgage Balance
                        executeNonQuery("UPDATE tblapplications SET MortgageBalance = " & formatField(intMortgageBalance * 1000, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")

                        'Dim intWorstStatus As Integer
                        'intWorstStatus = getNodeValueAsNumber(objApplication, "PDC11")
                        'If (intWorstStatus > 7) Then
                        '    intWorstStatus += intPaymentsMissed
                        '    executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(intWorstStatus, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                        'End If

                        'Dim intWorstStatus3m As Integer
                        'intWorstStatus3m = getNodeValueAsNumber(objApplication, "PDC12")
                        'If (intWorstStatus3m > 7) Then
                        '    intWorstStatus3m += intPaymentsMissed
                        '    executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(intWorstStatus3m, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                        'End If

                        'Dim intWorstStatus6m As Integer
                        'intWorstStatus6m = getNodeValueAsNumber(objApplication, "PDC13")
                        'If (intWorstStatus6m > 7) Then
                        '    intWorstStatus6m += intPaymentsMissed
                        '    executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(intWorstStatus6m, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                        'End If

                        Dim strResults As String = "<CreditCheck>" & _
                          "<Status>1</Status>" & _
                          "<CreditScore>" & strCreditScore & "</CreditScore>" & _
                          "</CreditCheck>"

                        ' Credit Score
                        saveCreditHistory(objApplication, "E5S051", "Opt-In Credit Score", "Score")
                        ' Address History & Electoral Roll
                        saveCreditHistory(objApplication, "E4Q01", "1st Address Reference for SS", "Address History")
                        saveCreditHistory(objApplication, "E4Q02", "1st Address Reference for SP", "Address History")
                        saveCreditHistory(objApplication, "E4Q03", "1st Address Years on ER for SS", "Address History")
                        saveCreditHistory(objApplication, "E4Q04", "1st Address Years on ER for SP", "Address History")
                        saveCreditHistory(objApplication, "E4Q05", "2nd Address Reference for SS", "Address History")
                        saveCreditHistory(objApplication, "E4Q06", "2nd Address Reference for SP", "Address History")
                        saveCreditHistory(objApplication, "E4Q07", "2nd Address Years on ER for SS", "Address History")
                        saveCreditHistory(objApplication, "E4Q08", "2nd Address Years on ER for SP", "Address History")
                        saveCreditHistory(objApplication, "E4Q17", "Name on Electoral Roll (SP)", "Address History")
                        saveCreditHistory(objApplication, "E4Q18", "Name on Electoral Roll (SPA)", "Address History")
                        saveCreditHistory(objApplication, "E45U01", "1st Address Electoral Roll Surname Count", "Address History")
                        saveCreditHistory(objApplication, "E45U02", "2nd Address Electoral Roll Surname Count", "Address History")
                        ' Adverse Credit
                        saveCreditHistory(objApplication, "E1A01", "No. CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "E1A02", "Value of CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "E1A03", "Age of Most Recent CCJ", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA1C01", "Bankruptcy Detected", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA1D01", "No. Outstanding CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA1D02", "Value of Outstanding CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA1D03", "Age of Most Recent Outstanding CCJ", "Adverse Credit")
                        saveCreditHistory(objApplication, "E2G01", "Assoc. No. CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "E2G02", "Assoc. Value of CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "E2G03", "Assoc. Age of Most Recent CCJ", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA2I01", "Assoc. Bankruptcy Detected", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA2J01", "Assoc. No. Outstanding CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA2J02", "Assoc. Value of Outstanding CCJs", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA2J03", "Assoc. Age of Most Recent Outstanding CCJ", "Adverse Credit")
                        saveCreditHistory(objApplication, "EA4Q06", "Satisfied CCJ", "Adverse Credit")
                        saveCreditHistory(objApplication, "SPBRPRESENT", "Bankruptcy/Restriction Order Present (SP)", "Adverse Credit")
                        saveCreditHistory(objApplication, "SPABRPRESENT", "Bankruptcy/Restriction Order Present (SPA)", "Adverse Credit")
                        ' CAIS Data
                        saveCreditHistory(objApplication, "E1A04", "No. Non-MO CAIS 8-9", "CAIS Data")
                        saveCreditHistory(objApplication, "E1A05", "Value of CAIS 8-9", "CAIS Data")
                        saveCreditHistory(objApplication, "E1A06", "Age of Most Recent CAIS 8-9", "CAIS Data")
                        saveCreditHistory(objApplication, "E1A10", "Value of Deleted Accounts", "CAIS Data")
                        saveCreditHistory(objApplication, "E1A10", "Age of Most Recent Deleted Account", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B01", "No. CAIS Accounts (Last 3 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B02", "Balance of CAIS Accounts (Last 3 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B03", "Worse Status (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B04", "Balance of CAIS Accounts (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B05", "Worst Status (Over 12 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B06", "Balance of CAIS Accounts (Over 12 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B07", "Worst Status (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B08", "Worst Status (Current)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B09", "No. CAIS Accounts (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B10", "Balance of CAIS Accounts (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B11", "Mortgage Balance", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B12", "No. Status 1 (Current)", "CAIS Data")
                        saveCreditHistory(objApplication, "E1B13", "No. Status 3 (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC01", "Balance of Revolving CAIS (Last 6 Months SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC02", "Balance of Revolving CAIS (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC03", "No. Active CAIS (Last Months SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC04", "Ratio of Ave Balance (Last 3 Months) to Avg Balance (10-12 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC05", "Time Since Most Recent Mortgage Account (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC07", "Balance of Mortgage Accounts (Last 12 Months SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC01", "Worst Status (Last 3 Months) on CAIS Accounts (Last 12 Months SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC02", "Worst Status (Last 3 Months) on CAIS Accounts (Over 12 Months SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC03", "Worst Status (Last 4-6 Months) on CAIS Accounts (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC04", "Worst Status (Current) on Revolving CAIS Accounts (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC07", "Worst Status (Current) on CAIS Accounts (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDHAC08", "Worst Status (Last 6 Months) on Mortgage Accounts (SP)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H01", "Assoc. No. CAIS Accounts (Last 3 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H02", "Assoc. Balance of CAIS Accounts (Last 3 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H03", "Assoc. Worse Status (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H04", "Assoc. Balance of CAIS Accounts (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H05", "Assoc. Worst Status (Over 12 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H06", "Assoc. Balance of CAIS Accounts (Over 12 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H07", "Assoc. Worst Status (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H08", "Assoc. Worst Status (Current)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H09", "Assoc. No. CAIS Accounts (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H10", "Assoc. Balance of CAIS Accounts (All Time)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H11", "Assoc. Mortgage Balance", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H12", "Assoc. No. Status 1 (Current)", "CAIS Data")
                        saveCreditHistory(objApplication, "E2H13", "Assoc. No. Status 3 (Last 6 Months)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC05", "Assoc. Balance of Revolving CAIS (Last 6 Months SPA)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC09", "Assoc. Time Since Most Recent Mortgage Account (SPA)", "CAIS Data")
                        saveCreditHistory(objApplication, "NDECC10", "Assoc. Balance of Mortgage Accounts (Last 12 Months SPA)", "CAIS Data")
                        ' Previous Searches
                        saveCreditHistory(objApplication, "E1E01", "No. Searches (Last 3 Months)", "Previous Searches")
                        saveCreditHistory(objApplication, "E1E02", "No. Searches (Last 6 Months)", "Previous Searches")
                        saveCreditHistory(objApplication, "EA1B01", "No. Searches (SP)", "Previous Searches")
                        saveCreditHistory(objApplication, "NDPSD01", "No. Finance House Searches (Last 3 Months SP)", "Previous Searches")
                        saveCreditHistory(objApplication, "NDPSD03", "No. Bank Searches (Last 3 Months SP)", "Previous Searches")
                        saveCreditHistory(objApplication, "NDPSD04", "No. Credit Card Searches (Last 3 Months SP)", "Previous Searches")
                        saveCreditHistory(objApplication, "NDPSD05", "No. Searches (Last 4-6 Months SP)", "Previous Searches")
                        saveCreditHistory(objApplication, "E2K01", "Assoc. No. Searches (Last 3 Months)", "Previous Searches")
                        saveCreditHistory(objApplication, "E2K02", "Assoc. No. Searches (Last 6 Months)", "Previous Searches")
                        ' CIFAS
                        saveCreditHistory(objApplication, "EA1A01", "CIFAS Detected (SP)", "CIFAS")
                        saveCreditHistory(objApplication, "EA2G01", "CIFAS Detected (SPA)", "CIFAS")
                        saveCreditHistory(objApplication, "EA4P01", "CIFAS Detected", "CIFAS")
                        ' CII
                        saveCreditHistory(objApplication, "NDSPCII", "Consumer Indebtedness Index (SP)", "Consumer Indebtedness")
                        saveCreditHistory(objApplication, "NDSPACII", "Consumer Indebtedness Index (SPA)", "Consumer Indebtedness")
                        ' Never Paid Defaults
                        saveCreditHistory(objApplication, "SPNONPDL12", "Never Paid Defaults (Last 12 Months SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPBALNPDL12", "Balance of Never Paid Defaults (Last 12 Months SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPNONPD", "Never Paid Defaults (All Time SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPBALNPD", "Balance of Never Paid Defaults (All Time SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPTSMRNPD", "Age of Most Recent Never Paid Default (All Time SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPNOEBADSL12", "No. Early Bads (Last 12 Months SP)", "Never Paid Defaults")
                        saveCreditHistory(objApplication, "SPBALEBADSL12", "Balance of Early Bads (Last 12 Months SP)", "Never Paid Defaults")
                        ' Home Credit
                        saveCreditHistory(objApplication, "HCA01", "No. Home Credit Accounts (Last 3 Months)", "Home Credit")
                        saveCreditHistory(objApplication, "HCA02", "Worst Status (Last 3 Months) on Home Credit Accounts (Last 12 Months)", "Home Credit")
                        saveCreditHistory(objApplication, "HCA05", "No. Status 3+ Home Credit Accounts (Last 6 Months)", "Home Credit")
                        saveCreditHistory(objApplication, "HCC01", "No. Default Home Credit Accounts", "Home Credit")
                        saveCreditHistory(objApplication, "HCC02", "Value of Default Home Credit Accounts", "Home Credit")
                        saveCreditHistory(objApplication, "HCC03", "No. Default Home Credit Accounts (Last 12 Months)", "Home Credit")
                        saveCreditHistory(objApplication, "HCC04", "Value of Default Home Credit Accounts (Last 12 Months)", "Home Credit")
                        saveCreditHistory(objApplication, "HCC05", "TSMR Default Home Credit Accounts", "Home Credit")
                        'Payday Loans
                        saveCreditHistory(objApplication, "PDA01", "No. PDL Accounts (Last 6 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDA02", "Time Since Most Recent PDL Account", "Payday Loans")
                        saveCreditHistory(objApplication, "PDA03", "Balance of PDL Accounts", "Payday Loans")
                        saveCreditHistory(objApplication, "PDA04", "Worst Current Status on PDL Accounts", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB05", "No. Payday Loans", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB06", "No. Unique PDL Providers", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB07", "No. Settled Payday Loans (Last 12 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB08", "No. Default Payday Loans (Last 12 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB09", "No. Payday Loans (Last Month)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDB010", "No. Payday Loans (Last 3 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC11", "Worst Status (Last Month)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC12", "Worst Status (Last 3 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC13", "Worst Status (Last 6 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC14", "No. Payday Loan Arrears (Last Month)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC15", "No. Payday Loan Arrears (Last 3 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDC16", "No. Payday Loan Arrears (Last 6 Months)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDE17", "No. Personal Loan Searches (Last 14 Days)", "Payday Loans")
                        saveCreditHistory(objApplication, "PDE18", "No. Personal Loan Arrears (Last Month)", "Payday Loans")
                        ' CATO
                        saveCreditHistory(objApplication, "SPI42", "Unsec Balance % Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPI43", "Sec Balance % Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPI44", "Unsec Balance % Gross Annual Income (Unbanded)", "CATO")
                        saveCreditHistory(objApplication, "SPI45", "Sec Balance % Gross Annual Income (Unbanded)", "CATO")
                        saveCreditHistory(objApplication, "SPI46", "Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPI47", "Revolving Credit Commitments % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPI48", "Non-Revolving Credit Commitments % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPI49", "Mortgage Payment % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPI50", "Credit Limit % Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPI51", "Credit Commitments % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPI52", "Monthly Payments (incl. Mortgage) % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPI53", "Monthly Payments (excl. Mortgage) % Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPK69", "Main Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPK70", "Main Net Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPK71", "Main Net Monthly Income", "CATO")
                        saveCreditHistory(objApplication, "SPK72", "Assoc. Gross Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPK73", "Assoc. Net Annual Income", "CATO")
                        saveCreditHistory(objApplication, "SPK74", "Assoc. Net Monthly Income", "CATO")

                        If (HttpContext.Current.Request("Interactive") <> "Y") Then
                            saveStatus(AppID, Config.DefaultUserID, "CRD", "")
                        End If
                        saveUpdatedDate(AppID, Config.DefaultUserID, "CreditChecked")
                        Dim strAssignUserID As String = getAnyField("MediaCampaignAssignUserID", "tblmediacampaigns", "MediaCampaignID", HttpContext.Current.Request("MediaCampaignID"))
                        If (checkValue(strAssignUserID)) Then
                            Dim strQry2 As String = "SELECT TOP 1 UserID FROM tblusers WHERE UserID IN(" & strAssignUserID & ") AND UserCallCentre = 1 AND UserSuperAdmin = 0 AND UserActive = 1 AND UserLockedOut = 0 AND CompanyID = " & CompanyID & " ORDER BY ISNULL((SELECT COUNT(AppID) AS Expr1 FROM dbo.tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID AND ApplicationStatusDateName = 'AssignedToSales' WHERE (CallCentreUserID = dbo.tblusers.UserID) AND (DATEDIFF(DD, ApplicationStatusDate, GETDATE()) = 0)),0)"
                            Dim strResult2 As String = New Caching(Nothing, strQry2, "", "", "").returnCacheString
                            executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = " & formatField(strResult2, "N", 0) & " WHERE CallCentreUserID = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                            saveUpdatedDate(AppID, Config.DefaultUserID, "AssignedToSales")
                        End If
                        saveNote(AppID, Config.DefaultUserID, "Credit check successfully performed: score was " & strCreditScore)
                        'saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "SCEMS Credit Check Successfully Generated", objInputXMLDoc.InnerXml)
                        SOAPSuccessfulMessage("SCEMS Credit Check Successfully Retrieved", 1, AppID, strResults, objOutputXMLDoc.InnerXml)

                    Else

                        Dim strMessage As String = "Person not found" 'objError.SelectSingleNode("//Message", objNSM2).InnerText
                        saveNote(AppID, Config.DefaultUserID, "Credit check unsuccessful: " & strMessage)
                        'saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "SCEMS Credit Check Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                        SOAPUnsuccessfulMessage("SCEMS Credit Check Unsuccessfully Retrieved - " & strMessage, -5, AppID, "<CreditCheck><Status>-5</Status><Reason>" & strMessage & "</Reason></CreditCheck>", objOutputXMLDoc.InnerXml)

                    End If

                Else
                    Dim strMessage As String = objError.SelectSingleNode("//Message", objNSM2).InnerText
                    Dim strStatus As String = objError.SelectSingleNode("//ErrorCode", objNSM2).InnerText
                    saveNote(AppID, Config.DefaultUserID, "Credit check unsuccessful: " & strMessage)
                    'saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -5, "SCEMS Credit Check Unsuccessfully Generated", objInputXMLDoc.InnerXml)
                    SOAPUnsuccessfulMessage("SCEMS Credit Check Unsuccessfully Retrieved", -5, AppID, "<CreditCheck><Status>-5</Status><Reason>" & strMessage & "</Reason></CreditCheck>", objOutputXMLDoc.InnerXml)
                End If

            Else
                SOAPUnsuccessfulMessage("SCEMS Credit Check Unsuccessfully Retrieved", -5, AppID, "<CreditCheck><Status>0</Status><Reason>Data error</Reason></CreditCheck>", "")
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing
    End Sub

    Private Sub saveCreditHistory(xml As XmlElement, ByVal field As String, ByVal name As String, ByVal typ As String)
        Dim strNode As XmlNode = xml.SelectSingleNode("//" & field)
        If (Not strNode Is Nothing) Then
            Dim strValue As String = Trim(strNode.InnerText)
            Dim strSQL As String = "INSERT INTO tblcredithistory (AppID, CompanyID, CreditHistoryField, CreditHistoryType, CreditHistoryValue) VALUES (" & _
                                    formatField(AppID, "N", 0) & ", " & _
                                    formatField(CompanyID, "N", 0) & ", " & _
                                    formatField(name, "", "") & ", " & _
                                    formatField(typ, "", "") & ", " & _
                                    formatField(strValue, "", "") & ")"
            executeNonQuery(strSQL)
        End If
    End Sub

    Private Sub checkPreviousCreditHistory()
        Dim strApp1FirstName As String = "", strApp1Surname As String = "", dteApp1DOB As String = "", strAddressPostCode As String = ""
        Dim strQry As String = "SELECT App1FirstName, App1Surname, App1DOB, AddressPostCode FROM tblapplications WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strApp1FirstName = Row.Item("App1FirstName")
                strApp1Surname = Row.Item("App1Surname")
                dteApp1DOB = Row.Item("App1DOB")
                strAddressPostCode = Row.Item("AddressPostCode")
            Next
        End If
        dsCache = Nothing
        strQry = "SELECT TOP 1 CreditScore, MortgagePaymentsMissed12, NonMortgagePaymentsMissed12, DefaultTotal, CCJTotal, UnsecuredCreditorTotal, ApplicationCreditSearches, DeclaredBankrupt FROM vwcredithistory WHERE App1FirstName = '" & Replace(strApp1FirstName, "'", "''") & "' AND App1Surname = '" & Replace(strApp1Surname, "'", "''") & "' AND App1DOB = " & formatField(dteApp1DOB & " 00:00:00", "DTTM", Config.DefaultDate) & " AND AddressPostCode = '" & strAddressPostCode & "' AND CompanyID = '" & CompanyID & "' AND DATEDIFF(D, CreditHistoryDate, GETDATE()) <= 90 ORDER BY CreditHistoryDate ASC"
        dsCache = New Caching(Nothing, strQry, "", "", "").returnCache
        Dim intCreditScore As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                intCreditScore = Row.Item("CreditScore")
                executeNonQuery("UPDATE tblapplications SET ApplicationCreditScore = " & formatField(Row.Item("CreditScore"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET MortgagePaymentsMissed12 = " & formatField(Row.Item("MortgagePaymentsMissed12"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET NonMortgagePaymentsMissed12 = " & formatField(Row.Item("NonMortgagePaymentsMissed12"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET DefaultTotal = " & formatField(Row.Item("DefaultTotal"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET CCJTotal = " & formatField(Row.Item("CCJTotal"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET UnsecuredCreditorTotal = " & formatField(Row.Item("UnsecuredCreditorTotal"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET ApplicationCreditSearches = " & formatField(Row.Item("ApplicationCreditSearches"), "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                executeNonQuery("UPDATE tblapplications SET DeclaredBankrupt = " & formatField(Row.Item("DeclaredBankrupt"), "U", "N") & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            Next
        End If
        If (checkValue(intCreditScore)) Then
            Dim strResults As String = "<CreditCheck>" & _
              "<Status>1</Status>" & _
              "<CreditScore>" & intCreditScore & "</CreditScore>" & _
              "</CreditCheck>"
            If (HttpContext.Current.Request("Interactive") <> "Y") Then
                saveStatus(AppID, Config.DefaultUserID, "CRD", "")
            End If
            saveUpdatedDate(AppID, Config.DefaultUserID, "CreditChecked")
            Dim strAssignUserID As String = getAnyField("MediaCampaignAssignUserID", "tblmediacampaigns", "MediaCampaignID", HttpContext.Current.Request("MediaCampaignID"))
            If (checkValue(strAssignUserID)) Then
                Dim strQry2 As String = "SELECT TOP 1 UserID FROM tblusers WHERE UserID IN(" & strAssignUserID & ") AND UserCallCentre = 1 AND UserSuperAdmin = 0 AND UserActive = 1 AND UserLockedOut = 0 AND CompanyID = " & CompanyID & " ORDER BY ISNULL((SELECT COUNT(AppID) AS Expr1 FROM dbo.tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID AND ApplicationStatusDateName = 'AssignedToSales' WHERE (CallCentreUserID = dbo.tblusers.UserID) AND (DATEDIFF(DD, ApplicationStatusDate, GETDATE()) = 0)),0)"
                Dim strResult2 As String = New Caching(Nothing, strQry2, "", "", "").returnCacheString
                executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = " & formatField(strResult2, "N", 0) & " WHERE CallCentreUserID = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                saveUpdatedDate(AppID, Config.DefaultUserID, "AssignedToSales")
            End If
            saveNote(AppID, Config.DefaultUserID, "Credit check previously performed: score was " & intCreditScore)
            SOAPSuccessfulMessage("SCEMS Credit Check Previously Performed", 1, AppID, strResults, "")
        End If
    End Sub

    Private Function getNodeValueAsNumber(ByVal xml As XmlElement, ByVal fields As String) As Integer
        Dim intValue As Integer = 0
        Dim arrFields As Array = Split(fields, ",")
        For x As Integer = 0 To UBound(arrFields)
            Dim strNode As XmlNode = xml.SelectSingleNode("//" & arrFields(x))
            If (Not strNode Is Nothing) Then
                If (IsNumeric(strNode.InnerText)) Then
                    intValue += CInt(strNode.InnerText)
                End If
            End If
        Next
        If (intValue < 0) Then intValue = 0
        Return intValue
    End Function

    Private Sub writeAttribute(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objElement As XmlElement = doc.SelectSingleNode("//" & parent, nsm)
                If Not (objElement Is Nothing) Then
                    objElement.SetAttribute(fld, val)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String, ByVal nsm As XmlNamespaceManager, Optional ns As String = "def")
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                If (nsm Is Nothing) Then
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                Else
                    objTest = doc.SelectSingleNode("//" & parent & "/" & fld, nsm)
                End If
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    If (nsm Is Nothing) Then
                        objApplication = doc.SelectSingleNode("//" & parent)
                    Else
                        objApplication = doc.SelectSingleNode("//" & parent, nsm)
                    End If
                    Dim objNewNode As XmlElement = doc.CreateElement(Replace(fld, ns & ":", ""), nsm.LookupNamespace(ns))
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub writeToNode(ByVal doc As XmlDocument, ByVal man As String, ByVal parent As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = Nothing
                objTest = doc.SelectSingleNode("//" & parent & "/" & fld)
                If Not (objTest Is Nothing) Then
                    objTest.InnerText = val
                Else
                    Dim objApplication As XmlNode = Nothing
                    objApplication = doc.SelectSingleNode("//" & parent)
                    Dim objNewNode As XmlElement = doc.CreateElement(fld)
                    Dim objNewText As XmlText = doc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String, resp As String, xml As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append(resp)
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (checkValue(xml)) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg, xml)
        End If
        'postEmail("", "itsupport@engaged-solutions.co.uk", "Iovation XML (Test) Error", "AppID: " & AppID & "<br>" & msg, True, "")
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Function saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String) As String
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strQry)
    End Function

    Private Function postExperianWebRequest(ByVal url As String, ByVal post As String, soapaction As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            Dim objCert As X509Certificate = getCertificate()
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .ContentType = "text/xml; charset=utf-8"
                .Timeout = 120000
                .ReadWriteTimeout = 120000
                .Headers.Add("SOAPAction", soapaction)
                .ClientCertificates.Add(objCert)
            End With
            'Try
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'For Each item As X509Certificate In objRequest.ClientCertificates
            '    responseWrite("Name:" & item.GetName & "<br>")
            '    responseWrite("Public Key:" & item.GetPublicKeyString & "<br>")
            '    responseWrite("Data:" & item.GetRawCertDataString & "<br>")
            '    responseWrite("Issuer:" & item.GetIssuerName & "<br>")
            '    responseWrite("Hash:" & item.Subject & "<br>")
            '    responseWrite("Serial:" & item.GetSerialNumberString & "<br>")
            'Next
            'responseEnd()
            With objWriter
                .Write(post)
                .Close()
            End With
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
            'Try
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
            'Catch err As Exception
            '    reportErrorMessage(err)
            '    Return Nothing
            'End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
