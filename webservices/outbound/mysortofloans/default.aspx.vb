﻿Imports Config, Common, CommonSave
Imports System.IO

Partial Class SendIndividualApp
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignIDOutbound As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
    Private strMailTo As String = HttpContext.Current.Request("MediaCampaignDeliveryEmailAddress")
	Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")  
	Private intHotkeyUserID As String = HttpContext.Current.Request("HotkeyUserID")

    Public Sub sendIndividualApp()
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        Dim strResult As String = ""
		Dim strCompanyName As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CompanyName")
		Dim strDeliveryEmailAddress = getAnyFieldByCompanyID("MediaCampaignDeliveryEmailAddress", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignIDOutbound)
		Dim strFriendlyName As String = getAnyFieldByCompanyID("MediaCampaignFriendlyNameOutbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)        
		If (checkValue(strFriendlyName)) Then strCompanyName = strFriendlyName  		           
           
                Dim strHeaderRow As String = "", strDataRow As String = "", strComma As String = ""
                Dim strSQL As String = "SELECT * FROM vwmysortofloans WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' "				
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim strRowClass As String = "row1"
                    Dim intCols As Integer = 0
                    For Each Row As DataRow In dsCache.Rows													
                        intCols = 0						
                        For Each Column As DataColumn In dsCache.Columns
                            If intCols = 0 Then strComma = "" Else strComma = ","
                            intCols += 1
                            strHeaderRow += strComma & Column.ColumnName
                            strDataRow += strComma & Row(Column).ToString																			
                        Next
						
                    Next
                End If
                dsCache = Nothing  

                Dim strTimeStamp As String = Year(Today) & padZeros(Month(Today), 2) & padZeros(Day(Today), 2) & padZeros(TimeOfDay.Hour, 2) & padZeros(TimeOfDay.Minute, 2) & padZeros(TimeOfDay.Second, 2) & padZeros(TimeOfDay.Millisecond, 2)
                Dim strDir As String = "F:\data\attachments"
				Dim strCompanyDir As String = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))
                If (Not Directory.Exists(strDir & "\" & strCompanyDir)) Then
                    Directory.CreateDirectory(strDir & "\" & strCompanyDir)
                End If
                Dim strDateDir As String = DatePart(DateInterval.WeekOfYear, Config.DefaultDate) & "-" & Config.DefaultDate.Year
                If (Not Directory.Exists(strDir & "\" & strCompanyDir & "\" & strDateDir)) Then
                    Directory.CreateDirectory(strDir & "\" & strCompanyDir & "\" & strDateDir)
                End If
			    Dim strFileName As String = strDir & "\" & strCompanyDir & "\" & strDateDir & "\" & AppID & "_" & strTimeStamp & ".csv"
                Dim objFile As StreamWriter = New StreamWriter(strFileName)
                objFile.WriteLine(strHeaderRow)
                objFile.WriteLine(strDataRow)
                objFile.Close()
				incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
                strResult = postEmailWithMediaCampaignID("", strDeliveryEmailAddress, "New lead from " & strCompanyName, "Please find attached a new application from " & strCompanyName, True, AppID & "_" & strTimeStamp & ".csv")
           
        If (strResult = "1") Then
            setTransferState(AppID, strMediaCampaignIDOutbound, "", "", intHotkeyUserID, strMediaCampaignScheduleID)
            HttpContext.Current.Response.Write(1 & "|" & encodeURL("Email transfer successful"))
        Else
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Email transfer not successful"))
        End If
    End Sub

End Class
