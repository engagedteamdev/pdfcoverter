using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using Newtonsoft.Json.Converters;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;

public partial class Default : System.Web.UI.Page
{



	private string strAppID = HttpContext.Current.Request["AppID"];
	private string strLendingWorksURL = "";
	private string strLendingWorksURLApply = "";

	class theData
	{
		public string token { get; set; }
		public theUser user { get; set; }
		public theLoan loan { get; set; }
	}

	class theUser
	{
		public List<theAddress> addresses { get; set; }
		public string email { get; set; }
		public string title { get; set; }
		public string first { get; set; }
		public string last { get; set; }
		public string dob { get; set; }
		public string mobile { get; set; }
		public string telephone { get; set; }
		public string sort_code { get; set; }
		public string account_number { get; set; }
	}

	class theAddress
	{
		public int position { get; set; }
		public List<string> lines { get; set; }
		public string post_code { get; set; }
		public string residential_status { get; set; }
		public int months { get; set; }
	}

	class theLoan
	{
		public theLoan()
		{
			british = 0;
			future_changes_of_income = 0;
		}
		public int amount { get; set; }
		public int duration { get; set; }
		public string purpose { get; set; }
		//public string purpose_specified { get; set; }
		public int home_worth { get; set; }
		public int rent_mortgage { get; set; }
		public int british { get; set; }
		public int income { get; set; }
		public string marital_status { get; set; }
		public int future_changes_of_income { get; set; }
		public string employment_status { get; set; }
		public int time_in_role { get; set; }
		public int dependants { get; set; }
		public int earners { get; set; }

	}

	class theApplyData
	{
		public string token { get; set; }
		public string id { get; set; }
	}





	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}


	public void Run()
	{


		ServiceModel model = new ServiceModel();

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringESBeta"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}


		try
		{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwlendingworksnew where AppId = {0}", strAppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{

				int AddressMonths = 0,
					ProductTerm = 0,
					App1TimeInEmployment = 0;

				int Amount = 0,
					PropertyValue = 0,
					MortgageBalance = 0,
					App1AnnualIncome = 0;

				int.TryParse(reader["AddressMonths"].ToString(), out AddressMonths);

				int.TryParse(reader["ProductTerm"].ToString(), out ProductTerm);

				int.TryParse(reader["App1EmploymentMonths"].ToString(), out App1TimeInEmployment);

				int.TryParse(reader["Amount"].ToString(), out Amount);

				int.TryParse(reader["PropertyValue"].ToString(), out PropertyValue);

				int.TryParse(reader["MortgageBalance"].ToString(), out MortgageBalance);

				int.TryParse(reader["App1Salary"].ToString(), out App1AnnualIncome);

				model = new ServiceModel()
				{
					App1EmailAddress = reader["App1EmailAddress"].ToString(),
					App1Title = reader["App1Title"].ToString(),
					App1FirstName = reader["App1FirstName"].ToString(),
					App1Surname = reader["App1Surname"].ToString(),
					App1DOB = reader["App1DOB"].ToString(),
					App1MobileTelephone = reader["App1MobileTelephone"].ToString(),
					App1HomeTelephone = reader["App1HomeTelephone"].ToString(),
					App1SortCode = reader["App1BankSortCode"].ToString(),
					App1BankAccountNumber = reader["App1BankAccountNumber"].ToString(),
					AddressLine1 = reader["AddressLine1"].ToString(),
					AddressPostCode = reader["AddressPostCode"].ToString(),
					AddressLivingArrangment = reader["AddressLivingArrangement"].ToString(),
					AddressMonths = AddressMonths,
					Amount = Amount,
					ProductTerm = ProductTerm,
					ProductPurpose = reader["ProductPurpose"].ToString(),
					ProductPurposeDesciption = reader["ProductPurposeDescription"].ToString(),
					PropertyValue = PropertyValue,
					MortgageBalance = MortgageBalance,
					App1Nationality = reader["App1Nationality"].ToString(),
					App1AnnualIncome = App1AnnualIncome,
					App1MaritalStatus = reader["App1MaritalStatus"].ToString(),
					App1EmploymentStatus = reader["App1EmploymentStatus"].ToString(),
					App1EmploymentTimeInRole = App1TimeInEmployment,
					AddressTown = reader["AddressTown"].ToString(),
					AddressCounty = reader["AddressCounty"].ToString()


				};

			}

			connection.Close();

		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}



		var APISecret = "8e5a2a65b9d682710effa76491231339";
		var APIToken = "7da5b8cc6b5066a5835b80c07571e17c";

		//root node for json
		theData data = new theData();

		//setup the user class data
		theUser auser = new theUser();
		auser.addresses = new List<theAddress>();
		auser.email = model.App1EmailAddress;
		auser.title = model.App1Title;
		auser.first = model.App1FirstName;
		auser.last = model.App1Surname;
		auser.dob = model.App1DOB;
		auser.mobile = model.App1MobileTelephone;
		auser.telephone = model.App1HomeTelephone;
		auser.sort_code = model.App1SortCode;
		auser.account_number = model.App1BankAccountNumber;

		//setup the addresses + address lines
		List<string> AddLines = new List<string>();
		AddLines.Add(model.AddressLine1 + "," + model.AddressTown + "," + model.AddressCounty);
		AddLines.Add(model.AddressCounty);
		//set the address with the above lines
		auser.addresses.Add(new theAddress() { position = 0, post_code = model.AddressPostCode, residential_status = model.AddressLivingArrangment, months = model.AddressMonths, lines = AddLines });

		//setup loan details
		theLoan aloan = new theLoan();
		aloan.amount = model.Amount;
		aloan.duration = model.ProductTerm;
		aloan.purpose = model.ProductPurpose;
		if (model.ProductPurpose == "ot")
		{
			//aloan.purpose_specified = model.ProductPurposeDesciption;
		}
		aloan.home_worth = model.PropertyValue;
		aloan.rent_mortgage = model.MortgageBalance;
		aloan.british = 1;
		aloan.income = model.App1AnnualIncome;
		aloan.marital_status = model.App1MaritalStatus;
		aloan.future_changes_of_income = 0;
		aloan.employment_status = model.App1EmploymentStatus;
		aloan.time_in_role = model.App1EmploymentTimeInRole;
		aloan.dependants = 1;
		aloan.earners = 1;

		data.loan = aloan;
		data.user = auser;
		data.token = APIToken;

		string json = JsonConvert.SerializeObject(data);

		string json64 = System.Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(json));
		HttpContext.Current.Response.Write(json + "<br></br>");
		//HttpContext.Current.Response.End();
		//System.Web.HttpContext.Current.Response.Write(json64 + "<br></br>");


		var encoding = new ASCIIEncoding();
		var secretByte = encoding.GetBytes(APISecret);
		var msgbytes = encoding.GetBytes(json64);
		byte[] hashmsg;
		string hashmsgstring;

		using (var hmac265 = new HMACSHA256(secretByte))
		{
			hashmsg = hmac265.ComputeHash(msgbytes);
			hashmsgstring = BitConverter.ToString(hashmsg).Replace("-", "").ToLower();
		}

		//send the request to LendingWorks
		strLendingWorksURL = "http://integration.lendingworks.co.uk/api/introduction/loan/create";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(strLendingWorksURL);

		HttpContent contentBody = new StringContent("payload=" + json64 + "." + hashmsgstring);
		contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

		var res = client.PostAsync(strLendingWorksURL, contentBody).Result;

		var responseContent = res.Content;
		//HttpContext.Current.Response.Write(responseContent.ReadAsStringAsync().Result);
		//HttpContext.Current.Response.End();
		var parsed = JObject.Parse(responseContent.ReadAsStringAsync().Result);
		//HttpContext.Current.Response.Write(parsed + "<br></br>");
		//HttpContext.Current.Response.End();

		string id = parsed.SelectToken("loan.id").Value<string>();
		var reference = parsed.SelectToken("loan.reference").Value<string>();

		//HttpContext.Current.Response.Write(id + "</br>");


		if (!string.IsNullOrEmpty(id))
		{

			theApplyData dataapply = new theApplyData();

			dataapply.id = id;
			dataapply.token = APIToken;
			string jsonApply = JsonConvert.SerializeObject(dataapply);

			string jsonApply64 = System.Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(jsonApply));
			HttpContext.Current.Response.Write(jsonApply + "<br></br>");
			//HttpContext.Current.Response.End();
			//System.Web.HttpContext.Current.Response.Write(json64 + "<br></br>");


			var encodingApply = new ASCIIEncoding();
			var secretByteApply = encodingApply.GetBytes(APISecret);
			var msgbytesApply = encodingApply.GetBytes(jsonApply64);
			byte[] hashmsgApply;
			string hashmsgstringApply;

			using (var hmac265 = new HMACSHA256(secretByteApply))
			{
				hashmsgApply = hmac265.ComputeHash(msgbytesApply);
				hashmsgstringApply = BitConverter.ToString(hashmsgApply).Replace("-", "").ToLower();
			}

			strLendingWorksURLApply = "http://integration.lendingworks.co.uk/api/introduction/loan/apply";
			HttpClient clientApply = new HttpClient();
			clientApply.BaseAddress = new Uri(strLendingWorksURLApply);

			HttpContent contentBodyApply = new StringContent("payload=" + jsonApply64 + "." + hashmsgstringApply);
			contentBodyApply.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

			var resapply = clientApply.PostAsync(strLendingWorksURLApply, contentBodyApply).Result;

			var responseContentApply = resapply.Content;

			var parsedApply = JObject.Parse(responseContentApply.ReadAsStringAsync().Result);

			HttpContext.Current.Response.Write(parsedApply + "<br></br>");
			HttpContext.Current.Response.End();

			if (parsedApply["errors"] != null)
			{
				int errors = 0;
				string message = "";
				foreach (var error in parsedApply["errors"]) {
					if (errors == 0)
					{
						message = parsedApply.SelectToken("errors[" + errors + "].message").Value<string>();
					}
					else
					{
						message += parsedApply.SelectToken("errors[" + errors + "].message").Value<string>() + ",";
					}
					errors = errors + 1;
				}
				HttpContext.Current.Response.Write("0| " + message);
			}
			else
			{
				string applyid = parsedApply.SelectToken("loan.id").Value<string>();
				var applyreference = parsedApply.SelectToken("loan.reference").Value<string>();
				var monthly_cost = parsedApply.SelectToken("loan.monthly_cost").Value<string>();
				var apr = parsedApply.SelectToken("loan.apr").Value<string>();
				var TotalCost = parsedApply.SelectToken("loan.total_cost").Value<string>();
				HttpContext.Current.Response.Write("1| Accepted By Lending Works: Loan ID: " + applyid + ", Reference: " + applyreference + ", Monthly Payment: " + monthly_cost + ", APR: " + apr + ", Total Cost: " + TotalCost + "");

			}

			HttpContext.Current.Response.End();

		}
	}




	class ServiceModel
	{

		public string App1EmailAddress { get; set; }
		public string App1Title { get; set; }
		public string App1FirstName { get; set; }
		public string App1Surname { get; set; }
		public string App1DOB { get; set; }
		public string App1MobileTelephone { get; set; }
		public string App1HomeTelephone { get; set; }
		public string App1SortCode { get; set; }
		public string App1BankAccountNumber { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressTown { get; set; }
		public string AddressCounty { get; set; }
		public string AddressPostCode { get; set; }
		public string AddressLivingArrangment { get; set; }
		public int AddressMonths { get; set; }
		public int Amount { get; set; }
		public int ProductTerm { get; set; }
		public string ProductPurpose { get; set; }
		public string ProductPurposeDesciption { get; set; }
		public int PropertyValue { get; set; }
		public int MortgageBalance { get; set; }
		public string App1Nationality { get; set; }
		public int App1AnnualIncome { get; set; }
		public string App1MaritalStatus { get; set; }
		public string App1EmploymentStatus { get; set; }
		public int App1EmploymentTimeInRole { get; set; }
		public int App1Earners { get; set; }

	}



	}
