using System;
using System.Net;
using System.Collections.Specialized;
using WebSocketSharp;
using MinimalJson;
using System.Web;
using System.Web.UI;

public partial class ScarletMetrics : Page
{
		private string MetricsWSURL = "engagedcrmapi.etpbx.com/wss";
		private string MetricsWSPort = "8089";
		private string Mode = HttpContext.Current.Request["Mode"];
		private WebSocket elisha;

		/* WS */
		public ScarletMetrics()
		{
			elisha = new WebSocket("wss://" + MetricsWSURL);

			elisha.OnOpen += (sender, e) =>
			{
				
				
				JsonObject jsonMessage = new JsonObject()
					.add("Command", "login")
					.add("ClientExtension", "003709106")
					.add("ClientAuthKey", ">YEL~Y*mUbSq2h?U7j<EPHUb&Sks@T$+");
				elisha.Send(jsonMessage.ToString());
				
				var status = elisha.ReadyState.ToString();

				if (status == "Open")
				{

					JsonObject jsonCall = new JsonObject()
					.add("Command", "call")
					.add("ClientExtension", "003709106")
					.add("CallNumber", "907974400840");
					elisha.Send(jsonCall.ToString());


				}

			};

	

		elisha.OnMessage += (sender, e) =>
		{

			HttpContext.Current.Response.Write(e.Data);
			HttpContext.Current.Response.End();

		};


		elisha.OnClose += (sender, e) =>
		{
			HttpContext.Current.Response.Write(e.Reason);
			HttpContext.Current.Response.End();

		};

		elisha.OnError += (sender, e) =>
		{
			HttpContext.Current.Response.Write(e.Message);
			HttpContext.Current.Response.End();

		};

		elisha.Connect();

		}
		public void Send(string message)
		{

		
			JsonObject jsonMessage = new JsonObject();
			jsonMessage.add("Command", "login").add("message", message);
			elisha.Send(jsonMessage.ToString());
			
		}
	}
