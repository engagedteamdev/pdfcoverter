﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLCRM
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private AppID As String = HttpContext.Current.Request.QueryString("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignIDOutbound")
    Private strMediaCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
	Private strCRMInstance As String = HttpContext.Current.Request.QueryString("CRMInstance")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XMLDocument = New XMLDocument


    Private strProductType As String = HttpContext.Current.Request("frmProductType"), strCustomerEmailID As String = ""
    Private strErrorMessage As String = ""
	Private strMediaCampID as string = HttpContext.Current.Request.QueryString("MediaCampaignID")

    Public Sub generateXml()
        strXMLURL = "https://crm.lunarmedia.co.uk/net/webservices/inbound/soappost.aspx"

        If checkValue(AppID) Then
           sendXML()
        End If
    End Sub




    Private Sub sendXML()
        
		Dim LeadIncome1 As String = ""
        Dim LeadIncome2 As String = ""
		
		Dim strConntection As String = ""

		If(strCRMInstance = "ES")Then 
			strConntection = System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringES").ConnectionString
		Else If(strCRMInstance = "Sparky")Then 
			strConntection = System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringSparky").ConnectionString
		Else If (strCRMInstance = "CoreCover")Then 
			strConntection = System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringCoverGroup").ConnectionString
		End If

		
		objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/webservices/xmlfiles/TPFC/blank.xml"))

        writeXML("n", "MediaCampaignID", strMediaCampaignReference)
        'writeXML("n", "SalesUserID", getAnyField("MediaUserRef", "tblmediausers", "MediaUserID", strTransferUserID))

			Dim strSQLLead As String = "SELECT * FROM vwXMLTPFC WHERE ClientReference = " & AppID & "" 
			Dim dtb1Lead As New DataTable
	        using DBConnection As New SqlConnection(strConntection)					 
	        DBConnection.open()			
	        Using dad As New SqlDataAdapter(strSQLLead,DBConnection)
	        dad.Fill(dtb1Lead)
	        End Using
	        DBConnection.close()
	        End Using
	        If (dtb1Lead.Rows.Count > 0) Then   
			   For Each Row As DataRow In dtb1Lead.Rows
                For Each Column As DataColumn In dtb1Lead.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") Then
                        Call writeXML("n", Column.ColumnName.ToString, Row(Column).ToString)
                    End If
                Next
            Next
			End If 


       	If (checkValue(strProductType)) Then
			Call writeXML("n", "ProductType", strProductType)
		End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	

                Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
                Dim objReader As New StreamReader(objResponse.GetResponseStream())

				'HttpContext.Current.Response.ContentType = "text/xml"
                'HttpContext.Current.Response.Write(objReader.ReadToEnd())
                'HttpContext.Current.Response.End()
            

                objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

                objReader.Close()
                objReader = Nothing

                If (objResponse.StatusCode.ToString = "OK") Then
                    Dim objApplication As XmlNode = objOutputXMLDoc.SelectSingleNode("//executeSoapResponse")
                    Select Case objApplication.SelectSingleNode("executeSoapStatus").InnerText
                        Case "1"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(1 & "|" & objApplication.SelectSingleNode("executeSoapNo").InnerText & "| Umbrella")

                        Case "-1"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText & "| Failed To Transfer")
						
                        Case "-2"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText & "| Failed To Transfer")
						
                        Case "-3"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText & "| Failed To Transfer")
						
                        Case "-4"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText & "| Failed To Transfer")
						
                        Case "-5"
                            HttpContext.Current.Response.Clear()
                            HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText & "| Failed To Transfer")
                 
						End Select
                Else
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(0)
                End If

                objResponse = Nothing

           

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeXML(ByVal man As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                Else
                    Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal strPost As String)

		Dim strQry As String = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
				   "VALUES(" & formatField(ip, "", "") & ", " & _
				   formatField(soapid, "N", 0) & ", " & _
				   formatField(result, "N", 0) & ", " & _
				   formatField(msg, "", "") & ", " & _
				   formatField(strPost, "", "") & ", " & _
				   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
		executeNonQuery(strQry)
	End Sub
End Class
