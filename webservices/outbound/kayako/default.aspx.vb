﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography

' ** Revision history **
'
' 18/06/2013 - File created - BSE
'
' ** End Revision History **

Partial Class RESTKayako
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strURL As String = "", strAPIKey As String = "", strSecret As String = "", strGUID As String = Guid.NewGuid.ToString
    Private strErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        ' Get last error from the server
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As Diagnostics.StackTrace = New Diagnostics.StackTrace(ex, True)
        Dim sf As Diagnostics.StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " Error from " & objLeadPlatform.Config.DefaultUserFullName, strMessage, True, "")
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write("<?xml version=""1.0"" encoding=""utf-8""?><error><message>" & ex.Message & "</message><type>" & ex.GetType.ToString & "</type><method>" & sf.GetMethod().ToString() & "</method><line_number>" & sf.GetFileLineNumber().ToString() & "</line_number><url>" & encodeURL(HttpContext.Current.Request.Url.ToString) & "</url><query_string>" & encodeURL(Request.QueryString.ToString) & "</query_string><form_data>" & encodeURL(Request.Form.ToString) & "</form_data></error>")
        HttpContext.Current.Response.End()
    End Sub

    Public Sub setupREST()
        If (strEnvironment = "live") Then
            strURL = "http://support.jibbagroup.com/api/index.php"
        Else
            strURL = "http://support.jibbagroup.com/api/index.php"
        End If

        Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Kayako%' AND CompanyID = '" & Config.CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "KayakoAPIKey") Then strAPIKey = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "KayakoSecret") Then strSecret = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing
        sendREST()
    End Sub

    Private Sub sendREST()

        Dim strSignature As String = hashString(strSecret, strGUID)
        'responseWrite(strURL & "?e=/Tickets/TicketCount&apikey=" & strAPIKey & "&signature=" & strSignature & "&salt=" & strGUID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = getWebRequest(strURL & "?e=/Tickets/TicketCount&apikey=" & strAPIKey & "&signature=" & strSignature & "&salt=" & strGUID)

        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            HttpContext.Current.Response.ContentType = "text/xml"
            HttpContext.Current.Response.Write(objReader.ReadToEnd)
            HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())
            objReader.Close()
            objReader = Nothing

            'HttpContext.Current.Response.ContentType = "text/xml"
            'HttpContext.Current.Response.Write(objOutputXMLDoc.InnerXml)
            'HttpContext.Current.Response.End()

            ' Parse the XML document.
            Dim objApplication As XmlElement = objOutputXMLDoc.SelectSingleNode("//ticketcount/departments")
            If (Not objApplication Is Nothing) Then
                Dim strCount As String = objApplication.SelectSingleNode("//department[1]/totalitems").InnerText
                responseWrite(strCount & ",")
            End If
        End If
        If (checkResponse(objResponse, "")) Then
            objResponse.Close()
        End If
        objResponse = Nothing

        objOutputXMLDoc = Nothing

    End Sub

    Private Function hashString(ByVal key As String, ByVal text As String) As String
        Dim objHash As HMACSHA256 = New HMACSHA256(Encoding.UTF8.GetBytes(key))
        Dim bHash As Byte() = objHash.ComputeHash(Encoding.UTF8.GetBytes(text))
        Return encodeURL(Convert.ToBase64String(bHash))
    End Function

End Class
