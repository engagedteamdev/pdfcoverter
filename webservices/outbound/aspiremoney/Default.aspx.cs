using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using System.ServiceModel;
using uk.co.secureloanportal.brokerfeed;

public partial class Default : Page
{
	private string AppID = HttpContext.Current.Request["AppID"];
	private string loanType = HttpContext.Current.Request["loanType"];
	private string strMediaCampaignID = HttpContext.Current.Request["MediaCampaignIDOutbound"];
	private string strMediaCampaignCampaignReference = HttpContext.Current.Request["MediaCampaignReference"];
	private string strTransferUserID = HttpContext.Current.Request["TransferUserID"];
	private string intHotkeyUserID = HttpContext.Current.Request["HotkeyUserID"];
	private string strMediaCampaignScheduleID = HttpContext.Current.Request["MediaCampaignScheduleID"];
	private string strMediaCampID = HttpContext.Current.Request["MediaCampaignID"];
	

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			httpPost();
		}
		catch (Exception t)
		{
			throw t;
		}
	}

	public void httpPost()
	{
		string strdob = "";


		XMLReceive client = new XMLReceive();
		
		ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
		client.Url = "https://brokerfeed.secureloanportal.co.uk/TestSite/XMLReceive.asmx";
		NetworkCredential myCred = new System.Net.NetworkCredential("xmltester", "XMLT35t", null);
		client.Credentials = myCred;


		ServiceModel model = new ServiceModel();

		List<string> valid = new List<string>();
	

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringES"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception)
		{
			throw new Exception("Failed to open connection to server.");
		}



		try
		{
			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwaspiremoney where AppId = {0}", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{
				DateTime app1DOB = DateTime.Now;
				decimal app1Salary = 0;

				int appId = 0,
					principal = 0,
					AddressMonths = 0,
					EmployerMonths = 0,
					BankMonths = 0,
					homevalue = 0,
					MortgageBal = 0,
					MortgagePayment = 0,
					PreviousHouseNumber = 0,
					PreviousMonthsAtAddress = 0;

				int PreviousMonthsAtEmployment = 0;

				if (!string.IsNullOrEmpty(reader["App1DOB"].ToString()))
				{
					app1DOB = DateTime.Parse(reader["App1DOB"].ToString());
					strdob = app1DOB.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				}

				int.TryParse(reader["AppID"].ToString(), out appId);
			
				int.TryParse(reader["Amount"].ToString(), out principal);

				int.TryParse(reader["AddressMonths"].ToString(), out AddressMonths);

				decimal.TryParse(reader["App1Salary"].ToString(), out app1Salary);

				int.TryParse(reader["App1EmploymentMonths"].ToString(), out EmployerMonths);

				int.TryParse(reader["App1TimeWithBank"].ToString(), out BankMonths);

				int.TryParse(reader["PropertyValue"].ToString(), out homevalue);

				int.TryParse(reader["MortgageBalance"].ToString(), out MortgageBal);

				int.TryParse(reader["MonthlyMortgageAmount"].ToString(), out MortgagePayment);

				int.TryParse(reader["PreviousMonthsAtAddress"].ToString(), out PreviousMonthsAtAddress);

				int.TryParse(reader["PreviousHouseNumber"].ToString(), out PreviousHouseNumber);

				int.TryParse(reader["PreviousMonthsAtEmployment"].ToString(), out PreviousMonthsAtEmployment);

				model = new ServiceModel()
					{
						AppID = appId,
						Proposal_Principal = principal,

						//First Applicant
						App1Customer_Title = reader["App1Title"].ToString(),
						App1Customer_Forename = reader["App1Firstname"].ToString(),
						App1Customer_Surname = reader["App1Surname"].ToString().Trim('\r', '\n'),
						App1App1DOB = strdob,
						App1Customer_MartitalStatus = reader["App1MaritalStatus"].ToString(),
						App1Customer_HomeNumber = reader["App1HomeTelephone"].ToString(),
						App1Customer_MobileNumber = reader["App1MobileTelephone"].ToString(),
						App1EmploymentStatus = reader["App1EmploymentStatus"].ToString(),
						App1Customer_Salary = app1Salary,
						App1ResidentStatus = reader["AddressLivingArrangement"].ToString(),
						App1EmployerName = reader["App1EmployerName"].ToString(),
						App1EmployerPostCode = reader["App1EmployerPostCode"].ToString(),
						App1EmployerAddressLine1 = reader["App1EmployerAddressLine1"].ToString(),
						App1EmployerMonths = EmployerMonths,
						App1BankAccountType = reader["App1BankAccountType"].ToString(),
						App1BankAccountNumber = reader["App1BankAccountNumber"].ToString(),
						App1BankAccountMonths = BankMonths,
						App1BankHolderName = reader["App1BankHolderName"].ToString(),
						App1BankName = reader["App1BankName"].ToString(),

						//App 1 Customer Address
						Customer_Address_HouseNumber = reader["AddressHouseNumber"].ToString(),
						Customer_Address_Line1 = reader["AddressLine1"].ToString(),
						Customer_Address_Town = reader["AddressTown"].ToString(),
						Customer_Address_PostCode = reader["AddressPostCode"].ToString(),
						AddressMonths = AddressMonths,

						//Homeowner Fields
						PropertyValue = homevalue,
						MortgageProvider = reader["MortgageProvider"].ToString(),
						MortgageBalance = MortgageBal,
						MortgagePayment = MortgagePayment,

						//Previous Address
						PreviousAddressLine1 = reader["PreviousAddressLine1"].ToString(),
						PreviousAddressLine3 = reader["PreviousAddressLine3"].ToString(),
						PreviousAddressPostcode = reader["PreviousAddressPostcode"].ToString(),
						PreviousMonthsAtAddress = PreviousMonthsAtAddress,
						PreviousHouseNumber = PreviousHouseNumber,

						//Previous Employment 
						PreviousEmployerName = reader["PreviousEmployerName"].ToString(),
						PreviousEmployerPostcode = reader["PreviousEmployerPostcode"].ToString(),
						PreviousEmployerAddressLine1 = reader["PreviousEmployerAddressLine1"].ToString(),
						PreviousEmployerAddressLine3 = reader["PreviousEmployerAddressLine3"].ToString(),
						PreviousMonthsAtEmployment = PreviousMonthsAtEmployment

				};

			}

			connection.Close();

			if (!valid.Any())
			{
				valid = ValidationCheck(model);
			}



		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Problem with building model: {0}", e.Message));
		}
	

		if (!valid.Any())
		{

			if (!valid.Any())
			{

				
				XElement xml;
				//build then XML and call to receiveXML 
				if (model.App1ResidentStatus == "A")
				{
						xml = new XElement("Applications",
						new XElement("broker_id", "10"),
						new XElement("Version", "2"),
						new XElement("app_source", "S"),
						new XElement("run_transform", "False"),
						new XElement("Application",
						new XElement("web_app_id", AppID),
						new XElement("amount", model.Proposal_Principal),
						new XElement("main_title", model.App1Customer_Title),
						new XElement("main_firstname", model.App1Customer_Forename),
						new XElement("main_surname", model.App1Customer_Surname),
						new XElement("main_MStatus", model.App1Customer_MartitalStatus),
						new XElement("main_dob", model.App1App1DOB),
						new XElement("RStatus", model.App1ResidentStatus),
						new XElement("main_home_tel", model.App1Customer_HomeNumber),
						new XElement("main_mobile_tel", model.App1Customer_MobileNumber),
						new XElement("main_emp_status", model.App1EmploymentStatus),
						new XElement("main_curr_postcode", model.Customer_Address_PostCode),
						new XElement("months_curr_add", model.AddressMonths),
						new XElement("main_curr_houseno", model.Customer_Address_HouseNumber),
						new XElement("main_curr_address1", model.Customer_Address_Line1),
						new XElement("main_curr_address3", model.Customer_Address_Town),
						new XElement("main_emp_name", model.App1EmployerName),
						new XElement("main_emp_postcode", model.App1EmployerPostCode),
						new XElement("main_emp_address1", model.App1EmployerAddressLine1),
						new XElement("months_curr_emp", model.App1EmployerMonths),
						new XElement("main_ann_income", model.App1Customer_Salary),
						new XElement("bank_name", model.App1BankName),
						new XElement("account_type", model.App1BankAccountType),
						new XElement("months_bank", model.App1BankAccountMonths),
						new XElement("bankAccountName", model.App1BankHolderName),
						new XElement("main_home_value", model.PropertyValue),
						new XElement("main_mortgage_provider", model.MortgageProvider),
						new XElement("main_mort_balance", model.MortgageBalance),
						new XElement("main_mort_payment", model.MortgagePayment),
						new XElement("prev1_address1", model.PreviousAddressLine1),
						new XElement("prev1_address3", model.PreviousAddressLine3),
						new XElement("prev1_postcode", model.PreviousAddressPostcode),
						new XElement("months_prev1_add", model.PreviousMonthsAtAddress),
						new XElement("prev1_houseno", model.PreviousHouseNumber),
						new XElement("main_pemp_address1", model.PreviousEmployerAddressLine1),
						new XElement("main_pemp_address3", model.PreviousEmployerAddressLine3),
						new XElement("main_pemp_postcode", model.PreviousAddressPostcode),
						new XElement("months_prev_emp", model.PreviousMonthsAtEmployment)
						));
				}
				else
				{
					xml = new XElement("Applications",
						new XElement("broker_id", "10"),
						new XElement("Version", "2"),
						new XElement("app_source", "S"),
						new XElement("run_transform", "False"),
						new XElement("Application",
						new XElement("web_app_id", AppID),
						new XElement("amount", model.Proposal_Principal),
						new XElement("main_title", model.App1Customer_Title),
						new XElement("main_firstname", model.App1Customer_Forename),
						new XElement("main_surname", model.App1Customer_Surname),
						new XElement("main_MStatus", model.App1Customer_MartitalStatus),
						new XElement("main_dob", model.App1App1DOB),
						new XElement("RStatus", model.App1ResidentStatus),
						new XElement("main_home_tel", model.App1Customer_HomeNumber),
						new XElement("main_mobile_tel", model.App1Customer_MobileNumber),
						new XElement("main_emp_status", model.App1EmploymentStatus),
						new XElement("main_curr_postcode", model.Customer_Address_PostCode),
						new XElement("months_curr_add", model.AddressMonths),
						new XElement("main_curr_houseno", model.Customer_Address_HouseNumber),
						new XElement("main_curr_address1", model.Customer_Address_Line1),
						new XElement("main_curr_address3", model.Customer_Address_Town),
						new XElement("main_emp_name", model.App1EmployerName),
						new XElement("main_emp_postcode", model.App1EmployerPostCode),
						new XElement("main_emp_address1", model.App1EmployerAddressLine1),
						new XElement("months_curr_emp", model.App1EmployerMonths),
						new XElement("main_ann_income", model.App1Customer_Salary),
						new XElement("bank_name", model.App1BankName),
						new XElement("account_type", model.App1BankAccountType),
						new XElement("months_bank", model.App1BankAccountMonths),
						new XElement("bankAccountName", model.App1BankHolderName),
						new XElement("prev1_address1", model.PreviousAddressLine1),
						new XElement("prev1_address3", model.PreviousAddressLine3),
						new XElement("prev1_postcode", model.PreviousAddressPostcode),
						new XElement("months_prev1_add", model.PreviousMonthsAtAddress),
						new XElement("prev1_houseno", model.PreviousHouseNumber),
						new XElement("main_pemp_address1", model.PreviousEmployerAddressLine1),
						new XElement("main_pemp_address3", model.PreviousEmployerAddressLine3),
						new XElement("main_pemp_postcode", model.PreviousAddressPostcode),
						new XElement("months_prev_emp", model.PreviousMonthsAtEmployment)
						));
				}

				

				var xmlsubmission = client.receiveXML(xml.ToString());

				XmlDocument myXml = new XmlDocument();
				XPathNavigator xNav = myXml.CreateNavigator();
				XmlSerializer xn = new XmlSerializer(xmlsubmission.GetType());
				using (var xs = xNav.AppendChild())
				{
					xn.Serialize(xs, xmlsubmission);
				}
				HttpContext.Current.Response.ContentType = "text/xml";
				HttpContext.Current.Response.Write(myXml.OuterXml);
				HttpContext.Current.Response.End();



			}
			else
			{
				string errors = "0| ";
				foreach (var item in valid)
				{
					errors += string.Format("{0} ", item);
				}
				Response.Write(errors);
			}
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("0| ");

			foreach (var item in valid)
			{
				sb.Append(string.Format("{0}, ", item));
			}
			Response.Write(sb.ToString());
		}

	}


	private static List<string> ValidationCheck(ServiceModel model)
	{
		List<string> RequiredPropertyList = new List<string>();

		if (string.IsNullOrEmpty(model.App1Customer_Forename))
			RequiredPropertyList.Add("Customer Forename missing or not valid.");

		if (string.IsNullOrEmpty(model.App1Customer_Surname))
			RequiredPropertyList.Add("Customer Surname missing or not valid.");


		return RequiredPropertyList;
	}


	class BuildCustomData
	{
		ServiceModel _ServiceModel;

		public BuildCustomData(ServiceModel serviceModel)
		{
			_ServiceModel = serviceModel;
		}


	}

	class ServiceModel
	{
		//Main Information
		public int? CompanyID { get; set; }
		public int? AppID { get; set; }
		public int? Proposal_Product_DefID { get; set; }
		public int? Proposal_Principal { get; set; }

		//First Applicant Details
		public string App1Customer_Title { get; set; }
		public string App1Customer_Forename { get; set; }
		public string App1Customer_Surname { get; set; }
		public string App1App1DOB { get; set; }
		public string App1Customer_MartitalStatus { get; set; }
		public string App1Customer_MobileNumber { get; set; }
		public string App1Customer_HomeNumber { get; set; }
		public decimal? App1Customer_Salary { get; set; }
		public string App1EmployerName { get; set; }
		public string App1EmploymentStatus { get; set; }
		public string App1ResidentStatus { get; set; }
		public string App1EmployerPostCode { get; set; }
		public string App1EmployerAddressLine1 { get; set; }
		public int? App1EmployerMonths { get; set; }
		public string App1BankAccountType { get; set; }
		public string App1BankAccountNumber { get; set; }
		public int? App1BankAccountMonths { get; set; }
		public string App1BankHolderName { get; set; }
		public string App1BankName { get; set; }

		//App1 Main Address
		public string Customer_Address_HouseNumber { get; set; }
		public string Customer_Address_Line1 { get; set; }
		public string Customer_Address_Town { get; set; }
		public string Customer_Address_PostCode { get; set; }
		public string Customer_Address_Tenure { get; set; }
		public int? AddressYears { get; set; }
		public int? AddressMonths { get; set; }

		//App1 Employer Address
		public string Employer_Address_Line1 { get; set; }
		public string Employer_Address_Town { get; set; }
		public string Employer_Address_PostCode { get; set; }
		public int? EmployerAddressYears { get; set; }
		public int? EmployerAddressMonths { get; set; }

		//Homeowner Fields
		public int? PropertyValue { get; set; }
		public string MortgageProvider { get; set; }
		public int? MortgageBalance { get; set; }
		public int? MortgagePayment { get; set; }

		//Previous Address
		public string PreviousAddressPostcode { get; set; }
		public int? PreviousMonthsAtAddress { get; set; }
		public int? PreviousHouseNumber { get; set; }
		public string PreviousAddressLine1 { get; set; }
		public string PreviousAddressLine3 { get; set; }

		//Previous Employment 
		public string PreviousEmployerName { get; set; }
		public string PreviousEmployerPostcode { get; set; }
		public string PreviousEmployerAddressLine1 { get; set; }
		public string PreviousEmployerAddressLine3 { get; set; }
		public int? PreviousMonthsAtEmployment { get; set; }
		public decimal? PreviousApp1Salary { get; set; }
		public int? PreviousEmployerHouseNumber { get; set; }
	}
}

