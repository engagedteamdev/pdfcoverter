using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;
using System.Linq;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public partial class Default : Page
{

	private string AppID = HttpContext.Current.Request["AppID"];
	private string RequestType = HttpContext.Current.Request["RequestType"];
	private string CallID = HttpContext.Current.Request["CallID"];
	private string DataList = HttpContext.Current.Request["DataList"];


	class theAuth
	{

		public string username { get; set; }
		public string password { get; set; }

		//public string[] scopes { get; set; }

	}

	class theCustomer
	{
		public string token { get; set; }
		public bool dupe_check_data_list { get; set; }
		public bool dupe_check_everything { get; set; }
		public bool auto_queue { get; set; }
		public int queue_priority { get; set; }
		public bool confirmation_sms { get; set; }
		public bool confirmation_email { get; set; }
		public string title { get; set; }
		public string first_name { get; set; }
		public string middle_name { get; set; }
		public string last_name { get; set; }
		public string main_phone { get; set; }
		public string alternative_phone { get; set; }
		public string address1 { get; set; }
		public string address2 { get; set; }
		public string address3 { get; set; }
		public string city { get; set; }
		public string postcode { get; set; }
		public string source { get; set; }
		public int data_list { get; set; }
		public string best_time_to_call { get; set; }
		public int called_count { get; set; }
		public string email { get; set; }
		public string date_of_birth { get; set; }
		public string source_code { get; set; }
	}



	class theUpdateCustomer
	{
		public string token { get; set; }
		public int customer_id { get; set; }
		public string status { get; set; }
	}




	class theCallRecordings
	{
		public string token { get; set; }
		public string unique_id { get; set; }

	}




	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	public void Run()
	{

		try
		{

			theAuth data = new theAuth();
			data.username = "CoreCover";
			data.password = "v3ABYTLzAknBTr7W";

			string json = JsonConvert.SerializeObject(data);


			

			string PostURL = "https://api5.cnx1.uk/consumer/login";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(PostURL);


			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(PostURL, contentBody).Result;

			var responseContent = res.Content.ReadAsStringAsync().Result;

			var resptoken = JObject.Parse(responseContent);

			


			var token = resptoken["token"].ToString();


		

			if (token != null)
				{
					if (RequestType == "CreateCustomer")
					{
						CreateCustomer(token);
					}
					else if (RequestType == "CallRecording")
					{
						CallRecordings(token);
					}
					else if (RequestType == "UpdateCustomer")
					{
						UpdateCustomer(token);
					}
			}
			else
			{
				Response.Write("Token Unavailable");
			}



			client.Dispose();

        }
        catch (Exception e)
        {
			Response.Write(e.Message);
		}


	}


	public void CreateCustomer(string token)
	{


		try
		{

			theCustomer customer = new theCustomer();
			customer.token = "8Lx1jUwqIqq6B0J7u9kRSOyZJAUlLgrC6ptWmWWGOaS3Ga8ZRr";
			customer.dupe_check_data_list = true;
			customer.dupe_check_everything = false;
			customer.auto_queue = true;
			customer.queue_priority = 99;
			customer.confirmation_sms = false;
			customer.confirmation_email = false;
			
			customer.called_count = 0;



			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception e)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
			}

			SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwConnex where AppID = '{0}'", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{
				
				customer.title = reader["App1Title"].ToString();
				customer.first_name = reader["App1FirstName"].ToString();
				customer.middle_name = reader["App1MiddleNames"].ToString();
				customer.last_name = reader["App1Surname"].ToString();
				if (reader["App1MobileTelephone"].ToString() == "")
				{
					customer.main_phone = reader["App1HomeTelephone"].ToString();
				}
				else
				{
					customer.main_phone = reader["App1MobileTelephone"].ToString();
				}
				if (reader["App1HomeTelephone"].ToString() == "")
				{
					customer.alternative_phone = reader["App1MobileTelephone"].ToString();
				}
				else
				{
					customer.alternative_phone = reader["App1HomeTelephone"].ToString();
				}
				customer.address1 = reader["AddressLine1"].ToString();
				customer.address2 = reader["AddressLine2"].ToString();
				customer.address3 = "";
				customer.city = reader["AddressLine3"].ToString();
				customer.postcode = reader["AddressPostCode"].ToString();
				customer.source = reader["MediaCampaignName"].ToString();
				customer.best_time_to_call = reader["NextCallDate"].ToString();
				if (reader["App1EmailAddress"].ToString() == "")
				{
					customer.email = null;
                }
                else
                {
					customer.email = reader["App1EmailAddress"].ToString();

				}

				customer.data_list = Convert.ToInt32(DataList);
				customer.date_of_birth = reader["App1DOB"].ToString();
				customer.source_code = reader["AppID"].ToString();


			}



			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;


			string json = JsonConvert.SerializeObject(customer, settings);

			


			string PostURL = "https://api5.cnx1.uk/customer/create";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(PostURL);
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
			client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; " + "Windows NT 5.2; .NET CLR 1.0.3705;)");

			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			

			var res = client.PostAsync(PostURL, contentBody).Result;

			

			var responseContent = res.Content.ReadAsStringAsync().Result;

			

			var respcustomer = JObject.Parse(responseContent);

			


			var sucess = respcustomer["success"].ToString();
			var message = respcustomer["message"].ToString();

			if (sucess == "True")
			{
				var customerid = respcustomer["customer_id"].ToString();

				SqlCommand saveCustomerID = new SqlCommand();
				saveCustomerID.Connection = connection;
				saveCustomerID.CommandText = string.Format("update tblapplications set ConnexCustomerID = '" + customerid + "' where AppID = '" + AppID + "'");
				saveCustomerID.ExecuteNonQuery();
				Response.Write("1|" + customerid + "");
			}
			else
			{
				
				Response.Write("1|" + message + "");


			}


			
            SqlCommand insertXML = new SqlCommand();
            insertXML.Connection = connection;
            insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Response From Connex','" + message.ToString() + "',getDate())");
            insertXML.ExecuteNonQuery();

			SqlCommand insertXML1 = new SqlCommand();
			insertXML1.Connection = connection;
			insertXML1.CommandText = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Data Sent To Connex','" + json.ToString() + "',getDate())";
			insertXML1.ExecuteNonQuery();



			client.Dispose();

		}catch(Exception e)
        {
		
			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception y)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", y.Message));
			}


			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Error Response From Connex','" + e.Message + "',getDate())");
			insertXML.ExecuteNonQuery();
		}


}

	public void UpdateCustomer(string token)
	{


		try
		{

			theUpdateCustomer customerupdate = new theUpdateCustomer();
			customerupdate.token = "8Lx1jUwqIqq6B0J7u9kRSOyZJAUlLgrC6ptWmWWGOaS3Ga8ZRr";




			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception e)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
			}

			SqlCommand myCommand = new SqlCommand(string.Format("Select ConnexCustomerID FROM tblapplications where AppID = '{0}'", AppID), connection);

			var reader = myCommand.ExecuteReader();

			while (reader.Read())
			{

				customerupdate.customer_id = Convert.ToInt32(reader["ConnexCustomerID"].ToString());


			}
			customerupdate.status = "DNC";


			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;


			string json = JsonConvert.SerializeObject(customerupdate, settings);




			string PostURL = "https://api5.cnx1.uk/customer/update";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(PostURL);
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
			client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; " + "Windows NT 5.2; .NET CLR 1.0.3705;)");

			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");



			var res = client.PostAsync(PostURL, contentBody).Result;



			var responseContent = res.Content.ReadAsStringAsync().Result;



			var respcustomer = JObject.Parse(responseContent);



			var sucess = respcustomer["success"].ToString();
			var message = respcustomer["message"].ToString();

			if (sucess == "True")
			{
				var customerid = respcustomer["message"].ToString();

				Response.Write("1|" + customerid + "");
			}
			else
			{

				Response.Write("1|" + message + "");


			}



			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Update Response From Connex','" + message.ToString() + "',getDate())");
			insertXML.ExecuteNonQuery();



			client.Dispose();

		}
		catch (Exception e)
		{

			SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

			try
			{
				connection.Open();
			}
			catch (Exception y)
			{
				throw new Exception(string.Format("Failed to open connection to server. Message: {0}", y.Message));
			}


			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Update Error Response From Connex','" + e.Message + "',getDate())");
			insertXML.ExecuteNonQuery();
		}


	}


	public void CallRecordings(string token)
	{

		try
		{

			theCallRecordings callrecording = new theCallRecordings();
			callrecording.token = "8Lx1jUwqIqq6B0J7u9kRSOyZJAUlLgrC6ptWmWWGOaS3Ga8ZRr"; 
			callrecording.unique_id = CallID;


			string json = JsonConvert.SerializeObject(callrecording);

		

			string PostURL = "https://api5.cnx1.uk/call/recording/get_url";
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(PostURL);
			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
			client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; " + "Windows NT 5.2; .NET CLR 1.0.3705;)");

			HttpContent contentBody = new StringContent(json);
			contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

			var res = client.PostAsync(PostURL, contentBody).Result;

			var responseContent = res.Content.ReadAsStringAsync().Result;

			var respcallrecordings = JObject.Parse(responseContent);

			

			var sucess = respcallrecordings["success"].ToString();
			var message = respcallrecordings["message"].ToString();

			if (sucess == "True")
			{
				var url = respcallrecordings["data"]["url"].ToString();

				Response.Write("" + url + "");
			}
			else
			{

				Response.Write("0|" + message + "");


			}


			//var detals = respcallrecordings["totalResults"]["details"][0].ToString();

			//var objdetals = JObject.Parse(detals);


			//foreach (var x in objdetals)
			//{

			//	string name = x.Key;

			//		Response.Write(name);


			//}
			//Response.End();

			client.Dispose();

		}catch(Exception e)
        {
			Response.Write(e.Message);
		}


	}

}
