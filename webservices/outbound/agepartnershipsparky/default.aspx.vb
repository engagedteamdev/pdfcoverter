﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Web.Script.Serialization




Partial Class CSCTransfers
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private AppID As String = HttpContext.Current.Request.QueryString("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignIDOutbound")
    Private strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignReference")
    Private strTransferUserID As String = HttpContext.Current.Request.QueryString("TransferUserID")
    Private intHotkeyUserID As String = HttpContext.Current.Request.QueryString("HotkeyUserID")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request.QueryString("MediaCampaignScheduleID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XMLDocument = New XMLDocument
    Private strProductType As String = HttpContext.Current.Request("frmProductType"), strCustomerEmailID As String = ""
    Private strErrorMessage As String = ""
	Private strMediaCampID as string = HttpContext.Current.Request.QueryString("MediaCampaignID")


    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
		leadSend()
    End Sub

    Public Sub leadSend()

		ServicePointManager.Expect100Continue = true
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
      
            Dim App1Title As String = ""
            Dim App1FirstName As String = ""
            Dim App1Surname As String = ""
            Dim App1EmailAddress As String = ""
            Dim App1HomeTelephone As String = ""
            Dim App1MobileTelephone As String = ""
            Dim AddressLine1 As String = ""
            Dim AddressPostCode As String = ""
            Dim Telephone As String = ""
            

      
            Dim strRequestURL = "https://engaged-solutions-equityrelease:LqFG3uGKueweEubF@data.agepartnership.co.uk/rest/eqr/customer/"
			Dim myUri As Uri = New Uri(strRequestURL)

			Dim strSQL As String = "select * from vwagepartnership where AppID='" & AppID & "'"
			 Dim dtb1 As New DataTable
	         using DBConnection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringSparky").ConnectionString)					 
	         DBConnection.open()			
	         Using dad As New SqlDataAdapter(strSQL,DBConnection)
	         dad.Fill(dtb1)
	         End Using
	         DBConnection.close()
	         End Using
			 
	         If (dtb1.Rows.Count > 0) Then   
    
			    For Each Row As DataRow In dtb1.Rows 
	
                App1Title = Row.Item("App1Title").ToString()
                App1FirstName = Row.Item("App1Firstname").ToString()
                App1Surname = Row.Item("App1Surname").ToString()
                App1EmailAddress = Row.Item("App1EmailAddress").ToString()
                App1HomeTelephone = Row.Item("App1HomeTelephone").ToString()
                App1MobileTelephone = Row.Item("App1MobileTelephone").ToString()
				If(isDBNull(App1MobileTelephone) Or App1MobileTelephone = "")Then 
					Telephone = Row.Item("App1HomeTelephone").ToString()
				Else 
					Telephone = Row.Item("App1MobileTelephone").ToString()
				End If
                AddressLine1 = Row.Item("AddressLine1").ToString()
                AddressPostCode = Row.Item("AddressPostcode").ToString()
				Next
			End If
		

              
			
           Dim strMessage As String = "Invalid "

        If(strMessage = "Invalid ")Then

            

            Dim strPost As String = "{""title"":""" & App1Title & """,""forename"":""" & App1FirstName & """,""surname"":""" & App1Surname & """,""email"":""" & App1EmailAddress & """,""tel_day"":""" & Telephone & """,""address1"":""" & AddressLine1 & """,""postcode"":""" & AddressPostCode & """}"
			
      
		
			    Dim myRequest As HttpWebRequest = PostJSON(strPost)
				Dim response As String = GetResponse(myRequest)
				
				Dim json As JObject = JObject.Parse(response)

				Dim reference As String = json.SelectToken("transaction_id")
				Dim status As String = json.SelectToken("status")

				

				If(status = "1")Then 
					HttpContext.Current.Response.Write("1|" & reference)
				
				Else 
					HttpContext.Current.Response.Write("0|Error")
				End If

				HttpContext.Current.Response.End()
                 

        Else
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & encodeURL("Application Declined by Age Partnership: " & strMessage))
			'postEmail("", "dev.team@engagedcrm.co.uk", "Age Partnership Lead Failed to Send " & AppID & "", "Application Declined by Age Partnership", True, "", True)
            
        End If 
	      
    End Sub
	
    Private Function PostJSON(ByVal JsonData As String) As HttpWebRequest
        Dim objhttpWebRequest As HttpWebRequest
        Try
            Dim httpWebRequest = DirectCast(WebRequest.Create("https://engaged-solutions-equityrelease:LqFG3uGKueweEubF@data.agepartnership.co.uk/rest/eqr/customer/"), HttpWebRequest)
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "POST"
			httpWebRequest.Credentials = New NetworkCredential("engaged-solutions-equityrelease","LqFG3uGKueweEubF")
            Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())


                streamWriter.Write(JsonData)
                streamWriter.Flush()
                streamWriter.Close()
            End Using

            objhttpWebRequest = httpWebRequest

        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message)

            Return Nothing
        End Try

        Return objhttpWebRequest

    End Function

    Private Function GetResponse(ByVal httpWebRequest As HttpWebRequest) As String
        Dim strResponse As String = "Bad Request:400"
        Try
            Dim httpResponse = DirectCast(httpWebRequest.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                Dim result = streamReader.ReadToEnd()

                strResponse = result.ToString()
            End Using
        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message)

            Return ex.Message
        End Try

        Return strResponse

    End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES('1127', " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub



End Class