using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;
using System.Linq;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Activities.Expressions;
using System.Data;

public partial class Default : Page
{

	private string AppID = HttpContext.Current.Request["AppID"];
	private string PolicyID = HttpContext.Current.Request["PolicyID"];
	private string Request = HttpContext.Current.Request["Request"];
	string UserID = HttpContext.Current.Request["UserID"];



	class theAuth
	{

		public string username { get; set; }
		public string password { get; set; }

	}

	class theData
	{
		public string owner { get; set; }
		public theCustomers[] customers { get; set; }
		public theProducts[] products { get; set; }
	}

	class theCustomers
	{
		public string referenceId { get; set; }
		public string name { get; set; }
		public string surname { get; set; }
		public string title { get; set; }
		public string gender { get; set; }
		public string dateOfBirth { get; set; }
		public bool smoker { get; set; }
		public string occupation { get; set; }
		public string email { get; set; }
		public string maritalStatus { get; set; }

		public theContactDetails contactDetails { get; set; }
	}


	class theContactDetails
	{
		public string telephoneNumber { get; set; }
		public theAddress address { get; set; }

	}

	class theAddress
	{
		public string line1 { get; set; }
		public string line2 { get; set; }
		public string town { get; set; }
		public string county { get; set; }
		public string postcode { get; set; }

	}

	class theProducts
	{
		public string referenceId { get; set; }
		public string productTypeId { get; set; }
		public theQuestions[] questions { get; set; }
		

	}

	public partial class theQuestions
	{
		public string name { get; set; }
		public string[] answers { get; set; }
	}

	class thelifeAssured
	{
		public string refersTo { get; set; }
		public bool waiverOfPremium { get; set; }
		public bool totalPermanentDisability { get; set; }
	}



	class ServiceModel
	{
		public int AppID { get; set; }
		public string App1FirstName { get; set; }
		public string App1Surname { get; set; }
		public string App1Title { get; set; }
		public string App1Gender { get; set; }
		public string App1DOB { get; set; }
		public string App1Smoker { get; set; }
		public string App1Occupation { get; set; }
		public string App1EmailAddress { get; set; }
		public string App1MobileNumber { get; set; }
		public string App1MaritalStatus { get; set; }
		public string App2FirstName { get; set; }
		public string App2Surname { get; set; }
		public string App2Title { get; set; }
		public string App2Gender { get; set; }
		public string App2DOB { get; set; }
		public string App2Smoker { get; set; }
		public string App2Occupation { get; set; }
		public string App2EmailAddress { get; set; }
		public string App2MobileNumber { get; set; }
		public string App2MaritalStatus { get; set; }

		public string App1AddressLine1 { get; set; }

		public string App1AddressLine2 { get; set; }

		public string App1AddressTown { get; set; }

		public string App1AddressCounty { get; set; }

		public string App1AddressPostCode { get; set; }
		public string App2AddressLine1 { get; set; }

		public string App2AddressLine2 { get; set; }

		public string App2AddressTown { get; set; }

		public string App2AddressCounty { get; set; }

		public string App2AddressPostCode { get; set; }

	}



	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	public void Run()
	{

		try
		{




            theAuth data = new theAuth();
            data.username = "CoreCoverAPI";
            data.password = "Tuesday16";

            string json = JsonConvert.SerializeObject(data);




            string PostURL = "https://admin-compare-int-01.underwriteme.co.uk/api/auth";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(PostURL);


            HttpContent contentBody = new StringContent(json);
            contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var res = client.PostAsync(PostURL, contentBody).Result;

            var responseContent = res.Content.ReadAsStringAsync().Result;

            var resptoken = JObject.Parse(responseContent);

            var token = resptoken["access_token"].ToString();
			//Response.Write(token);
			//Response.End();


			if (token != null)
            {

				sumbitQuote(token);

            }
            else
            {
                Response.Write("Token Unavailable");
            }



            //client.Dispose();

        }
		catch (Exception e)
		{
			Response.Write(e.Message);
		}


	}

	public void sumbitQuote(string token)
	{




		ServiceModel model = new ServiceModel();

		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}

		SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwunderwriteme where AppID = '{0}'", AppID), connection);

		var reader = myCommand.ExecuteReader();

		while (reader.Read())
		{

			model.App1Title = reader["App1Title"].ToString();
			model.App1FirstName = reader["App1FirstName"].ToString();
			model.App1Surname = reader["App1Surname"].ToString();
			model.App1Gender = reader["App1Sex"].ToString();
			model.App1Smoker = reader["App1Smoker"].ToString();
			model.App1Occupation = reader["App1Occupation"].ToString();
			model.App1DOB = reader["App1DOB"].ToString();
			model.App1MaritalStatus = reader["App1MaritalStatus"].ToString();
			model.App1EmailAddress = reader["App1EmailAddress"].ToString();
			model.App1MobileNumber = reader["App1MobileTelephone"].ToString();
			model.App1AddressLine1 = reader["AddressLine1"].ToString();
			model.App1AddressLine2 = reader["AddressLine2"].ToString();
			model.App1AddressTown = reader["AddressLine3"].ToString();
			model.App1AddressCounty = reader["AddressCounty"].ToString();
			model.App1AddressPostCode = reader["AddressPostCode"].ToString();

			model.App2Title = reader["App2Title"].ToString();
			model.App2FirstName = reader["App2FirstName"].ToString();
			model.App2Surname = reader["App2Surname"].ToString();
			model.App2Gender = reader["App2Sex"].ToString();
			model.App2Smoker = reader["App2Smoker"].ToString();
			model.App2Occupation = reader["App2Occupation"].ToString();
			model.App2DOB = reader["App2DOB"].ToString();
			model.App2MaritalStatus = reader["App2MaritalStatus"].ToString();
			model.App2EmailAddress = reader["App2EmailAddress"].ToString();
			model.App2MobileNumber = reader["App2MobileTelephone"].ToString();
			model.App2AddressLine1 = reader["PartnerAddressLine1"].ToString();
			model.App2AddressLine2 = reader["PartnerAddressLine2"].ToString();
			model.App2AddressTown = reader["PartnerAddressLine3"].ToString();
			model.App2AddressCounty = reader["PartnerAddressCounty"].ToString();
			model.App2AddressPostCode = reader["PartnerAddressPostCode"].ToString();

		}

		

		theData data = new theData();


		string Username = "";


		SqlCommand myCommandUser = new SqlCommand(string.Format("SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'UnderwriteMeUsername%' AND CompanyID = '1181' AND UserID = '{0}'", UserID), connection);

		var readerUser = myCommandUser.ExecuteReader();

		while (readerUser.Read())
		{
			Username = readerUser["StoredUserValue"].ToString();

		}


		data.owner = Username;



	
		

		SqlCommand cmdbenefits = new SqlCommand(string.Format("Select * FROM vwunderwritemebenefits where AppID = '{0}' and PolicyID = {1}", AppID, PolicyID), connection);
		SqlDataAdapter da = new SqlDataAdapter(cmdbenefits);
		DataTable dt = new DataTable();
		da.Fill(dt);

		List<theProducts> products = new List<theProducts>();
		List<theQuestions> questions = new List<theQuestions>();
		List<thelifeAssured> LifeAssured = new List<thelifeAssured>();
		int BenefitAmount = 0, CICAmount = 0, LifeOrCIC = 0, totalamount = 0;
		bool WaiverOfPremium = false, TotalPermanentDisability = false;
		int Lives = 0; 
		foreach (DataRow dr in dt.Rows)
		{

			BenefitAmount = Convert.ToInt32(dr["BenefitAmount"].ToString());
			CICAmount = Convert.ToInt32(dr["CICAmount"].ToString());
			LifeOrCIC = Convert.ToInt32(dr["LifeOrCICAmount"].ToString());

			totalamount = (BenefitAmount + CICAmount + LifeOrCIC);


			string[] durationBasisAns = { "NUMBER_OF_YEARS" };
			string[] coverPeriodAns = { dr["Term"].ToString() };
			string[] coverBasisAns = { "LEVEL" };
			string[] coverAmountAns = { totalamount.ToString() };

			string[] whoForAns = { dr["LivesAssured"].ToString() };
			string[] deferredPeriodInWeeksAns = { dr["DeferredPeriod"].ToString() };


			string productType = "";

			if (dr["CoverType"].ToString() == "Income Protection")
			{
				productType = "INCOME_PROTECTION";
			}
			else if (dr["CoverType"].ToString() == "Life Cover Only")
			{
				productType = "TERM";
			}
			else if (dr["CoverType"].ToString() == "Critical Illness Only")
			{
				productType = "CRITICAL_ILLNESS";
			}
			else if (dr["CoverType"].ToString() == "Life Cover With Critical Illness")
			{
				productType = "CRITICAL_ILLNESS_WITH_LIFE_COVER";
			}
			else if (dr["CoverType"].ToString() == "Income Protection")
			{
				productType = "INCOME_PROTECTION";
			}
			else
			{
				productType = "TERM";
			}



			questions.Add(
			new theQuestions
			{
				name = "durationBasis",
				answers = durationBasisAns
			}
			);

			questions.Add(
			new theQuestions
			{
				name = "coverBasis",
				answers = coverBasisAns
			}
			);

			questions.Add(
			new theQuestions
			{
				name = "coverPeriod",
				answers = coverPeriodAns
			}
			);

			questions.Add(
			new theQuestions
			{
				name = "coverAmount",
				answers = coverAmountAns
			}
			);

			if (dr["LivesAssured"].ToString() == "Joint")
			{

				string[] ans = { "1", "2" };
				questions.Add(
				new theQuestions
				{
					name = "whoFor",
					answers = ans
				}
				); ; ;
			}
			else
			{
				questions.Add(
				new theQuestions
				{
					name = "whoFor",
					answers = whoForAns
				}
				);
			}

			if (dr["CoverType"].ToString() == "Income Protection") { 
					questions.Add(
					new theQuestions
					{
						name = "deferredPeriod",
						answers = deferredPeriodInWeeksAns
					}
					);
			}

			products.Add(
				new theProducts
				{
					referenceId = dr["BenefitID"].ToString(),
					productTypeId = productType,
					questions = questions.ToArray()
					
				}
			);

			if (dr["LivesAssured"].ToString() == "Joint" || dr["LivesAssured"].ToString() == "2")
			{
				Lives = Lives + 1;
			}


				questions.Clear();

		}

		if (model.App2FirstName != "" && Lives > 0)
		{



			theCustomers[] customer = new theCustomers[2];
			customer[0] = new theCustomers();
			customer[0].referenceId = "1";
			customer[0].title = model.App1Title.ToUpper();
			customer[0].name = model.App1FirstName;
			customer[0].surname = model.App1Surname;
			customer[0].gender = model.App1Gender;
			if (model.App1Smoker == "Y")
			{
				customer[0].smoker = true;
			}
			else
			{
				customer[0].smoker = false;
			}
			customer[0].occupation = model.App1Occupation;
			customer[0].dateOfBirth = model.App1DOB;
			customer[0].maritalStatus = model.App1MaritalStatus;
			customer[0].email = model.App1EmailAddress;
			customer[0].contactDetails = new theContactDetails();
			customer[0].contactDetails.telephoneNumber = model.App1MobileNumber;
			customer[0].contactDetails.address = new theAddress();
			customer[0].contactDetails.address.line1 = model.App1AddressLine1;
			customer[0].contactDetails.address.line2 = model.App1AddressLine2;
			customer[0].contactDetails.address.town = model.App1AddressTown;
			customer[0].contactDetails.address.county = model.App1AddressCounty;
			customer[0].contactDetails.address.postcode = model.App1AddressPostCode;


			customer[1] = new theCustomers();
			customer[1].referenceId = "2";
			customer[1].title = model.App2Title.ToUpper();
			customer[1].name = model.App2FirstName;
			customer[1].surname = model.App2Surname;
			customer[1].gender = model.App2Gender;
			if (model.App2Smoker == "Y")
			{
				customer[1].smoker = true;
			}
			else
			{
				customer[1].smoker = false;
			}
			customer[1].occupation = model.App2Occupation;
			customer[1].dateOfBirth = model.App2DOB;
			customer[1].maritalStatus = model.App2MaritalStatus;
			customer[1].email = model.App2EmailAddress;
			customer[1].contactDetails = new theContactDetails();
			customer[1].contactDetails.telephoneNumber = model.App2MobileNumber;
			customer[1].contactDetails.address = new theAddress();
			customer[1].contactDetails.address.line1 = model.App2AddressLine1;
			customer[1].contactDetails.address.line2 = model.App2AddressLine2;
			customer[1].contactDetails.address.town = model.App2AddressTown;
			customer[1].contactDetails.address.county = model.App2AddressCounty;
			customer[1].contactDetails.address.postcode = model.App2AddressPostCode;

			data.customers = customer;
		}
		else
		{

			theCustomers[] customer = new theCustomers[1];
			customer[0] = new theCustomers();
			customer[0].referenceId = "1";
			customer[0].title = model.App1Title.ToUpper();
			customer[0].name = model.App1FirstName;
			customer[0].surname = model.App1Surname;
			customer[0].gender = model.App1Gender;
			if (model.App1Smoker == "Y")
			{
				customer[0].smoker = true;
			}
			else
			{
				customer[0].smoker = false;
			}
			customer[0].occupation = model.App1Occupation;
			customer[0].dateOfBirth = model.App1DOB;
			customer[0].maritalStatus = model.App1MaritalStatus;
			customer[0].email = model.App1EmailAddress;
			customer[0].contactDetails = new theContactDetails();
			customer[0].contactDetails.telephoneNumber = model.App1MobileNumber;
			customer[0].contactDetails.address = new theAddress();
			customer[0].contactDetails.address.line1 = model.App1AddressLine1;
			customer[0].contactDetails.address.line2 = model.App1AddressLine2;
			customer[0].contactDetails.address.town = model.App1AddressTown;
			customer[0].contactDetails.address.county = model.App1AddressCounty;
			customer[0].contactDetails.address.postcode = model.App1AddressPostCode;

			data.customers = customer;
		}



		data.products = products.ToArray();
		

		string json = JsonConvert.SerializeObject(data);
	

		try {
			
			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Data Sent To Underwrite Me','" + json.Replace("'", "''") + "',getDate())";
			insertXML.ExecuteNonQuery();
        }
        catch
        {

        }

		string PostURL = "https://admin-compare-int-01.underwriteme.co.uk/api/v2/application/pre-populate";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(PostURL);
		client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);


		HttpContent contentBody = new StringContent(json);
		contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var res = client.PostAsync(PostURL, contentBody).Result;

		var responseContent = res.Content.ReadAsStringAsync().Result;

		var respobj = JObject.Parse(responseContent);

		


		var id = "";
		
		try
		{
			id = respobj["id"].ToString();

        }
        catch
        {
			id = "";

		}

		if(id != "") { 

			SqlCommand updateID = new SqlCommand();
			updateID.Connection = connection;
			updateID.CommandText = string.Format("update tblpolicy set UnderwriteMeID = '" + id + "' where ID = '" + PolicyID + "'");
			updateID.ExecuteNonQuery();

			Response.Write(id);

			SqlCommand insertXML = new SqlCommand();
			insertXML.Connection = connection;
			insertXML.CommandText = string.Format("INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Underwrite Me Response','" + id + "',getDate())");
			insertXML.ExecuteNonQuery();
		}
        else
        {

			var errorMessage = "";

			try
			{
				errorMessage = respobj["errorMessage"].ToString();

			}
			catch
			{

				errorMessage = respobj.ToString();


			}

			try
			{
				SqlCommand insertXML = new SqlCommand();
				insertXML.Connection = connection;
				insertXML.CommandText =  "INSERT INTO tblxmlreceived (XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) values ('80.244.190.5','" + AppID + "','0','Underwrite Me Response','" + errorMessage.Replace("'", "''") + "',getDate())";
				insertXML.ExecuteNonQuery();
			}
			catch
			{

			}
			if(errorMessage.Contains("Unexpected Server Error."))
            {

				if (model.App2FirstName != "" && Lives > 0)
				{

					if (model.App2FirstName == "")
					{

						Response.Write("First Name Required");

					}
					else if (model.App2Surname == "")
					{
						Response.Write("Surname Required");
					}
					else if (model.App1Title == "")
					{
						Response.Write("Title Required");
					}
					else if (model.App2Gender == "")
					{
						Response.Write("Gender Required");
					}
					else if (model.App2DOB == "")
					{
						Response.Write("Date Of Birth Required");
					}
					else if (model.App2Smoker == "")
					{
						Response.Write("Smoker Status Required");
					}
					else if (model.App2Occupation == "")
					{
						Response.Write("Occupation Required");
					}
					else if (model.App2EmailAddress == "")
					{
						Response.Write("Email Address Required");
					}
					else if (model.App2MaritalStatus == "")
					{
						Response.Write("Martial Status Required");
					}
					else if (model.App2MobileNumber == "")
					{
						Response.Write("Mobile Number Required");
					}
					else if (model.App1AddressLine1 == "")
					{
						Response.Write("Address Line 1 Required");
					}
					else if (model.App2AddressTown == "")
					{
						Response.Write("Town Required");
					}
					else if (model.App2AddressCounty == "")
					{
						Response.Write("County Required");
					}
					else if (model.App2AddressPostCode == "")
					{
						Response.Write("Postcode Required");
					}
					else
					{
						Response.Write("" + errorMessage + " - Please check application details. For example Telphone numbers, Email Address and Address Details are required");
					}

                }
                else
                {
					if (model.App1FirstName == "")
					{

						Response.Write("First Name Required");

					}
					else if (model.App1Surname == "")
					{
						Response.Write("Surname Required");
					}
					else if (model.App1Title == "")
					{
						Response.Write("Title Required");
					}
					else if (model.App1Gender == "")
					{
						Response.Write("Gender Required");
					}
					else if (model.App1DOB == "")
					{
						Response.Write("Date Of Birth Required");
					}
					else if (model.App1Smoker == "")
					{
						Response.Write("Smoker Status Required");
					}
					else if (model.App1Occupation == "")
					{
						Response.Write("Occupation Required");
					}
					else if (model.App1EmailAddress == "")
					{
						Response.Write("Email Address Required");
					}
					else if (model.App1MaritalStatus == "")
					{
						Response.Write("Martial Status Required");
					}
					else if (model.App1MobileNumber == "")
					{
						Response.Write("Mobile Number Required");
					}
					else if (model.App1AddressLine1 == "")
					{
						Response.Write("Address Line 1 Required");
					}
					else if (model.App1AddressTown == "")
					{
						Response.Write("Town Required");
					}
					else if (model.App1AddressCounty == "")
					{
						Response.Write("County Required");
					}
					else if (model.App1AddressPostCode == "")
					{
						Response.Write("Postcode Required");
					}
					else
					{
						Response.Write("" + errorMessage + " - Please check application details. For example Telphone numbers, Email Address and Address Details are required");
					}

				}
				
			}
            else
            {
				Response.Write(errorMessage);
			}
			
		}
		

	}
}