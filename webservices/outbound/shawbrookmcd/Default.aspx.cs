using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;
using RestSharp;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

public partial class Default : Page
{
    private string AppID = HttpContext.Current.Request["AppID"];

    protected void Page_Load(object sender, EventArgs e)
    {
        httpPost();
    }

    public void httpPost()
    {
        try
        {
            Run();
        }
        catch (Exception e)
        {

            throw;
        }
    }


    private void Run()
    {
        int appId = int.Parse(AppID);
        string formattedErrorDisplay = "";

        QuotationSoftSearchDetail search = new QuotationSoftSearchDetail();
        ShawbrookLogic sl = new ShawbrookLogic();
        vwXMLShawbrook serviceModel = null;
        SoftSearchResponse softSearchRespose = null;

        List<string> inValid = new List<string>();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }

        try
        {
            SqlCommand myCommand = new SqlCommand(string.Format("Select * FROM vwXMLShawbrookMCD where AppId = {0}", appId), connection);

            var reader = myCommand.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    serviceModel = new vwXMLShawbrook(reader);


                    sl.ValidateModel(serviceModel, ref inValid);

                    #region Check Empty Properties
                    //var serviceModelProperties = serviceModel.GetType().GetProperties();

                    //foreach (PropertyInfo property in serviceModelProperties)
                    //{
                    //    if (property.CanRead)
                    //    {
                    //        var value = property.GetValue(serviceModel, null);

                    //        if (value != null)
                    //        {
                    //            if (string.IsNullOrEmpty(value.ToString()) || string.IsNullOrWhiteSpace(value.ToString()))
                    //            {
                    //                inValid.Add(string.Format("Property: {0} does not contain a value", property.Name));
                    //            }
                    //        }
                    //        else
                    //        {
                    //            inValid.Add(string.Format("Property: {0} does not contain a value", property.Name));
                    //        }
                    //    }
                    //}
                    #endregion

                    if (!inValid.Any())
                    {
                        #region Create Soft Search Model
                        search = sl.CreateSoftSearch(serviceModel, ref inValid);


                        #endregion

                        #region Call Shawbrook API
                        var authCode = new BrokerUserEmailClass();
						
						string userName = Common.getAnyFieldFromUserStore("shawbrookUsername", Config.DefaultUserID);
                        authCode.BrokerUserEmail = userName;

                        var dpaToken = JsonConvert.SerializeObject(authCode);
                        //var messageContentToken = new StringContent(dpaToken, Encoding.UTF8, "application/json");

                        if (!inValid.Any())
                        {
                            #region Get DPA Key

                            RestClient client = sl.CreateRestClient();
							var request = new RestRequest("consentforms", Method.POST);

                            client.AddDefaultHeader("Content-Type", "application/json");
                            var response = client.Execute(request);

                           // Response.Write(response.ErrorMessage);

                            BrokerKeyModel result = new BrokerKeyModel();

                            try
                            {
                                result = JsonConvert.DeserializeObject<BrokerKeyModel>(response.Content);
                            }
                            catch (Exception e)
                            {
								throw new Exception(string.Format("Broker key issue: {0} Error: , DPA Token: {1}", e.Message, response.Content));
                            }

                            #endregion

                            #region Post To Shawbrook
                            search.DPAAuthorisationToken = result.Key;

                         

                            var searchJson = JsonConvert.SerializeObject(search);
                            // HttpContext.Current.Response.Write(JsonConvert.SerializeObject(searchJson));
                            // HttpContext.Current.Response.End();

                            var doc = JsonConvert.DeserializeXmlNode(searchJson, "root");


                           

                            client = sl.CreateRestClient();

                            request = new RestRequest("quotation/quotationsoftsearched", Method.POST);
                            request.AddParameter("text/json", searchJson, ParameterType.RequestBody);


                            response = client.Execute(request);


                            BadRequestResponse errorResult = new BadRequestResponse();

                            try
                            {
                                errorResult = JsonConvert.DeserializeObject<BadRequestResponse>(response.Content);
                            }
                            catch (Exception)
                            {                                
                                throw new Exception("Bad Request error");
                            }

                            #region Post Error Check
                           if (!string.IsNullOrEmpty(errorResult.Message) && errorResult.ModelState == null)
                            {
                                formattedErrorDisplay = Common.encodeURL(string.Format("{0}, ", errorResult.Message));
                            }

                            if (errorResult.ModelState != null)
                            {
                                formattedErrorDisplay = sl.HandlePostErrors(errorResult, ref inValid);
                            }

                            if (string.IsNullOrEmpty(formattedErrorDisplay))
                            {
                                softSearchRespose = new SoftSearchResponse();

                                try
                                {
                                    softSearchRespose = JsonConvert.DeserializeObject<SoftSearchResponse>(response.Content);
                                }
                                catch (Exception)
                                {
                                    new Exception("Soft search response error");
                                }

                                if (!string.IsNullOrEmpty(softSearchRespose.InfoMessage))
                                {
                                    inValid.Add(string.Format("{0}, ", softSearchRespose.InfoMessage));
                                }
                            }
                            #endregion

                            #endregion


                        }
                        #endregion
                    }

                    if (inValid.Any())
                    {
                        formattedErrorDisplay = sl.FormatErrorMessageForDisplay(inValid);
                    }

                    if (!string.IsNullOrEmpty(formattedErrorDisplay) && formattedErrorDisplay != "")
                    {
						formattedErrorDisplay = formattedErrorDisplay.Replace('+', ' ');
						formattedErrorDisplay = formattedErrorDisplay.Replace("'", "");
                        Response.Write(string.Format("0| {0}", formattedErrorDisplay));
                    }
                    else
                    {
                        if (softSearchRespose.ShawbrookCaseId != 0)
                        {
                            if (softSearchRespose.EligiblePlanDetails.Any())
                            {
                                #region Eligible Plans
                                ProductCalculatedDetails planToSubmit = null;

                                try
                                {
                                    planToSubmit = softSearchRespose.EligiblePlanDetails.OrderBy(x => x.MonthlyRepayment).First();
                                }
                                catch (Exception)
                                {
                                    throw new Exception("Failed to get plan");
                                }

                                ProductSelectionDetail productSelectionDetail = null;

                                try
                                {
                                    productSelectionDetail = new ProductSelectionDetail()
                                    {
                                        LtvRangeId = planToSubmit.LtvRangeId,
                                        ShawbrookCaseId = softSearchRespose.ShawbrookCaseId
                                    };
                                }
                                catch (Exception)
                                {
                                    throw new Exception("Failed to get product select details");
                                }

                                string jsonProductSelection = "";

                                try
                                {
                                    jsonProductSelection = JsonConvert.SerializeObject(productSelectionDetail);
                                }
                                catch (Exception)
                                {
                                    throw new Exception("Json product Selection Problem");
                                }

                                RestClient client = sl.CreateRestClient();
                                var request = new RestRequest("quotation/quotationselectproduct", Method.PUT);
                                request.AddParameter("text/json", jsonProductSelection, ParameterType.RequestBody);
                                client.AddDefaultHeader("Content-Type", "application/json");

                                var response = client.Execute(request);

                                ProductSelectionResponse refResponse = null;

                                try
                                {
                                    refResponse = JsonConvert.DeserializeObject<ProductSelectionResponse>(response.Content);
                                }
                                catch (Exception)
                                {
                                    throw new Exception("ref Response problem");
                                }

                                try
                                {
                                    if (!string.IsNullOrEmpty(response.Content) && refResponse.ReferralStatusId != null)
                                    {
                                        if (refResponse.ReferralStatusId == EnumReferralStatuses.Approved || refResponse.ReferralStatusId == EnumReferralStatuses.ApprovedAlternative)
                                        {
                                            string formattedDisplay = Common.encodeURL(string.Format("1| Application has been approved."));

                                            sl.UpdateDatabase(planToSubmit, connection, serviceModel, softSearchRespose.ShawbrookCaseId, appId);

                                            Response.Write(string.Format("0| {0}", Common.encodeURL(formattedDisplay)));
                                        }
                                        else
                                        {
                                            string formattedDisplay = "Application not approved. Reasons: ";
                                            foreach (var item in refResponse.ReferralReasons)
                                            {
                                                formattedDisplay += string.Format("{0}, ", item);
                                            }

                                            formattedDisplay += "";
                                            Response.Write(string.Format("0| {0}", Common.encodeURL(formattedDisplay)));  
                                        } 
                                    }
                                    else
                                    {
                                        Response.Write(string.Format("1| Shawbrook Case ID: {0}, Plan: {1} @ {2} per month", productSelectionDetail.ShawbrookCaseId, planToSubmit.Plan, planToSubmit.MonthlyRepayment));
                                    }
                                }
                                catch (Exception)
                                {
									throw new Exception(string.Format("Status Code: {0}, LtvRangeId: {1},  Shawbrook Case Id: {2}", response.StatusCode, productSelectionDetail.LtvRangeId, productSelectionDetail.ShawbrookCaseId));
                                }
                                //string plansTable = sl.CreatePlansTable(softSearchRespose.EligiblePlanDetails); 
                                #endregion
                            }
                            else
                            {
                                //string plansTable = sl.CreatePlansTable(softSearchRespose.InEligiblePlanDetails);
                                try
                                {
                                    string reasons = "";

                                    if (softSearchRespose.InfoMessage != null)
                                    {
                                        foreach (var item in softSearchRespose.InfoMessage)
                                        {
                                            reasons += string.Format("{0}, ", item);
                                        }
                                    }

								Response.Write(string.Format("0| {0} Shawbrook Case ID: {1}, Reasons: {2}", Common.encodeURL("No Plans"), softSearchRespose.ShawbrookCaseId, reasons));
                                }
                                catch (Exception)
                                {
                                    throw new Exception("No plans exception");
                                }

                                #region Submit Referral
                                //RestClient client = sl.CreateRestClient();

                                //var request = new RestRequest("referrals/submitreferral", Method.POST);
                                //ICollection<EnumReferralReasons> reasons = new List<EnumReferralReasons>();

                                //reasons.Add(EnumReferralReasons.Employment);
                                //reasons.Add(EnumReferralReasons.DebtManagement);

                                //SubmitReferralDetails refDetails = new SubmitReferralDetails()
                                //{
                                //    ShawbrookCaseId = softSearchRespose.ShawbrookCaseId,
                                //    Reasons = reasons,
                                //    CaseMerits = "",
                                //    OtherReason = ""
                                //};

                                //var refSerialized = JsonConvert.SerializeObject(refDetails);

                                //request.AddParameter("text/json", refSerialized, ParameterType.RequestBody);
                                //var response = client.Execute(request);

                                //var referralDetails = JsonConvert.DeserializeObject<ResponseMessage>(response.Content);

                                //Response.Write(referralDetails.Message); 
                                #endregion
                            }
                        }
                        else
                        {
                            Response.Write("0| Shawbrook case Id: 0. Application has failed to create.");
                        }
                    }
                }
            }
            else
            {
                Response.Write(string.Format("0| {0}", Common.encodeURL("No details found for App Id")));
            }
        }
        catch (Exception e)
        {
            throw;
        }
    }
}

#region Logic
internal class ShawbrookLogic
{
    internal void UpdateDatabase(ProductCalculatedDetails planToSubmit, SqlConnection connection, vwXMLShawbrook serviceModel, int shawbrookCaseId, int AppID)
    {
        connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());
        connection.Open();

        SqlCommand chkcmd = new SqlCommand();
        SqlCommand updatecmd = new SqlCommand();
        bool update = false;

        chkcmd.Connection = connection;

        #region Under Writing Lender Name
        chkcmd.CommandText = string.Format("SELECT * FROM tblDatastore WHERE [StoredDataName] = 'UnderWritingLenderName'");

        using (SqlDataReader dr = chkcmd.ExecuteReader())
        {
            if (dr.HasRows)
                update = true;
            else
                update = false;
        }

        updatecmd.Connection = connection;

        if (update)
        {
            updatecmd.CommandText = string.Format("UPDATE tblDatastore  SET [AppID] = {0},  [StoredDataType] = '',  [StoredDataValue] = 'Shawbrook',  [CompanyID] = {1} WHERE [StoredDataName] = 'UnderWritingLenderName'", AppID, serviceModel.CompanyID);
            updatecmd.ExecuteNonQuery();
        }
        else
        {
            updatecmd.CommandText = string.Format("INSERT INTO  tblDatastore  ([AppID], [StoredDataType], [StoredDataName], [StoredDataValue], [CompanyID]) VALUES ('{0}', '', 'UnderWritingLenderName', 'Shawbrook', '{1}')", AppID, serviceModel.CompanyID);
            updatecmd.ExecuteNonQuery();
        }
        #endregion

        #region Under Writing Lender Plan
        chkcmd.CommandText = string.Format("SELECT * FROM tblDatastore WHERE [StoredDataName] = 'UnderWritingLenderPlan'");

        using (SqlDataReader dr = chkcmd.ExecuteReader())
        {
            if (dr.HasRows)
                update = true;
            else
                update = false;
        }

        updatecmd.Connection = connection;

        if (update)
        {
            updatecmd.CommandText = string.Format("UPDATE tblDatastore  SET [AppID] = {0},  [StoredDataType] = '',  [StoredDataValue] = '{2}',  [CompanyID] = {1} WHERE [StoredDataName] = 'UnderWritingLenderPlan'", AppID, serviceModel.CompanyID, planToSubmit.ProductId);
            updatecmd.ExecuteNonQuery();
        }
        else
        {
            updatecmd.CommandText = string.Format("INSERT INTO  tblDatastore  ([AppID], [StoredDataType], [StoredDataName], [StoredDataValue], [CompanyID]) VALUES ('{0}', '', 'UnderWritingLenderPlan', '{2}', '{1}')", AppID, serviceModel.CompanyID, planToSubmit.ProductId);
            updatecmd.ExecuteNonQuery();
        }
        #endregion

        #region Under Writing Lender Ref
        chkcmd.CommandText = string.Format("SELECT * FROM tblDatastore WHERE [StoredDataName] = 'UnderWritingLenderRef'");

        using (SqlDataReader dr = chkcmd.ExecuteReader())
        {
            if (dr.HasRows)
                update = true;
            else
                update = false;
        }

        updatecmd.Connection = connection;

        if (update)
        {
            updatecmd.CommandText = string.Format("UPDATE tblDatastore  SET [AppID] = {0},  [StoredDataType] = '',  [StoredDataValue] = '{2}',  [CompanyID] = {1} WHERE [StoredDataName] = 'UnderWritingLenderRef'", AppID, serviceModel.CompanyID, shawbrookCaseId);
            updatecmd.ExecuteNonQuery();
        }
        else
        {
            updatecmd.CommandText = string.Format("INSERT INTO  tblDatastore  ([AppID], [StoredDataType], [StoredDataName], [StoredDataValue], [CompanyID]) VALUES ('{0}', '', 'UnderWritingLenderRef', '{2}', '{1}')", AppID, serviceModel.CompanyID, shawbrookCaseId);
            updatecmd.ExecuteNonQuery();
        }
        #endregion

        #region Add Note
        updatecmd.CommandText = string.Format("INSERT INTO  tblNotes  ([AppID], [Note], [CreatedUserID], [CreatedDate], [NoteActive], [CompanyID], [NoteType]) VALUES ('{0}', 'Accepted in principal by Shawbrook, Shawbrook Reference ID: {2}', '1', GETDATE(), '1', '{1}', '2')", AppID, serviceModel.CompanyID, shawbrookCaseId);
        updatecmd.ExecuteNonQuery();
        #endregion

        #region XML Recieved
        updatecmd.CommandText = string.Format("INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) VALUES({0}, '{1}', {2}, {3}, 'Application Successfully sent to Shawbrook', 'Webservice post', GETDATE())", serviceModel.CompanyID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], AppID, "1");
        updatecmd.ExecuteNonQuery();
        #endregion
    }

    internal RestClient CreateRestClient()
    {
        try
        {
            string userName = Common.getAnyFieldFromUserStore("shawbrookUsername", Config.DefaultUserID);
            string password = Common.getAnyFieldFromUserStore("shawbrookPassword", Config.DefaultUserID);

  			//RestClient client = new RestClient("https://test-securedlending.shawbrook.co.uk/brokerapi/api/");

            RestClient client = new RestClient("https://securedlending.shawbrook.co.uk/brokerapi/api/");
            client.Authenticator = new HttpBasicAuthenticator(userName, password);
            client.AddDefaultHeader("Content-Type", "application/json");

            //RestClient client = new RestClient("https://securedlending.shawbrook.co.uk/brokerapi/");
            //client.Authenticator = new HttpBasicAuthenticator(userName, password);
            //client.AddDefaultHeader("Content-Type", "application/json");

            return client;
        }
        catch (Exception)
        {
            throw;
        }
    }

    internal QuotationSoftSearchDetail CreateSoftSearch(vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        List<ApplicantDetail> Applicants = GetApplications(serviceModel, ref inValid);
        List<ExpenditureItem> ExpenditureList = GetExpenditures(serviceModel, ref inValid);
		List<FeeNew> FeesList = GetFees(serviceModel, ref inValid);

        #region Create Model
        try
        {
            if (serviceModel.BrokerFee.HasValue && serviceModel.EstimatedPropertyValue.HasValue
                       && serviceModel.EstimatedPropertyValue.HasValue && serviceModel.IsExLocalAuthority.HasValue
                       && serviceModel.LoanTerm.HasValue && serviceModel.MarketingOptOutShawbrook.HasValue
                       && serviceModel.MarketingOptOutThirdParties.HasValue && serviceModel.MonthlyMortgagePayment.HasValue
                       && serviceModel.LoanType.HasValue && serviceModel.MortgageBalance.HasValue && serviceModel.PropertyType.HasValue
                       && serviceModel.MortgageCompanyId.HasValue && serviceModel.App1SuppIncome.HasValue && serviceModel.App2SuppIncome.HasValue
                       && serviceModel.NetLoanAmount.HasValue && serviceModel.MonthlyRentalIncome.HasValue &&  Applicants.Any())
            {

					
                QuotationSoftSearchDetail softSearchDetailModel = new QuotationSoftSearchDetail()
                {
                    Applicants = Applicants,
					Fees = FeesList,
                    //BrokerFee = serviceModel.BrokerFee.Value,
                     Expenditure = ExpenditureList,
                    IsExLocalAuthority = serviceModel.IsExLocalAuthority.Value,
                    LoanTerm = serviceModel.LoanTerm.Value,
                    MarketingOptOutShawbrook = serviceModel.MarketingOptOutShawbrook.Value,
                    MarketingOptOutThirdParties = serviceModel.MarketingOptOutThirdParties.Value,
                    MonthlyMortgagePayment = serviceModel.MonthlyMortgagePayment.Value,
                    LoanType = serviceModel.LoanType.Value,
                    PropertyType = serviceModel.PropertyType.Value,
                    SupplementaryIncomeAllApplicants = (serviceModel.App1SuppIncome.Value + serviceModel.App2SuppIncome.Value),
                    NetLoanAmount = serviceModel.NetLoanAmount.Value,
                    NumberOfChildrenUnder18 = serviceModel.App1NoDependants,
                    MonthlyRentalIncome = serviceModel.MonthlyRentalIncome.Value,
                    BrokerIsAdvisor = serviceModel.BrokerIsAdvisor,


                    LoanReasons = new LoanReasons()
                    {
                        Consolidation = true
                    },

                    FirstChargeDetail = new FirstChargeDetail()
                    {
                        EstimatedPropertyValue = serviceModel.EstimatedPropertyValue.Value,
                        MortgageBalance = serviceModel.MortgageBalance.Value,
                        MortgageCompanyId = serviceModel.MortgageCompanyId.Value,
                        IsInterestOnly = serviceModel.IsInterestOnly.Value,
                        IsFixedTerm = serviceModel.IsFixedTerm.Value,
                        FixedTermRemaining = serviceModel.FixedTermRemaining.Value,
                    }
                };


                return softSearchDetailModel;
                
            }
            else
            {
                inValid.Add(string.Format("Please check application "));
            }
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("Problem with soft search data: {0}, ", e.Message));
        }
        #endregion

        return null;
    }


    internal List<FeeNew> GetFees(vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        try
        {
            List<FeeNew> FeesList = new List<FeeNew>();

            FeesList.Add(new FeeNew()
            {
                FeeAmount = serviceModel.BrokerFee.Value,
                FeeType = EnumFeeType.Broker,
                FeePaymentArrangement = EnumFeesPaymentArrangements.AddToLoan
            });
            FeesList.Add(new FeeNew()
            {
                FeeAmount = (decimal)35.00,
                FeeType = EnumFeeType.ShawbrookLender,
                FeePaymentArrangement = EnumFeesPaymentArrangements.AddToLoan
            });
            FeesList.Add(new FeeNew()
            {
                FeeAmount = (decimal)0,
                FeeType = EnumFeeType.Advise,
                FeePaymentArrangement = EnumFeesPaymentArrangements.AddToLoan
            });
            FeesList.Add(new FeeNew()
            {
                FeeAmount = (decimal)0,
                FeeType = EnumFeeType.Discharge,
                FeePaymentArrangement = EnumFeesPaymentArrangements.PaidOnCompletion
            });
            FeesList.Add(new FeeNew()
            {
                FeeAmount = (decimal)0,
                FeeType = EnumFeeType.Disbursement,
                FeePaymentArrangement = EnumFeesPaymentArrangements.AddToLoan
            });



            return FeesList;
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("{0}, ", e.Message));
            return null;
        }

    }

    internal List<ExpenditureItem> GetExpenditures(vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        try
        {
            List<ExpenditureItem> ExpenditureList = new List<ExpenditureItem>();

            if (serviceModel.App1ExpInsurance.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpInsurance.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.BuildingsAndContentInsurance
                });
            }

            if (serviceModel.App1ExpChildcareMaintenance.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpChildcareMaintenance.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.MaintenanceOrChildSupport
                });
            }

            if (serviceModel.App1ExpCouncilTax.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpCouncilTax.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.CouncilTax
                });
            }

            if (serviceModel.App1ExpGasElectricity.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpGasElectricity.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.GasElectricityFuel
                });
            }

            if (serviceModel.App1ExpWaterRates.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpWaterRates.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.Water
                });
            }

            if (serviceModel.App1ExpFoodToiletCleaning.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpFoodToiletCleaning.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.ClothingAndFootWear,
                });
            }

            if (serviceModel.App1ExpTravel.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpTravel.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.Transport,
                });
            }

            if (serviceModel.App1ExpPetrol.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpPetrol.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.GasElectricityFuel,
                });
            }

            if (serviceModel.App1ExpEducation.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpEducation.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.Education,
                });
            }

            if (serviceModel.App1ExpClothing.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpClothing.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.ClothingAndFootWear,
                });
            }

            if (serviceModel.App1ExpHolidaysLeisure.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpHolidaysLeisure.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.AnyOtherExpenses,
                    ExplanatoryNote = "Holiday and Leisure expense"
                });
            }

            if (serviceModel.App1ExpTelephone.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpTelephone.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.AnyOtherExpenses,
                    ExplanatoryNote = "Telephone expense"
                });
            }

            if (serviceModel.App1ExpOther.HasValue)
            {
                ExpenditureList.Add(new ExpenditureItem()
                {
                    Amount = serviceModel.App1ExpOther.Value,
                    Cause = ExpenditureItem.EnumExpenditureCauses.AnyOtherExpenses,
                    ExplanatoryNote = "Other expenses"
                });
            }

            return ExpenditureList;
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("{0}, ", e.Message));
            return null;
        }

    }

    internal List<ApplicantDetail> GetApplications(vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        try
        {
            List<ApplicantDetail> applicants = new List<ApplicantDetail>();
            List<AddressDetail> app1Addresses = GetApplicantsAddresses(true, serviceModel, ref inValid);
            List<AddressDetail> app2Addresses = GetApplicantsAddresses(false, serviceModel, ref inValid);

            if (serviceModel.App1DateOfBirth.HasValue && serviceModel.App1Gender.HasValue && serviceModel.App1GrossAnnualIncome.HasValue
                && serviceModel.App1SelfEmployedEvidenceType.HasValue && serviceModel.App1MaritalStatus.HasValue
                && serviceModel.App1EmploymentStatus.HasValue && serviceModel.App1Title.HasValue && serviceModel.App1EmploymentStatus.HasValue
                && serviceModel.App1TimeInEmployment.HasValue && serviceModel.App1SelfEmployedEvidenceType.HasValue)
            {
                applicants.Add(new ApplicantDetail()
                {
                    Addresses = app1Addresses,
                    DateOfBirth = (DateTime)serviceModel.App1DateOfBirth,
                    EmailAddress = serviceModel.App1EmailAddress,
                    EmployerName = serviceModel.App1EmployerName,
                    EmployerTelephone = serviceModel.App1EmployerTelephone,
                    EmploymentStatus = (int)serviceModel.App1EmploymentStatus.Value,
                    Firstname = serviceModel.App1Firstname,
                    Gender = (int)serviceModel.App1Gender,
                    NationalityCountryId = (int)serviceModel.NationalityCountryId,
                    BirthCountryId = (int)serviceModel.BirthCountryId,
                    GrossAnnualIncome = (decimal)serviceModel.App1GrossAnnualIncome.Value,

                    LiRExplanation = EnumLiRExplanations.NotSet,
                    LiRWorkingExplanation = EnumLiRExplanations.NotSet,
                    HomeTelephone = serviceModel.App1HomeTelephone,
                    MaidenPreviousNames = serviceModel.App1MaidenPreviousNames,
                    MaritalStatus = serviceModel.App1MaritalStatus.Value,
                    Middlenames = serviceModel.App1Middlenames,
                    MobileNumber = serviceModel.App1MobileTelephone,
                    RegularIncome = new IncomeRecord()
                    {
                        Income = serviceModel.App1GrossAnnualIncome.Value
                    },
                    Occupation = serviceModel.App1Occupation,
                    Title = serviceModel.App1Title.Value,
                    SelfEmployedStatus = serviceModel.App1EmploymentStatus.Value,
                    Surname = serviceModel.App1Surname,
                    TimeInEmployment = serviceModel.App1TimeInEmployment.Value,
                    SelfEmployedEvidenceType = serviceModel.App1SelfEmployedEvidenceType.Value
                    


                });



                if (serviceModel.App2Title.HasValue)
                {
                    if (serviceModel.App2DateOfBirth.HasValue && serviceModel.App2Gender.HasValue && serviceModel.App2GrossAnnualIncome.HasValue
                                        && serviceModel.App2SelfEmployedEvidenceType.HasValue && serviceModel.App2MaritalStatus.HasValue
                                        && serviceModel.App2EmploymentStatus.HasValue && serviceModel.App2Title.HasValue 
                                        && serviceModel.App2TimeInEmployment.HasValue)
                    {
                        applicants.Add(new ApplicantDetail()
                        {
                            Addresses = app2Addresses,
                            DateOfBirth = (DateTime)serviceModel.App2DateOfBirth,
                            EmailAddress = serviceModel.App2EmailAddress,
                            EmployerName = serviceModel.App2EmployerName,
                            EmployerTelephone = serviceModel.App2EmployerTelephone,
                            EmploymentStatus = (int)serviceModel.App2EmploymentStatus.Value,
                            Firstname = serviceModel.App2Firstname,
                            Gender = (int)serviceModel.App2Gender,
                            NationalityCountryId = (int)serviceModel.NationalityCountryId,
                            BirthCountryId = (int)serviceModel.BirthCountryId,
                            GrossAnnualIncome = (decimal)serviceModel.App2GrossAnnualIncome.Value,

                            LiRExplanation = EnumLiRExplanations.NotSet,
                            LiRWorkingExplanation = EnumLiRExplanations.NotSet,
                            HomeTelephone = serviceModel.App2HomeTelephone,
                            MaidenPreviousNames = serviceModel.App2MaidenPreviousNames,
                            MaritalStatus = serviceModel.App2MaritalStatus.Value,
                            Middlenames = serviceModel.App2Middlenames,
                            MobileNumber = serviceModel.App2MobileTelephone,
                            RegularIncome = new IncomeRecord()
                            {
                                Income = serviceModel.App2GrossAnnualIncome.Value
                            },
                            Occupation = serviceModel.App2Occupation,
                            Title = serviceModel.App2Title.Value,
                            SelfEmployedStatus = serviceModel.App2EmploymentStatus.Value,
                            Surname = serviceModel.App2Surname,
                            TimeInEmployment = serviceModel.App2TimeInEmployment.Value,
                            SelfEmployedEvidenceType = serviceModel.App2SelfEmployedEvidenceType.Value

                        });
                    }
                    else
                    {
                        if (!serviceModel.App2DateOfBirth.HasValue)
                        {
                            inValid.Add("App2 date of birth missing, ");
                        }

                        if (!serviceModel.App2Gender.HasValue)
                        {
                            inValid.Add("App2 gender missing, ");
                        }

                        if (!serviceModel.App2GrossAnnualIncome.HasValue)
                        {
                            inValid.Add("App2 gross annual income missing, ");
                        }

                        if (!serviceModel.App2SelfEmployedEvidenceType.HasValue)
                        {
                            inValid.Add("App2 self employed evidence type missing, ");
                        }

                        if (!serviceModel.App2MaritalStatus.HasValue)
                        {
                            inValid.Add("App2 marital status missing, ");
                        }

                        if (!serviceModel.App2EmploymentStatus.HasValue)
                        {
                            inValid.Add("App2 employment status missing, ");
                        }

                        if (!serviceModel.App2Title.HasValue)
                        {
                            inValid.Add("App2 title missing, ");
                        }

                        if (!serviceModel.App2TimeInEmployment.HasValue)
                        {
                            inValid.Add("App2 time in employment missing, ");
                        }
                    }

                }
                return applicants;
            }           

            return null;
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("{0}, ", e.Message));
            return null;
        }
    }

    internal List<AddressDetail> GetApplicantsAddresses(bool isApp1, vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        try
        {
            List<AddressDetail> addressList = new List<AddressDetail>();
            if (isApp1)
            {
                if (serviceModel.App1Type.HasValue && serviceModel.App1ResidentialStatusId.HasValue
                    && serviceModel.App1TimeAtAddressInMonths.HasValue)
                {
                    addressList.Add(new AddressDetail()
                    {
                        County = serviceModel.App1County,
                        District = serviceModel.App1District,
                        HouseNo = string.IsNullOrEmpty(serviceModel.App1HouseNo) ? serviceModel.App1AddressHouseName : serviceModel.App1HouseNo,
                        Postcode = serviceModel.App1Postcode,
                        PostTown = serviceModel.App1PostTown,
                        ResidentialStatusId = serviceModel.App1ResidentialStatusId.Value,
                        Street1 = serviceModel.App1Street1,
                        Street2 = serviceModel.App1Street2,
                        TimeAtAddressInMonths = serviceModel.App1TimeAtAddressInMonths.Value,
                        Type = serviceModel.App1Type.Value
                    });

                    if (serviceModel.App1PreviousTimeAtAddressInMonths.HasValue)
                    {
                        addressList.Add(new AddressDetail()
                        {
                            County = serviceModel.App1PreviousCounty,
                            District = serviceModel.App1PreviousDistrict,
                            HouseNo = serviceModel.App1PreviousHouseNo,
                            Postcode = serviceModel.App1PreviousPostcode,
                            PostTown = serviceModel.App1PreviousPostTown,
                            Street1 = serviceModel.App1PreviousStreet1,
                            TimeAtAddressInMonths = serviceModel.App1PreviousTimeAtAddressInMonths.Value,
                            Type = EnumAddressTypes.Previous
                        });
                    }

                    return addressList;
                }
            }
            else
            {

                if (serviceModel.App2ResidentialStatusId.HasValue
                    && serviceModel.App2TimeAtAddressInMonths.HasValue)
                {
                    addressList.Add(new AddressDetail()
                            {
                                County = serviceModel.App2County,
                                District = serviceModel.App2District,
                                HouseNo = serviceModel.App2HouseNo,
                                Postcode = serviceModel.App2Postcode,
                                PostTown = serviceModel.App2PostTown,
                                ResidentialStatusId = serviceModel.App2ResidentialStatusId.Value,
                                Street1 = serviceModel.App2Street1,
                                Street2 = serviceModel.App2Street2,
                                TimeAtAddressInMonths = serviceModel.App2TimeAtAddressInMonths.Value,
                                Type = EnumAddressTypes.Permanent
                    }); 
                }
                else
                {
                    

                    if (!serviceModel.App2AddressType.HasValue)
                        inValid.Add("App 2 Address type, ");
                    if (!serviceModel.App2ResidentialStatusId.HasValue)
                        inValid.Add("App 2 Residential status, ");
                    if (!serviceModel.App2ResidentialStatusId.HasValue)
                        inValid.Add("App 2 Time at address, ");
                }
                return addressList;
            }
            return addressList;
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("Server Error: {0}", e.Message));
            return null;
        }
    }

    //internal static RestClient CreateAndConfigureClient(string searialisedjson)
    //{
    //    try
    //    {
    //        RestClient client = new RestClient("https://test-securedlending.shawbrook.co.uk/brokerapi/api/");  //new HttpClient { BaseAddress = new Uri("https://test-securedlending.shawbrook.co.uk/brokerapi/api/") };
    //        client.Authenticator = new HttpBasicAuthenticator("dev.team@engagedcrm.co.uk", "Y0rkshire");

    //        var request = new RestRequest("consentforms");
    //        request.AddBody(searialisedjson);

    //        client.AddDefaultHeader("Content-Type", "application/json");

    //        var response = client.Execute(request);

    //        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //        //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
    //        //    Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", "dev.team@engagedcrm.co.uk", "Y0rkshire"))));

    //        return client;
    //    }
    //    catch (Exception e)
    //    {                
    //        throw;
    //    }
    //}

    internal string HandlePostErrors(BadRequestResponse errorResult, ref List<string> valid)
    {
        try
        {
            if (errorResult.ModelState != null)
            {
                if (errorResult.ModelState.Any())
                {
                    foreach (var error in errorResult.ModelState)
                    {
                        FriendlyErrorMessage(error, ref valid);
                    }
                }
                string displayError = "";

                foreach (var item in valid)
                {
                    displayError += item;
                }

                displayError += "";
                return displayError;
            }
            return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    private void FriendlyErrorMessage(KeyValuePair<string, IEnumerable<string>> error, ref List<string> inValid)
    {
        var splitKey = error.Key.Split('.');
        string errorMessage = "";

        if (splitKey.Any(x => x.Contains("Applicants")))
        {
            var applicant = splitKey[1].Split('[')[1].First();
            string applicantType = "";

            switch (applicant)
            {
                case '0':
                    applicantType = "First Applicant - ";
                    break;
                case '1':
                    applicantType = "Second Applicant - ";
                    break;
                default:
                    break;
            }

            errorMessage += applicantType;
        }

        if (splitKey.Any(x => x.Contains("Addresses")))
        {
            var address = splitKey[2].Split('[')[1].First();
            string addressType = "";

            switch (address)
            {
                case '0':
                    addressType = "Current";
                    break;
                case '1':
                    addressType = "Previous";
                    break;
                default:
                    break;
            }

            foreach (var item in error.Value)
            {
                inValid.Add(string.Format("Please Check {0} Address: {1}, ", addressType, item));
            }
        }
        else
        {
            foreach (var item in error.Value)
            {
                inValid.Add(string.Format("Please Check {0} {1}, ", errorMessage, item));
            }
        }
        //switch (error.Key.Split('.'))
        //{
        //    default:
        //        break;
        //}
    }

    internal void ValidateModel(vwXMLShawbrook serviceModel, ref List<string> inValid)
    {
        try
        {
            if (serviceModel.BrokerFee.HasValue)
            {
                if (serviceModel.BrokerFee.Value == 0)
                    inValid.Add("Broker Fee, ");
            }
            else
            {
                inValid.Add("Broker Fee, ");
            }


            if (serviceModel.EstimatedPropertyValue.HasValue)
            {
                if (serviceModel.EstimatedPropertyValue.Value == 0)
                    inValid.Add("Estimated Property Value, ");
            }
            else
            {
                inValid.Add("Estimated Property Value, ");
            }

            if (serviceModel.LoanTerm.HasValue)
            {
                if (serviceModel.LoanTerm.Value == 0)
                    inValid.Add("Loan Term, ");
            }
            else
            {
                inValid.Add("Loan Term, ");
            }

            if (!serviceModel.LoanType.HasValue)
                inValid.Add("Loan Type, ");


            if (serviceModel.MonthlyMortgagePayment.HasValue)
            {
                if (serviceModel.MonthlyMortgagePayment.Value == 0)
                    inValid.Add("Monthly Mortgage Payment, ");
            }
            else
            {
                inValid.Add("Monthly Mortgage Payment, ");
            }

            if (serviceModel.MortgageBalance.HasValue)
            {
                if (serviceModel.MortgageBalance.Value == 0)
                    inValid.Add("Mortgage Balance, ");
            }
            else
            {
                inValid.Add("Mortgage Balance, ");
            }

            if (serviceModel.NetLoanAmount.HasValue)
            {
                if (serviceModel.NetLoanAmount.Value == 0)
                    inValid.Add("Net Loan Amount, ");
            }
            else
            {
                inValid.Add("Net Loan Amount, ");
            }


            //Not to use
            //if (serviceModel.MortgageCompanyId == null)
            //    inValid.Add("Net Loan Amount, ");





            if (serviceModel.PropertyType.HasValue)
            {
                if (serviceModel.PropertyType.Value == 0)
                    inValid.Add("Property Type, ");
            }
            else
            {
                inValid.Add("Property Type, ");
            }

            if (string.IsNullOrEmpty(serviceModel.App1HouseNo) && string.IsNullOrEmpty(serviceModel.App1AddressHouseName))
                inValid.Add("App 1 House number or name required ");

            if (string.IsNullOrEmpty(serviceModel.App1Street1))
                inValid.Add("App 1 Street 1, ");

            //if (string.IsNullOrEmpty(serviceModel.App1Street2))
            //    inValid.Add("App 1 Street 2, ");

            //if (string.IsNullOrEmpty(serviceModel.App1County))
            //    inValid.Add("App1 County, ");

            if (string.IsNullOrEmpty(serviceModel.App1Postcode))
                inValid.Add("App1 Postcode, ");

            if (serviceModel.App1TimeAtAddressInMonths.HasValue)
            {
                if (serviceModel.App1TimeAtAddressInMonths.Value == 0)
                    inValid.Add("App1 Time at Address, ");
            }
            else
            {
                inValid.Add("App1 Time at Address, ");
            }

            if (!serviceModel.App1ResidentialStatusId.HasValue)
                inValid.Add("App1 Residential Status, ");

            if (!serviceModel.App1Type.HasValue)
                inValid.Add("App1 Address Type, ");

            if (!serviceModel.App1DateOfBirth.HasValue)
                inValid.Add("App1 Date of Birth, ");

            if (string.IsNullOrEmpty(serviceModel.App1EmailAddress))
                inValid.Add("App1 Email Address, ");

            if (string.IsNullOrEmpty(serviceModel.App1EmployerName))
                inValid.Add("App1 Employer Name, ");

            if (!serviceModel.App1EmploymentStatus.HasValue)
                inValid.Add("App1 Employer Status, ");

            //if (string.IsNullOrEmpty(serviceModel.App1EmployerTelephone))
            //    inValid.Add("App1 Employer Telephone, ");

            if (string.IsNullOrEmpty(serviceModel.App1Firstname))
                inValid.Add("App1 First Name, ");

            if (!serviceModel.App1Gender.HasValue)
                inValid.Add("App1 Gender, ");

            if (serviceModel.App1GrossAnnualIncome.HasValue)
            {
                if (serviceModel.App1GrossAnnualIncome.Value == 0)
                    inValid.Add("App1 Gross Annual Income, ");
            }
            else
            {
                inValid.Add("App1 Gross Annual Income, ");
            }

            if (string.IsNullOrEmpty(serviceModel.App1HomeTelephone) && string.IsNullOrEmpty(serviceModel.App1MobileTelephone))
                inValid.Add("App1 Atleast one contact number required, ");


            if (!serviceModel.App1MaritalStatus.HasValue)
                inValid.Add("App1 Marital Status, ");
         

            if (string.IsNullOrEmpty(serviceModel.App1Occupation))
                inValid.Add("App1 Occupation, ");

            if (serviceModel.App1TimeInEmployment.HasValue)
            {
                if (serviceModel.App1TimeInEmployment.Value == 0)
                    inValid.Add("App1 Time in Employment, ");
            }
            else
            {
                inValid.Add("App1 Time in Employment, ");
            }

            if (string.IsNullOrEmpty(serviceModel.App1Surname))
                inValid.Add("App1 Surname, ");

            if (!serviceModel.App1Title.HasValue)
                inValid.Add("App1 Title, ");

            if (!serviceModel.LoanPurpose.HasValue)
                inValid.Add("App1 Loan Purpose, ");

            //if (string.IsNullOrEmpty(serviceModel.SecurityHouseNo))
            //    inValid.Add("App1 Security House No, ");

            if (string.IsNullOrEmpty(serviceModel.SecurityStreet1))
                inValid.Add("App1 Security Street 1, ");

            //if (string.IsNullOrEmpty(serviceModel.SecurityStreet2))
            //    inValid.Add("App1 Security Street 2, ");

            //if (string.IsNullOrEmpty(serviceModel.SecurityCounty))
            //    inValid.Add("App1 Security County, ");

            if (string.IsNullOrEmpty(serviceModel.SecurityPostcode))
                inValid.Add("App1 Security Post Code, ");

            if (serviceModel.SecurityTimeAtAddressInMonths.HasValue)
            {
                if (serviceModel.SecurityTimeAtAddressInMonths.Value == 0)
                    inValid.Add("App1 Security Time at Address, ");
            }
            else
            {
                inValid.Add("App1 Security Time at Address, ");
            }

            if (string.IsNullOrEmpty(serviceModel.SecurityResidentialStatusId))
                inValid.Add("App1 Security Residential Status, ");

            if (!serviceModel.SecurityAddressType.HasValue)
                inValid.Add("App1 Security Address Type, ");
        }
        catch (Exception e)
        {
            inValid.Add(string.Format("{0}, ", e.Message));
        }
    }

    internal string FormatErrorMessageForDisplay(List<string> inValid)
    {
        string formattedErrorDisplay = "Please check the following fields: ";
        foreach (var message in inValid)
        {
            formattedErrorDisplay += message;
        }
        formattedErrorDisplay += "";
        return formattedErrorDisplay;
    }

    internal string CreatePlansTable(IEnumerable<ProductCalculatedDetails> eligiblePlans)
    {
        string display = "<table><tr><th>Policy Details</th><th>Annual Rate</th><th>Fees</th><th>Gross Loan</th><th>Interest</th><th>Total Repayable</th><th>Stressed Payment</th><th>Monthly Payment</th><th>Continue</th></tr>";

        foreach (var plan in eligiblePlans)
        {
            display += string.Format(/* Policy Details */"<tr><td>{14}</br>LTV Range: {13} - {11} </br> Min/Max Loan: {12} - {10} </td>"
                                + /* Annual Rate */"<td>Annual Rate: {15} {3} </td>"
                                + /* Fees */"<td>Broker Fee: {1} </br> Commission: {2} </br> Lender Fee: {8} </br> TT Fee: {24}</td>"
                                + /* Gross Loan */"<td>{5}</td>"
                                + /* Interest */"<td>{22}</td>"
                                + /* Total Repayable */"<td>{20} </br> {16} </br> {17} </br> {18}</td>"
                                + /* Stressed Payment */"<td>{19}</td>"
                                + /* Monthly Payment */"<td>{25}</td>"
                                + /* Continue */"<td><input type='button' value='Submit' /></td></tr>",
                /*{0}*/plan.ArrangementFee,
                /*{1}*/plan.BrokerFee,
                /*{2}*/plan.Commission,
                /*{3}*/plan.FixedAnnualRate,
                /*{4}*/plan.FixedTerm,
                /*{5}*/plan.GrossLoanAmount,
                /*{6}*/plan.IsFixedOrVariable,
                /*{7}*/plan.LegalFee,
                /*{8}*/plan.LenderFee,
                /*{9}*/plan.MaxBrokerFee,
                /*{10}*/plan.MaxLoan,
                /*{11}*/plan.MaxLTV,
                /*{12}*/plan.MinLoan,
                /*{13}*/plan.MinLTV,
                /*{14}*/plan.Plan,
                /*{15}*/plan.Rate,
                /*{16}*/plan.Settle3Quarter,
                /*{17}*/plan.SettleHalf,
                /*{18}*/plan.SettleQuarter,
                /*{19}*/plan.StressedPayment,
                /*{20}*/plan.TotalAmountToRepay,
                /*{21}*/plan.TotalCharge,
                /*{22}*/plan.TotalIntCostFees,
                /*{23}*/plan.TotalIntPlusFees,
                /*{24}*/plan.TTFee,
                /*{25}*/plan.MonthlyRepayment);
        }
        display += "</table>";
        return display;
    }
}
#endregion

#region Engaged Models
class ProductSelectionDetail
{
    public int ShawbrookCaseId { get; set; }
    public int LtvRangeId { get; set; }
}

class ProductSelectionResponse
{
    public int ShawbrookCaseId { get; set; }
    public List<EnumReferralReasons> ReferralReasons { get; set; }
    public EnumReferralStatuses ReferralStatusId { get; set; }
}

enum EnumReferralStatuses
{
    New = 1,
    CaseReOpened = 3,
    SalesReview = 4,
    ReviewPending = 5,
    Pending = 7,
    Approved = 18,
    ApprovedAlternative = 19,
    Declined = 20,
    Advised = 21,
    Closed = 22,
    Invalidated = 23,
    Incomplete = 24
}
public class BrokerKeyModel
{
    public string Script { get; set; }
    public Guid Key { get; set; }
    public DateTime DateIssued { get; set; }
}

public class BrokerUserEmailClass
{
    public string BrokerUserEmail { get; set; }
}

internal class vwXMLShawbrook
{
    private SqlDataReader reader;

    public vwXMLShawbrook(SqlDataReader reader)
    {
        this.reader = reader;
    }
	
	/// new code for MCD Changes
    
    public int NationalityCountryId
    {
        get
        {
            int NationalityCountryId = 221;

            return NationalityCountryId;
        }
    }
    
     public int BirthCountryId
    {
        get
        {
            int BirthCountryId = 221;
            return BirthCountryId;
        }
    }
	
    
    public int? CountryId
    {
        get
        {
            int CountryId = 221;

            if (!int.TryParse(reader["CountryId"].ToString(), out CountryId))
            {
                return null;
            }
            return CountryId;
        }

    }

    public string BrokerFcaNumber { get { return reader["BrokerFcaNumber"].ToString(); } }

    public string BrokerCompanyName { get { return reader["BrokerCompanyName"].ToString(); } }

    public string BrokerTelephone { get { return reader["BrokerTelephone"].ToString(); } }

    public string BrokerEmailAddress { get { return reader["BrokerEmailAddress"].ToString(); } }

    public string WebsiteUrl { get { return reader["WebsiteUrl"].ToString(); } } 
    
    public int? FixedTermRemaining {

        get {

            int FixedTermRemaining = 0;
            return FixedTermRemaining;
        }


    }

     public bool? IsInterestOnly
    {
        get
        {
            bool IsInterestOnly = false;

            if (!bool.TryParse(reader["IsInterestOnly"].ToString(), out IsInterestOnly))
            {
                return IsInterestOnly;
            }

            return IsInterestOnly;
        }
    }
    
     public bool? IsFixedTerm
    {
        get
        {
            bool IsFixedTerm = false;

            if (!bool.TryParse(reader["IsFixedTerm"].ToString(), out IsFixedTerm))
            {
                return IsFixedTerm;
            }

            return IsFixedTerm;
        }
    }
    
    public bool BrokerIsAdvisor
    {
        get
        {
            bool BrokerIsAdvisor = true;
            return BrokerIsAdvisor;
        }
    }

    //public EnumFeeType? FeeType
    //{
    //    get
    //    {
    //        EnumFeeType? FeeType = null;

    //        switch (reader["FeeType"].ToString())
    //        {
    //            case "Lender":
    //                FeeType = EnumFeeType.Lender;
    //                break;
    //            case "Broker ":
    //                FeeType = EnumFeeType.Broker;
    //                break;
    //            case "Advise ":
    //                FeeType = EnumFeeType.Advise;
    //                break;
    //            case "TelegraphicTransfer ":
    //                FeeType = EnumFeeType.TelegraphicTransfer;
    //                break;
    //        }

    //        return FeeType;
    //    }
    //}

   
    //public Enum FeesPaymentArrangements? Arrangment
   // {
       // get
     //   {
      //      EnumFeesPaymentArrangements? Arrangment = null;

       //     switch (reader["Arrangment/EnumLoanPurposes"].ToString())
       //     {
       //         case "PaidUpFront":
      //              loanPurpose = EnumFeesPaymentArrangements.PaidUpFront;
       //             break;
        //        case "AddToLoan":
       //             loanPurpose = EnumFeesPaymentArrangements.AddToLoan;
       //             break;
       //         case "DeductedFromNetAdvance":
       //             loanPurpose = EnumFeesPaymentArrangements.DeductedFromNetAdvance;
      //              break;
              
      //      }
      //      return Arrangment;
       // }
  //  }
    








    /// End of MCD Code
    
    public string App1AddressHouseName { get { return reader["AddressHouseName"].ToString(); } }

    public string App2AddressHouseName { get { return reader["AddressHouseName"].ToString(); } }


    public int AppID { get { return !string.IsNullOrEmpty(reader["AppID"].ToString()) ? int.Parse(reader["AppID"].ToString()) : 0; } }

    public int CompanyID { get { return !string.IsNullOrEmpty(reader["CompanyID"].ToString()) ? int.Parse(reader["CompanyID"].ToString()) : 0; } }
	
	public int App1NoDependants { get { return !string.IsNullOrEmpty(reader["App1NoDependants"].ToString()) ? int.Parse(reader["App1NoDependants"].ToString()) : 0; }  }

    public decimal? BrokerFee
    {
        get
        {
            decimal brokerFee = 0;

            if (!decimal.TryParse(reader["BrokerFee"].ToString(), out brokerFee))
            {
                return null;
            }

            return brokerFee;
        }
    }

    public string DPAAuthorisationToken { get; set; }

    public decimal? EstimatedPropertyValue
    {
        get
        {
            decimal estimatedPropertyValue = 0;

            if (!decimal.TryParse(reader["EstimatedPropertyValue"].ToString(), out estimatedPropertyValue))
            {
                return null;
            }

            return estimatedPropertyValue;
        }
    }

    public bool? IsExLocalAuthority
    {
        get
        {
            bool isLocal = false;

            if (!bool.TryParse(reader["IsExLocalAuthority"].ToString(), out isLocal))
            {
                return null;
            }

            return isLocal;
        }
    }

    public int? LoanTerm
    {
        get
        {
            int loanTerm = 0;

            if (!int.TryParse(reader["LoanTerm"].ToString(), out loanTerm))
            {
                return null;
            }
            return loanTerm;
        }
    }

    public EnumLoanTypes? LoanType
    {
        get
        {
            EnumLoanTypes? loanType = null;

            switch (reader["LoanType"].ToString())
            {
                case "SecuredLoan":
                    loanType = EnumLoanTypes.SecuredLoan;
                    break;
                case "SecondChargeBtl":
                    loanType = EnumLoanTypes.SecondChargeBtl;
                    break;
                case "FurtherAdvances":
                    loanType = EnumLoanTypes.FurtherAdvances;
                    break;
                case "FirstChargeDeeds":
                    loanType = EnumLoanTypes.FirstChargeDeeds;
                    break;
                case "FirstChargeBtl":
                    loanType = EnumLoanTypes.FirstChargeBtl;
                    break;
            }

            return loanType;
        }
    }

    public bool? MarketingOptOutShawbrook
    {
        get
        {
            bool marketingOptOutShawbrook = false;

            if (!bool.TryParse(reader["MarketingOptOutShawbrook"].ToString(), out marketingOptOutShawbrook))
            {
                return null;
            }

            return marketingOptOutShawbrook;
        }
    }

    public bool? MarketingOptOutThirdParties
    {
        get
        {
            bool marketingOptOutThirdParties = false;

            if (!bool.TryParse(reader["MarketingOptOutThirdParties"].ToString(), out marketingOptOutThirdParties))
            {
                return null;
            }

            return marketingOptOutThirdParties;
        }
    }

    public decimal? MonthlyMortgagePayment
    {
        get
        {
            decimal monthlyMortgagePayment = 0;

            if (!decimal.TryParse(reader["MonthlyMortgagePayment"].ToString(), out monthlyMortgagePayment))
            {
                return null;
            }

            return monthlyMortgagePayment;
        }
    }

    public decimal? MonthlyRentalIncome
    {
        get
        {
            decimal monthlyRentalIncome = 0;

            if (!decimal.TryParse(reader["MonthlyRentalIncome"].ToString(), out monthlyRentalIncome))
            {
                return null;
            }

            return monthlyRentalIncome;
        }
    }

    public decimal? MortgageBalance
    {
        get
        {
            decimal mortgageBalance = 0;

            if (!decimal.TryParse(reader["MortgageBalance"].ToString(), out mortgageBalance))
            {
                return null;
            }

            return mortgageBalance;
        }
    }

    public decimal? NetLoanAmount
    {
        get
        {
            decimal netLoanAmount = 0;

            if (!decimal.TryParse(reader["NetLoanAmount"].ToString(), out netLoanAmount))
            {
                return null;
            }

            return netLoanAmount;
        }
    }

    

    public int? MortgageCompanyId
    {
        get
        {
            int mortgageCompanyId = 0;

            if (!int.TryParse(reader["MortgageCompanyId"].ToString(), out mortgageCompanyId))
            {
                return null;
            }
            return mortgageCompanyId;
        }
    }

    public EnumPropertyTypes? PropertyType
    {
        get
        {
            EnumPropertyTypes? propertyType = null;

            switch (reader["PropertyType"].ToString())
            {
                case "House":
                    propertyType = EnumPropertyTypes.House;
                    break;
                case "PurposeBuiltFlat":
                    propertyType = EnumPropertyTypes.PurposeBuiltFlat;
                    break;
                case "Maisonette":
                    propertyType = EnumPropertyTypes.Maisonette;
                    break;
                case "FlatMoreThan6Storeys":
                    propertyType = EnumPropertyTypes.FlatMoreThan6Storeys;
                    break;
                case "ConvertedFlat":
                    propertyType = EnumPropertyTypes.ConvertedFlat;
                    break;
                case "Bungalow":
                    propertyType = EnumPropertyTypes.Bungalow;
                    break;
            }
            return propertyType;
        }
    }

    public string LookupHouseNo { get { return reader["LookupHouseNo"].ToString(); } }

    public string LookupPostcode { get { return reader["LookupPostcode"].ToString(); } }

    public string App1HouseNo { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/HouseNo"].ToString(); } }

    public string App1Street1 { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/Street1"].ToString(); } }

    public string App1Street2 { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/Street2"].ToString(); } }

    public string App1District { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/District"].ToString(); } }

    public string App1PostTown { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/PostTown"].ToString(); } }

    public string App1County { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/County"].ToString(); } }

    public string App1Postcode { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/Postcode"].ToString(); } }

    public int? App1TimeAtAddressInMonths
    {
        get
        {
            int app1TimeAtAddressInMonths = 0;

            if (!int.TryParse(reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/TimeAtAddressInMonths"].ToString(), out app1TimeAtAddressInMonths))
            {
                return null;
            }
            return app1TimeAtAddressInMonths;
        }
    }

    public EnumResidentialStatuses? App1ResidentialStatusId
    {
        get
        {
            EnumResidentialStatuses? app1ResidentialStatusId = null;

            switch (reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/ResidentialStatusId"].ToString())
            {
                case "OwnerOccupied":
                    app1ResidentialStatusId = EnumResidentialStatuses.OwnerOccupied;
                    break;
                case "BuyToLet":
                    app1ResidentialStatusId = EnumResidentialStatuses.BuyToLet;
                    break;
            }

            return app1ResidentialStatusId;
        }
    }

    public EnumAddressTypes? App1Type
    {
        get
        {
            EnumAddressTypes? app1Type = null;

            switch (reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/Type"].ToString())
            {
                case "BankBuildingSociety":
                    app1Type = EnumAddressTypes.BankBuildingSociety;
                    break;
                case "Employer":
                    app1Type = EnumAddressTypes.Employer;
                    break;
                case "LandRegistry":
                    app1Type = EnumAddressTypes.LandRegistry;
                    break;
                case "LandRegistryForBtlHome":
                    app1Type = EnumAddressTypes.LandRegistryForBtlHome;
                    break;
                case "LandRegistryLinked":
                    app1Type = EnumAddressTypes.LandRegistryLinked;
                    break;
                case "LandRegistryLinkedForBtlHome":
                    app1Type = EnumAddressTypes.LandRegistryLinkedForBtlHome;
                    break;
                case "Mortgage":
                    app1Type = EnumAddressTypes.Mortgage;
                    break;
                case "Permanent":
                    app1Type = EnumAddressTypes.Permanent;
                    break;
                case "Previous":
                    app1Type = EnumAddressTypes.Previous;
                    break;
                case "Security":
                    app1Type = EnumAddressTypes.Security;
                    break;
                case "Unset":
                    app1Type = EnumAddressTypes.Unset;
                    break;
            }
            return app1Type;
        }
    }

    public string App1PreviousHouseNo { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/HouseNo"].ToString(); } }

    public string App1PreviousStreet1 { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/Street1"].ToString(); } }

    public string App1PreviousDistrict { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/District"].ToString(); } }

    public string App1PreviousPostTown { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/PostTown"].ToString(); } }

    public string App1PreviousCounty { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/County"].ToString(); } }

    public string App1PreviousPostcode { get { return reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/Postcode"].ToString(); } }

    public int? App1PreviousTimeAtAddressInMonths
    {
        get
        {
            int app1PreviousTimeAtAddressInMonths = 0;
            if (!int.TryParse(reader["ApplicantDetail(1)/Addresses/AddressDetail(2)/TimeAtAddressInMonths"].ToString(), out app1PreviousTimeAtAddressInMonths))
            {
                return null;
            }
            return app1PreviousTimeAtAddressInMonths;
        }
    }

    public DateTime? App1DateOfBirth
    {
        get
        {
            DateTime app1DateOfBirth;

            if (!DateTime.TryParse(reader["ApplicantDetail(1)/DateOfBirth"].ToString(), out app1DateOfBirth))
            {
                return null;
            }

            return app1DateOfBirth;
        }
    }

    public string App1EmailAddress { get { return reader["ApplicantDetail(1)/EmailAddress"].ToString(); } }

    public string App1EmployerName { get { return reader["ApplicantDetail(1)/EmployerName"].ToString(); } }

    public EnumEmploymentStatuses? App1EmploymentStatus
    {
        get
        {
            EnumEmploymentStatuses? app1EmploymentStatus = null;

            switch (reader["ApplicantDetail(1)/EmploymentStatus"].ToString())
            {
                case "Employed":
                    app1EmploymentStatus = EnumEmploymentStatuses.Employed;
                    break;
                case "Retired":
                    app1EmploymentStatus = EnumEmploymentStatuses.Retired;
                    break;
                case "SelfEmployed":
                    app1EmploymentStatus = EnumEmploymentStatuses.SelfEmployed;
                    break;
                case "SelfEmployedProfessional":
                    app1EmploymentStatus = EnumEmploymentStatuses.SelfEmployedProfessional;
                    break;
                case "UnemployedHouseperson":
                    app1EmploymentStatus = EnumEmploymentStatuses.UnemployedHouseperson;
                    break;
            }
            return app1EmploymentStatus;
        }
    }

    public string App1EmployerTelephone { get { return reader["ApplicantDetail(1)/EmployerTelephone"].ToString(); } }

    public string App1Firstname { get { return reader["ApplicantDetail(1)/Firstname"].ToString(); } }

    public int? App1Gender
    {
        get
        {
            int app1Gender = 0;

            if (!int.TryParse(reader["ApplicantDetail(1)/Gender"].ToString(), out app1Gender))
            {
                return null;
            }
            return app1Gender;
        }
    }

    public decimal? App1GrossAnnualIncome
    {
        get
        {
            decimal app1GrossAnnualIncome = 0;

            if (!decimal.TryParse(reader["ApplicantDetail(1)/GrossAnnualIncome"].ToString(), out app1GrossAnnualIncome))
            {
                return null;
            }

            return app1GrossAnnualIncome;
        }
    }

    public string SecFFPastRetirement { get { return reader["SecFFPastRetirement"].ToString(); } }

    public string App1HomeTelephone { get { return reader["ApplicantDetail(1)/HomeTelephone"].ToString(); } }

    public string App1MaidenPreviousNames { get { return reader["ApplicantDetail(1)/MaidenPreviousNames"].ToString(); } }

    public string App1Middlenames { get { return reader["ApplicantDetail(1)/Middlenames"].ToString(); } }

    public EnumMaritalStatuses? App1MaritalStatus
    {
        get
        {
            EnumMaritalStatuses? app1MaritalStatus = null;

            switch (reader["ApplicantDetail(1)/MaritalStatus"].ToString())
            {
                case "Married":
                    app1MaritalStatus = EnumMaritalStatuses.Married;
                    break;
                case "CivilPartnership":
                    app1MaritalStatus = EnumMaritalStatuses.CivilPartnership;
                    break;
                case "CoHabiting":
                    app1MaritalStatus = EnumMaritalStatuses.CoHabiting;
                    break;
                case "Divorced":
                    app1MaritalStatus = EnumMaritalStatuses.Divorced;
                    break;
                case "Seperated":
                    app1MaritalStatus = EnumMaritalStatuses.Seperated;
                    break;
                case "Single":
                    app1MaritalStatus = EnumMaritalStatuses.Single;
                    break;
                case "Widowed":
                    app1MaritalStatus = EnumMaritalStatuses.Widowed;
                    break;
            }
            return app1MaritalStatus;
        }
    }

    public string App1MobileTelephone { get { return reader["ApplicantDetail(1)/MobileTelephone"].ToString(); } }

    public string App1Occupation { get { return reader["ApplicantDetail(1)/Occupation"].ToString(); } }

    public string App1SelfEmploymentEvidenceId { get { return reader["ApplicantDetail(1)/RegularIncome/SelfEmploymentEvidenceId"].ToString(); } }

    public string App1AnnualOtherIncomeItem { get { return reader["App1AnnualOtherIncomeItem"].ToString(); } }

    public string App1AnnualOtherIncomeValue { get { return reader["App1AnnualOtherIncomeValue"].ToString(); } }

    public string App1SelfEmploymentStatusId { get { return reader["ApplicantDetail(1)/RegularIncome/SelfEmploymentStatusId"].ToString(); } }

    public EnumSelfEmploymentEvidenceTypes? App1SelfEmployedEvidenceType
    {
        get
        {
            EnumSelfEmploymentEvidenceTypes? app1SelfEmployedEvidenceType = null;

            switch (reader["ApplicantDetail(1)/SelfEmployedEvidenceType"].ToString())
            {
                case "AccountantsCertificate":
                    app1SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.AccountantsCertificate;
                    break;
                case "Notset":
                    app1SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.Notset;
                    break;
                case "Payslips":
                    app1SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.Payslips;
                    break;
                case "SA302":
                    app1SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.SA302;
                    break;
            }
            return app1SelfEmployedEvidenceType;
        }
    }

    public string App1SelfEmployedStatus { get { return reader["ApplicantDetail(1)/SelfEmployedStatus"].ToString(); } }

    public int? App1TimeInEmployment
    {
        get
        {
            int app1TimeInEmployment = 0;

            if (!int.TryParse(reader["ApplicantDetail(1)/Addresses/AddressDetail(1)/TimeAtAddressInMonths"].ToString(), out app1TimeInEmployment))
            {
                return null;
            }
            return app1TimeInEmployment;
        }
    }

    public string App1Surname { get { return reader["ApplicantDetail(1)/Surname"].ToString(); } }

    public EnumTitles? App1Title
    {
        get
        {
            EnumTitles? app1Title = null;

            switch (reader["ApplicantDetail(1)/Title"].ToString())
            {
                case "Mr":
                    app1Title = EnumTitles.Mr;
                    break;
                case "Miss":
                    app1Title = EnumTitles.Miss;
                    break;
                case "Dr":
                    app1Title = EnumTitles.Dr;
                    break;
                case "Mrs":
                    app1Title = EnumTitles.Mrs;
                    break;
                case "Ms":
                    app1Title = EnumTitles.Ms;
                    break;
                case "Prof":
                    app1Title = EnumTitles.Prof;
                    break;
                case "Sir":
                    app1Title = EnumTitles.Sir;
                    break;
            }
            return app1Title;
        }
    }

    public string App2HouseNo { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/HouseNo"].ToString(); } }

    public string App2Street1 { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/Street1"].ToString(); } }

    public string App2Street2 { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/Street2"].ToString(); } }

    public string App2District { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/District"].ToString(); } }

    public string App2PostTown { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/PostTown"].ToString(); } }

    public string App2County { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/County"].ToString(); } }

    public string App2Postcode { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/Postcode"].ToString(); } }

    public int? App2TimeAtAddressInMonths
    {
        get
        {
            int app2TimeAtAddressInMonths = 0;

            if (!int.TryParse(reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/TimeAtAddressInMonths"].ToString(), out app2TimeAtAddressInMonths))
            {
                return app2TimeAtAddressInMonths;
            }
            return app2TimeAtAddressInMonths;
        }
    }

    public EnumResidentialStatuses? App2ResidentialStatusId
    {
        get
        {
            EnumResidentialStatuses? app2ResidentialStatusId = null;

            switch (reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/ResidentialStatusId"].ToString())
            {
                case "OwnerOccupied":
                    app2ResidentialStatusId = EnumResidentialStatuses.OwnerOccupied;
                    break;
                case "BuyToLet":
                    app2ResidentialStatusId = EnumResidentialStatuses.BuyToLet;
                    break;
            }

            return app2ResidentialStatusId;
        }
    }

    public EnumAddressTypes? App2AddressType
    {
        get
        {
            EnumAddressTypes? app2AddressType = null;

            switch (reader["ApplicantDetail(2)/Addresses/AddressDetail(1)/Type"].ToString())
            {
                case "BankBuildingSociety":
                    app2AddressType = EnumAddressTypes.BankBuildingSociety;
                    break;
                case "Employer":
                    app2AddressType = EnumAddressTypes.Employer;
                    break;
                case "LandRegistry":
                    app2AddressType = EnumAddressTypes.LandRegistry;
                    break;
                case "LandRegistryForBtlHome":
                    app2AddressType = EnumAddressTypes.LandRegistryForBtlHome;
                    break;
                case "LandRegistryLinked":
                    app2AddressType = EnumAddressTypes.LandRegistryLinked;
                    break;
                case "LandRegistryLinkedForBtlHome":
                    app2AddressType = EnumAddressTypes.LandRegistryLinkedForBtlHome;
                    break;
                case "Mortgage":
                    app2AddressType = EnumAddressTypes.Mortgage;
                    break;
                case "Permanent":
                    app2AddressType = EnumAddressTypes.Permanent;
                    break;
                case "Previous":
                    app2AddressType = EnumAddressTypes.Previous;
                    break;
                case "Security":
                    app2AddressType = EnumAddressTypes.Security;
                    break;
                case "Unset":
                    app2AddressType = EnumAddressTypes.Unset;
                    break;
            }
            return app2AddressType;
        }
    }

    public string App2PreviousHouseNo { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/HouseNo"].ToString(); } }

    public string App2PreviousStreet1 { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/Street1"].ToString(); } }

    public string Appl2PreviousDistrict { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/District"].ToString(); } }

    public string App2PreviousPostTown { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/PostTown"].ToString(); } }

    public string App2PreviousCounty { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/County"].ToString(); } }

    public string App2PreviousPostcode { get { return reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/Postcode"].ToString(); } }

    public int? App2PreviousTimeAtAddressInMonths
    {
        get
        {
            int app2PreviousTimeAtAddressInMonths = 0;

            if (!int.TryParse(reader["ApplicantDetail(2)/Addresses/AddressDetail(2)/TimeAtAddressInMonths"].ToString(), out app2PreviousTimeAtAddressInMonths))
            {
                return null;
            }
            return app2PreviousTimeAtAddressInMonths;
        }
    }

    public DateTime? App2DateOfBirth
    {
        get
        {
            DateTime app2DateOfBirth;

            if (!DateTime.TryParse(reader["ApplicantDetail(2)/DateOfBirth"].ToString(), out app2DateOfBirth))
            {
                return null;
            }

            return app2DateOfBirth;
        }
    }

    public string App2EmailAddress { get { return reader["ApplicantDetail(2)/EmailAddress"].ToString(); } }

    public string App2EmployerName { get { return reader["ApplicantDetail(2)/EmployerName"].ToString(); } }

    public EnumEmploymentStatuses? App2EmploymentStatus
    {
        get
        {
            EnumEmploymentStatuses? app2EmploymentStatus = null;

            switch (reader["ApplicantDetail(1)/EmploymentStatus"].ToString())
            {
                case "Employed":
                    app2EmploymentStatus = EnumEmploymentStatuses.Employed;
                    break;
                case "Retired":
                    app2EmploymentStatus = EnumEmploymentStatuses.Retired;
                    break;
                case "SelfEmployed":
                    app2EmploymentStatus = EnumEmploymentStatuses.SelfEmployed;
                    break;
                case "SelfEmployedProfessional":
                    app2EmploymentStatus = EnumEmploymentStatuses.SelfEmployedProfessional;
                    break;
                case "UnemployedHouseperson":
                    app2EmploymentStatus = EnumEmploymentStatuses.UnemployedHouseperson;
                    break;
            }
            return app2EmploymentStatus;
        }
    }

    public string App2EmployerTelephone { get { return reader["ApplicantDetail(2)/EmployerTelephone"].ToString(); } }

    public string App2Firstname { get { return reader["ApplicantDetail(2)/Firstname"].ToString(); } }

    public int? App2Gender
    {
        get
        {
            int app2Gender = 0;

            if (!int.TryParse(reader["ApplicantDetail(2)/Gender"].ToString(), out app2Gender))
            {
                return null;
            }
            return app2Gender;
        }
    }

    public decimal? App2GrossAnnualIncome
    {
        get
        {
            decimal app2GrossAnnualIncome = 0;

            if (!decimal.TryParse(reader["ApplicantDetail(2)/GrossAnnualIncome"].ToString(), out app2GrossAnnualIncome))
            {
                return null;
            }

            return app2GrossAnnualIncome;
        }
    }

    public string App2HomeTelephone { get { return reader["ApplicantDetail(2)/HomeTelephone"].ToString(); } }

    public string App2MaidenPreviousNames { get { return reader["ApplicantDetail(2)/MaidenPreviousNames"].ToString(); } }

    public string App2Middlenames { get { return reader["ApplicantDetail(2)/Middlenames"].ToString(); } }

    public EnumMaritalStatuses? App2MaritalStatus
    {
        get
        {
            EnumMaritalStatuses? app2MaritalStatus = null;

            switch (reader["ApplicantDetail(2)/MaritalStatus"].ToString())
            {
                case "Married":
                    app2MaritalStatus = EnumMaritalStatuses.Married;
                    break;
                case "CivilPartnership":
                    app2MaritalStatus = EnumMaritalStatuses.CivilPartnership;
                    break;
                case "CoHabiting":
                    app2MaritalStatus = EnumMaritalStatuses.CoHabiting;
                    break;
                case "Divorced":
                    app2MaritalStatus = EnumMaritalStatuses.Divorced;
                    break;
                case "Seperated":
                    app2MaritalStatus = EnumMaritalStatuses.Seperated;
                    break;
                case "Single":
                    app2MaritalStatus = EnumMaritalStatuses.Single;
                    break;
                case "Widowed":
                    app2MaritalStatus = EnumMaritalStatuses.Widowed;
                    break;
            }
            return app2MaritalStatus;
        }
    }

    public string App2MobileTelephone { get { return reader["ApplicantDetail(2)/MobileTelephone"].ToString(); } }

    public string App2Occupation { get { return reader["ApplicantDetail(2)/Occupation"].ToString(); } }

    public string App2AnnualOtherIncomeItem { get { return reader["App2AnnualOtherIncomeItem"].ToString(); } }

    public string App2AnnualOtherIncomeValue { get { return reader["App2AnnualOtherIncomeValue"].ToString(); } }

    public string App2SelfEmploymentEvidenceId { get { return reader["ApplicantDetail(2)/RegularIncome/SelfEmploymentEvidenceId"].ToString(); } }

    public string App2SelfEmploymentStatusId { get { return reader["ApplicantDetail(2)/RegularIncome/SelfEmploymentStatusId"].ToString(); } }

    public EnumSelfEmploymentEvidenceTypes? App2SelfEmployedEvidenceType
    {
        get
        {
            EnumSelfEmploymentEvidenceTypes? app2SelfEmployedEvidenceType = null;

            switch (reader["ApplicantDetail(1)/SelfEmployedEvidenceType"].ToString())
            {
                case "AccountantsCertificate":
                    app2SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.AccountantsCertificate;
                    break;
                case "Notset":
                    app2SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.Notset;
                    break;
                case "Payslips":
                    app2SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.Payslips;
                    break;
                case "SA302":
                    app2SelfEmployedEvidenceType = EnumSelfEmploymentEvidenceTypes.SA302;
                    break;
            }
            return app2SelfEmployedEvidenceType;
        }
    }

    public string App2SelfEmployedStatus { get { return reader["ApplicantDetail(2)/SelfEmployedStatus"].ToString(); } }

    public int? App2TimeInEmployment
    {
        get
        {
            int app2TimeInEmployment = 0;

            if (!int.TryParse(reader["ApplicantDetail(2)/TimeInEmployment"].ToString(), out app2TimeInEmployment))
            {
                return null;
            }
            return app2TimeInEmployment;
        }
    }

    public string App2Surname { get { return reader["ApplicantDetail(2)/Surname"].ToString(); } }

    public EnumTitles? App2Title
    {
        get
        {
            EnumTitles? app2Title = null;

            switch (reader["ApplicantDetail(2)/Title"].ToString())
            {
                case "Mr":
                    app2Title = EnumTitles.Mr;
                    break;
                case "Miss":
                    app2Title = EnumTitles.Miss;
                    break;
                case "Dr":
                    app2Title = EnumTitles.Dr;
                    break;
                case "Mrs":
                    app2Title = EnumTitles.Mrs;
                    break;
                case "Ms":
                    app2Title = EnumTitles.Ms;
                    break;
                case "Prof":
                    app2Title = EnumTitles.Prof;
                    break;
                case "Sir":
                    app2Title = EnumTitles.Sir;
                    break;
            }
            return app2Title;
        }
    }

    public decimal? App1ExpInsurance
    {
        get
        {
            decimal app1ExpInsurance = 0;

            if (!decimal.TryParse(reader["App1ExpInsurance"].ToString(), out app1ExpInsurance))
            {
                return null;
            }

            return app1ExpInsurance;
        }
    }

    public decimal? App1ExpChildcareMaintenance
    {
        get
        {
            decimal app1ExpInsurance = 0;

            if (!decimal.TryParse(reader["App1ExpChildcareMaintenance"].ToString(), out app1ExpInsurance))
            {
                return null;
            }

            return app1ExpInsurance;
        }
    }

    public decimal? App1ExpCouncilTax
    {
        get
        {
            decimal app1ExpCouncilTax = 0;

            if (!decimal.TryParse(reader["App1ExpCouncilTax"].ToString(), out app1ExpCouncilTax))
            {
                return null;
            }

            return app1ExpCouncilTax;
        }
    }

    public decimal? App1ExpGasElectricity
    {
        get
        {
            decimal app1ExpGasElectricity = 0;

            if (!decimal.TryParse(reader["App1ExpGasElectricity"].ToString(), out app1ExpGasElectricity))
            {
                return null;
            }

            return app1ExpGasElectricity;
        }
    }

    public decimal? App1ExpWaterRates
    {
        get
        {
            decimal app1ExpGasElectricity = 0;

            if (!decimal.TryParse(reader["App1ExpWaterRates"].ToString(), out app1ExpGasElectricity))
            {
                return null;
            }

            return app1ExpGasElectricity;
        }
    }

    public decimal? App1ExpFoodToiletCleaning
    {
        get
        {
            decimal app1ExpFoodToiletCleaning = 0;

            if (!decimal.TryParse(reader["App1ExpFoodToiletCleaning"].ToString(), out app1ExpFoodToiletCleaning))
            {
                return null;
            }

            return app1ExpFoodToiletCleaning;
        }
    }

    public decimal? App1ExpTravel
    {
        get
        {
            decimal app1ExpTravel = 0;

            if (!decimal.TryParse(reader["App1ExpTravel"].ToString(), out app1ExpTravel))
            {
                return null;
            }

            return app1ExpTravel;
        }
    }

    public decimal? App1ExpPetrol
    {
        get
        {
            decimal app1ExpPetrol = 0;

            if (!decimal.TryParse(reader["App1ExpPetrol"].ToString(), out app1ExpPetrol))
            {
                return null;
            }

            return app1ExpPetrol;
        }
    }

    public decimal? App1ExpEducation
    {
        get
        {
            decimal app1ExpEducation = 0;

            if (!decimal.TryParse(reader["App1ExpEducation"].ToString(), out app1ExpEducation))
            {
                return null;
            }

            return app1ExpEducation;
        }
    }

    public decimal? App1ExpClothing
    {
        get
        {
            decimal app1ExpClothing = 0;

            if (!decimal.TryParse(reader["App1ExpClothing"].ToString(), out app1ExpClothing))
            {
                return null;
            }

            return app1ExpClothing;
        }
    }

    public decimal? App1ExpHolidaysLeisure
    {
        get
        {
            decimal app1ExpHolidaysLeisure = 0;

            if (!decimal.TryParse(reader["App1ExpHolidaysLeisure"].ToString(), out app1ExpHolidaysLeisure))
            {
                return null;
            }

            return app1ExpHolidaysLeisure;
        }
    }

    public decimal? App1ExpTelephone
    {
        get
        {
            decimal app1ExpTelephone = 0;

            if (!decimal.TryParse(reader["App1ExpTelephone"].ToString(), out app1ExpTelephone))
            {
                return null;
            }

            return app1ExpTelephone;
        }
    }

    public decimal? App1ExpOther
    {
        get
        {
            decimal app1ExpOther = 0;

            if (!decimal.TryParse(reader["App1ExpOther"].ToString(), out app1ExpOther))
            {
                return null;
            }

            return app1ExpOther;
        }
    }

    public EnumLoanPurposes? LoanPurpose
    {
        get
        {
            EnumLoanPurposes? loanPurpose = null;

            switch (reader["LoanPurpose/EnumLoanPurposes"].ToString())
            {
                case "BusinessPurposes":
                    loanPurpose = EnumLoanPurposes.BusinessPurposes;
                    break;
                case "Consolidation":
                    loanPurpose = EnumLoanPurposes.Consolidation;
                    break;
                case "GiftingMoneyToAChild":
                    loanPurpose = EnumLoanPurposes.GiftingMoneyToAChild;
                    break;
                case "HomeImprovement":
                    loanPurpose = EnumLoanPurposes.HomeImprovement;
                    break;
                case "Other":
                    loanPurpose = EnumLoanPurposes.Other;
                    break;
                case "PropertyPurchase":
                    loanPurpose = EnumLoanPurposes.PropertyPurchase;
                    break;
                case "RepayIva":
                    loanPurpose = EnumLoanPurposes.RepayIva;
                    break;
                case "RepayTaxBill":
                    loanPurpose = EnumLoanPurposes.RepayTaxBill;
                    break;
                case "TransferOfEquity":
                    loanPurpose = EnumLoanPurposes.TransferOfEquity;
                    break;
                default:
                    loanPurpose = EnumLoanPurposes.Other;
                    break;
            }
            return loanPurpose;
        }
    }

    public string SecurityHouseNo { get { return reader["SecurityAddress/HouseNo"].ToString(); } }

    public string SecurityStreet1 { get { return reader["SecurityAddress/Street1"].ToString(); } }

    public string SecurityStreet2 { get { return reader["SecurityAddress/Street2"].ToString(); } }

    public string SecurityDistrict { get { return reader["SecurityAddress/District"].ToString(); } }

    public string SecurityPostTown { get { return reader["SecurityAddress/PostTown"].ToString(); } }

    public string SecurityCounty { get { return reader["SecurityAddress/County"].ToString(); } }

    public string SecurityPostcode { get { return reader["SecurityAddress/Postcode"].ToString(); } }

    public int? SecurityTimeAtAddressInMonths
    {
        get
        {
            int securityTimeAtAddressInMonths = 0;

            if (!int.TryParse(reader["SecurityAddress/TimeAtAddressInMonths"].ToString(), out securityTimeAtAddressInMonths))
            {
                return null;
            }
            return securityTimeAtAddressInMonths;
        }
    }

    public string SecurityResidentialStatusId { get { return reader["SecurityAddress/ResidentialStatusId"].ToString(); } }

    public EnumAddressTypes? SecurityAddressType
    {
        get
        {
            EnumAddressTypes? securityAddressType = null;

            switch (reader["SecurityAddress/Type"].ToString())
            {
                case "BankBuildingSociety":
                    securityAddressType = EnumAddressTypes.BankBuildingSociety;
                    break;
                case "Employer":
                    securityAddressType = EnumAddressTypes.Employer;
                    break;
                case "LandRegistry":
                    securityAddressType = EnumAddressTypes.LandRegistry;
                    break;
                case "LandRegistryForBtlHome":
                    securityAddressType = EnumAddressTypes.LandRegistryForBtlHome;
                    break;
                case "LandRegistryLinked":
                    securityAddressType = EnumAddressTypes.LandRegistryLinked;
                    break;
                case "LandRegistryLinkedForBtlHome":
                    securityAddressType = EnumAddressTypes.LandRegistryLinkedForBtlHome;
                    break;
                case "Mortgage":
                    securityAddressType = EnumAddressTypes.Mortgage;
                    break;
                case "Permanent":
                    securityAddressType = EnumAddressTypes.Permanent;
                    break;
                case "Previous":
                    securityAddressType = EnumAddressTypes.Previous;
                    break;
                case "Security":
                    securityAddressType = EnumAddressTypes.Security;
                    break;
                case "Unset":
                    securityAddressType = EnumAddressTypes.Unset;
                    break;
            }
            return securityAddressType;
        }
    }

    public decimal? App1SuppIncome
    {
        get
        {
            decimal app1SuppIncome = 0;

            if (!decimal.TryParse(reader["App1SuppIncome"].ToString(), out app1SuppIncome))
            {
                return null;
            }

            return app1SuppIncome;
        }
    }

    public decimal? App2SuppIncome
    {
        get
        {
            decimal app2SuppIncome = 0;

            if (!decimal.TryParse(reader["App2SuppIncome"].ToString(), out app2SuppIncome))
            {
                return null;
            }

            return app2SuppIncome;
        }
    }

}
#endregion

#region Post Models

class ResponseMessage
{
    public string Message { get; set; }
}

class SubmitReferralDetails
{
    public int ShawbrookCaseId { get; set; }

    public ICollection<EnumReferralReasons> Reasons { get; set; }

    public string OtherReason { get; set; }

    public string CaseMerits { get; set; }
}

enum EnumReferralReasons
{
    RestrictedLender = 1,
    ApplicantsAge = 3,
    Arrears = 4,
    Bankruptcy = 5,
    CCJs = 7,
    CreditScore = 8,
    Defaults = 9,
    Employment = 11,
    IncomeBelowMinimum = 14,
    IVA = 15,
    LTI = 17,
    LTV = 18,
    MorgageHistory = 19,
    PertnerNotOnLoan = 20,
    PaymentProfile = 21,
    PropertyType = 22,
    RecentMorgageSecuredLoan = 24,
    TimeInProperty = 25,
    ExLocalAuthorityHouseAboveMaximumLTV = 28,
    ExlocalAuthorityFlat = 29,
    ValuationBelowMinimum = 30,
    NumberOfPropertiesInPortfolio = 31,
    RentalCover = 32,
    PropertyOutsideOfLendingArea = 33,
    LoanSize = 34,
    Affordability = 35,
    Arrangement = 36,
    PrivateFlatOverSixStoreys = 37,
    PrivateLeaseholdFlatOverMaximumLTV = 38,
    HistoricCcjDefault = 39,
    DebtManagement = 40,
    UnacceptableLender = 41,
    ExLocalAuthorityFlatBelowMinimumValuation = 42,
    ExLocalAuthorityFlatOnBTLoan = 43,
    GrossAnnualIncomeBelowMinimum = 44,
    ExLocalAuthorityFlatAboveMaximumLTV = 45,
    LandRegistryNameMismatch = 46,
    RentalCoverBelowPlanMinimum = 47,
    MortgageNameMismatch = 48,
    ExLocalAuthorityFlatOverSixStoreys = 49,
    TimeLeftOnLease = 50,
    LandRegistryTitleNotFoundForSecurityAddress = 52,
    LandRegistryTitleNotFoundForHomeAddress = 53,
    ApplicantDoesNotOwnResidentialProperty = 54,
    FamilyDwellingInSecurityAddress = 55,
    PaymentHoliday = 56,
    Expenditure = 57,
    LoanPurpose = 58,
    ExLocalAuthorityFlatAboveNetLoanAmount = 59,
    SupplementaryIncomeExceedsEarnedIncome = 60,
    TimeInIvaBelowPlanMinimum = 61,
    Over80AfterLoan = 62,
    RepayIVA = 65,
    BusinessPurposesConsolidationsTooHigh = 66,
    LendingIntoRetirement = 125,
    FlatAboveCommercialPremisesExceedsMaxLTV = 126,
    FlatAboveFoodOutlet = 127
}
public class AddressDetail
{
    public string HouseNo { get; set; }
    public string Street1 { get; set; }
    public string Street2 { get; set; }
    public string District { get; set; }
    public string PostTown { get; set; }
    public string County { get; set; }
    public string Postcode { get; set; }
    public EnumResidentialStatuses ResidentialStatusId { get; set; }
    public int TimeAtAddressInMonths { get; set; }
    public EnumAddressTypes Type { get; set; }
}

public class ApplicantDetail
{
    public EnumTitles Title { get; set; }
    public string Firstname { get; set; }
    public string Surname { get; set; }
    public string Middlenames { get; set; }
    public string MaidenPreviousNames { get; set; }
    public DateTime DateOfBirth { get; set; }
    public EnumMaritalStatuses MaritalStatus { get; set; }
    public string HomeTelephone { get; set; }
    public string MobileNumber { get; set; }
    public string EmailAddress { get; set; }
    public int Gender { get; set; }
    public int NationalityCountryId { get; set; }
    public int BirthCountryId { get; set; }
    public int EmploymentStatus { get; set; }
    public string EmployerName { get; set; }
    public string EmployerTelephone { get; set; }
    public decimal GrossAnnualIncome { get; set; }
    public string Occupation { get; set; }
    public IncomeRecord RegularIncome { get; set; }
    public int TimeInEmployment { get; set; }
    public EnumSelfEmploymentEvidenceTypes SelfEmployedEvidenceType { get; set; }
    public EnumEmploymentStatuses SelfEmployedStatus { get; set; }
    public IEnumerable<AddressDetail> Addresses { get; set; }

    public EnumLiRExplanations LiRExplanation { get; set; }
    public string LirExplanationOther { get; set; }
    public EnumLiRExplanations LiRWorkingExplanation { get; set; }
    public string LirWorkingExplanationOther { get; set; }

}

public enum EnumAddressTypes
{
    Unset = 0,
    Security = 1,
    Permanent = 2,
    Previous = 3,
    Employer = 4,
    BankBuildingSociety = 5,
    LandRegistry = 6,
    Mortgage = 7,
    LandRegistryForBtlHome = 8,
    LandRegistryLinked = 9,
    LandRegistryLinkedForBtlHome = 10,
}

public enum EnumLiRExplanations
{
    NotSet = 0,
    CarryOnWorking = 1,
    PensionIncome = 2,
    DownsizeProperty = 3,
    AdditionalBTLProperties = 4,
    Other = 5
}

public enum EnumMaritalStatuses
{
    Married = 1,
    Single = 2,
    Divorced = 3,
    Widowed = 4,
    CoHabiting = 5,
    CivilPartnership = 6,
    Seperated = 7,
}

public enum EnumPropertyTypes
{
    House = 1,
    FlatMoreThan6Storeys = 4,
    Maisonette = 6,
    ConvertedFlat = 7,
    Bungalow = 8,
    PurposeBuiltFlat = 9,
}

public enum EnumResidentialStatuses
{
    OwnerOccupied = 1,
    BuyToLet = 2,
}

public enum EnumSelfEmploymentEvidenceTypes
{
    Notset = 0,
    AccountantsCertificate = 1,
    Payslips = 2,
    SA302 = 3,
}

public enum EnumTitles
{
    Mr = 1,
    Mrs = 2,
    Miss = 3,
    Ms = 4,
    Sir = 5,
    Dr = 6,
    Prof = 7,
}

public struct ExpenditureItem
{
    public enum EnumExpenditureCauses
    {
        BuildingsAndContentInsurance = 0,
        GroundRentOrServiceCharge,
        MaintenanceOrChildSupport,
        CouncilTax,
        GasElectricityFuel,
        Water,
        Shopping,
        Transport,
        Education,
        ClothingAndFootWear,
        EntertainmentAndRecreation,
        Communication,
        AnyOtherExpenses,
    }

    public EnumExpenditureCauses Cause { get; set; }
    public decimal Amount { get; set; }
    public string Description { get { return Cause.ToString(); } }
    public string ExplanatoryNote { get; set; }
}

public struct FeeNew
{

    public EnumFeeType FeeType { get; set; }

    public EnumFeesPaymentArrangements FeePaymentArrangement { get; set; }

    public decimal FeeAmount { get; set; }
}


public class IncomeRecord
{
    public decimal CarersAllowance { get; set; }
    public decimal ChildTaxCredit { get; set; }
    public decimal DisabilityLivingAllowance { get; set; }
    public decimal FosterCarerIncome { get; set; }
    public decimal IncapacityBenefit { get; set; }
    public decimal Income { get; set; }
    public decimal MaintenanceCSA { get; set; }
    public decimal PrivatePension { get; set; }
    public decimal RentalIncome { get; set; }
    public decimal SecondJobIncome { get; set; }
    public decimal SevereDisabilityAllowance { get; set; }
    public decimal StatePensionCredit { get; set; }
    public decimal WorkingTaxCredit { get; set; }
}

public enum EnumLoanTypes
{
    SecuredLoan = 1,
    FirstChargeBtl = 2,
    SecondChargeBtl = 3,
    FirstChargeDeeds = 4,
    FurtherAdvances = 5
}

public enum EnumEmploymentStatuses
{
    Employed = 1,
    SelfEmployed = 2,
    Retired = 3,
    UnemployedHouseperson = 4,
    SelfEmployedProfessional = 5,
}

public enum EnumLoanPurposes
{
    Consolidation = 1,
    HomeImprovement = 2,
    TransferOfEquity = 6,
    RepayIva = 9,
    RepayTaxBill = 10,
    GiftingMoneyToAChild = 11,
    PropertyPurchase = 12,
    BusinessPurposes = 13,
    Other = 14
}

public class LoanReasons
{
    public bool Consolidation { get; set; }
    public bool HomeImprovement { get; set; }
    public bool TransferOfEquity { get; set; }
    public bool RepayIva { get; set; }
    public bool RepayTaxBill { get; set; }
    public bool GiftingMoneyToAChild { get; set; }
    public bool PropertyPurchase { get; set; }
    public bool BusinessPurposes { get; set; }
    public bool Other { get; set; }

    public static implicit operator EnumLoanPurposes[](LoanReasons reasons)
    {
        var loanPurposes = new List<EnumLoanPurposes>();

        if (reasons.Consolidation)
        {
            loanPurposes.Add(EnumLoanPurposes.Consolidation);
        }

        if (reasons.HomeImprovement)
        {
            loanPurposes.Add(EnumLoanPurposes.HomeImprovement);
        }

        if (reasons.TransferOfEquity)
        {
            loanPurposes.Add(EnumLoanPurposes.TransferOfEquity);
        }

        if (reasons.RepayIva)
        {
            loanPurposes.Add(EnumLoanPurposes.RepayIva);
        }

        if (reasons.RepayTaxBill)
        {
            loanPurposes.Add(EnumLoanPurposes.RepayTaxBill);
        }

        if (reasons.GiftingMoneyToAChild)
        {
            loanPurposes.Add(EnumLoanPurposes.GiftingMoneyToAChild);
        }

        if (reasons.PropertyPurchase)
        {
            loanPurposes.Add(EnumLoanPurposes.PropertyPurchase);
        }

        if (reasons.BusinessPurposes)
        {
            loanPurposes.Add(EnumLoanPurposes.BusinessPurposes);
        }

        if (reasons.Other)
        {
            loanPurposes.Add(EnumLoanPurposes.Other);
        }

        return loanPurposes.ToArray();
    }

    public static implicit operator LoanReasons(EnumLoanPurposes[] loanPurposes)
    {
        return new LoanReasons
        {
            Consolidation = loanPurposes.Contains(EnumLoanPurposes.Consolidation),
            HomeImprovement = loanPurposes.Contains(EnumLoanPurposes.HomeImprovement),
            TransferOfEquity = loanPurposes.Contains(EnumLoanPurposes.TransferOfEquity),
            RepayIva = loanPurposes.Contains(EnumLoanPurposes.RepayIva),
            RepayTaxBill = loanPurposes.Contains(EnumLoanPurposes.RepayTaxBill),
            GiftingMoneyToAChild = loanPurposes.Contains(EnumLoanPurposes.GiftingMoneyToAChild),
            PropertyPurchase = loanPurposes.Contains(EnumLoanPurposes.PropertyPurchase),
            BusinessPurposes = loanPurposes.Contains(EnumLoanPurposes.BusinessPurposes),
            Other = loanPurposes.Contains(EnumLoanPurposes.Other)
        };
    }
}

public class QuickQuoteDetail : INotifyPropertyChanged
{
    private EnumLoanTypes _loadType;
    private bool _IsNeeded;

    public QuickQuoteDetail()
    {
        this.PropertyChanged += HandlePropertyChanged;
    }

    private void HandlePropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == "LoanType")
        {
            IsNeeded = true;
        }
    }


    public EnumLoanTypes LoanType
    {
        get { return _loadType; }
        set
        {
            _loadType = value;
            //OnPropertyChanged("LoanType");
        }
    }

    public bool IsNeeded
    {
        get { return LoanType == EnumLoanTypes.SecondChargeBtl; }
        set
        {
            _IsNeeded = value;
            //OnPropertyChanged("IsNeeded");
        }

    }

    public EnumEmploymentStatuses EmploymentStatus { get; set; }

    public EnumLoanPurposes[] LoanPurpose { get { return LoanReasons; } }

    public string LoanPurposeOtherExplanation { get; set; }

    public decimal NetLoanAmount { get; set; }

    public int LoanTerm { get; set; }

    public decimal GrossAnnualIncomeMainApplicant { get; set; }

    public decimal GrossAnnualIncomeOtherApplicants { get; set; }

    public decimal SupplementaryIncomeAllApplicants { get; set; }

    public decimal MortgageBalance { get; set; }

    public decimal EstimatedPropertyValue { get; set; }

    public decimal BrokerFee { get; set; }

    public decimal MonthlyMortgagePayment { get; set; }

    public decimal MonthlyRentalIncome { get; set; }

    public int NumberOfChildrenUnder18 { get; set; }

    public AddressDetail SecurityAddress { get; set; }

    [JsonIgnore]
    public LoanReasons LoanReasons { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;


    //protected virtual void OnPropertyChanged(string propertyName = null)
    //{
    //    PropertyChangedEventHandler handler = PropertyChanged;
    //    if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    //}

}

//public FirstChargeDetail FirstChargeDetail { get; set; }

public class FirstChargeDetail
{
    public decimal EstimatedPropertyValue { get; set; }
    public int MortgageCompanyId { get; set; }
    public decimal MortgageBalance { get; set; }
    public bool IsInterestOnly { get; set; }
    public bool IsFixedTerm { get; set; }
    public int FixedTermRemaining { get; set; }

}

public class QuotationSoftSearchDetail
{

    public EnumLoanTypes LoanType { get; set; }
    public EnumLoanPurposes[] LoanPurpose { get { return LoanReasons; } }
    public string LoanPurposeOtherExplanation { get; set; }
    public decimal NetLoanAmount { get; set; }
    public int LoanTerm { get; set; }
    public decimal SupplementaryIncomeAllApplicants { get; set; }
    //public decimal MortgageBalance { get; set; }
    //public decimal EstimatedPropertyValue { get; set; }
	public List<FeeNew> Fees { get; set; }


   // public decimal BrokerFee { get; set; }
    public decimal MonthlyMortgagePayment { get; set; }
    public decimal MonthlyRentalIncome { get; set; }

    public IEnumerable<ApplicantDetail> Applicants { get; set; }
    //public int MortgageCompanyId { get; set; }
    public EnumPropertyTypes PropertyType { get; set; }
    public bool IsExLocalAuthority { get; set; }
    public IEnumerable<ExpenditureItem> Expenditure { get; set; }
    public Guid DPAAuthorisationToken { get; set; }
    public bool MarketingOptOutShawbrook { get; set; }
    public bool MarketingOptOutThirdParties { get; set; }
    public string CaseOwnerEmailAddress { get; set; }
    public int NumberOfChildrenUnder18 { get; set; }
    public AddressDetail SecurityAddress { get; set; }


    
    public bool BrokerIsAdvisor { get; set; }
    
    public AdvisingBrokerDetail AdvisingBroker { get; set; }

    public class AdvisingBrokerDetail
    {
        public string CompanyName { get; set; }
        public string FcaNumber { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public string WebsiteUrl { get; set; }
        public AddressDetail Address { get; set; }
    }

  

    public class CountryDetail
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }




    public enum EnumFeeType{

    Lender = 1,
    Broker = 2,
    Advise = 3,
    TelegraphicTransfer = 4,

}

    public enum EnumFeesPaymentArrangements{

    PaidUpFront = 1,
    AddToLoan,
    DeductedFromNetAdvance,
}

    public class Fee{

    public Enum FeeTypeFeeType {get;set;}
    public Enum FeesPaymentArrangementsFeePaymentArrangements {get;set;}
    public decimal FeeAmount {get;set;}
}


    public bool NotConsumerBtlCustomer {get; set;}


    public class GetDocPackDetail {

        public int ShawbrookCaseId {get; set;}
        public bool ReturnDuplex {get; set;}
        public DocPackType DocPackType {get; set;}
}

    public enum DocPackType {

        Day1=0,
        Day1StartConsideration=1,
        Signable=2,
        EsisOnly=3,
}



    [JsonIgnore]
    public LoanReasons LoanReasons { get; set; }

    public FirstChargeDetail FirstChargeDetail { get; set; }


}

#endregion

#region Response Models
public class Applicant : ICreditSearchableApplicant
{
    public List<CoreAddress> Addresses { get; set; }
    public string AdverseCreditExplanation { get; set; }
    public int? Age { get; set; }
    public int ApplicantId { get; set; }
    [XmlIgnore]
    public List<ICreditSearchableAddress> CreditSearchableAddresses { get; set; }
    public int? CreditSearchFootPrintRefId { get; set; }
    public int? CreditSearchReferenceID { get; set; }
    public DateTime? CreditSearchRequestDate { get; set; }
    public bool DirectDebitAccountHolder { get; set; }


    public DateTime DoB { get; set; }
    public string ElectoralRollExplanation { get; set; }

    public string EmailAddress { get; set; }

    public string EmployerName { get; set; }

    public string EmployerTelephone { get; set; }

    public int EmploymentStatus { get; set; }

    public string Firstname { get; set; }
    public string Fullname { get; set; }

    public int Gender { get; set; }

    public decimal GrossAnnualIncome { get; set; }
    public bool HasLendingIntoRetirementExplanation { get; set; }

    public string HomeTelephone { get; set; }
    public bool IsJointApplicant { get; set; }
    public bool IsLendingIntoRetirement { get; set; }


    public string MaidenPreviousNames { get; set; }

    public int MaritalStatus { get; set; }

    public string Middlenames { get; set; }

    public string MobileNumber { get; set; }

    public string Occupation { get; set; }
    public int Ordinal { get; set; }
    public IncomeRecord RegularIncome { get; set; }

    [DisplayName("Title")]
    public int SelectedTitle { get; set; }
    [DisplayName("Self employed evidence")]

    public int SelfEmployedEvidenceType { get; set; }
    [DisplayName("Self employed status")]

    public int SelfEmployedStatus { get; set; }
    [DisplayName("Surname")]

    public string Surname { get; set; }
    [DisplayName("Time in employment")]
    public int? TimeInEmployment { get; set; }
    public string TitleString { get; set; }
}

public class BadRequestResponse
{
    public string Message { get; set; }

    public Dictionary<string, IEnumerable<string>> ModelState { get; set; }
}

public class CaisData
{
    public string ApplicantId { get; set; }
    public string AddressId { get; set; }
    public string Address { get; set; }
    public string WorstStatus { get; set; }
    public string DateOfBirth { get; set; }
    public string PaymentProfile { get; set; }
    public string CompanyType { get; set; }
    public string AccountType { get; set; }
    public string AccountCode { get; set; }
    public string AccountGroup { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public DateTime? PaymentStartDate { get; set; }
    public DateTime? SettlementDate { get; set; }
    public decimal MonthlyPayment { get; set; }
    public decimal Balance { get; set; }
    public string BalanceNarrative { get; set; }
    public string BalanceCaption { get; set; }
    public decimal? DefaultBalance { get; set; }
    public decimal? CreditLimit { get; set; }
    public DateTime? CaisLastUpdated { get; set; }
    public string BureauRefCategory { get; set; }
    public string AddressType { get; set; }
    public string Title { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string LenderLoan { get; set; }
    public string AccountNumber { get; set; }
    public Decimal? StartBalance { get; set; }
    public int NoOfPayments { get; set; }
    public string PaymentFrequency { get; set; }
    public string ClientNumber { get; set; }
    public string QualityFlag { get; set; }
    public string QualityIndicator1 { get; set; }
    public string QualityIndicator2 { get; set; }
    public string DataFrom { get; set; }
    public int RepaymentPeriod { get; set; }
    public decimal LumpPayment { get; set; }
    public bool PromotionalRate { get; set; }
    public bool MinimumPayment { get; set; }
    public DateTime? ReposessionDate { get; set; }
    public string JointAccountFlag { get; set; }
    public string JointAccountApplicantIds { get; set; }
    public string JointAccountSearchIds { get; set; }
    public int Status1To2 { get; set; }
    public int StatusTo3 { get; set; }
    public DateTime? DefaultDate { get; set; }
    public bool IsMortgage { get; set; }
}

public class Ccj
{
    /// <summary>
    /// Supplied case number
    /// </summary>
    public string CaseNumber { get; set; }

    /// <summary>
    /// Supplied court code
    /// </summary>
    public string CourtCode { get; set; }

    /// <summary>
    /// Date of court hearing
    /// </summary>
    public DateTime CourtDate { get; set; }


    /// <summary>
    /// Name of presiding court
    /// </summary>
    public string CourtName { get; set; }


    /// <summary>
    /// Date of birth if supplied
    /// </summary>
    public DateTime? DateOfBirth { get; set; }


    /// <summary>
    /// First name
    /// </summary>
    public string Forename { get; set; }

    /// <summary>
    /// Translated from equifax supplied info...A or B = applicant, C to H = associate
    /// </summary>
    public NameMatchIndictors NameMatchIndicator { get; set; }

    /// <summary>
    /// Date the judgement was satisfied (if that is the case)
    /// </summary>
    public DateTime? SatisfiedDate { get; set; }

    /// <summary>
    /// Middle names
    /// </summary>
    public string SecondName { get; set; }

    /// <summary>
    /// Last name
    /// </summary>
    public string Surname { get; set; }

    /// <summary>
    /// Title
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// Type of judgement.  See Enum for details
    /// </summary>
    public EnumCcjJudgementTypes JudgementType { get; set; }

    /// <summary>
    /// Judgement value
    /// </summary>
    public decimal Value { get; set; }
}

/// <summary>
/// Name match indicator
/// </summary>
public enum NameMatchIndictors
{
    Unknown = 0,
    Applicant = 1,
    Associate = 2
}

public class CoreAddress : ICreditSearchableAddress
{
    [DisplayName("County")]
    public string County { get; set; }

    public DateTime DateMovedIn { get; set; }
    [DisplayName("District")]
    public string District { get; set; }
    public string ExternalRefId { get; set; }
    [DisplayName("House number")]

    public string HouseNo { get; set; }
    public int Id { get; set; }
    [DisplayName("Postcode")]

    public string Postcode { get; set; }
    [DisplayName("Post town")]

    public string PostTown { get; set; }
    public string PtcAbsCode { get; set; }
    [DisplayName("Residential status")]

    public int ResidentialStatusId { get; set; }
    [DisplayName("Street 1")]
    public string Street1 { get; set; }
    [DisplayName("Street 2")]
    public string Street2 { get; set; }

    public int TimeAtAddressInMonths { get; set; }

    public int TimeAtAddressSplitMonths { get; set; }

    public int TimeAtAddressSplitYears { get; set; }
    public EnumAddressTypes Type { get; set; }
}

public class CreditData
{
    /// <summary>
    /// Credit search header data
    /// </summary>
    public CreditSearchHeader CreditSearchHeader { get; set; }

    /// <summary>
    /// Cais data
    /// </summary>
    public IEnumerable<CaisData> CaisData { get; set; }

}

public class CreditSearchHeader
{
    /// <summary>
    /// Overall is the app an AML Refer
    /// </summary>
    public bool AMLRefer { get; set; }

    /// <summary>
    /// Is applicant 1 an AML refer
    /// </summary>
    public bool AMLReferApp1 { get; set; }

    /// <summary>
    /// Is applicant 2 an AML refer
    /// </summary>
    public bool AMLReferApp2 { get; set; }

    /// <summary>
    /// The applicants that were searched
    /// </summary>
    public IEnumerable<Applicant> Applicants { get; set; }

    /// <summary>
    /// Any CCJs
    /// </summary>
    public IEnumerable<Ccj> CCJs { get; set; }

    /// <summary>
    /// No of applicable CCJs
    /// </summary>
    public int? NoOfCCJs { get; set; }

    public int? Score { get; set; }
}

public enum EnumCcjJudgementTypes
{
    Unknown = 0,
    AdjudicationBankruptcy = 1,
    AdministrationOrder = 2,
    AdministrationOrderSuspended = 3,
    AdministrationOrderRescindedRevoked = 4,
    AdministrationOrderSuperseded = 5,
    BankruptcyOrder = 6,
    CourtDecreeScotland = 7,
    CourtDecreePaid = 8,
    UnenforcedCourtDecree = 9,
    CountyCourtJudgement = 10,
    CountyCourtJudgementPaid = 11,
    UnenforcedCountCourtJudgement = 12,
    CertificateOfUnenforceability = 13,
    DeedOfArrangement = 14,
    OrderOfDischarge = 15,
    PetitionForDischargeOfSequestration = 16,
    SequestrationActioned = 17,
    DischargeOfSequestration = 18,
    PetitionForSequestration = 19,
    TrustDeeds = 20,
    VoluntaryArrangement = 21
}

public class FailureReasonItem
{
    public string FailureMessage { get; set; }
    public int RuleId { get; set; }
}

public interface ICreditSearchableAddress
{
    DateTime DateMovedIn { get; set; }
    string HouseNo { get; set; }
    string Postcode { get; set; }
    string PtcAbsCode { get; set; }
    EnumAddressTypes Type { get; set; }
}

public interface ICreditSearchableApplicant
{
    [XmlIgnore]
    List<ICreditSearchableAddress> CreditSearchableAddresses { get; set; }
    int? CreditSearchFootPrintRefId { get; set; }
    int? CreditSearchReferenceID { get; set; }
    DateTime DoB { get; set; }
    string Firstname { get; set; }
    bool IsJointApplicant { get; set; }
    string MaidenPreviousNames { get; set; }
    string Middlenames { get; set; }
    string Surname { get; set; }
    string TitleString { get; set; }
}

public class ProductCalculatedDetails
{
    public decimal ActualBaseCommissionPercentage { get; set; } //No
    public decimal ArrangementFee { get; set; }
    public decimal BaseMinusFee { get; set; } //No
    public decimal BrokerFee { get; set; }
    public decimal Commission { get; set; }
    public double FixedAnnualRate { get; set; }
    public decimal FixedInterestCharge { get; set; } //No
    public decimal? FixedTerm { get; set; }
    public int? FixedTermInMonths { get; set; } //No
    public decimal GrossLoanAmount { get; set; }
    public bool IsFixedOrVariable { get; set; }
    public decimal LegalFee { get; set; }
    public decimal LenderFee { get; set; }
    public int LtvRangeId { get; set; } //No
    public decimal? MaxBrokerFee { get; set; }
    public decimal MaxLoan { get; set; }
    public decimal MaxLTV { get; set; }
    public decimal MinLoan { get; set; }
    public decimal MinLTV { get; set; }
    public decimal MonthlyRepayment { get; set; }
    public string Plan { get; set; }
    public decimal PostFixedInterestCharge { get; set; } //No
    public decimal? PostFixedRate { get; set; } //No
    public int ProductEngineGroupId { get; set; } //No
    public int ProductId { get; set; } //No
    public decimal Rate { get; set; }
    public decimal? RepaymentFixed { get; set; }//No
    public decimal? RepaymentPostFixed { get; set; }//No
    public List<FailureReasonItem> RuleFailureReasons { get; set; }
    public decimal Settle3Quarter { get; set; }
    public decimal SettleHalf { get; set; }
    public decimal SettleQuarter { get; set; }
    public decimal StressedPayment { get; set; }
    public decimal TotalAmountToRepay { get; set; }
    public decimal TotalCharge { get; set; }
    public decimal TotalIntCostFees { get; set; }
    public decimal TotalIntPlusFees { get; set; }
    public decimal TTFee { get; set; }
    public decimal VariableInterestCharge { get; set; }//No
    public decimal VariableRate { get; set; }//No
    public decimal VariableRepayment { get; set; }//No
    public decimal VariableTerm { get; set; }//No
}

public class SoftSearchResponse
{
    /// <summary>
    /// Shawbrooks unique case identifier for the application just created
    /// </summary>
    public int ShawbrookCaseId { get; set; }

    /// <summary>
    /// Details of credit data returned
    /// First will contain applicants 1 & 2 (if applicable)
    /// Second will contain applicants 3 & 4 (if applicable)
    /// </summary>
    public IEnumerable<CreditData> CreditData { get; set; }

    /// <summary>
    /// All eligible plans.
    /// </summary>
    public IEnumerable<ProductCalculatedDetails> EligiblePlanDetails { get; set; }

    /// <summary>
    /// All ineligible plans.
    /// </summary>
    public IEnumerable<ProductCalculatedDetails> InEligiblePlanDetails { get; set; }

    /// <summary>
    /// Reason returned if case is saved but soft search is not run
    /// </summary>
    public string InfoMessage { get; set; }

    /// <summary>
    /// Returns false if case is saved but soft search is not run
    /// </summary>
    public bool SoftSearched { get; set; }
}


public enum EnumFeesPaymentArrangements
{
	PaidUpfront = 1,
	AddToLoan,
	DedutedFromNetAdvance,
    PaidOnCompletion,
}
public enum EnumFeeType
{
	Lender = 1,
	Broker = 2,
	Advise = 3,
    ShawbrookLender = 4,
    Disbursement = 5,
    Discharge = 6

}
#endregion
