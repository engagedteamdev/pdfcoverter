﻿Imports Config, Common
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Web.Script.Serialization
Imports System.Threading
Imports System.Diagnostics

Public Class CallResponse

    Private strResponse As String = "", strMessage As String = ""

    Public Property Response() As String
        Get
            Return strResponse
        End Get
        Set(ByVal value As String)
            strResponse = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return strMessage
        End Get
        Set(ByVal value As String)
            strMessage = value
        End Set
    End Property

End Class

Partial Class ClickToDial
    Inherits System.Web.UI.Page

    Private strGUID As String = Guid.NewGuid.ToString

    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private strTelephoneTo As String = HttpContext.Current.Request("TelephoneTo")
    Private UserSessionID As String = HttpContext.Current.Request("UserSessionID")
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strClickToDialProviderURL As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strErrorMessage As String = ""
    Private strUserID As String = "", strUserName As String = ""

    Private Sub Page_Error(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Error
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim st As StackTrace = New StackTrace(ex, True)
        Dim sf As StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & "</div>" & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & "</div>" & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & "</div>" & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & "</div>" & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & "</div>" & vbCrLf & _
            "<div><strong>Query String:</strong> " & Request.QueryString.ToString & "</div>" & vbCrLf & _
            "<div><strong>Form Variables:</strong> " & Request.Form.ToString & "</div>" & vbCrLf & _
            "<div><strong>Host:</strong> " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & "</div>" & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf
        responseWrite(strMessage)
        responseEnd()
    End Sub

    Private Enum ActionType
        makeCall = 1
        endCall = 2
    End Enum

    Public Sub clickToDial()
        strClickToDialProviderURL = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "ClickToDialProviderURL")
        strUserID = getAnyFieldByCompanyID("UserID", "tblusers", "UserSessionID", UserSessionID)
        If checkValue(strClickToDialProviderURL) Then
            Select Case intActionType
                Case ActionType.makeCall
                    'responseWrite(1)
                    makeCall()
                Case ActionType.endCall
                    'responseWrite(2)
                    endCall()
                    'Case ActionType.getCallRecording
                    'responseWrite(3)
                    'getCallRecording()
            End Select
        End If
    End Sub

    Private Sub makeCall()
        Dim strRequestURL As String = "", strClickToDialAccountNumber As String = "", strSecret As String = "", strTenant As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialAccountNumber" Then strClickToDialAccountNumber = Row.Item("StoredUserValue").ToString
            Next
        End If
        strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%EngagedPBX%' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "EngagedPBXSecret") Then strSecret = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "EngagedPBXTenant") Then strTenant = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If (Not checkValue(strClickToDialAccountNumber) Or Not checkValue(strTelephoneTo) Or strTelephoneTo = "0") Then ' Error in data before sending
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("2|An invalid CLI, CLD or Account Number was provided")
            sendMail("Click to Dial Make Call Error", "An invalid CLI, CLD, Password or Account Number was provided")
        Else
            strRequestURL = strClickToDialProviderURL & "/pbxapi/call.php?reqtype=DIAL&source=" & strClickToDialAccountNumber & "&dest=" & strTelephoneTo & "&tenant=" & strTenant & "&key=" & strSecret & "&guid=" & strGUID

            Dim objCall As HttpWebResponse = getPBXWebRequest(strRequestURL)
            Dim objReader As New StreamReader(objCall.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing

            updateUserStoreField(strUserID, "ClickToDialSessionID", strGUID, "", "")

            Dim objSer As JavaScriptSerializer = New JavaScriptSerializer
            Dim callResponse As CallResponse = objSer.Deserialize(Of CallResponse)(strResponse)

            If (callResponse.Response() = "Success") Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("0|Success")
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write("1|" & callResponse.Message())
            End If

            'Dim result As IAsyncResult = startAsyncWebRequest(strRequestURL)

            'Dim objResponse As HttpWebResponse = Nothing

            'Dim state As RequestState = DirectCast(result.AsyncState, RequestState)
            'Dim request As WebRequest = DirectCast(state.WebRequest, WebRequest)
            'Dim response As HttpWebResponse = DirectCast(request.GetResponse, HttpWebResponse)

            'responseWrite(response.ContentType)
            'responseEnd()

            'objResponse = getAsyncWebResponse(result)
            'Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'Dim strResponse As String = objReader.ReadToEnd()

            'responseWrite(strResponse)
            'responseEnd()

            'If (callResponse.Response() = "Success") Then
            '    HttpContext.Current.Response.Clear()
            '    HttpContext.Current.Response.Write("0|Success")
            'Else
            '    HttpContext.Current.Response.Clear()
            '    HttpContext.Current.Response.Write("1|" & callResponse.Message())
            'End If

            'strRequestURL = strClickToDialProviderURL & "/accounts/" & strClickToDialAccountNumber & "/Calls/"
            'Dim strJSONPost As String = "{'CLI':'" & strClickToDialDDI & "','CLD':'" & strTelephoneTo & "','AccountID':'" & strClickToDialAccountNumber & "'}"
            'Dim objCall As HttpWebResponse = postSynetyWebRequest(strRequestURL, strClickToDialAccountNumber, strClickToDialPassword, strSynetyLicenseKey, strJSONPost)
            'Dim objReader As New StreamReader(objCall.GetResponseStream())
            'Dim strResponse As String = objReader.ReadToEnd()
            'Dim strSessionID As String = regexFirstMatch("""SessionID"":""([a-z0-9\-]{1,64})""", strResponse)
            'updateUserStoreField(strUserID, "ClickToDialSessionID", strSessionID, "", "")
            'responseWrite(strResponse & "<br><br>")
            'responseWrite("SessionID=" & strSessionID & "<br>")
            'responseEnd()
        End If
    End Sub

    Private Sub endCall()
        Dim strRequestURL As String = "", strClickToDialAccountNumber As String = "", strSecret As String = "", strTenant As String = ""
        Dim strSQL As String = "SELECT StoredUserName, StoredUserValue FROM tbluserstore WHERE StoredUserName LIKE N'ClickToDial%' AND CompanyID = '" & CompanyID & "' AND UserID = '" & strUserID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row In dsCache.Rows
                If Row.Item("StoredUserName").ToString = "ClickToDialAccountNumber" Then strClickToDialAccountNumber = Row.Item("StoredUserValue").ToString
            Next
        End If
        strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%EngagedPBX%' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("SystemConfigurationName") = "EngagedPBXSecret") Then strSecret = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "EngagedPBXTenant") Then strTenant = Row.Item("SystemConfigurationValue")
            Next
        End If
        dsCache = Nothing

        If (Not checkValue(strClickToDialAccountNumber)) Then ' Error in data before sending
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("2|An error has occurred")
            sendMail("Click to Dial End Call Error", "Err15: A default value is not present in synety/default.aspx: " & strErrorMessage)
        Else
            strRequestURL = strClickToDialProviderURL & "/pbxapi/endcall.php?source=" & strClickToDialAccountNumber & "&tenant=" & strTenant & "&key=" & strSecret

            Dim objCall As HttpWebResponse = getPBXWebRequest(strRequestURL)
            Dim objReader As New StreamReader(objCall.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing

            'Dim objSer As JavaScriptSerializer = New JavaScriptSerializer
            'Dim callResponse As CallResponse = objSer.Deserialize(Of CallResponse)(strResponse)

            'If (callResponse.Response() = "Success") Then
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.Write("0|Success")
            'Else
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.Write("1|" & callResponse.Message())
            'End If

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write("0|Success")
        End If
    End Sub

    Private Function checkResponse(ByRef objResponse As HttpWebResponse) As Boolean
        Dim boolValid = True
        Dim excInnerException As String = ""
        Try ' Make sure the response is valid
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            If Not (objResponse.StatusCode.ToString = "OK") Then
                'sendMail("Click to Dial Call Error", "An unknown status code has been received from " & strClickToDialProviderURL & " in engagedpbx/default.aspx.")
                boolValid = False
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValid = False
        End Try
        Return boolValid
    End Function

    Public Function getPBXWebRequest(ByVal url As String) As HttpWebResponse
        Certificate.OverrideCertificateValidation()
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                '.UnsafeAuthenticatedConnectionSharing = True
                .Method = WebRequestMethods.Http.Get
                .KeepAlive = False
            End With
            'Dim result As IAsyncResult = Nothing
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function startAsyncWebRequest(ByVal url As String) As IAsyncResult
        Dim objRequest As WebRequest = WebRequest.Create(New Uri(url))
        With objRequest
            .Method = WebRequestMethods.Http.Get
        End With
        Dim objState As RequestState = New RequestState(objRequest, Nothing, url)
        Dim result As IAsyncResult = objRequest.BeginGetResponse(New AsyncCallback(AddressOf endAsyncWebRequest), objState)
        Return result
    End Function

    Private Sub endAsyncWebRequest(ByVal result As IAsyncResult)
        Dim state As RequestState = DirectCast(result.AsyncState, RequestState)
        Dim request As WebRequest = DirectCast(state.WebRequest, WebRequest)
        Dim response As HttpWebResponse = DirectCast(request.EndGetResponse(result), HttpWebResponse)
    End Sub

    Private Function getAsyncWebResponse(ByVal result As IAsyncResult) As HttpWebResponse
        Dim state As RequestState = DirectCast(result.AsyncState, RequestState)
        Dim request As WebRequest = DirectCast(state.WebRequest, WebRequest)
        Dim response As HttpWebResponse = DirectCast(request.GetResponse, HttpWebResponse)
        Return response
    End Function

    '    private void ScanSites ()
    '{
    '  // for each URL in the collection...
    '  WebRequest request = HttpWebRequest.Create(uri);
    '  request.Method = "HEAD";

    '  // RequestState is a custom class to pass info
    '  RequestState state = new RequestState(request,data);

    '  IAsyncResult result = request.BeginGetResponse(
    '    new AsyncCallback(UpdateItem),state);

    '  // PLACEHOLDER: See below...
    '}

    'private void UpdateItem (IAsyncResult result)
    '{
    '  // grab the custom state object
    '  RequestState state = (RequestState)result.AsyncState;
    '  WebRequest request = (WebRequest)state.request;
    '  // get the Response
    '  HttpWebResponse response =
    '    (HttpWebResponse )request.EndGetResponse(result);
    '  // process the response...
    '}

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=True&strAttachment=&UserSessionID=" & UserSessionID)
    End Sub

End Class
