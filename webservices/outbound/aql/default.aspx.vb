﻿Imports Config, Common
Imports System.Net
Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strTelephoneTo As String = HttpContext.Current.Request("frmTelephoneTo")
    Private strSMSBody As String = HttpContext.Current.Request("frmSMSBody")
	Private strSMSProviderFrom As String = HttpContext.Current.Request("frmSMSFrom")
    Private strSMSProviderURL As String = "", strSMSProviderUserName As String = "", strSMSProviderPassword As String = ""
    Private strUserName As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        strSMSProviderURL = objLeadPlatform.Config.SMSProviderURL()
        strSMSProviderUserName = objLeadPlatform.Config.SMSProviderUserName()
        strSMSProviderPassword = objLeadPlatform.Config.SMSProviderPassword()
		If (Not checkValue(strSMSProviderFrom)) Then
        	strSMSProviderFrom = objLeadPlatform.Config.SMSProviderFrom()
		End If
    End Sub

    Public Sub sendSMS()
        ' Testing only
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.Write("0|Success|0")
        'responseEnd()
        strTelephoneTo = "44" & Right(strTelephoneTo, 10)
        Dim strAPIKey As String = getSystemConfigValue("EngagedAPIKey")
        'responseWrite(objLeadPlatform.Config.SMSProviderURL() & "?username=" & strSMSProviderUserName & "&password=" & strSMSProviderPassword & "&destination=" & strTelephoneTo & "&originator=" & strSMSProviderFrom & "&message=" & strSMSBody & "&dlr_url=" & encodeURL("http://positive.engagedcrm.co.uk/webservices/inbound/aql/delivery/?AppID=" & AppID & "&apiKey=" & strAPIKey & "&reportcode=%code&destinationnumber=%dest"))
        'responseEnd()
        Dim objResponse As HttpWebResponse = getWebRequest(objLeadPlatform.Config.SMSProviderURL() & "?username=" & strSMSProviderUserName & "&password=" & strSMSProviderPassword & "&destination=" & strTelephoneTo & "&originator=" & strSMSProviderFrom & "&message=" & strSMSBody & "&dlr_url=" & encodeURL("http://positive.engagedcrm.co.uk/webservices/inbound/aql/delivery/?AppID=" & AppID & "&apiKey=" & strAPIKey & "&reportcode=%code&destinationnumber=%dest"))
        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            Dim arrResponse As Array = Split(strResponse, ":")
            Dim strStatus As String = getStatus(arrResponse(0))
            Select Case arrResponse(0)
                Case "0" ' Success
                    If (Len(strSMSBody) > 160) Then
                        incrementSystemConfigurationField("SMSNumberSent", 1)
                    End If
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("0|Success|0")
                Case Else ' Display user message
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write("1|" & strStatus & "|0")
            End Select
            objReader.Close()
            objReader = Nothing
        End If
        If (checkResponse(objResponse, "")) Then
            objResponse.Close()
        End If
        objResponse = Nothing
    End Sub

    Private Function getStatus(ByVal code As String) As String
        Select Case code
            Case "0"
                Return "SMS successfully queued"
            Case "1"
                Return "SMS queued partially"
            Case "2"
                Return "Authentication error"
            Case "3"
                Return "Destination number(s) error"
            Case "4"
                Return "Send time error"
            Case "5"
                Return "Insufficient credit or invalid number of msg/destination"
            Case "9"
                Return "Undefined error"
            Case Else
                Return "Undefined error"
        End Select
    End Function

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailFrom=crm@engaged-solutions.co.uk&strMailTo=devteam@engagedcrm.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&strAttachment=&UserSessionID=" & HttpContext.Current.Request("UserSessionID"))
    End Sub

End Class
