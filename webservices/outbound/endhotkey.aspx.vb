﻿Imports Config, Common, CallInterface, CommonSave
Imports System.Net
Imports System.IO

Partial Class EndHotkey
    Inherits System.Web.UI.Page

    Private intActionType As String = HttpContext.Current.Request("intActionType")
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strMediaCampaignIDOutbound As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
    Private strMediaCampaignDeliveryEmailAddress As String = HttpContext.Current.Request("MediaCampaignDeliveryEmailAddress")
    Private intHotkeyUserID As String = HttpContext.Current.Request("HotkeyUserID")
    Private strMediaUserRef As String = HttpContext.Current.Request("MediaUserRef")
    Private intSalesUserID As String = HttpContext.Current.Request("SalesUserID")
    Private strWTDSubStatusCode As String = HttpContext.Current.Request("WTDSubStatusCode")
    Private strTUDSubStatusCode As String = HttpContext.Current.Request("TUDSubStatusCode")
    Private strINVSubStatusCode As String = HttpContext.Current.Request("INVSubStatusCode")
    Private strDateType As String = HttpContext.Current.Request("DateType")
    Private strStatusCode As String = HttpContext.Current.Request("StatusCode")
    Private strSubStatusCode As String = HttpContext.Current.Request("SubStatusCode")
    Private strCallBackDate As String = HttpContext.Current.Request("CallBackDate")
    Private strCallBackTime As String = HttpContext.Current.Request("CallBackTime")
    Private strClientReferenceOutbound As String = HttpContext.Current.Request("ClientReferenceOutbound")
    Private strTransferMessage As String = "Transfer successful"
    Private strFrmAction As String = HttpContext.Current.Request("strFrmAction")
    Private strCreateDiaries As String = "", strCompleteDiaries As String = "", strCancelDiaries As String = ""
    Private strReturnURL As String = HttpContext.Current.Request("strReturnURL")

    Private Enum ActionType
        Email = 3
        CSVEmail = 4
        VoiceOnly = 5
        IssueDocuments = 7
        EmailTextOnly = 8
        SalesTransfer = 9
        SetStatusDate = 99
        ChangeStatus = 100
        WeTurnDown = 101
        CustomerTurnDown = 102
        Invalid = 103
        CallBack = 104
        ClientReference = 105
    End Enum

    Public Sub endHotkey()
        If (Not checkKickOut(intHotkeyUserID, False)) Then
            Select Case intActionType
                Case ActionType.Email
                    transferApplication()
                    emailHotkey()
                Case ActionType.CSVEmail
                    transferApplication()
                    csvEmailHotkey()
                Case ActionType.EmailTextOnly
                    transferApplication()
                    textEmailHotkey()
                Case ActionType.VoiceOnly
                    transferApplication()
                Case ActionType.SalesTransfer
                    transferApplicationToSales()
                Case ActionType.IssueDocuments
                    transferApplication()
                    strTransferMessage = "Action confirmed"
                Case ActionType.WeTurnDown
                    turnDownApplication("WTD", strWTDSubStatusCode)
                Case ActionType.CustomerTurnDown
                    turnDownApplication("TUD", strTUDSubStatusCode)
                Case ActionType.Invalid
                    turnDownApplication("INV", strINVSubStatusCode)
                Case ActionType.CallBack
                    arrangeCallBack()
                Case ActionType.SetStatusDate
                    setStatusDate()
                Case ActionType.ChangeStatus
                    changeStatus()
                Case ActionType.ClientReference
                    changeClientReference()
                    strTransferMessage = "Reference number saved"
            End Select
        End If
        Dim strSQL As String = "SELECT ApplicationPageCreateDiaries, ApplicationPageCompleteDiaries, ApplicationPageCancelDiaries FROM tblapplicationpages WHERE ApplicationPageID = '" & strFrmAction & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strCreateDiaries = Row.Item("ApplicationPageCreateDiaries").ToString
                strCompleteDiaries = Row.Item("ApplicationPageCompleteDiaries").ToString
                strCancelDiaries = Row.Item("ApplicationPageCancelDiaries").ToString
            Next
        End If
        dsCache = Nothing
        If (checkValue(strCreateDiaries)) Then
            createDiaries(AppID, strCreateDiaries)
        End If
        If (checkValue(strCompleteDiaries)) Then
            completeDiaries(AppID, strCompleteDiaries)
        End If
        If (checkValue(strCancelDiaries)) Then
            cancelDiaries(AppID, strCancelDiaries)
        End If
        If (checkValue(strReturnURL)) Then
            Response.Redirect(strReturnURL)
        Else
            HttpContext.Current.Response.Write(1 & "|" & encodeURL(strTransferMessage))
        End If
    End Sub

    Private Sub transferApplication()
        If (Not checkValue(strMediaUserRef)) Then strMediaUserRef = HttpContext.Current.Request("TransferUserID")
        setTransferState(AppID, strMediaCampaignIDOutbound, strMediaUserRef, "", intHotkeyUserID, strMediaCampaignScheduleID)
        changeClientReference()
        cacheDependency(Cache, "tblkpi,vwsalestoday")
    End Sub

    Private Sub transferApplicationToSales()
        setTransferState(AppID, strMediaCampaignIDOutbound, "", intSalesUserID, intHotkeyUserID, strMediaCampaignScheduleID)
        saveNote(AppID, intHotkeyUserID, "Case transferred as " & getAnyFieldByCompanyID("ProductType", "tblapplications", "AppID", AppID) & " to " & getAnyFieldByCompanyID("UserFullName", "tblusers", "UserID", intSalesUserID))
        changeClientReference()
        cacheDependency(Cache, "tblkpi,vwsalestoday")
    End Sub

    Private Sub turnDownApplication(ByVal st As String, ByVal sst As String)
        Dim strQry As String = "UPDATE tblapplicationstatus SET " & _
            "StatusCode     = " & formatField(st, "U", st) & ", " & _
            "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
        If (st = "INV") Then
            saveUpdatedDate(AppID, intHotkeyUserID, "OutsideCriteriaAccepted")
            strQry += _
                "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
        Else
            If (st = "WTD") Then
                Select Case sst
                    Case "DNS", "SEL", "OOW", "BUS"
                        saveUpdatedDate(AppID, intHotkeyUserID, "OutsideCriteriaAccepted")
                        strQry += _
                            "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
                End Select
            ElseIf (st = "TUD") Then
                Select Case sst
                    Case "OOW", "TPS", "REF", "HNG"
                        saveUpdatedDate(AppID, intHotkeyUserID, "OutsideCriteriaAccepted")
                        strQry += _
                            "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
                End Select
            End If
        End If
        saveUpdatedDate(AppID, intHotkeyUserID, st)
        strQry += _
            "CallBackDate       = " & formatField("", "DTTM", "NULL") & ", " & _
            "LastContactedDate  = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE AppID        = '" & AppID & "' " & _
            "AND CompanyID      = '" & CompanyID & "'"

        executeNonQuery(strQry)

        insertStatusHistory(AppID, st, sst, intHotkeyUserID)
        callLogging(AppID, ActivityType.EndCall, "", intHotkeyUserID, st, sst)
    End Sub

    Private Sub emailHotkey()
        getWebRequest(Config.ApplicationURL & "/webservices/sendindividualapp.aspx?AppID=" & AppID & "&strMailTo=" & strMediaCampaignDeliveryEmailAddress & "&intMode=1&HotkeyUserID= " & intHotkeyUserID & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & strMediaCampaignID)
    End Sub

    Private Sub csvEmailHotkey()
        getWebRequest(Config.ApplicationURL & "/webservices/sendindividualapp.aspx?AppID=" & AppID & "&strMailTo=" & strMediaCampaignDeliveryEmailAddress & "&intMode=2&HotkeyUserID= " & intHotkeyUserID & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & strMediaCampaignID)
    End Sub

    Private Sub textEmailHotkey()
        getWebRequest(Config.ApplicationURL & "/webservices/sendindividualapp.aspx?AppID=" & AppID & "&strMailTo=" & strMediaCampaignDeliveryEmailAddress & "&intMode=8&HotkeyUserID= " & intHotkeyUserID & "&UserSessionID=" & HttpContext.Current.Request("UserSessionID") & "&MediaCampaignID=" & strMediaCampaignID)
    End Sub

    Private Sub arrangeCallBack()
        Dim strQry As String = "UPDATE tblapplicationstatus SET " & _
            "CallBackDate       = " & formatField(strCallBackDate & " " & strCallBackTime, "DTTM", Config.DefaultDateTime) & ", " & _
            "NextCallDate       = " & formatField(strCallBackDate & " " & strCallBackTime, "DTTM", Config.DefaultDateTime) & ", " & _
            "SubStatusCode      = " & formatField("CBK", "U", "CBK") & ", " & _
            "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE AppID        = '" & AppID & "' " & _
            "AND CompanyID      = '" & CompanyID & "'"

        executeNonQuery(strQry)

        insertStatusHistory(AppID, "", "CBK", intHotkeyUserID)
        saveReminder(AppID, intHotkeyUserID, intHotkeyUserID, strCallBackDate & " " & strCallBackTime, "Call back due")
        callLogging(AppID, ActivityType.EndCall, "", intHotkeyUserID, getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID), "CBK")
    End Sub

    Private Sub setStatusDate()
        If (checkValue(strDateType)) Then
            saveUpdatedDate(AppID, intHotkeyUserID, strDateType)
        End If
    End Sub

    Private Sub changeStatus(Optional ByVal callstatus As Boolean = False)
        Dim strExistingStatusCode As String = "", strDialableStatus As String = ""
        Dim strSQL As String = "SELECT StatusCode FROM tblapplicationstatus WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim ds As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                strExistingStatusCode = Row.Item("StatusCode").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim strQry As String = ""
        strQry += "UPDATE tblapplicationstatus SET "
        If (checkValue(strDateType)) Then
            saveUpdatedDate(AppID, intHotkeyUserID, strDateType)
        End If
        If (checkValue(strStatusCode)) Then
            strQry += _
                "StatusCode     = " & formatField(strStatusCode, "U", strStatusCode) & ", "
        Else
            strStatusCode = strExistingStatusCode
        End If
        If (checkValue(strSubStatusCode)) Then
            strQry += _
                "SubStatusCode  = " & formatField(strSubStatusCode, "U", strSubStatusCode) & ", "
        End If
        strQry += _
            "CallBackDate       = " & formatField("", "DTTM", "NULL") & ", " & _
            "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE AppID        = '" & AppID & "' " & _
            "AND CompanyID      = '" & CompanyID & "'"

        executeNonQuery(strQry)

        insertStatusHistory(AppID, strStatusCode, strSubStatusCode, intHotkeyUserID)
        If (strSubStatusCode = "WRN") Then
            executeNonQuery("EXECUTE spmarkinvalidtelephone @AppID = " & AppID & ", @UserID = " & intHotkeyUserID & ", @CompanyID = " & CompanyID)
            strDialableStatus = getAnyField("DialableStatus", "tblapplicationstatus", "AppID", AppID)
            If (strDialableStatus = "000") Then
                turnDownApplication("INV", "WRN")
            Else
                callLogging(AppID, ActivityType.EndCall, "", intHotkeyUserID, strStatusCode, strSubStatusCode)
            End If
        Else
            callLogging(AppID, ActivityType.EndCall, "", intHotkeyUserID, strStatusCode, strSubStatusCode)
        End If
    End Sub

    Private Sub changeClientReference()
        If (checkValue(strClientReferenceOutbound)) Then
            updateSingleDatabaseField(AppID, "tblapplications", "ClientReferenceOutbound", "", strClientReferenceOutbound, "NULL")
        End If
    End Sub

End Class
