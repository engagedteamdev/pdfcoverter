﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates

Partial Class XMLUpload
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub readXML()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim strDir As String = "F:\data\attachments"
        Dim dteDate As Date = Date.Now
        Dim strFileName As String = Year(dteDate) & Month(dteDate) & Day(dteDate) & Hour(dteDate) & Minute(dteDate) & Second(dteDate)
        Dim strCompanyDir As String = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))
        If (Not Directory.Exists(strDir & "\" & strCompanyDir)) Then
            Directory.CreateDirectory(strDir & "\" & strCompanyDir)
        End If
        Dim strDateDir As String = DatePart(DateInterval.WeekOfYear, Config.DefaultDate) & "-" & Config.DefaultDate.Year
        If (Not Directory.Exists(strDir & "\" & strCompanyDir & "\" & strDateDir)) Then
            Directory.CreateDirectory(strDir & "\" & strCompanyDir & "\" & strDateDir)
        End If
        Dim strFileData As String = objInputXMLDoc.SelectSingleNode("//FileData").InnerText
        Dim strFileExt As String = objInputXMLDoc.SelectSingleNode("//FileExtension").InnerText
        Dim intFileSize As String = objInputXMLDoc.SelectSingleNode("//FileSize").InnerText
        Dim bytes As Byte() = Convert.FromBase64String(strFileData)
        File.WriteAllBytes(strDir & "\" & strCompanyDir & "\" & strDateDir & "\" & strFileName & strFileExt, bytes)
        Dim strQry As String = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryCreatedUserID) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(AppID, "N", 0) & ", " & _
                    formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                    formatField(intFileSize, "N", 0) & ", " & _
                    formatField("Bank Statement" & strFileExt, "T", "") & ", " & _
                    formatField(Config.DefaultUserID, "N", getSystemUser()) & ")"
        executeNonQuery(strQry)
        saveNote(AppID, Config.DefaultUserID, "Bank Statement was uploaded for this case.")
        responseWrite("Document(s) successfully uploaded")
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""https://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

End Class
