﻿Imports Config, Common

Partial Class Calls
    Inherits System.Web.UI.Page

    Private intReminderAssignedToUserID As String = HttpContext.Current.Request("UserID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub checkCalls()
        Dim strCalls As String = "<Calls>"
        Dim strTenant As String = getSystemConfigValue("EngagedPBXTenant")
        Dim strQry As String = "SELECT st_states.st_state, st_states.st_timestamp, sipfriends.callerid, sipfriends.id FROM st_states INNER JOIN sipfriends ON sipfriends.name = st_states.st_extension INNER JOIN te_tenants ON te_tenants.te_id = sipfriends.te_id WHERE te_code = '" & strTenant & "' AND TIMESTAMPDIFF(MINUTE,st_timestamp,NOW()) <= 30 AND st_state <> 'UNAVAILABLE' ORDER BY st_timestamp DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "", CommandType.Text, Nothing, "pbx").returnCache
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                strCalls += "<Call><Query>" & encodeURL(strQry) & "</Query><CallID>" & Row.Item("id") & "</CallID><Name>" & regexReplace("<[0-9]{0,3}>", "", Row.Item("callerid")) & "</Name><Date>" & Replace(ddmmyyhhmmss2ddmmhhmm(Row.Item("st_timestamp")), "&nbsp;", " ") & "</Date><Status>" & Row.Item("st_state") & "</Status><Seconds>" & DateDiff(DateInterval.Second, Row.Item("st_timestamp"), Config.DefaultDateTime.AddSeconds(120)) & "</Seconds><Length>" & ss2mmss(DateDiff(DateInterval.Second, Row.Item("st_timestamp"), Config.DefaultDateTime.AddSeconds(120))) & "</Length></Call>"
            Next
        End If
        strCalls += "</Calls>"
        Response.ContentType = "text/xml"
        Response.Write(strCalls)
    End Sub

End Class
