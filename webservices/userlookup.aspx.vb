﻿Imports Config
Imports Common

Partial Class UserLookup
    Inherits System.Web.UI.Page

    Private intMode As String = HttpContext.Current.Request("intMode")
    Private strUserFullName As String = HttpContext.Current.Request("strUserFullName")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If		
    End Sub

    Private Enum Mode
        UserReference = 1
        UserName = 2
    End Enum

    Public Sub userLookup()
        strUserFullName = regexReplace("[^a-z ]", "", strUserFullName)
        Select Case intMode
            Case Mode.UserReference
                Response.Write(getUserReference())
            Case Mode.UserName
                Response.Write(getUserName())
        End Select
    End Sub

    Private Function getUserReference() As String
        Dim strUserReference As String = ""
        If (checkValue(strUserFullName)) Then
            strUserReference = Left(strUserFullName, 1)
            Dim arrUserFullName As Array = Split(strUserFullName, " ")
            strUserReference += Left(arrUserFullName(UBound(arrUserFullName)), 1) & Right(arrUserFullName(UBound(arrUserFullName)), 1)
            Dim x As Integer = 0
            If (userReferenceExists(strUserReference)) Then
                While userReferenceExists(strUserReference)
                    x += 1
                    arrUserFullName = Split(strUserFullName, " ")
                    strUserReference = Left(strUserFullName, 1)
                    strUserReference += Right(Left(arrUserFullName(UBound(arrUserFullName)), 1 + x), 1) & Right(arrUserFullName(UBound(arrUserFullName)), 1)
                End While
				Return UCase(strUserReference)
            Else
                Return UCase(strUserReference)
            End If
        Else
            Return ""
        End If
    End Function

    Private Function userReferenceExists(ByVal ref As String) As Boolean
        Dim boolExists As Boolean = False
        Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserReference = '" & ref & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                boolExists = True
            Next
        Else
            boolExists = False
        End If
        dsUsers.Clear()
        dsUsers = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolExists
    End Function

    Private Function getUserName() As String
        Dim strUserName As String = ""
        Dim arrUserFullName As Array = Split(strUserFullName, " ")
        Dim x As Integer = 0
        For Each Item As String In arrUserFullName
            If (x = 0) Then
                strUserName += LCase(Item)
            Else
                strUserName += "." & LCase(Item)
            End If
            x += 1
        Next
        If (userNameExists(strUserName)) Then
            x = 1
            While userNameExists(strUserName)
                strUserName += x.ToString
                x += 1
            End While
        Else
            Return strUserName
        End If
        Return strUserName
    End Function

    Private Function userNameExists(ByVal name As String) As Boolean
        Dim boolExists As Boolean = False
        Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserName = '" & name & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                boolExists = True
            Next
        Else
            boolExists = False
        End If
        dsUsers.Clear()
        dsUsers = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolExists
    End Function

End Class
