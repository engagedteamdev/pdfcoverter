﻿Imports Config, Common

Partial Class ClearCache
    Inherits System.Web.UI.Page

    Private strTable As String = HttpContext.Current.Request("frmTable")

    Public Sub ClearCache()
        cacheDependency(Cache, strTable)
    End Sub

End Class
