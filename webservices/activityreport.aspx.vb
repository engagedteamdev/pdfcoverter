﻿Imports System.Data
Imports System.Globalization
Imports Config, Common

Partial Class BillingEmail
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform




    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub billingSend()

      
			Dim strTable As String = ""
            Dim strSQL As String = "SELECT MediaCampaignInboundName, DATENAME(MONTH, GETDATE()) AS 'Month', DATEPART(yyyy, GETDATE()) AS 'Year', COUNT(*) AS CountCreated, SUM(ISNULL(StatusChanged, 0)) AS 'Count', SUM(ISNULL(TransfferedFromDialler, 0)) AS TFR, SUM(ISNULL(TransferredToMortgageDesk, 0)) AS TFR_MD, SUM(ISNULL(CustomerFactFindMortgageDesk, 0)) AS CFF_MD, SUM(ISNULL(Proceeding, 0)) AS PRO_MD, SUM(ISNULL(MortgageSalesMortgageDesk, 0)) AS 'MIP Created', SUM(ISNULL(DayTwoPacksOutsMortgageDesk, 0)) AS ORG_MD, SUM(ISNULL(DayTwoPacksBackMortgageDesk, 0)) AS DOX_MD, SUM(ISNULL(TransferredToAdvisor, 0)) AS TFR_ADV, SUM(ISNULL(CustomerFactFindAdvisor, 0)) AS CFF_ADV, SUM(ISNULL(DayTwoPacksOutsAdvisor, 0)) AS ORG_ADV, SUM(ISNULL(DecisionInPrincipleAdvisor, 0)) AS PRO_ADV, SUM(ISNULL(DayTwoPacksOutsAdvisor, 0)) AS ORG_ADV, SUM(ISNULL(DayTwoPacksBackAdvisor, 0)) AS DOX_ADV FROM (SELECT MediaCampaignIDInbound, MediaCampaignInboundName, CreatedDate, ApplicationStatusDateName, CASE WHEN NOT ApplicationStatusDate IS NULL THEN 1.00 ELSE 0.00 END AS ApplicationStatusDate FROM vwreportcaselist LEFT JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = vwreportcaselist.AppID WHERE Active = 1 AND vwreportcaselist.CompanyID = '1430' AND DATEPART(dd, ApplicationStatusDate) = DATEPART(dd, getdate()) AND DATEPART(mm, ApplicationStatusDate) = DATEPART(mm, getdate()) AND DATEPART(yyyy, ApplicationStatusDate) = DATEPART(yyyy, getdate())) A PIVOT (SUM(ApplicationStatusDate) FOR ApplicationStatusDateName IN ([INV], [WTD], [TUD], [StatusChanged], [LastDialed], [AppointmentRearranged], [AppointmentCompleted], [AppointmentCancelled], [ProvisionalAppointment], [AppointmentConfirmed], [TransfferedFromDialler], [CustomerFactFind], [Proceeding], [DocumentsIssued], [PartialPackBack], [DocumentsReceived], [TransferredToAdvisor], [TransferredToMortgageDesk], [DecisionInPrincipleAdvisor], [DecisionInPrincipleMortgageDesk], [MortgageSalesAdvisor], [MortgageSalesMortgageDesk], [DayTwoPacksOutsAdvisor], [DayTwoPacksOutsMortgageDesk], [PartialPackBackAdvisor], [PartialPackBackMortgageDesk], [DayTwoPacksBackAdvisor], [DayTwoPacksBackMortgageDesk], [SubmittedToLenderAdvisor], [SubmittedToLenderMortgageDesk], [Midus], [CustomerFactFindAdvisor], [CustomerFactFindMortgageDesk], [OfferAdvisor], [PaidOutAdvisor])) C WHERE (MediaCampaignInboundName <> '' AND NOT MediaCampaignInboundName IS NULL) GROUP BY MediaCampaignIDInbound, MediaCampaignInboundName ORDER BY MediaCampaignInboundName"
            Dim dtb1 As New DataTable
	         using DBConnection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionStringFreedom365").ConnectionString)					 
	         DBConnection.open()			
	         Using dad As New SqlDataAdapter(strSQL,DBConnection)
	         dad.Fill(dtb1)
	         End Using
	         DBConnection.close()
	         End Using
			 
	         If (dtb1.Rows.Count > 0) Then   
                    Dim count As Integer = 0
            
				strTable += "<table>"
				strTable += "<thead>"
				strTable += "<tr>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Introducer</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Month Lead Received</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Created</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Status Changed</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Transferred To Customer Service - Bronze</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Transferred To Customer Service - Silver</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Customer Fact Find Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Mortgage Sales Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Mortgage In Principle Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">MIP Pack Out Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Documents Returned Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Transferred To Advisor</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Customer Fact Find Advisor</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Day Two Packs Back Customer Service</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Decision In Principle Advisor</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Mortgage Sales Advisor</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Day Two Packs Outs Advisor</th>"
				strTable += "<th style=""border-left: 1px solid rgba(255,255,255,.15);border-right: 1px solid #FFF;color: #FFF;text-align: left; font-size: 13px;"">Day Two Packs Back Advisor</th>"
				strTable += "</tr>"
				strTable += "</thead>"
				strTable += "<tbody>"
				For Each Row As DataRow In dtb1.Rows 
					strTable += "<tr>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("MediaCampaignInboundName").replace(")", "").replace("(", "") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("Month") & "  "  & Row.Item("Year") & " </td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("CountCreated") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("Count") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("TFR") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("TFR_MD") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("CFF_MD") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("PRO_MD") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("MIP Created") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("ORG_MD") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("DOX_MD") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("TFR_ADV") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("CFF_ADV") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("ORG_ADV") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("PRO_ADV") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("ORG_ADV") & "</td>"
					strTable += "<td class=""smlc sort-alpha"" style=""border-top: 1px solid #dddddd;"">" & Row.Item("DOX_ADV") & "</td>"
					strTable += "</tr>"
				Next
				strTable += "</tbody>"
				strTable += "</table>"
	
			End If
		

			'  Response.Write(strTable)
		'	Response.End()

			Dim strEmail As String = "<html><body><table width=""100%"" cellpadding=""0"" cellspacing=""0"" ><tr><td valign=""top"" align=""left""><table width=""648"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><img src=""https://crm.engaged-solutions.co.uk/net/images/eshot/top_ECRM.png"" width=""648"" height=""121"" /></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td valign=""top"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 10px 30px;""> <font face=""Calibri, Verdana, Arial"" size=""2"" color=""#000000""><br /><h3>Billing for the month of 8</h3>" & strTable & "<br/><br /><img src=""https://crm.engaged-solutions.co.uk/net/images/eshot/ECRM.png"" /> <br /> <a href=""http://www.engagedcrm.co.uk"" style=""margin-left:7px;"">www.engagedcrm.co.uk</a> </font></div></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td bgcolor=""#F6F7F7""><hr style=""border: 1px solid #CCCCCC; width:100%; margin:0px""></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 0px 30px;""><font face=""Calibri, Verdana, Arial"" size=""2"" color=""#999999""><p>This email and any file attachments are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the originator as soon as possible.</p><p>If you are not the intended recipient, you are not authorised to, and must not use, disclose, copy, distribute, retain or rely on this email or any part of it. Any views expressed in this message are those of the individual sender, except where the sender specifies and with authority states them to be the views of Engaged CRM LTD or any of its agents, representatives or associated companies</p><p>Engaged CRM LTD is a limited company registered in England and Wales. Registered number: 08989455. Registered Office: Gresley House, Ten Pound Walk, Doncaster, South Yorkshire, DN4 5HX</p> </font><br /></div></td><td align=""center"">&nbsp;</td></tr><tr><td width=""648"" height=""32"" colspan=""3"" align=""center"">&nbsp;</td></tr></table></td></tr></table></body></html>"


	Common.postEmail("", "dev.team@engagedcrm.co.uk", "Activity Report", strTable, True, "", True)

        
		
	


    End Sub




End Class