﻿Imports System.Net
Imports Config, Common, CommonSave

Partial Class Pixel
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID")
    Private strEmailName As String = HttpContext.Current.Request("EmailName")
    Private strDateType As String = HttpContext.Current.Request("DateType")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub pixel()
        Response.ContentType = "image/gif"
        If (checkValue(strDateType)) Then
            saveUpdatedDate(AppID, Config.DefaultUserID, strDateType)
        End If
        saveNote(AppID, Config.DefaultUserID, "Email: " & strEmailName & " was read")
        Dim objClient As New WebClient
        Dim oData As Byte() = objClient.DownloadData("https://beta.engaged-solutions.co.uk/img/blank.gif")
        Response.BinaryWrite(oData)
    End Sub

End Class
