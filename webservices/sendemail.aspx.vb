﻿Imports Config
Imports Common
Imports System.Net
Imports System.Net.Mail
Imports System.IO
Imports System.Data

Partial Class SendEmail
    Inherits System.Web.UI.Page

    Private strMailFrom As String = HttpContext.Current.Request("strMailFrom")
    Private strMailTo As String = HttpContext.Current.Request("strMailTo")
    Private strSubject As String = HttpContext.Current.Request("strSubject")
    Private strText As String = HttpContext.Current.Request("strText")
    Private boolBodyHTML As Boolean = HttpContext.Current.Request("boolBodyHTML")
    Private strAttachment As String = HttpContext.Current.Request("strAttachment")
    Private boolUseDefault As Boolean = HttpContext.Current.Request("boolUseDefault")
    Private intEmailProfileID As String = HttpContext.Current.Request("frmEmailProfileID")
    Private strEmailReplyTo As String = HttpContext.Current.Request("frmEmailReplyTo")
    Private objAttachment As Attachment

    Public Sub sendEmail()

		Try
        Dim strHost As String = "", strPort As String = "", strUserName As String = "", strPassword As String = "", strUseSSL As String = "", strEmailReplyToAddress As String = ""
        Dim objLeadPlatform As LeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()


        strHost = "smtp.sendgrid.net"
        strPort = "587"
        strUserName = "engagedcrm"
        strPassword = "Y0rkshire"
        strUseSSL = "Y"

        If (checkValue(strMailTo) And checkValue(strHost) And checkValue(strPort) And checkValue(strUserName) And checkValue(strPassword)) Then
            If (Not checkValue(strMailFrom)) Then strMailFrom = objLeadPlatform.Config.DefaultEmailFromAddress
            If (Not checkValue(boolBodyHTML)) Then boolBodyHTML = False
            If (strEmailReplyTo = "True") Then
                strEmailReplyToAddress = getAnyField("UserEmailAddress", "tblusers", "UserSessionID", Request("UserSessionID"))
            End If

            Dim objMail As New MailMessage
            With objMail
                Dim arrMailTo As Array = Split(strMailTo, ";")
                For Each Item As String In arrMailTo
                    If (checkValue(Item)) Then
                        .To.Add(Trim(Item))
                    End If
                Next
                .Bcc.Add("archive@engagedcrm.co.uk")
                .From = New MailAddress(strMailFrom)
                .Subject = strSubject
                .Body = strText
                .IsBodyHtml = boolBodyHTML
                If (strEmailReplyTo = "True") Then
                    .ReplyTo = New MailAddress(strEmailReplyToAddress)
                End If
            End With
            Dim objSMTP As New SmtpClient
            With objSMTP
                .Host = strHost
                .Port = CInt(strPort)
                If (strUseSSL = "Y") Then
                    .EnableSsl = True
                Else
                    .EnableSsl = False
                End If
                .DeliveryMethod = SmtpDeliveryMethod.Network
                '.UseDefaultCredentials = False
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            Try
                objSMTP.Send(objMail)
                responseWrite("1")
                If (Not boolUseDefault) Then incrementSystemConfigurationField("EmailNumberSent", 1)
            Catch ex As System.Net.Mail.SmtpException
                responseWrite("0")
               ' Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " (Live) Error from " & objLeadPlatform.Config.DefaultUserFullName, ex.Message, True, "", True)
                objMail.Dispose()
                objMail = Nothing
                objSMTP = Nothing
                Response.End()
            End Try
            objMail.Dispose()
            objMail = Nothing
            objSMTP = Nothing
        End If
		Catch 
		End Try
    End Sub

End Class
