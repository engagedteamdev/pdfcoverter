﻿Imports Config, Common

Partial Class BusinessContacts
    Inherits System.Web.UI.Page

    Private intParentClientID As String = HttpContext.Current.Request("ClientID")
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub businessContacts()
        Dim strContacts As String = "<Contacts>"
        Dim strQry As String = "SELECT APP.BusinessObjectID, BusinessObjectContactFirstName, BusinessObjectContactSurname FROM tblbusinessobjects APP WHERE APP.CompanyID = '" & CompanyID & "'AND APP.BusinessobjectActive = 1 AND BusinessObjectParentClientID = '" & intParentClientID & "' ORDER BY BusinessObjectContactFirstName DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strContacts += "<Contact><BusinessObjectID>" & Row.Item("BusinessObjectID") & "</BusinessObjectID><FirstName>" & Row.Item("BusinessObjectContactFirstName") & "</FirstName><Surname>" & Row.Item("BusinessObjectContactSurname") & "</Surname></Contact>"
            Next
        End If
        strContacts += "</Contacts>"
        Response.ContentType = "text/xml"
        responseWrite(strContacts)
    End Sub

End Class
