﻿Imports Config, Common

Partial Class statusDeals
    Inherits System.Web.UI.Page

    Public Sub leadStatus()
       

      
            Dim strXML As String = ""
            Dim strSQL As String = "select * from vwlivestatus"
           

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<cases>"
                    strXML += "<NumberOfCases>" & Row.Item("Apps") & "</NumberOfCases>"
                    strXML += "<StatusCode>" & Row.Item("StatusCode") & "</StatusCode>"
                    strXML += "<SalesUser>" & Row.Item("Sales User") & "</SalesUser>"
					strXML += "<ProductType>" & Row.Item("ProductType") & "</ProductType>"					
                    strXML += "</cases>"
                Next
                strXML += "</CaseStatus>"
            Else
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus><Error>No leads found</Error></CaseStatus>"
            End If
            dsCache = Nothing
            Response.ContentType = "text/xml"
            Response.Write(strXML)       
    End Sub

  

End Class