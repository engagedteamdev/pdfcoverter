﻿Imports Config, Common

Partial Class UpdateCustomerLogon
    Inherits System.Web.UI.Page

    Public Sub updateCustomerLogon()
        If (checkValue(HttpContext.Current.Request("frmNewPassword"))) Then
            Dim strQry As String = "SELECT TOP 1 Password FROM tblcustomerlogons WHERE Username = '" & HttpContext.Current.Request("frmEmailAddress") & "'"
            Dim strPassword As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            If (checkValue(strPassword) And Encryption.encrypt(strPassword) = HttpContext.Current.Request("frmPassword")) Then
				If (passwordPolicyCheck(HttpContext.Current.Request("frmNewPassword"))) Then
                	Dim strSQL As String = "UPDATE tblcustomerlogons SET Password = '" & hashString256(HttpContext.Current.Request("frmEmailAddress"), HttpContext.Current.Request("frmNewPassword")) & "' WHERE Username = '" & HttpContext.Current.Request("frmEmailAddress") & "'"
                	executeNonQuery(strSQL)
					responseWrite("1|Your details have beeen updated")
				Else
					responseWrite("0|Password does not meet minimum criteria")
				End If
			Else
				responseWrite("0|Incorrect username or password")
            End If
        ElseIf (checkValue(HttpContext.Current.Request("frmNewEmailAddress"))) Then
            Dim strQry As String = "SELECT TOP 1 Password FROM tblcustomerlogons WHERE Username = '" & HttpContext.Current.Request("frmEmailAddress") & "'"
            Dim strPassword As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            If (checkValue(strPassword) And Encryption.encrypt(strPassword) = HttpContext.Current.Request("frmPassword")) Then
                Dim strSQL As String = "UPDATE tblcustomerlogons SET Username = '" & HttpContext.Current.Request("frmNewEmailAddress") & "' WHERE Username = '" & HttpContext.Current.Request("frmEmailAddress") & "'"
                executeNonQuery(strSQL)
				responseWrite("1|Your details have beeen updated")
			Else
				responseWrite("0|Incorrect username or password")
            End If
        End If
    End Sub

End Class
