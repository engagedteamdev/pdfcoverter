﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class SecuredSourcing
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private strAPIKey As String = HttpContext.Current.Request("apiKey"), strGUID As String = Guid.NewGuid.ToString
    Private intAmount As String = HttpContext.Current.Request("frmAmount"), intTerm As String = HttpContext.Current.Request("frmTerm")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
    End Sub

    Public Sub getSourcing()

        checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), strAPIKey)

        If (Not checkValue(intAmount)) Then
            intAmount = 5000
        End If
        If (Not checkValue(intTerm)) Then
            intTerm = 5
        End If
        Dim strXML As String = ""
        Dim strSQL As String = "SELECT Lender, PolicyName, AnnualRate, MinTerm, MaxTerm, MinNetLoan, MaxNetLoan, " & _
            " CASE WHEN CONVERT (int , " & intAmount & ") >= 5000 AND CONVERT (int , " & intAmount & ") <= 14999 THEN " & intAmount & " * 0.12 WHEN CONVERT (int , " & intAmount & ") >= 15000 AND CONVERT (int , " & intAmount & ") <= 29999 THEN " & intAmount & " * 0.09 WHEN CONVERT (int , " & intAmount & ") >= 30000 AND CONVERT (int , " & intAmount & ") <= 44999 THEN CONVERT (int , " & intAmount & ") * 0.06 WHEN CONVERT (int , " & intAmount & ") >= 45000 AND CONVERT (int , " & intAmount & ") <= 59999 THEN " & intAmount & " * 0.05 WHEN CONVERT (int , " & intAmount & ") >= 60000 THEN " & intAmount & " * 0.04 ELSE 1 END AS RecomendedBrokerFee," & _
            " CASE WHEN CONVERT (int , tblsecuredpolicys.LenderFee) > (" & intAmount & " * CONVERT(DECIMAL(18,2),tblsecuredPolicys.LenderFeePercent)) THEN CONVERT (int , tblsecuredpolicys.LenderFee) ELSE " & intAmount & " * CONVERT(DECIMAL(18,2),tblsecuredPolicys.LenderFeePercent) END AS LenderFee" & _
            " FROM tblsecuredpolicys WHERE Lender <> ''"
        If (checkValue(intAmount)) Then
            strSQL += " AND MinNetLoan <= " & intAmount & " AND MaxNetLoan >= " & intAmount
        End If
        If (checkValue(intTerm)) Then
            strSQL += " AND MinTerm <= " & intTerm & " AND MaxTerm >= " & intTerm
        End If
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim intNumItems As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<SecuredSourcing>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<Policy>"
                    strXML += "<Lender>" & Row.Item("Lender") & "</Lender>"
                    strXML += "<PolicyName>" & Row.Item("PolicyName").ToString & "</PolicyName>"
                    strXML += "<AnnualRate>" & Row.Item("AnnualRate") & "</AnnualRate>"
                    strXML += "<RecomendedBrokerFee>" & Row.Item("RecomendedBrokerFee") & "</RecomendedBrokerFee>"
                    strXML += "<LenderFee>" & Row.Item("LenderFee") & "</LenderFee>"
                    strXML += "<MinTerm>" & Row.Item("MinTerm") & "</MinTerm>"
                    strXML += "<MaxTerm>" & Row.Item("MaxTerm") & "</MaxTerm>"
                    strXML += "<MinNetLoan>" & Row.Item("MinNetLoan") & "</MinNetLoan>"
                    strXML += "<MaxNetLoan>" & Row.Item("MaxNetLoan") & "</MaxNetLoan>"
                    strXML += "</Policy>"
                Next
                strXML += "</SecuredSourcing>"
            End If
            dsCache = Nothing
        End If
		
		dim strSQL2 as string =""_
            "INSERT INTO tblSecuredXMLs (AppID, CompanyID, XML, SourceDate) " & _
            "VALUES (" &  formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", "") & ", " & _
            formatField(strXML, "", "") & ", " & _          
            formatField(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), "DTTM", "") & ") "			
        executeNonQuery(strSQL2)
		
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write(strSQL2)
        HttpContext.Current.Response.End()

    End Sub

    Private Sub checkAuthority(ByVal ip As String, ByVal apiKey As String)
        If (checkValue(apiKey)) Then
            Dim boolAuthorised As Boolean
            Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
                  "FROM tblmediacampaigns INNER JOIN " & _
                  "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
                  "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                  "WHERE (tblwebservices.WebServiceAPIKey = '" & apiKey & "' OR '" & ip & "' = '127.0.0.1') " & _
                  "AND (ISNULL(WebServiceActive,1) = 1) "
            Dim objDataBase As New DatabaseManager
            Dim objResult As Object = objDataBase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                CompanyID = objResult.ToString
                boolAuthorised = True
            Else
                boolAuthorised = False
            End If
            objResult = Nothing
            objDataBase = Nothing
            If (boolAuthorised = False) Then
                unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
            End If
        Else
            unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
        End If
    End Sub

    Private Sub unsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        HttpContext.Current.Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""http://sourcing.engaged-solutions.co.uk/"">")
            .Append("<Result>Invalid request received: " & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append("</Response>")
        End With
        responseWrite(objStringBuilder.ToString)
        responseEnd()
    End Sub

End Class
