﻿Imports SSL, Config, Common, Reporting

Partial Class Statistics
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private StatisticID As String = HttpContext.Current.Request("StatisticID")
    Public strReportType As String = HttpContext.Current.Request("frmReportType")
    Private strMode As String = HttpContext.Current.Request("frmMode")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        checkSSL()
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise(False, Cache)
		checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If		
    End Sub

    Public Sub getStatistics()
        If (strMode = "Single") Then
            getSingleStatistic()
        Else
            getReportStatistics()
        End If
    End Sub

    Private Sub getSingleStatistic()
		Dim intUserID As String = getAnyField("UserID", "tblusers", "UserSessionID", HttpContext.Current.Request("UserSessionID"))
		Dim intSupplierCompanyID As String = getAnyField("UserSupplierCompanyID", "tblusers", "UserSessionID", HttpContext.Current.Request("UserSessionID"))
        Dim objStringWriter As StringWriter = New StringWriter, strStatValue As String = ""
        With objStringWriter
            Dim strSQL As String = "SELECT StatisticSQL, StatisticCachedMinutes FROM tblstatistics WHERE StatisticID = '" & StatisticID & "' AND CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(Cache, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strStatSQL As String = ""
                    strStatSQL = Row.Item("StatisticSQL").ToString
                    strStatSQL = Replace(strStatSQL, "{CompanyID}", CompanyID)
					strStatSQL = Replace(strStatSQL, "{UserID}", intUserID)
					strStatSQL = Replace(strStatSQL, "{MediaIDInbound}", intSupplierCompanyID)
                    If (checkValue(strStatSQL)) Then
                        Dim dsCache2 As DataTable = Nothing
                        If (Row.Item("StatisticCachedMinutes") > 0) Then
                            dsCache2 = New Caching(Cache, strStatSQL, "", Config.DefaultDateTime.AddMinutes(Row.Item("StatisticCachedMinutes")), "").returnCache
                        Else
                            dsCache2 = New Caching(Nothing, strStatSQL, "", "", "").returnCache
                        End If
                        If (dsCache2.Rows.Count > 0) Then
                            Dim x As Integer = 0
                            For Each Row2 As DataRow In dsCache2.Rows
                                For Each column As DataColumn In dsCache2.Columns

                                    If x = 0 Then
                                        strStatValue = Row2(column).ToString()
                                    Else
                                        strStatValue += "," & Row2(column).ToString()
                                    End If

                                    x += 1
                                Next
                            Next
                        End If
                    End If
                Next
            End If
            .Write(strStatValue)

            dsCache = Nothing
        End With
        HttpContext.Current.Response.Write(objStringWriter.ToString)
    End Sub

    Private Sub getReportStatistics()
        If (strReportType = "CaseHistory") Then
            objLeadPlatform.Reporting.listCaseHistory()
        Else
            objLeadPlatform.Reporting.listCases()
        End If
    End Sub

End Class