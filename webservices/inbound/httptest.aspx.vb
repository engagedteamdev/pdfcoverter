﻿Imports Config
Imports Common
Imports System.Net

Partial Class HTTPTest
    Inherits System.Web.UI.Page

    Public Sub HTTPTest()

        Dim strPostURL As String = "https://beta.engaged-solutions.co.uk/webservices/inbound/httppost.aspx"
        Dim strPostData As String = "MediaCampaignID=11508&LeadId=12345&FirstName=Engaged&Surname=Tester&HomePhone=01302230152&Postcode=DN93GA&MortgageSize=150000&MortgageType=First Time Buyer&PropertyValue=180000&MobilePhone=07123456789&Address1=1st Avenue&Note=Test note"

        Dim objResponse As HttpWebResponse = postWebRequest(strPostURL, strPostData, "", "")
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

        Response.Write(objReader.ReadToEnd())

        objReader.Close()
        objReader = Nothing

    End Sub

    Private Function postWebRequest(ByVal url As String, ByVal post As String, ByVal user As String, ByVal password As String, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
        If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
            Call Certificate.OverrideCertificateValidation()
        End If
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                .ContentType = "application/x-www-form-urlencoded"
                '.Headers.Add("Authorization", "Basic " & Convert.ToBase64String(Encoding.UTF8.GetBytes(user & ":" & password)))
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                'reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

End Class
