﻿Imports Config, Common

Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = "", intCompanyID As String = "", strNumber As String = "", strReplyNumber As String = HttpContext.Current.Request("frmReplyNumber")
    Private strUserName As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        strNumber = "0" & Right(Request("number"), 10)
        intCompanyID = getAnyField("CompanyID", "tblemailprofiles", "EmailProfileEmailAddress", strReplyNumber)
        Dim strSQL As String = "SELECT TOP 1 AppID FROM tblapplications WHERE App1MobileTelephone = '" & strNumber & "' AND CompanyID = '" & intCompanyID & "' ORDER BY AppID DESC"
        AppID = New Caching(Nothing, strSQL, "", "", "").returnCacheString
    End Sub

    Public Sub receiveSMS()
        Dim strNote As String = "SMS reply from " & strnumber & ": " & Request("message")
        If (checkValue(AppID)) Then
            CommonSave.saveNote(AppID, returnSystemUser(intCompanyID), strNote, intCompanyID)
        End If
        'sendMail("SMS Reply", encodeURL(Request.ServerVariables("QUERY_STRING")) & "<br />" & strNote)
		responseWrite(strNote)
    End Sub

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserFullName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=False&strAttachment=&UserSessionID=" & HttpContext.Current.Request("UserSessionID"))
    End Sub

End Class
