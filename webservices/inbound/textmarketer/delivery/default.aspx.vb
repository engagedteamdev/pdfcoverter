﻿Imports Config, Common

Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = ""
    Private strUserName As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        AppID = getAnyField("AppID", "tblpdfhistory", "PDFHistorySMSID", Request("id"))
    End Sub

    Public Sub receiveSMS()
        Dim strNote As String = "SMS delivery status report: " & Request("status") & " to " & Request("MSISDN") & " on " & unix2ddmmyyhhss(Request("delivered_date"))
		Dim intCompanyID As String = getAnyField("CompanyID", "tblpdfhistory", "PDFHistorySMSID", Request("id"))
        If (checkValue(AppID)) Then
            CommonSave.saveNote(AppID, returnSystemUser(intCompanyID), strNote, intCompanyID)
        End If
        'sendMail("SMS Delivery Report", encodeURL(Request.ServerVariables("QUERY_STRING")) & "<br />" & strNote)
		responseWrite(strNote)
    End Sub

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserFullName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=itsupport@engaged-solutions.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=False&strAttachment=&UserSessionID=" & HttpContext.Current.Request("UserSessionID"))
    End Sub

End Class
