using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;
using System.IO.Compression;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Collections.Specialized;

public partial class Default : Page
{
    private string AppID = HttpContext.Current.Request["AppID"];


	protected void Page_Load(object sender, EventArgs e)
    {
       // httpPost();
    }


    public void httpPost()
    {

		String data = new System.IO.StreamReader(Context.Request.InputStream).ReadToEnd();



		var json = JObject.Parse(data);
		var AccessID = json["accessId"].ToString();
		var Reference = json["reference"].ToString();
		var Token = json["token"].ToString();
		var Status = json["status"].ToString();
		var CreatedDate = json["createDate"].ToString();
		var NotifyType = json["notifyType"].ToString();

		if (AccessID != "")
		{


			string url = "https://integrations.engagedcrm.co.uk/webservices/outbound/mogoplus/default.aspx";

			using (var wb = new WebClient())
			{
				var querydata = new NameValueCollection();
				querydata["Type"] = "DataRetrieval";
				querydata["CompanyName"] = "Engaged";
				querydata["AccessID"] = "" + AccessID + "";


				var response = wb.UploadValues(url, "POST", querydata);
				string responseInString = Encoding.UTF8.GetString(response);
				HttpContext.Current.Response.Write(responseInString);
			}

			
			HttpContext.Current.Response.End();
		}


	}


  



}

