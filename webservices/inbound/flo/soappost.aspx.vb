﻿Imports Config
Imports Common
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class SOAP
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = ""
    Private Const MediaID As String = "1020" ' Live 1028
    Private MediaCampaignID As String = "", strLeadID As String = "", strProductType As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private CompanyID As String = "0"

    Public Sub SOAPPost()
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//MortgageLead")
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = MortgageLead", -2, intSOAPRequestNo)
        End If
        Dim objLeadID As XmlNode = objInputXMLDoc.SelectSingleNode("//id")
        If (Not objLeadID Is Nothing) Then
            strLeadID = objLeadID.InnerText
        End If

        Dim objLoanPurpose As XmlNode = objInputXMLDoc.SelectSingleNode("//product")
        If (Not objLoanPurpose Is Nothing) Then
            Select Case objLoanPurpose.InnerText
                Case "Mortgage"
                    strProductType = "Mortgage - REM"
                    MediaCampaignID = "10075" ' Live 10218
                Case "Loan"
                    strProductType = "Secured Loan"
                    MediaCampaignID = "10076"
            End Select
        End If
        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        Call checkMandatory()
        Call readData()
        Call writeData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.CompanyID " & _
                                "FROM tblmediacampaigns INNER JOIN " & _
                                "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                                "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                                "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;') " & _
                                "AND (WebServiceActive = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkAuthoritySimple(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 CompanyID " & _
                                "FROM tblwebservices " & _
                                "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;') " & _
                                " AND (WebServiceActive = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (Not boolAuthorised) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"product", "amount", "firstName", "surname", "telHome", "address1", "postcode"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            Call SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo)
        End If
    End Sub

    Private Sub readData()
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.DocumentElement.SelectSingleNode("//MortgageLead").ChildNodes
        For Each child As XmlNode In objChildNodes
            validateData(child.Name, child.InnerText)
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex")
                strMessage = Row.Item("ValRegexMessage")
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        ' Call regular expression routine
        If (regexTest(strRegex, val)) Then
            If (checkValue(val)) Then
                If (checkValue(strFunction)) Then
                    Dim arrParams As String() = {val}
                    val = executeMethodByName(Me, strFunction, arrParams)
                End If
                Select Case strTable
                    Case "tblapplications"
                        tblapplications_flds.Add(strField)
                        tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatus"
                        tblapplicationstatus_flds.Add(strField)
                        tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                End Select
            End If
            boolValidate = True
        Else
            boolValidate = False
        End If

        If (boolValidate = False) Then
            Call SOAPUnsuccessfulMessage("Inconsistent data found. Field = " & fld & ", value = " & val & ", Criteria = " & strMessage, -5, intSOAPRequestNo)
        End If

    End Sub
    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub writeData()
        Dim boolWrite As Boolean = True
        Dim strQryFields As String = "", strQryValues As String = "", strQryFieldsValues As String = ""
        ' Only start insert if main application array is found
        If (tblapplications_flds.Count > 0) Then

            For i = 0 To tblapplications_flds.Count - 1
                If (i = 0) Then
                    strQryFields = "INSERT INTO tblapplications (CompanyID, MediaCampaignIDInbound, ProductType, "
                    strQryValues = "VALUES (" & formatField(CompanyID, "N", 0) & ", " & formatField(MediaCampaignID, "N", 0) & ", " & formatField(strProductType, "", "Mortgage - REM") & ", "
                End If

                strQryFields = strQryFields & tblapplications_flds(i)
                strQryValues = strQryValues & tblapplications_vals(i)

                If (i <> tblapplications_flds.Count - 1) Then
                    strQryFields = strQryFields & ", "
                    strQryValues = strQryValues & ", "
                Else
                    strQryFields = strQryFields & ") "
                    strQryValues = strQryValues & ") "
                End If
            Next

            'Response.Write(strQryFields & strQryValues & "<br />")
            'Response.End()
            AppID = executeIdentityQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            ' Insert default starter values in the application status table
            strQryFields = "INSERT INTO tblapplicationstatus (CompanyID, AppID) "
            strQryValues = "VALUES (" & formatField(CompanyID, "N", 0) & ", " & formatField(AppID, "N", 0) & ") "

            'Response.Write(strQryFields & strQryValues & "<br />")
            Call executeNonQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            If (tblapplicationstatus_flds.Count > 0) Then
                For i = 0 To tblapplicationstatus_flds.Count - 1
                    If (i = 0) Then
                        strQryFieldsValues = "UPDATE tblapplicationstatus SET "
                    End If

                    strQryFieldsValues = strQryFieldsValues & tblapplicationstatus_flds(i) & " = " & tblapplicationstatus_vals(i)

                    If (i <> tblapplicationstatus_flds.Count - 1) Then
                        strQryFieldsValues = strQryFieldsValues & ", "
                    Else
                        strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' "
                    End If
                Next
                'Response.Write(strQryFieldsValues & "<br />")
                Call executeNonQuery(strQryFieldsValues)
                strQryFieldsValues = ""
            End If

            'Set working hour
            If (checkWorkingHour(Config.DefaultDateTime)) Then
                Call executeNonQuery("UPDATE tblapplicationstatus SET CreatedInsideBusinessHours = 1 WHERE (AppID = '" & AppID & "')")
            End If

            ' Set COA
            Call executeNonQuery("EXECUTE spcostofacquisition @AppID = " & formatField(AppID, "N", 0) & ",@CompanyID = " & formatField(CompanyID, "N", 0))

            ' Set LTV
            Call executeNonQuery("EXECUTE spupdateltv @AppID = " & formatField(AppID, "N", 0) & ",@CompanyID = " & formatField(CompanyID, "N", 0))

            ' Set BTL Yield
            Call executeNonQuery("EXECUTE spupdatebtlyield @AppID = " & formatField(AppID, "N", 0) & ",@CompanyID = " & formatField(CompanyID, "N", 0))

            ' Correct telephone numbers
            Call executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & formatField(AppID, "N", 0) & ",@CompanyID = " & formatField(CompanyID, "N", 0))

            ' Set CountryCode
            Call executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT CountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "')), 'ENG') WHERE (AppID = '" & AppID & "') AND (CompanyID = '" & CompanyID & "')")

            ' Run duplicate check
            Call executeNonQuery("EXECUTE spduplicateappcheck @AppID = " & formatField(AppID, "N", 0) & ",@CompanyID = " & formatField(CompanyID, "N", 0))

            boolWrite = True

        Else

            boolWrite = False

        End If

        If (Not boolWrite) Then
            Call SOAPUnsuccessfulMessage("An unknown error has occured", -1, intSOAPRequestNo)
        Else
            Call SOAPSuccessfulMessage("Application successfully received", 1, AppID)
        End If

    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crmdev.lunarmedia.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crmdev.lunarmedia.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
