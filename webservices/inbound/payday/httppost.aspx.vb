﻿Imports SSL, Config, Common, CommonSave
Imports System.Net
Imports System.Xml

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Previous Addresses
    Private tbladdresses_flds As ArrayList = New ArrayList, tbladdresses_vals As ArrayList = New ArrayList
    ' Previous Employers
    Private tblemployers_flds As ArrayList = New ArrayList, tblemployers_vals As ArrayList = New ArrayList
    ' Notes
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList
    ' Reminders
    Private tblreminders_flds As ArrayList = New ArrayList, tblreminders_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    ' Milestones
    Private tblapplicationstatusdates_flds As ArrayList = New ArrayList, tblapplicationstatusdates_vals As ArrayList = New ArrayList
    ' Diaries
    Private tbldiaries_flds As ArrayList = New ArrayList, tbldiaries_vals As ArrayList = New ArrayList
    ' Customer Logons
    Private tblcustomerlogons_flds As ArrayList = New ArrayList, tblcustomerlogons_vals As ArrayList = New ArrayList
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private UserID As String = HttpContext.Current.Request("UserID")
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strReturnURL As String = HttpContext.Current.Request("ReturnURL")
    Private intSkipTelephoneCorrection As String = HttpContext.Current.Request("SkipTelephoneCorrection")
    Private intSkipLiveTransfer As String = HttpContext.Current.Request("SkipLiveTransfer")
    Private strInteractive As String = HttpContext.Current.Request("Interactive")
    Private strCheckCard As String = HttpContext.Current.Request("CheckCard")
    Private strMakePayment As String = HttpContext.Current.Request("MakePayment")
    Private strNoStatusChange As String = HttpContext.Current.Request("NoStatusChange")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

    Public Sub httpPost()
        Server.ScriptTimeout = 120
        If (Left(AppID, 1) = "0") Then
            AppID = Right(AppID, 7)
        End If
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        If (Not checkValue(AppID)) Then
            intSkipTelephoneCorrection = "1"
            Call checkAuthority(Request.ServerVariables("REMOTE_ADDR"))
        Else
            intSkipTelephoneCorrection = "1"
            Call checkAuthoritySimple(Request.ServerVariables("REMOTE_ADDR"))
            If checkValue(MediaCampaignID) Then
                If (getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID) <> CInt(MediaCampaignID) Or Not checkValue(getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID))) Then
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid AppID - " & AppID)
                    Response.Write("Invalid AppID - " & AppID)
                    Response.End()
                End If
            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid Media Campaign - " & MediaCampaignID)
                Response.Write("Invalid Media Campaign - " & MediaCampaignID)
                Response.End()
            End If
        End If
        'If (Not checkValue(AppID)) Then
        '    Call checkMandatory()
        'Else
        Call checkMandatorySimple()
        'End If
        Call readData()
        Call saveData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.WebServiceID " & _
                                "FROM tblmediacampaigns INNER JOIN " & _
                                "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                                "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                                "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR tblwebservices.WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                "AND (WebServiceActive = 1) AND tblmediacampaigns.CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub

    Private Sub checkAuthoritySimple(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 WebServiceID " & _
                                "FROM tblwebservices " & _
                                "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;' OR WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                " AND (WebServiceActive = 1) AND CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID", "Amount", "AddressPostCode"}
        For i As Integer = 0 To UBound(arrMandatory)
            If (Not checkValue(Request(arrMandatory(i)))) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -3, "Mandatory Data Missing [" & strMandatoryField & "]")
            Response.Write("Mandatory Data Missing [" & strMandatoryField & "]")
            Response.End()
        End If
    End Sub

    Private Sub checkMandatorySimple()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID"}
        For i As Integer = 0 To UBound(arrMandatory)
            If (Not checkValue(Request(arrMandatory(i)))) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -3, "Mandatory Data Missing [" & strMandatoryField & "]")
            Response.Write("Mandatory Data Missing [" & strMandatoryField & "]")
            Response.End()
        End If
    End Sub

    Private Sub readData()
        For Each Item In Request.QueryString
            If (checkValue(Item)) Then
                Call validateData(Item, Request.QueryString(Item))
            End If
        Next
        For Each Item In Request.Form
            If (checkValue(Item)) Then
                Call validateData(Item, Request.Form(Item))
            End If
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = "", boolDataGrid As Boolean = False
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                strFunctionParameters = Row.Item("ValFunctionParameters").ToString
                boolDataGrid = Row.Item("ApplicationDataGridColumn")
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        val = Replace(val, vbLf, "<br />")

        ' Call regular expression routine
        If (boolDataGrid) Then
            Dim arrVal As Array = Split(val, ",")
            For x As Integer = 0 To UBound(arrVal)
                If (checkValue(strRegex)) Then
                    If (regexTest(strRegex, arrVal(x))) Then
                        If (checkValue(val) Or checkValue(AppID)) Then
                            tbldatastore_flds.Add(strField)
                            tbldatastore_vals.Add(formatField(val, "", ""))
                        End If
                        boolValidate = True
                    Else
                        boolValidate = False
                        Exit For
                    End If
                Else
                    boolValidate = True
                End If
            Next
        Else
            If (regexTest(strRegex, val)) Then
                If (checkValue(val) Or checkValue(AppID)) Then
                    If (checkValue(strFunction)) Then
                        If (strFunction = "splitApp1FullName") Then
                            splitApp1FullName(val)
                        ElseIf (strFunction = "splitApp2FullName") Then
                            splitApp2FullName(val)
                        Else
                            Dim arrParams As String() = {val}
                            If (checkValue(strFunctionParameters)) Then
                                Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                                For y As Integer = 0 To UBound(arrFunctionParameters)
                                    If (checkValue(arrFunctionParameters(y))) Then
                                        ReDim Preserve arrParams(y + 1)
                                        arrParams(y + 1) = arrFunctionParameters(y)
                                    End If
                                Next
                            End If
                            val = executeMethodByName(Me, strFunction, arrParams)
                        End If
                    End If
                    Select Case strTable
                        Case "tblapplications"
                            If (checkValue(AppID)) Then
                                If (fld <> "MediaCampaignID" And strField <> "LoanType") Then
                                    tblapplications_flds.Add(strField)
                                    tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                                End If
                            Else
                                tblapplications_flds.Add(strField)
                                tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                            End If
                        Case "tblapplicationstatus"
                            tblapplicationstatus_flds.Add(strField)
                            tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbladdresses"
                            tbladdresses_flds.Add(strField)
                            tbladdresses_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblemployers"
                            tblemployers_flds.Add(strField)
                            tblemployers_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblnotes"
                            tblnotes_flds.Add(strField)
                            tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblreminders"
                            tblreminders_flds.Add(strField)
                            tblreminders_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbldatastore"
                            If (strField = "BankCardNumber" Or strField = "BankCardCV2") Then
                                ' Do nothing, encryption to come later
                            Else
                                tbldatastore_flds.Add(strField)
                                tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                            End If
                        Case "tblapplicationstatusdates"
                            tblapplicationstatusdates_flds.Add(strField)
                            tblapplicationstatusdates_vals.Add(val)
                        Case "tbldiaries"
                            tbldiaries_flds.Add(strField)
                            tbldiaries_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblcustomerlogons"
                            tblcustomerlogons_flds.Add(strField)
                            tblcustomerlogons_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    End Select
                End If
                boolValidate = True
            Else
                boolValidate = False
            End If
        End If

        If (boolValidate = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -5, "Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
            If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
                Else
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
                End If
            Else
                Response.Write("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
                Response.End()
            End If
        End If

    End Sub
    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    'Private Sub writeData()
    '    Dim boolWrite As Boolean = True
    '    Dim strQryFields As String = "", strQryValues As String = ""
    '    ' Only start insert if main application array is found
    '    If (tblapplications_flds.Count > 0) Then
    '        For i = 0 To tblapplications_flds.Count - 1
    '            If (i = 0) Then
    '                strQryFields = "INSERT INTO tblapplications ("
    '                strQryValues = "VALUES ("
    '            End If

    '            strQryFields = strQryFields & tblapplications_flds(i)
    '            strQryValues = strQryValues & tblapplications_vals(i)

    '            If (i <> tblapplications_flds.Count - 1) Then
    '                strQryFields = strQryFields & ", "
    '                strQryValues = strQryValues & ", "
    '            Else
    '                strQryFields = strQryFields & ") "
    '                strQryValues = strQryValues & ") "
    '            End If
    '        Next

    '        'Response.Write(strQryFields & strQryValues & "<br />")

    '        AppID = executeIdentityQuery(strQryFields & strQryValues)

    '        strQryFields = ""
    '        strQryValues = ""

    '        ' Insert default starter values in the application status table
    '        strQryFields = "INSERT INTO tblapplicationstatus (AppID) "
    '        strQryValues = "VALUES (" & formatField(AppID, "N", 0) & ") "

    '        'Response.Write(strQryFields & strQryValues & "<br />")

    '        Call executeNonQuery(strQryFields & strQryValues)

    '        strQryFields = ""
    '        strQryValues = ""

    '        ' Insert previous addresses
    '        If (tbladdresses_flds.Count > 0) Then
    '            For i = 0 To tbladdresses_flds.Count - 1
    '                If (i = 0) Then
    '                    strQryFields = "INSERT INTO tbladdresses (AppID"
    '                    strQryValues = "VALUES (" & AppID
    '                End If

    '                strQryFields = strQryFields & tbladdresses_flds(i)
    '                strQryValues = strQryValues & tbladdresses_vals(i)

    '                If (i <> tbladdresses_flds.Count - 1) Then
    '                    strQryFields = strQryFields & ", "
    '                    strQryValues = strQryValues & ", "
    '                Else
    '                    strQryFields = strQryFields & ") "
    '                    strQryValues = strQryValues & ") "
    '                End If
    '            Next
    '        End If

    '        ' Insert previous employers
    '        If (tblemployers_flds.Count > 0) Then
    '            For i = 0 To tblemployers_flds.Count - 1
    '                If (i = 0) Then
    '                    strQryFields = "INSERT INTO tblemployers (AppID"
    '                    strQryValues = "VALUES (" & AppID
    '                End If

    '                strQryFields = strQryFields & tblemployers_flds(i)
    '                strQryValues = strQryValues & tblemployers_vals(i)

    '                If (i <> tblemployers_flds.Count - 1) Then
    '                    strQryFields = strQryFields & ", "
    '                    strQryValues = strQryValues & ", "
    '                Else
    '                    strQryFields = strQryFields & ") "
    '                    strQryValues = strQryValues & ") "
    '                End If
    '            Next
    '        End If

    '        'Set working hour
    '        If (checkWorkingHour(Config.DefaultDateTime)) Then
    '            Call executeNonQuery("UPDATE tblapplicationstatus SET CreatedInsideBusinessHours = 1 WHERE (AppID = '" & AppID & "')")
    '        End If

    '        ' Set COA
    '        Call executeNonQuery("EXECUTE spcostofacquisition @AppID = " & formatField(AppID, "N", 0))

    '        ' Set LTV
    '        Call executeNonQuery("EXECUTE spupdateltv @AppID = " & formatField(AppID, "N", 0))

    '        ' Set BTL Yield
    '        Call executeNonQuery("EXECUTE spupdatebtlyield @AppID = " & formatField(AppID, "N", 0))

    '        ' Correct telephone numbers
    '        If (intSkipTelephoneCorrection <> "1") Then
    '            Call executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & formatField(AppID, "N", 0))
    '        End If

    '        ' Set CountryCode
    '        Call executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT CountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "')), 'ENG') WHERE (AppID = '" & AppID & "')")

    '        ' Run duplicate check
    '        Call executeNonQuery("EXECUTE spduplicateappcheck @AppID = " & formatField(AppID, "N", 0))

    '        boolWrite = True

    '    Else

    '        boolWrite = False

    '    End If

    '    If (Not boolWrite) Then
    '        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, -1, "An unknown error has occured")
    '        Response.Write("An unknown error has occured.")
    '        Response.End()
    '    Else
    '        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), AppID, 1, "Application successfully received")
    '        If (checkValue(strReturnURL)) Then
    '            responseRedirect(strReturnURL)
    '        Else
    '            Response.Write("1|" & AppID)
    '        End If
    '    End If
    'End Sub

    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub saveData()

        Dim strResult As String = saveApplication(False, AppID, UserID, CompanyID, MediaCampaignID, "", intSkipTelephoneCorrection, _
                                                    tblapplications_flds, _
                                                    tblapplications_vals, _
                                                    tblapplicationstatus_flds, _
                                                    tblapplicationstatus_vals, _
                                                    tbladdresses_flds, _
                                                    tbladdresses_vals, _
                                                    tblemployers_flds, _
                                                    tblemployers_vals, _
                                                    tblnotes_flds, _
                                                    tblnotes_vals, _
                                                    tblreminders_flds, _
                                                    tblreminders_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals, _
                                                    tblapplicationstatusdates_flds, _
                                                    tblapplicationstatusdates_vals, _
                                                    tbldiaries_flds, _
                                                    tbldiaries_vals, _
                                                    tblcustomerlogons_flds, _
                                                    tblcustomerlogons_vals, _
                                                    strInteractive)

        Dim arrResult As Array = Split(strResult, "|")
        Dim strStatusCode As String = "", strSubStatusCode As String = ""
        If (UBound(arrResult) >= 1) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), arrResult(1), 1, "Application successfully received")
            executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & arrResult(1) & "'")
            getRenewal(arrResult(1))
            Call executeNonQuery("UPDATE tblapplications SET AddressRegion = ISNULL ((SELECT (SELECT PostCodeRegion FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & arrResult(1) & "' AND CompanyID = '" & CompanyID & "')), '') WHERE (AppID = '" & arrResult(1) & "' AND CompanyID = '" & CompanyID & "')")
            strStatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", arrResult(1))
            strSubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", arrResult(1))
            AppID = arrResult(1)
        End If
        If (UBound(arrResult) >= 1 And strStatusCode = "INV") Then
            Select Case strSubStatusCode
                Case "DUP"
                    If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&duplicate=1")
                        Else
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?duplicate=1")
                        End If
                    Else
                        Response.Write("0|Duplicate+case")
                    End If
                Case "WRN"
                    If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&invalid=1")
                        Else
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?invalid=1")
                        End If
                    Else
                        Response.Write("0|Invalid+telephone+number")
                    End If
            End Select
        ElseIf (UBound(arrResult) >= 1 And strStatusCode = "BLA") Then
            If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&blacklist=1")
                Else
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?blacklist=1")
                End If
            Else
                Response.Write("0|Blacklisted+case")
            End If
        ElseIf (UBound(arrResult) >= 1 And getActiveLoan(AppID)) Then
            If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&active=1")
                Else
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?active=1")
                End If
            Else
                Response.Write("0|Active+loan+found")
                saveStatus(AppID, Config.DefaultUserID, "INV", "ACT")
            End If
        Else
            If (checkValue(strReturnURL)) Then
                If (InStr(strReturnURL, "?") > 0) Then
                    If (UBound(arrResult) = 1) Then
                        responseRedirect(strReturnURL & "&AppID=" & arrResult(1))
                    Else
                        responseRedirect(strReturnURL)
                    End If
                Else
                    If (UBound(arrResult) = 1) Then
                        responseRedirect(strReturnURL & "?AppID=" & arrResult(1))
                    Else
                        responseRedirect(strReturnURL)
                    End If
                End If
            Else
                ' Run credit search if requested
                If (strStatusCode <> "INV" And strStatusCode <> "BLA") Then

                    Dim boolValidCard As Boolean = True, boolValidPayment As Boolean = True
                    If (checkValue(Request("BankCardNumber"))) Then
                        boolValidCard = checkCardDetails(AppID)
                    End If
                    If (strMakePayment = "Y") Then
                        boolValidPayment = makePayment(AppID)
                    End If

                    Dim strPaydayDecision As String = ""
                    If (strInteractive = "Y") Then
                        strPaydayDecision = getPaydayDecision(arrResult(1))
                        Dim intAmount As Decimal = CDec(Request("Amount"))
                        Dim boolDownSell As Boolean = getAnyField("MediaCampaignDownSell", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
                        If (strPaydayDecision = "N" And boolDownSell) Then
                            While strPaydayDecision = "N" And intAmount >= 100
                                executeNonQuery("UPDATE tblapplications SET Amount = (Amount - 50) WHERE Amount >= 100 AND AppID = '" & arrResult(1) & "' AND CompanyID = '" & CompanyID & "'")
                                executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & arrResult(1) & "'")
                                intAmount = intAmount - 50
                                saveNote(arrResult(1), Config.DefaultUserID, "Automatic down-sell: " & displayCurrency(intAmount))
                                strPaydayDecision = getPaydayDecision(arrResult(1))
                            End While
                        End If
                        If (strPaydayDecision = "Y") Then
                            saveUpdatedDate(arrResult(1), Config.DefaultUserID, "AffiliateAccepted")
                            saveNote(arrResult(1), Config.DefaultUserID, "Decision given to supplier: " & strPaydayDecision)
                            strResult = "1|" & arrResult(1) & "|" & arrResult(2) & "|" & strPaydayDecision
                        Else
                            saveStatus(AppID, Config.DefaultUserID, "INV", "OOC")
                            saveUpdatedDate(AppID, Config.DefaultUserID, "INV")
                            saveUpdatedDate(arrResult(1), Config.DefaultUserID, "AffiliateRejected")
                            saveNote(arrResult(1), Config.DefaultUserID, "Decision given to supplier: " & strPaydayDecision)
                            strResult = "1|" & arrResult(1) & "|" & arrResult(2) & "|" & strPaydayDecision
                        End If
                    End If
                    'If (strInteractive = "Y" And strPaydayDecision = "Y") Then
                    'Dim strCreditDecision As String = getCreditDecision(arrResult(1))
                    'strResult = "1|" & arrResult(1) & "|" & arrResult(2) & "|" & strCreditDecision
                    'saveNote(arrResult(1), Config.DefaultUserID, "Decision given to supplier: " & strCreditDecision)
                    'If (strCreditDecision = "Y") Then
                    'saveUpdatedDate(arrResult(1), Config.DefaultUserID, "AffiliateAccepted")
                    'Else
                    'saveUpdatedDate(arrResult(1), Config.DefaultUserID, "AffiliateRejected")
                    'End If
                    'ElseIf (strInteractive = "Y" And strPaydayDecision = "N") Then
                    'strResult = "1|" & arrResult(1) & "|" & arrResult(2) & "|" & strPaydayDecision
                    'saveNote(arrResult(1), Config.DefaultUserID, "Decision given to supplier: " & strPaydayDecision)
                    'saveUpdatedDate(arrResult(1), Config.DefaultUserID, "AffiliateRejected")
                    'Else
                    If (checkValue(Request("BankCardNumber"))) Then
                        If (Not boolValidCard) Then
                            strResult = "0|Invalid+card+details"
                        End If
                    End If
                    If (strMakePayment = "Y") Then
                        If (Not boolValidPayment) Then
                            strResult = "0|Payment+unsuccessful"
                        End If
                    End If
                    'End If

                End If
                ' Send customer communication
                strStatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
                If (strStatusCode = "NEW" Or (strInteractive = "Y" And strStatusCode = "TFR")) Then
                    Dim intLetterID As String = getAnyField("MediaCampaignLetterID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
                    If (checkValue(intLetterID)) Then
                        If (CInt(intLetterID) > 0) Then
                            Dim intLetterType As String = getAnyField("LetterType", "tbllettertemplates", "LetterID", intLetterID)
                            If (intLetterType = 2) Then
                                sendEmails(AppID, intLetterID, CompanyID)
                            ElseIf (intLetterType = 3) Then
                                sendSMS(AppID, intLetterID)
                            End If
                        End If
                    End If
                End If
                If (checkValue(Request("BankCardNumber")) Or checkValue(Request("BankCardCV2"))) Then
                    executeNonQuery("EXECUTE spencryptcarddetails @AppID = '" & AppID & "',@CompanyID = " & CompanyID & ", @CardNumber = '" & Request("BankCardNumber") & "', @CardCV2 = '" & Request("BankCardCV2") & "'")
                End If
                Response.Write(strResult)
            End If
        End If
    End Sub

    'Public Function saveAdditionalFields() As Boolean
    '    Dim strQry As String = "", strScriptID As String = ""
    '    Dim intRowsAffected As Integer = 0
    '    For Each Item In Request.Form
    '        If (Left(Item, 6) = "Script") Then
    '            strScriptID = Replace(Item, "Script", "")
    '            strQry = "UPDATE tblapplicationscriptanswers SET " & _
    '            "ScriptAnswer = " & formatField(Request.Form(Item), "", "") & " " & _
    '           "WHERE ScriptID = '" & strScriptID & "' AND AppID = '" & AppID & "'"
    '            intRowsAffected = executeRowsAffectedQuery(strQry)
    '            If (intRowsAffected = 0) Then
    '                strQry = "INSERT INTO tblapplicationscriptanswers (AppID, ScriptID, ScriptAnswer) " & _
    '                   "VALUES(" & formatField(AppID, "N", 0) & ", " & _
    '                   formatField(strScriptID, "N", 0) & ", " & _
    '                   formatField(Request.Form(Item), "", "") & ")"
    '                executeNonQuery(strQry)
    '            End If
    '        End If
    '    Next
    '    Return True
    'End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strFormPost As String = ""
        Dim x As Integer = 0
        For Each Item In Request.Form
            If (checkValue(Item)) Then
                If (x > 0) Then
                    strFormPost += "&" & Item & "=" & Request.Form(Item)
                Else
                    strFormPost += Item & "=" & Request.Form(Item)
                End If
                x += 1
            End If
        Next
        For Each Item In Request.QueryString
            If (checkValue(Item)) Then
                If (checkValue(strFormPost)) Then
                    strFormPost += "&" & Item & "=" & Request.QueryString(Item)
                Else
                    strFormPost += Item & "=" & Request.QueryString(Item)
                End If
            End If
        Next
        strFormPost = regexReplace("&BankCardNumber=([0-9]{0,16})", "&BankCardNumber=****************", strFormPost)
        strFormPost = regexReplace("&BankCardCV2=([0-9]{0,16})", "&BankCardCV2=***", strFormPost)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(strFormPost, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub splitApp1FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 1)
    End Sub

    Private Sub splitApp2FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 2)
    End Sub

    Private Sub splitFullName(ByVal strFullName As String, ByVal intAppNo As Integer)
        Dim boolTitleSet As Boolean = False
        Dim strMiddleNames As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                validateData("App" & intAppNo & "Surname", arrFullName(i))
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                validateData("App" & intAppNo & "Title", arrFullName(i))
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        If (strMiddleNames <> "") Then
            validateData("App" & intAppNo & "MiddleNames", strMiddleNames)
        End If
    End Sub

    Private Sub runCreditCheck(ByVal AppID As String, ByVal MediaCampaignID As String)
        getExperianWebRequest(Config.ApplicationURL & "/webservices/outbound/experian/scems/?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&Interactive=Y")
    End Sub

    Private Function getExperianWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                If (InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                .Method = WebRequestMethods.Http.Get
                .KeepAlive = False
                .Timeout = 120000
                .ReadWriteTimeout = 120000
            End With
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Private Function getPaydayDecision(ByVal AppID As String) As String
        Dim strQry As String = "SELECT TOP 1 MediaCampaignID FROM vwpaydaysearch WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND MediaCampaignDeliveryTypeID <> 9"
        Dim intDecision As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
        If (checkValue(intDecision)) Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function

    Private Function getCreditDecision(ByVal AppID As String) As String
        Dim strQry As String = "SELECT TOP 1 MediaCampaignID FROM vwdistributionsearch WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND MediaCampaignDeliveryTypeID <> 9"
        Dim intDecision As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
        If (checkValue(intDecision)) Then
            Return "Y"
        Else
            saveStatus(AppID, Config.DefaultUserID, "INV", "CRD")
            saveUpdatedDate(AppID, Config.DefaultUserID, "INV")
            Return "N"
        End If
    End Function

    Private Function checkCardDetails(ByVal AppID As String) As Boolean
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/scheduled/takepayments.aspx?frmMode=1&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&frmNoStatusChange=" & strNoStatusChange)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
        End If
        If (strStatus = "1") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function makePayment(ByVal AppID As String) As Boolean
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/scheduled/takepayments.aspx?frmMode=3&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
        End If
        If (strStatus = "1") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function getActiveLoan(ByVal AppID As String) As Boolean
        If (checkValue(Request("App1DOB"))) Then
            Dim intCustomerID As String = getAnyField("CustomerID", "tblapplications", "AppID", AppID)
            Dim strApp1Surname As String = Request("App1Surname")
            Dim dteApp1DOB As String = Request("App1DOB")
            Dim strQry As String = "SELECT TOP 1 APP.AppID FROM tblapplications APP INNER JOIN tblapplicationstatus APS ON APS.AppID = APP.AppID INNER JOIN tblcustomerlogons LOG ON LOG.CustomerID = APP.CustomerID WHERE (ProductType = N'Payday Loan') AND (LOG.CustomerID = '" & intCustomerID & "' OR (App1Surname = '" & Replace(strApp1Surname, "'", "''") & "' AND App1DOB = " & formatField(dteApp1DOB & " 00:00:00", "DTTM", Config.DefaultDate) & ")) AND (StatusCode IN ('ACC','APP','PRQ','FPB','POT','PAY','DEF','CPB','BAD','OFF','EXT','COL','DEB','DMP')) AND Active = 1 ORDER BY UpdatedDate DESC"
            Dim strResult As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            If (checkValue(strResult)) Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Sub getRenewal(ByVal AppID As String)
        If (checkValue(Request("App1DOB"))) Then
            Dim intCustomerID As String = getAnyField("CustomerID", "tblapplications", "AppID", AppID)
            Dim strApp1Surname As String = Request("App1Surname")
            Dim dteApp1DOB As String = Request("App1DOB")
            Dim strQry As String = "SELECT TOP 1 APP.AppID FROM tblapplications APP INNER JOIN tblapplicationstatus APS ON APS.AppID = APP.AppID INNER JOIN tblcustomerlogons LOG ON LOG.CustomerID = APP.CustomerID WHERE (ProductType = N'Payday Loan') AND (LOG.CustomerID = '" & intCustomerID & "' OR (App1Surname = '" & Replace(strApp1Surname, "'", "''") & "' AND App1DOB = " & formatField(dteApp1DOB & " 00:00:00", "DTTM", Config.DefaultDate) & ")) AND (StatusCode IN ('SET','PPY','COL','DEB','DMP','PAY','OFF')) AND Active = 1 ORDER BY UpdatedDate DESC"
            Dim strResult As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            If (checkValue(strResult)) Then
                updateDataStoreField(AppID, "LoanRenewal", "Y", "U", "")
                saveUpdatedDate(AppID, Config.DefaultUserID, "Renewal")
            Else
                updateDataStoreField(AppID, "LoanRenewal", "N", "U", "")
            End If
        End If
    End Sub

    Private Sub assignCallCentreUser()
        Dim strQry As String = "SELECT TOP 1 UserID FROM tblusers WHERE UserCallCentre = 1 AND UserLoggedIn = 1 AND UserSuperAdmin = 0 AND UserActive = 1 AND UserLockedOut = 0 AND CompanyID = " & CompanyID & " ORDER BY (SELECT COUNT(AppID) AS Expr1 FROM dbo.tblapplicationstatus WHERE (CallCentreUserID = dbo.tblusers.UserID) AND (DATEDIFF(DD, CreatedDate, GETDATE()) = 0))"
        Dim strResult As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
        executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = " & formatField(strResult, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
    End Sub

End Class