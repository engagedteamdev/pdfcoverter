﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class CloneApplication
    Inherits System.Web.UI.Page

    Private strXMLURL As String = ""
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private UserID As String = HttpContext.Current.Request("UserID")
    Private strExistingAppID As String = ""
    Private strProductType As String = HttpContext.Current.Request("ProductType")
    Private intMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private intCloneMediaCampaignID As String = HttpContext.Current.Request("CloneMediaCampaignID")
    Private intMediaCampaignIDOutbound As String = HttpContext.Current.Request("MediaCampaignIDOutbound")
    Private strStatusCode As String = HttpContext.Current.Request("StatusCode"), strSubStatusCode As String = HttpContext.Current.Request("SubStatusCode")
    Private intAction As String = HttpContext.Current.Request("Action")
    Private strMaintainSubStatus As String = HttpContext.Current.Request("MaintainSubStatus")
    Private strForceClone As String = HttpContext.Current.Request("ForceClone")
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XMLDocument = New XMLDocument
    Private strErrorMessage As String = ""
    Private objLeadPlatform As LeadPlatform = New LeadPlatform

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform.initialise()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
    End Sub

    Public Sub cloneApplication()
        strXMLURL = Config.ApplicationURL & "/webservices/inbound/soappost.aspx"
        If checkValue(AppID) Then
            If (strForceClone = "Y") Then
                sendXML()
            Else
                strExistingAppID = checkExistingApplication()
                If (checkValue(strExistingAppID)) Then
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.Write(1 & "|" & strExistingAppID)
                Else
                    sendXML()
                End If
            End If
        End If
    End Sub

    Private Function checkExistingApplication() As String
        Dim strAppID As String = ""
        Dim strSQL As String = "SELECT AppID FROM tblapplications WHERE ParentAppID = '" & AppID & "' AND ProductType = '" & strProductType & "' AND CompanyID = '" & CompanyID & "' "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAppID = objResult.ToString
        Else
            strAppID = ""
        End If
        objResult = Nothing
        objDataBase = Nothing
        Return strAppID
    End Function

    Private Sub sendXML()
        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath("/net/webservices/xmlfiles/crm/blank.xml"))

        Dim strSQL As String = "", dsCache As DataTable
        Dim strCloneStatusCode As String = "", strCloneSubStatusCode As String = "", strCloneProductType As String = ""

        Select Case intAction
            Case CloneType.Lookup
                strSQL = "SELECT TOP 1 CloneMediaCampaignID, ProductType, StatusCode FROM tblclonecampaigns WHERE MediaCampaignID = '" & intMediaCampaignID & "' AND CompanyID = '" & CompanyID & "'"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        intCloneMediaCampaignID = Row.Item("CloneMediaCampaignID")
                        strCloneProductType = Row.Item("ProductType")
                        strCloneStatusCode = Row.Item("StatusCode").ToString
                    Next
                End If
                dsCache = Nothing
            Case CloneType.Maintain
                intCloneMediaCampaignID = intMediaCampaignID
                strCloneProductType = getAnyField("ProductType", "tblapplications", "AppID", AppID)
                strCloneStatusCode = "NEW"
            Case CloneType.Free
                strCloneProductType = strProductType
                If (Not checkValue(strCloneProductType)) Then strCloneProductType = getAnyField("MediaCampaignProductType", "tblmediacampaigns", "MediaCampaignID", intCloneMediaCampaignID)
                strCloneStatusCode = strStatusCode
                If (Not checkValue(strCloneStatusCode)) Then strCloneStatusCode = "NEW"
        End Select

        If (strMaintainSubStatus = "Y") Then
            strCloneSubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
        ElseIf (checkValue(strSubStatusCode)) Then
            strCloneSubStatusCode = strSubStatusCode
        End If

        writeXML("y", "SkipValidation", "Y")
        writeXML("y", "SkipTelephoneCorrection", "Y")
        writeXML("y", "ProductType", strCloneProductType)
        writeXML("y", "MediaCampaignID", intCloneMediaCampaignID)
        writeXML("n", "MediaCampaignIDOutbound", intMediaCampaignIDOutbound)
        writeXML("n", "StatusCode", strCloneStatusCode)
        If (checkValue(strCloneSubStatusCode)) Then
            writeXML("n", "SubStatusCode", strCloneSubStatusCode)
        End If
		If (checkValue(UserID)) Then
			writeXML("y", "UserID", UserID)
		End If

        strSQL = "SELECT * FROM vwxmlclone WHERE AppID = " & AppID
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                For Each Column As DataColumn In dsCache.Columns
                    If (checkValue(Row(Column).ToString)) And (Column.ColumnName.ToString <> "AppID") And (Column.ColumnName.ToString <> "CompanyID") Then
                        Call writeXML("n", Column.ColumnName.ToString, Row(Column).ToString)
                    End If
                Next
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT StoredDataName, StoredDataValue FROM tbldatastore WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' AND StoredDataName NOT LIKE '%Fee%' AND StoredDataName NOT LIKE '%PPC%' AND StoredDataName NOT LIKE '%Iovation%'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Call writeXML("n", Row.Item("StoredDataName"), Row.Item("StoredDataValue"))
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT TOP 1 Note FROM tblnotes WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NoteActive = 1 AND NoteType = 1"
        Dim strNote As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        writeXML("n", "Notes", strNote)

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        If (strErrorMessage <> "") Then
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Write(0 & "|" & strErrorMessage)
        Else
            ' Post the SOAP message.	

            Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())

            'HttpContext.Current.Response.Write(objReader.ReadToEnd())
            'HttpContext.Current.Response.End()

            objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

            objReader.Close()
            objReader = Nothing

            If (objResponse.StatusCode.ToString = "OK") Then
                Dim objApplication As XmlNode = objOutputXMLDoc.SelectSingleNode("//executeSoapResponse")
                Select Case objApplication.SelectSingleNode("executeSoapStatus").InnerText
                    Case "1"
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(1 & "|" & encodeURL(objApplication.SelectSingleNode("executeSoapNo").InnerText))
                        saveNote(AppID, UserID, "Case cloned to " & strCloneProductType & " - " & objApplication.SelectSingleNode("executeSoapNo").InnerText)
                        saveNote(objApplication.SelectSingleNode("executeSoapNo").InnerText, UserID, "Case cloned from original case " & AppID)
                    Case Else
                        HttpContext.Current.Response.Clear()
                        HttpContext.Current.Response.Write(0 & "|" & objApplication.SelectSingleNode("executeSoapResult").InnerText)
                        postEmail("", "itsupport@engaged-solutions.co.uk", "Application Clone Failure - " & AppID, objApplication.SelectSingleNode("executeSoapResult").InnerText, True, "")
                End Select
            Else
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.Write(0)
            End If

            objResponse = Nothing

        End If

        objInputXMLDoc = Nothing
        objOutputXMLDoc = Nothing

    End Sub

    Private Sub writeXML(ByVal man As String, ByVal fld As String, ByVal val As String)
        If (man = "y") And (Not checkValue(val)) Then
            strErrorMessage += fld & ","
        Else
            If (man = "y") Or ((man = "n") And (val <> "")) Then
                val = regexReplace("<", "&lt;", val)
                val = regexReplace(">", "&gt;", val)
                val = regexReplace(" & ", " and ", val)
                val = regexReplace(" ' ", "&apos;", val)
                val = regexReplace("""", "&quot;", val)

                Dim objTest As XmlNode = objInputXMLDoc.SelectSingleNode("//" & fld)
                If Not (objTest Is Nothing) Then
                    objInputXMLDoc.SelectSingleNode("//" & fld).InnerText = val
                Else
                    Dim objApplication As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
                    Dim objNewNode As XmlElement = objInputXMLDoc.CreateElement(fld)
                    Dim objNewText As XmlText = objInputXMLDoc.CreateTextNode(val)
                    objNewNode.AppendChild(objNewText)
                    objApplication.AppendChild(objNewNode)
                End If
            End If
        End If
    End Sub

End Class