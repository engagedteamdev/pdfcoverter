﻿Imports Config
Imports Common
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class XMLGopher
    Inherits System.Web.UI.Page

    Public Sub generateXml()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<?xml version=""1.0"" encoding=""UTF-8""?>")
            .WriteLine("<Response><Status>Success</Status><CustomerID>123456</CustomerID></Response>")
        End With
        HttpContext.Current.Response.Write(objStringWriter.ToString)
    End Sub

End Class