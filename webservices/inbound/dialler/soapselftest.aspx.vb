﻿Imports Config
Imports Common
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class SOAPSelfTest
    Inherits System.Web.UI.Page

    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strXMLURL As String = Config.ApplicationURL & "/webservices/inbound/dialler/startcall/?MediaID=1086"
    Private strXMLFile As String = "/webservices/xmlfiles/engagedpbx/startcall.xml"

    Public Sub SOAPSelfTest()

        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath(strXMLFile))

        Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

        Response.ContentType = "text/xml"
        Response.Write(objReader.ReadToEnd())

        objReader.Close()
        objReader = Nothing

    End Sub

End Class