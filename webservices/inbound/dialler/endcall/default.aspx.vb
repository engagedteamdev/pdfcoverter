﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

' ** Revision history **
'
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class XMLEngagedPBX
    Inherits System.Web.UI.Page
    Private intSOAPRequestNo As String = getIndexNumber()
    Private MediaID As String = HttpContext.Current.Request("MediaID")
    Private MediaCampaignID As String = "", MediaCampaignIDOutbound As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private CompanyID As String = getAnyField("CompanyID", "tblmedia", "MediaID", MediaID)

    Public Sub receiveXML()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//call")
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = call", -2, intSOAPRequestNo)
        End If
        Call saveData()
    End Sub

    Private Sub saveData()
        Dim strName As String = "", strNumberFrom As String = "", strNumberTo As String = ""
        Dim objName As XmlNode = objInputXMLDoc.SelectSingleNode("//call/callerid/name")
        If (Not objName Is Nothing) Then
            strName = objName.InnerText
        End If
        Dim objNumberFrom As XmlNode = objInputXMLDoc.SelectSingleNode("//call/callerid/num")
        If (Not objNumberFrom Is Nothing) Then
            strNumberFrom = objNumberFrom.InnerText
        End If
        Dim objNumberTo As XmlNode = objInputXMLDoc.SelectSingleNode("//call/dialed/number")
        If (Not objNumberTo Is Nothing) Then
            strNumberTo = objNumberTo.InnerText
        End If
        Dim strSQL As String = "INSERT INTO tbldialercalls (CompanyID, DialerTelephoneName, DialerTelephoneNumberFrom, DialerTelephoneNumberTo) VALUES (" & _
                                    formatField(CompanyID, "N", 0) & ", " & _
                                    formatField(strName, "", "") & ", " & _
                                    formatField(strNumberFrom, "", "") & ", " & _
                                    formatField(strNumberTo, "", "") & ")"
        executeNonQuery(strSQL)
        SOAPSuccessfulMessage("Call data successfully received", 1, intSOAPRequestNo)
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response>")
            .Append("<Message>" & msg & "</Message>")
            .Append("<Status>" & st & "</Status>")
            .Append("<Ref>" & app & "</Ref>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response>")
            .Append("<Message>Invalid XML received: " & msg & "</Message>")
            .Append("<Status>" & st & "</Status>")
            .Append("<Ref>" & app & "</Ref>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
