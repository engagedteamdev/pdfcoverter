﻿Imports Config, Common

Partial Class SMS
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("AppID"), intCompanyID As String = "", strNumber As String = ""
    Private strUserName As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        'sendMail("SMS Delivery Report from " & Request.ServerVariables("REMOTE_ADDR"), encodeURL(Request.ServerVariables("QUERY_STRING")))
        strNumber = "0" & Right(Request("destinationnumber"), 10)
        intCompanyID = getAnyField("CompanyID", "tblapplications", "AppID", AppID)
    End Sub

    Public Sub receiveSMS()
        Dim strNote As String = "SMS delivery status report: " & getStatus(Request("reportcode")) & " to " & Request("destinationnumber") & " on " & Config.DefaultDateTime
        If (checkValue(AppID)) Then
            CommonSave.saveNote(AppID, returnSystemUser(intCompanyID), strNote, intCompanyID)
        End If
		responseWrite(strNote)
    End Sub

    Private Function getStatus(ByVal code As String) As String
        Select Case code
            Case "1"
                Return "Delivered to Handset"
            Case "2"
                Return "Rejected from Handset"
            Case "4"
                Return "Buffered in transit (phone probably off / out of reception)"
            Case "8"
                Return "Accepted by SMSC"
            Case "16"
                Return "Rejected by SMSC"
            Case Else
                Return "Undefined error"
        End Select
    End Function

    Private Sub sendMail(ByVal subject As String, ByVal msg As String)
        strUserName = getAnyFieldByCompanyID("UserFullName", "tblusers", "UserSessionID", UserSessionID)
        getWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx?strMailTo=ben.snaize@engagedcrm.co.uk&strSubject=" & subject & " from " & strUserName & "&strText=" & msg & "&boolBodyHTML=True&boolUseDefault=False&strAttachment=&UserSessionID=" & HttpContext.Current.Request("UserSessionID"))
    End Sub

End Class
