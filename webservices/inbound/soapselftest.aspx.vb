﻿Imports Config
Imports Common
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.engagedsolutions.apitest

Partial Class SOAPSelfTest
    Inherits System.Web.UI.Page

    Private objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    'Private strXMLURL As String = Config.ApplicationURL & "/webservices/inbound/experian/mortgage/soappost.aspx?MediaCampaignID=10426"
    'Private strXMLFile As String = "/net/webservices/xmlfiles/experian/mortgage.xml"
    'Private strXMLURL As String = Config.ApplicationURL & "/webservices/inbound/paa/soappost.aspx?MediaCampaignID=10736"
    'Private strXMLFile As String = "/net/webservices/xmlfiles/paa/TermAssurance.xml"	
    Private strXMLURL As String = "https://beta.engaged-solutions.co.uk/webservices/inbound/soappost.aspx"
    Private strXMLFile As String = "/webservices/xmlfiles/crm/fullapp_completed.xml"
    'Private strXMLFile As String = "/webservices/xmlfiles/crm/app_api.xml"

    Public Sub SOAPSelfTest()

        objInputXMLDoc.Load(HttpContext.Current.Server.MapPath(strXMLFile))

        'Dim objAPI As EngagedAPI = New EngagedAPI
        'Dim strResult As XmlDocument = objAPI.addNewCall("2312240", "123", "Inbound Call", "07921914865", "5c1621b9-ab4e-4283-8fbf-ec1b205a549f")

        Dim objResponse As HttpWebResponse = postWebRequest(strXMLURL, objInputXMLDoc.InnerXml, True)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())

        Response.ContentType = "text/xml"
        'Response.Write(strResult.InnerXml)
        Response.Write(objReader.ReadToEnd())

        'objReader.Close()
        'objReader = Nothing

    End Sub

End Class