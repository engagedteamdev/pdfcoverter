using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using EAGetMail; //add EAGetMail namespace
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using HtmlAgilityPack;
using System.Linq;
using System.Text.RegularExpressions;

public partial class Default : Page
{

    string EmailAppID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Main();

    }

    public void Main()
    {

		string SalesUserID = "";
     

        string mailbox = "c:\\metroinbox";
        


        // Get all *.eml files in specified folder and parse it one by one.
        string[] files = Directory.GetFiles(mailbox, "*.eml");

       

        for (int i = 0; i < files.Length; i++)
        {
            ParseEmail(files[i]);
            
        }
        DeleteFiles();
    }

    public void ParseEmail(string emlFile)
    {
        bool True = true;

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringMetro"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }

        Mail oMail = new Mail("EG-B1374632949-00931-42VE5BBUC5UCBCV6-5FB8D8CFD548A123");
        oMail.Load(emlFile, false);

        // Parse Mail From, Sender

        
            string shtml = StripHTML(oMail.HtmlBody);
        

        if (oMail.Subject.Contains("Regarding") || oMail.Subject.Contains("(REF:"))
            {

                string subject = oMail.Subject.ToString();
                string AppID = subject.Substring(subject.LastIndexOf(':') + 1);
            if (AppID.Contains(")"))
            {
                AppID = AppID.Remove(AppID.Length - 1);
            }

//
         // HttpContext.Current.Response.Write(shtml.Replace("'", ""));
		  // HttpContext.Current.Response.End();

            try
            {
               
                SqlCommand updateemail = new SqlCommand();
                updateemail.Connection = connection;
                updateemail.CommandText = "INSERT INTO tblnotes (AppID,note,createduserid,createddate,noteactive,companyid,notetype) VALUES ('" + AppID + "','" + shtml.Replace("&quot;", "").Replace(":", "").Replace("'", "").Replace("`", "").Replace("&amp;", "and").Replace("&nbsp;"," ").Replace("-", "").Replace("?", "") + "','772',GETDATE(),1,1430,2)";
                updateemail.ExecuteNonQuery();
                

                SqlCommand updateemailnew = new SqlCommand();
                updateemailnew.Connection = connection;
                updateemailnew.CommandText = "INSERT INTO tblemaillogs (EmailSubject, EmailText, EmailSenderAddress, EmailType, InOrOut, AppID, CompanyID) VALUES ('" + oMail.Subject.ToString() + "', '" + shtml.Replace("'", "").Replace("&nbsp;","<br></br>") + "', '" + oMail.From + "', 'Email', 'In', '" + AppID + "',1430)";
                updateemailnew.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {

            }


            try
            {

                string SalesUserID = "";
                string CallCentreUserID = "";
                string AdminUserID = "";
                string CallCentreUserEmail = "";
                string SalesUserEmail = "";
                string AdminUserEmail = "";
				string App1FirstName = "";
				string App1Surname = "";

                connection.Open();
                SqlCommand myCommandUsers = new SqlCommand(string.Format("SELECT dbo.tblapplicationstatus.CallCentreUserID, dbo.tblapplicationstatus.SalesUserID, dbo.tblapplicationstatus.AdministratorUserID, dbo.tblapplications.App1FirstName, dbo.tblapplications.App1Surname FROM dbo.tblapplicationstatus RIGHT OUTER JOIN dbo.tblapplications ON dbo.tblapplicationstatus.AppID = dbo.tblapplications.AppID where dbo.tblapplicationstatus.AppID = {0} ", AppID), connection);

                var userReader = myCommandUsers.ExecuteReader();

                while (userReader.Read())
                {
                    SalesUserID = userReader["SalesUserID"].ToString();
                    CallCentreUserID = userReader["CallCentreUserID"].ToString();
                    AdminUserID = userReader["AdministratorUserID"].ToString();
					App1FirstName = userReader["App1FirstName"].ToString();
					App1Surname = userReader["App1Surname"].ToString();


					SqlCommand myCommandCallcentreemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", CallCentreUserID), connection);

                    var callcentreReader = myCommandCallcentreemail.ExecuteReader();

                    while (callcentreReader.Read())
                    {

                        CallCentreUserEmail = callcentreReader["UserEmailAddress"].ToString();
                        if (Common.checkValue(CallCentreUserEmail))
                        {
                            Common.postEmail("", CallCentreUserEmail, "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
                        }else
                        {
							Common.postEmail("", "jon.lord@metrofinance.co.uk", "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
						}

                    }

                    SqlCommand myCommandsalesemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", SalesUserID), connection);

                    var salesReader = myCommandsalesemail.ExecuteReader();

                    while (salesReader.Read())
                    {

                        SalesUserEmail = salesReader["UserEmailAddress"].ToString();
                        if (Common.checkValue(SalesUserEmail))
                        {
                            Common.postEmail("", SalesUserEmail, "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
                        }
                        else
                        {
							Common.postEmail("", "jon.lord@metrofinance.co.uk", "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
						}

                    }

                    SqlCommand myCommandadminemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", AdminUserID), connection);

                    var adminReader = myCommandadminemail.ExecuteReader();

                    while (adminReader.Read())
                    {

                        AdminUserEmail = adminReader["UserEmailAddress"].ToString();
                        if (Common.checkValue(AdminUserEmail))
                        {
                            Common.postEmail("", AdminUserEmail, "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
                        }
                        else
                        {
							Common.postEmail("", "jon.lord@metrofinance.co.uk", "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);
						}

                    }

                }
                connection.Close();
                Common.postEmail("", "dev.team@engagedcrm.co.uk", "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " Please login for updates.", true, "", true);


            }
            catch (Exception ex)
            {
                Common.postEmail("", "dev.team@engagedcrm.co.uk", "Notification Email Failed to Send " + AppID + "", "" + ex + "", true, "", true);
            }


        }
        else
        {
            //SqlCommand myCommandEmail = new SqlCommand(string.Format("SELECT TOP (1) dbo.tblapplications.AppID FROM dbo.tblapplications RIGHT OUTER JOIN dbo.tblapplicationstatus ON dbo.tblapplications.AppID = dbo.tblapplicationstatus.AppID WHERE (dbo.tblapplicationstatus.Active = 1) AND (dbo.tblapplicationstatus.StatusCode not in ('INV','WTD','TUD','COM')) AND (dbo.tblapplications.App1EmailAddress = '{0}' or dbo.tblapplications.App2EmailAddress = '{0}')", oMail.From.Address), connection);
            ////HttpContext.Current.Response.Write(string.Format("SELECT TOP (1) dbo.tblapplications.AppID FROM dbo.tblapplications RIGHT OUTER JOIN dbo.tblapplicationstatus ON dbo.tblapplications.AppID = dbo.tblapplicationstatus.AppID WHERE (dbo.tblapplicationstatus.Active = 1) AND (dbo.tblapplicationstatus.StatusCode not in ('INV','WTD','TUD','COM')) AND (dbo.tblapplications.App1EmailAddress = '{0}' or dbo.tblapplications.App2EmailAddress = '{0}')", oMail.From.Address));
            //var EmailReader = myCommandEmail.ExecuteReader();

            //while (EmailReader.Read())
            //{
            //    EmailAppID = EmailReader["AppID"].ToString();
            //}

            //if (EmailAppID.Length > 1)
            //{



            //    SqlCommand updateemail = new SqlCommand();
            //    updateemail.Connection = connection;
            //    updateemail.CommandText = string.Format("INSERT INTO tblnotes (AppID,note,createduserid,createddate,noteactive,companyid,notetype) VALUES ('" + EmailAppID.Substring(0, 8) + "','" + shtml + "',GETDATE(),1,1430,2)");
            //    updateemail.ExecuteNonQuery();
            //    connection.Close();

            //    string SalesUserID = Common.getAnyField("SalesUserID", "tblapplicationstatus", "AppID", EmailAppID);
            //    string SalesUserEmail = Common.getAnyField("UserEmailAddress", "tblusers", "UserID", SalesUserID);

            //    string CallCentreUserID = Common.getAnyField("CallCentreUserID", "tblapplicationstatus", "AppID", EmailAppID);
            //    string CallCentreUserEmail = Common.getAnyField("UserEmailAddress", "tblusers", "UserID", CallCentreUserID);

            //    string AdminUserID = Common.getAnyField("CallCentreUserID", "tblapplicationstatus", "AppID", EmailAppID);
            //    string AdminUserEmail = Common.getAnyField("UserEmailAddress", "tblusers", "UserID", AdminUserID);

            //    if (Common.checkValue(SalesUserEmail))
            //    {
            //        Common.postEmail("", SalesUserEmail, "New Email Recieved for AppID " + EmailAppID + "", "New Email Reply Recieved There has been a response on case:" + EmailAppID + " Please login for updates.", true, "", true);
            //    }

            //    if (Common.checkValue(CallCentreUserEmail))
            //    {
            //        Common.postEmail("", CallCentreUserEmail, "New Email Recieved for AppID " + EmailAppID + "", "New Email Reply Recieved There has been a response on case:" + EmailAppID + " Please login for updates.", true, "", true);
            //    }

            //    if (Common.checkValue(AdminUserEmail))
            //    {
            //        Common.postEmail("", AdminUserEmail, "New Email Recieved for AppID " + EmailAppID + "", "New Email Reply Recieved There has been a response on case:" + EmailAppID + " Please login for updates.", true, "", true);
            //    }
            //    Common.postEmail("", "dev.team@engagedcrm.co.uk", "New Email Recieved for AppID " + EmailAppID + "", "New Email Reply Recieved There has been a response on case:" + EmailAppID + " Please login for updates.", true, "", true);

            //}

        }
            
        





    }

    public static string StripHTML(string input)
    {
        return Regex.Replace(input, "\\<[^\\>]*\\>", String.Empty);
    }

    public void DeleteFiles()
    {
        System.IO.DirectoryInfo di = new DirectoryInfo("c:\\metroinbox");

        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();
        }

    }

}


