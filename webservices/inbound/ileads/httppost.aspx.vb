﻿Imports SSL
Imports Config
Imports Common

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

	Public Sub httpPost()
		Dim arrPost As String() = {"redirect","from_site","DOB","leadType","loanRequired","paymentTerm","purchasePrice","self_cert","client1Title","client1Forename","client1Surname","dobDay","dobMonth","dobYear","client1EmailAddress","additionalInfo","Adr1","Town","County","client1Postcode1","client1HomeTelephone","client1WorkTelephone"}
		Dim arrRePost As String() = {"","MediaCampaignName","","ProductType","Amount","","PropertyPurchasePrice","","App1Title","App1Firstname","App1Surname","DOBDD","DOBMM","DOBYYYY","App11EmailAddress","","AddressLine1","AddressLine2","AddressCounty","AddressPostCode","App1HomeTelephone","App1MobileTelephone"}
        Dim strPost As String = "", strDOBDD As String = "", strDOBMM As String = "", strDOBYYYY As String = "", strDOB As String = "", strDebug As String = ""
        Dim intMediaCampaignID As String = "", strProductType As String = ""

		Dim Counter1 As Integer
		Dim Keys() As String
		Dim FormElements As NameValueCollection
		FormElements=HttpContext.Current.Request.Form
		Keys = FormElements.AllKeys
		For Counter1 = 0 To Keys.GetUpperBound(0)
		   strDebug += Keys(Counter1) & "=" & FormElements(Counter1) & VbCrLf
		Next Counter1     

		For i = 0 To UBound(arrPost)
			If checkValue(arrRePost(i)) Then 
				If (arrRePost(i) = "DOBDD") Then
					strDOBDD = padZeros(HttpContext.Current.Request(arrPost(i)),2)
				ElseIf (arrRePost(i) = "DOBMM") Then
					strDOBMM = padZeros(HttpContext.Current.Request(arrPost(i)),2)
				ElseIf (arrRePost(i) = "DOBYYYY") Then
					strDOBYYYY = padZeros(HttpContext.Current.Request(arrPost(i)),4)
				ElseIf (arrRePost(i) = "ProductType") Then
					Select Case HttpContext.Current.Request(arrPost(i))
						Case "1"
							strProductType = "Mortgage - REM"
						Case "2"
							strProductType = "Mortgage - FTB"
						Case "3"
							strProductType = "Mortgage - BTL"
						Case "7"
							strProductType = "Mortgage - HM"
						Case Else
							strProductType = "Mortgage - HM"
					End Select
				ElseIf (arrRePost(i) = "MediaCampaignName") Then
					Select Case True
						Case (InStr(1, HttpContext.Current.Request(arrPost(i)), "www.hellomortgages.co.uk") > 0)
							intMediaCampaignID = "10031"							
						Case (InStr(1, HttpContext.Current.Request(arrPost(i)), "www.oneclickmortgages.co.uk") > 0)
							intMediaCampaignID = "10032"							
					End Select
				Else
					strPost += arrRePost(i) & "=" & HttpContext.Current.Request(arrPost(i)) & "&"
				End If
			End If
		Next
		
		If checkValue(strDOBDD) And checkValue(strDOBMM) And checkValue(strDOBYYYY) Then 
			strPost += "App1DOB=" & strDOBDD & "/" & strDOBMM & "/" & strDOBYYYY & "&"
		End If
		
		If checkValue(intMediaCampaignID) And checkValue(strProductType) Then
			strPost += "MediaCampaignID=" & intMediaCampaignID & "&ProductType=" & strProductType
			postWebRequest(Config.ApplicationURL & "/webservices/inbound/httppost.aspx", strPost)
			Response.Write(strPost)
		Else
			postEmail("ITSupport@lunarmedia.co.uk", "ITSupport@lunarmedia.co.uk", "Invalid Email Submission",strDebug, False, "")
		End If
		
		If checkValue(HttpContext.Current.Request("redirect")) Then
			responseRedirect(HttpContext.Current.Request("redirect"))
		End If

	End Sub

End Class
