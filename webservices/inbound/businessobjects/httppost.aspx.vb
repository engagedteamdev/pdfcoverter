﻿Imports SSL, Config, Common, CommonSave
Imports System.Net
Imports System.Data
Imports System.IO


' ** Revision history **
'
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Private objLeadPlatform As LeadPlatform = New LeadPlatform()
    ' General application
    Private tblbusinessobjects_flds As ArrayList = New ArrayList, tblbusinessobjects_vals As ArrayList = New ArrayList
    ' Application status
    Private tblbusinessobjectstatus_flds As ArrayList = New ArrayList, tblbusinessobjectstatus_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList

    Private intSOAPRequestNo As String = System.Guid.NewGuid().ToString
    Private BusinessObjectID As String = HttpContext.Current.Request("BusinessObjectID")
    Private UserID As String = HttpContext.Current.Request("UserID")
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private BusinessObjectTypeID As String = HttpContext.Current.Request("BusinessObjectType")
    Private ProductTypeID As String = HttpContext.Current.Request("BusinessProductType")
    Private strReturnURL As String = HttpContext.Current.Request("ReturnURL")
	

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
        objLeadPlatform.initialise()
    End Sub

    Public Sub httpPost()
       

        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        If (Not checkValue(BusinessObjectID)) Then
            Call checkAuthority(Request.ServerVariables("REMOTE_ADDR"))
        Else
            Call checkAuthoritySimple(Request.ServerVariables("REMOTE_ADDR"))
            If checkValue(MediaCampaignID) Then
                
                If (getAnyField("BusinessObjectMediaCampaignID", "tblbusinessobjects", "BusinessObjectID", BusinessObjectID) <> CInt(MediaCampaignID) Or Not checkValue(getAnyField("BusinessObjectMediaCampaignID", "tblbusinessobjects", "BusinessObjectID", BusinessObjectID))) Then
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid BusinessObjectID - " & BusinessObjectID)
                    Response.Write("Invalid BusinessObjectID - " & BusinessObjectID)
                    Response.End()
                End If
            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid Media Campaign - " & MediaCampaignID)
                Response.Write("Invalid Media Campaign - " & MediaCampaignID)
                Response.End()
            End If
        End If
  

        Call checkMandatorySimple()
        Call readData()
        Call saveData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.WebServiceID " & _
                                "FROM tblmediacampaigns INNER JOIN " & _
                                "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                                "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                                "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR tblwebservices.WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                "AND (WebServiceActive = 1) AND tblmediacampaigns.CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub

    Private Sub checkAuthoritySimple(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 WebServiceID " & _
                                "FROM tblwebservices " & _
                                "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;' OR WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                " AND (WebServiceActive = 1) AND CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub


    Private Sub checkMandatorySimple()

        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID"}
        For i As Integer = 0 To UBound(arrMandatory)
            If (Not checkValue(Request(arrMandatory(i)))) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            responseWrite(MediaCampaignID)
            responseEnd()
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -3, "Mandatory Data Missing [" & strMandatoryField & "]")
            Response.Write("Mandatory Data Missing [" & strMandatoryField & "]")
            Response.End()
        End If
    End Sub


    Private Sub readData()

        For Each Item In Request.QueryString
            If (checkValue(Item)) Then
                'responseWrite(Item & "=" & Request.QueryString(Item) & "<br>")
                Call validateData(Item, Request.QueryString(Item))
            End If
        Next
        For Each Item In Request.Form
            If (checkValue(Item)) Then
                Call validateData(Item, Request.Form(Item))
            End If
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = "", boolDataGrid As Boolean = False
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                strFunctionParameters = Row.Item("ValFunctionParameters").ToString
                boolDataGrid = Row.Item("ApplicationDataGridColumn")
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        val = Replace(val, vbLf, "<br />")

        ' Call regular expression routine
        ' Call regular expression routine
        If (boolDataGrid) Then
            Dim arrVal As Array = Split(val, ",")
            For x As Integer = 0 To UBound(arrVal)
                If (checkValue(strRegex)) Then
                    If (regexTest(strRegex, arrVal(x))) Then
                        If (checkValue(val) Or checkValue(BusinessObjectID)) Then
                            tbldatastore_flds.Add(strField)
                            tbldatastore_vals.Add(formatField(val, "", ""))
                        End If
                        boolValidate = True
                    Else
                        boolValidate = False
                        Exit For
                    End If
                Else
                    boolValidate = True
                End If
            Next
        Else
            If (regexTest(strRegex, val)) Then
                If (checkValue(val) Or checkValue(BusinessObjectID)) Then
                    If (checkValue(strFunction)) Then
                        'responseWrite(strFunction)
                        'responseEnd()
                        Dim arrParams As String() = {val}
                        If (checkValue(strFunctionParameters)) Then
                            Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                            For y As Integer = 0 To UBound(arrFunctionParameters)
                                If (checkValue(arrFunctionParameters(y))) Then
                                    ReDim Preserve arrParams(y + 1)
                                    arrParams(y + 1) = arrFunctionParameters(y)
                                End If
                            Next
                        End If
                        val = executeMethodByName(Me, strFunction, arrParams)
                    End If
                    Select Case strTable
                        Case "tblbusinessobjects"
                            If (checkValue(BusinessObjectID)) Then
                                If (fld <> "MediaCampaignID") Then
                                    tblbusinessobjects_flds.Add(strField)
                                    tblbusinessobjects_vals.Add(formatField(val, strFieldType, strFieldDefault))
                                End If
                            Else
                                If (fld <> "MediaCampaignID") Then
                                    tblbusinessobjects_flds.Add(strField)
                                    tblbusinessobjects_vals.Add(formatField(val, strFieldType, strFieldDefault))
                                End If
                            End If
                        Case "tblbusinessobjectstatus"
                            tblbusinessobjectstatus_flds.Add(strField)
                            tblbusinessobjectstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblbusinessdatastore"
                            tbldatastore_flds.Add(strField)
                            tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    End Select
                End If
                boolValidate = True
            Else
                boolValidate = False
            End If
        End If

        If (boolValidate = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -5, "Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
            If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
                Else
                    responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
                End If
            Else
                Response.Write("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
                Response.End()
            End If
        End If

    End Sub

    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub saveData()


        Dim strResult As String = saveBusinessObject(BusinessObjectID, UserID, CompanyID, MediaCampaignID, BusinessObjectTypeID, ProductTypeID, _
                                                    tblbusinessobjects_flds, _
                                                    tblbusinessobjects_vals, _
                                                    tblbusinessobjectstatus_flds, _
                                                    tblbusinessobjectstatus_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals _
                                                    )

        Dim arrResult As Array = Split(strResult, "|")

        Dim strLogon As String = "", arrLogon As Array = Nothing

        If (UBound(arrResult) >= 1) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), arrResult(1), 1, "Case successfully received")
            If (BusinessObjectTypeID = 3) Then ' Contact
                strLogon = createContactLogon(arrResult(1), Request("BusinessObjectContactFirstName") & " " & Request("BusinessObjectContactSurname"), HttpContext.Current.Request("BusinessObjectContactEmailAddress"))
                arrLogon = Split(strLogon, "|")
            End If
            If (BusinessObjectTypeID = 5) Then ' Broker
                Dim strBrokerName As String = HttpContext.Current.Request("BusinessObjectBusinessName")				
				
			Dim strEmail As String = HttpContext.Current.Request("BusinessObjectContactEmailAddress")
			Dim body As String = "<style> td{padding:10px; border:1px solid black;}  <div class=""emailheader""><img style=""float:right;"" src=""http://brokerzone.engagedcrm.co.uk/images/positive.jpg""></div><p>Thank you for your registration.</p> <p>Our new business team will be looking into the information you provided and will contact you shortly.</p><p> Once the team are happy with the information you provided you will receive your username and password for the brokerzone.</p> <p>If you have any questions regarding your registration please feel free to call: 0845 260 7511</p> <p>Kind Regards, <br>Positive Leading</p>"
     		'  postEmail("29@engagedcrm.co.uk", strEmail, "Registration Complete", body, True, "", True)
      		'  postEmail("29@engagedcrm.co.uk", strEmail, "Registration Complete", "<style> td{padding:10px; border:1px solid black;}  <div class=""emailheader""><img style=""float:right;"" src=""http://brokerzone.engagedcrm.co.uk/images/positive.jpg""></div><p>Thank you for your registration.</p> <p>Our new business team will be looking into the information you provided and will contact you shortly.</p><p> Once the team are happy with the information you provided you will receive your username and password for the brokerzone.</p> <p>If you have any questions regarding your registration please feel free to call: 0845 260 7511</p> <p>Kind Regards, <br>Positive Leading</p>", True, "", True)
		
                createPrimaryContact(arrResult(1))

                'createBrokerLogon(arrResult(1), strBrokerName, HttpContext.Current.Request("BusinessObjectContactEmailAddress"), objLeadPlatform.Config.CompanyName)

            End If
			If (BusinessObjectTypeID = 11) Then
				Dim strBrokerName As String = HttpContext.Current.Request("BusinessObjectContactFirstName") & " " & HttpContext.Current.Request("BusinessObjectContactSurname")	
				createBrokerLogon(arrResult(1), strBrokerName, HttpContext.Current.Request("BusinessObjectContactEmailAddress"), objLeadPlatform.Config.CompanyName)
				
			End if 
			
        End If
        If (checkValue(strReturnURL)) Then
			
            If (InStr(strReturnURL, "?") > 0) Then
			
				Dim strReturnNo As String = "/prompts/managecontacts.aspx?BusinessObjectID="& HttpContext.Current.Request("BusinessObjectParentClientID")  &""
				
				If (UBound(arrResult) = 1) Then 
					If(strReturnURL = strReturnNo) Then
						 responseRedirect(strReturnURL)
				    Else 
						Response.Write("Error Contact Added Incorrectly")
						Response.End()
					End If
				End If
				
                If (UBound(arrResult) = 1) Then
                    responseRedirect(strReturnURL & "&BusinessObjectID=" & arrResult(1))
                Else
                    responseRedirect(strReturnURL)
                End If
            Else
                If (UBound(arrResult) = 1) Then
                    responseRedirect(strReturnURL & "?BusinessObjectID=" & arrResult(1))
                Else
                    responseRedirect(strReturnURL)
                End If
            End If
        Else
            If (Request("ShowPassword") = "Y") Then
                Response.Write(strResult & "|" & arrLogon(0) & "|" & arrLogon(1))
            Else
                Response.Write(strResult)
            End If
        End If
    End Sub

    Private Sub createContact(ByVal BusinessObjectID As String)
        Dim strPost As String = "MediaCampaignID=" & Request("MediaCampaignID") & "&BusinessObjectParentClientID=" & BusinessObjectID & "&BusinessObjectType=3&BusinessProductType=8&BusinessObjectContactFirstName=" & Request("BusinessObjectContactFirstName") & "&BusinessObjectContactSurname=" & Request("BusinessObjectContactSurname") & "&BusinessObjectContactBusinessTelephone=" & Request("BusinessObjectContactBusinessTelephone") & "&BusinessObjectContactMobileTelephone=" & Request("BusinessObjectContactMobileTelephone") & "&BusinessObjectContactEmailAddress=" & Request("BusinessObjectContactEmailAddress")
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/inbound/businessobjects/httppost.aspx", strPost)
        
		

        'Dim objReader As New StreamReader(objResponse.GetResponseStream())
        'objReader = Nothing
        'objResponse = Nothing
    End Sub

    Private Sub createPrimaryContact(ByVal BusinessObjectID As String)
        Dim strPost As String = "MediaCampaignID=" & Request("MediaCampaignID") & "&BusinessObjectParentClientID=" & BusinessObjectID & "&BusinessObjectType=11&BusinessProductType=8&BusinessObjectContactFirstName=" & Request("BusinessObjectContactFirstName") & "&BusinessObjectContactSurname=" & Request("BusinessObjectContactSurname") & "&BusinessObjectContactBusinessTelephone=" & Request("BusinessObjectContactBusinessTelephone") & "&BusinessObjectContactMobileTelephone=" & Request("BusinessObjectContactMobileTelephone") & "&BusinessObjectContactEmailAddress=" & Request("BusinessObjectContactEmailAddress")

        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/inbound/businessobjects/httppost.aspx", strPost)


		
        'Dim objReader As New StreamReader(objResponse.GetResponseStream())
        'objReader = Nothing
        'objResponse = Nothing
    End Sub 

    Private Sub createBrokerLogon(ByVal BusinessObjectID As String, ByVal brokerName As String, ByVal email As String, ByVal companyName As String)
        Dim strUserName As String = getUserName(brokerName)
        Dim strUserReference As String = getUserReference(brokerName)
        Dim strPassword As String = generatePassword(8)
        Dim strInsert As String = "INSERT INTO tblusers(CompanyID, UserName, UserReference, UserFullName, UserPassword, UserEmailAddress, UserClientID) " & _
                                        "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                        formatField(strUserName, "L", "") & ", " & _
                                        formatField(strUserReference, "U", "") & ", " & _
                                        formatField(brokerName, "T", "") & ", " & _
                                        formatField(hashString256(LCase(strUserName), strPassword), "", "") & ", " & _
                                        formatField(email, "", "") & ", " & _
                                        formatField(BusinessObjectID, "N", 0) & ") "
									
        executeNonQuery(strInsert)
        'postEmail("", email, "Broker Portal Details", "<h2>Broker Portal</h2><p>Welcome to the Broker Portal, your username is: " & strUserName & " and your password is " & strPassword & ".</p><p>Thanks, " & companyName, True, "", True)
    End Sub

    Private Function createContactLogon(ByVal BusinessObjectID As String, ByVal contactName As String, ByVal email As String) As String
        Dim strUserName As String = getUserName(contactName)
        Dim strUserReference As String = getUserReference(contactName)
        Dim strPassword As String = generatePassword(8)
        Dim strInsert As String = "INSERT INTO tblusers(CompanyID, UserName, UserReference, UserFullName, UserPassword, UserEmailAddress, UserClientID) " & _
                                        "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                        formatField(strUserName, "L", "") & ", " & _
                                        formatField(strUserReference, "U", "") & ", " & _
                                        formatField(contactName, "T", "") & ", " & _
                                        formatField(hashString256(LCase(strUserName), strPassword), "", "") & ", " & _
                                        formatField(email, "", "") & ", " & _
                                        formatField(BusinessObjectID, "N", 0) & ") "
        executeNonQuery(strInsert)
        Return strUserName & "|" & strPassword
    End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strFormPost As String = ""
        Dim x As Integer = 0
        For Each Item In Request.Form
            If (checkValue(Item)) Then
                If (x > 0) Then
                    strFormPost += "&" & Item & "=" & Request.Form(Item)
                Else
                    strFormPost += Item & "=" & Request.Form(Item)
                End If
                x += 1
            End If
        Next

        For Each Item In Request.QueryString
            If (checkValue(Item)) Then
                If (checkValue(strFormPost)) Then
                    strFormPost += "&" & Item & "=" & Request.QueryString(Item)
                Else
                    strFormPost += Item & "=" & Request.QueryString(Item)
                End If
            End If
        Next
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "", "") & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(strFormPost, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
