﻿Imports Config, Common, System.Xml, System.Net, System.IO

Partial Class saveCheckList
    Inherits System.Web.UI.Page

    Private intSOAPRequestNo As String = getIndexNumber(), strSOAPMessage As String = "", intNoReceived As Integer = 0, intNoFailures As Integer = 0
    Private MediaID As String = "", CompanyID As String = "0"
	Private frmDateName As String = HttpContext.Current.Request("frmDateName")
	Private frmNoteName As String = HttpContext.Current.Request("frmNoteName")
	Private frmNote as string = HttpContext.Current.Request("frmNote")
	private AppId as string = HttpContext.Current.Request("frmAppID")
    Private objInputXMLDoc As XmlDocument = New XmlDocument
	Public strAccess As String = HttpContext.Current.Request("access")
	Public typeid as string = HttpContext.Current.Request("typeid")
	Public strDeleteAd As String = HttpContext.Current.Request("deletead")
	Public strDeleteCon As String = HttpContext.Current.Request("deletecon")

	
	
	Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    	
		If (strAccess = "Advisor") Then
            completeItemAdvisor(typeid)
			
		ElseIf (strAccess = "Controller") Then
			completeItemQC(typeid)
			
        End If
		
		If(strDeleteAd = "Y")Then
			deleteitemad(typeid)
		End If
		
		If(strDeleteCon = "Y")Then
			deleteitemcon(typeid)
		End If

    End Sub
		
	Public Function completeItemAdvisor(ByVal typeid As String) as string
	
	Dim strComplete as string = "INSERT INTO tblCheckList (AppID, AdvisorUser, AdvisorDate, ItemType) VALUES ('"& AppId &"', '" & Config.DefaultUserId & "', GETDATE(), '" & typeid & "')"
	executeNonQuery(strComplete)
	
	End Function
	
	
	Public Function completeItemQC(ByVal typeid As String) as string
	
	Dim strComplete as string = "UPDATE tblCheckList SET QCUser = '" & Config.DefaultUserId & "', QCDate =GETDATE() WHERE AppID = '"& AppId &"' AND ItemType = '" & typeid & "'"

	executeNonQuery(strComplete)
	
	End Function
	
	Public Function deleteitemad(ByVal typeid As String) as string
	
	Dim strDelete as string = "update tblCheckList set AdvisorUser=null, AdvisorDate=null WHERE AppID = '"& AppId &"' AND ItemType = '" & typeid & "'"

	executeNonQuery(strDelete)
	
	End Function

	Public Function deleteitemcon(ByVal typeid As String) as string

	Dim strDelete as string = "update tblCheckList set QCUser=null, QCDate=null WHERE AppID = '"& AppId &"' AND ItemType = '" & typeid & "'"

	executeNonQuery(strDelete)
	
	End Function
	
	
End Class
