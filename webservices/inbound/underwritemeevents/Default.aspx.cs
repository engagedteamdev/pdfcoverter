using System;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;
using System.Linq;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Activities.Expressions;
using System.Data;


public partial class Default : Page
{



	class theAuth
	{

		public string username { get; set; }
		public string password { get; set; }

	}



	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			Run();
		}
		catch (Exception t)
		{
			throw t;
		}
	}



	public void Run()
	{



		String data = new System.IO.StreamReader(Context.Request.InputStream).ReadToEnd();
		var json = JObject.Parse(data);
		var id = json["context"]["id"].ToString();


		theAuth datatoken = new theAuth();
		datatoken.username = "CoreCoverAPI";
		datatoken.password = "Tuesday16";

		string jsontoken = JsonConvert.SerializeObject(datatoken);
		string PostURL = "https://admin-compare-int-01.underwriteme.co.uk/api/auth";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(PostURL);


		HttpContent contentBody = new StringContent(jsontoken);
		contentBody.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

		var res = client.PostAsync(PostURL, contentBody).Result;

		var responseContent = res.Content.ReadAsStringAsync().Result;

		var resptoken = JObject.Parse(responseContent);

		var token = resptoken["access_token"].ToString();


		if (token != null)
		{

			retrieveBasket(token, id);

		}
		else
		{
			Response.Write("Token Unavailable");
		}
	}

	public void retrieveBasket(string token, string id)
	{

		


		SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringLifeCover"].ToString());

		try
		{
			connection.Open();
		}
		catch (Exception e)
		{
			throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
		}




		string GetURL = "https://admin-compare-int-01.underwriteme.co.uk/api/v2/application/" + id + "/basket/snapshot";
		HttpClient client = new HttpClient();
		client.BaseAddress = new Uri(GetURL);
		client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

		var res = client.GetAsync(GetURL).Result;

		var responseContent = res.Content.ReadAsStringAsync().Result;

		var respbasket = JObject.Parse(responseContent);

		Common.postEmail("", "william.worthington@engagedcrm.co.uk", "Underwrite Me", "" + respbasket + "", true, "", true);


		string itemsid = ""; 
		try
        {
			itemsid = respbasket["id"].ToString();
		}
        catch
        {
			itemsid = "";
			

		}


		if(itemsid != "") { 

		SqlCommand myCommand = new SqlCommand(string.Format("Select ID, AppID FROM tblpolicy where UnderwriteMeID = '{0}'", id), connection);

		var reader = myCommand.ExecuteReader();

		string PolicyID = "", AppID = "";

		while (reader.Read())
		{
			PolicyID = reader["ID"].ToString();
			AppID = reader["AppID"].ToString();
		}

			string CoverPeriod = "", LifeCoverCoverAmount = "", CiCCoverAmount = "", LifeOrCiCCoverAmount = "", DisplayName = "", PremiumBasis = "", CoverBasis = "", Premium= "", initialcomms = "", Provider = "";
			int ProviderID = 0; 
            try
            {
				CoverPeriod = respbasket["items"][0]["product"]["attributes"]["coverPeriod"].ToString();
			}
            catch
            {
				CoverPeriod = "";
				
			}

			try
			{

				DisplayName = respbasket["items"][0]["product"]["displayName"].ToString();


				if (DisplayName == "Life Cover")
				{
					LifeCoverCoverAmount = respbasket["items"][0]["product"]["attributes"]["coverAmount"].ToString();
				}else if (DisplayName == "Standalone Critical Illness")
                {
					CiCCoverAmount = respbasket["items"][0]["product"]["attributes"]["coverAmount"].ToString();
				}else if (DisplayName == "Life & Critical Illness")
                {
					LifeOrCiCCoverAmount = respbasket["items"][0]["product"]["attributes"]["coverAmount"].ToString();

				}

				
			}
			catch
			{
				
			}

			try
			{
				PremiumBasis = respbasket["items"][0]["product"]["attributes"]["premiumBasis"].ToString();

				if(PremiumBasis == "GUARANTEED")
                {
					PremiumBasis = "Guaranteed";

                }
                else
                {
					PremiumBasis = "Reviewable"; 

				}
			}
			catch
			{
				PremiumBasis = "";
				

			}

			try
			{
				CoverBasis = respbasket["items"][0]["product"]["attributes"]["coverBasis"].ToString();

				if (CoverBasis == "LEVEL")
				{
					CoverBasis = "Level";

				}
				else if (CoverBasis == "DECREASING")
				{
					CoverBasis = "Decreasing";

				}
				else if (CoverBasis == "INCREASING")
                {
					CoverBasis = "Level";
				}
			}
			catch
			{
				CoverBasis = "";
				
			}

			try
			{
				Premium = respbasket["items"][0]["premium"]["to"].ToString();
			}
			catch
			{
				Premium = "";
				
			}

			try
			{
				initialcomms = respbasket["items"][0]["commission"]["initial"].ToString();
			}
			catch
			{
				initialcomms = "";
				

			}


			try
			{
				Provider = respbasket["items"][0]["provider"]["name"].ToString();

				switch (Provider)
				{
					case "Aegon":
						ProviderID = 5;
						break;
					case "AigLife":
						ProviderID = 11;
						break;
					case "BeagleStreet":
						ProviderID = 22;
						break;
					case "BudgetInsurance":
						ProviderID = 23;
						break;
					case "CanadaLife":
						ProviderID = 12;
						break;
					case "CavendishLife":
						ProviderID = 24;
						break;
					case "Churchill":
						ProviderID = 25;
						break;
					case "EffIncomeOne":
						ProviderID = 26;
						break;
					case "EffPureProtection":
						ProviderID = 27;
						break;
					case "HSBCLife":
						ProviderID = 19;
						break;
					case "LiverpoolVictoria":
						ProviderID = 13;
						break;
					case "OldMutual":
						ProviderID = 4;
						break;
					case "PostOffice":
						ProviderID = 28;
						break;
					case "RL":
						ProviderID = 3;
						break;
					case "Reviti":
						ProviderID = 29;
						break;
					case "ScottishWidows":
						ProviderID = 14;
						break;
					case "VirginMoney":
						ProviderID = 30;
						break;
					case "Zurich":
						ProviderID = 17;
						break;
					default:
						ProviderID = 0;
						break;
				}
			}
			catch
			{
				Provider = "";
				Common.postEmail("", "william.worthington@engagedcrm.co.uk", "Underwrite Me Error", "Provider", true, "", true);

			}


			if(CoverBasis != "") { 

			SqlCommand updateID = new SqlCommand();
			updateID.Connection = connection;
			updateID.CommandText = string.Format("insert into tblbenefits (PolicyID, AppID, BenefitActive, term, BenefitAmount, CICAmount,LifeOrCICAmount,PremiumType,CoverType,Premium,GrossCommission,BenefitProvider) values ('" + PolicyID + "','" + AppID + "','1','" + CoverPeriod + "','" + LifeCoverCoverAmount + "','" + CiCCoverAmount + "','" + LifeOrCiCCoverAmount + "','" + PremiumBasis + "','" + CoverBasis + "','" + Premium + "','" + initialcomms + "','" + ProviderID + "')");
			updateID.ExecuteNonQuery();

			}

            string kfidocname = "", kfiurl = "", tandcdocname = "", tandcurl = "", compdocname = "",compurl = "", quotedocname = "", quotedocurl = "";

            try
            {
                kfidocname = respbasket["items"][0]["documents"][0]["name"].ToString();
            }
            catch
            {
                
            }

            try
            {
                kfiurl = respbasket["items"][0]["documents"][0]["url"].ToString();
            }
            catch
            {
               
            }

			try
			{
				tandcdocname = respbasket["items"][0]["documents"][1]["name"].ToString();
			}
			catch
			{
				
			}

			try
			{
				tandcurl = respbasket["items"][0]["documents"][1]["url"].ToString();
			}
			catch
			{
				
			}

			try
			{
				compdocname = respbasket["documents"][0]["name"].ToString();
			}
			catch
			{
				
			}

			try
			{
				compurl = respbasket["documents"][0]["url"].ToString();
			}
			catch
			{
				
			}

			try
			{
				quotedocname = respbasket["documents"][1]["name"].ToString();
			}
			catch
			{
				
			}

			try
			{
				quotedocurl = respbasket["documents"][1]["url"].ToString();
			}
			catch
			{
				
			}

			if (kfidocname != "") { 
				SqlCommand savedocKFI = new SqlCommand();
				savedocKFI.Connection = connection;
				savedocKFI.CommandText = string.Format("insert into tblpdfhistory (AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryCreatedUserID, CompanyID, PolicyID) values ('" + AppID + "','" + kfiurl + "','" + Provider + " " + kfidocname + "','1470','1181','" + PolicyID + "')");
				savedocKFI.ExecuteNonQuery();

			}

			if (tandcdocname != "")
			{
				SqlCommand savedocKFI = new SqlCommand();
				savedocKFI.Connection = connection;
				savedocKFI.CommandText = string.Format("insert into tblpdfhistory (AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryCreatedUserID, CompanyID, PolicyID) values ('" + AppID + "','" + tandcurl + "','" + Provider + " " + tandcdocname + "','1470','1181','" + PolicyID + "')");
				savedocKFI.ExecuteNonQuery();

			}

			if (compdocname != "")
			{
				SqlCommand savedocKFI = new SqlCommand();
				savedocKFI.Connection = connection;
				savedocKFI.CommandText = string.Format("insert into tblpdfhistory (AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryCreatedUserID, CompanyID, PolicyID) values ('" + AppID + "','" + compurl + "','" + Provider + " " + compdocname + "','1470','1181','" + PolicyID + "')");
				savedocKFI.ExecuteNonQuery();

			}

			if (quotedocname != "")
			{
				SqlCommand savedocKFI = new SqlCommand();
				savedocKFI.Connection = connection;
				savedocKFI.CommandText = string.Format("insert into tblpdfhistory (AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryCreatedUserID, CompanyID, PolicyID) values ('" + AppID + "','" + quotedocurl + "','" + Provider + " " + quotedocname + "','1470','1181','" + PolicyID + "')");
				savedocKFI.ExecuteNonQuery();

			}





		}

	

	}

}