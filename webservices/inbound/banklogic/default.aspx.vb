﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

' ** Revision history **
'
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class XMLBankLogic
    Inherits System.Web.UI.Page
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = ""
    Private MediaCampaignID As String = "", MediaCampaignIDOutbound As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private CompanyID As String = "0"

    Public Sub receiveXML()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//Message")
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = Message", -2, intSOAPRequestNo)
        End If
        Dim objReference As XmlNode = objInputXMLDoc.SelectSingleNode("//Borrower[1]/Reference")
        If (Not objReference Is Nothing) Then
            AppID = objReference.InnerText
            CompanyID = getAnyField("CompanyID", "tblapplications", "AppID", AppID)
        Else
            Call SOAPUnsuccessfulMessage("Data field not found. Field = Borrower/Reference", -2, intSOAPRequestNo)
        End If
        Call saveData()
    End Sub

    Private Sub saveData()
        Dim objReports As XmlNodeList = objInputXMLDoc.DocumentElement.SelectNodes("//Reports/Report")
        If (objReports.Count > 0) Then
            executeNonQuery("DELETE FROM tblbankaccounts WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            executeNonQuery("DELETE FROM tblbanktransactions WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            Dim intBankAccountID As Integer = 0
            For Each report As XmlNode In objReports
                Dim strSQL As String = "INSERT INTO tblbankaccounts (AppID, CompanyID, BankAccountBankName, BankAccountNumber, BankAccountType, BankAccountReportDate, BankAccountHistoryFrom, BankAccountHistoryTo, BankAccountAvailableBalance, BankAccountCurrentBalance, BankAccountDepositsCredits, BankAccountWithdrawalsDebits, BankAccountAverageBalance, BankAccountAverageBalanceLastMonth) VALUES (" & _
                                            formatField(AppID, "N", 0) & ", " & _
                                            formatField(CompanyID, "N", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Response/BankName").InnerText, "T", "") & ", " & _
                                            formatField(report.SelectSingleNode("Response/AccountNumber").InnerText, "", "") & ", " & _
                                            formatField(report.SelectSingleNode("Response/AccountType").InnerText, "T", "") & ", " & _
                                            formatField(CDate(report.SelectSingleNode("Summary/ReportDate").InnerText).ToLongDateString, "DTTM", Config.DefaultDateTime) & ", " & _
                                            formatField(CDate(report.SelectSingleNode("Summary/TransactionHistoryDateRange/From").InnerText).ToLongDateString, "DTTM", Config.DefaultDateTime) & ", " & _
                                            formatField(CDate(report.SelectSingleNode("Summary/TransactionHistoryDateRange/To").InnerText).ToLongDateString, "DTTM", Config.DefaultDateTime) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/AvailableBalance").InnerText, "M", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/CurrentBalance").InnerText, "M", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/DepositsCredits").InnerText, "M", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/WithdrawalsDebits").InnerText, "M", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/AverageBalance").InnerText, "M", 0) & ", " & _
                                            formatField(report.SelectSingleNode("Summary/AverageBalanceLastMonth").InnerText, "M", 0) & ")"
                intBankAccountID = executeIdentityQuery(strSQL)
                Dim objTransactions As XmlNodeList = report.SelectNodes("Transactions/Transaction")
                If (objTransactions.Count > 0) Then
                    For Each transaction As XmlNode In objTransactions
                        strSQL = "INSERT INTO tblbanktransactions (AppID, CompanyID, BankAccountID, BankTransactionCodes, BankTransactionDescription, BankTransactionAmount, BankTransactionBalance, BankTransactionDate) VALUES (" & _
                                                    formatField(AppID, "N", 0) & ", " & _
                                                    formatField(CompanyID, "N", 0) & ", " & _
                                                    formatField(intBankAccountID, "N", 0) & ", " & _
                                                    formatField(transaction.SelectSingleNode("Codes").InnerText, "U", "") & ", " & _
                                                    formatField(transaction.SelectSingleNode("Description").InnerText, "T", "") & ", " & _
                                                    formatField(transaction.SelectSingleNode("Amount").InnerText, "M", 0) & ", " & _
                                                    formatField(transaction.SelectSingleNode("Balance").InnerText, "M", 0) & ", " & _
                                                    formatField(CDate(transaction.SelectSingleNode("Date").InnerText).ToLongDateString, "DTTM", Config.DefaultDateTime) & ")"
                        executeNonQuery(strSQL)
                    Next
                End If
            Next
        End If
        postEmail("", "ben.snaize@engaged-solutions.co.uk", "BankLogic Report Successful", objInputXMLDoc.InnerXml, True, "")
        SOAPSuccessfulMessage("Report successfully received", 1, AppID)
    End Sub

    Private Sub saveSummaryField(ByVal xPath As String, ByVal name As String, ByVal format As String, ByVal dft As String)
        Dim objElement As XmlElement = objInputXMLDoc.SelectSingleNode(xPath)
        If (Not objElement Is Nothing) Then
            If (format = "DTTM") Then
                updateDataStoreField(AppID, name, FormatDateTime(CDate(objElement.InnerText), DateFormat.ShortDate), "", dft)
            Else
                updateDataStoreField(AppID, name, objElement.InnerText, format, dft)
            End If
        End If
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response>")
            .Append("<Message>" & msg & "</Message>")
            .Append("<Status>" & st & "</Status>")
            .Append("<Ref>" & app & "</Ref>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response>")
            .Append("<Message>Invalid XML received: " & msg & "</Message>")
            .Append("<Status>" & st & "</Status>")
            .Append("<Ref>" & app & "</Ref>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
