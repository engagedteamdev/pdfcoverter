﻿Imports Config, Common, CommonSave
Imports System.Reflection

' ** Revision history **
'
' 29/02/2012    - Major revision
'               - Check on incoming email address, subject and body for special tags
'               - Create an application
'               - Read in additional form fields
' ** End Revision History **

Partial Class StatusUpdate
    Inherits System.Web.UI.Page

    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    Private tbladdresses_flds As ArrayList = New ArrayList, tbladdresses_vals As ArrayList = New ArrayList
    Private tblemployers_flds As ArrayList = New ArrayList, tblemployers_vals As ArrayList = New ArrayList
	Private tblunderwriting_flds As ArrayList = New ArrayList, tblunderwriting_vals As ArrayList = New ArrayList
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList
    Private tblreminders_flds As ArrayList = New ArrayList, tblreminders_vals As ArrayList = New ArrayList
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    Private tbldatastoregrid_gridnames As ArrayList = New ArrayList, tbldatastoregrid_names As ArrayList = New ArrayList, tbldatastoregrid_vals As ArrayList = New ArrayList
    Private tblapplicationstatusdates_flds As ArrayList = New ArrayList, tblapplicationstatusdates_vals As ArrayList = New ArrayList
    Private tbldiaries_flds As ArrayList = New ArrayList, tbldiaries_vals As ArrayList = New ArrayList
    Private tbldatastoreincrement_fld As ArrayList = New ArrayList, tbldatastoreincrement_vals As ArrayList = New ArrayList

    Private UserID As String = ""
    Private MediaCampaignID As String = ""
    Private strReturnURL As String = HttpContext.Current.Request("ReturnURL")
    Private intSkipTelephoneCorrection As String = HttpContext.Current.Request("SkipTelephoneCorrection")
    Private intSkipLiveTransfer As String = HttpContext.Current.Request("SkipLiveTransfer")

    Private strEmailFromAddress As String = HttpContext.Current.Request("frmEmailFromAddress"), strEmailToAddress As String = HttpContext.Current.Request("frmEmailToAddress"), strEmailSubject As String = HttpContext.Current.Request("frmSubject"), strEmailBody As String = HttpContext.Current.Request("frmEmailBody"), strAttachments As String = HttpContext.Current.Request("frmAttachments")
    Private CompanyID As String = "", AppID As String = "", strNote As String = "", strRoutineID As String = "", strAppUpdate As String = ""

    Public Sub statusUpdate()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            'Response.End()
        End If
        strEmailToAddress = regexFirstMatch("<([a-z0-9@.-_]{0,100})>", strEmailToAddress)

        Dim strSQL As String = "SELECT EmailParserID, CompanyID, EmailParserFromEmailAddressCondition, EmailParserSubjectCondition, EmailParserBodyCondition, EmailParserAppIDRegEx, EmailParserRoutineID, EmailParserNewApplication, EmailParserNewApplicationMediaCampaignID FROM tblemailparser WHERE EmailParserToEmailAddressCondition LIKE '%" & strEmailToAddress & "%' AND EmailParserActive = 1"
        'responseWrite(strSQL & "<br />")
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (regexTest(Row.Item("EmailParserSubjectCondition"), strEmailSubject) Or Not checkValue(Row.Item("EmailParserSubjectCondition"))) _
                    And (regexTest(Row.Item("EmailParserBodyCondition"), strEmailBody) Or Not checkValue(Row.Item("EmailParserBodyCondition"))) _
                    And (regexTest(Row.Item("EmailParserFromEmailAddressCondition"), strEmailFromAddress) Or Not checkValue(Row.Item("EmailParserFromEmailAddressCondition"))) Then

                    CompanyID = Row.Item("CompanyID")
                    If (Row.Item("EmailParserNewApplication") = True) Then
                        MediaCampaignID = Row.Item("EmailParserNewApplicationMediaCampaignID")
                    Else
                        AppID = regexFirstMatch(Row.Item("EmailParserAppIDRegEx"), strEmailSubject)
                        If Not checkValue(AppID) Then ' Check body if not found in subject
                            AppID = regexFirstMatch(Row.Item("EmailParserAppIDRegEx"), strEmailBody)
                        End If
                        If checkValue(AppID) Then
                            MediaCampaignID = getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID)
                        End If
                    End If

                    If (Row.Item("EmailParserNewApplication") = True) Or checkValue(AppID) Then

                        strSQL = "SELECT EmailParserFieldType, EmailParserFieldName, EmailParserFieldRegex, EmailParserFieldDefault, EmailParserFieldDataGridName FROM tblemailparserfields " & _
                            "INNER JOIN tblemailparsermappings ON tblemailparserfields.EmailParserFieldID = tblemailparsermappings.EmailParserFieldID " & _
                            "WHERE tblemailparsermappings.EmailParserID = " & Row.Item("EmailParserID") & " AND tblemailparsermappings.CompanyID = '" & CompanyID & "' AND (EmailParserFieldActive = 1)"
                        'responseWrite(strSQL)
                        Dim dsCache2 As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            Dim strField As String = "", strMatch As String = ""
                            For Each Row2 In dsCache2.Rows
                                If checkValue(Row2.item("EmailParserFieldRegex").ToString) Then
                                    Select Case Row2.Item("EmailParserFieldType").ToString
                                        Case "Address"
                                            strField = strEmailFromAddress
                                        Case "Subject"
                                            strField = strEmailSubject
                                        Case "Body"
                                            strField = strEmailBody
                                        Case Else
                                            strField = strEmailBody
                                    End Select
                                    strMatch = regexFirstMatch(Row2.Item("EmailParserFieldRegex").ToString, strField)
                                End If
                                If Not checkValue(strMatch) And Not checkValue(Row2.item("EmailParserFieldRegex").ToString) Then
                                    If checkValue(Row2.Item("EmailParserFieldDataGridName").ToString) Then
                                        tbldatastoregrid_gridnames.Add(Row2.Item("EmailParserFieldDataGridName").ToString)
                                        tbldatastoregrid_names.Add(Row2.Item("EmailParserFieldName").ToString)
                                        tbldatastoregrid_vals.Add(Row2.Item("EmailParserFieldDefault").ToString)
                                    Else
                                        If checkValue(Row2.item("EmailParserFieldDefault").ToString) Then
                                            If Left(Row2.item("EmailParserFieldDefault").ToString, 1) = "+" Then
                                                tbldatastoreincrement_fld.Add(Row2.Item("EmailParserFieldName").ToString)
                                                tbldatastoreincrement_vals.Add(Replace(Row2.item("EmailParserFieldDefault").ToString, "+", ""))
                                            Else
                                                strMatch = Row2.item("EmailParserFieldDefault").ToString
                                            End If
                                        End If
                                    End If
                                End If
                                If checkValue(strMatch) Then
                                    Call validateData(Row2.Item("EmailParserFieldName").ToString, strMatch)
                                End If
                                strMatch = ""
                            Next
                            dsCache2 = Nothing
                            If checkValue(AppID) Then
                                saveData()
                            Else
                                AppID = saveData()
                            End If
                        End If

                        If (tbldatastoregrid_gridnames.Count > 0) And checkValue(AppID) Then
                            For i = 0 To tbldatastoregrid_gridnames.Count - 1
                                appendDataStoreGrid(AppID, tbldatastoregrid_gridnames(i), tbldatastoregrid_names(i), tbldatastoregrid_vals(i))
                            Next
                        End If

                        If (tbldatastoreincrement_fld.Count > 0) And checkValue(AppID) Then
                            For i = 0 To tbldatastoreincrement_fld.Count - 1
                                incrementDataStoreField(AppID, tbldatastoreincrement_fld(i), tbldatastoreincrement_vals(i))
                            Next
                        End If

                        strRoutineID = Row.Item("EmailParserRoutineID").ToString

                    End If
                End If
            Next
        End If
        dsCache = Nothing

        'If Not checkValue(CompanyID) Or Not checkValue(AppID) Then
        '    postEmail("", "neb.stevenson@gmail.com", "Email Parse Error", "To: " & strEmailToAddress & "<br />Subject: " & strEmailSubject & "<br />Body: " & strEmailBody & "<br />AppID:" & AppID & "<br />CompanyID: " & CompanyID, True, "")
        'End If

        'responseWrite(strEmailBody & "<br /><br />")
        'responseWrite("CompanyID = " & CompanyID & "<br />")
        'responseWrite("AppID = " & AppID & "<br />")

        If checkValue(CompanyID) And checkValue(AppID) Then

            If checkValue(strRoutineID) Then ' Lookup the routine assigned to the parser and run it.
                strSQL = "SELECT AdditionalFieldButtonStatusCode, AdditionalFieldButtonSubStatusCode, AdditionalFieldButtonDateType, AdditionalFieldButtonClearDateType, AdditionalFieldButtonNote, AdditionalFieldButtonCreateReminders, AdditionalFieldButtonCreateRemindersUserID, AdditionalFieldRoutine, AdditionalFieldRoutineParameters FROM tblapplicationpageadditionalfields " & _
                "WHERE AdditionalFieldID = '" & strRoutineID & "' AND CompanyID = '" & CompanyID & "' AND AdditionalFieldActive = 1"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()

                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If (checkValue(Row.Item("AdditionalFieldButtonStatusCode").ToString)) Then
                            saveStatus(AppID, "", Row.Item("AdditionalFieldButtonStatusCode").ToString, Row.Item("AdditionalFieldButtonSubStatusCode").ToString, "", "", "", CompanyID)
                        End If
                        If (checkValue(Row.Item("AdditionalFieldButtonDateType").ToString)) Then
                            saveUpdatedDate(AppID, "", Row.Item("AdditionalFieldButtonDateType").ToString, "", CompanyID)
                        End If
                        If (checkValue(Row.Item("AdditionalFieldButtonClearDateType").ToString)) Then
                            clearUpdatedDate(AppID, Row.Item("AdditionalFieldButtonClearDateType").ToString, CompanyID)
                        End If
                        If (checkValue(Row.Item("AdditionalFieldButtonNote").ToString)) Then
                            saveNote(AppID, "", Row.Item("AdditionalFieldButtonNote").ToString, CompanyID)
                        End If
                        If (checkValue(Row.Item("AdditionalFieldButtonCreateReminders").ToString)) Then
                            createReminders(AppID, Row.Item("AdditionalFieldButtonCreateReminders").ToString, Row.Item("AdditionalFieldButtonCreateRemindersUserID").ToString)
                        End If

                        If (checkValue(Row.Item("AdditionalFieldRoutine").ToString)) Then
                            Dim arrParams As String() = {Nothing, AppID}
                            If (checkValue(Row.Item("AdditionalFieldRoutineParameters").ToString)) Then
                                Dim arrFrmActionParameters As String() = Nothing
                                arrFrmActionParameters = Split(Row.Item("AdditionalFieldRoutineParameters").ToString, ",")
                                For y As Integer = 0 To UBound(arrFrmActionParameters)
                                    ReDim Preserve arrParams(2 + y)
                                    arrParams(2 + y) = arrFrmActionParameters(y)
                                Next
                            End If
                            executeSub(Me, Row.Item("AdditionalFieldRoutine"), arrParams)
                        End If

                    Next
                End If
                dsCache = Nothing
            End If

            If (checkValue(strAttachments)) Then
                Dim arrAttachment As Array = Split(strAttachments, ",")
                For x As Integer = 0 To UBound(arrAttachment)
                    If (checkValue(arrAttachment(x))) Then
                        Dim strQry As String = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryName, PDFHistoryCreatedUserID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(AppID, "N", 0) & ", " & _
                            formatField(arrAttachment(x), "", "") & ", " & _
                            formatField("Email Attachment - " & arrAttachment(x), "T", "") & ", " & _
                            formatField("", "N", getSystemUser()) & ")"

                        executeNonQuery(strQry)
                        saveNote(AppID, "", arrAttachment(x) & " was uploaded for this case.")
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", boolDataGrid As Boolean = False, strDataGridName As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                boolDataGrid = Row.Item("ApplicationDataGridColumn")
                strDataGridName = Row.Item("ApplicationDataGridTableClass").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        val = Replace(val, vbLf, "<br />")

        ' Call regular expression routine
        If (boolDataGrid) Then
            ' don't allow datagrid items to be added via this process
        Else
            If (regexTest(strRegex, val)) Then
                If (checkValue(val) Or checkValue(AppID)) Then
                    If (checkValue(strFunction)) Then
                        Dim arrParams As String() = {val}
                        val = executeMethodByName(Me, strFunction, arrParams)
                    End If
                    Select Case strTable
                        Case "tblapplications"
                            If (checkValue(AppID)) Then
                                If (fld <> "MediaCampaignID" And strField <> "LoanType") Then
                                    tblapplications_flds.Add(strField)
                                    tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                                End If
                            Else
                                tblapplications_flds.Add(strField)
                                tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                            End If
                        Case "tblapplicationstatus"
                            tblapplicationstatus_flds.Add(strField)
                            tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbladdresses"
                            tbladdresses_flds.Add(strField)
                            tbladdresses_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblemployers"
                            tblemployers_flds.Add(strField)
                            tblemployers_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblnotes"
                            tblnotes_flds.Add(strField)
                            tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblreminders"
                            tblreminders_flds.Add(strField)
                            tblreminders_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbldatastore"
                            tbldatastore_flds.Add(strField)
                            tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblapplicationstatusdates"
                            tblapplicationstatusdates_flds.Add(strField)
                            tblapplicationstatusdates_vals.Add(val)
                        Case "tbldiaries"
                            tbldiaries_flds.Add(strField)
                            tbldiaries_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    End Select
                End If
            End If
        End If
    End Sub

    Private Function saveData() As String

        Dim strResult As String = saveApplication(False, AppID, UserID, CompanyID, MediaCampaignID, "", 1, _
                                                    tblapplications_flds, _
                                                    tblapplications_vals, _
                                                    tblapplicationstatus_flds, _
                                                    tblapplicationstatus_vals, _
                                                    tbladdresses_flds, _
                                                    tbladdresses_vals, _
                                                    tblemployers_flds, _
                                                    tblemployers_vals, _
                                                    tblunderwriting_flds, _
                                                    tblunderwriting_vals, _
                                                    tblnotes_flds, _
                                                    tblnotes_vals, _
                                                    tblreminders_flds, _
                                                    tblreminders_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals, _
                                                    tblapplicationstatusdates_flds, _
                                                    tblapplicationstatusdates_vals, _
                                                    tbldiaries_flds, _
                                                    tbldiaries_vals)



        Dim arrResult As Array = Split(strResult, "|")
        If (UBound(arrResult) = 1) Then
            Return arrResult(1)
        Else
            Return ""
        End If

    End Function

    Private Sub appendDataStoreGrid(ByVal AppID As String, ByVal strGridName As String, ByVal strNewGridNames As String, ByVal strNewGridValues As String)
        Dim intRowsAffected As Integer = 0
        If checkValue(strGridName) Then
            correctDataStoreGrid(AppID, strGridName)
        End If
        If checkValue(strNewGridNames) Then
            Dim arrNewGridNames = Split(strNewGridNames, ",")
            Dim arrNewGridValues = Split(strNewGridValues, ",")
            For x As Integer = 0 To UBound(arrNewGridNames)
                intRowsAffected = executeRowsAffectedQuery("UPDATE tbldatastore SET StoredDataValue = CASE WHEN StoredDataValue = '' THEN '" & arrNewGridValues(x) & "' ELSE StoredDataValue + '," & arrNewGridValues(x) & "' END WHERE StoredDataName = '" & arrNewGridNames(x) & "' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'")
                If (intRowsAffected = 0) Then
                    Call executeNonQuery("INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & arrNewGridNames(x) & "', '" & arrNewGridValues(x) & "')")
                End If
                intRowsAffected = 0
            Next
        End If
    End Sub

    Private Sub incrementDataStoreField(ByVal AppID As String, ByVal fld As String, ByVal incr As String)
        Dim intRowsAffected As Integer = 0
        If checkValue(fld) And checkValue(incr) Then
            intRowsAffected = executeRowsAffectedQuery("UPDATE tbldatastore SET StoredDataValue = CONVERT(FLOAT,StoredDataValue) + CONVERT(FLOAT," & incr & ") WHERE StoredDataName = '" & fld & "' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'")
            If (intRowsAffected = 0) Then
                Call executeNonQuery("INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & fld & "', '" & incr & "')")
            End If
        End If
    End Sub

    Shared Sub executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array)
        Dim objMethodType As Type = GetType(StatusUpdate)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        objMethodInfo.Invoke(inst, params)
    End Sub

End Class
