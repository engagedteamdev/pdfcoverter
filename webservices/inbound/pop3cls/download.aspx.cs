using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using EAGetMail; //add EAGetMail namespace
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using HtmlAgilityPack;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Xml.XPath;

public partial class Default : Page
{

    string EmailAppID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Main();

    }

    public void Main()
    {
        
       
     

        string mailbox = "c:\\clsinbox";
        


        // Get all *.eml files in specified folder and parse it one by one.
        string[] files = Directory.GetFiles(mailbox, "*.eml");

       

        for (int i = 0; i < files.Length; i++)
        {
            ParseEmail(files[i]);
            
        }
        DeleteFiles();
    }

    public void ParseEmail(string emlFile)
    {
        bool True = true;

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringCLSBeta"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }

        Mail oMail = new Mail("EG-B1374632949-00931-42VE5BBUC5UCBCV6-5FB8D8CFD548A123");
        oMail.Load(emlFile, false);

		// Parse Mail From, Sender

		string shtml = StripHTML(oMail.TextBody);


		//HttpContext.Current.Response.Write("Test" + shtml);
		//HttpContext.Current.Response.End();

		if (oMail.Subject.Contains("Regarding") || oMail.Subject.Contains("(REF:"))
            {

                string subject = oMail.Subject.ToString();
                string AppID = subject.Substring(subject.LastIndexOf(':') + 1);
            if (AppID.Contains(")"))
            {
                AppID = AppID.Remove(AppID.Length - 1);
            }

//
         // HttpContext.Current.Response.Write(shtml.Replace("'", ""));
		  // HttpContext.Current.Response.End();

            try
            {
               
                SqlCommand updateemail = new SqlCommand();
                updateemail.Connection = connection;
                updateemail.CommandText = "INSERT INTO tblnotes (AppID,note,createduserid,createddate,noteactive,companyid,notetype) VALUES ('" + AppID + "','" + shtml + "','772',GETDATE(),1,1430,2)";
                updateemail.ExecuteNonQuery();
                

                SqlCommand updateemailnew = new SqlCommand();
                updateemailnew.Connection = connection;
                updateemailnew.CommandText = "INSERT INTO tblemaillogs (EmailSubject, EmailText, EmailSenderAddress, EmailType, InOrOut, AppID, CompanyID) VALUES ('" + oMail.Subject.ToString() + "', '" + shtml + "', '" + oMail.From + "', 'Email', 'In', '" + AppID + "',1430)";
                updateemailnew.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
//             	 try
//            	{
//                	Common.postEmail("", "dev.team@engagedcrm.co.uk;projectsupport@engagedcrm.co.uk;jon.lord@metrofinance.co.uk", "Error When parsing email " + AppID + "", "Error when parsing the email " + shtml.Replace("&quot;", "").Replace(":", "").Replace("'", "").Replace("`", "").Replace("&amp;", "and").Replace("&nbsp;"," ").Replace("-", "").Replace("?", "") + "", true, "", true);
//				}
//				catch
//				{
//					Common.postSMS("07974400840","Email Failed to parse " + AppID + "");
//				}
            }


            try
            {

                string SalesUserID = "";
                string CallCentreUserID = "";
                string AdminUserID = "";
                string CallCentreUserEmail = "";
                string SalesUserEmail = "";
                string AdminUserEmail = "";
				string App1FirstName = "";
				string App1Surname = "";

                connection.Open();
                SqlCommand myCommandUsers = new SqlCommand(string.Format("SELECT dbo.tblapplications.AppID, dbo.tblCustomers.CustomerFirstName AS App1FirstName, dbo.tblCustomers.CustomerSurname AS App1Surname, dbo.tblapplications.CallCentreUserID, dbo.tblapplications.SalesUserID, dbo.tblapplications.AdministratorUserID FROM dbo.tblCustomers LEFT OUTER JOIN dbo.tblApplicants ON dbo.tblCustomers.CustomerID = dbo.tblApplicants.ApplicantId RIGHT OUTER JOIN dbo.tblapplications ON dbo.tblApplicants.ApplicantId = dbo.tblapplications.App1ID WHERE dbo.tblapplications.AppID = {0} ", AppID), connection);

                var userReader = myCommandUsers.ExecuteReader();

                while (userReader.Read())
                {
                    SalesUserID = userReader["SalesUserID"].ToString();
                    CallCentreUserID = userReader["CallCentreUserID"].ToString();
                    AdminUserID = userReader["AdministratorUserID"].ToString();
					App1FirstName = userReader["App1FirstName"].ToString();
					App1Surname = userReader["App1Surname"].ToString();

                    SqlCommand myCommandCallcentreemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", CallCentreUserID), connection);

                    var callcentreReader = myCommandCallcentreemail.ExecuteReader();

                    while (callcentreReader.Read())
                    {

                        CallCentreUserEmail = callcentreReader["UserEmailAddress"].ToString();
                        if (Common.checkValue(CallCentreUserEmail))
                        {
                            Common.postEmail("", CallCentreUserEmail, "CSC New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
                        }else
                        {
                            Common.postEmail("", "william.worthington@engageccrm.co.uk", "Unassigned Call Centre Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
                        }

                    }

					SqlCommand myCommandsalesemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", SalesUserID), connection);

					var salesReader = myCommandsalesemail.ExecuteReader();

					while (salesReader.Read())
					{

						SalesUserEmail = salesReader["UserEmailAddress"].ToString();
						if (Common.checkValue(SalesUserEmail))
						{
							Common.postEmail("", SalesUserEmail, "Sales New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
						}
						else
						{
							Common.postEmail("", "william.worthington@engageccrm.co.uk", "Unassigned Sales Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
						}

					}

					SqlCommand myCommandadminemail = new SqlCommand(string.Format("select UserEmailAddress from tblusers where UserID = {0} ", AdminUserID), connection);

					var adminReader = myCommandadminemail.ExecuteReader();

					while (adminReader.Read())
					{

						AdminUserEmail = adminReader["UserEmailAddress"].ToString();
						if (Common.checkValue(AdminUserEmail))
						{
							Common.postEmail("", AdminUserEmail, "Admin New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
						}
						else
						{
							Common.postEmail("", "william.worthington@engageccrm.co.uk", "Unassigned Admin Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);
						}

					}

				}
				connection.Close();
                Common.postEmail("", "dev.team@engagedcrm.co.uk", "New Email Recieved for AppID " + AppID + " - " + App1FirstName + " " + App1Surname + "", "New Email Reply Recieved There has been a response on case:" + AppID + " - " + App1FirstName + " " + App1Surname + " Please login for updates.", true, "", true);


            }
            catch (Exception ex)
            {
                Common.postEmail("", "dev.team@engagedcrm.co.uk", "Notification Email Failed to Send " + AppID + "", "" + ex + "", true, "", true);
            }


        }

            
        





    }

    public static string StripHTML(string input)
    {
        return Regex.Replace(input, "\\<[^\\>]*\\>", String.Empty);
    }

    public void DeleteFiles()
    {
        System.IO.DirectoryInfo di = new DirectoryInfo("c:\\clsinbox");

        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();
        }

    }

}


