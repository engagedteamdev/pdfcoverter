' Pickup2.vbs - A pickup event script for Mail Enable
' Requires Windows 2K or XP (I believe)
'
' This script is fairly restrictive in it's current form.
' It removes all attachments with a "Content-Type" of 
' "application/octet-stream" and "application/x-msdownload".
' It also parses the file name of any attachment(s) and
' removes any hta, bat, com, exe, vbs, js, java, pl, pif and php
' extensions that it finds. It also looks for ".{" to thwart the 
' MS CLSID vulnerability.
'
' If you want a good scare, try running the tests offered at
' http://www.windowsecurity.com/emailsecuritytest before you apply any 
' additional security to Mail Enable. Your virus protection
' software will catch many of the tests, but I bet it doesn't
' catch them all!
'
' This script is an expanded version of a script posted by "Sunpost"
' on the Mail Enable deveopers forum (http://forum.mailenable.com/viewtopic.php?t=1095)
' and is by no means complete. Nor is it quaranteed to be efficient, effective
' or even safe. Use at your own risk, and share any improvements you make!
'
'============================
' And away we go...
'============================ 

Dim message 'As New CDO.message 
Dim ibp 'As IBodyPart 
Dim WshShell 
Dim MailDataPath 
Dim MessageID 
Dim ConnectorCode 
Dim MessageFilePath
Dim htmlSource
Dim msgDetails(5)
Dim postString
Dim strMessageText

Set WshShell = CreateObject("WScript.Shell")
MailDataPath = WshShell.RegRead("HKLM\SOFTWARE\Mail Enable\Mail Enable\Data Directory") 

' Live
WshShell.LogEvent 4,"Email parsing started with arguments " & WScript.Arguments(0) & " " & WScript.Arguments(1) & " " & WScript.Arguments(2)
ConnectorCode = "SMTP"
MessageID = WScript.Arguments(2) ' e.g. "FF63684D20984FF1B1A5B58475A1E24C.MAI"
MessageFilePath = MailDataPath & "\Postoffices\" & WScript.Arguments(0) & "\mailroot\" & WScript.Arguments(1) & "\Inbox\" & MessageID ' Live

' Test
'MessageFilePath = MailDataPath & "\Postoffices\cms\mailroot\itsupport\Inbox\053902DAFC404B44BA6B6E06A0C5C7B3.MAI" 'Single line test
'msgbox(MessageFilePath)


Call checkEmailParser()

WshShell.LogEvent 4,"Email parsing ended"

Sub checkEmailParser()
    Set message = LoadMessageFromFile(MessageFilePath)
    Select Case True ' List of allowed to/from combinations
        Case InStr(message.To, "mooveinsurance@lunarmedia.co.uk") > 0 And InStr(message.From, "no-reply@paymentshield.co.uk") > 0 ' neb.stevenson@gmail.com
            emailParser()
        Case InStr(message.To, "neb.stevenson@gmail.com") > 0
            emailParser()
        Case Else
            ' Do nothing
    End Select
End Sub

Sub emailParser()
    
    y = 0
    strAttachments = ""
    arrExtensions = Array(".doc",".docx",".xls",".xlsx",".csv",".txt",".jpg",".jpeg",".bmp",".gif",".png",".pdf",".rtf",".xml") 'Add or remove the extensions you want filtered    
    For Each attach In message.Attachments
        blnValid = False
	    For x As Integer = 0 To UBound(arrExtensions) 'Step through all the defined test strings
		    If (InStr(LCase(attach.FileName),arrExtensions(x))) Then
		        blnValid = True
		        Exit For
		    End If
		Next
		If (blnValid = True) Then
		    strNewFileName = getFileName(attach.FileName,y)
		    attach.SaveToFile("C:\inetpub\vhosts\lunarmedia.co.uk\crmdev\net\attachments" & strNewFileName)
		    If (strAttachments = "") Then
		        strAttachments = strNewFileName
		    Else
		        strAttachments = strAttachments & "," & strNewFileName
		    End If
		End If
		y = y + 1
    Next
    
    ' Post the email to the CRM email parser
    strPost = "frmEmailFromAddress=" & message.From & "&frmEmailToAddress=" & message.To & "&frmSubject=" & message.Subject & "&frmEmailBody=" & Replace(message.TextBody,"&"," AND ") & "&frmAttachments=" & strAttachments
	postEmail(strPost)
    
End Sub

Function getFileName(filename,fileno)
    arrFile = Split(filename,".")
    If (UBound(arrFile) > 0) Then
        strFileExt = arrFile(UBound(arrFile))
    End If
    getFileName = Year(Now) & padZeros(Month(Now()),2) & padZeros(Day(Now()),2) & padZeros(Hour(Now()),2) & padZeros(Minute(Now()),2) & padZeros(Second(Now()),2) & "_" & fileno & "." & strFileExt
End Function

Sub postEmail(post)
    Dim strURL, objXMLHTTP, arrResponseText
    strURL = "http://crmdev.lunarmedia.co.uk/net/webservices/inbound/emailstatusupdate.aspx"
    Set objXMLHTTP = CreateObject("Msxml2.ServerXMLHTTP.3.0")
	With objXMLHTTP
		' resolve, connect, send, receive - in milliseconds		
		.SetTimeouts 0, 0, 0, 0
		.Open "POST", strURL, False
		.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		.Send(post)
	End With
	Set objXMLHTTP = Nothing
End Sub

' End of script. Functions and subs follow.

'************************************************************
'                 FUNCTIONS & SUBROUTINES
'************************************************************

Sub buildMsgDetails() 'Retrieves values from the message and stores them in an array

	msgDetails(0) = message.To
	msgDetails(1) = message.From
	msgDetails(2) = message.Subject
	msgDetails(3) = msgDetails(3) & ibp.FileName & " "
	attachFileID = messageID & " - " & ibp.FileName
	msgDetails(4) = msgDetails(4) & attachFileID & " "

End Sub

' Function LoadMessageFromFile creates several objects to open the mail file
' and turn it into a CDO.Message object. Once you have a message object,
' reading and modifying the message (and it's associated attachments etc)
' becomes quite simple. If you're not familiar with CDO, start here - 
' http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cdosys/html/_cdosys_about_cdo_for_windows_2000.asp?frame=true

Function LoadMessageFromFile(Path) 'As Message 
	Dim Stm 
	Set Stm = CreateObject("ADODB.Stream")
	Stm.Charset = "ascii" 
	Stm.Open 
	Stm.LoadFromFile Path 
	Dim iMsg 
	Set iMsg = CreateObject("CDO.Message") 
	Dim iDsrc 
	Set iDsrc = iMsg.GetInterface("IDataSource") 
	iDsrc.OpenObject Stm, "_Stream" 
	Set LoadMessageFromFile = iMsg 
End Function 

Function padZeros(str,no)
	If IsNull(str) Then
		padZeros = str
	Else
		For i = 1 To (no - Len(str))
			str = "0" & str
		Next
		padZeros = str
	End If
End Function

'#######################################################################
'# Function BuildErrorText builds a custom error string to be inserted
'# into any email that has it's attachment stripped off. Plain text only!
'# Edit the content of the last two "msgSrc =" lines to point to an email
'# address on your server.
'#######################################################################

Function BuildErrorText(attachType, refFile) 'As String
	Select Case attachType
		Case 0 ' Executable file
			msgSrc = "Attachment was removed beacuse it contained executable code. "
			msgSrc = msgSrc & "If the attachment was expected and from a trusted source, please ask them to zip and re-send the attachment. "

		Case 1 ' Hidden script file - Klez and the like.
			msgSrc = "Attachment was removed beacuse it contained hidden executable code. This attachment was most likely a virus. "
	End Select
	msgSrc = msgSrc & "The attched file has been saved. If you need this file, email GuyInCharge@some.com and refer to file '" & refFile & "'. " 
	msgSrc = msgSrc & "If the attachment was expected and from a trusted source, please ask them to contact GuyInCharge@some.com. "
  BuildErrorText = msgSrc
End Function

Sub SaveMessageToFile(iMsg, Filepath) 
	Dim Stm 
	Set Stm = CreateObject("ADODB.Stream") 
	Stm.Open 
	Stm.Type = 2 'adTypeText 
	Stm.Charset = "US-ASCII" 
	Dim iDsrc 
	Set iDsrc = iMsg.DataSource 
	iDsrc.SaveToObject Stm, "_Stream" 
	Stm.SaveToFile Filepath, 2 'adSaveCreateOverWrite 
End Sub

Sub SaveDataToFile

    msgbox("Saving")

	Dim strWaitingDirectory, strCommandList
	Dim objFSO, objTextFile

	strWaitingDirectory = "C:\inetpub\vhosts\lunarmedia.co.uk\crmdev\net\waiting\"
	strCommandList 		= strWaitingDirectory & "leads.csv"
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	Set objTextFile = objFSO.CreateTextFile(strCommandList, True)
	With objTextFile
		.WriteLine message.TextBody
	End With
	
	objTextFile.Close
	Set objTextFile = Nothing

End Sub

Function regexReplace(expr,rep,str)
	Dim RegEx
	Set RegEx = New RegExp
	RegEx.Pattern = expr
	RegEx.Multiline = True
	RegEx.Global = True
	RegEx.IgnoreCase = True
	str = RegEx.Replace(str, rep)
	regexReplace = str
	Set RegEx = Nothing
End Function

Function regexFirstMatch(pat, val)
    If (pat = "") Then
        regexFirstMatch = ""
    Else   
        Dim RegEx, Matches 
		Set RegEx = New RegExp 
		RegEx.Global = True 
		RegEx.IgnoreCase = True 
		RegEx.Pattern = pat	
		Set Matches = RegEx.Execute(val)
        If (Matches.Count > 0) Then
            regexFirstMatch = regexReplace(pat, "$1", Matches.Item(0))
        Else
            regexFirstMatch = ""
        End If
    End If
End Function

'########################################################
'# SENDWARNINGMAIL
'# The following subroutine uses CDO.sys to send notification
'# of a removed attachment. There are several items that require
'# configuration for this to operate correctly. The 11th line
'# (.Item(http:// ... /smtpserver) MUST be set to your ME server
'# address. You can use any format (URL, IP, etc) that will
'# resolve to your server. You may also need to adjust the following
'# line (smtpconnectiontimeout) if you're having problems with timeouts.
'# The timeout value is in seconds. If your ME server requires authentication
'# you'll need to supply a valid username/password in lines 14 and 15. Be 
'# sure to remove the comment (') before these lines! If you're using 
'# a port besides 25 (default) for SMTP, adjust the last variable accordingly.
'#
'# Moving on, you'll see the "msgBody" variable. This contains the HTML
'# that will make up the email. Change it as you see fit. The "myDetails" 
'# array contains the following values:
'# myDetails(0) = the address of the original recipient of the message
'# myDetails(1) = the address of the original sender of the message
'# myDetails(2) = the subject of the original message
'# myDetails(3) = the name of the file that was removed
'# myDetails(4) = the name of the file as it was stored in your "safe" directory
'# Be sure to change the "postmaster@yourdomain.com" addresses
'# so that they point to a real account.
'# It is not necessary to generate a "plain text" version of the
'# message - CDO handles this auto-magically.
'#
'# Finally, there is the "With iMsg2" section. This constructs the
'# header on the outgoing mail. You can control who receives notification
'# in this section by setting the "CC" and "BCC" variables. If you
'# only need to notify the original sender, comment these lines out.
'# Also be sure to provide a valid "From" address, or ME may
'# fail to relay the message.
'#
'# GOOD LUCK!
'##########################################################

Sub sendEmail(mailfrom, mailto, subject, htmlbody)
	Dim iMsg2
	Set iMsg2 = CreateObject("CDO.Message")
	Dim iConf2
	Set iConf2 = CreateObject("CDO.Configuration")
	Dim Flds2
	Set Flds2 = iConf2.Fields
	dim iItem2

	With Flds2
		.Item("http://schemas.microsoft.com/cdo/configuration/sendusing")  				= 2
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")        		= "mail.lunarmedia.co.uk"
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") 	= 10 ' quick timeout
		' Un-comment the following lines if your server requires authentication
		.Item("http://schemas.microsoft.com/cdo/configuration/sendusername")   		    = "itsupport@cms" 
		.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword")   		    = "D0ncaster" 
		.item("http://schemas.microsoft.com/cdo/configuration/smtpserverport")			= 25
		.Update
	End With

	' Build the message
	dim msgBody
	'msgBody = "<H2>Attachment Removed</H2>The attachment(s) <b><em>" & myDetails(3) _
	'	& "</em></b> sent to <b>" & myDetails(0) & "</b> was removed " _
	'	& "because it could not be verified as being safe. Please contact " _
	'	& "<a href=""mailto:postmaster@yourdomain.com"">postmaster@yourdomain.com</a> " _
	'	& "to arrange for delivery of this attachment.<br>Please refer to Attachment ID " _
	'	& myDetails(4)

	With iMsg2
		Set .Configuration = iConf2
		.To       =	mailto
		If (mailfrom <> "" And Not IsNull(mailfrom)) Then
		    .CC        = mailfrom ' Send notification to original sender
		End If
		'.BCC	  = """Postmaster"" <postmaster@yourdomain.com>"  ' Send notification to postmaster
		.From     = "neb.stevenson@gmail.com"
		.Subject  = subject
		.HtmlBody = htmlbody
		.Send
	End With
End Sub
