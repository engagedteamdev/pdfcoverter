﻿Imports Config, Common, System.Xml, System.Net, System.IO

Partial Class StatusUpdate
    Inherits System.Web.UI.Page

    Private intSOAPRequestNo As String = getIndexNumber(), strSOAPMessage As String = "", intNoReceived As Integer = 0, intNoFailures As Integer = 0
    Private MediaID As String = "", CompanyID As String = "0"
    Private objInputXMLDoc As XmlDocument = New XmlDocument

    Public Sub statusUpdate()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
        If (objHeaderTest Is Nothing) Then
            SOAPUnsuccessfulMessage("Data field not found. Field = executeSoap", -2, intSOAPRequestNo)
        End If
        Dim objMediaID As XmlNode = objInputXMLDoc.SelectSingleNode("//MediaID")
        If (Not objMediaID Is Nothing) Then
            MediaID = objMediaID.InnerText
        End If
        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        If (Not checkValue(MediaID)) Then
            SOAPUnsuccessfulMessage("Not Authorised - " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), -4, intSOAPRequestNo)
        End If
        checkMandatory()
        readData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 CompanyID FROM tblwebservices " & _
                                "WHERE (WebServiceMediaID = '" & MediaID & "') AND (WebServiceIPAddress LIKE '%" & ip & "%;' OR '" & ip & "' = '109.234.207.186') " & _
                                "AND (WebServiceActive = 1)"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"Lead"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo)
        End If
    End Sub

    Private Sub readData()
        Dim boolUpdated As Boolean = False
        Dim AppID As String = "", strClientRef As String = "", strStatusCode As String = "", strSubStatusCode As String = ""
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.DocumentElement.SelectSingleNode("//executeSoap").ChildNodes
        For Each child As XmlNode In objChildNodes
            If (child.Name = "Lead") Then
                intNoReceived += 1
                strClientRef = getNodeText(child, Nothing, "ClientRef")
                strStatusCode = getNodeText(child, Nothing, "StatusCode")
                strSubStatusCode = getNodeText(child, Nothing, "SubStatusCode")
                AppID = validAppID(strClientRef)
                If (checkValue(AppID)) Then
                    If (Not boolUpdated) Then
                        Dim strSQL As String = "UPDATE tblapplicationstatus SET PartnerStatusCode = '" & strStatusCode & "' WHERE (PartnerStatusCode = '' OR PartnerStatusCode <> '" & strStatusCode & "') AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                        Dim intRowsUpdated As Integer = executeRowsAffectedQuery(strSQL)
                        If (intRowsUpdated > 0) Then
                            boolUpdated = True
                            appendSOAPMessage("Application successfully updated", 1, AppID)
                        Else
                            intNoFailures += 1
                            appendSOAPMessage("Update not required", 1, AppID)
                        End If
                    End If
                Else
                    boolUpdated = False
                    intNoFailures += 1
                    appendSOAPMessage("Inconsistent data found. Field = ClientRef, value = " & strClientRef, -5, AppID)
                End If
            End If
        Next
        SOAPMessage()
    End Sub

    Private Function validAppID(ByVal clientref As String) As String
        Dim boolValid As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 AppID FROM tblapplications APP INNER JOIN tblmediacampaigns MC ON MC.MediaCampaignID = APP.MediaCampaignIDOutbound " & _
                                    "WHERE APP.ClientReferenceOutbound = '" & clientref & "' AND MC.MediaID = '" & MediaID & "' AND APP.CompanyID  = '" & CompanyID & "' "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDataBase = Nothing
    End Function

    Private Function validSubStatusCode(ByVal sst As String) As Boolean
        If (Not checkValue(sst)) Then
            Return True
        Else
            If (sst = "REF" Or sst = "LTV" Or sst = "ADV" Or sst = "NOF" Or sst = "SEL") Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Private Sub appendSOAPMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<ResultSet>")
            .Append("<Result>" & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<SoapNo>" & app & "</SoapNo>")
            .Append("</ResultSet>")
        End With
        strSOAPMessage += objStringBuilder.ToString
    End Sub

    Private Sub SOAPMessage()
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crm.engaged-solutions.co.uk/"">")
            .Append(strSOAPMessage)
            .Append("<NoReceived>" & intNoReceived & "</NoReceived>")
            .Append("<NoUpdated>" & intNoReceived - intNoFailures & "</NoUpdated>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        If (intNoFailures = 0) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, 1, intNoReceived & " updates received, " & intNoReceived - intNoFailures & " updated")
        Else
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -5, intNoReceived & " updates received, " & intNoReceived - intNoFailures & " updated")
        End If
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

End Class
