﻿Imports Config, Common
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class SOAP
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    ' Notes
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList	
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = ""
    Private strLeadID As String = "", strLeadSource As String = "", dteApp1DOB As String = "", dteApp2DOB As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strProductType As String = "Secured Loan"

    Public Sub SOAPPost()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        If Not checkValue(strMediaCampaignID) Then
            Call SOAPUnsuccessfulMessage("Invalid MediaCampaignID. Not Authorised - " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), -4, intSOAPRequestNo)
            'strMediaCampaignID = "10000" ' Set default
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//LmbLeads/LmbLead")
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = LmbLeads/LmbLead", -2, intSOAPRequestNo)
        End If
        Dim objLeadID As XmlNode = objInputXMLDoc.SelectSingleNode("//LeadID")
        If (Not objLeadID Is Nothing) Then
            strLeadID = objLeadID.InnerText
        End If
        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        Call checkMandatory()
        Call readData()
        Call writeData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.WebServiceID " & _
                                "FROM tblmediacampaigns INNER JOIN " & _
                                "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                                "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                                "WHERE (tblmediacampaigns.MediaCampaignID = '" & strMediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR '" & ip & "' = '127.0.0.1') " & _
                                "AND (WebServiceActive = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"LeadType", "LoanPurpose", "DesiredLoanAmnt", "MainFirstName", "MainLastName", "Phone1", "CurStreetAddressLine1", "CurPostCode"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            Call SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo)
        End If
    End Sub

    Private Sub readData()
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.DocumentElement.SelectSingleNode("//LmbLeads/LmbLead").ChildNodes
        For Each child As XmlNode In objChildNodes
            If (child.ChildNodes.Count > 1) Then
                For Each child2 As XmlNode In child.ChildNodes
                    validateData(child2.Name, child2.InnerText)
                Next
            Else
                validateData(child.Name, child.InnerText)
            End If
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName <> 'ProductType' AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex")
                strMessage = Row.Item("ValRegexMessage")
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
				strFunctionParameters = Row.Item("ValFunctionParameters").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        ' Call regular expression routine
        If (regexTest(strRegex, val)) Then
            If (checkValue(val)) Then
                If (checkValue(strFunction)) Then
                    Dim arrParams As String() = {val}
                    If (checkValue(strFunctionParameters)) Then
                        Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                        For y As Integer = 0 To UBound(arrFunctionParameters)
                            If (checkValue(arrFunctionParameters(y))) Then
                                ReDim Preserve arrParams(y + 1)
                                arrParams(y + 1) = arrFunctionParameters(y)
                            End If
                        Next
                    End If
                    val = executeMethodByName(Me, strFunction, arrParams)
                End If
                Select Case strTable
                    Case "tblapplications"
                        tblapplications_flds.Add(strField)
                        tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatus"
                        tblapplicationstatus_flds.Add(strField)
                        tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbldatastore"
                        tbldatastore_flds.Add(strField)
                        tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblnotes"
						If (fld = "LenderName") Then
							val = "Client is interested in product from " & val
						End If
						If (fld = "ClientProductAPR") Then
							val = "Product APR is " & val & "%"
						End If			
                        tblnotes_flds.Add(strField)
                        tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))							
                End Select
            End If
            boolValidate = True
        Else
            boolValidate = False
        End If

        If (boolValidate = False) Then
            Call SOAPUnsuccessfulMessage("Inconsistent data found. Field = " & fld & ", value = " & val & ", Criteria = " & strMessage, -5, intSOAPRequestNo)
        End If

    End Sub
    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub writeData()
        Dim boolWrite As Boolean = True
        Dim strQryFields As String = "", strQryValues As String = "", strQryFieldsValues As String = ""
        ' Only start insert if main application array is found
        If (tblapplications_flds.Count > 0) Then

            Dim objMainBirthDateDay As XmlNode = objInputXMLDoc.SelectSingleNode("//MainBirthDateDay")
            Dim objMainBirthDateMonth As XmlNode = objInputXMLDoc.SelectSingleNode("//MainBirthDateMonth")
            Dim objMainBirthDateYear As XmlNode = objInputXMLDoc.SelectSingleNode("//MainBirthDateYear")

            If (Not objMainBirthDateDay Is Nothing And Not objMainBirthDateMonth Is Nothing And Not objMainBirthDateYear Is Nothing) Then
                dteApp1DOB = combineDate(objMainBirthDateDay.InnerText, objMainBirthDateMonth.InnerText, objMainBirthDateYear.InnerText)
            End If

            Dim objJointBirthDateDay As XmlNode = objInputXMLDoc.SelectSingleNode("//JointBirthDateDay")
            Dim objJointBirthDateMonth As XmlNode = objInputXMLDoc.SelectSingleNode("//JointBirthDateMonth")
            Dim objJointBirthDateYear As XmlNode = objInputXMLDoc.SelectSingleNode("//JointBirthDateYear")

            If (Not objJointBirthDateDay Is Nothing And Not objJointBirthDateMonth Is Nothing And Not objJointBirthDateYear Is Nothing) Then
                dteApp2DOB = combineDate(objJointBirthDateDay.InnerText, objJointBirthDateMonth.InnerText, objJointBirthDateYear.InnerText)
            End If

            For i = 0 To tblapplications_flds.Count - 1
                If (i = 0) Then
                    strQryFields = "INSERT INTO tblapplications (CompanyID, MediaCampaignIDInbound, ProductType, App1DOB, App2DOB, "
                    strQryValues = "VALUES (" & formatField(CompanyID, "N", 0) & ", " & formatField(strMediaCampaignID, "N", 0) & ", " & formatField(strProductType, "", "Secured Loan") & ", " & formatField(dteApp1DOB, "DT", "NULL") & ", " & formatField(dteApp2DOB, "DT", "NULL") & ", "
                End If

                strQryFields = strQryFields & tblapplications_flds(i)
                strQryValues = strQryValues & tblapplications_vals(i)

                If (i <> tblapplications_flds.Count - 1) Then
                    strQryFields = strQryFields & ", "
                    strQryValues = strQryValues & ", "
                Else
                    strQryFields = strQryFields & ") "
                    strQryValues = strQryValues & ") "
                End If
            Next

            'Response.Write(strQryFields & strQryValues & "<br />")
            AppID = executeIdentityQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            ' Insert default starter values in the application status table
            strQryFields = "INSERT INTO tblapplicationstatus (AppID, CompanyID, CreatedUserID) "
            strQryValues = "VALUES (" & formatField(AppID, "N", 0) & ", " & formatField(CompanyID, "N", 0) & ", " & formatField(returnSystemUser(CompanyID), "N", 0) & ") "

            'Response.Write(strQryFields & strQryValues & "<br />")
            Call executeNonQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            If (tblapplicationstatus_flds.Count > 0) Then
                For i = 0 To tblapplicationstatus_flds.Count - 1
                    If (i = 0) Then
                        strQryFieldsValues = "UPDATE tblapplicationstatus SET "
                    End If

                    strQryFieldsValues = strQryFieldsValues & tblapplicationstatus_flds(i) & " = " & tblapplicationstatus_vals(i)

                    If (i <> tblapplicationstatus_flds.Count - 1) Then
                        strQryFieldsValues = strQryFieldsValues & ", "
                    Else
                        strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' "
                    End If
                Next
                'Response.Write(strQryFieldsValues & "<br />")
                Call executeNonQuery(strQryFieldsValues)
                strQryFieldsValues = ""
            End If

            ' Insert stored data
            If (tbldatastore_flds.Count > 0) Then
                For i As Integer = 0 To tbldatastore_flds.Count - 1
                    strQryFieldsValues = "INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & tbldatastore_flds(i) & "'," & tbldatastore_vals(i) & ")"
                    Call executeNonQuery(strQryFieldsValues)
                Next
            End If
			
			' Insert notes
			If (tblnotes_flds.Count > 0) Then
				For i As Integer = 0 To tblnotes_flds.Count - 1
					strQryFields = "INSERT INTO tblnotes (CompanyID, AppID, CreatedUserID, "
					strQryValues = "VALUES ('" & CompanyID & "','" & AppID & "','" & returnSystemUser(CompanyID) & "', "
	
					strQryFields = strQryFields & tblnotes_flds(i) & ") "
					strQryValues = strQryValues & tblnotes_vals(i) & ") "
	
					Call executeNonQuery(strQryFields & strQryValues)	
				Next
			End If			

            executeNonQuery("UPDATE tblapplications SET MediaCampaignCostInbound = '" & getAnyField("MediaCampaignCostInbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID) & "' WHERE MediaCampaignCostInbound = 0 AND AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")

            'Set working hour
            If (checkWorkingHour(Config.DefaultDateTime)) Then
                executeNonQuery("UPDATE tblapplicationstatus SET CreatedInsideBusinessHours = 1 WHERE (AppID = '" & AppID & "')")
            End If

            ' Set LTV
            executeNonQuery("EXECUTE spupdateltv @AppID = " & AppID & ", @CompanyID = " & CompanyID)

            ' Correct telephone numbers
            executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)

            ' Set CountryCode
            executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT PostCodeCountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "')), 'ENG') WHERE (AppID = '" & AppID & "')")

            boolWrite = True

        Else

            boolWrite = False

        End If

        If (Not boolWrite) Then
            SOAPUnsuccessfulMessage("An unknown error has occured", -1, intSOAPRequestNo)
        Else
            'liveLeadTransfer()
            Dim strDuplicateChecker As String = getAnyField("MediaCampaignDuplicateChecker", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
            If (strDuplicateChecker) Then
                Call executeNonQuery("EXECUTE spduplicateappcheck @AppID = " & formatField(AppID, "N", 0) & ", @CompanyID = " & CompanyID)
            End If
            'Dim strStatusCode As String = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
            'Dim strSubStatusCode As String = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
            'If (strStatusCode = "INV") Then
            '    Select Case strSubStatusCode
            '        Case "DUP"
            '            executeNonQuery("UPDATE tblapplications SET MediaCampaignCostInbound = 0 WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")
            '            Call SOAPUnsuccessfulMessage("Duplicate case", -5, intSOAPRequestNo)
            '        Case "WRN"
            '            executeNonQuery("UPDATE tblapplications SET MediaCampaignCostInbound = 0 WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")
            '            Call SOAPUnsuccessfulMessage("Invalid telephone number", -5, intSOAPRequestNo)
            '    End Select
        	'Else
                Dim strLetterID As String = getAnyField("MediaCampaignLetterID", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
                If (checkValue(strLetterID)) Then
                    Dim arrLetterID As Array = Split(strLetterID, ",")
                    For x As Integer = 0 To UBound(arrLetterID)
                        Dim intLetterID As String = arrLetterID(x)
                        If (CInt(intLetterID) > 0) Then
                            Dim intLetterType As String = getAnyField("LetterType", "tbllettertemplates", "LetterID", intLetterID)
                            If (intLetterType = 2) Then
                                CommonSave.sendEmails(AppID, intLetterID, CompanyID)
                            ElseIf (intLetterType = 3) Then
                                CommonSave.sendSMS(AppID, intLetterID)
                            End If
                        End If
                    Next
                End If
            	Call SOAPSuccessfulMessage("Application successfully received", 1, AppID)
            'End If
        End If

    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<LmbLeads xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<LmbLead>")
            .Append("<LeadID>" & strLeadID & "</LeadID>")
            .Append("<ClientLeadID>" & app & "</ClientLeadID>")
            .Append("<ReceiptStamp>" & ddmmyyhhmmss2utc(Config.DefaultDateTime) & "</ReceiptStamp>")
            .Append("<SuccessFlag>Y</SuccessFlag>")
            .Append("<ErrorTxt></ErrorTxt>")
            .Append("</LmbLead>")
            .Append("</LmbLeads>")
        End With
        saveXMLReceived(Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<LmbLeads xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<LmbLead>")
            .Append("<LeadID>" & strLeadID & "</LeadID>")
            .Append("<ClientLeadID>" & app & "</ClientLeadID>")
            .Append("<ReceiptStamp>" & ddmmyyhhmmss2utc(Config.DefaultDateTime) & "</ReceiptStamp>")
            .Append("<SuccessFlag>N</SuccessFlag>")
            .Append("<ErrorTxt>" & msg & "</ErrorTxt>")
            .Append("</LmbLead>")
            .Append("</LmbLeads>")
        End With
        saveXMLReceived(Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub liveLeadTransfer()
        Dim strURL As String = ""
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignID, MediaCampaignCampaignReference, MediaCampaignScheduleID, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress " & _
                            "FROM vwdistributionsearchlive " & _
                            "WHERE (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') " & _
                            "ORDER BY MediaCampaignScheduleRequiredRate DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "vwdistributionsearchlive")
        Dim dsHotkey = objDataSet.Tables("vwdistributionsearchlive")
        If (dsHotkey.Rows.Count > 0) Then
            For Each Row In dsHotkey.Rows
                If (Row.Item("MediaCampaignDeliveryTypeID") = "2") Then ' XML Transfer
                    If (checkValue(Row.Item("MediaCampaignDeliveryXMLPath"))) Then
                        strURL = ApplicationURL & "/webservices/outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignID") & "&MediaCampaignCampaignReference=" & Row.Item("MediaCampaignCampaignReference") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID")
                    End If
                Else ' Email / Email & CSV
                    strURL = ApplicationURL & "/webservices/outbound/endhotkey.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignID") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&MediaCampaignDeliveryEmailAddress=" & Row.Item("MediaCampaignDeliveryEmailAddress") & "&intActionType=" & Row.Item("MediaCampaignDeliveryTypeID")
                End If
                If (checkValue(strURL)) Then
                    getWebRequest(strURL)
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsHotkey = Nothing
        objDataBase = Nothing
    End Sub

End Class
