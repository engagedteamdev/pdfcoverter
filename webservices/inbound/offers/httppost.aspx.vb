﻿Imports SSL, Config, Common, CommonSave

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Previous Addresses
    Private tbladdresses_flds As ArrayList = New ArrayList, tbladdresses_vals As ArrayList = New ArrayList
    ' Previous Employers
    Private tblemployers_flds As ArrayList = New ArrayList, tblemployers_vals As ArrayList = New ArrayList
    ' Notes
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList
    ' Reminders
    Private tblreminders_flds As ArrayList = New ArrayList, tblreminders_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    ' Milestones
    Private tblapplicationstatusdates_flds As ArrayList = New ArrayList, tblapplicationstatusdates_vals As ArrayList = New ArrayList
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = HttpContext.Current.Request("AppID")
    Private UserID As String = HttpContext.Current.Request("UserID")
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strReturnURL As String = ""
    Private intSkipTelephoneCorrection As String = HttpContext.Current.Request("SkipTelephoneCorrection")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

    Public Sub httpPost()
        If (Left(AppID, 1) = "0") Then
            AppID = Right(AppID, 7)
        End If
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        If (Not checkValue(AppID)) Then
            intSkipTelephoneCorrection = "1"
            Call checkAuthority(Request.ServerVariables("REMOTE_ADDR"))
        Else
            intSkipTelephoneCorrection = "1"
            Call checkAuthoritySimple(Request.ServerVariables("REMOTE_ADDR"))
            If checkValue(MediaCampaignID) Then
                If (getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID) <> CInt(MediaCampaignID) Or Not checkValue(getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID))) Then
                    saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid AppID - " & AppID)
                    Response.Write("Invalid AppID - " & AppID)
                    Response.End()
                End If
            Else
                saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Invalid Media Campaign - " & MediaCampaignID)
                Response.Write("Invalid Media Campaign - " & MediaCampaignID)
                Response.End()
            End If
        End If
        'If (Not checkValue(AppID)) Then
        '    Call checkMandatory()
        'Else
        Call checkMandatorySimple()
        'End If
        Call readData()
        Call saveData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.WebServiceID " & _
                                "FROM tblmediacampaigns INNER JOIN " & _
                                "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                                "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                                "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR tblwebservices.WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                "AND (WebServiceActive = 1) AND tblmediacampaigns.CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub

    Private Sub checkAuthoritySimple(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 WebServiceID " & _
                                "FROM tblwebservices " & _
                                "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;' OR WebServiceIPAddress LIKE '%xxx.xxx.xxx.xxx%;') " & _
                                " AND (WebServiceActive = 1) AND CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = True ' False - temporarily allow all posts
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -4, "Not Authorised - " & ip)
            Response.Write("Not Authorised - " & ip)
            Response.End()
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID", "Amount", "AddressPostCode"}
        For i As Integer = 0 To UBound(arrMandatory)
            If (Not checkValue(Request(arrMandatory(i)))) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -3, "Mandatory Data Missing [" & strMandatoryField & "]")
            Response.Write("Mandatory Data Missing [" & strMandatoryField & "]")
            Response.End()
        End If
    End Sub

    Private Sub checkMandatorySimple()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID"}
        For i As Integer = 0 To UBound(arrMandatory)
            If (Not checkValue(Request(arrMandatory(i)))) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -3, "Mandatory Data Missing [" & strMandatoryField & "]")
            Response.Write("Mandatory Data Missing [" & strMandatoryField & "]")
            Response.End()
        End If
    End Sub

    Private Sub readData()
        For Each Item In Request.QueryString
			If (checkValue(Item)) Then
            	Call validateData(Item, Request.QueryString(Item))
			End If
        Next
        For Each Item In Request.Form
			If (checkValue(Item)) Then
            	Call validateData(Item, Request.Form(Item))
			End If
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = "", boolDataGrid As Boolean = False
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
				strFunctionParameters = Row.Item("ValFunctionParameters").ToString
                boolDataGrid = Row.Item("ApplicationDataGridColumn")
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        val = Replace(val, vbLf, "<br />")

        ' Call regular expression routine
        If (boolDataGrid) Then
            Dim arrVal As Array = Split(val, ",")
            For x As Integer = 0 To UBound(arrVal)
				If (checkValue(strRegex)) Then
					If (regexTest(strRegex, arrVal(x))) Then
						If (checkValue(val) Or checkValue(AppID)) Then
							tbldatastore_flds.Add(strField)
							tbldatastore_vals.Add(formatField(val, "", ""))
						End If
						boolValidate = True
					Else
						boolValidate = False
						Exit For
					End If
				Else
					boolValidate = True
				End If
            Next
        Else
            If (regexTest(strRegex, val)) Then
                If (checkValue(val) Or checkValue(AppID)) Then
                    If (checkValue(strFunction)) Then
                        If (strFunction = "splitApp1FullName") Then
                            splitApp1FullName(val)
                        ElseIf (strFunction = "splitApp2FullName") Then
                            splitApp2FullName(val)
                        Else
                            Dim arrParams As String() = {val}
							If (checkValue(strFunctionParameters)) Then
								Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
								For y As Integer = 0 To UBound(arrFunctionParameters)
									If (checkValue(arrFunctionParameters(y))) Then
										ReDim Preserve arrParams(y + 1)
										arrParams(y + 1) = arrFunctionParameters(y)
									End If
								Next
							End If
							val = executeMethodByName(Me, strFunction, arrParams)
                        End If
                    End If
                    Select Case strTable
                        Case "tblapplications"
                            If (checkValue(AppID)) Then
                                If (fld <> "MediaCampaignID" And strField <> "LoanType") Then
                                    tblapplications_flds.Add(strField)
                                    tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                                End If
                            Else
                                tblapplications_flds.Add(strField)
                                tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                            End If
                        Case "tblapplicationstatus"
                            tblapplicationstatus_flds.Add(strField)
                            tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbladdresses"
                            tbladdresses_flds.Add(strField)
                            tbladdresses_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblemployers"
                            tblemployers_flds.Add(strField)
                            tblemployers_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblnotes"
                            tblnotes_flds.Add(strField)
                            tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblreminders"
                            tblreminders_flds.Add(strField)
                            tblreminders_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tbldatastore"
                            tbldatastore_flds.Add(strField)
                            tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                        Case "tblapplicationstatusdates"
                            tblapplicationstatusdates_flds.Add(strField)
                            tblapplicationstatusdates_vals.Add(val)
                    End Select
                End If
                boolValidate = True
            Else
                boolValidate = False
            End If
        End If

        If (boolValidate = False) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), intSOAPRequestNo, -5, "Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
			If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
				If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),"?") > 0) Then
					responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
				Else
					responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?msg=" & encodeURL("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage))
				End If         
			Else
				Response.Write("Inconsistent data found. [" & fld & "], value = " & val & ", Criteria = " & strMessage)
				Response.End()
			End If
		End If

    End Sub

    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub saveData()

        Dim strResult As String = saveApplication(False, AppID, UserID, CompanyID, MediaCampaignID, "", intSkipTelephoneCorrection, _
                                                    tblapplications_flds, _
                                                    tblapplications_vals, _
                                                    tblapplicationstatus_flds, _
                                                    tblapplicationstatus_vals, _
                                                    tbladdresses_flds, _
                                                    tbladdresses_vals, _
                                                    tblemployers_flds, _
                                                    tblemployers_vals, _
                                                    tblnotes_flds, _
                                                    tblnotes_vals, _
                                                    tblreminders_flds, _
                                                    tblreminders_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals, _
                                                    tblapplicationstatusdates_flds, _
                                                    tblapplicationstatusdates_vals, _
                                                    Nothing, _
                                                    Nothing, _
                                                    Nothing, _
                                                    Nothing, _
                                                    "N")

        Dim arrResult As Array = Split(strResult, "|")
        Dim strStatusCode As String = "", strSubStatusCode As String = ""
        If (UBound(arrResult) >= 1) Then
            saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), arrResult(1), 1, "Application successfully received")
            strStatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", arrResult(1))
            strSubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", arrResult(1))
        End If
        If (UBound(arrResult) >= 1 And strStatusCode = "INV") Then
            Select Case strSubStatusCode
                Case "DUP"
                    If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&duplicate=1")
                        Else
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?duplicate=1")
                        End If
                    Else
                        Response.Write("0|Duplicate+case")
                    End If
                Case "WRN"
                    If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&invalid=1")
                        Else
                            responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?invalid=1")
                        End If
                    Else
                        Response.Write("0|Duplicate+case")
                    End If
            End Select
        Else
            strReturnURL = lookupValue(HttpContext.Current.Request("OfferID"), "Experian", "tbloffers")
            If (checkValue(strReturnURL)) Then
                If (InStr(strReturnURL, "?") > 0) Then
                    If (UBound(arrResult) = 1) Then
                        responseRedirect(strReturnURL & "&AppID=" & arrResult(1))
                    Else
                        responseRedirect(strReturnURL)
                    End If
                Else
                    If (UBound(arrResult) = 1) Then
                        responseRedirect(strReturnURL & "?AppID=" & arrResult(1))
                    Else
                        responseRedirect(strReturnURL)
                    End If
                End If
            Else
                Response.Write(strResult)
            End If
        End If
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strFormPost As String = ""
        Dim x As Integer = 0
        For Each Item In Request.Form
			If (checkValue(Item)) Then
				If (x > 0) Then
					strFormPost += "&" & Item & "=" & Request.Form(Item)
				Else
					strFormPost += Item & "=" & Request.Form(Item)
				End If
				x += 1
			End If
        Next
        For Each Item In Request.QueryString
			If (checkValue(Item)) Then
				If (checkValue(strFormPost)) Then
					strFormPost += "&" & Item & "=" & Request.QueryString(Item)
				Else
					strFormPost += Item & "=" & Request.QueryString(Item)
				End If
			End If
        Next
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(strFormPost, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub splitApp1FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 1)
    End Sub

    Private Sub splitApp2FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 2)
    End Sub

    Private Sub splitFullName(ByVal strFullName As String, ByVal intAppNo As Integer)
        Dim boolTitleSet As Boolean = False
        Dim strMiddleNames As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                validateData("App" & intAppNo & "Surname", arrFullName(i))
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                validateData("App" & intAppNo & "Title", arrFullName(i))
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        If (strMiddleNames <> "") Then
            validateData("App" & intAppNo & "MiddleNames", strMiddleNames)
        End If
    End Sub

End Class
