using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using EAGetMail; //add EAGetMail namespace
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using HtmlAgilityPack;
using System.Linq;
using System.Text.RegularExpressions;

public partial class Default : Page
{
    string EmailAppID = "";

    public DateTime ReceivedDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Main();

    }

    public void Main()
    {

        string mailbox = "C:\\esfluent";

        // Get all *.eml files in specified folder and parse it one by one.
        string[] files = Directory.GetFiles(mailbox, "*.eml");

        for (int i = 0; i < files.Length; i++)
        {
            ParseEmail(files[i]);
            
        }
       DeleteFiles();
    }

    public void ParseEmail(string emlFile)
    {

        Mail oMail = new Mail("EG-B1374632949-00931-42VE5BBUC5UCBCV6-5FB8D8CFD548A123");
        oMail.Load(emlFile, false);

        ReceivedDate = oMail.ReceivedDate;
        

        // Parse Mail From, Sender

        string textbodystatus = oMail.TextBody.Replace("::",":");
        var vals = textbodystatus.Split(':')[1];
        string status = vals;
        var statuscode = status.Split(' ')[1].Substring(0, 3);

        string textbodyappid = oMail.TextBody.Replace("::", ":");
        var valsappid = textbodyappid.Split(':')[2];
        string apps = valsappid;
        var appid = apps.Split(' ')[1].Substring(0, 4);
       


        var test = updatestatus(statuscode, appid, ReceivedDate);

    }

    public static string StripHTML(string input)
    {
        return Regex.Replace(input, "\\<[^\\>]*\\>", String.Empty);
    }

    public void DeleteFiles()
    {
        System.IO.DirectoryInfo di = new DirectoryInfo("c:\\esfluent");

        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();
        }

    }

    private static string updatestatus(string status, string AppID, DateTime RecDate)
    {
        
         
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionStringES"].ToString());

        try
        {
            connection.Open();
        }
        catch (Exception e)
        {
            throw new Exception(string.Format("Failed to open connection to server. Message: {0}", e.Message));
        }


        string statusupdate = "";
        try { 
        switch (status)
        {
            case "SOL":
                SqlCommand updatestatussol = new SqlCommand();
                updatestatussol.Connection = connection;
                updatestatussol.CommandText = string.Format("update tblapplications set PartnerSold = '1' where AppID = '" + AppID + "'");
                updatestatussol.ExecuteNonQuery();
				
				SqlCommand updatestatussol1 = new SqlCommand();
                updatestatussol1.Connection = connection;
                updatestatussol1.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatussol1.ExecuteNonQuery();
				
                return statusupdate = "SOL";
            case "DUP":
                SqlCommand updatestatusparstatus = new SqlCommand();
                updatestatusparstatus.Connection = connection;
                updatestatusparstatus.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatus.ExecuteNonQuery();

                decimal Min = 0;

                     try
                     {
                        SqlConnection connection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["crm.live.ConnectionString"].ToString());

                        SqlCommand myCommand = new SqlCommand(string.Format("SELECT DATEDIFF(mi, CreatedDate, '" + System.DateTime.Now + "') as minutes from tblapplicationstatus where AppID = '" + AppID + "'"), connection1);

                        var reader2 = myCommand.ExecuteReader();
                        
                         while (reader2.Read()) {
                                decimal.TryParse(reader2["minutes"].ToString(), out Min);

                             if (Min <= 10)
                             {
                                 SqlCommand UpdateStatusFRE = new SqlCommand();
                                 UpdateStatusFRE.Connection = connection;
                                 UpdateStatusFRE.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FRE' where AppID = '" + AppID + "'");
                                 UpdateStatusFRE.ExecuteNonQuery();

                             }
                             else
                             {
                                 SqlCommand updatestatusdup = new SqlCommand();
                                 updatestatusdup.Connection = connection;
                                 updatestatusdup.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                                 updatestatusdup.ExecuteNonQuery();
                             }
                         }
                     }
                     catch (Exception e)
                     {
                         SqlCommand updatestatusdup = new SqlCommand();
                         updatestatusdup.Connection = connection;
                         updatestatusdup.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                         updatestatusdup.ExecuteNonQuery();
                     }
                 return statusupdate = "DUP";
            case "CAN":
                SqlCommand updatestatusparstatuscan = new SqlCommand();
                updatestatusparstatuscan.Connection = connection;
                updatestatusparstatuscan.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatuscan.ExecuteNonQuery();

                SqlCommand updatestatuscan = new SqlCommand();
                updatestatuscan.Connection = connection;
                updatestatuscan.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                updatestatuscan.ExecuteNonQuery();
                return statusupdate = "CAN";
            case "DEC":
                SqlCommand updatestatusparstatusdec = new SqlCommand();
                updatestatusparstatusdec.Connection = connection;
                updatestatusparstatusdec.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusdec.ExecuteNonQuery();

                SqlCommand updatestatusdec = new SqlCommand();
                updatestatusdec.Connection = connection;
                updatestatusdec.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                updatestatusdec.ExecuteNonQuery();
                return statusupdate = "DEC";
            case "MTG":
                SqlCommand updatestatusparstatusmtg = new SqlCommand();
                updatestatusparstatusmtg.Connection = connection;
                updatestatusparstatusmtg.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusmtg.ExecuteNonQuery();

                SqlCommand updatestatusmtg = new SqlCommand();
                updatestatusmtg.Connection = connection;
                updatestatusmtg.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                updatestatusmtg.ExecuteNonQuery();
                return statusupdate = "MTG";
            case "NOC":
                SqlCommand updatestatusparstatusnoc = new SqlCommand();
                updatestatusparstatusnoc.Connection = connection;
                updatestatusparstatusnoc.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusnoc.ExecuteNonQuery();

                SqlCommand updatestatusnoc = new SqlCommand();
                updatestatusnoc.Connection = connection;
                updatestatusnoc.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                updatestatusnoc.ExecuteNonQuery();
                return statusupdate = "NOC";
            case "NPW":
                SqlCommand updatestatusparstatusnpw = new SqlCommand();
                updatestatusparstatusnpw.Connection = connection;
                updatestatusparstatusnpw.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusnpw.ExecuteNonQuery();

                SqlCommand updatestatusnpw = new SqlCommand();
                updatestatusnpw.Connection = connection;
                updatestatusnpw.CommandText = string.Format("update tblapplicationstatus set StatusCode = 'FLU' where AppID = '" + AppID + "'");
                updatestatusnpw.ExecuteNonQuery();
                return statusupdate = "NPW";
            case "UNS":
                SqlCommand updatestatusparstatusuns = new SqlCommand();
                updatestatusparstatusuns.Connection = connection;
                updatestatusparstatusuns.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusuns.ExecuteNonQuery();
                return statusupdate = "UNS";
            case "SEA":
                SqlCommand updatestatusparstatussea = new SqlCommand();
                updatestatusparstatussea.Connection = connection;
                updatestatusparstatussea.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatussea.ExecuteNonQuery();
                return statusupdate = "SEA";
            case "ISS":
                SqlCommand updatestatusparstatusiss = new SqlCommand();
                updatestatusparstatusiss.Connection = connection;
                updatestatusparstatusiss.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusiss.ExecuteNonQuery();
                return statusupdate = "ISS";
            case "RES":
                SqlCommand updatestatusparstatusres = new SqlCommand();
                updatestatusparstatusres.Connection = connection;
                updatestatusparstatusres.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusres.ExecuteNonQuery();
                return statusupdate = "RES";
            case "REF":
                SqlCommand updatestatusparstatusref = new SqlCommand();
                updatestatusparstatusref.Connection = connection;
                updatestatusparstatusref.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatusref.ExecuteNonQuery();
                return statusupdate = "REF";
            case "LEN":
                SqlCommand updatestatusparstatuslen = new SqlCommand();
                updatestatusparstatuslen.Connection = connection;
                updatestatusparstatuslen.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatuslen.ExecuteNonQuery();
                return statusupdate = "LEN";
            case "COM":
                SqlCommand updatestatusparstatuscom = new SqlCommand();
                updatestatusparstatuscom.Connection = connection;
                updatestatusparstatuscom.CommandText = string.Format("update tblapplications set PartnerStatus = '" + status + "' where AppID = '" + AppID + "'");
                updatestatusparstatuscom.ExecuteNonQuery();
                return statusupdate = "COM";

        }

        }
        catch (Exception ep)
        {
            HttpContext.Current.Response.Write(ep.Message);


        }
        return statusupdate;
    }

}


