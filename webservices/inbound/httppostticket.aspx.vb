﻿Imports Config, Common
Imports System.Net

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Private AppID As String = ""
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strReturnURL As String = HttpContext.Current.Request("strReturnURL")
    Private SystemTitle As String = HttpContext.Current.Request("SystemTitle")
	Private SystemDescription As String = HttpContext.Current.Request("SystemDescription")
	Private ChangeCurrentProcess As String = HttpContext.Current.Request("ChangeCurrentProcess")
	Private ChangeRevisedProcess As String = HttpContext.Current.Request("ChangeRevisedProcess")
	Private SystemCategory As String = HttpContext.Current.Request("SystemCategory")
    Private SystemPriority As String = HttpContext.Current.Request("SystemPriority")
    Private App1EmailAddress As String = HttpContext.Current.Request("App1EmailAddress")
    Private Username As String = HttpContext.Current.Request("DefaultUsername")
    Private DevRate As String = HttpContext.Current.Request("BusinessDevRate")
	
	
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
        'httpPost()
    End Sub

    Public Sub httpPost()
		
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If

        Dim strPost As String = "", x As Integer = 0

        strPost = String.Format("MediaCampaignID={0}&SystemTitle={1}&SystemDescription={2}&ChangeCurrentProcess={3}&ChangeRevisedProcess={4}&SystemCategory={5}&SystemPriority={6}&App1EmailAddress={7}&ChangeContact={8}&BusinessDevRate={9}", MediaCampaignID, SystemTitle, SystemDescription, ChangeCurrentProcess, ChangeRevisedProcess, SystemCategory, SystemPriority, App1EmailAddress, Username, DevRate)
        'strPost = "?MediaCampaignID=12258"
        'strPost = "?MediaCampaignID=" & MediaCampaignID
        'strPost += "&SystemTitle=" & SystemTitle
        'strPost += "&SystemDescription=" & SystemDescription
        'strPost += "&ChangeCurrentProcess=" & ChangeCurrentProcess
        'strPost += "&ChangeRevisedProcess=" & ChangeRevisedProcess
        'strPost += "&SystemCategory=" & SystemCategory
        'strPost += "&SystemPriority=" & SystemPriority

        If (checkValue(strPost)) Then
            Dim objResponse As HttpWebResponse = postWebRequest("http://projects.engaged-solutions.co.uk/webservices/inbound/httppost.aspx", strPost)

            If (objResponse.StatusCode = HttpStatusCode.OK) Then
                If (checkResponse(objResponse, "")) Then
                    Dim objReader As New StreamReader(objResponse.GetResponseStream())
                    Dim strResult As String = objReader.ReadToEnd()

                    objReader.Close()
                    objReader = Nothing
                    Dim arrResult As Array = Split(strResult, "|")

                    If (checkValue(strReturnURL)) Then
                        Response.Redirect(String.Format("{0}&strMessage=Request submitted", strReturnURL))
                    End If

                End If
                If (checkResponse(objResponse, "")) Then
                    objResponse.Close()
                End If
                objResponse = Nothing
            End If
        Else
            Response.Write("0|No+post+data")
        End If
    End Sub

    'Private Sub TESTRUN()
    '    Dim s As HttpWebRequest
    '    Dim enc As UTF8Encoding
    '    Dim postdata As String
    '    Dim postdatabytes As Byte()
    '    s = HttpWebRequest.Create("http://projects.engaged-solutions.co.uk/webservices/inbound/httppost.aspx")
    '    enc = New System.Text.UTF8Encoding()
    '    postdata = "?MediaCampaignID=12258&SystemTitle=hghgh&SystemDescription=ghghg&ChangeCurrentProcess=ghghg&ChangeRevisedProcess=ghghgh&SystemCategory=Administration&SystemPriority=HighMandatory"
    '    postdatabytes = enc.GetBytes(postdata)
    '    s.Method = "POST"
    '    s.ContentType = "application/x-www-form-urlencoded"
    '    s.ContentLength = postdatabytes.Length


    '    Using stream = s.GetRequestStream()
    '        stream.Write(postdatabytes, 0, postdatabytes.Length)
    '    End Using


    '    Dim result = s.GetResponse()
    '    Dim objReader As New StreamReader(result.GetResponseStream())
    '    Dim strResult As String = objReader.ReadToEnd()


    '    Response.Write(strResult)
    'End Sub

End Class
