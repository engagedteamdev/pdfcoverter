﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

' ** Revision history **
'
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class SOAP
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Previous Addresses
    Private tbladdresses_flds As ArrayList = New ArrayList, tbladdresses_vals As ArrayList = New ArrayList
    ' Previous Employers
    Private tblemployers_flds As ArrayList = New ArrayList, tblemployers_vals As ArrayList = New ArrayList
    ' Notes
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList
    ' Reminders
    Private tblreminders_flds As ArrayList = New ArrayList, tblreminders_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    ' Milestones
    Private tblapplicationstatusdates_flds As ArrayList = New ArrayList, tblapplicationstatusdates_vals As ArrayList = New ArrayList
    ' Diaries
    Private tbldiaries_flds As ArrayList = New ArrayList, tbldiaries_vals As ArrayList = New ArrayList
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = ""
    Private UserID As String = ""
    Private MediaCampaignID As String = "", MediaCampaignIDOutbound As String = ""
    Private blnSkipValidation As Boolean = False, intSkipTelephoneCorrection As String = "0"
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private CompanyID As String = "0"

    Public Sub SOAPPost()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//executeSoap")
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = executeSoap", -2, intSOAPRequestNo)
        End If
        Dim objSkipValidation As XmlNode = objInputXMLDoc.SelectSingleNode("//SkipValidation")
        If (Not objSkipValidation Is Nothing) Then
            If (objSkipValidation.InnerText = "Y") Then
                blnSkipValidation = True
            End If
        End If
        Dim objblnSkipTelephoneCorrection As XmlNode = objInputXMLDoc.SelectSingleNode("//SkipTelephoneCorrection")
        If (Not objblnSkipTelephoneCorrection Is Nothing) Then
            If (objblnSkipTelephoneCorrection.InnerText = "Y") Then
                intSkipTelephoneCorrection = "1"
            End If
        End If
        Dim objMediaCampaignID As XmlNode = objInputXMLDoc.SelectSingleNode("//MediaCampaignID")
        If (Not objMediaCampaignID Is Nothing) Then
            MediaCampaignID = objMediaCampaignID.InnerText
        End If
        Dim objMediaCampaignIDOutbound As XmlNode = objInputXMLDoc.SelectSingleNode("//MediaCampaignIDOutbound")
        If (Not objMediaCampaignIDOutbound Is Nothing) Then
            MediaCampaignIDOutbound = objMediaCampaignIDOutbound.InnerText
        End If
        Dim objUserID As XmlNode = objInputXMLDoc.SelectSingleNode("//UserID")
        If (Not objUserID Is Nothing) Then
            UserID = objUserID.InnerText
        End If
        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        Call checkMandatory()
        Call readData()
        Call saveData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
              "FROM tblmediacampaigns INNER JOIN " & _
              "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
              "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
              "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR '" & ip & "' = '127.0.0.1') " & _
              "AND (ISNULL(WebServiceActive,1) = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    'Private Sub checkAuthoritySimple(ByVal ip As String)
    '    Dim boolAuthorised As Boolean
    '    Dim strSQL As String = "SELECT TOP 1 CompanyID " & _
    '                            "FROM tblwebservices " & _
    '                            "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;') " & _
    '                            " AND (WebServiceActive = 1) "
    '    Dim objDataBase As New DatabaseManager
    '    Dim objResult As Object = objDataBase.executeScalar(strSQL)
    '    If (objResult IsNot Nothing) Then
    '        CompanyID = objResult.ToString
    '        boolAuthorised = True
    '    Else
    '        boolAuthorised = False
    '    End If
    '    objResult = Nothing
    '    objDataBase = Nothing
    '    If (Not boolAuthorised) Then
    '        Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
    '    End If
    'End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID", "ProductType", "Amount", "App1HomeTelephone", "AddressLine1", "AddressPostCode"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            Call SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo)
        End If
    End Sub

    Private Sub readData()
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.DocumentElement.SelectSingleNode("//executeSoap").ChildNodes
        For Each child As XmlNode In objChildNodes
            validateData(child.Name, child.InnerText)
        Next
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                strFunctionParameters = Row.Item("ValFunctionParameters").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        ' Call regular expression routine
        If (regexTest(strRegex, val) Or blnSkipValidation) Then
            If (checkValue(val)) Then
                If (checkValue(strFunction)) Then
                    If (strFunction = "splitApp1FullName") Then
                        splitApp1FullName(val)
                    ElseIf (strFunction = "splitApp2FullName") Then
                        splitApp2FullName(val)
                    Else
                        Dim arrParams As String() = {val}
                        If (checkValue(strFunctionParameters)) Then
                            Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                            For y As Integer = 0 To UBound(arrFunctionParameters)
                                If (checkValue(arrFunctionParameters(y))) Then
                                    ReDim Preserve arrParams(y + 1)
                                    arrParams(y + 1) = arrFunctionParameters(y)
                                End If
                            Next
                        End If
                        val = executeMethodByName(Me, strFunction, arrParams)
                    End If
                End If
                Select Case strTable
                    Case "tblapplications"
                        tblapplications_flds.Add(strField)
                        tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatus"
                        tblapplicationstatus_flds.Add(strField)
                        tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbladdresses"
                        tbladdresses_flds.Add(strField)
                        tbladdresses_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblemployers"
                        tblemployers_flds.Add(strField)
                        tblemployers_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblnotes"
                        tblnotes_flds.Add(strField)
                        tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblreminders"
                        tblreminders_flds.Add(strField)
                        tblreminders_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbldatastore"
                        tbldatastore_flds.Add(strField)
                        tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatusdates"
                        tblapplicationstatusdates_flds.Add(strField)
                        tblapplicationstatusdates_vals.Add(val)
                    Case "tbldiaries"
                        tbldiaries_flds.Add(strField)
                        tbldiaries_vals.Add(formatField(val, strFieldType, strFieldDefault))
                End Select
            End If
            boolValidate = True
        Else
            boolValidate = False
        End If

        If (boolValidate = False) Then
            Call SOAPUnsuccessfulMessage("Inconsistent data found. Field = " & fld & ", value = " & val & ", Criteria = " & strMessage, -5, intSOAPRequestNo)
        End If

    End Sub

    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub saveData()

        AppID = saveApplication(True, "", UserID, CompanyID, MediaCampaignID, MediaCampaignIDOutbound, intSkipTelephoneCorrection, _
                                                    tblapplications_flds, _
                                                    tblapplications_vals, _
                                                    tblapplicationstatus_flds, _
                                                    tblapplicationstatus_vals, _
                                                    tbladdresses_flds, _
                                                    tbladdresses_vals, _
                                                    tblemployers_flds, _
                                                    tblemployers_vals, _
                                                    tblnotes_flds, _
                                                    tblnotes_vals, _
                                                    tblreminders_flds, _
                                                    tblreminders_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals, _
                                                    tblapplicationstatusdates_flds, _
                                                    tblapplicationstatusdates_vals, _
                                                    tbldiaries_flds, _
                                                    tbldiaries_vals, _
                                                    Nothing, _
                                                    Nothing, _
                                                    "N")

        Dim strStatusCode As String = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
        Dim strSubStatusCode As String = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
        If (strStatusCode = "INV") Then
            Select Case strSubStatusCode
                Case "DUP"
                    Call SOAPUnsuccessfulMessage("Duplicate case", -5, intSOAPRequestNo)
                Case "WRN"
                    Call SOAPUnsuccessfulMessage("Invalid telephone number", -5, intSOAPRequestNo)
            End Select
        Else
            Call SOAPSuccessfulMessage("Application successfully received", 1, AppID)
        End If

    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>" & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">")
            .Append("<soap:Body>")
            .Append("<executeSoapResponse xmlns:lead=""http://crm.engaged-solutions.co.uk/"">")
            .Append("<executeSoapResult>Invalid XML received: " & msg & "</executeSoapResult>")
            .Append("<executeSoapStatus>" & st & "</executeSoapStatus>")
            .Append("<executeSoapNo>" & app & "</executeSoapNo>")
            .Append("</executeSoapResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub splitApp1FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 1)
    End Sub

    Private Sub splitApp2FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 2)
    End Sub

    Private Sub splitFullName(ByVal strFullName As String, ByVal intAppNo As Integer)
        Dim boolTitleSet As Boolean = False
        Dim strMiddleNames As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                validateData("App" & intAppNo & "Surname", arrFullName(i))
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                validateData("App" & intAppNo & "Title", arrFullName(i))
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                validateData("App" & intAppNo & "FirstName", arrFullName(i))
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        If (strMiddleNames <> "") Then
            validateData("App" & intAppNo & "MiddleNames", strMiddleNames)
        End If
    End Sub

End Class
