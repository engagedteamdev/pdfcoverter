﻿Imports Config
Imports Common
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Xml.XPath

Partial Class SOAP
    Inherits System.Web.UI.Page

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    Private intSOAPRequestNo As String = getIndexNumber()
    Private AppID As String = ""
    Private objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager = Nothing
    Private strMediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")

    Public Sub SOAPPost()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Try
            objInputXMLDoc.Load(Request.InputStream)
            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
            objNSM.AddNamespace("ns0", "http://www.paaleads.com/LeadConsumerService/")
        Catch e As System.Xml.XmlException
            Call SOAPUnsuccessfulMessage("Error in XML file", -1, intSOAPRequestNo)
        End Try

        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//ns0:ConsumeLead/ns0:request/ns0:LeadXml", objNSM)
        If (objHeaderTest Is Nothing) Then
            Call SOAPUnsuccessfulMessage("Data field not found. Field = ConsumeLead/LeadXml", -2, intSOAPRequestNo)
        Else
            If (objHeaderTest.FirstChild.NodeType = XmlNodeType.CDATA) Then
                Dim objCDATA As XmlCDataSection = objHeaderTest.FirstChild
                objInputXMLDoc.LoadXml(objCDATA.Value)
                objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                'objNSM.AddNamespace("ns1", "http://PAA.LeadDelivery.Generic.Schemas.LeadAssigned/")
                objNSM.AddNamespace("ns0", "http://PAA.LeadDelivery.Generic.Schemas.Lead")
            End If
        End If

        'HttpContext.Current.Response.ContentType = "text/xml"
        'HttpContext.Current.Response.Write(objInputXMLDoc.InnerXml)
        'HttpContext.Current.Response.End()

        'Dim objMortgageValueUpperBound As XmlNode = objInputXMLDoc.SelectSingleNode("//ns1:LeadAssigned/ns0:Lead/ExtendedInfo/Mortgage/MortgageValueUpperBound", objNSManager)
        'If (Not objMortgageValueUpperBound Is Nothing) Then
        '    strMortgageValueUpperBound = objMortgageValueUpperBound.InnerText
        'End If

        'Dim objMortgageValue As XmlNode = objInputXMLDoc.SelectSingleNode("//ns1:LeadAssigned/ns0:Lead/ExtendedInfo/Mortgage/MortgageValue", objNSManager)
        'If (Not objMortgageValue Is Nothing) Then
        '    If Not IsNumeric(objMortgageValue.InnerText) Then
        '        objMortgageValue.InnerText = strMortgageValueUpperBound
        '    End If
        '    strAmount = objMortgageValue.InnerText
        'End If

        'Dim objPropertyValueUpperBound As XmlNode = objInputXMLDoc.SelectSingleNode("//ns1:LeadAssigned/ns0:Lead/ExtendedInfo/Mortgage/PropertyValueUpperBound", objNSManager)
        'If (Not objPropertyValueUpperBound Is Nothing) Then
        '    strPropertyValueUpperBound = objPropertyValueUpperBound.InnerText
        'End If

        'Dim objPropertyValue As XmlNode = objInputXMLDoc.SelectSingleNode("//ns1:LeadAssigned/ns0:Lead/ExtendedInfo/Mortgage/PropertyValue", objNSManager)
        'If (Not objPropertyValue Is Nothing) Then
        '    If Not IsNumeric(objPropertyValue.InnerText) Then
        '        objPropertyValue.InnerText = strPropertyValueUpperBound
        '    End If
        'End If

        Call checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        Call checkMandatory()
        Call readData()
        Call writeData()
    End Sub

    Private Sub checkAuthority(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblwebservices.WebServiceID " & _
                               "FROM tblmediacampaigns INNER JOIN " & _
                               "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID INNER JOIN " & _
                               "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                               "WHERE (tblmediacampaigns.MediaCampaignID = '" & strMediaCampaignID & "') AND (tblwebservices.WebServiceIPAddress LIKE '%" & ip & "%;' OR '" & ip & "' = '109.234.207.186') " & _
                               "AND (WebServiceActive = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkAuthoritySimple(ByVal ip As String)
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 CompanyID " & _
                                "FROM tblwebservices " & _
                                "WHERE (WebServiceIPAddress LIKE '%" & ip & "%;') " & _
                                " AND (WebServiceActive = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (Not boolAuthorised) Then
            Call SOAPUnsuccessfulMessage("Not Authorised - " & ip, -4, intSOAPRequestNo)
        End If
    End Sub

    Private Sub checkMandatory()
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"Forename", "Surname", "TelephoneNumber/Number", "Address/Street", "Address/Postcode"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            Call SOAPUnsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, intSOAPRequestNo)
        End If
    End Sub

    Private Sub readData()
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.SelectSingleNode("//ns0:Lead", objNSM).ChildNodes
        For Each child As XmlNode In objChildNodes
            For Each child2 As XmlNode In child.ChildNodes
                If (child2.ChildNodes.Count > 1) Then
                    For Each child3 As XmlNode In child2.ChildNodes
                        If (child3.ChildNodes.Count >= 1) Then
                            For Each child4 As XmlNode In child3.ChildNodes
                                If (child4.ChildNodes.Count > 1) Then
                                    For Each child5 As XmlNode In child4.ChildNodes
                                        If (child4.Name = "TelephoneNumber" And child5.Name = "Type") Then
                                            If (child5.InnerText = "Home") Then
                                                'validateData(child2.Name & "/" & child3.Name & "/" & child4.Name & "/HomeNumber", child5.PreviousSibling.InnerText)
                                            ElseIf (child5.InnerText = "Mobile") Then
                                                validateData(child2.Name & "/" & child3.Name & "/" & child4.Name & "/MobileNumber", child5.PreviousSibling.InnerText)
                                            ElseIf (child5.InnerText = "Notification") Then
                                                validateData(child2.Name & "/" & child3.Name & "/" & child4.Name & "/HomeNumber", child5.PreviousSibling.InnerText)
                                            End If
                                        Else
                                            validateData(child2.Name & "/" & child3.Name & "/" & child4.Name & "/" & child5.Name, child5.InnerText)
                                        End If
                                        'Response.Write(child2.Name & "/" & child3.Name & "/" & child4.Name & "/" & child5.Name & "=" & child5.InnerText & "<br>")
                                    Next
                                Else
                                    If (child2.Name = "Applicant1" And child3.Name = "Smoker") Then
                                        If (child4.Value = "true") Then
                                            validateData(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", ""), "Y")
                                        Else
                                            validateData(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", ""), "N")
                                        End If
                                    ElseIf (child2.Name = "Applicant2" And child3.Name = "Smoker") Then
                                        If (child3.Value = "true") Then
                                            validateData(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", ""), "Y")
                                        Else
                                            validateData(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", ""), "N")
                                        End If
                                    Else
                                        validateData(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", ""), child4.InnerText)
                                    End If
                                    'Response.Write(Replace(child2.Name & "/" & child3.Name & "/" & child4.Name, "/#text", "") & "=" & child4.InnerText & "<br>")
                                End If
                            Next
                        Else
                            validateData(child2.Name & "/" & child3.Name, child3.InnerText)
                            'Response.Write(child2.Name & "/" & child3.Name & "=" & child3.InnerText & "<br>")
                        End If
                    Next
                Else
                    validateData(Replace(child.Name & "/" & child2.Name, "/#text", ""), child2.InnerText)
                    'Response.Write(Replace(child.Name & "/" & child2.Name, "/#text", "") & "=" & child2.InnerText & "<br>")
                End If
            Next
        Next
        objChildNodes = objInputXMLDoc.DocumentElement.SelectSingleNode("//Assignment", objNSM).ChildNodes
        For Each child As XmlNode In objChildNodes
            For Each child2 As XmlNode In child.ChildNodes              
                validateData(Replace(child.Name & "/" & child2.Name, "/#text", ""), child2.InnerText)
                'Response.Write(Replace(child.Name & "/" & child2.Name, "/#text", "") & "=" & child2.InnerText & "<br>")
            Next
        Next
        'Response.End()
    End Sub

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Sub validateData(ByVal fld As String, ByVal val As String)
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex")
                strMessage = Row.Item("ValRegexMessage")
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                strFunctionParameters = Row.Item("ValFunctionParameters").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        ' Call regular expression routine
        If (regexTest(strRegex, val)) Then
            If (checkValue(val)) Then
                If (checkValue(strFunction)) Then
                    Dim arrParams As String() = {val}
                    If (checkValue(strFunctionParameters)) Then
                        Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                        For y As Integer = 0 To UBound(arrFunctionParameters)
                            If (checkValue(arrFunctionParameters(y))) Then
                                ReDim Preserve arrParams(y + 1)
                                arrParams(y + 1) = arrFunctionParameters(y)
                            End If
                        Next
                    End If
                    val = executeMethodByName(Me, strFunction, arrParams)
                End If
                Select Case strTable
                    Case "tblapplications"
                        tblapplications_flds.Add(strField)
                        tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatus"
                        tblapplicationstatus_flds.Add(strField)
                        tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbldatastore"
                        tbldatastore_flds.Add(strField)
                        tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                End Select
            End If
            boolValidate = True
        Else
            boolValidate = False
        End If

        If (boolValidate = False) Then
            Call SOAPUnsuccessfulMessage("Inconsistent data found. Field = " & fld & ", value = " & val & ", Criteria = " & strMessage, -5, intSOAPRequestNo)
        End If

    End Sub
    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Sub writeData()

        Dim boolWrite As Boolean = True
        Dim strQryFields As String = "", strQryValues As String = "", strQryFieldsValues As String = ""

        ' Only start insert if main application array is found
        If (tblapplications_flds.Count > 0) Then

            For i = 0 To tblapplications_flds.Count - 1
                If (i = 0) Then
                    strQryFields = "INSERT INTO tblapplications (CompanyID, MediaCampaignIDInbound, ProductType, "
                    strQryValues = "VALUES (" & formatField(CompanyID, "N", 0) & ", " & formatField(strMediaCampaignID, "N", 0) & ", " & formatField(getAnyField("MediaCampaignProductType", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID), "", "Mortgage - REM") & ", "
                End If

                strQryFields = strQryFields & tblapplications_flds(i)
                strQryValues = strQryValues & tblapplications_vals(i)

                If (i <> tblapplications_flds.Count - 1) Then
                    strQryFields = strQryFields & ", "
                    strQryValues = strQryValues & ", "
                Else
                    strQryFields = strQryFields & ") "
                    strQryValues = strQryValues & ") "
                End If
            Next

            AppID = executeIdentityQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            ' Insert default starter values in the application status table
            strQryFields = "INSERT INTO tblapplicationstatus (CompanyID, AppID, CreatedUserID) "
            strQryValues = "VALUES (" & formatField(CompanyID, "N", 0) & "," & formatField(AppID, "N", 0) & ", " & formatField(returnSystemUser(CompanyID), "N", 0) & ") "

            'Response.Write(strQryFields & strQryValues & "<br />")
            Call executeNonQuery(strQryFields & strQryValues)

            strQryFields = ""
            strQryValues = ""

            If (tblapplicationstatus_flds.Count > 0) Then
                For i = 0 To tblapplicationstatus_flds.Count - 1
                    If (i = 0) Then
                        strQryFieldsValues = "UPDATE tblapplicationstatus SET "
                    End If

                    strQryFieldsValues = strQryFieldsValues & tblapplicationstatus_flds(i) & " = " & tblapplicationstatus_vals(i)

                    If (i <> tblapplicationstatus_flds.Count - 1) Then
                        strQryFieldsValues = strQryFieldsValues & ", "
                    Else
                        strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' "
                    End If
                Next
                'Response.Write(strQryFieldsValues & "<br />")
                Call executeNonQuery(strQryFieldsValues)
                strQryFieldsValues = ""
            End If

            ' Insert stored data
            If (tbldatastore_flds.Count > 0) Then
                For i As Integer = 0 To tbldatastore_flds.Count - 1
                    strQryFieldsValues = "INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & tbldatastore_flds(i) & "'," & tbldatastore_vals(i) & ")"
                    Call executeNonQuery(strQryFieldsValues)
                Next
            End If

            executeNonQuery("UPDATE tblapplications SET MediaCampaignCostInbound = '" & getAnyField("MediaCampaignCostInbound", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID) & "' WHERE MediaCampaignCostInbound = 0 AND AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")

            'Set working hour
            If (checkWorkingHour(Config.DefaultDateTime)) Then
                executeNonQuery("UPDATE tblapplicationstatus SET CreatedInsideBusinessHours = 1 WHERE (AppID = '" & AppID & "')")
            End If

            ' Set LTV
            executeNonQuery("EXECUTE spupdateltv @AppID = " & AppID & ", @CompanyID = " & CompanyID)

            ' Correct telephone numbers
            executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)

            ' Set CountryCode
            executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT PostCodeCountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "')), 'ENG') WHERE (AppID = '" & AppID & "')")

            boolWrite = True

        Else

            boolWrite = False

        End If

        If (Not boolWrite) Then
            SOAPUnsuccessfulMessage("An unknown error has occured", -1, intSOAPRequestNo)
        Else
            'liveLeadTransfer()
            Dim strDuplicateChecker As String = getAnyField("MediaCampaignDuplicateChecker", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignID)
            If (strDuplicateChecker) Then
                Call executeNonQuery("EXECUTE spduplicateappcheck @AppID = " & formatField(AppID, "N", 0) & ", @CompanyID = " & CompanyID)
            End If
            SOAPSuccessfulMessage("Application successfully received", 1, AppID)
        End If

    End Sub

    Private Sub SOAPSuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">")
            .Append("<soap:Body>")
            .Append("<ConsumeLeadResponse xmlns=""http://www.paaleads.com/LeadConsumerService/"">")
            .Append("<ConsumeLeadResult>")
            .Append("<LeadReceived>true</LeadReceived>")
            .Append("</ConsumeLeadResult>")
            .Append("</ConsumeLeadResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With

        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub SOAPUnsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">")
            .Append("<soap:Body>")
            .Append("<ConsumeLeadResponse xmlns=""http://www.paaleads.com/LeadConsumerService/"">")
            .Append("<ConsumeLeadResult>")
            .Append("<LeadReceived>false</LeadReceived>")
            .Append("<FailureMessage>" & msg & "</FailureMessage>")
            .Append("</ConsumeLeadResult>")
            .Append("</ConsumeLeadResponse>")
            .Append("</soap:Body>")
            .Append("</soap:Envelope>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Response.Write(objStringBuilder.ToString)
        Response.End()
    End Sub

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub liveLeadTransfer()
        Dim strURL As String = ""
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignID, MediaCampaignCampaignReference, MediaCampaignScheduleID, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress " & _
                            "FROM vwdistributionsearchlive " & _
                            "WHERE (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') " & _
                            "ORDER BY MediaCampaignScheduleRequiredRate DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "vwdistributionsearchlive")
        Dim dsHotkey = objDataSet.Tables("vwdistributionsearchlive")
        If (dsHotkey.Rows.Count > 0) Then
            For Each Row In dsHotkey.Rows
                If (Row.Item("MediaCampaignDeliveryTypeID") = "2") Then ' XML Transfer
                    If (checkValue(Row.Item("MediaCampaignDeliveryXMLPath"))) Then
                        strURL = ApplicationURL & "/webservices/outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignID") & "&MediaCampaignCampaignReference=" & Row.Item("MediaCampaignCampaignReference") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID")
                    End If
                Else ' Email / Email & CSV
                    strURL = ApplicationURL & "/webservices/outbound/endhotkey.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignID") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&MediaCampaignDeliveryEmailAddress=" & Row.Item("MediaCampaignDeliveryEmailAddress") & "&intActionType=" & Row.Item("MediaCampaignDeliveryTypeID")
                End If
                If (checkValue(strURL)) Then
                    getWebRequest(strURL)
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsHotkey = Nothing
        objDataBase = Nothing
    End Sub

End Class
