﻿Imports Config, Common

Partial Class packsrecieved
    Inherits System.Web.UI.Page
	
    Public Sub packsrecieved()
		
      
            Dim strXML As String = ""
            Dim strSQL As String = "select * from vwlivepackoutcases"
           

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<cases>"
                    strXML += "<PacksRecieved>" & Row.Item("Apps") & "</PacksRecieved>"					
                    strXML += "<AdminUser>" & Row.Item("Administration User") & "</AdminUser>"	
					strXML += "<ProductType>" & Row.Item("ProductType") & "</ProductType>"
					strXML += "<StatusCode>" & Row.Item("StatusCode") & "</StatusCode>"
                    strXML += "</cases>"
                Next
                strXML += "</CaseStatus>"
            Else
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<CaseStatus><Error>No leads found</Error></CaseStatus>"
            End If
            dsCache = Nothing
            Response.ContentType = "text/xml"
            Response.Write(strXML)       
    End Sub

End Class