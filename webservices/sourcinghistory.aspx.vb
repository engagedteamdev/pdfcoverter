﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports uk.co.brokertoolbox.www

Partial Class sourcingHistory
    Inherits System.Web.UI.Page

    Public objLeadPlatform As LeadPlatform = New LeadPlatform
    Private intSourceID As String = HttpContext.Current.Request("SourceID"), UserID As String = HttpContext.Current.Request("UserID"), XMLReceivedID As String = HttpContext.Current.Request("XMLReceivedID"), QuoteID As String = HttpContext.Current.Request("QuoteID"), strViewDocuments As String = HttpContext.Current.Request("frmViewDocuments"), strSaveDocuments As String = HttpContext.Current.Request("frmSaveDocuments"), intNoQuotes As String = HttpContext.Current.Request("frmNoQuotes")
    Private boolViewDocuments As Boolean = False, boolSaveDocuments As Boolean = False
    Private intSOAPRequestNo As String = getIndexNumber()
    Private objAuthXMLDoc As XmlDocument = New XmlDocument, objInputXMLDoc As XmlDocument = New XmlDocument, objOutputXMLDoc As XmlDocument = New XmlDocument
    Private strXMLURL As String = "", strXMLSOAPAction As String = "", strXMLFile As String = "", strXMLView As String = "", strApplicationSOAPAction As String = "", strApplicationFile As String = "", strApplicationView As String = "", strDocumentsSOAPAction As String = "", strDocumentsFile As String = "", strXMLNamespace As String = "", intCompanyID As String = "", intSiteID As String = "", strUserID As String = "", strAPIKey As String = ""
    Private intMode As String = HttpContext.Current.Request("intMode"), strRateType As String = HttpContext.Current.Request("frmRateType"), strTerm As String = HttpContext.Current.Request("frmProductTerm"), strLenderCodes As String = HttpContext.Current.Request("frmLenderCodes"), strFeeFree As String = HttpContext.Current.Request("frmFeeFree"), strAmount As String = HttpContext.Current.Request("frmAmount")
    Private strErrorMessage As String = ""
	'Private App1DOB as date = getAnyField("App1DOB", "tblapplications", "AppID", AppID)

	

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
    End Sub
   


    Public Sub getSourcing()

        
		Dim strSQL As String = "SELECT XML from tblsecuredXMLs where sourceID = " & intSourceID & ""
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()  
		Dim strXML As String = ""
		If (dsCache.Rows.Count > 0) Then  
       	For Each row As DataRow In dsCache.Rows
		   strXML  = row.Item("XML")  
		Next
		End IF
		
	   HttpContext.Current.Response.ContentType = "text/xml"
       HttpContext.Current.Response.Write(strXML)
       HttpContext.Current.Response.End()
          
       'HttpContext.Current.Response.Write(strXML)
       'HttpContext.Current.Response.End()
        
    End Sub
    

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String, ByVal xml As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "N", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(xml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    

 

End Class
