<%@ Page Language="vb" AutoEventWireup="false" CodeFile="testwebsocket.aspx.vb" Inherits="ticketDefault" %>

<!DOCTYPE HTML>

<html>
   <head>

	   <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
      
      <script type = "text/javascript">

		  if (typeof websocket === "undefined") {
			  $(document).ready(function () {
				  WebSocketTest2();


				  $("#directCall").click(function (e) {
					  makeCall("907974400840");
				  });
			  });




			  function WebSocketTest2() {
				  try {
					  ws = new WebSocket("wss://engagedcrmapi.etpbx.com/wss");

					  ws.onopen = function () {

						  var loginrequest = { "Command": "login", "ClientExtension": "003709106", "ClientAuthKey": ">YEL~Y*mUbSq2h?U7j<EPHUb&Sks@T$+" };
						  var jsonData = JSON.stringify(loginrequest);
						  console.log(jsonData);
						  ws.send(jsonData);

					  };

					  ws.onmessage = function (evt) {
						  var received_msg = JSON.parse(evt.data);

						   $("#hangupCall").click(function (e) {
								hangupCall(received_msg.HangupChannel);
							});

						  console.log(received_msg);
					  };

					  ws.onclose = function (event) {
						  console.log('Socket is closed. Reconnect will be attempted in 3 second.', event.message);
						  setTimeout(WebSocketTest2, 5000);

					  };

				  } catch (exception) {
					  console.log(exception);
				  }

			  }


			  function makeCall(number) {
				  var makeCallObj = {
					  "Command": "call",
					  "ClientExtension": "003709106",
					  "CallNumber": number,
				  };

				  var makeCallJSON = JSON.stringify(makeCallObj);
				  console.log(makeCallJSON);
				  ws.send(makeCallJSON);
			  }

			  function hangupCall(channel) { // channel is required
				  var hangupCallObj = {
					  "Command": "hangupCall",
					  "ClientExtension": "003709106",
					  "HangupChannel": channel,
				  };

				  var hangupCallJSON = JSON.stringify(hangupCallObj);
				  console.log(hangupCallJSON);
				  ws.send(hangupCallJSON);
			  }


		  }	
      </script>
		
   </head>
   
   <body>
      <div id = "sse">
         <a href = "javascript:WebSocketTest()">Run WebSocket</a>
		  <button id="directCall">Call my mobile</button>
		  <button id="hangupCall">Hangup call</button>
      </div>
      
   </body>
</html>