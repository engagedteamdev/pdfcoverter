﻿Imports Config, Common
Imports Microsoft.VisualBasic

'21/09/2012   -----  Added logic in calendar to round event mins to 30/0 to fit in calendar ----- LTS 

Public Class Calendar

    Private objCache As Cache, dteStartDate As Date, dteEndDate As Date, intStartHour As Double = 0, intEndHour As Double = 0, intInterval As Double = 0, boolMultiBook As Boolean = False
    Private arrCalendar As ArrayList = New ArrayList, arrAppIDs As ArrayList = New ArrayList, arrEventList As ArrayList = New ArrayList, arrEventTimes As ArrayList = New ArrayList, arrEventLengths As ArrayList = New ArrayList, arrEventComplete As ArrayList = New ArrayList

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return dteStartDate
        End Get
        Set(ByVal value As Date)
            dteStartDate = value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return dteEndDate
        End Get
        Set(ByVal value As Date)
            dteEndDate = value
        End Set
    End Property

    Public Property StartHour() As Double
        Get
            Return intStartHour
        End Get
        Set(ByVal value As Double)
            intStartHour = value
        End Set
    End Property

    Public Property EndHour() As Double
        Get
            Return intEndHour
        End Get
        Set(ByVal value As Double)
            intEndHour = value
        End Set
    End Property

    Public Property Interval() As Double
        Get
            Return intInterval
        End Get
        Set(ByVal value As Double)
            intInterval = value
        End Set
    End Property

    Public Property MultiBook() As Boolean
        Get
            Return boolMultiBook
        End Get
        Set(ByVal value As Boolean)
            boolMultiBook = value
        End Set
    End Property

    Public Sub New(ByVal cache As Cache, ByVal startdt As Date, ByVal enddt As Date, ByVal starthr As Double, ByVal endhr As Double, ByVal interv As Double, ByVal weekends As Boolean, Optional multi As Boolean = False)
        If (startdt.DayOfWeek <> DayOfWeek.Sunday) Then
            startdt = startdt.AddDays(-startdt.DayOfWeek)
            enddt = enddt.AddDays(-enddt.DayOfWeek)
        End If
        CacheObject() = cache
        StartDate() = startdt
        EndDate() = enddt
        StartHour() = starthr
        EndHour() = endhr
        Interval() = interv
        MultiBook() = multi

        Dim dteCurrent As Date = StartDate()

        While dteCurrent <= EndDate()
            If (Not weekends) Then
                If (dteCurrent.DayOfWeek <> DayOfWeek.Saturday And dteCurrent.DayOfWeek <> DayOfWeek.Sunday) Then
                    arrCalendar.Add(dteCurrent)
                Else
                    If (dteCurrent = StartDate()) Then
                        StartDate() = StartDate().AddDays(1)
                    End If
                    If (dteCurrent = EndDate()) Then
                        EndDate() = EndDate().AddDays(-1)
                    End If
                End If
                dteCurrent = dteCurrent.AddDays(1)
            Else
                arrCalendar.Add(dteCurrent)
                dteCurrent = dteCurrent.AddDays(1)
            End If
        End While
    End Sub

    Public Function displayCalendar() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">&nbsp;</th>")
            For Each calDay As Date In arrCalendar
                .WriteLine("<th class=""day smlc"" title=""" & calDay & """><div class=""floatLeft strong"">" & calDay.Day & "</div>" & WeekdayName(Weekday(calDay), False, FirstDayOfWeek.Sunday) & "</th>")
            Next
            .WriteLine("</tr"">")
            .WriteLine("<tr>")
            .WriteLine("<th>&nbsp;</th>")
            For Each calDay As Date In arrCalendar
                .WriteLine("<th class=""spacer"">&nbsp;</th>")
            Next
            .WriteLine("</tr"">")
            For x As Double = StartHour() To EndHour() Step Interval()
                .WriteLine("<tr>")
                .WriteLine("<td valign=""top"" class=""smlc time"">" & calcTime(x) & "</td>")
                Dim strClass As String = "", boolDrag As Boolean = True, intEventLength As Boolean = 0
                For Each calDay As Date In arrCalendar
                    strClass = "booking"
                    boolDrag = True
                    If (CDate(calDay & " " & calcTime(x)) < Config.DefaultDateTime) Then
                        strClass = "nobooking"
                        boolDrag = False
                    End If
                    .WriteLine("<td class=""smlc " & strClass & """>")
                    .WriteLine("<div class=""tooltip displayNone"">" & calDay & " " & calcTime(x) & " - Click to make a booking</div>")
                    If (boolDrag) Then
                        .WriteLine("<ul class=""dragBooking editBooking"" title=""" & calDay & " " & calcTime(x) & """>")
                    Else
                        .WriteLine("<ul title=""" & calDay & " " & calcTime(x) & """>")
                    End If

                    .WriteLine(getEvent(calDay & " " & calcTime(x)))

                    .WriteLine("</ul>")
                    '.WriteLine("<div class=""sixty"">")
                    '.WriteLine(getEvent(calDay & " " & calcTime(x + 0.5)))
                    '.WriteLine("</div>")
                    .WriteLine("</td>")
                   
                    intEventLength = getEventLength(calDay & " " & calcTime(x))

                Next
                .WriteLine("</tr>")
            Next
        End With
        Return objStringWriter.ToString
    End Function
	
	 Public Function displayCalendarFull() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">&nbsp;</th>")
            For Each calDay As Date In arrCalendar
                .WriteLine("<th class=""day smlc"" title=""" & calDay & """><div class=""floatLeft strong"">" & calDay.Day & "</div>" & WeekdayName(Weekday(calDay), False, FirstDayOfWeek.Sunday) & "</th>")
            Next
            .WriteLine("</tr"">")
            .WriteLine("<tr>")
            .WriteLine("<th>&nbsp;</th>")
            For Each calDay As Date In arrCalendar
                .WriteLine("<th class=""spacer"">&nbsp;</th>")
            Next
            .WriteLine("</tr"">")
            For x As Double = StartHour() To EndHour() Step Interval()
                .WriteLine("<tr>")
                .WriteLine("<td valign=""top"" class=""smlc time"">" & calcTime(x) & "</td>")
                Dim strClass As String = "", boolDrag As Boolean = True, intEventLength As Boolean = 0
                For Each calDay As Date In arrCalendar
                    strClass = "booking"
                    boolDrag = True
                    If (CDate(calDay & " " & calcTime(x)) < Config.DefaultDateTime) Then
                        strClass = "nobooking"
                        boolDrag = False
                    End If
                    .WriteLine("<td class=""smlc " & strClass & """>")
                   
                    If (boolDrag) Then
                       .WriteLine("<ul title=""" & calDay & " " & calcTime(x) & """>")
                    Else
                        .WriteLine("<ul title=""" & calDay & " " & calcTime(x) & """>")
                    End If


                    .WriteLine(getEvent(calDay & " " & calcTime(x)))


                    .WriteLine("</ul>")
                    '.WriteLine("<div class=""sixty"">")
                    '.WriteLine(getEvent(calDay & " " & calcTime(x + 0.5)))
                    '.WriteLine("</div>")
                    .WriteLine("</td>")
                   
                        intEventLength = getEventLength(calDay & " " & calcTime(x))                   

                Next
                .WriteLine("</tr>")
            Next
        End With
        Return objStringWriter.ToString
    End Function

    Public Sub addEvent(ByVal AppID As String, ByVal eventnm As String, ByVal eventdate As Date, ByVal eventlength As Double, ByVal eventcomplete As Boolean)

        arrAppIDs.Add(AppID)
        arrEventList.Add(eventnm)
        arrEventTimes.Add(eventdate)
        arrEventLengths.Add(eventlength)
        arrEventComplete.Add(eventcomplete)
    End Sub
    

    Private Function getEvent(ByVal calDate As String) As String
        Dim strEvent As String = "", strEventLength As String = "", strEventComplete As String = "", eventcount As Integer = 0

        For x As Integer = 0 To arrEventTimes.Count - 1
            Dim eventDate As Date = CDate(arrEventTimes.Item(x))
            Dim roundedMins As Integer = 0
            Dim Hour As Integer = eventDate.Hour

            If (Interval() = 0.25) Then
                If eventDate.Minute > 0 And eventDate.Minute <= 15 Then
                    roundedMins = 15
                ElseIf eventDate.Minute > 15 And eventDate.Minute <= 30 Then
                    roundedMins = 30
                ElseIf eventDate.Minute > 30 And eventDate.Minute <= 45 Then
                    roundedMins = 45
                ElseIf eventDate.Minute > 45 And eventDate.Minute <= 59 Then
                    roundedMins = 0
                    Hour = Hour + 1
                'Else : roundedMins = 0
                '    Hour = Hour + 1
                End If
            End If
            If (Interval() = 0.5) Then
                If eventDate.Minute >= 0 And eventDate.Minute <= 14 Then
                    roundedMins = 0
                ElseIf eventDate.Minute >= 45 And eventDate.Minute <= 59 Then
                    roundedMins = 0
                    Hour = Hour + 1
                Else : roundedMins = 30
                End If
            End If

            If (Hour = 24) Then Hour = 0

            Dim NewDate As Date = (eventDate.Date & " " & Hour & ":" & roundedMins & ":" & eventDate.Second)

            If (CDate(NewDate) = CDate(calDate)) Then
                strEvent += arrEventList.Item(x) & "|"
                strEventLength += arrEventLengths.Item(x) & "|"
                strEventComplete += arrEventComplete.Item(x) & "|"
            End If

        Next
        Return formatEvent(strEvent, strEventLength, strEventComplete)
    End Function

    Private Function getEventLength(ByVal eventdate As String) As Integer
        Dim intEventLength As Integer = 0
        For x As Integer = 0 To arrEventTimes.Count - 1
            If (arrEventTimes.Item(x) = eventdate) Then
                intEventLength += arrEventLengths.Item(x)
            End If
        Next
        Return intEventLength
    End Function

    Private Function getEventComplete(ByVal eventdate As String) As Boolean
        Dim boolEventComplete As Boolean = False
        For x As Integer = 0 To arrEventTimes.Count - 1
            If (arrEventTimes.Item(x) = eventdate) Then
                boolEventComplete += arrEventComplete.Item(x)
            End If
        Next
        Return boolEventComplete
    End Function

    Private Function formatEvent(ByVal eventnm As String, ByVal eventlength As String, ByVal eventcomplete As String) As String
        Dim arrEvent As Array = Split(eventnm, "|")
        Dim arrEventLength As Array = Split(eventlength, "|")
        Dim arrEventComplete As Array = Split(eventcomplete, "|")
        Dim intWidth As Integer = 0
       ' If (UBound(arrEvent) > 0) Then
'            intWidth = 100 / UBound(arrEvent) - 10
'        Else
            intWidth = 100 - 10
      '  End If

        Dim strEvent As String = "", strComplete As String = ""
        For x As Integer = 0 To UBound(arrEvent)
            If (checkValue(arrEvent(x))) Then
                If (arrEventComplete(x)) Then
                    strComplete = " complete"
                End If
                'If (arrEventLength(x) > Interval()) Then
                'strEvent += "<li><div style=""position: relative""><div class=""event smlc"" style=""position: absolute; float: none; left: " & (x * intWidth) + 10 & "%; height: 180px; width: " & intWidth & "%;"">" & arrEvent(x) & "</div></div></li>"
                'Else
                strEvent += "<li><div class=""event smlc" & strComplete & """ style=""width: " & intWidth & "%;"">" & arrEvent(x) & "<span class=""AppID displayNone"">" & arrAppIDs(x) & "</span></div></li>"
                'End If

                strComplete = ""
            End If
        Next
        Return strEvent
    End Function

    Private Function calcTime(ByVal tm As Double) As String
        If (tm - Fix(tm) > 0) Then
            Return Fix(tm) & ":" & 60 * (tm - Fix(tm))
        Else
            Return tm & ":00"
        End If
    End Function

End Class
