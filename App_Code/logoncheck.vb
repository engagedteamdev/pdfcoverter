﻿Imports Config, Common, CallInterface
Imports Microsoft.VisualBasic

Public Class LogonCheck

    Private objLeadPlatform As LeadPlatform = Nothing

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Sub checkLogon(ByVal UserID As String, Optional ByVal admin As String = "", Optional ByVal unlock As Boolean = True)
        Common.checkBlackList(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), "")
        If (Common.checkRequestSQLInjection(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))) Then
            'HttpContext.Current.Response.End()
        End If
        setCache()
        'setReturnURL()
        setUserID()
        checkLoggedIn()
        checkUserActivity(True)
        logUserActivity()
        checkAuthorised(True)
        If (checkValue(admin)) Then
            checkAdminLevel(admin, True)
        End If
        If (unlock) Then
            If (objLeadPlatform.Config.UserActiveWorkflowActivityID = ActivityType.StartCall) Then
                'If (HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") <> "/default.aspx" And HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") <> "/Default.aspx") Then
                If (Len(objLeadPlatform.Config.UserActiveWorkflowAppID) < 7) Then
                    responseRedirect("/businessprocessing.aspx?BusinessObjectID=" & objLeadPlatform.Config.UserActiveWorkflowAppID & "&strMessageTitle=Call+Resumed&strMessage=" & encodeURL("Case was quit mid call, resuming previous call...") & "&strMessageType=info")

                Else
                    responseRedirect("/processing.aspx?AppID=" & objLeadPlatform.Config.UserActiveWorkflowAppID & "&strMessageTitle=Call+Resumed&strMessage=" & encodeURL("Case was quit mid call, resuming previous call...") & "&strMessageType=info")
                End If
                'End If
            Else
                unlockCases(UserID)
                callLogging("", ActivityType.EndWorkflow, "", Config.DefaultUserID)
                callLogging("", ActivityType.ClearWorkflow, "", Config.DefaultUserID)
            End If
        End If
        If (HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") <> "/default.aspx" And HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") <> "/Default.aspx" And HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") <> "/dashboard/dashboard.aspx") Then
            If (objLeadPlatform.Config.UserOutOfOfficeID <> OutOfOfficeStatus.Ready) Then
                CommonSave.saveOutOfOffice(CompanyID, Config.DefaultUserID, Config.DefaultUserID, OutOfOfficeStatus.Ready)
            End If
        End If
    End Sub

    Public Sub checkLogonSimple(Optional ByVal admin As String = "")
        setCache()
        checkUserActivity(False)
		logUserActivity()
        checkAuthorised(False)
        If (checkValue(admin)) Then
            checkAdminLevel(admin, False)
        End If
    End Sub

    Private Sub setCache()
        '*** Cache Settings***'
        HttpContext.Current.Response.Expires = -1
        HttpContext.Current.Response.ExpiresAbsolute = DateAdd(DateInterval.Day, -1, Now)
        HttpContext.Current.Response.AddHeader("pragma", "no-cache")
        HttpContext.Current.Response.AddHeader("cache-control", "private")
        HttpContext.Current.Response.CacheControl = "no-cache"
    End Sub

    'Private Sub setReturnURL()
    '    Dim strReturnURL As String = ""
    '    Dim arrUrl As Array = Split(HttpContext.Current.Request.Url.ToString, "/net")
    '    strReturnURL = "/net" & arrUrl(1)
    '    Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies("LeadPlatform")
    '    If (objCookie Is Nothing) Then
    '        objCookie = New HttpCookie("LeadPlatform")
    '    End If
    '    objCookie.Domain = Config.CurrentDomain
    '    objCookie.Values("ReturnURL") = Replace(strReturnURL, "&", "+")
    '    HttpContext.Current.Response.Cookies.Add(objCookie)
    'End Sub

    Public Sub setUserID()
        If (Not checkValue(loggedInUserValue("UserID"))) Then
            If (Not checkValue(cookieValue("LeadPlatform", "UserID"))) Then
                Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies("LeadPlatform")
                If (objCookie Is Nothing) Then
                    objCookie = New HttpCookie("LeadPlatform")
                End If
                objCookie.Domain = Config.CurrentDomain
                HttpContext.Current.Response.Cookies.Add(objCookie)
                If (checkValue(HttpContext.Current.Request.QueryString("UserDialerReference"))) Then
                    objCookie.Values("UserID") = getAnyField("UserID", "tblusers", "UserDialerReference", HttpContext.Current.Request.QueryString("UserDialerReference"))
                End If
            End If
        End If
    End Sub

    Public Sub checkLoggedIn()
        If (Not checkValue(loggedInUserValue("UserLoggedIn").ToString)) Then
            HttpContext.Current.Response.Redirect("/logout.aspx")
        End If
    End Sub

    Private Sub checkUserActivity(ByVal redirect As Boolean)

        Dim strSQL As String = "SELECT UserLoggedIn, UserLockedOut, UserLastActivityDate FROM tblusers WHERE UserID = @UserID AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                          New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, Config.DefaultUserID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers", CommandType.Text, arrParams)
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                If (checkValue(Row.Item("UserLastActivityDate"))) Then
                    If (DateDiff("N", Row.Item("UserLastActivityDate"), Now) > 360) Then
                        If (redirect) Then
                            HttpContext.Current.Response.Redirect("/logout.aspx?forward=1")
                        Else
                            HttpContext.Current.Response.End()
                        End If
                    End If
                End If
                If (Not Row.Item("UserLoggedIn") Or Row.Item("UserLockedOut")) Then
                    If (redirect) Then
                        HttpContext.Current.Response.Redirect("/logout.aspx?forward=1")
                    Else
                        HttpContext.Current.Response.End()
                    End If
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsUsers = Nothing
        objDatabase = Nothing
    End Sub

    Public Function checkUserLevel(ByVal strLevel As String) As Boolean
        If checkValue(strLevel) Then
            Dim strSQL As String = "SELECT " & strLevel & " FROM tblusers WHERE " & strLevel & " = 1 AND UserID = @UserID AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                              New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, Config.DefaultUserID)}
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(strSQL, CommandType.Text, arrParams)
            If (objResult IsNot Nothing) Then
                Return True
            Else
                Return False
            End If
            objResult = Nothing
            objDatabase = Nothing
        Else
            Return False
        End If
    End Function

    Private Sub logUserActivity()
        Dim strQry As String = "UPDATE tblusers SET " & _
            "UserLastActivity		= " & formatField(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "", "NULL") & ", " & _
            "UserLastActivityDate	= " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE UserID 			= @UserID " & _
            "AND CompanyID          = @CompanyID "
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                            New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, Config.DefaultUserID)}
        executeNonQuery(strQry, CommandType.Text, arrParams)
    End Sub

    Private Sub checkAuthorised(ByVal redirect As Boolean)
        If (Not Config.LoggedIn) Then
            If (redirect) Then
                HttpContext.Current.Response.Redirect("/logon.aspx?strMessage=" & encodeURL("You are not authorised to view the requested page. Please log on to the system"))
            Else
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

    Private Sub checkIPAddress()
        setUserID()
        If (Not Config.LoggedIn) Then
            If (HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") <> "78.32.186.165") Then
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

    Public Sub checkAdminLevel(ByVal admin As String, ByVal redirect As Boolean)
        If (loggedInUserValue("UserSuperAdmin") <> "True" And loggedInUserValue(admin) <> "True") Then
            If (redirect) Then
                HttpContext.Current.Response.Redirect("/")
            Else
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

End Class
