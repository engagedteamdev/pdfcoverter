﻿Imports Microsoft.VisualBasic

Public Class LeadPlatform

    Private objConfig As Config = Nothing, objLogonCheck As LogonCheck = Nothing
    Private objProcessing As Processing = Nothing, objPanel As Panel = Nothing, objToolBar As Toolbar = Nothing, objBusinessProcessing As BusinessProcessing = Nothing, objBusinessPanel As BusinessPanel = Nothing
    Private objReporting As Reporting = Nothing, objBusinessReporting As BusinessReporting = Nothing, objDashboard As Dashboard = Nothing, objPrompts As Prompts = Nothing
    Private objAdminSave As AdminSave = Nothing, objProcessingSave As ProcessingSave = Nothing, objUtilitySave As UtilitySave = Nothing

    Public Sub initialise(Optional ByVal toolbar As Boolean = False, Optional ByVal cache As Cache = Nothing)
        objConfig = New Config
        objConfig.assignProperties()
        objLogonCheck = New LogonCheck
        objLogonCheck.LeadPlatform = Me
        objProcessing = New Processing
        objProcessing.LeadPlatform = Me
        objBusinessProcessing = New BusinessProcessing
        objBusinessProcessing.LeadPlatform = Me
        objPanel = New Panel
        objPanel.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objPanel.CacheObject = cache
        End If
        objBusinessPanel = New BusinessPanel
        objBusinessPanel.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objBusinessPanel.CacheObject = cache
        End If
        If (toolbar) Then
            objToolBar = New Toolbar
            objToolBar.LeadPlatform = Me
        End If
        objReporting = New Reporting
        objReporting.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objReporting.CacheObject = cache
        End If
        objBusinessReporting = New BusinessReporting
        objBusinessReporting.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objBusinessReporting.CacheObject = cache
        End If
        objDashboard = New Dashboard
        objDashboard.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objDashboard.CacheObject = cache
        End If
        objPrompts = New Prompts
        objPrompts.LeadPlatform = Me
        objAdminSave = New AdminSave
        objAdminSave.LeadPlatform = Me
        If (Not cache Is Nothing) Then
            objAdminSave.CacheObject = cache
        End If
        objProcessingSave = New ProcessingSave
        objProcessingSave.LeadPlatform = Me
        objUtilitySave = New UtilitySave
        objUtilitySave.LeadPlatform = Me
    End Sub

    Public Property Config() As Config
        Get
            Return objConfig
        End Get
        Set(ByVal value As Config)

        End Set
    End Property

    Public Property LogonCheck() As LogonCheck
        Get
            Return objLogonCheck
        End Get
        Set(ByVal value As LogonCheck)

        End Set
    End Property

    Public Property Processing() As Processing
        Get
            Return objProcessing
        End Get
        Set(ByVal value As Processing)

        End Set
    End Property

    Public Property BusinessProcessing() As BusinessProcessing
        Get
            Return objBusinessProcessing
        End Get
        Set(ByVal value As BusinessProcessing)

        End Set
    End Property

    Public Property Panel() As Panel
        Get
            Return objPanel
        End Get
        Set(ByVal value As Panel)

        End Set
    End Property

    Public Property BusinessPanel() As BusinessPanel
        Get
            Return objBusinessPanel
        End Get
        Set(ByVal value As BusinessPanel)

        End Set
    End Property

    Public Property ToolBar() As Toolbar
        Get
            Return objToolBar
        End Get
        Set(ByVal value As Toolbar)

        End Set
    End Property

    Public Property Reporting() As Reporting
        Get
            Return objReporting
        End Get
        Set(ByVal value As Reporting)

        End Set
    End Property

    Public Property BusinessReporting() As BusinessReporting
        Get
            Return objBusinessReporting
        End Get
        Set(ByVal value As BusinessReporting)

        End Set
    End Property

    Public Property Dashboard() As Dashboard
        Get
            Return objDashboard
        End Get
        Set(ByVal value As Dashboard)

        End Set
    End Property

    Public Property Prompts() As Prompts
        Get
            Return objPrompts
        End Get
        Set(ByVal value As Prompts)

        End Set
    End Property

    Public Property AdminSave() As AdminSave
        Get
            Return objAdminSave
        End Get
        Set(ByVal value As AdminSave)

        End Set
    End Property

    Public Property ProcessingSave() As ProcessingSave
        Get
            Return objProcessingSave
        End Get
        Set(ByVal value As ProcessingSave)

        End Set
    End Property

    Public Property UtilitySave() As UtilitySave
        Get
            Return objUtilitySave
        End Get
        Set(ByVal value As UtilitySave)

        End Set
    End Property

End Class
