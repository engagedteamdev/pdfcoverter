﻿Imports Config, Common, CommonDropdowns, CallInterface
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Xml

' ** Revision history **
'
' ** End Revision History **

Public Class Panel

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Public AppID As String = HttpContext.Current.Request("AppID")
    Public MediaCampaignID As String = getAnyField("MediaCampaignIDInbound", "tblapplications", "AppID", AppID)
    Public ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID"), ApplicationPageID As String = ""
    Public ApplicationPageOrder As String = HttpContext.Current.Request("Page"), intNextPageOrder As String = "", intPrevPageOrder As String = "", strPageName As String = "", strNextPageName As String = "", strPrevPageName As String = ""
    Public strOnSubmit As String = "", strOnLoadFunctions As String = "", strJavascriptFunctions As String = "", strFormAction As String = "", strDefaultReturnURL As String = "", strReturnURL As String = "", strTargetFrame As String = ""
    Public strUpdateStatusCode As String = "", strUpdateSubStatusCode As String = "", strUpdateDateType As String = "", strNote As String = "", boolSubmit As Boolean = False, boolMandatory As Boolean = False
    Public strLockUserName As String = "", intWorkflowActivityID As Integer = 0
	Public strFirstname As String = ""
	Public strSurname As String = "", strPostCode As String = "", strDOB As String = "", strAccountNumber As String = "", strSortCode As String = "", strCardNum As String = ""
	Public strHomeTel As String = "", strMobTel As String = "", strEmail As String =""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property JavascriptFunctions() As String
        Get
            Return strJavascriptFunctions
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property OnLoadFunctions() As String
        Get
            Return strOnLoadFunctions
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property NumPages() As Integer
        Get
            Dim intNumPages As String = ""
            Dim strSQL As String = "SELECT MAX(ApplicationPageOrder) + 1 FROM vwpanel WHERE AppID = '" & AppID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND CompanyID = '" & CompanyID & "'"
            intNumPages = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
            If (checkValue(intNumPages)) Then
                intNumPages = CInt(intNumPages)
            Else
                intNumPages = 0
            End If
            Return intNumPages
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Private Enum InputType
        Hidden = 0
        Text = 1
        SelectBox = 2
        Multi = 3
        TextArea = 4
        CheckBox = 5
        Radio = 6
        Quote = 7
    End Enum

    Public Sub checkConditions(ByVal frm As HtmlForm)

        intWorkflowActivityID = getAnyField("UserActiveWorkflowActivityID", "tblusers", "UserID", Config.DefaultUserID)
        If (Not checkValue(ApplicationPageOrder)) Then ApplicationPageOrder = 0
        strLockUserName = checkCaseLock(Config.DefaultUserID, AppID)

        Dim strRedirect As Boolean = False
        Dim strSQL As String = "SELECT ApplicationPageID, ApplicationPageName, ApplicationPageSQLConditions, ApplicationPageFunctionOnSubmitEvent, ApplicationPageJavascriptFunctions, ApplicationPageFormAction, ApplicationPageReturnURL, ApplicationPageTargetFrame, ApplicationPageUpdateStatusCode, " & _
            "ApplicationPageUpdateSubStatusCode, ApplicationPageUpdateDateType, ApplicationPageNote, ApplicationPageSubmit, ApplicationPageMandatory " & _
            "FROM vwpanel " & _
            "WHERE AppID = '" & AppID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder = '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("ApplicationPageSQLConditions").ToString)) Then
                    strSQL = "SELECT AppID FROM tblapplications WHERE AppID = '" & AppID & "' AND " & Row.Item("ApplicationPageSQLConditions").ToString & " AND tblapplications.CompanyID = '" & CompanyID & "'"
                    Dim dsCache2 As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                    If (dsCache2.Rows.Count = 0) Then
                        strRedirect = True
                    End If
                    dsCache2 = Nothing
                End If
                ApplicationPageID = Row.Item("ApplicationPageID")
                strPageName = Row.Item("ApplicationPageName")
                strOnSubmit = Row.Item("ApplicationPageFunctionOnSubmitEvent").ToString
                strOnSubmit = Replace(strOnSubmit, "{AppID}", AppID)
                strJavascriptFunctions = dateTags(Row.Item("ApplicationPageJavascriptFunctions").ToString)
                strFormAction = Row.Item("ApplicationPageFormAction").ToString
                strDefaultReturnURL = "/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&Page=" & ApplicationPageOrder
                strReturnURL = Row.Item("ApplicationPageReturnURL").ToString
                If (Not checkValue(strReturnURL)) Then
                    strReturnURL = strDefaultReturnURL
                Else
                    strReturnURL = Replace(strReturnURL, "{AppID}", AppID)
                    strReturnURL = Replace(strReturnURL, "{ApplicationTemplateID}", ApplicationTemplateID)
                    strReturnURL = Replace(strReturnURL, "{NextPage}", getNextPage())
					strReturnURL = Replace(strReturnURL, "/net/application/processing.aspx", "/processing.aspx")
					strReturnURL = Replace(strReturnURL, "/net/application/panel.aspx", "/application/panel.aspx")
                End If
                strTargetFrame = Row.Item("ApplicationPageTargetFrame").ToString
				If (strTargetFrame = "content") Then strTargetFrame = "_top"
                If (checkValue(strTargetFrame)) Then frm.Target = strTargetFrame
                strUpdateStatusCode = Row.Item("ApplicationPageUpdateStatusCode").ToString
                strUpdateSubStatusCode = Row.Item("ApplicationPageUpdateSubStatusCode").ToString
                strUpdateDateType = Row.Item("ApplicationPageUpdateDateType").ToString
                strNote = Row.Item("ApplicationPageNote").ToString
                boolSubmit = Row.Item("ApplicationPageSubmit")
                boolMandatory = Row.Item("ApplicationPageMandatory")
            Next
        End If
        dsCache = Nothing
        If (strRedirect) Then
            responseRedirect(getNextPageURL())
        End If
        If (Not checkValue(strReturnURL)) Then
            strReturnURL = HttpContext.Current.Request.ServerVariables("PATH_INFO")
            If (checkValue(HttpContext.Current.Request.ServerVariables("QUERY_STRING"))) Then
                strReturnURL += "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            End If
        End If
    End Sub

    Public Sub getApplicationFields()
        Dim strFields As String = "<div class=""btn-toolbar"">", strSelect As String = "", strData As String = ""
        Dim strSQL As String = "SELECT ValName, ValTable FROM tblapplicationpages PGS INNER JOIN tblapplicationpagemappings MAP ON MAP.ApplicationPageID = PGS.ApplicationPageID INNER JOIN tblimportvalidation IMP ON IMP.ValID = MAP.ApplicationFieldID " & _
            " WHERE PGS.ApplicationPageTemplateID = '" & ApplicationTemplateID & "' AND PGS.ApplicationPageOrder = '" & ApplicationPageOrder & "' AND PGS.CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("ValTable") = "tbldatastore") Then
                    strSelect += ", (SELECT TOP 1 StoredDataValue FROM dbo.tbldatastore WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AND (StoredDataName = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                ElseIf (Row.Item("ValTable") = "tblapplicationstatusdates") Then
                    strSelect += ", (SELECT TOP 1 ApplicationStatusDate FROM dbo.tblapplicationstatusdates WHERE (ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AND (ApplicationStatusDateName = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                ElseIf (Row.Item("ValTable") = "tbldiaries") Then
                    strSelect += ", (SELECT TOP 1 DiaryDueDate FROM dbo.tbldiaries WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND DiaryActive = 1 AND DiaryComplete = 0) AND (DiaryType = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                Else
                    strSelect += ", " & Row.Item("ValName")
                End If
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT ValID, ValName, AdditionalFieldButtonMultiSelect, ApplicationFriendlyName, ApplicationTooltip, ApplicationMandatory, ApplicationInputType, ApplicationDropdownFunction, ApplicationDropdownValues, ApplicationDataGridColumn, ApplicationDataGridTableClass, ApplicationDataGridColumnClass, ApplicationFunctionOnChangeEvent, ApplicationFunctionOnBlurEvent, " & _
                "ApplicationDateType, AdditionalFieldType, AdditionalFieldID, AdditionalFieldHTML, AdditionalFieldRoutine, AdditionalFieldRoutineParameters, AdditionalFieldButtonText, AdditionalFieldButtonStatusCode, AdditionalFieldButtonSubStatusCode, AdditionalFieldButtonDateType, AdditionalFieldButtonClearDateType, AdditionalFieldButtonPanelID, AdditionalFieldButtonPanelWidth, AdditionalFieldButtonPanelHeight, AdditionalFieldButtonShowNote, AdditionalFieldButtonNote, ScriptColour, ScriptText, AdditionalFieldName " & strSelect & " FROM vwpanel WHERE AppID = '" & AppID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder = '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageMappingID"

        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim y As Integer = 0, intColSpan As Integer = 0, boolWriteLabel As Boolean = True, boolNewRow As Boolean = True
            'strFields += "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""margin: 0px; padding: 0px"">" & vbCrLf
            For Each Row As DataRow In dsCache.Rows
                Dim strName As String = "", strTitle As String = "", strClass As String = "", strScriptClass As String = "", strChecked As String = "", strTip As String = ""
                If checkValue(Row.Item("ValID").ToString) Then
                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        If (Row.Item("ApplicationInputType") <> InputType.Hidden) Then
                            strFields += "<div id=""" & Row.Item("ValName") & "Container"" class=""control-group"">" & vbCrLf
                        Else
                            strFields += "<div id=""" & Row.Item("ValName") & "Container"" class=""control-group"">" & vbCrLf
                        End If
                    End If
                    If (checkValue(Row.Item("ApplicationTooltip"))) Then
                        strTitle = Row.Item("ApplicationTooltip")
                        strClass = " ui-tooltip"
                    ElseIf (checkValue(Row.Item("ApplicationFriendlyName"))) Then
                        strTitle = Row.Item("ApplicationFriendlyName")
                        strName = Row.Item("ValName")
                    End If
                    If (checkValue(Row.Item("ApplicationFriendlyName"))) Then
                        strName = Row.Item("ApplicationFriendlyName")
                    Else
                        strName = Row.Item("ValName")
                    End If
                    'If (checkValue(Row.Item("ApplicationTooltip"))) Then
                    'strTip = "<div class=""tooltip displayNone"">" & Row.Item("ApplicationTooltip") & "</div>"
                    'End If
                    'strFields += strTip
                    'intColSpan = 2
                    'boolNewRow = True
                    'strFields += "<tr>" & vbCrLf
                    'strFields += "<td class=""smlr"" style=""width: 40%"">"
                    'strFields += "<div style=""width: 40%;"" class=""label"">"
                    'strFields += "<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">"
                    If (Row.Item("ApplicationInputType") <> InputType.Hidden And boolWriteLabel And Not (Row.Item("ApplicationDataGridColumn"))) Then
                        'strFields += "<span id=""" & Row.Item("ValName") & "Label"">" & strName & ":</span>"
                        strFields += "<label class=""control-label ui-tooltip"" for=""" & Row.Item("ValName") & """ data-placement=""right"" title=""" & strTitle & """>" & strName & "</label>"
                    Else
                        strFields += "&nbsp;"
                    End If
                    'strFields += "</span>"
                    'strFields += "</div>" & vbCrLf
                    'strFields += "</td>" & vbCrLf
                    'strFields += "<td class=""sml"" style=""width: 60%"">"
                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        strFields += "<div class=""controls"">"
                    End If
                    If (Row.Item("ApplicationDataGridColumn")) Then
                        If (Row.Item("ApplicationMandatory")) Then
                            strClass += " mandatory"
                        End If
                        If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                            strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" class=""" & Row.Item("ApplicationDataGridTableClass") & strClass & """ value=""" & Row.Item(Row.Item("ValName")) & """ title=""" & Row.Item("ApplicationFriendlyName") & """ data-friendly-name=""" & Row.Item("ApplicationFriendlyName") & """ data-input-type=""" & Row.Item("ApplicationInputType") & """ data-dropdown-values=""" & Row.Item("ApplicationDropdownValues").ToString & """ data-column-class=""" & Row.Item("ApplicationDataGridColumnClass").ToString & """ />" & vbCrLf
                        Else
                            strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" class=""" & Row.Item("ApplicationDataGridTableClass") & strClass & """ value=""" & Row.Item(Row.Item("ValName")) & """ title=""" & Row.Item("ApplicationFriendlyName") & """ data-friendly-name=""" & Row.Item("ApplicationFriendlyName") & """ data-input-type=""" & Row.Item("ApplicationInputType") & """ data-dropdown-values=""" & Row.Item("ApplicationDropdownValues").ToString & """ data-column-class=""" & Row.Item("ApplicationDataGridColumnClass").ToString & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf
                        End If
                    Else
                        Select Case Row.Item("ApplicationInputType")
                            Case InputType.Hidden
                                strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                            Case InputType.Text
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                If (Row.Item("ValName") = "BankCardNumber") Then
                                    strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & getAnyFieldFromDataStore("BankCardLastFour", AppID) & """ />" & vbCrLf
                                    strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & getAnyFieldFromDataStore("BankCardLastFour", AppID) & """ />" & vbCrLf
                                ElseIf (Row.Item("ValName") = "BankCardCV2") Then
                                    strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""***"" />" & vbCrLf
                                    strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""***"" />" & vbCrLf
                                Else
                                    strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                    strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                End If
                                If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    If (Row.Item("ValName") = "BankCardNumber") Then
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & getAnyFieldFromDataStore("BankCardLastFour", AppID) & """ class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & AppID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ data-placement=""right"" />" & vbCrLf  ' hideTooltip();
                                    ElseIf (Row.Item("ValName") = "BankCardCV2") Then
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""***"" class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & AppID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ data-placement=""right"" />" & vbCrLf  ' hideTooltip();
                                    Else
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & Row.Item(Row.Item("ValName")) & """ class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & AppID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ data-placement=""right"" />" & vbCrLf  ' hideTooltip();
                                    End If
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & AppID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    If (Row.Item("ValName") = "BankCardNumber") Then
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & getAnyFieldFromDataStore("BankCardLastFour", AppID) & """ class=""text input-xlarge" & strClass & """ title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf  ' hideTooltip();
                                    ElseIf (Row.Item("ValName") = "BankCardCV2") Then
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""***"" class=""text input-xlarge" & strClass & """ title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf  ' hideTooltip();
                                    Else
                                        strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & Row.Item(Row.Item("ValName")) & """ class=""text input-xlarge" & strClass & """ title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf  ' hideTooltip();
                                    End If
                                End If
                            Case InputType.TextArea
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & """ />" & vbCrLf
                                strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & """ />" & vbCrLf
                                If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strFields += "<textarea id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & AppID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ rows=""3"" data-placement=""right"" />" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & "</textarea>" & vbCrLf  ' hideTooltip();
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & AppID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    strFields += "<textarea id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" class=""text input-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" rows=""3"" />" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & "</textarea>" & vbCrLf  ' hideTooltip();
                                End If
                            Case InputType.SelectBox
                                Dim strOptions As String = ""
                                If (checkValue(Row.Item("ApplicationDropdownFunction").ToString)) Then
                                    Dim arrParams As Object() = {CacheObject(), "--" & strName & "--", Row.Item(Row.Item("ValName")).ToString}
                                    strOptions = executeDropdownByName(Me, Row.Item("ApplicationDropdownFunction"), arrParams)
                                ElseIf (checkValue(Row.Item("ApplicationDropdownValues").ToString)) Then
                                    Dim strValue As String = Row.Item(Row.Item("ValName")).ToString
                                    Dim arrOptions As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                                    strOptions += "<option value="""">--" & strName & "--</option>"
                                    For x As Integer = 0 To UBound(arrOptions)
                                        strOptions += "<option value=""" & arrOptions(x) & """"
                                        If (checkValue(strValue)) Then
                                            If (strValue = arrOptions(x)) Then
                                                strOptions += " selected=""selected"""
                                            End If
                                        End If
                                        strOptions += ">" & arrOptions(x) & "</option>" & vbCrLf
                                    Next
                                End If
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strClass = Replace(strClass, "ui-tooltip", "")
                                    strFields += "<select id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ onchange=""" & Row.Item("ApplicationFunctionOnChangeEvent") & """ onblur=""saveField('" & AppID & "','" & MediaCampaignID & "',this);"" class=""text select-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ data-placement=""right"">" & vbCrLf
                                    strFields += strOptions & vbCrLf
                                    strFields += "</select>" ' hideTooltip();
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & AppID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    strFields += "<select id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ class=""text select-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"">" & vbCrLf
                                    strFields += strOptions & vbCrLf
                                    strFields += "</select>" ' hideTooltip();
                                End If
                            Case InputType.CheckBox
                                If (Row.Item(Row.Item("ValName")).ToString = "1") Then
                                    strChecked = " checked=""checked"""
                                End If
                                If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""checkbox"" value=""1"" onclick=""saveField('" & AppID & "','" & MediaCampaignID & "',this);""" & strChecked & " />" & vbCrLf
                                Else
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""checkbox"" value=""1"" readonly=""readonly""" & strChecked & " />" & vbCrLf
                                End If
                        End Select
                    End If

                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        strFields += _
                        "<span class=""displayNone""><i id=""" & Row.Item("ValName") & "Error"" class=""icon-ok"" style=""position: absolute; margin: 12px 0px 0px 0px;""></i></span><br />"
                        strFields += "</div></div>" & vbCrLf
                    End If
                    'strFields += "</td>" & vbCrLf
                    'strFields += "</tr>" & vbCrLf
                    strOnLoadFunctions += Row.Item("ApplicationFunctionOnChangeEvent")
                Else
                    Select Case Row.Item("AdditionalFieldType").ToString
                        Case 1 ' HTML
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td>&nbsp;</td>" & vbCrLf
                            '    strFields += "<td class=""sml"">"
                            'End If
                            strFields += Row.Item("AdditionalFieldHTML")
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 2 ' Script
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            'If (y = 0) Then strFields += "<br />"
                            If (Row.Item("ScriptColour") = "Green" Or Row.Item("ScriptColour") = "Red") Then
                                strFields += "<blockquote class=""no-triangle-right displayBlock"" id=""" & Replace(Row.Item("AdditionalFieldName").ToString, " ", "") & """><span id=""script" & Row.Item("ScriptColour") & "Icon"" /></span>" & replaceTags(Row.Item("ScriptText"), AppID) & "</blockquote>"
                            Else
                                strFields += "<blockquote class=""triangle-right displayBlock"" id=""" & Replace(Row.Item("AdditionalFieldName").ToString, " ", "") & """><span id=""quoteOpen""></span><span id=""script" & Row.Item("ScriptColour") & "Icon"" /></span>" & replaceTags(Row.Item("ScriptText"), AppID) & "<span id=""quoteClose""></span></blockquote>"
                            End If
                        Case 3 ' Subroutine
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (checkValue(Row.Item("AdditionalFieldRoutine").ToString)) Then
                                Dim arrParams As Object() = {CacheObject(), AppID}
                                If (checkValue(Row.Item("AdditionalFieldRoutineParameters").ToString)) Then
                                    Dim arrFrmActionParameters As String() = Nothing
                                    arrFrmActionParameters = Split(Row.Item("AdditionalFieldRoutineParameters").ToString, ",")
                                    For y = 0 To UBound(arrFrmActionParameters)
                                        ReDim Preserve arrParams(2 + y)
                                        arrParams(2 + y) = arrFrmActionParameters(y)
                                    Next
                                End If
                                strFields += executeSub(Me, Row.Item("AdditionalFieldRoutine").ToString, arrParams)
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 4 ' Progress Case Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(AppID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-success btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/application/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&AppID=" & AppID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                        Else
                                            If (Row.Item("AdditionalFieldButtonShowNote")) Then
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', true)"" class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                                '"<a id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.jPrompt('This action will progress the case. Please enter a note and confirm.', '', '" & Row.Item("AdditionalFieldButtonText") & "', '" & Row.Item("AdditionalFieldButtonText") & "', 'Cancel', function(r) { if (r) { setStatusDate('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "');$('#frmHiddenNote').val($('#frmHiddenNote').val() + ': ' + r);setFormAction('" & Row.Item("AdditionalFieldID") & "');submitform(document.form1) } } );"" class=""salesButtonGreen"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                            Else
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 5 ' Regress Case Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(AppID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-danger btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/application/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&AppID=" & AppID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button>"
                                        Else
                                            If (Row.Item("AdditionalFieldButtonShowNote")) Then
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', true)"" class=""btn btn-danger btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            Else
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-danger btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 6 ' Utility Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(AppID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-tertiary btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/application/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&AppID=" & AppID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            '"<a id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onclick=""parent.parent.tb_show('" & Row.Item("AdditionalFieldButtonText") & "', '/application/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&AppID=" & AppID & "&TB_iframe=true&width=" & Row.Item("AdditionalFieldButtonPanelWidth") & "&height=" & Row.Item("AdditionalFieldButtonPanelHeight") & "', '')"" class=""salesButton"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                        Else
                                            strFields += _
                                                "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-tertiary btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If

                        Case 7, 8, 9 ' multi Select Progress Button 
                            Dim strParams As String() = Split(Row.Item("AdditionalFieldButtonMultiSelect").ToString(), "|")
                            If (strParams.Length = 5) Then
                                Dim strStatuses As String() = Split(strParams(0), ",")
                                Dim strSubStatuses As String() = Split(strParams(1), ",")
                                Dim strDateTypes As String() = Split(strParams(2), ",")
                                Dim strRemoveDateTypes As String() = Split(strParams(3), ",")
                                Dim strFriendlyNames As String() = Split(strParams(4), ",")
                                Dim strButton As String = ""
                                If (Row.Item("AdditionalFieldType").ToString = 7) Then
                                    strButton = " btn-success"
                                ElseIf (Row.Item("AdditionalFieldType").ToString = 8) Then
                                    strButton = " btn-danger"
                                ElseIf (Row.Item("AdditionalFieldType").ToString = 9) Then
                                    strButton = " btn-tertiary"
                                End If
                                'If (boolNewRow) Then
                                'strFields += "<tr>" & vbCrLf
                                'strFields += "<td colspan=""" & intColSpan & """>"
                                'End If

                                If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    If (checkDateField(AppID, Row.Item("ApplicationDateType"))) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                            If (checkValue(Row.Item("AdditionalFieldButtonMultiSelect"))) Then
                                                Dim x As Integer = 0
                                                'If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                                strFields += _
                                               "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ class=""btn" & strButton & " btn-medium dropdown-toggle"" data-toggle=""dropdown"">" & Row.Item("AdditionalFieldButtonText") & " <span class=""caret""></span></button>" & vbCrLf & _
                                               "<ul class=""dropdown-menu"">"
                                                For z As Integer = 0 To UBound(strStatuses)
                                                    strFields += "<li><a href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & " - " & strFriendlyNames(z) & "'); parent.progressCase('" & strStatuses(z).ToString & "','" & strSubStatuses(z).ToString & "','" & strDateTypes(z).ToString & "','" & strRemoveDateTypes(z) & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"">" & strFriendlyNames(z) & "</a></li>" & vbCrLf
                                                Next
                                                strFields += "</ul></div>"
                                                '"<a id= & Row.Item("AdditionalFieldName") & """ href=""#"" onclick=""parent.parent.tb_show('" & Row.Item("AdditionalFieldButtonText") & "', '/application/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&AppID=" & AppID & "&TB_iframe=true&width=" & Row.Item("AdditionalFieldButtonPanelWidth") & "&height=" & Row.Item("AdditionalFieldButtonPanelHeight") & "', '')"" class=""salesButton"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                                'Else
                                                '    strFields += _
                                                '    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ class=""btn btn-primary btn-small dropdown-toggle"" data-toggle=""dropdown"">" & Row.Item("AdditionalFieldButtonText") & "<span class=""caret""></span></button>" & vbCrLf & _
                                                '    "<ul class=""dropdown-menu"">"
                                                '    While x <= strcount
                                                '        strFields += "<li><a href=""onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "'); parent.progressCase('INF','','','','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)>" & strFriendlyNames(x) & "</a></li>"
                                                '        x = x + 1
                                                '    End While
                                                '    strFields += "</ul></div>"
                                                'End If
                                                ' "<button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "'); parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)""
                                                '										 class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button>"
                                                '                                          
                                            End If

                                        End If
                                    End If
                                End If
                                'If (boolNewRow) Then
                                'strFields += "</td>" & vbCrLf
                                'strFields += "</tr> " & vbCrLf
                                'boolNewRow = False
                                'End If
                            End If
                    End Select
                End If
                y += 1
            Next
            'strFields += "</table>"
            strFields += "</div>"
        End If
        dsCache = Nothing
        HttpContext.Current.Response.Write(strFields)
    End Sub

    Public Function getNextPage() As String
        Dim strPageOrder As String = ""
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder FROM vwpanel WHERE AppID = '" & AppID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder > '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder"
        strPageOrder = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        Return strPageOrder
    End Function

    Public Function getPreviousPage() As String
        Dim strPageOrder As String = ""
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder FROM vwpanel WHERE AppID = '" & AppID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder < '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder DESC"
        strPageOrder = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        Return strPageOrder
    End Function

    Public Function getNextPageURL() As String
        Dim strLink As String = "", intNextPage As String = getNextPage()
        If (checkValue(intNextPage)) Then
            strLink += "panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&Page=" & intNextPage
        End If
        Return strLink
    End Function

    Public Sub setNavigation()
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder, ApplicationPageName FROM vwpanel WHERE AppID = '" & AppID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder > '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intNextPageOrder = Row.Item("ApplicationPageOrder")
                strNextPageName = Row.Item("ApplicationPageName").ToString
            Next
        End If
        dsCache = Nothing
        strSQL = "SELECT TOP 1 ApplicationPageOrder, ApplicationPageName FROM vwpanel WHERE AppID = '" & AppID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "'AND ApplicationPageOrder < '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder DESC"
        dsCache = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intPrevPageOrder = Row.Item("ApplicationPageOrder")
                strPrevPageName = Row.Item("ApplicationPageName").ToString
            Next
        End If
        dsCache = Nothing
    End Sub

    'Private Function replaceParameters(ByVal params As String) As Object()
    '    Dim arrObjects As Object() = Nothing
    '    Dim arrParams As Array = Split(params)
    '    For Each param As Object In arrParams
    '        Dim x As Integer = 0
    '        If (param = "{AppID}") Then
    '            param = AppID
    '        End If
    '        If (param = "{Cache}") Then
    '            param = CacheObject()
    '        End If
    '        arrObjects(x) = Nothing
    '    Next
    '    Return arrObjects
    'End Function
    '*******************************************************
    ' Execute any method from this class
    '*******************************************************
    Shared Function executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(Panel)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        Return objMethodInfo.Invoke(inst, params)
    End Function
	
	Public Function callback(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
			Dim strCallBack As String = "<button id=""callBack"" class=""btn btn-secondary btn-medium"" href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Call Back', '/prompts/endcall.aspx?AppID=" & AppID & "&StatusCode=CBK', '400', '500');"">Call Back</button>"
	        Return strCallBack
    End Function
	

	

    Public Function hotkey(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String

        Dim strSuccessMessage As String = HttpContext.Current.Request("strSuccessMessage")
        Dim strFailureMessage As String = HttpContext.Current.Request("strFailureMessage")
        Dim strReturnURL As String = "/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&Page=" & HttpContext.Current.Request("Page")
        Dim strClass As String = "row1"

        Dim boolHotkeyed As Boolean = False
        Dim strOptions As String = ""

        strOptions += " <script type=""text/javascript"" src=""/js/hotkey.aspx?v=" & Config.VersionFormScripts & """></script>" & vbCrLf
        strOptions += "<div id=""hotkeyBox""><br /><br />" & vbCrLf
        strOptions += "<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped table-highlight"" id=""hotkey"" width=""100%"">" & vbCrLf

        If (checkValue(strSuccessMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><div class=""alert alert-success""><h4 class=""alert-heading"">Error</h4><p>" & strSuccessMessage & "</p></div></td>" & vbCrLf & _
            "</tr>"
        End If
        If (checkValue(strFailureMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>" & strFailureMessage & "</p></div></td>" & vbCrLf & _
            "</tr>"
        End If
        strOptions += _
        "<tr>" & vbCrLf & _
        "<td colspan=""4""><h5>Please select a partner to transfer data to:</h5><br /></td>" & vbCrLf & _
        "</tr>"
        '        If (Not boolHotkeyed) Then
     If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
            'If (intDialerGrade <> "7" And intDialerGrade <> "8" And intDialerGrade <> "9") Then
            Dim strSQL = "SELECT MediaCampaignID, MediaID, MediaCampaignName, MediaCampaignIDOutbound, MediaCampaignCampaignReference, MediaCampaignClientReferenceRequired, ClientReferenceOutbound, MediaCampaignTelephoneNumberOutbound, MediaCampaignScheduleID, MediaName, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress, MediaCampaignDeliveryToUser, MediaCampaignMaxCostInbound, MediaCampaignCostInbound " & _
                        "FROM vwdistributionsearch " & _
                        "WHERE MediaCampaignDeliveryTypeID <> 6 AND MediaCampaignDeliveryTypeID <> 10 AND MediaCampaignDeliveryBatch = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND MediaCampaignCompanyID = '" & CompanyID & "' ORDER BY MediaCampaignDeliveryRemaining ASC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim boolQualified As Boolean = True
                Dim y As Integer = 0
                Dim strRowClass As String = "even"
                For Each Row As DataRow In dsCache.Rows

                    Dim strInnerSQL As String = "", strAdhocRules As String = ""
                    strInnerSQL = "SELECT MediaCampaignHotkeyRuleField, MediaCampaignHotkeyRuleComparison, DataTypeCode, MediaCampaignHotkeyRuleValue FROM tblmediacampaignhotkeyrules HKR " & _
                                    "LEFT JOIN tbldatatypes DTY ON DTY.DataTypeID = HKR.MediaCampaignHotkeyRuleDataType " & _
                                    "WHERE (MediaCampaignHotkeyRuleActive = 1 AND MediaCampaignID = '" & Row.Item("MediaCampaignID") & "' AND CompanyID = '" & CompanyID & "')"
                    Dim dsCacheInner As DataTable = New Caching(Nothing, strInnerSQL, "", "", "").returnCache()
                    If (dsCacheInner.Rows.Count > 0) Then
                        For Each InnerRow As DataRow In dsCacheInner.Rows
                            Dim strRuleField As String = Replace(Replace(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, "SD:", ""), "DS:", "")
                            If (dsCacheInner.Rows.IndexOf(InnerRow) = 0) Then
                                strAdhocRules += "SELECT AppID FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                            End If
                            If (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "DS:") Then
                                strAdhocRules += " AND ((SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & strRuleField & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                            ElseIf (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "SD:") Then
                                Select Case strRuleField
                                    Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "Lock"
                                        strAdhocRules += " AND (" & strRuleField & "Date " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                    Case Else
                                        strAdhocRules += " AND ((SELECT ApplicationStatusDate FROM tblapplicationstatusdates WHERE ApplicationStatusDateName = '" & strRuleField & "' AND ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                End Select
                            Else
                                strAdhocRules += " AND (" & strRuleField & " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                            End If
                        Next
                    End If
                    dsCacheInner = Nothing

                    If (checkValue(strAdhocRules)) Then
                        dsCacheInner = New Caching(Nothing, strAdhocRules, "", "", "").returnCache()
                        If (dsCacheInner.Rows.Count > 0) Then
                            boolQualified = True
                        Else
                            boolQualified = False
                        End If
                    End If

                    If (strRowClass = "row1") Then
                        strRowClass = "row2"
                    Else
                        strRowClass = "row1"
                    End If
                    If (y = 0) Then
                        strOptions += _
						"<thead>"& vbCrLf & _
                        "<tr class=""tablehead"">" & vbCrLf & _
                        "<th class=""smlc"">Partner</th>" & vbCrLf & _
                        "<th class=""smlc"">Telephone Number</th>" & vbCrLf & _
                        "<th class=""smlc"">Action</th>" & vbCrLf & _
                        "<th class=""smlc"">Reference</th>" & vbCrLf & _
                        "</tr>"& vbCrLf & _
						"</thead>"
                    End If
                    strOptions += _
                        "<tr class=""" & strRowClass & """>" & vbCrLf & _
                        "<td class=""smlc"">" & Row.Item("MediaName") & "<br />" & Row.Item("MediaCampaignName")
                    'If (checkValue(Row.Item("MediaCampaignMaxCostInbound").ToString)) Then
                    'If (Row.Item("MediaCampaignCostInbound") > Row.Item("MediaCampaignMaxCostInbound")) Then
                    'strOptions += "<div class=""alert""><img src=""/images/script_reminder.gif"" width=""16"" height=""16"" class=""floatLeft"" /><span class=""red"">&nbsp;Overflow only</span></div>"
                    'End If
                    'End If
                    strOptions += _
                    "</td>" & vbCrLf & _
                    "<td class=""smlc"">" & Row.Item("MediaCampaignTelephoneNumberOutbound") & "</td>" & vbCrLf & _
                    "<td class=""smlc""><span id=""MessageContainer" & y & """>"
                    If (boolQualified) Then
                        If (Row.Item("MediaCampaignClientReferenceRequired")) Then
                            strOptions += "<input id=""txtClientReferenceOutbound" & y & """ name=""txtClientReferenceOutbound" & y & """ type=""text"" value=""Ref No."" class=""text smaller mandatory"" onfocus=""checkDisplay('Ref No.',this)"" onblur=""populateDisplay('Ref No.',this)"" />"
                        End If
                        Select Case Row.Item("MediaCampaignDeliveryTypeID")
                            Case DeliveryType.XMLTransfer
                                If (Row.Item("MediaCampaignDeliveryToUser") = "1") Then
                                    strOptions += _
                                    "<select id=""ddTransferUserID" & y & """ class=""text tiny mandatory"">" & hotkeyUserList(cache, "--User--", Row.Item("MediaID"), "") & "</select>"
                                End If
                                strOptions += _
                                "<br />Transfer Customer&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & "','','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','" & Row.Item("MediaCampaignCampaignReference") & "')""><i class=""icon-share-alt"" /></a>"
                                'Case DeliveryType.XMLLenderScoring
                                '    strOptions += _
                                '    "Awaiting Score&nbsp;<a href=""#"" onclick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & "','','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','" & Row.Item("MediaCampaignCampaignReference") & "')""><img src=""/images/hotkey.png"" title=""Awaiting Score"" border=""0"" /></a>"
                            Case DeliveryType.Email, DeliveryType.EmailCSV, DeliveryType.EmailTextOnly
                                If (Row.Item("MediaCampaignDeliveryToUser") = "1") Then
                                    strOptions += _
                                    "<select id=""ddTransferUserID" & y & """ class=""text select-medium mandatory"">" & hotkeyUserList(cache, "--User--", Row.Item("MediaID"), "") & "</select>"
                                End If
                                strOptions += _
                                "<br />Transfer Customer&nbsp;<a href=""#"" onClick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','sendindividualapp.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','" & Row.Item("MediaCampaignDeliveryEmailAddress") & "','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','')"";><i class=""icon-share-alt"" /></a>"
                            Case DeliveryType.VoiceOnly
                                strOptions += _
                                "Transfer Customer&nbsp;<a href=""#"" onClick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','')"";><i class=""icon-share-alt"" /></a>"
                            Case DeliveryType.SalesTransfer
                                If (Row.Item("MediaCampaignDeliveryToUser") = "1") Then
                                    strOptions += _
                                    "<select id=""ddSalesUserID" & y & """ class=""text select-medium mandatory"">" & transferUserList(cache, "--User--", Row.Item("MediaID"), "") & "</select>"
                                End If
                                strOptions += _
                               "<br />Transfer Customer&nbsp;<a href=""#"" onClick=""sendToSales('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','')"";><i class=""icon-share-alt"" /></a>"
                            Case DeliveryType.IssueDocuments
                                strOptions += _
                                "Issue Documents&nbsp;<a href=""#"" onClick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/endhotkey.aspx?AppID=','" & Row.Item("MediaCampaignDeliveryTypeID") & "','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','')"";><i class=""icon-share-alt"" /></a>"
                        End Select
                    Else
                        strOptions += "Outside criteria"
                    End If
                    strOptions += _
                    "</span></td>" & vbCrLf & _
                    "<td class=""smlc""><span style=""color:green""><strong>" & Row.Item("ClientReferenceOutbound") & "</strong></span></td>" & vbCrLf & _
                    "</tr>"
                    y = y + 1
                Next
                strOptions += "<input type=""hidden"" id=""NumOptions"" value=""" & y & """ />"
            Else
                strOptions += _
                "<tr>" & vbCrLf & _
                "<td colspan=""4"" class=""smlc"">No hotkey options found.</td>" & vbCrLf & _
                "</tr>"
            End If
            dsCache = Nothing

            strSQL = "SELECT MC.MediaCampaignID, MC.MediaCampaignName, MC.MediaCampaignTelephoneNumberOutbound, MC.MediaCampaignDeliveryTypeID, MED.MediaName, APP.MediaCampaignIDOutbound, APP.ClientReferenceOutbound " & _
                                    "FROM tblmediacampaigns MC " & _
                                    "INNER JOIN tblmedia MED ON MED.MediaID = MC.MediaID " & _
                                    "LEFT JOIN tblapplications APP ON APP.MediaCampaignIDOutbound = MC.MediaCampaignID " & _
                                    "INNER JOIN tblapplicationstatus APS ON APS.AppID = APP.AppID " & _
                                    "WHERE MC.MediaCampaignDeliveryBatch = 0 AND APP.MediaCampaignIDOutbound > 0 AND APP.AppID = '" & AppID & "' AND MC.CompanyID = '" & CompanyID & "'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                boolHotkeyed = True
                Dim x As Integer = 0
                Dim strRowClass As String = "row1"
                For Each Row As DataRow In dsCache.Rows
                    If (strRowClass = "even") Then
                        strRowClass = "odd"
                    Else
                        strRowClass = "even"
                    End If
                    If (Row.Item("MediaCampaignDeliveryTypeID") = DeliveryType.SalesTransfer) Then
                        boolHotkeyed = False
                    End If
                    If (x = 0) Then
                        strOptions += _
                        "<tr>" & vbCrLf & _
                        "<td class=""smlc"" colspan=""4"">&nbsp;</td>" & vbCrLf & _
                        "</tr>" & _
                        "<tr>" & vbCrLf & _
                        "<td colspan=""4""><h3 class=""green"">Previous Transfers</h3><br /></td>" & vbCrLf & _
                        "</tr>" & vbCrLf & _
						"<thead>"& vbCrLf & _
                        "<tr class=""tablehead"">" & vbCrLf & _
                        "<th class=""smlc"">Partner</th>" & vbCrLf & _
                        "<th class=""smlc"">Telephone Number</th>" & vbCrLf & _
                        "<th class=""smlc"" width=""200"">Result</th>" & vbCrLf & _
                        "<th class=""smlc"">Reference</th>" & vbCrLf & _
						"</thead>" & vbcrlf & _
                        "</tr>"
                    End If
                    strOptions += _
                    "<tr class=""" & strRowClass & """>" & vbCrLf & _
                    "<td class=""smlc"">" & Row.Item("MediaName") & "<br />" & Row.Item("MediaCampaignName") & "</td>" & vbCrLf & _
                    "<td class=""smlc"">" & Row.Item("MediaCampaignTelephoneNumberOutbound") & "</td>" & vbCrLf & _
                    "<td class=""smlc""><span style=""color:green"">Actions complete</span></td>" & vbCrLf
                    strOptions += "<td class=""smlc""><span style=""color:green; font-weight: bold;"">" & shortenShowTitle(Row.Item("ClientReferenceOutbound").ToString, 10) & "</span></td>" & vbCrLf
                    strOptions += "</tr>"
                    x = x + 1
                Next
                If ((intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") And Not checkValue(strLockUserName)) Then
                    strOptions += _
                        "<tr class=""" & strRowClass & """>" & vbCrLf & _
                        "<td colspan=""4"" class=""smlc"">" & _
                        "<a href=""#"" onClick=""cancelTransfer('/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strReturnURL) & "&frmAppID=" & AppID & "&strThisPg=hotkey&strFrmAction=cancel');""><button class=""btn btn-primary btn-mini""><i class=""icon-ban-circle""></i> Cancel Transfer</button></a> "
                    'If (objLeadPlatform.Config.Reports Or objLeadPlatform.Config.SuperAdmin) Then
                    '    strOptions += "&nbsp;<a href=""/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strReturnURL) & "&frmAppID=" & AppID & "&strThisPg=hotkey&strFrmAction=return"" onClick=""if (confirm('Are you sure you want to return this transfer?')){return true;} else {return false;}""><img src=""/images/icons/hotkey_return.gif"" width=""16"" height=""16"" title=""Return Transfer"" border=""0"" /></a> Return Transfer"
                    'End If
                    strOptions += _
                        "</td>" & vbCrLf & _
                        "</tr>"
                End If
            End If
            dsCache = Nothing

            'Else
            '    If (Not checkSurveyDate(AppID) And Not checkTurnDown(AppID)) Then
            '        strOptions += _
            '        "<tr>" & vbCrLf & _
            '        "<td colspan=""3""><h3 class=""green"">Customer survey complete, please confirm.</h3></td>" & vbCrLf & _
            '        "<td class=""smlc""><span id=""MessageContainer0"">Survey Complete&nbsp;<a href=""#"" onclick=""changeStatus('" & AppID & "','Survey','SVY','','" & Config.DefaultUserID & "','" & strView & "')""><img src=""/images/hotkey.png"" title=""Survey Complete"" border=""0"" /></a></td>" & vbCrLf & _
            '        "</tr>"
            '    Else
            '        strOptions += _
            '        "<tr>" & vbCrLf & _
            '        "<td colspan=""4"" class=""smlc"">Survey already complete.</td>" & vbCrLf & _
            '        "</tr>"
            '    End If
            'End If
            'If (Not checkTurnDown(AppID) And objLeadPlatform.Config.Reports) Then
            '                    strOptions += _
            '                    "<tr>" & vbCrLf & _
            '                    "<td colspan=""4""><hr /><h3 class=""green"">Or select turn down type:</h3></td>" & vbCrLf & _
            '                    "</tr>" & vbCrLf & _
            '                    "<tr>" & vbCrLf & _
            '                    "<td colspan=""4"" class=""smlc"">" & vbCrLf & _
            '                    "<select id=""ddStatusCode"" class=""medium"" onchange=""toggleSubStatusDropdown(this.value);"">" & vbCrLf & _
            '                    "<option value="""" selected=""selected"">--Select Reason--</option>" & vbCrLf & _
            '                    "<option value=""WTD"">We Turned Down</option>" & vbCrLf & _
            '                    "<option value=""TUD"">Turned Us Down</option>" & vbCrLf & _
            '                    "<option value=""INV"">Invalid Application</option>" & vbCrLf & _
            '                    "</select>&nbsp;" & vbCrLf & _
            '                    "<select id=""ddWTDSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(cache, "--WTD Reason--", "5", intSubStatusGroupID, "") & "</select>" & vbCrLf & _
            '                    "<select id=""ddTUDSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(cache, "--TUD Reason--", "6", intSubStatusGroupID, "") & "</select>" & vbCrLf & _
            '                    "<select id=""ddINVSubStatusCode"" class=""medium"" style=""display:none;"">" & subStatusList(cache, "--INV Reason--", "3", intSubStatusGroupID, "") & "</select>" & vbCrLf & _
            '                    "&nbsp;<a id=""lnkTurnDownReason"" href=""#"" onclick=""turnDown('" & AppID & "','" & Config.DefaultUserID & "','" & strView & "')"" class=""hidden""><img src=""/images/turndown.gif"" width=""16"" height=""16"" title=""Turn Down application"" border=""0"" /></a>" & _
            '                    "&nbsp;<img id=""imgTurnDownReason"" src=""/images/invalid.png"" width=""12"" height=""12"" title=""Please select a reason"" class=""hidden"" />" & vbCrLf & _
            '                    "</td>" & vbCrLf & _
            '                    "</tr>" & vbCrLf & _
            '                    "<tr>" & vbCrLf
            '                    If (intDialerLoadType = "2") Then
            '                        strOptions += "<td colspan=""4""><hr /><h3 class=""green"">Arrange Call Back:</h3></td>" & vbCrLf & _
            '                        "</tr>" & vbCrLf
            '                        If (checkCallBack(AppID)) Then
            '                            strOptions += _
            '                            "<tr>" & vbCrLf & _
            '                            "<td colspan=""4"" class=""smlc"">Call Back arranged: <strong>" & getAnyField("CallBackDate", "tblapplicationstatus", "AppID", AppID) & "</strong></td>" & vbCrLf & _
            '                            "</tr>" & vbCrLf
            '                        End If
            '                        strOptions += _
            '                        "<tr>" & vbCrLf & _
            '                        "<td colspan=""4"" class=""smlc"">" & vbCrLf & _
            '                        "Date:&nbsp;<input type=""text"" name=""txtCallBackDate"" id=""txtCallBackDate"" class=""small"" value=""" + Config.DefaultDate + """ /><span style=""position: absolute; margin: 5px 0px 0px -20px;""><img id=""chkCallBackDate"" src=""/images/invalid.png"" width=""12"" height=""12"" title=""Please specify a valid date"" class=""hidden"" /></span>" & _
            '                        "&nbsp;Time:&nbsp;<input type=""text"" name=""txtCallBackTime"" id=""txtCallBackTime"" class=""small"" value=""" + Left(Config.DefaultTime, 5) + """ /><span style=""position: absolute; margin: 5px 0px 0px -20px;""><img id=""chkCallBackTime"" src=""/images/invalid.png"" width=""12"" height=""12"" title=""Please specify a valid time"" class=""hidden"" /></span>" & _
            '                        "&nbsp;<a href=""#"" onclick=""arrangeCallBack('" & AppID & "','" & Config.DefaultUserID & "','" & strView & "');""><img src=""/images/callback.gif"" width=""16"" height=""16"" title=""Arrange Call Back"" border=""0"" /></a>" & _
            '                        "</td>" & vbCrLf & _
            '                        "</tr>"
            '                    End If
            '                Else
            '                    strOptions += _
            '                    "<tr>" & vbCrLf & _
            '                    "<td colspan=""4""><hr /><h3 class=""green"">Or select a Turn Down reason:</h3></td>" & vbCrLf & _
            '                    "</tr>" & vbCrLf & _
            '                    "<tr>" & vbCrLf & _
            '                    "<td colspan=""4"" class=""smlc"">Turn Down unavailable.</td>" & vbCrLf & _
            '                    "</tr>" & vbCrLf
            '                End If
        End If
        '       End If
        strOptions += "</table>" & vbCrLf
        'strOptions += "</div>" & vbCrLf
        Return strOptions
    End Function

    Public Function lenderScoring(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String

        Dim strSuccessMessage As String = HttpContext.Current.Request("strSuccessMessage")
        Dim strFailureMessage As String = HttpContext.Current.Request("strFailureMessage")
        Dim strXmlID As String = HttpContext.Current.Request.QueryString("xmlID")
        Dim strSelectedPlan As String = HttpContext.Current.Request.QueryString("selectedPlan")
        Dim strReturnURL As String = "/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&Page=" & HttpContext.Current.Request("Page")
        Dim strClass As String = "row1"

        Dim boolHotkeyed As Boolean = False
        Dim strOptions As String = ""


        strOptions += " <script type=""text/javascript"" src=""/js/hotkey.aspx?v=" & Config.VersionFormScripts & """></script>" & vbCrLf
        'strOptions += "<div id=""hotkeyBox""><br /><br />" & vbCrLf
        strOptions += "<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable table-bordered table-striped table-highlight"" width=""100%"">" & vbCrLf




        If (checkValue(strSuccessMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><div class=""alert alert-success""><h4 class=""alert-heading"">Error</h4><p>" & strSuccessMessage & "</p></div></td>" & vbCrLf & _
            "</tr>"
        End If
        If (checkValue(strFailureMessage)) Then
            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4"" class=""lrgc""><div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>" & strFailureMessage & "</p></div></td>" & vbCrLf & _
            "</tr>"
        End If


        If (checkValue(strSelectedPlan)) Then

            Dim strMediaCampaignID As String = HttpContext.Current.Request.QueryString("MediaCampaignID")
            Dim strMediaCampaignCampaignReference As String = HttpContext.Current.Request.QueryString("MediaCampaignCampaignReference")
            Dim bitUsers As String = HttpContext.Current.Request("bitUsers")
            Dim bitClientReference As String = HttpContext.Current.Request.QueryString("bitClientReference")
            Dim strMediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
            Dim strXMLAddress As String = HttpContext.Current.Request("strXMLAddress")


            Dim strSQL = "SELECT [XMLReceived]" & _
                            "FROM [tblxmlreceived] " & _
                            "WHERE [XMLReceivedID] = " & strXmlID

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()

            If (dsCache.Rows.Count > 0) Then

                strOptions += _
                            "<thead>" & vbCrLf & _
                            "<tr class=""tablehead"">" & vbCrLf & _
                            "<th class=""smlc sort-alpha"">Plan Details</th>" & vbCrLf & _
                            "<th class=""smlc"">Fixed Annual Rate</th>" & vbCrLf & _
                            "<th class=""smlc"">LTV Range</th>" & vbCrLf & _
                            "<th class=""smlc"">Fees</th>" & vbCrLf & _
                            "<th class=""smlc sort-numeric"">Gross Loan Amount</th>" & vbCrLf & _
                            "<th class=""smlc"">Post Fixed Interest Change</th>" & vbCrLf & _
                            "<th class=""smlc"">Post Fixed Interest Rate</th>" & vbCrLf & _
                            "<th class=""smlc""></th>" & vbCrLf & _
                            "</tr>" & vbCrLf & _
                            "</thead>"

                strOptions += _
               "<tr>" & vbCrLf & _
               "<td colspan=""4""><h5>The following plan has been selected:</h5><br /></td>" & vbCrLf & _
               "</tr>"

                Dim strSavedResponse As String = dsCache.Rows(0).Item(0).ToString
                Dim xmlSavedResponse As XmlDocument = New XmlDocument

                xmlSavedResponse.LoadXml(strSavedResponse)

                Dim nsmgr = New XmlNamespaceManager(xmlSavedResponse.NameTable)
                nsmgr.AddNamespace("d2p1", "http://schemas.datacontract.org/2004/07/Shawbrook.Secured.Entities")

                Dim strShawbrookCaseID As String = xmlSavedResponse.SelectSingleNode("/SoftSearchResponse/ShawbrookCaseId", nsmgr).InnerText

                Dim ndlEligiblePlans As XmlNodeList = xmlSavedResponse.SelectNodes("/SoftSearchResponse/EligiblePlanDetails/d2p1:ProductCalculatedDetails", nsmgr)

                For Each node As XmlNode In ndlEligiblePlans

                    Dim strAPR As String = node.SelectSingleNode("d2p1:APR", nsmgr).InnerText
                    Dim strBrokerFee As String = node.SelectSingleNode("d2p1:BrokerFee", nsmgr).InnerText
                    Dim strCommission As String = node.SelectSingleNode("d2p1:Commission", nsmgr).InnerText
                    Dim strPlanName As String = node.SelectSingleNode("d2p1:Plan", nsmgr).InnerText
                    Dim strMonthlyRepayment As String = node.SelectSingleNode("d2p1:MonthlyRepayment", nsmgr).InnerText
                    Dim strFixedAnnualRate As String = node.SelectSingleNode("d2p1:FixedAnnualRate", nsmgr).InnerText
                    Dim strGrossLoanAmount As String = node.SelectSingleNode("d2p1:GrossLoanAmount", nsmgr).InnerText
                    Dim strIsFixedOrVariable As String = node.SelectSingleNode("d2p1:IsFixedOrVariable", nsmgr).InnerText
                    Dim strLenderFee As String = node.SelectSingleNode("d2p1:LenderFee", nsmgr).InnerText
                    Dim strPostFixedInterestCharge As String = node.SelectSingleNode("d2p1:PostFixedInterestCharge", nsmgr).InnerText
                    Dim strPostFixedRate As String = node.SelectSingleNode("d2p1:PostFixedRate", nsmgr).InnerText
                    Dim strMaxLTV As String = node.SelectSingleNode("d2p1:MaxLTV", nsmgr).InnerText
                    Dim strMinLTV As String = node.SelectSingleNode("d2p1:MinLTV", nsmgr).InnerText
                    Dim strFixedTermInMonths As String = node.SelectSingleNode("d2p1:FixedTermInMonths", nsmgr).InnerText
                    Dim strMaxLoan As String = node.SelectSingleNode("d2p1:MaxLoan", nsmgr).InnerText
                    Dim strMinLoan As String = node.SelectSingleNode("d2p1:MinLoan", nsmgr).InnerText
                    Dim strLTVRangeID As String = node.SelectSingleNode("d2p1:LtvRangeId", nsmgr).InnerText

                    If (strIsFixedOrVariable = "true") Then
                        strIsFixedOrVariable = "Fixed"
                    Else
                        strIsFixedOrVariable = "Variable"
                    End If

                    strOptions += _
                            "<tr>" & vbCrLf & _
                            "<td><span >" & strPlanName & "<br /> " & FormatCurrency(strGrossLoanAmount) & " loan for " & strFixedTermInMonths & " Months at " & strFixedAnnualRate & "% <br /> Available Loan £" & strMinLoan.ToString & " - £" & strMaxLoan.ToString & " </span></td>" & vbCrLf & _
                            "<td><span >" & strFixedAnnualRate & "%</span></td>" & vbCrLf & _
                            "<td><span >LTV Range: " & strMinLTV & " - " & strMaxLTV & "</span></td>" & vbCrLf & _
                            "<td><span >Broker Fee: " & FormatCurrency(strBrokerFee) & "<br />Lender Fee: " & FormatCurrency(strLenderFee) & "<br />Commission: " & FormatCurrency(strCommission) & " </span></td>" & vbCrLf & _
                            "<td><span >" & FormatCurrency(strGrossLoanAmount) & "</span></td>" & vbCrLf & _
                            "<td><span >" & FormatCurrency(Decimal.Parse(strPostFixedInterestCharge)).ToString & "</span></td>" & vbCrLf & _
                            "<td><span >" & strPostFixedRate.ToString & "%</span></td>" & vbCrLf & _
                            "<td class=""smlc""><span id=""MessageContainer" & strLTVRangeID & """>" & "Select Plan&nbsp;<a href=""#"" onClick=""selectProduct('Shawbrook','" & strFixedAnnualRate & "','" & strMonthlyRepayment & "','" & strBrokerFee & "','" & strLenderFee & "','" & strPlanName & "','" & strShawbrookCaseID & "','" & strCommission & "', top.$('#panel149'));sendHotkey('" & strLTVRangeID & "','" & AppID & "','" & Config.DefaultUserID & "','" & strMediaCampaignScheduleID & "','" & strXMLAddress.Replace("?AppID=", "?selectedPlan=" & strLTVRangeID & "&xmlID=" & strXmlID & "&AppID=") & "','','','" & bitUsers & "','" & bitClientReference & "','" & MediaCampaignID & "','" & strMediaCampaignID & "','" & strMediaCampaignCampaignReference & "');""><i class=""icon-share-alt"" /></a></span></td>" & vbCrLf & _
                            "</tr>"
                Next


            End If


        Else


            strOptions += _
            "<tr>" & vbCrLf & _
            "<td colspan=""4""><h5>Please select a lender to transfer data to:</h5><br /></td>" & vbCrLf & _
            "</tr>"
            '        If (Not boolHotkeyed) Then
            If (checkValue(strLockUserName)) Then
                strOptions += _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4"" class=""smlc"">Scoring unavailable.</td>" & vbCrLf & _
                    "</tr>"
            Else
                Dim strSQL = "SELECT MediaCampaignID, MediaID, MediaCampaignName, MediaCampaignIDOutbound, MediaCampaignCampaignReference, MediaCampaignClientReferenceRequired, ClientReferenceOutbound, MediaCampaignTelephoneNumberOutbound, MediaCampaignScheduleID, MediaName, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress, MediaCampaignDeliveryToUser, MediaCampaignMaxCostInbound, MediaCampaignCostInbound " & _
                            "FROM vwdistributionsearch " & _
                            "WHERE MediaCampaignDeliveryTypeID = 6 AND MediaCampaignDeliveryBatch = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND MediaCampaignCompanyID = '" & CompanyID & "' ORDER BY MediaCampaignScheduleRequiredRate DESC"
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim boolQualified As Boolean = True
                    Dim y As Integer = 0
                    Dim strRowClass As String = "row1"
                    For Each Row As DataRow In dsCache.Rows

                        Dim strInnerSQL As String = "", strAdhocRules As String = ""
                        strInnerSQL = "SELECT MediaCampaignHotkeyRuleField, MediaCampaignHotkeyRuleComparison, DataTypeCode, MediaCampaignHotkeyRuleValue FROM tblmediacampaignhotkeyrules HKR " & _
                                        "LEFT JOIN tbldatatypes DTY ON DTY.DataTypeID = HKR.MediaCampaignHotkeyRuleDataType " & _
                                        "WHERE (MediaCampaignID = '" & Row.Item("MediaCampaignID") & "' AND CompanyID = '" & CompanyID & "')"
                        Dim dsCacheInner As DataTable = New Caching(Nothing, strInnerSQL, "", "", "").returnCache()
                        If (dsCacheInner.Rows.Count > 0) Then
                            For Each InnerRow As DataRow In dsCacheInner.Rows
                                Dim strRuleField As String = Replace(Replace(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, "SD:", ""), "DS:", "")
                                If (dsCacheInner.Rows.IndexOf(InnerRow) = 0) Then
                                    strAdhocRules += "SELECT AppID FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                                End If
                                If (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "DS:") Then
                                    strAdhocRules += " AND ((SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & strRuleField & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                ElseIf (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "SD:") Then
                                    Select Case strRuleField
                                        Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "Lock"
                                            strAdhocRules += " AND (" & strRuleField & "Date " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                        Case Else
                                            strAdhocRules += " AND ((SELECT ApplicationStatusDate FROM tblapplicationstatusdates WHERE ApplicationStatusDateName = '" & strRuleField & "' AND ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                    End Select
                                Else
                                    strAdhocRules += " AND (" & strRuleField & " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                End If
                            Next
                        End If
                        dsCacheInner = Nothing

                        If (checkValue(strAdhocRules)) Then
                            dsCacheInner = New Caching(Nothing, strAdhocRules, "", "", "").returnCache()
                            If (dsCacheInner.Rows.Count > 0) Then
                                boolQualified = True
                            Else
                                boolQualified = False
                            End If
                        End If

                        If (strRowClass = "even") Then
                            strRowClass = "odd"
                        Else
                            strRowClass = "even"
                        End If
                        If (y = 0) Then
                            strOptions += _
                            "<thead>" & vbCrLf & _
                            "<tr>" & vbCrLf & _
                            "<th>Lender</th>" & vbCrLf & _
                            "<th>Telephone Number</th>" & vbCrLf & _
                            "<th>Action</th>" & vbCrLf & _
                            "<th>Reference</th>" & vbCrLf & _
							"<th>Errors</th>" & vbCrLf & _
                            "</tr>" & vbCrLf & _
                            "</thead>"
                        End If
                        strOptions += _
                            "<tr class=""" & strRowClass & """>" & vbCrLf & _
                            "<td class=""smlc"">" & Row.Item("MediaName") & "<br />" & Row.Item("MediaCampaignName") & "</td>" & vbCrLf & _
                            "<td class=""smlc"">" & Row.Item("MediaCampaignTelephoneNumberOutbound") & "</td>" & vbCrLf & _
                            "<td class=""smlc""><span id=""MessageContainer" & y & """>"
                        If (boolQualified) Then
                            If(Row.Item("MediaCampaignDeliveryXMLPath").Contains("Precise")) Then 
                           	    strOptions += _
                            	"Awaiting Score&nbsp;<a href=""#"" onClick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Select Plan', '/webservices/outbound/Precise/default.aspx?AppID=" & AppID & "&SelectPlan=Y', '600', '600');""><i class=""icon-share-alt"" /></a>"
                       		Else 
								strOptions += _
                            	"Awaiting Score&nbsp;<a href=""#"" onClick=""sendHotkey('" & y & "','" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignScheduleID") & "','outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & "','','','" & Row.Item("MediaCampaignDeliveryToUser") & "','" & Row.Item("MediaCampaignClientReferenceRequired") & "','" & MediaCampaignID & "','" & Row.Item("MediaCampaignID") & "','" & Row.Item("MediaCampaignCampaignReference") & "')""><i class=""icon-share-alt"" /></a>"
							End If 
                        Else
                            strOptions += "Outside criteria"
                        End If
                        strOptions += _
                        "</span></td>" & vbCrLf & _
                        "<td class=""smlc""><span style=""color:green""><strong>" & Row.Item("ClientReferenceOutbound") & "</strong></span></td>" & vbCrLf & _
						"<td class=""smlc""><span style=""color:green""><a href=""#"" onclick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Error Messages', '/prompts/lenderscoringerrors.aspx?AppID=" & AppID & "&LenderName=" & Row.Item("MediaName") & "', '600', '600');""  class=""btn btn-danger btn-small"">Error Message</a></span></td>" & vbCrLf & _
                        "</tr>"
                        y = y + 1
                    Next
                    strOptions += "<input type=""hidden"" id=""NumOptions"" value=""" & y & """ />"
                Else
                    strOptions += _
                    "<tr>" & vbCrLf & _
                    "<td colspan=""4"" class=""smlc"">No transfer options found.</td>" & vbCrLf & _
                    "</tr>"
                End If
                dsCache = Nothing
            End If
        End If
        strOptions += "</table>" & vbCrLf
        'strOptions += "</div>" & vbCrLf
        Return strOptions
    End Function

    Public Function createAuxiliaryProducts(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim MediaCampaignIDInbound As String = ""
        Dim ProductType As String = "", ProductPurpose As String = "", PropertyBuyToLetMonthlyYield As String = "", PropertyType As String = "", PropertyConstructionType As String = "", PropertyLTV As Double = 0.0
        Dim AddressYears As String = "", Amount As String = "", App1EmploymentStatus As String = "", AnnualIncome As String = "", App1EmployerYears As String = "", PropertyValue As String = ""
        Dim MortgageBalance As String = "", MortgageMonths As String = "", AddressLivingArrangement As String = "", App1Age As String = "", UnsecuredCreditorTotal As String = ""
        Dim PropertyNewBuild As String = "", MortgagePaymentsMissed3 As String = "", MortgagePaymentsMissed6 As String = "", MortgagePaymentsMissed12 As String = "", NonMortgagePaymentsMissed12 As String = "", DefaultTotal As String = "", CCJTotal As String = "", DeclaredBankrupt As String = "", DebtManagementPlan As String = "", ProductStartMonths As String = "", MediaCampaignCostInbound As String = ""
        Dim StatusCode As String = "", ExpiredDate As String = "", BankMonthlyOverdraft As String = "", PropertyOnMarket As String = ""
        Dim strClass As String = "row1"
        Dim strSQL As String = "SELECT APP.MediaCampaignIDInbound, APP.ProductType, APP.ProductPurpose, APP.PropertyBuyToLetMonthlyYield, APP.PropertyType, APP.PropertyConstructionType, APP.PropertyLTV, APP.AddressYears, APP.Amount, " & _
            "APP.App1EmploymentStatus, (ISNULL(APP.App1AnnualIncome,0) + ISNULL(APP.App2AnnualIncome,0) + ISNULL(APP.App1AnnualOtherIncome,0) + ISNULL(APP.App2AnnualOtherIncome,0)) AS AnnualIncome, APP.App1EmployerYears, APP.PropertyValue, APP.MortgageBalance, " & _
            "(ISNULL(APP.AddressYears, 0) * 12 + ISNULL(APP.AddressMonths, 0)) AS MortgageMonths, APP.AddressLivingArrangement, APP.App1Age, APP.UnsecuredCreditorTotal, " & _
            "APP.PropertyNewBuild, APP.MortgagePaymentsMissed3, APP.MortgagePaymentsMissed6, APP.MortgagePaymentsMissed12, APP.NonMortgagePaymentsMissed12, APP.DefaultTotal, APP.CCJTotal, APP.DeclaredBankrupt, APP.DebtManagementPlan, APP.ProductStartMonths, APP.MediaCampaignCostInbound, " & _
            "APP.PropertyOnMarket, APS.StatusCode, APP.BankMonthlyOverdraft FROM vwexportapplication APP " & _
            "INNER JOIN tblapplicationstatus APS ON APS.AppID = APP.AppID WHERE APP.AppID = '" & AppID & "' AND APP.CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                MediaCampaignIDInbound = Row.Item("MediaCampaignIDInbound").ToString
                ProductType = Row.Item("ProductType").ToString
                ProductPurpose = Row.Item("ProductPurpose").ToString
                PropertyBuyToLetMonthlyYield = Row.Item("PropertyBuyToLetMonthlyYield").ToString
                PropertyType = Row.Item("PropertyType").ToString
                PropertyConstructionType = Row.Item("PropertyConstructionType").ToString
                PropertyLTV = Row.Item("PropertyLTV")
                AddressYears = Row.Item("AddressYears").ToString
                Amount = Row.Item("Amount").ToString
                App1EmploymentStatus = Row.Item("App1EmploymentStatus").ToString
                AnnualIncome = Row.Item("AnnualIncome").ToString
                App1EmployerYears = Row.Item("App1EmployerYears").ToString
                PropertyValue = Row.Item("PropertyValue").ToString
                MortgageBalance = Row.Item("MortgageBalance").ToString
                MortgageMonths = Row.Item("MortgageMonths").ToString
                AddressLivingArrangement = Row.Item("AddressLivingArrangement").ToString
                App1Age = Row.Item("App1Age").ToString
                UnsecuredCreditorTotal = Row.Item("UnsecuredCreditorTotal")
                PropertyNewBuild = Row.Item("PropertyNewBuild").ToString
                MortgagePaymentsMissed3 = Row.Item("MortgagePaymentsMissed3").ToString
                MortgagePaymentsMissed6 = Row.Item("MortgagePaymentsMissed6").ToString
                MortgagePaymentsMissed12 = Row.Item("MortgagePaymentsMissed12").ToString
                NonMortgagePaymentsMissed12 = Row.Item("NonMortgagePaymentsMissed12").ToString
                DefaultTotal = Row.Item("DefaultTotal").ToString
                CCJTotal = Row.Item("CCJTotal").ToString
                DeclaredBankrupt = Row.Item("DeclaredBankrupt").ToString
                DebtManagementPlan = Row.Item("DebtManagementPlan").ToString
                ProductStartMonths = Row.Item("ProductStartMonths").ToString
                MediaCampaignCostInbound = Row.Item("MediaCampaignCostInbound").ToString
                StatusCode = Row.Item("StatusCode")
                ExpiredDate = getDateField(AppID, "Expired")
                BankMonthlyOverdraft = Row.Item("BankMonthlyOverdraft").ToString
                PropertyOnMarket = Row.Item("PropertyOnMarket").ToString
            Next
        End If
        dsCache = Nothing

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input id=""frmCloneMediaCampaignID"" name=""frmCloneMediaCampaignID"" type=""hidden"" />")
            .WriteLine("<table border=""0"" cellpadding=""2"" class=""table table-bordered table-striped table-highlight"" width=""100%"" align=""center"">")
            .WriteLine("<thead>")
            .WriteLine("<tr>")
            .WriteLine("<th>&nbsp;</th>")
            .WriteLine("<th>Auxiliary</th>")
            .WriteLine("<th>&nbsp;</th>")
            .WriteLine("<th>Reason</th>")
            .WriteLine("<th>&nbsp;</th>")
            .WriteLine("</tr>")
            .WriteLine("</thead>")
            strSQL = "SELECT * FROM vwscheduledrules WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignDeliveryTypeID = 10 ORDER BY MediaName, MediaCampaignName"
            dsCache = New Caching(cache, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim y As Integer = 0
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""smlc""></td>")
                    .WriteLine("<td class=""smlc"" style=""white-space: normal"">" & Row.Item("MediaName") & " - " & Row.Item("MediaCampaignName") & "</td>")
                    Dim strOutsideCriteria As String = ""
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignStatusCodes").ToString, "|" & StatusCode & "|") > 0, "Status", StatusCode, Row.Item("MediaCampaignStatusCodes").ToString)
                    strOutsideCriteria += getFailureReason(Not checkValue(ExpiredDate), "Expiry Date", ddmmyyhhmmss2ddmmhhmm(ExpiredDate), "Lead expired")
                    strOutsideCriteria += getFailureReason(checkInt(Row.Item("MediaCampaignScheduleStartHour")) <= Config.DefaultDateTime.Hour And checkInt(Row.Item("MediaCampaignScheduleEndHour")) >= Config.DefaultDateTime.Hour, "Partner Open", "No", Row.Item("MediaCampaignScheduleStartHour") & ":00 - " & Row.Item("MediaCampaignScheduleEndHour") & ":00")
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignProductTypes").ToString, "|" & ProductType & "|") > 0 Or (Row.Item("MediaCampaignProductTypes").ToString = "||"), "Product Type", ProductType, Row.Item("MediaCampaignProductTypes").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignProductPurposes").ToString, "|" & ProductPurpose & "|") > 0 Or (Row.Item("MediaCampaignProductPurposes").ToString = "||"), "Purpose", ProductPurpose, Row.Item("MediaCampaignProductPurposes").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(PropertyBuyToLetMonthlyYield) >= checkInt(Row.Item("MediaCampaignMinPropertyBuyToLetMonthlyYield").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinPropertyBuyToLetMonthlyYield").ToString)), "BTL Yield", PropertyBuyToLetMonthlyYield, Row.Item("MediaCampaignMinPropertyBuyToLetMonthlyYield").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignPropertyTypes").ToString, "|" & PropertyType & "|") > 0 Or (Row.Item("MediaCampaignPropertyTypes").ToString = "||"), "Property Type", PropertyType, Row.Item("MediaCampaignPropertyTypes").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignPropertyConstructionTypes").ToString, "|" & PropertyConstructionType & "|") > 0 Or (Row.Item("MediaCampaignPropertyConstructionTypes").ToString = "||"), "Construction Type", PropertyConstructionType, Row.Item("MediaCampaignPropertyConstructionTypes").ToString)
                    strOutsideCriteria += getFailureReason((PropertyNewBuild = Row.Item("MediaCampaignPropertyNewBuild").ToString) Or (Not checkValue(Row.Item("MediaCampaignPropertyNewBuild").ToString)), "New Build", PropertyNewBuild, Row.Item("MediaCampaignPropertyNewBuild").ToString)
                    strOutsideCriteria += getFailureReason((PropertyLTV >= checkInt(Row.Item("MediaCampaignMinLTV").ToString) And PropertyLTV <= checkInt(Row.Item("MediaCampaignMaxLTV").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinLTV").ToString) And Not checkValue(Row.Item("MediaCampaignMaxLTV").ToString)), "LTV", PropertyLTV, "Min: " & Row.Item("MediaCampaignMinLTV").ToString & ", Max: " & Row.Item("MediaCampaignMaxLTV").ToString)
                    strOutsideCriteria += getFailureReason((PropertyOnMarket = Row.Item("MediaCampaignPropertyOnMarket").ToString) Or (Not checkValue(Row.Item("MediaCampaignPropertyOnMarket").ToString)), "Property On Market", PropertyOnMarket, Row.Item("MediaCampaignPropertyOnMarket").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(AddressYears) >= checkInt(Row.Item("MediaCampaignMinAddressYears").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinAddressYears").ToString)), "Years at Address", AddressYears, Row.Item("MediaCampaignMinAddressYears").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(Amount) >= checkInt(Row.Item("MediaCampaignMinAmount").ToString) And checkInt(Amount) <= checkInt(Row.Item("MediaCampaignMaxAmount").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinAmount").ToString) And Not checkValue(Row.Item("MediaCampaignMaxAmount").ToString)), "Amount", Amount, "Min: " & Row.Item("MediaCampaignMinAmount").ToString & ", Max: " & Row.Item("MediaCampaignMaxAmount").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignEmploymentStatuses").ToString, "|" & App1EmploymentStatus & "|") > 0 Or (Row.Item("MediaCampaignEmploymentStatuses").ToString = "||"), "Employment Status", App1EmploymentStatus, Row.Item("MediaCampaignEmploymentStatuses").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(AnnualIncome) >= checkInt(Row.Item("MediaCampaignMinIncome").ToString) And checkInt(AnnualIncome) <= checkInt(Row.Item("MediaCampaignMaxIncome").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinIncome").ToString) And Not checkValue(Row.Item("MediaCampaignMaxIncome").ToString)), "Annual Income", AnnualIncome, "Min: " & Row.Item("MediaCampaignMinIncome").ToString & ", Max: " & Row.Item("MediaCampaignMaxIncome").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(Amount) <= checkDec(Row.Item("MediaCampaignIncomeMultiplier").ToString) * checkInt(AnnualIncome)) Or (Not checkValue(Row.Item("MediaCampaignIncomeMultiplier").ToString)), "Amount", Amount, "Affordable: " & formatNumber(checkDec(Row.Item("MediaCampaignIncomeMultiplier").ToString) * checkInt(AnnualIncome), 2))
                    strOutsideCriteria += getFailureReason(((checkInt(BankMonthlyOverdraft) / (AnnualIncome / 12)) * 100 <= checkDec(Row.Item("MediaCampaignMaxMonthlyOverdraftPercentage").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMaxMonthlyOverdraftPercentage").ToString)), "Overdraft", BankMonthlyOverdraft, "Affordable: " & formatNumber(checkDec(Row.Item("MediaCampaignMaxMonthlyOverdraftPercentage").ToString) / 100 * (checkInt(AnnualIncome) / 12), 2))
                    strOutsideCriteria += getFailureReason((checkInt(App1EmployerYears) >= checkInt(Row.Item("MediaCampaignMinEmployerYears").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinEmployerYears").ToString)), "Years at Employer", App1EmployerYears, Row.Item("MediaCampaignMinEmployerYears").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(PropertyValue) >= checkInt(Row.Item("MediaCampaignMinPropertyValue").ToString) And checkInt(PropertyValue) <= checkInt(Row.Item("MediaCampaignMaxPropertyValue").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinPropertyValue").ToString) And Not checkValue(Row.Item("MediaCampaignMaxPropertyValue").ToString)), "Property Value", PropertyValue, "Min: " & Row.Item("MediaCampaignMinPropertyValue").ToString & ", Max: " & Row.Item("MediaCampaignMaxPropertyValue").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MortgageBalance) >= checkInt(Row.Item("MediaCampaignMinMortgageBalance").ToString) And checkInt(MortgageBalance) <= checkInt(Row.Item("MediaCampaignMaxMortgageBalance").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinMortgageBalance").ToString) And Not checkValue(Row.Item("MediaCampaignMaxMortgageBalance").ToString)), "Mortgage Balance", MortgageBalance, "Min: " & Row.Item("MediaCampaignMinMortgageBalance").ToString & ", Max: " & Row.Item("MediaCampaignMaxMortgageBalance").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MortgageMonths) >= checkInt(Row.Item("MediaCampaignMinMortgageMonths").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinMortgageMonths").ToString)), "Months in Mortgage", MortgageMonths, Row.Item("MediaCampaignMinMortgageMonths").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignLivingArrangement").ToString, "|" & AddressLivingArrangement & "|") > 0 Or (Row.Item("MediaCampaignLivingArrangement").ToString = "||"), "Living Arrangement", AddressLivingArrangement, Row.Item("MediaCampaignLivingArrangement").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(App1Age) >= checkInt(Row.Item("MediaCampaignMinAge").ToString) And checkInt(App1Age) <= checkInt(Row.Item("MediaCampaignMaxAge").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinAge").ToString) And Not checkValue(Row.Item("MediaCampaignMaxAge").ToString)), "Age", App1Age, "Min: " & Row.Item("MediaCampaignMinAge").ToString & ", Max: " & Row.Item("MediaCampaignMaxAge").ToString)
                    'strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignCountry").ToString, "||") > 0 Or (Row.Item("MediaCampaignCountry").ToString = "||"), "Country", AddressCountry, Row.Item("MediaCampaignCountry").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(UnsecuredCreditorTotal) >= checkInt(Row.Item("MediaCampaignMinUnsecuredCreditors").ToString) And checkInt(UnsecuredCreditorTotal) <= checkInt(Row.Item("MediaCampaignMaxUnsecuredCreditors").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinUnsecuredCreditors").ToString) And Not checkValue(Row.Item("MediaCampaignMaxUnsecuredCreditors").ToString)), "Unsecured Creditors", UnsecuredCreditorTotal, "Min: " & Row.Item("MediaCampaignMinUnsecuredCreditors").ToString & ", Max: " & Row.Item("MediaCampaignMaxUnsecuredCreditors").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MortgagePaymentsMissed3) >= checkInt(Row.Item("MediaCampaignMinMortgagePaymentsMissed3").ToString) And checkInt(MortgagePaymentsMissed3) <= checkInt(Row.Item("MediaCampaignMaxMortgagePaymentsMissed3").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinMortgagePaymentsMissed3").ToString) And Not checkValue(Row.Item("MediaCampaignMaxMortgagePaymentsMissed3").ToString)), "Mortgage Payments Missed Last 3", MortgagePaymentsMissed3, "Min: " & Row.Item("MediaCampaignMinMortgagePaymentsMissed3").ToString & ", Max " & Row.Item("MediaCampaignMaxMortgagePaymentsMissed3").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MortgagePaymentsMissed6) >= checkInt(Row.Item("MediaCampaignMinMortgagePaymentsMissed6").ToString) And checkInt(MortgagePaymentsMissed6) <= checkInt(Row.Item("MediaCampaignMaxMortgagePaymentsMissed6").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinMortgagePaymentsMissed6").ToString) And Not checkValue(Row.Item("MediaCampaignMaxMortgagePaymentsMissed6").ToString)), "Mortgage Payments Missed Last 6", MortgagePaymentsMissed6, "Min: " & Row.Item("MediaCampaignMinMortgagePaymentsMissed6").ToString & ", Max " & Row.Item("MediaCampaignMaxMortgagePaymentsMissed6").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MortgagePaymentsMissed12) >= checkInt(Row.Item("MediaCampaignMinMortgagePaymentsMissed12").ToString) And checkInt(MortgagePaymentsMissed12) <= checkInt(Row.Item("MediaCampaignMaxMortgagePaymentsMissed12").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinMortgagePaymentsMissed12").ToString) And Not checkValue(Row.Item("MediaCampaignMaxMortgagePaymentsMissed12").ToString)), "Mortgage Payments Missed Last 12", MortgagePaymentsMissed12, "Min: " & Row.Item("MediaCampaignMinMortgagePaymentsMissed12").ToString & ", Max " & Row.Item("MediaCampaignMaxMortgagePaymentsMissed12").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(NonMortgagePaymentsMissed12) >= checkInt(Row.Item("MediaCampaignMinNonMortgagePaymentsMissed12").ToString) And checkInt(NonMortgagePaymentsMissed12) <= checkInt(Row.Item("MediaCampaignMaxNonMortgagePaymentsMissed12").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinNonMortgagePaymentsMissed12").ToString) And Not checkValue(Row.Item("MediaCampaignMaxNonMortgagePaymentsMissed12").ToString)), "Credit Payments Missed Last 12", NonMortgagePaymentsMissed12, "Min: " & Row.Item("MediaCampaignMinNonMortgagePaymentsMissed12").ToString & ", Max " & Row.Item("MediaCampaignMaxNonMortgagePaymentsMissed12").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(DefaultTotal) >= checkInt(Row.Item("MediaCampaignMinDefaultTotal").ToString)) And (checkInt(DefaultTotal) <= checkInt(Row.Item("MediaCampaignMaxDefaultTotal").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinDefaultTotal").ToString) And Not checkValue(Row.Item("MediaCampaignMaxDefaultTotal").ToString)), "Defaults", DefaultTotal, "Min: " & Row.Item("MediaCampaignMinDefaultTotal").ToString & ", Max: " & Row.Item("MediaCampaignMaxDefaultTotal").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(CCJTotal) >= checkInt(Row.Item("MediaCampaignMinCCJTotal").ToString)) And (checkInt(CCJTotal) <= checkInt(Row.Item("MediaCampaignMaxCCJTotal").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMinCCJTotal").ToString) And Not checkValue(Row.Item("MediaCampaignMaxCCJTotal").ToString)), "CCJs", CCJTotal, "Min: " & Row.Item("MediaCampaignMinCCJTotal").ToString & ", Max: " & Row.Item("MediaCampaignMaxCCJTotal").ToString)
                    strOutsideCriteria += getFailureReason((DeclaredBankrupt = Row.Item("MediaCampaignBankruptcyAllowed").ToString) Or (Not checkValue(Row.Item("MediaCampaignBankruptcyAllowed").ToString)), "Declared Bankrupt", DeclaredBankrupt, Row.Item("MediaCampaignBankruptcyAllowed").ToString)
                    strOutsideCriteria += getFailureReason((DebtManagementPlan = Row.Item("MediaCampaignBankruptcyAllowed").ToString) Or (Not checkValue(Row.Item("MediaCampaignDMPAllowed").ToString)), "Debt Management Plan", DebtManagementPlan, Row.Item("MediaCampaignDMPAllowed").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(ProductStartMonths) <= checkInt(Row.Item("MediaCampaignProductStartMonths").ToString)) Or (Not checkValue(Row.Item("MediaCampaignProductStartMonths").ToString)), "Product Start Period", ProductStartMonths, Row.Item("MediaCampaignProductStartMonths").ToString)
                    strOutsideCriteria += getFailureReason((checkInt(MediaCampaignCostInbound) <= checkInt(Row.Item("MediaCampaignMaxCostInbound").ToString)) Or (Not checkValue(Row.Item("MediaCampaignMaxCostInbound").ToString)), "Lead Cost", MediaCampaignCostInbound, "Max: " & Row.Item("MediaCampaignMaxCostInbound").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignExemptMediaCampaignID").ToString, "|" & MediaCampaignIDInbound & "|") = 0 Or (Row.Item("MediaCampaignExemptMediaCampaignID").ToString = "||"), "Exempt From", getAnyField("MediaCampaignName", "tblmediacampaigns", "MediaCampaignID", MediaCampaignIDInbound), Row.Item("MediaCampaignExemptMediaCampaignID").ToString)
                    strOutsideCriteria += getFailureReason(InStr(Row.Item("MediaCampaignMediaCampaignID").ToString, "|" & MediaCampaignIDInbound & "|") > 0 Or (Row.Item("MediaCampaignMediaCampaignID").ToString = "||"), "Only From", getAnyField("MediaCampaignName", "tblmediacampaigns", "MediaCampaignID", MediaCampaignIDInbound), Row.Item("MediaCampaignMediaCampaignID").ToString)
                    strOutsideCriteria += getFailureReason(checkInt(Row.Item("MediaCampaignScheduleDelivered")) < checkInt(Row.Item("MediaCampaignDeliveryRequired")), "Leads Required", Row.Item("MediaCampaignScheduleDelivered"), Row.Item("MediaCampaignDeliveryRequired"))

                    Dim strInnerSQL As String = "", strAdhocRules As String = "", strActualValueSQL As String = ""
                    strInnerSQL = "SELECT MediaCampaignHotkeyRuleField, MediaCampaignHotkeyRuleComparison, DataTypeCode, MediaCampaignHotkeyRuleValue FROM tblmediacampaignhotkeyrules HKR " & _
                                    "LEFT JOIN tbldatatypes DTY ON DTY.DataTypeID = HKR.MediaCampaignHotkeyRuleDataType " & _
                                    "WHERE (MediaCampaignID = '" & Row.Item("MediaCampaignID") & "' AND CompanyID = '" & CompanyID & "')"
                    Dim dsCacheInner As DataTable = New Caching(Nothing, strInnerSQL, "", "", "").returnCache()
                    If (dsCacheInner.Rows.Count > 0) Then
                        For Each InnerRow As DataRow In dsCacheInner.Rows
                            Dim strRuleField As String = Replace(Replace(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, "SD:", ""), "DS:", "")
                            If (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "DS:") Then
                                strAdhocRules = "SELECT (SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & strRuleField & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AS DatastoreValue FROM vwexportapplication WHERE ((SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & strRuleField & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                Dim strValue As String = New Caching(Nothing, strAdhocRules, "", "", "").returnCacheString
                                If (Not checkValue(strValue)) Then
                                    strActualValueSQL = "SELECT (SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & strRuleField & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AS DatastoreValue FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                                    Dim strActualValue As String = New Caching(Nothing, strActualValueSQL, "", "", "").returnCacheString
                                    strOutsideCriteria += getFailureReason(False, strRuleField, strActualValue, " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString)
                                End If
                            ElseIf (Left(InnerRow.Item("MediaCampaignHotkeyRuleField").ToString, 3) = "SD:") Then
                                Select Case strRuleField
                                    Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "Lock"
                                        strAdhocRules = "SELECT " & strRuleField & "Date FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND (" & strRuleField & "Date " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                        Dim strValue As String = New Caching(Nothing, strAdhocRules, "", "", "").returnCacheString
                                        If (Not checkValue(strValue)) Then
                                            strActualValueSQL = "SELECT " & strRuleField & "Date FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                                            Dim strActualValue As String = New Caching(Nothing, strActualValueSQL, "", "", "").returnCacheString
                                            strOutsideCriteria += getFailureReason(False, strRuleField, strActualValue, " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString)
                                        End If
                                    Case Else
                                        strAdhocRules = "SELECT (SELECT ApplicationStatusDate FROM tblapplicationstatusdates WHERE ApplicationStatusDateName = '" & strRuleField & "' AND ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AS DateValue FROM vwexportapplication WHERE ((SELECT ApplicationStatusDate FROM tblapplicationstatusdates WHERE ApplicationStatusDateName = '" & strRuleField & "' AND ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')" & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                        Dim strValue As String = New Caching(Nothing, strAdhocRules, "", "", "").returnCacheString
                                        If (Not checkValue(strValue)) Then
                                            strActualValueSQL = "SELECT (SELECT ApplicationStatusDate FROM tblapplicationstatusdates WHERE ApplicationStatusDateName = '" & strRuleField & "' AND ApplicationStatusDateAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "') AS DateValue FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                                            Dim strActualValue As String = New Caching(Nothing, strActualValueSQL, "", "", "").returnCacheString
                                            strOutsideCriteria += getFailureReason(False, strRuleField, strActualValue, " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString)
                                        End If
                                End Select
                            Else
                                strAdhocRules = "SELECT " & strRuleField & " FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND (" & strRuleField & " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & formatField(InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString, InnerRow.Item("DataTypeCode").ToString, "") & ")"
                                Dim strValue As String = New Caching(Nothing, strAdhocRules, "", "", "").returnCacheString
                                If (Not checkValue(strValue)) Then
                                    strActualValueSQL = "SELECT " & strRuleField & " FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
                                    Dim strActualValue As String = New Caching(Nothing, strActualValueSQL, "", "", "").returnCacheString
                                    strOutsideCriteria += getFailureReason(False, strRuleField, strActualValue, " " & InnerRow.Item("MediaCampaignHotkeyRuleComparison").ToString & " " & InnerRow.Item("MediaCampaignHotkeyRuleValue").ToString)
                                End If
                            End If
                        Next
                    End If
                    dsCacheInner = Nothing

                    If (checkValue(strOutsideCriteria)) Then
                        .WriteLine("<td class=""smlc"">" & tickCross(True, False) & "</td>")
                        .WriteLine("<td class=""sml"" style=""white-space: normal""><ul class=""bullet"" style=""margin: 0px 0px 0px 0px; padding: 0px 0px 0px 15px;"">" & strOutsideCriteria & "</ul></td>")
                    Else
                        .WriteLine("<td class=""smlc"">" & tickCross(True, True) & "</td>")
                        .WriteLine("<td class=""sml"" style=""white-space: normal"">&nbsp;</td>")
                    End If
                    If (Not checkValue(strOutsideCriteria) And (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N")) Then
                        .WriteLine("<td class=""smlc""><span id=""MessageContainer" & y & """><a href=""#"" onclick=""cloneApplication(" & y & ",'" & AppID & "','" & Config.DefaultUserID & "','" & Row.Item("MediaCampaignCampaignReference") & "')""><button class=""btn btn-primary btn-mini"">Create</button></a></span></td>")
                    Else
                        .WriteLine("<td class=""sml"" style=""white-space: normal""><span id=""MessageContainer" & y & """>&nbsp;</span></td>")
                    End If
                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                    y += 1
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""5"" class=""smlc"">No auxiliaries found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</tr>")
            .WriteLine("</table><br /><br />")
        End With
        Return objStringWriter.ToString
    End Function

    Private Function getFailureReason(ByVal compare As Boolean, ByVal nm As String, ByVal val As String, ByVal rule As String) As String
        If (Not checkValue(val)) Then
            val = "Please fill in"
        End If
        If (Not compare) Then
            Return "<li>" & nm & " = " & val & " (" & Replace(rule, "|", " ") & ")</li>"
        Else
            Return ""
        End If
    End Function

    Public Enum DeliveryType
        IncomingData = 1
        XMLTransfer = 2
        Email = 3
        EmailCSV = 4
        VoiceOnly = 5
        XMLLenderScoring = 6
        IssueDocuments = 7
        EmailTextOnly = 8
        SalesTransfer = 9
    End Enum

    Public Function checkTurnDown(ByVal AppID As String) As Boolean
        Dim boolTurnedDown As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus " & _
                                "WHERE AppID = '" & AppID & "' AND (WTDDate IS NULL AND TUDDate IS NULL AND INVPostDialerDate IS NULL) AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolTurnedDown = False
            Next
        Else
            boolTurnedDown = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolTurnedDown
    End Function

    Public Function checkCallBack(ByVal AppID As String) As Boolean
        Dim boolCallBack As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus " & _
                                "WHERE AppID = '" & AppID & "' AND (CallBackDate IS NULL) AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolCallBack = False
            Next
        Else
            boolCallBack = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolCallBack
    End Function

    Public Function checkSurveyDate(ByVal AppID As String) As Boolean
        Dim boolSurveyComplete As Boolean = False
        Dim strSQL As String = "SELECT AppID FROM tblapplicationstatus " & _
                                "WHERE AppID = '" & AppID & "' AND (SurveyDate IS NULL) AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim dsApplication As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (dsApplication.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplication.Rows
                boolSurveyComplete = False
            Next
        Else
            boolSurveyComplete = True
        End If
        dsApplication.Clear()
        dsApplication = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolSurveyComplete
    End Function

    Public Function subStatusList(ByVal cache As Web.Caching.Cache, ByVal txt As String, ByVal StatusID As String, ByVal SubStatusGroupID As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT SST.SubStatusCode, SST.SubStatusDescription FROM tblsubstatuses SST " & _
            "INNER JOIN tblstatusmapping MAP ON MAP.SubStatusID = SST.SubStatusID " & _
            "WHERE SST.CompanyID = '" & CompanyID & "' AND MAP.StatusID = '" & StatusID & "' AND SubStatusActive = 1 AND " & _
            "EXISTS (SELECT SubStatusID FROM tblsubstatusgroupmapping GRM WHERE GRM.SubStatusID = SST.SubStatusID AND GRM.SubStatusGroupID = '" & SubStatusGroupID & "' AND GRM.CompanyID = '" & CompanyID & "') " & _
            "ORDER BY SubStatusDescription"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("SubStatusCode") & """")
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        objStringBuilder.Append(" selected=""selected""")
                    End If
                End If
                objStringBuilder.Append(">" & Row.Item("SubStatusDescription") & "</option>")
            Next
        End If
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

    Public Function getSubStatusByStatusCode(ByVal cache As Web.Caching.Cache, ByVal AppID As String, ByVal txt As String, ByVal StatusCode As String) As String
        Dim strSQL As String = "SELECT SST.SubStatusCode, SST.SubStatusDescription FROM tblsubstatuses SST " & _
                     "INNER JOIN tblstatusmapping MAP ON MAP.SubStatusID = SST.SubStatusID " & _
                     "INNER JOIN tblstatuses ST ON ST.StatusID = MAP.StatusID " & _
                     "WHERE (SST.CompanyID = '" & CompanyID & "' OR SST.CompanyID = 0) AND ST.StatusCode = '" & StatusCode & "' AND SubStatusActive = 1 " & _
                     "ORDER BY SubStatusDescription"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<select id=""SubStatusCode"" name=""SubStatusCode"" class=""text mandatory"">")
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("SubStatusCode") & """>" & Row.Item("SubStatusDescription") & "</option>")
            Next
        End If
        objStringBuilder.Append("</option>")
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

     Public Function getNotes(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As New StringWriter
        Dim strReturnURL As String = "/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&Page=" & HttpContext.Current.Request("Page")
        With objStringWriter
            .WriteLine("<div class=""widget-tabs""><ul class=""nav nav-tabs""><li class=""active""><a href=""#user""><i class=""icon-user""></i> User</a></li><li><a href=""#system""><i class=""icon-laptop""></i> System</a></li><li><a href=""#advise""><i class=""icon-laptop""></i> Advise Notes</a></li></ul></div><!-- /.widget-tabs -->")
            .WriteLine("<div id=""notes-container"" style=""overflow: auto;"">")
            .WriteLine("<div class=""tab-content"">")
            .WriteLine("<div class=""tab-pane active"" id=""user"">")
            .WriteLine("<table id=""notes-table"" class=""table sortable"">")
            Dim strSQL As String = "SELECT NTS.NoteID, NTS.Note, NTS.NoteType, NTS.CreatedDate, USR.UserFullName, USR.UserID FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                "WHERE AppID = '" & AppID & "' AND NTS.CompanyID = '" & CompanyID & "' AND UserFullName <> 'System' AND NoteActive = 1 And (AdvisedNote = 'false') ORDER BY NTS.NoteType, NTS.CreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            Dim x As Integer = 0
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""tablehead"">")
                    '.WriteLine("<td class=""smlc"" style=""padding: 5px 5px 5px 5px;""><img src=""/images/icons/note.gif"" width=""16"" height=""16"" border=""0"" /></td>")
                    Dim strClass As String = ""
                    'If (Row.Item("UserFullName") = "System") Then
                    'strClass = " system-note"
                    'Else
                    'strClass = ""
                    'End If
                    If (Row.Item("NoteType") = 1) Then ' Sticky note
                        .WriteLine("<td class=""sml sticky"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        .WriteLine("<i class=""icon-exclamation-sign""></i>&nbsp;<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;<span class=""stickyNote"">" & Row.Item("Note") & "</span><br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                        .WriteLine("<span style=""float: right; margin: -2px 4px 0px 0px;""><a href=""#"" onclick=""editStickyNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-pencil"" title=""Edit Note""></i></a></span>")
                    Else
                        .WriteLine("<td class=""sml" & strClass & """ style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        If (objLeadPlatform.Config.Partner Or objLeadPlatform.Config.Supplier) Then
                            .WriteLine("<span class=""title"">User " & Row.Item("UserID") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        Else
                            .WriteLine("<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        End If
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                    End If
                    .WriteLine("</div></td>")
                    .WriteLine("</tr>")
                    x += 1
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No notes found.</td>")
                .WriteLine("</tr>")
            End If

            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            .WriteLine("<div class=""tab-pane"" id=""system"">")
            .WriteLine("<table id=""system-notes-table"" class=""table sortable"">")
            strSQL = "SELECT NTS.NoteID, NTS.Note, NTS.NoteType, NTS.CreatedDate, USR.UserFullName, USR.UserID FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                "WHERE AppID = '" & AppID & "' AND NTS.CompanyID = '" & CompanyID & "' AND UserFullName = 'System' AND NoteActive = 1 ORDER BY NTS.NoteType, NTS.CreatedDate DESC"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache
            x = 0
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""tablehead"">")
                    '.WriteLine("<td class=""smlc"" style=""padding: 5px 5px 5px 5px;""><img src=""/images/icons/note.gif"" width=""16"" height=""16"" border=""0"" /></td>")
                    Dim strClass As String = ""
                    'If (Row.Item("UserFullName") = "System") Then
                    'strClass = " system-note"
                    'Else
                    'strClass = ""
                    'End If
                    If (Row.Item("NoteType") = 1) Then ' Sticky note
                        .WriteLine("<td class=""sml sticky"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        .WriteLine("<i class=""icon-exclamation-sign""></i>&nbsp;<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;<span class=""stickyNote"">" & Row.Item("Note") & "</span><br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                        .WriteLine("<span style=""float: right; margin: -2px 4px 0px 0px;""><a href=""#"" onclick=""editStickyNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-pencil"" title=""Edit Note""></i></a></span>")
                    Else
                        .WriteLine("<td class=""sml" & strClass & """ style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        If (objLeadPlatform.Config.Partner Or objLeadPlatform.Config.Supplier) Then
                            .WriteLine("<span class=""title"">User " & Row.Item("UserID") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        Else
                            .WriteLine("<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        End If
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                    End If
                    .WriteLine("</div></td>")
                    .WriteLine("</tr>")
                    x += 1
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No notes found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            .WriteLine("<div class=""tab-pane"" id=""advise"">")
            .WriteLine("<table id=""system-notes-table"" class=""table sortable"">")
            strSQL = "SELECT NTS.NoteID, NTS.Note, NTS.NoteType, NTS.CreatedDate, USR.UserFullName, USR.UserID FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                "WHERE AppID = '" & AppID & "' AND NTS.CompanyID = '" & CompanyID & "' AND NoteActive = 1 and AdvisedNote='true' ORDER BY NTS.NoteType, NTS.CreatedDate DESC"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache
            x = 0
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""tablehead"">")
                    '.WriteLine("<td class=""smlc"" style=""padding: 5px 5px 5px 5px;""><img src=""/images/icons/note.gif"" width=""16"" height=""16"" border=""0"" /></td>")
                    Dim strClass As String = ""
                    'If (Row.Item("UserFullName") = "System") Then
                    'strClass = " system-note"
                    'Else
                    'strClass = ""
                    'End If
                    If (Row.Item("NoteType") = 1) Then ' Sticky note
                        .WriteLine("<td class=""sml sticky"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        .WriteLine("<i class=""icon-exclamation-sign""></i>&nbsp;<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;<span class=""stickyNote"">" & Row.Item("Note") & "</span><br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                        .WriteLine("<span style=""float: right; margin: -2px 4px 0px 0px;""><a href=""#"" onclick=""editStickyNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-pencil"" title=""Edit Note""></i></a></span>")
                    Else
                        .WriteLine("<td class=""sml" & strClass & """ style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        If (objLeadPlatform.Config.Partner Or objLeadPlatform.Config.Supplier) Then
                            .WriteLine("<span class=""title"">User " & Row.Item("UserID") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        Else
                            .WriteLine("<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        End If
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                    End If
                    .WriteLine("</div></td>")
                    .WriteLine("</tr>")
                    x += 1
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No notes found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            If (Not objLeadPlatform.Config.Supplier) Then
                .WriteLine("<div id=""input-container"" class=""input-append"">")
                .WriteLine("<br /><textarea name=""frmNote"" id=""frmNote"" class=""text mandatory note"" title=""Note""></textarea><button class=""btn btn-primary btn-small"" type=""button"" onclick=""if (checkPageMandatory()) { dynamicNote(" & AppID & "," & Config.DefaultUserID & "); }"">Go</button><input id=""frmSticky"" name=""frmSticky"" type=""hidden"" value=""0""><button id=""sticky"" name=""sticky"" class=""btn btn-primary btn-small"" data-toggle=""button"" onclick=""toggleStickyNote()""><i class=""icon-exclamation-sign""></i></button>")
                '.WriteLine("<table border=""0"" cellpadding=""0"" class=""table"" align=""center"" width=""100%"">")
                '.WriteLine("<tr>")
                '.WriteLine("<td class=""smlc"">")
                '.WriteLine("<span style=""float: right; margin: 0px 0px 0px 0px;"">")
                '.WriteLine("<button class=""btn btn-primary btn-small"" onclick=""if (checkPageMandatory()) { dynamicNote(" & AppID & "," & Config.DefaultUserID & "); }""></i>Go</button>")
                '.WriteLine("<br /><img src=""/images/icons/note_important.png"" width=""16"" height=""16"" />&nbsp;<input id=""cbSticky"" name=""cbSticky"" type=""checkbox"" />")
                '.WriteLine("</span>")
                'If (intWorkflowActivityID = ActivityType.StartCall Or objLeadPlatform.Config.CallInterface = "N") Then
                '.WriteLine("<textarea name=""frmNote"" id=""frmNote"" class=""text mandatory note"" title=""Note""></textarea>")
                'Else
                '.WriteLine("<textarea name=""frmNote"" id=""frmNote"" class=""text note"" title=""Note"" readonly=""readonly"" disabled=""disabled""></textarea>")
                'End If
                'If (Not checkValue(strLockUserName)) Then
                'If (intWorkflowActivityID = ActivityType.StartCall Or objLeadPlatform.Config.CallInterface = "N") Then
                'End If
                'Else
                '.WriteLine("<input id=""go"" name=""go"" type=""button"" value=""Case locked"" class=""cancel"" />")
                'End If
                '.WriteLine("</td>")
                '.WriteLine("</tr>")
                '.WriteLine("</table>")
                .WriteLine("</div>")
            End If
            If (objLeadPlatform.Config.SuperAdmin Or objLeadPlatform.Config.Manager) Then
                .WriteLine("<script type=""text/javascript"">")
                .WriteLine("$(document).ready(function () {")
                .WriteLine("    $('textarea.note').autoResize({ extraSpace: 0, callBack: 'resizeNotesPrompt()' }); resizeNotesPrompt();")
                .WriteLine("    $('.rollover').rollover( { fade:false } );")
                .WriteLine("    $('#notes-table tr td div, #system-notes-table tr td div').mouseover(function() {")
                .WriteLine("        $(this).children('span').children('.delete').removeClass('displayNone').addClass('displayBlock');")
                .WriteLine("        $(this).parent().addClass('rowHighlight');")
                .WriteLine("    });")
                .WriteLine("    $('#notes-table tr td div, #system-notes-table tr td div').mouseout(function() {")
                .WriteLine("        $(this).children('span').children('.delete').removeClass('displayBlock').addClass('displayNone');")
                .WriteLine("        $(this).parent().removeClass('rowHighlight');")
                .WriteLine("    });")
                .WriteLine("});")
                .WriteLine("</script>")
            End If
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getLetterHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            If (intWorkflowActivityID = ActivityType.StartCase Or ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                .WriteLine("<a href=""#"" onclick=""top.uploadPrompt(" & AppID + ")"" class=""salesButton""><button class=""btn btn-primary btn-small""><i class=""icon-file""></i>Upload</button></a><br /><br />")
            End If
            '.WriteLine("<div style=""height: 160px; overflow: auto;"">")
            .WriteLine("<table class=""table table-bordered table-striped table-highlight"" width=""100%"" align=""center"">")
            Dim strSQL As String = "SELECT PDFHistoryID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryBody, PDFEditable, UserReference, UserFullName, PDFHistoryCreatedDate, PDFPackIDs FROM tblpdfhistory INNER JOIN tblusers ON tblusers.UserID = tblpdfhistory.PDFHistoryCreatedUserID " & _
                                    "WHERE AppID = '" & AppID & "' ORDER BY PDFHistoryCreatedDate DESC"

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strOpenLink As String = "", strEditLink As String = "", strSaveLink As String = "", strDeleteLink As String = ""
                    If (checkValue(Row.Item("PDFHistoryFileName"))) Then
                        strOpenLink = "javascript:top.openFileAttachment('" & Row.Item("PDFHistoryFileName") & "');"
                        strSaveLink = "javascript:saveFileAttachment('" & Row.Item("PDFHistoryFileName") & "');"
                        strDeleteLink = "&PDFHistoryFileName=" & Row.Item("PDFHistoryFileName")
                    ElseIf (checkValue(Row.Item("PDFHistoryBody"))) Then
                        If (Row.Item("PDFEditable")) Then
                            strOpenLink = "javascript:top.openHistoryLetter(" & Row.Item("PDFHistoryID") & ");"
                            strEditLink = "javascript:top.editLetter('','" & Row.Item("PDFHistoryID") & "', " & AppID & ",'" & getIndexNumber() & "')"");"
                        Else
                            strOpenLink = "javascript:top.openHistoryLetter(" & Row.Item("PDFHistoryID") & ");"
                        End If
                        strSaveLink = "javascript:saveHistoryLetter(" & Row.Item("PDFHistoryID") & ");"
                    ElseIf (checkValue(Row.Item("PDFPackIDs").ToString)) Then
                        strOpenLink = "javascript:top.openHistoryPack(" & Row.Item("PDFHistoryID") & "," & Row.Item("PDFPackIDs") & ");"
                        strSaveLink = "javascript:saveHistoryPack(" & Row.Item("PDFHistoryID") & "," & Row.Item("PDFPackIDs") & ");"
                    Else
                        strOpenLink = "javascript:top.openHistoryPDF(" & Row.Item("PDFHistoryID") & ");"
                        strSaveLink = "javascript:saveHistoryPDF(" & Row.Item("PDFHistoryID") & ");"
                    End If

                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                    .WriteLine("<tr class=""" & strClass & """>")
                    If (checkValue(strFileExt)) Then
                        .WriteLine("<td class=""smlc""><span class=""label label-info"">" & strFileExt & "</span></td>")
                    Else
                        .WriteLine("<td class=""smlc""><span class=""label label-info"">pdf</span></td>")
                    End If
                    .WriteLine("<td><a href=""" & strOpenLink & """ class=""ui-tooltip"" title=""" & Row.Item("PDFHistoryNote") & """ data-placement=""right"">" & strPDFName & "</a></td>")
                    .WriteLine("<td class=""smlc""><nobr>" & divideRound(Row.Item("PDFHistoryFileSize"), 1024, 0) & " kb</nobr></td>")
                    .WriteLine("<td class=""smlc"" title=""" & Row.Item("PDFHistoryCreatedDate") & """>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PDFHistoryCreatedDate")) & "</td>")
                    .WriteLine("<td class=""smlc"" title=""" & Row.Item("UserFullName") & """>" & Row.Item("UserReference") & "</td>")
                    .WriteLine("<td class=""smlc""><nobr>")
                    If (Row.Item("PDFEditable")) Then
                        .WriteLine("<a href=""" & strOpenLink & """ title=""View Document""><button class=""btn btn-primary btn-mini""><i class=""icon-search""></i> View</button></a>")
                        .WriteLine("&nbsp;<a href=""" & strEditLink & """ title=""View Document""><button class=""btn btn-primary btn-mini""><i class=""icon-edit""></i> Edit</button></a>")
                    Else
                        .WriteLine("<a href=""" & strOpenLink & """ title=""View Document""><button class=""btn btn-primary btn-mini""><i class=""icon-search""></i> View</button></a>")
                    End If
                    .WriteLine("&nbsp;<a href=""" & strSaveLink & """ title=""Save Document""><button class=""btn btn-primary btn-mini""><i class=""icon-download""></i> Save</button></a>")
                    If (objLeadPlatform.Config.SuperAdmin Or objLeadPlatform.Config.SeniorManager) Then
                        .WriteLine("&nbsp;<a href=""#"" onClick=""removeDocument('" & Row.Item("PDFHistoryName") & "','/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strReturnURL) & "&strThisPg=document&strFrmAction=delete&PDFHistoryID=" & Row.Item("PDFHistoryID") & strDeleteLink & "')""><button class=""btn btn-danger btn-mini""><i class=""icon-remove""></i> Remove</button></a>")
                    End If
                    .WriteLine("</nobr></td>")
                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""6"" class=""smlc"">No documents found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing

            'ESIS and EoR Document Load 
            Dim strSQL1 As String = "select * from tblpdffiles where AppID = " & AppID & ""
            Dim dsCache1 As DataTable = New Caching(Nothing, strSQL1, "", "", "").returnCache()
            If (dsCache1.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache1.Rows
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""smlc""><span class=""label label-info"">pdf</span></td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("Name") & "</td>")
                    .WriteLine("<td class=""smlc"">0 kb</td>")
                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("LenderName") & " - " & Row.Item("PlanName") & "</td>")
                    .WriteLine("<td class=""smlc""><a class=""btn btn-primary btn-mini"" href=""/quotes.aspx?ESIS=Y&AppID=" & AppID & "&NewID=" & Row.Item("ID") & """><i class=""icon-search""></i> View</a></td>")
                    .WriteLine("</tr>")
                Next
            Else
            End If
            dsCache1 = Nothing
            .WriteLine("</table>")
            '.WriteLine("</div>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepLookup(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<div id=""hotkeyBox"">")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table"" width=""100%"">")
            Dim strSQL As String = "SELECT RepAppointmentDate, RepAppointmentLength, DiaryAppID, DiaryDueDate, DiaryLength, UserID, UserFullName, UserTelephoneNumber, PostCodeArea " & _
                " FROM vwreplookup WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""4""><h3 class=""green"">Please select a Rep:</h3><br /></td>")
                .WriteLine("</tr>")
                '(CASE WHEN DiaryAppID = '" & AppID & "' THEN 'Assigned' WHEN (DiaryDueDate IS NULL OR DiaryDueDate > (SELECT TOP (1) dbo.tbldiaries.DiaryDueDate FROM dbo.tbldiaries WHERE AppID = '" & AppID & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND DiaryType = 'RepAppointment') OR DATEADD(HH, DiaryLength, DiaryDueDate) < (SELECT TOP (1) dbo.tbldiaries.DiaryDueDate FROM dbo.tbldiaries WHERE AppID = '" & AppID & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND DiaryType = 'RepAppointment')
                .WriteLine("<tr class=""tablehead"">")
                .WriteLine("<th class=""smlc"">Name</th>")
                .WriteLine("<th class=""smlc"">Area</th>")
                .WriteLine("<th class=""smlc"">Telephone</th>")
                .WriteLine("<th class=""smlc"">Status</th>")
                .WriteLine("<th class=""smlc"">&nbsp;</th>")
                .WriteLine("</tr>")
                For Each Row As DataRow In dsCache.Rows
                    Dim strStatus As String = ""
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("UserFullName") & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("PostCodeArea").ToString & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("UserTelephoneNumber").ToString & "</td>")
                    If (Row.Item("DiaryAppID").ToString = AppID) Then
                        strStatus = "Assigned"
                    ElseIf (Not checkValue(Row.Item("DiaryDueDate").ToString)) Then
                        strStatus = "Available"
                    ElseIf (Not checkValue(Row.Item("DiaryDueDate").ToString) Or Row.Item("DiaryDueDate").ToString > DateAdd(DateInterval.Hour, Row.Item("RepAppointmentLength"), Row.Item("RepAppointmentDate")) Or DateAdd(DateInterval.Hour, Row.Item("DiaryLength"), Row.Item("DiaryDueDate")) < Row.Item("RepAppointmentDate")) Then
                        strStatus = "Available"
                    Else
                        strStatus = "Unavailable"
                    End If
                    .WriteLine("<td class=""smlc"">" & strStatus & "</td>")
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                        .WriteLine("<td class=""smlc""><input id=""cbRep" & Row.Item("UserID") & """ type=""checkbox"" name=""cbRep" & Row.Item("UserID") & """ class=""rep"" onclick=""toggleRep(this.id);"" value=""" & Row.Item("UserID") & """ /></td>")
                    Else
                        .WriteLine("<td class=""smlc"">&nbsp;</td>")
                    End If

                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""5"" class=""smlc"">No reps available.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div><br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepCalendar(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim dteStartDate As String = HttpContext.Current.Request("frmStartDate")
        Dim strSQL As String = ""
        If (Not checkValue(dteStartDate)) Then
            strSQL = "SELECT TOP 1 DiaryDueDate FROM tbldiaries WHERE AppID = '" & AppID & "' AND DiaryActive = 1 AND DiaryType = 'RepAppointment'"
            dteStartDate = New Caching(cache, strSQL, "", "", "").returnCacheString()
            If (Not checkValue(dteStartDate)) Then
                dteStartDate = Config.DefaultDate
            End If
        End If
        Dim dteEndDate As Date = CDate(dteStartDate).AddDays(7)
        Dim objCalendar As Calendar = New Calendar(Nothing, CDate(dteStartDate).Date, dteEndDate.Date, 8, 20, 0.5, False)
        strSQL = "SELECT AppID, UserFullName, DiaryTypeDescription, DiaryDueDate, DiaryLength, DiaryComplete FROM vwrepappointments " & _
            "WHERE UserID = '" & HttpContext.Current.Request("UserID") & "' AND CompanyID = '" & CompanyID & "' " & _
            "AND DiaryDueDate >= " & formatField(objCalendar.StartDate(), "DTTM", Config.DefaultDate) & " " & _
            "AND DiaryDueDate <= " & formatField(objCalendar.EndDate() & " 23:59:59", "DTTM", Config.DefaultDate) & " "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objCalendar.addEvent(Row.Item("AppID"), "<strong>" & Row.Item("UserFullName") & "</strong><br />" & Row.Item("DiaryTypeDescription"), Row.Item("DiaryDueDate"), Row.Item("DiaryLength"), Row.Item("DiaryComplete"))
            Next
        End If
        dsCache = Nothing
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmEditAppID"" name=""frmEditAppID"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmUserID"" name=""frmUserID"" value=""" & HttpContext.Current.Request("UserID") & """ />")
            .WriteLine("<input type=""hidden"" id=""frmUserFullName"" name=""frmUserFullName"" value=""" & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("UserID")) & """ />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryDueDate"" name=""frmDiaryDueDate"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryTypeDescription"" name=""frmDiaryTypeDescription"" value=""" & getAnyFieldCached(cache, "DiaryTypeDescription", "tbldiarytypes", "DiaryTypeName", "RepAppointment") & """ />")
            .WriteLine("<div style=""padding: 5px;"">")
            .WriteLine("<div class=""lrg"">")
            .WriteLine("<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(-7) & """ title=""Previous Week""><img src=""/images/icons/arrow_prev.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine(FormatDateTime(objCalendar.StartDate(), DateFormat.LongDate) & " - " & FormatDateTime(objCalendar.EndDate().AddDays(-1), DateFormat.LongDate))
            .WriteLine("&nbsp;<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(7) & """ title=""Next Week""><img src=""/images/icons/arrow_next.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine("</div><br />")
            .WriteLine("<table class=""table calendar"" style=""width: 100%; height: 100%"" align=""center"">")
            .WriteLine(objCalendar.displayCalendar)
            .WriteLine("</table>")
            .WriteLine("</div>")
            '.WriteLine("<a href=""#"" onclick=""parent.jPrompt('Please specify a time', '', 'Specify Time', 'Ok', 'Cancel', function (r) { if (r) document.form1.submit(); })"">Book</a>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepCalendarAll(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim dteStartDate As String = HttpContext.Current.Request("frmStartDate")
        Dim strSQL As String = ""
        If (Not checkValue(dteStartDate)) Then
            strSQL = "SELECT TOP 1 DiaryDueDate FROM tbldiaries WHERE AppID = '" & AppID & "' AND DiaryActive = 1 AND (DiaryType = 'RepAppointment' OR DiaryType = 'SecondRepAppointment')"
            If (LeadPlatform.Config.Rep = True) Then
                strSQL += " AND (DiaryAssignedToUserID = '" & Config.DefaultUserID & "')"
            End If
            dteStartDate = New Caching(cache, strSQL, "", "", "").returnCacheString()
            If (Not checkValue(dteStartDate)) Then
                dteStartDate = Config.DefaultDate
            End If
        End If
        Dim dteEndDate As Date = CDate(dteStartDate).AddDays(7)
        Dim objCalendar As Calendar = New Calendar(Nothing, CDate(dteStartDate).Date, dteEndDate.Date, 8, 20, 0.25, False)
        strSQL = "SELECT AppID, AddressPostCode, UserFullName, DiaryTypeDescription, DiaryDueDate, DiaryLength, DiaryComplete FROM vwrepappointments " & _
            "WHERE DiaryDueDate >= " & formatField(objCalendar.StartDate(), "DTTM", Config.DefaultDate) & " " & _
            "AND DiaryDueDate <= " & formatField(objCalendar.EndDate() & " 23:59:59", "DTTM", Config.DefaultDate) & " AND CompanyID = '" & CompanyID & "'"
        If (LeadPlatform.Config.Rep = True) Then
            strSQL += " AND (UserID = '" & Config.DefaultUserID & "')"
        End If
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objCalendar.addEvent(Row.Item("AppID"), "<strong>" & Row.Item("UserFullName") & " - " & Row.Item("AddressPostCode") & "</strong><br />" & Row.Item("DiaryTypeDescription"), Row.Item("DiaryDueDate"), Row.Item("DiaryLength"), Row.Item("DiaryComplete"))
            Next
        End If
        dsCache = Nothing
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmEditAppID"" name=""frmEditAppID"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmUserID"" name=""frmUserID"" value=""" & HttpContext.Current.Request("UserID") & """ />")
            .WriteLine("<input type=""hidden"" id=""frmUserFullName"" name=""frmUserFullName"" value=""" & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("UserID")) & """ />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryDueDate"" name=""frmDiaryDueDate"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryType"" name=""frmDiaryType"" value=""" & HttpContext.Current.Request("frmDiaryType") & """ />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryTypeDescription"" name=""frmDiaryTypeDescription"" value=""" & getAnyFieldCached(cache, "DiaryTypeDescription", "tbldiarytypes", "DiaryTypeName", HttpContext.Current.Request("frmDiaryType")) & """ />")
            .WriteLine("<div style=""padding: 5px;"">")
            .WriteLine("<div class=""lrg"">")
            .WriteLine("<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(-7) & """ title=""Previous Week""><img src=""/images/icons/arrow_prev.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine(FormatDateTime(objCalendar.StartDate(), DateFormat.LongDate) & " - " & FormatDateTime(objCalendar.EndDate().AddDays(-1), DateFormat.LongDate))
            .WriteLine("&nbsp;<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(7) & """ title=""Next Week""><img src=""/images/icons/arrow_next.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine("</div><br />")
            .WriteLine("<table class=""table calendar"" style=""width: 100%; height: 100%"" align=""center"">")
            .WriteLine(objCalendar.displayCalendar)
            .WriteLine("</table>")
            .WriteLine("</div>")
            '.WriteLine("<a href=""#"" onclick=""parent.jPrompt('Please specify a time', '', 'Specify Time', 'Ok', 'Cancel', function (r) { if (r) document.form1.submit(); })"">Book</a>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepHolidayCalendar(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim dteStartDate As String = HttpContext.Current.Request("frmStartDate")
        Dim strSQL As String = ""
        If (Not checkValue(dteStartDate)) Then
            strSQL = "SELECT TOP 1 DiaryDueDate FROM tbldiaries WHERE AppID = '" & AppID & "' AND DiaryActive = 1 AND DiaryType = 'RepHoliday'"
            dteStartDate = New Caching(cache, strSQL, "", "", "").returnCacheString()
            If (Not checkValue(dteStartDate)) Then
                dteStartDate = Config.DefaultDate
            End If
        End If
        Dim dteEndDate As Date = CDate(dteStartDate).AddDays(7)
        Dim objCalendar As Calendar = New Calendar(Nothing, CDate(dteStartDate).Date, dteEndDate.Date, 8, 20, 0.25, False)
        strSQL = "SELECT AppID, AddressPostCode, UserFullName, DiaryTypeDescription, DiaryDueDate, DiaryLength, DiaryComplete FROM vwrepappointments " & _
            "WHERE DiaryDueDate >= " & formatField(objCalendar.StartDate(), "DTTM", Config.DefaultDate) & " " & _
            "AND DiaryDueDate <= " & formatField(objCalendar.EndDate() & " 23:59:59", "DTTM", Config.DefaultDate) & " AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objCalendar.addEvent(Row.Item("AppID"), "<strong>" & Row.Item("UserFullName") & "</strong><br />" & Row.Item("DiaryTypeDescription"), Row.Item("DiaryDueDate"), Row.Item("DiaryLength"), Row.Item("DiaryComplete"))
            Next
        End If
        dsCache = Nothing
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmEditAppID"" name=""frmEditAppID"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmUserID"" name=""frmUserID"" value=""" & HttpContext.Current.Request("UserID") & """ />")
            .WriteLine("<input type=""hidden"" id=""frmUserFullName"" name=""frmUserFullName"" value=""" & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("UserID")) & """ />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryDueDate"" name=""frmDiaryDueDate"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryType"" name=""frmDiaryType"" value=""" & HttpContext.Current.Request("frmDiaryType") & """ />")
            .WriteLine("<input type=""hidden"" id=""frmDiaryTypeDescription"" name=""frmDiaryTypeDescription"" value=""" & getAnyFieldCached(cache, "DiaryTypeDescription", "tbldiarytypes", "DiaryTypeName", HttpContext.Current.Request("frmDiaryType")) & """ />")
            .WriteLine("<div style=""padding: 5px;"">")
            .WriteLine("<div class=""lrg"">")
            .WriteLine("<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(-7) & """ title=""Previous Week""><img src=""/images/icons/arrow_prev.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine(FormatDateTime(objCalendar.StartDate(), DateFormat.LongDate) & " - " & FormatDateTime(objCalendar.EndDate().AddDays(-1), DateFormat.LongDate))
            .WriteLine("&nbsp;<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(7) & """ title=""Next Week""><img src=""/images/icons/arrow_next.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine("</div><br />")
            .WriteLine("<table class=""table calendar"" style=""width: 100%; height: 100%"" align=""center"">")
            .WriteLine(objCalendar.displayCalendar)
            .WriteLine("</table>")
            .WriteLine("</div>")
            '.WriteLine("<a href=""#"" onclick=""parent.jPrompt('Please specify a time', '', 'Specify Time', 'Ok', 'Cancel', function (r) { if (r) document.form1.submit(); })"">Book</a>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepInstructions(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table"" width=""100%"">")
            Dim strSQL As String = "SELECT DiaryID, DiaryType, DiaryTypeDescription, DiaryComplete FROM tbldiaries WHERE AppID = '" & AppID & "' AND DiaryType = 'RepInstruction' AND DiaryActive = 1"

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            .WriteLine("<tr>")
            .WriteLine("<td colspan=""4""><h3 class=""green"">Instructions:</h3><br /></td>")
            .WriteLine("</tr>")
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 1, strComplete = ""
                For Each Row As DataRow In dsCache.Rows
                    If (Row.Item("DiaryComplete")) Then
                        strComplete = " strike"
                    End If
                    .WriteLine("<tr class=""" & strClass & """>")
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                        .WriteLine("<td class=""sml"">")
                        .WriteLine("<a href=""#"" class=""strikeToggle"" onClick=""top.jConfirm('Are you sure you want to toggle the status of this instruction?','Confirm Action', 'Yes', 'No', function(r) { confirmClick(r, '/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strDefaultReturnURL) & "&strThisPg=diary&strFrmAction=complete&DiaryID=" & Row.Item("DiaryID") & "&frmAppID=" & AppID & "&DiaryComplete=" & Row.Item("DiaryComplete") & "') } );"">" & tickCrossValid(Row.Item("DiaryComplete"), True, True) & "</a>")
                        .WriteLine("&nbsp;<span class=""" & strComplete & """>" & x & ". <a href=""#"" onclick=""parent.parent.tb_show('Edit Instruction', '/prompts/diary.aspx?DiaryID=" & Row.Item("DiaryID") & "&frmDiaryType=RepInstruction&frmDiaryTypeDescription=" & Row.Item("DiaryTypeDescription") & "&AppID=" & AppID & "&strReturnURL=" & encodeURL("/application/processing.aspx?AppID=" & AppID) & "&frmTarget=content&TB_iframe=true&width=340&height=130', '')"">" & Row.Item("DiaryTypeDescription") & "</a></span>")
                        .WriteLine("</td>")
                    Else
                        .WriteLine("<td class=""sml"">")
                        .WriteLine(tickCrossValid(Row.Item("DiaryComplete"), False))
                        .WriteLine("&nbsp;<span class=""" & strComplete & """>" & x & ". " & Row.Item("DiaryTypeDescription") & "</span>")
                        .WriteLine("</td>")
                    End If
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                        .WriteLine("<td class=""smlc"">")
                        .WriteLine("<a href=""#"" onClick=""top.jConfirm('Are you sure you want to remove this instruction?','Confirm Action', 'Yes', 'No', function(r) { confirmClick(r, '/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strDefaultReturnURL) & "&strThisPg=diary&strFrmAction=active&DiaryID=" & Row.Item("DiaryID") & "&frmAppID=" & AppID & "&DiaryActive=True') } );""><img src=""/images/delete_dot.gif"" width=""16"" height=""16"" border=""0""></a>")
                        .WriteLine("</td>")
                    Else
                        .WriteLine("<td class=""smlc"">&nbsp;</td>")
                    End If
                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                    strComplete = ""
                    x += 1
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""2"" class=""smlc"">No instructions found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                .WriteLine("<a id=""AddInstruction"" href=""#"" onclick=""parent.parent.tb_show('New Instruction', '/prompts/diary.aspx?AppID=" & AppID & "&frmDiaryType=RepInstruction&strReturnURL=" & encodeURL("/application/processing.aspx?AppID=" & AppID) & "&frmTarget=content&TB_iframe=true&width=340&height=130', '')"" class=""salesButton"">New Instruction</a><br /><br />")
            End If
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getRepLookupAll(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<div id=""hotkeyBox"">")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table"" width=""100%"">")
            Dim strSQL As String = "SELECT RepAppointmentDate, RepAppointmentLength, DiaryAppID, DiaryDueDate, DiaryLength, DiaryAddressPostCode, UserID, UserFullName, UserTelephoneNumber, PostCodeArea " & _
                " FROM vwreplookupall WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY UserID"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""4""><h3 class=""green"">Please select a Rep:</h3><br /></td>")
                .WriteLine("</tr>")
                '(CASE WHEN DiaryAppID = '" & AppID & "' THEN 'Assigned' WHEN (DiaryDueDate IS NULL OR DiaryDueDate > (SELECT TOP (1) dbo.tbldiaries.DiaryDueDate FROM dbo.tbldiaries WHERE AppID = '" & AppID & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND DiaryType = 'RepAppointment') OR DATEADD(HH, DiaryLength, DiaryDueDate) < (SELECT TOP (1) dbo.tbldiaries.DiaryDueDate FROM dbo.tbldiaries WHERE AppID = '" & AppID & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND DiaryType = 'RepAppointment')
                .WriteLine("<tr class=""tablehead"">")
                .WriteLine("<th class=""smlc"">Name</th>")
                .WriteLine("<th class=""smlc"">Area</th>")
                .WriteLine("<th class=""smlc"">Telephone</th>")
                .WriteLine("<th class=""smlc"">Status</th>")
                .WriteLine("<th class=""smlc"">&nbsp;</th>")
                .WriteLine("</tr>")
                Dim intLastUserID As Integer = 0
                For Each Row As DataRow In dsCache.Rows
                    Dim strStatus As String = ""
                    If (intLastUserID <> Row.Item("UserID")) Then
                        .WriteLine("<tr class=""" & strClass & """>")
                        .WriteLine("<td class=""smlc"">" & Row.Item("UserFullName") & "</td>")
                        .WriteLine("<td class=""smlc"">" & Row.Item("PostCodeArea").ToString & "</td>")
                        .WriteLine("<td class=""smlc"">" & Row.Item("UserTelephoneNumber").ToString & "</td>")
                        If (Row.Item("DiaryAppID").ToString = AppID) Then
                            strStatus = "Assigned"
                        ElseIf (Not checkValue(Row.Item("DiaryDueDate").ToString)) Then
                            strStatus = "Available"
                        ElseIf (Not checkValue(Row.Item("DiaryDueDate").ToString) Or Row.Item("DiaryDueDate").ToString > DateAdd(DateInterval.Hour, Row.Item("RepAppointmentLength"), Row.Item("RepAppointmentDate")) Or DateAdd(DateInterval.Hour, Row.Item("DiaryLength"), Row.Item("DiaryDueDate")) < Row.Item("RepAppointmentDate")) Then
                            strStatus = "Available"
                        Else
                            strStatus = "Unavailable"
                        End If
                        .WriteLine("<td class=""smlc"">" & strStatus & "</td>")
                        If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                            .WriteLine("<td class=""smlc""><input id=""cbRep" & Row.Item("UserID") & """ type=""checkbox"" name=""cbRep" & Row.Item("UserID") & """ class=""rep"" onclick=""toggleRep(this.id);"" value=""" & Row.Item("UserID") & """ /></td>")
                        Else
                            .WriteLine("<td class=""smlc"">&nbsp;</td>")
                        End If

                        .WriteLine("</tr>")
                        strClass = nextClass(strClass)
                    End If
                    intLastUserID = Row.Item("UserID")
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""5"" class=""smlc"">No reps available.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div><br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getTaskList(ByVal cache As Web.Caching.Cache, ByVal AppID As String, ByVal FilterUser As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<br />")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table"" width=""100%"">")
            Dim strSQL As String = "SELECT DiaryID, DiaryAssignedToUserID, DiaryType, DiaryTypeDescription, DiaryDueDate, DiarySubStatusCode, DiaryComplete, SubStatusDescription FROM tbldiaries " & _
                "LEFT JOIN tblsubstatuses ON SubStatusCode = DiarySubStatusCode " & _
                "WHERE AppID = '" & AppID & "' "
            If (FilterUser = "True") Then
                strSQL += "AND DiaryAssignedToUserID = '" & Config.DefaultUserID & "' "
            End If
            strSQL += "AND DiaryActive = 1 ORDER BY DiaryComplete, DiaryID DESC "
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 1, strComplete As String = "", strInActive As String = "", strUserAdditionalMessage As String = ""
                For Each Row As DataRow In dsCache.Rows
                    If (Row.Item("DiaryComplete")) Then
                        strComplete = "strike"
                    End If
                    If (Row.Item("DiaryID") = objLeadPlatform.Config.UserActiveWorkflowDiaryID) Then
                        strClass = "row-highlight"
                    End If
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") And (Row.Item("DiaryID") = objLeadPlatform.Config.UserActiveWorkflowDiaryID) Then
                        strInActive = ""
                    ElseIf (Row.Item("DiaryComplete")) Then
                        strInActive = " grey"
                    Else
                        strInActive = " dark-grey"
                    End If
                    .WriteLine("<tr class=""" & strClass & """>")
                    If (Row.Item("DiaryAssignedToUserID") = Config.DefaultUserID) Then
                        .WriteLine("<td class=""smlc"" width=""16"">")
                        .WriteLine("<i class=""icon-user""></i>")
                        .WriteLine("</td>")
                    Else
                        .WriteLine("<td class=""smlc"" width=""16"">&nbsp;</td>")
                    End If
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") And (Row.Item("DiaryID") = objLeadPlatform.Config.UserActiveWorkflowDiaryID) Then
                        .WriteLine("<td class=""sml"">")
                        .WriteLine()
                        If (Row.Item("DiaryAssignedToUserID") <> Config.DefaultUserID) Then
                            strUserAdditionalMessage = "This task IS NOT assigned to you! "
                        End If
                        .WriteLine("<a href=""#"" onClick=""confirmAction('/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strDefaultReturnURL) & "&strThisPg=diary&strFrmAction=complete&DiaryID=" & Row.Item("DiaryID") & "&frmAppID=" & AppID & "&DiaryComplete=" & Row.Item("DiaryComplete") & "','" & strUserAdditionalMessage & "Are you sure you want to toggle the status of this task?','Confirm Action');"" target=""_self"">" & tickCross(Row.Item("DiaryComplete"), True) & "</a>")
                        .WriteLine("&nbsp;<span class=""" & strComplete & """>" & x & ". <a href=""#"" onclick=""parent.parent.tb_show('Edit Task', '/prompts/diary.aspx?DiaryID=" & Row.Item("DiaryID") & "&frmDiaryTypeDescription=" & Row.Item("DiaryTypeDescription") & "&AppID=" & AppID & "&strReturnURL=" & encodeURL("/application/processing.aspx?AppID=" & AppID) & "&frmTarget=content&TB_iframe=true&width=340&height=130', '')"">" & Row.Item("DiaryTypeDescription") & "</a></span>")
                        .WriteLine("</td>")
                    Else
                        .WriteLine("<td class=""sml"">")
                        .WriteLine("&nbsp;<span class=""" & strComplete & strInActive & """ title=""Work this task"">" & x & ". <a class=""" & strInActive & """ href=""/processing.aspx?DiaryID=" & Row.Item("DiaryID") & "&AppID=" & AppID & """ target=""_top"">" & Row.Item("DiaryTypeDescription") & "</a></span>")
                        .WriteLine("</td>")
                    End If
                    .WriteLine("<td class=""smlc""><span class=""" & strComplete & strInActive & """>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("DiaryDueDate")) & "</span></td>")
                    .WriteLine("<td class=""smlc"" title=""" & Row.Item("SubStatusDescription") & """><span class=""" & strComplete & strInActive & """>" & Row.Item("DiarySubStatusCode").ToString & "</span></td>")
                    If (intWorkflowActivityID = ActivityType.StartCase OR ActivityType.StartCall Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                        .WriteLine("<td class=""smlc"">")
                        .WriteLine("<a href=""#"" onClick=""confirmAction('/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strDefaultReturnURL) & "&strThisPg=diary&strFrmAction=active&DiaryID=" & Row.Item("DiaryID") & "&frmAppID=" & AppID & "&DiaryActive=True','Are you sure you want to remove this task?');""><i class=""icon-remove""></i></a>")
                        .WriteLine("</td>")
                    Else
                        .WriteLine("<td class=""smlc"">&nbsp;</td>")
                    End If
                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                    strComplete = ""
                    strUserAdditionalMessage = ""
                    x += 1
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""6"" class=""smlc"">No tasks found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getCloneApplication(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmStatusCode"" name=""frmStatusCode"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmMediaCampaignIDOutbound"" name=""frmMediaCampaignIDOutbound"" value="""" />")
            .WriteLine("<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">Start Case As:</span>")
            .WriteLine("<div class=""smlc"">")
            .WriteLine("<select id=""frmTransferStatus"" name=""frmTransferStatus"" class=""text mandatory"" title=""Campaign"" onchange=""setTransferStatus(this.value)"">")
            .WriteLine(transferStatusTextDropdown(Nothing, AppID))
            .WriteLine("</select>")
            .WriteLine("</div>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getCloneApplicationFree(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmStatusCode"" name=""frmStatusCode"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmMediaCampaignIDOutbound"" name=""frmMediaCampaignIDOutbound"" value="""" />")
            '.WriteLine("<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">Product Type:</span>")
            '.WriteLine("<div class=""smlc"">")
            '.WriteLine("<select id=""frmProductType"" name=""frmProductType"" class=""text mandatory"" title=""Product Type"">")
            '.WriteLine(productTypeTextDropdown(CacheObject(), "--Product Type--", getAnyField("ProductType", "tblapplications", "AppID", AppID)))
            '.WriteLine("</select>")
            '.WriteLine("</div>")
            .WriteLine("<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">Clone To Campaign:</span>")
            .WriteLine("<div class=""smlc"">")
            .WriteLine("<select id=""frmCloneMediaCampaignID"" name=""frmCloneMediaCampaignID"" class=""text mandatory"" title=""Campaign"">")
            .WriteLine(cloneMediaCampaignTextDropdown(Nothing, "--Campaign--", MediaCampaignID))
            .WriteLine("</select>")
            .WriteLine("</div>")
            .WriteLine("<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">Start Case As:</span>")
            .WriteLine("<div class=""smlc"">")
            .WriteLine("<select id=""frmTransferStatus"" name=""frmTransferStatus"" class=""text mandatory"" title=""Campaign"" onchange=""setTransferStatus(this.value)"">")
            .WriteLine(transferStatusTextDropdown(Nothing, AppID))
            .WriteLine("</select>")
            .WriteLine("</div>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getLinkedApplications(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<div id=""hotkeyBox"">")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table"" width=""100%"">")
            Dim strSQL As String = "SELECT ParentAppID, ChildAppID, ParentProductType, ParentAmount, ParentCreatedDate, ParentStatusCode, ParentSubStatusCode, ChildAmount, ChildProductType, ChildCreatedDate, ChildStatusCode, ChildSubStatusCode FROM vwlinkedapplications WHERE (ParentAppID = " & AppID & " OR ParentAppID = (SELECT ParentAppID FROM dbo.tblapplications AS tblapplications_2 WHERE (AppID = " & AppID & "))) AND CompanyID = '" & CompanyID & "' ORDER BY ParentAppID"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0
                For Each Row As DataRow In dsCache.Rows
                    If (x = 0) Then
                        .WriteLine("<br />")
                        .WriteLine("<tr class=""tablehead"">")
                        .WriteLine("<th class=""smlc"">AppID</th>")
                        .WriteLine("<th class=""smlc"">Product Type</th>")
                        .WriteLine("<th class=""smlc"">Amount</th>")
                        .WriteLine("<th class=""smlc"">Created</th>")
                        .WriteLine("<th class=""smlc"">Status</th>")
                        .WriteLine("<th class=""smlc"">&nbsp;</th>")
                        .WriteLine("</tr>")
                        If (Row.Item("ParentAppID") <> AppID) Then
                            .WriteLine("<tr class=""" & strClass & """>")
                            .WriteLine("<td class=""smlc""><a href=""/processing.aspx?AppID=" & Row.Item("ParentAppID") & "&Action=2"" target=""_top"">" & Row.Item("ParentAppID") & "</a></td>")
                            .WriteLine("<td class=""smlc"">" & Row.Item("ParentProductType") & "</td>")
                            .WriteLine("<td class=""smlc"">" & displayCurrency(Row.Item("ParentAmount")) & "</td>")
                            .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("ParentCreatedDate")) & "</td>")
                            .WriteLine("<td class=""smlc"">" & Row.Item("ParentStatusCode") & " " & Row.Item("ParentSubStatusCode") & "</td>")
                            .WriteLine("<td class=""smlc""><a href=""#"" onclick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("ParentAppID") & "', '600', '600');""><i class=""icon-edit""></i></a></td>")
                            .WriteLine("</tr>")
                            strClass = nextClass(strClass)
                        End If
                    End If
                    If (Row.Item("ChildAppID").ToString <> AppID And checkValue(Row.Item("ChildAppID").ToString)) Then
                        .WriteLine("<tr class=""" & strClass & """>")
                        .WriteLine("<td class=""smlc""><a href=""/processing.aspx?AppID=" & Row.Item("ChildAppID") & "&Action=2"" target=""_top"">" & Row.Item("ChildAppID") & "</a></td>")
                        .WriteLine("<td class=""smlc"">" & Row.Item("ChildProductType") & "</td>")
                        .WriteLine("<td class=""smlc"">" & displayCurrency(Row.Item("ChildAmount")) & "</td>")
                        .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("ChildCreatedDate")) & "</td>")
                        .WriteLine("<td class=""smlc"">" & Row.Item("ChildStatusCode") & " " & Row.Item("ChildSubStatusCode") & "</td>")
                        .WriteLine("<td class=""smlc""><a href=""#"" onclick=""parent.$('#modal-iframe').modal('show');parent.setPrompt('#modal-iframe','Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("ChildAppID") & "', '600', '600');""><i class=""icon-edit""></i></a></td>")
                        .WriteLine("</tr>")
                        strClass = nextClass(strClass)
                    End If
                    x += 1
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""6"" class=""smlc"">No linked cases found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getReport(ByVal cache As Web.Caching.Cache, ByVal AppID As String, ByVal strURL As String) As String
        If InStr(strURL, "{AppID}") Then strURL = Replace(strURL, "{AppID}", AppID)
        If InStr(strURL, "{MediaCampaignIDInbound}") Then strURL = Replace(strURL, "{MediaCampaignIDInbound}", MediaCampaignID)
        If InStr(strURL, "{MediaIDInbound}") Then strURL = Replace(strURL, "{MediaIDInbound}", getAnyField("MediaID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID))
        If InStr(strURL, "{MediaCampaignDialerGradeID}") Then strURL = Replace(strURL, "{MediaCampaignDialerGradeID}", getAnyField("MediaCampaignDialerGradeID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID))
        strURL = Replace(strURL, "|", ",")

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<iframe id=""reportiframe"" name=""reportiframe"" frameborder=""0"" scrolling=""no"" width=""100%"" height=""100px"" src=""" & strURL & "&frmProcessingReport=Y""></iframe>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getAveloQuote(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        If (checkValue(HttpContext.Current.Request("QuoteID"))) Then
            If (HttpContext.Current.Request("strSave") = "Y") Then
                Return saveAveloDocuments(cache, AppID)
            ElseIf (HttpContext.Current.Request("strView") = "Y") Then
                Return viewAveloDocuments(cache, AppID)
            Else
                Return getAveloNewBusiness(cache, AppID)
            End If
        Else
            Dim strQuoteType = getAnyFieldFromDataStore("PolicyQuotationType", AppID)
            Select Case strQuoteType
                Case "Whole Life Protection"
                    Return getAveloQuoteText(cache, AppID, "GetWOLQuoteV1", "1")
                Case "Term Protection"
                    Return getAveloQuoteText(cache, AppID, "GetTermQuoteV1", "2")
                Case "Income Protection"
                    Return getAveloQuoteText(cache, AppID, "GetIPQuoteV1", "3")
                Case Else
                    Return ""
            End Select
        End If
    End Function

    Public Function getAveloPreviousQuotes(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        If (checkValue(HttpContext.Current.Request("QuoteID"))) Then
            If (HttpContext.Current.Request("strSave") = "Y") Then
                Return saveAveloDocuments(cache, AppID)
            ElseIf (HttpContext.Current.Request("strView") = "Y") Then
                Return viewAveloDocuments(cache, AppID)
            Else
                Return getAveloNewBusiness(cache, AppID)
            End If
        Else
            Dim strQuoteType = getAnyFieldFromDataStore("PolicyQuotationType", AppID)
            Select Case strQuoteType
                Case "Whole Life Protection"
                    Return getAveloPreviousQuotesText(cache, AppID, "GetWOLQuoteV1")
                Case "Term Protection"
                    Return getAveloPreviousQuotesText(cache, AppID, "GetTermQuoteV1")
                Case "Income Protection"
                    Return getAveloPreviousQuotesText(cache, AppID, "GetIPQuoteV1")
                Case Else
                    Return ""
            End Select
        End If
    End Function

    Private Function getCardDetails(ByVal AppID As String, ByVal details As String) As String
        Dim strDetails As String = ""
        Dim strSQL As String = "EXEC spdecryptcarddetails @AppID = " & AppID & ", @CompanyID = '" & CompanyID & "' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strDetails = Row.Item(details).ToString
            Next
        End If
        dsCache = Nothing
        Return strDetails
    End Function

    Public Function matchingApps(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String

        strFirstname = getAnyField("App1FirstName", "tblapplications", "AppId", AppID)
        strSurname = getAnyField("App1SurName", "tblapplications", "AppId", AppID)
        strPostCode = getAnyField("AddressPostCode", "tblapplications", "AppId", AppID)
        strDOB = getAnyField("App1DOB", "tblapplications", "AppId", AppID)
        strHomeTel = getAnyField("App1HomeTelephone", "tblapplications", "AppId", AppID)
        strMobTel = getAnyField("App1MobileTelephone", "tblapplications", "AppId", AppID)
        strEmail = getAnyField("App1EmailAddress", "tblapplications", "AppId", AppID)
        strAccountNumber = getAnyFieldFromDataStore("BankAccountNumber", AppID)
        strSortCode = getAnyFieldFromDataStore("BankSortCode", AppID)
        strCardNum = getCardDetails(AppID, "CardNumber")

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped"" width=""100%"">")
            .WriteLine("<br />")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">AppID</th>")
            .WriteLine("<th class=""smlc"">First Name</th>")
            .WriteLine("<th class=""smlc"">Surname</th>")
            .WriteLine("<th class=""smlc"">DOB</th>")
            .WriteLine("<th class=""smlc"">Post Code</th>")
            .WriteLine("<th class=""smlc"">Home</th>")
            .WriteLine("<th class=""smlc"">Mobile</th>")
            .WriteLine("<th class=""smlc"">Email</th>")
            .WriteLine("<th class=""smlc"">A/C No.</th>")
            .WriteLine("<th class=""smlc"">Card No.</th>")
            .WriteLine("<th class=""smlc"">Created</th>")
            .WriteLine("<th class=""smlc"">Status</th>")
            .WriteLine("</tr>")

            'Dim strSQL As String =""' "SELECT ParentAppID, ChildAppID, ParentProductType, ParentAmount, ParentCreatedDate, ParentStatusCode, ParentSubStatusCode, ChildAmount, ChildProductType, ChildCreatedDate, ChildStatusCode, ChildSubStatusCode FROM vwlinkedapplications WHERE (ParentAppID = " & AppID & " OR ParentAppID = (SELECT ParentAppID FROM dbo.tblapplications AS tblapplications_2 WHERE (AppID = " & AppID & "))) AND CompanyID = '" & CompanyID & "' ORDER BY ParentAppID"

            Dim strSQL2 As String = "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
          "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
          "WHERE(dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') And (App1FirstName = '" & strFirstname & "') And (App1Surname = '" & strSurname & "')" & vbCrLf & _
           "       UNION " & vbCrLf & _
       "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
          "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
          "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND(AddressPostCode = '" & strPostCode & "')" 'AND (App1DOB = '" & strDOB & "' ) "
            If checkValue(strSortCode) And checkValue(strAccountNumber) Then
                strSQL2 += "UNION" & vbCrLf & _
        "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, BNK.StoredDataValue AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
          "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
           "                     INNER JOIN dbo.tbldatastore AS BNK ON dbo.tblapplications.AppID = BNK.AppID " & vbCrLf & _
          "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND(BNK.StoredDataName = N'BankAccountNumber') AND " & vbCrLf & _
           "                     (BNK.StoredDataValue = '" & strAccountNumber & "')"
            End If
            If checkValue(strCardNum) Then
                strSQL2 += "UNION" & vbCrLf & _
             "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, CRD.StoredDataValue AS BankCardNumber " & vbCrLf & _
          "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID INNER JOIN " & vbCrLf & _
                "                     dbo.tbldatastore AS CRD ON dbo.tblapplications.AppID = CRD.AppID " & vbCrLf & _
               "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND (CRD.StoredDataName = N'BankCardNumber') AND (CRD.StoredDataValue = (SELECT TOP 1 StoredDataValue FROM tbldatastore WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND StoredDataName = 'BankCardNumber'))"
            End If
            strSQL2 += " UNION " & vbCrLf & _
              "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
              "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
                "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND (App1HomeTelephone = '" & strHomeTel & "')" & vbCrLf & _
                "UNION" & vbCrLf & _
               "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
              "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
                "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND (App1MobileTelephone = '" & strMobTel & "')" & vbCrLf & _
                "UNION" & vbCrLf & _
               "SELECT  tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103) AS App1DOB, App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode, '' AS BankAccountNumber, '' AS BankCardNumber " & vbCrLf & _
              "FROM         dbo.tblapplications INNER JOIN dbo.tblapplicationstatus ON tblapplications.Appid = tblapplicationstatus.AppID " & vbCrLf & _
                "WHERE     (dbo.tblapplications.AppID <> " & AppID & " AND dbo.tblapplications.CompanyID = '" & CompanyID & "') AND (App1EmailAddress = '" & strEmail & "')" & vbCrLf & _
                "GROUP BY tblapplications.AppID, tblapplications.CompanyID, App1FirstName, App1Surname, statusCode, CreatedDate, CONVERT(NVARCHAR(10),App1DOB,103), App1HomeTelephone, App1MobileTelephone, App1EmailAddress, AddressPostCode ORDER BY CreatedDate DESC"

'            responseWrite(strSQL2)
'            responseEnd()

            Dim dsCache As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""sml""><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Application Summary', '/reports/case.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"">" & Row.Item("AppID") & "</a></td>")
                    If (strFirstname = Row.Item("App1FirstName")) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1FirstName") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1FirstName") & "</td>")
                    End If
                    If (strSurname = Row.Item("App1Surname")) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1Surname") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1Surname") & "</td>")
                    End If
                    If (strDOB = Row.Item("App1DOB").ToString) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1DOB") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1DOB") & "</td>")
                    End If
                    If (strPostCode = Row.Item("AddressPostCode")) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("AddressPostCode") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("AddressPostCode") & "</td>")
                    End If
                    If (strHomeTel = Row.Item("App1HomeTelephone").ToString) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1HomeTelephone") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1HomeTelephone") & "</td>")
                    End If
                    If (strMobTel = Row.Item("App1MobileTelephone").ToString) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1MobileTelephone") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1MobileTelephone") & "</td>")
                    End If
                    If (strEmail = Row.Item("App1EmailAddress").ToString) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("App1EmailAddress") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("App1EmailAddress") & "</td>")
                    End If
                    If (strAccountNumber = Row.Item("BankAccountNumber").ToString) Then
                        .WriteLine("<td class=""sml rowRed"">" & Row.Item("BankAccountNumber") & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("BankAccountNumber") & "</td>")
                    End If
                    Dim strDecryptedCardNum As String = getCardDetails(Row.Item("AppID"), "CardNumber")
                    If (strCardNum = strDecryptedCardNum) Then
                        .WriteLine("<td class=""sml rowRed"">************" & Right(strDecryptedCardNum, 4) & "</td>")
                    Else
                        .WriteLine("<td class=""sml"">" & Row.Item("BankCardNumber") & "</td>")
                    End If
                    .WriteLine("<td class=""sml"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</td>")
                    .WriteLine("<td class=""sml"">" & Row.Item("StatusCode") & "</td>")
                    .WriteLine("</tr>")
                Next
                .WriteLine("</table>")
            End If
        End With

        Return objStringWriter.ToString

    End Function

    'Public Function getAveloWOLQuote(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter
    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            .WriteLine("<script type=""text/javascript"">")
    '            .WriteLine("$(document).ready(function() {")
    '            .WriteLine("    $url = '" & Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&strServiceID=GetWOLQuoteV1&intMode=1&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID & "';")
    '            .WriteLine("    $http = httpRequest($url,'POST');")
    '            .WriteLine("    if ($http.readyState == 4) {")
    '            .WriteLine("        $arrMessage = $http.responseText.split('|');")
    '            .WriteLine("        if ($arrMessage.length > 1) {")
    '            .WriteLine("            if ($arrMessage[0] == '1') {")
    '            .WriteLine("                window.location = window.location + '&XMLReceivedID=' + $arrMessage[1];")
    '            .WriteLine("            }")
    '            .WriteLine("            else {")
    '            .WriteLine("                $('#msg').text($arrMessage[1]);")
    '            .WriteLine("                $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("            }")
    '            .WriteLine("        }")
    '            .WriteLine("        else {")
    '            .WriteLine("            $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("            $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("        }")
    '            .WriteLine("    }")
    '            .WriteLine("    else {")
    '            .WriteLine("        $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("        $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("    }")
    '            .WriteLine("});")
    '            .WriteLine("</script>")
    '            .WriteLine("<div id=""msgContainer"" class=""lrgc displayNone""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span id=""msg"" style=""color:red;font-weight:bold;""></span></div>")
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMlReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_wol_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                    'Dim objReason As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_wol_quote_response/quo:reason", objNSM)
    '                    'If Not (objReason Is Nothing) Then
    '                    '    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: " & objReason.InnerText
    '                    'Else
    '                    '    strErrorMessage = "Quote error. Please contact Engaged Solutions."
    '                    'End If
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_wol_quote_response/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
    '                            If (Not checkValue(strSumAssured)) Then
    '                                strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (checkValue(strSumAssured)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True") Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""Proceed""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></div>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    'Public Function getAveloPreviousWOLQuotes(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter

    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            Dim strSumAssured As String = "", strMonthlyBenefit As String = ""
    '            Dim strClass As String = "row1"
    '            Dim strSQL As String = "SELECT XMLReceivedID, XMLReceived, XMLReceivedDate FROM tblxmlreceived WHERE XMLReceivedResult = 101 AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY XMLReceivedDate DESC"
    '            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '            .WriteLine("<div id=""hotkeyBox"">")
    '            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '            .WriteLine("<thead>")
    '            .WriteLine("<tr class=""tablehead"">")
    '            .WriteLine("<th class=""smlc sort-numeric"">Sum Assured</th>")
    '            .WriteLine("<th class=""smlc sort-numeric"">Monthly Benefit</th>")
    '            .WriteLine("<th class=""smlc sort-numeric"">Lowest Payment</th>")
    '            .WriteLine("<th class=""smlc sort-date"">Quote Date</th>")
    '            .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '            .WriteLine("</tr>")
    '            .WriteLine("</thead>")
    '            .WriteLine("<tbody>")
    '            If (dsCache.Rows.Count > 0) Then
    '                For Each Row As DataRow In dsCache.Rows
    '                    Dim listPayments As New List(Of Single)

    '                    Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '                    objInputXMLDoc.XmlResolver = Nothing
    '                    objInputXMLDoc.LoadXml(Row.Item("XMLReceived"))
    '                    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '                    objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '                    objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1")
    '                    objNSM.AddNamespace("def", "http://www.origoservices.com")

    '                    strSumAssured = getNodeText(objInputXMLDoc, objNSM, "//quo:get_wol_quote_response/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
    '                    strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:get_wol_quote_response/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:risk_contribution/def:contribution/def:amount")

    '                    Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_wol_quote_response/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
    '                    If (objQuotes.Count > 0) Then
    '                        For Each objQuote As XmlNode In objQuotes
    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                listPayments.Add(strPayment)
    '                            End If
    '                        Next
    '                        listPayments.Sort()
    '                    End If
    '                    objInputXMLDoc = Nothing

    '                    .WriteLine("<tr class=""" & strClass & """>")
    '                    If (checkValue(strSumAssured)) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    If (checkValue(strMonthlyBenefit)) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strMonthlyBenefit)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    If (listPayments.Count > 0) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(listPayments.First)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("XMLReceivedDate")) & "<span class=""sort-key displayNone"">" & Row.Item("XMLReceivedDate") & "</span></td>")
    '                    .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&XMLReceivedID=" & Row.Item("XMLReceivedID") & """>View</a></td>")
    '                    .WriteLine("</tr>")
    '                    strClass = nextClass(strClass)
    '                Next
    '            Else
    '                .WriteLine("<tr class=""" & strClass & """>")
    '                .WriteLine("<td class=""sml"" colspan=""5"">No previous quotes found.</td>")
    '                .WriteLine("</tr>")
    '            End If
    '            .WriteLine("</tbody>")
    '            .WriteLine("</table>")
    '            .WriteLine("</div>")
    '            dsCache = Nothing
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
    '            Dim dteXMLDate As String = getAnyField("XMLReceivedDate", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_wol_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_wol_quote_response/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr>")
    '                    .WriteLine("<td colspan=""8"" class=""smlr""><a href=""javascript:history.back()""><img src=""/images/icons/arrow_submit_back.png"" width=""16"" height=""16"" border=""0"" title=""Back"" /></a></td>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
    '                            If (Not checkValue(strSumAssured)) Then
    '                                strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (checkValue(strSumAssured)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True" And DateDiff(DateInterval.Day, CDate(dteXMLDate), Config.DefaultDateTime) <= 30) Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""Proceed""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></td>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    'Public Function getAveloTermQuote(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter
    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            .WriteLine("<script type=""text/javascript"">")
    '            .WriteLine("$(document).ready(function() {")
    '            .WriteLine("    $url = '" & Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&strServiceID=GetTermQuoteV1&intMode=2&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID & "';")
    '            .WriteLine("    $http = httpRequest($url,'POST');")
    '            .WriteLine("    if ($http.readyState == 4) {")
    '            .WriteLine("        $arrMessage = $http.responseText.split('|');")
    '            .WriteLine("        if ($arrMessage.length > 1) {")
    '            .WriteLine("            if ($arrMessage[0] == '1') {")
    '            .WriteLine("                window.location = window.location + '&XMLReceivedID=' + $arrMessage[1];")
    '            .WriteLine("            }")
    '            .WriteLine("            else {")
    '            .WriteLine("                $('#msg').text($arrMessage[1]);")
    '            .WriteLine("                $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("            }")
    '            .WriteLine("        }")
    '            .WriteLine("        else {")
    '            .WriteLine("            $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("            $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("        }")
    '            .WriteLine("    }")
    '            .WriteLine("    else {")
    '            .WriteLine("        $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("        $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("    }")
    '            .WriteLine("});")
    '            .WriteLine("</script>")
    '            .WriteLine("<div id=""msgContainer"" class=""lrgc displayNone""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span id=""msg"" style=""color:red;font-weight:bold;""></span></div>")
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMlReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_term_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_term_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
    '                            Dim strCISumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[def:risk_cover/def:risk_event/text() = 'Critical Illness']/def:risk_cover/def:lump_sum_benefit/def:amount")
    '                            Dim strAccelerated As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:benefits_required/def:death_or_earlier_cic_ind")
    '                            If (Not checkValue(strSumAssured)) Then
    '                                strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (Not checkValue(strCISumAssured)) Then
    '                                strCISumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[2]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (checkValue(strSumAssured)) Then
    '                                Dim strTitle As String = ""
    '                                If (checkValue(strCISumAssured)) Then
    '                                    strTitle = "Life = " & displayCurrency(CDec(strSumAssured)) & ", CI = " & displayCurrency(CDec(strCISumAssured))
    '                                    If (strAccelerated = "Yes") Then
    '                                        strTitle += " Accelerator Policy"
    '                                    Else
    '                                        strTitle += " Additional Cover"
    '                                    End If
    '                                Else
    '                                    strTitle = ""
    '                                End If
    '                                .WriteLine("<td class=""smlc"" title=""" & strTitle & """>" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True") Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></td>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    'Public Function getAveloPreviousTermQuotes(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter

    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            Dim strSumAssured As String = "", strMonthlyBenefit As String = ""
    '            Dim strClass As String = "row1"
    '            Dim strSQL As String = "SELECT XMLReceivedID, XMLReceivedDate, XMLReceived FROM tblxmlreceived WHERE XMLReceivedResult = 102 AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY XMLReceivedDate DESC"

    '            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '            .WriteLine("<div id=""hotkeyBox"">")
    '            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '            .WriteLine("<thead>")
    '            .WriteLine("<tr class=""tablehead"">")
    '            .WriteLine("<th class=""smlc sort-numeric"">Sum Assured</th>")
    '            .WriteLine("<th class=""smlc sort-numeric"">Monthly Benefit</th>")
    '            .WriteLine("<th class=""smlc sort-numeric"">Lowest Payment</th>")
    '            .WriteLine("<th class=""smlc sort-date"">Quote Date</th>")
    '            .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '            .WriteLine("</tr>")
    '            .WriteLine("</thead>")
    '            .WriteLine("<tbody>")
    '            If (dsCache.Rows.Count > 0) Then
    '                For Each Row As DataRow In dsCache.Rows
    '                    Dim listPayments As New List(Of Single)

    '                    Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '                    objInputXMLDoc.XmlResolver = Nothing
    '                    objInputXMLDoc.LoadXml(Row.Item("XMLReceived"))
    '                    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '                    objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '                    objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1")
    '                    objNSM.AddNamespace("def", "http://www.origoservices.com")

    '                    strSumAssured = getNodeText(objInputXMLDoc, objNSM, "//quo:get_term_quote_response/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
    '                    strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:get_term_quote_response/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:risk_contribution/def:contribution/def:amount")

    '                    Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_term_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                    If (objQuotes.Count > 0) Then
    '                        For Each objQuote As XmlNode In objQuotes
    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                listPayments.Add(strPayment)
    '                            End If
    '                        Next
    '                        listPayments.Sort()
    '                    End If
    '                    objInputXMLDoc = Nothing

    '                    .WriteLine("<tr class=""" & strClass & """>")
    '                    If (checkValue(strSumAssured)) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    If (checkValue(strMonthlyBenefit)) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strMonthlyBenefit)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    If (listPayments.Count > 0) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(listPayments.First)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("XMLReceivedDate")) & "<span class=""sort-key displayNone"">" & Row.Item("XMLReceivedDate") & "</span></td>")
    '                    .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&XMLReceivedID=" & Row.Item("XMLReceivedID") & """>View</a></td>")
    '                    .WriteLine("</tr>")
    '                    strClass = nextClass(strClass)
    '                Next
    '            Else
    '                .WriteLine("<tr class=""" & strClass & """>")
    '                .WriteLine("<td class=""sml"" colspan=""5"">No previous quotes found.</td>")
    '                .WriteLine("</tr>")
    '            End If
    '            .WriteLine("</tbody>")
    '            .WriteLine("</table>")
    '            .WriteLine("</div>")
    '            dsCache = Nothing
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
    '            Dim dteXMLDate As String = getAnyField("XMLReceivedDate", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_term_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_term_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr>")
    '                    .WriteLine("<td colspan=""8"" class=""smlr""><a href=""javascript:history.back()""><img src=""/images/icons/arrow_submit_back.png"" width=""16"" height=""16"" border=""0"" title=""Back"" /></a></td>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
    '                            Dim strCISumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[def:risk_cover/def:risk_event/text() = 'Critical Illness']/def:risk_cover/def:lump_sum_benefit/def:amount")
    '                            Dim strAccelerated As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:benefits_required/def:death_or_earlier_cic_ind")
    '                            If (Not checkValue(strSumAssured)) Then
    '                                strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (Not checkValue(strCISumAssured)) Then
    '                                strCISumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[2]/def:lump_sum_benefit/def:amount")
    '                            End If
    '                            If (checkValue(strSumAssured)) Then
    '                                Dim strTitle As String = ""
    '                                If (checkValue(strCISumAssured)) Then
    '                                    strTitle = "Life = " & displayCurrency(CDec(strSumAssured)) & ", CI = " & displayCurrency(CDec(strCISumAssured))
    '                                    If (strAccelerated = "Yes") Then
    '                                        strTitle += " Accelerator Policy"
    '                                    Else
    '                                        strTitle += " Additional Cover"
    '                                    End If
    '                                Else
    '                                    strTitle = ""
    '                                End If
    '                                .WriteLine("<td class=""smlc"" title=""" & strTitle & """>" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True" And DateDiff(DateInterval.Day, CDate(dteXMLDate), Config.DefaultDateTime) <= 30) Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""Proceed""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></td>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    'Public Function getAveloIPQuote(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter
    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            .WriteLine("<script type=""text/javascript"">")
    '            .WriteLine("$(document).ready(function() {")
    '            .WriteLine("    $url = '" & Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&strServiceID=GetIPQuoteV1&intMode=3&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID & "';")
    '            .WriteLine("    $http = httpRequest($url,'POST');")
    '            .WriteLine("    if ($http.readyState == 4) {")
    '            .WriteLine("        $arrMessage = $http.responseText.split('|');")
    '            .WriteLine("        if ($arrMessage.length > 1) {")
    '            .WriteLine("            if ($arrMessage[0] == '1') {")
    '            .WriteLine("                window.location = window.location + '&XMLReceivedID=' + $arrMessage[1];")
    '            .WriteLine("            }")
    '            .WriteLine("            else {")
    '            .WriteLine("                $('#msg').text($arrMessage[1]);")
    '            .WriteLine("                $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("            }")
    '            .WriteLine("        }")
    '            .WriteLine("        else {")
    '            .WriteLine("            $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("            $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("        }")
    '            .WriteLine("    }")
    '            .WriteLine("    else {")
    '            .WriteLine("        $('#msg').text('Connection error. Please contact Engaged Solutions');")
    '            .WriteLine("        $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
    '            .WriteLine("    }")
    '            .WriteLine("});")
    '            .WriteLine("</script>")
    '            .WriteLine("<div id=""msgContainer"" class=""lrgc displayNone""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span id=""msg"" style=""color:red;font-weight:bold;""></span></div>")
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMlReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_ip_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_ip_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")
    '                            If (checkValue(strSumAssured)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:payment_period/def:term/def:months")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True") Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""Proceed""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></td>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    'Public Function getAveloPreviousIPQuotes(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
    '    Dim objStringWriter As StringWriter = New StringWriter

    '    With objStringWriter
    '        If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
    '            Dim strMonthlyBenefit As String = ""
    '            Dim strClass As String = "row1"
    '            Dim strSQL As String = "SELECT XMLReceivedID, XMLReceivedDate, XMLReceived FROM tblxmlreceived WHERE XMLReceivedResult = 103 AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY XMLReceivedDate DESC"
    '            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
    '            .WriteLine("<div id=""hotkeyBox"">")
    '            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '            .WriteLine("<thead>")
    '            .WriteLine("<tr class=""tablehead"">")
    '            .WriteLine("<th class=""smlc sort-numeric"">Monthly Benefit</th>")
    '            .WriteLine("<th class=""smlc sort-numeric"">Lowest Payment</th>")
    '            .WriteLine("<th class=""smlc sort-date"">Quote Date</th>")
    '            .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '            .WriteLine("</tr>")
    '            .WriteLine("</thead>")
    '            .WriteLine("<tbody>")
    '            If (dsCache.Rows.Count > 0) Then
    '                For Each Row As DataRow In dsCache.Rows
    '                    Dim listPayments As New List(Of Single)

    '                    Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '                    objInputXMLDoc.XmlResolver = Nothing
    '                    objInputXMLDoc.LoadXml(Row.Item("XMLReceived"))
    '                    objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '                    objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '                    objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1")
    '                    objNSM.AddNamespace("def", "http://www.origoservices.com")

    '                    strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:get_ip_quote_response/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")

    '                    Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_ip_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                    If (objQuotes.Count > 0) Then
    '                        For Each objQuote As XmlNode In objQuotes
    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                listPayments.Add(strPayment)
    '                            End If
    '                        Next
    '                        listPayments.Sort()
    '                    End If
    '                    objInputXMLDoc = Nothing

    '                    .WriteLine("<tr class=""" & strClass & """>")
    '                    If (checkValue(strMonthlyBenefit)) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strMonthlyBenefit)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    If (listPayments.Count > 0) Then
    '                        .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(listPayments.First)) & "</td>")
    '                    Else
    '                        .WriteLine("<td class=""smlc"">N/A</td>")
    '                    End If
    '                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("XMLReceivedDate")) & "<span class=""sort-key displayNone"">" & Row.Item("XMLReceivedDate") & "</span></td>")
    '                    .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&XMLReceivedID=" & Row.Item("XMLReceivedID") & """>View</a></td>")
    '                    .WriteLine("</tr>")
    '                    strClass = nextClass(strClass)
    '                Next
    '            Else
    '                .WriteLine("<tr class=""" & strClass & """>")
    '                .WriteLine("<td class=""sml"" colspan=""4"">No previous quotes found.</td>")
    '                .WriteLine("</tr>")
    '            End If
    '            .WriteLine("</tbody>")
    '            .WriteLine("</table>")
    '            .WriteLine("</div>")
    '            dsCache = Nothing
    '        Else
    '            Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
    '            Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

    '            Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
    '            Dim dteXMLDate As String = getAnyField("XMLReceivedDate", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

    '            objInputXMLDoc.XmlResolver = Nothing
    '            objInputXMLDoc.LoadXml(strXML)
    '            objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
    '            objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
    '            objNSM.AddNamespace("quo", "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1")
    '            objNSM.AddNamespace("def", "http://www.origoservices.com")

    '            Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:get_ip_quote_response/quo:status", objNSM)
    '            If Not (objSuccess Is Nothing) Then
    '                If (objSuccess.InnerText <> "Success") Then
    '                    boolSuccess = False
    '                    strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
    '                End If
    '            Else
    '                boolSuccess = False
    '                strErrorMessage = "Data error. Please contact Engaged Solutions."
    '            End If
    '            If (boolSuccess) Then
    '                Dim objQuotes As XmlNodeList = objInputXMLDoc.SelectNodes("//quo:get_ip_quote_response/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
    '                If (objQuotes.Count > 0) Then
    '                    Dim strClass As String = "row1"
    '                    .WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
    '                    .WriteLine("<div id=""hotkeyBox"">")
    '                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table sortable"" width=""100%"">")
    '                    .WriteLine("<thead>")
    '                    .WriteLine("<tr>")
    '                    .WriteLine("<td colspan=""8"" class=""smlr""><a href=""javascript:history.back()""><img src=""/images/icons/arrow_submit_back.png"" width=""16"" height=""16"" border=""0"" title=""Back"" /></a></td>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("<tr class=""tablehead"">")
    '                    .WriteLine("<th class=""smlc sort-alpha"">Provider</th>")
    '                    .WriteLine("<th class=""smlc"">Product</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Sum Ass.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Term</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric auto_asc"">Prem.</th>")
    '                    .WriteLine("<th class=""smlc sort-numeric"">Comm.</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("<th class=""smlc"">&nbsp;</th>")
    '                    .WriteLine("</tr>")
    '                    .WriteLine("</thead>")
    '                    .WriteLine("<tbody>")
    '                    Dim x As Integer = 0
    '                    For Each objQuote As XmlNode In objQuotes
    '                        .WriteLine("<tr class=""" & strClass & """>")
    '                        .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
    '                        Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
    '                        If (strStatus = "Success" Or strStatus = "Warning") Then
    '                            Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
    '                            If (Not checkValue(strProduct)) Then
    '                                strProduct = "N/A"
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

    '                            Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")
    '                            If (checkValue(strSumAssured)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strTerm As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:payment_period/def:term/def:months")
    '                            If (Not checkValue(strTerm)) Then
    '                                strTerm = "N/A"
    '                            Else
    '                                strTerm = Fix(CInt(strTerm) / 12)
    '                            End If
    '                            .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

    '                            Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
    '                            If (Not checkValue(strPayment)) Then
    '                                strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
    '                            End If
    '                            If (checkValue(strPayment)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
    '                            If (checkValue(strCommission)) Then
    '                                .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">N/A</td>")
    '                            End If

    '                            Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
    '                            If (strExtranetAvailable = "True" And DateDiff(DateInterval.Day, CDate(dteXMLDate), Config.DefaultDateTime) <= 30) Then
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & """ onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""Proceed""><img src=""/images/arrow_submit.gif"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            Else
    '                                Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
    '                                Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
    '                                .WriteLine("<td class=""smlc""><a href=""/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y"" onclick=""$('.loadingOverlay').removeClass('displayNone').addClass('displayBlock');"" title=""View Documents""><img src=""/images/icons/view.png"" width=""16"" height=""16"" border=""0"" /></a></td>")
    '                            End If

    '                            Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
    '                            Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
    '                            If (checkValue(strCommissionNotes)) Then
    '                                If (checkValue(strNotes)) Then
    '                                    strNotes += "<br />" & strCommissionNotes
    '                                Else
    '                                    strNotes += strCommissionNotes
    '                                End If
    '                            End If

    '                            If (checkValue(strNotes)) Then
    '                                .WriteLine("<td class=""info smlc"">")
    '                                .WriteLine("<img class=""floatLeft"" src=""/images/icons/info.gif"" width=""16"" height=""16"" title=""Hover for more information"" />")
    '                                .WriteLine("<div class=""tooltip displayNone"" style=""width: 400px;white-space: normal;"">" & Replace(Replace(Replace(strNotes, ". ", ".<br />"), "~~", "<br />"), "~", " ") & "</div>")
    '                                .WriteLine("</td>")
    '                            Else
    '                                .WriteLine("<td class=""smlc"">&nbsp;</td>")
    '                            End If

    '                        Else
    '                            .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
    '                        End If
    '                        .WriteLine("</tr>")
    '                        strClass = nextClass(strClass)
    '                        x += 1
    '                    Next
    '                    .WriteLine("</tbody>")
    '                    .WriteLine("</table>")
    '                    .WriteLine("</div>")
    '                Else
    '                    .WriteLine("<td class=""sml"" colspan=""8""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">No quotes available.</span></td>")
    '                End If

    '                objInputXMLDoc = Nothing

    '            Else
    '                .WriteLine("<div class=""lrgc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Connection error. Please contact Engaged Solutions.</span></div>")
    '            End If
    '        End If

    '    End With
    '    Return objStringWriter.ToString
    'End Function

    Public Function getAveloQuoteText(ByVal cache As Web.Caching.Cache, ByVal AppID As String, serviceid As String, mode As String) As String
        Dim strNS As String = "", strRootNode As String = ""
        Select Case serviceid
            Case "GetWOLQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1"
                strRootNode = "get_wol_quote_response"
            Case "GetTermQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1"
                strRootNode = "get_term_quote_response"
            Case "GetIPQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1"
                strRootNode = "get_ip_quote_response"
        End Select
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
                .WriteLine("<script type=""text/javascript"">")
                .WriteLine("$(document).ready(function() {")
                .WriteLine("    $url = '" & Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&strServiceID=" & serviceid & "&intMode=" & mode & "&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID & "';")
                .WriteLine("    $http = httpRequest($url,'POST');")
                .WriteLine("    if ($http.readyState == 4) {")
                .WriteLine("        $arrMessage = $http.responseText.split('|');")
                .WriteLine("        if ($arrMessage.length > 1) {")
                .WriteLine("            if ($arrMessage[0] == '1') {")
                .WriteLine("                window.location = window.location + '&XMLReceivedID=' + $arrMessage[1];")
                .WriteLine("            }")
                .WriteLine("            else {")
                .WriteLine("                $('#msg').text($arrMessage[1]);")
                .WriteLine("                $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
                .WriteLine("            }")
                .WriteLine("        }")
                .WriteLine("        else {")
                .WriteLine("            $('#msg').text('Connection error. Please contact Engaged Solutions');")
                .WriteLine("            $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
                .WriteLine("        }")
                .WriteLine("    }")
                .WriteLine("    else {")
                .WriteLine("        $('#msg').text('Connection error. Please contact Engaged Solutions');")
                .WriteLine("        $('#msgContainer').removeClass('displayNone').addClass('displayBlock');")
                .WriteLine("    }")
                .WriteLine("});")
                .WriteLine("</script>")
                .WriteLine("<div id=""msgContainer"" class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p id=""msg""></p></div>")
            Else
                Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
                Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

                Dim strXML As String = getAnyField("XMlReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

                objInputXMLDoc.XmlResolver = Nothing
                objInputXMLDoc.LoadXml(strXML)
                objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM.AddNamespace("quo", strNS)
                objNSM.AddNamespace("def", "http://www.origoservices.com")

                Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:" & strRootNode & "/quo:status", objNSM)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "Success") Then
                        boolSuccess = False
                        strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage = "Data error. Please contact Engaged Solutions."
                End If
                If (boolSuccess) Then
                    Dim objQuotes As XmlNodeList = Nothing
                    If (serviceid = "GetWOLQuoteV1") Then
                        objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
                    Else
                        objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
                    End If
                    If (objQuotes.Count > 0) Then
                        Dim strClass As String = "row1"
                        .WriteLine("<div id=""hotkeyBox"">")
                        .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped table-highlight sortable"" width=""100%"">")
                        .WriteLine("<thead>")
                        .WriteLine("<tr>")
                        .WriteLine("<th>Provider</th>")
                        .WriteLine("<th>Product</th>")
                        .WriteLine("<th>Sum Ass.</th>")
                        .WriteLine("<th>Term</th>")
                        .WriteLine("<th>Prem.</th>")
                        .WriteLine("<th>Comm.</th>")
                        .WriteLine("<th>Rates</th>")
                        .WriteLine("<th>&nbsp;</th>")
                        .WriteLine("<th>&nbsp;</th>")
                        .WriteLine("</tr>")
                        .WriteLine("</thead>")
                        .WriteLine("<tbody>")
                        Dim x As Integer = 0
                        For Each objQuote As XmlNode In objQuotes
                            .WriteLine("<tr class=""" & strClass & """>")
                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
                            Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
                            If (strStatus = "Success" Or strStatus = "Warning") Then
                                Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
                                If (Not checkValue(strProduct)) Then
                                    strProduct = "N/A"
                                End If
                                .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

                                Select Case serviceid
                                    Case "GetWOLQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
                                        If (Not checkValue(strSumAssured)) Then
                                            strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (checkValue(strSumAssured)) Then
                                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                    Case "GetTermQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
                                        Dim strNewSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:new_value")
                                        If (checkValue(strNewSumAssured)) Then
                                            strSumAssured = strNewSumAssured
                                        End If
                                        Dim strCISumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[def:risk_cover/def:risk_event/text() = 'Critical Illness']/def:risk_cover/def:lump_sum_benefit/def:amount")
                                        Dim strAccelerated As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:benefits_required/def:death_or_earlier_cic_ind")
                                        If (Not checkValue(strSumAssured)) Then
                                            strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (Not checkValue(strCISumAssured)) Then
                                            strCISumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[2]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (checkValue(strSumAssured)) Then
                                            Dim strTitle As String = ""
                                            If (checkValue(strCISumAssured)) Then
                                                strTitle = "Life = " & displayCurrency(CDec(strSumAssured)) & ", CI = " & displayCurrency(CDec(strCISumAssured))
                                                If (strAccelerated = "Yes") Then
                                                    strTitle += " Accelerator Policy"
                                                Else
                                                    strTitle += " Additional Cover"
                                                End If
                                            Else
                                                strTitle = ""
                                            End If
                                            .WriteLine("<td class=""smlc"" title=""" & strTitle & """>" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                    Case "GetIPQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")
                                        If (checkValue(strSumAssured)) Then
                                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "</td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                End Select

                                Dim strTerm As String = ""
                                If (serviceid = "GetIPQuoteV1") Then
                                    strTerm = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:payment_period/def:term/def:months")
                                Else
                                    strTerm = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
                                End If
                                If (Not checkValue(strTerm)) Then
                                    strTerm = "N/A"
                                Else
                                    strTerm = Fix(CInt(strTerm) / 12)
                                End If
                                .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

                                Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
                                If (Not checkValue(strPayment)) Then
                                    strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
                                End If
                                If (serviceid = "GetTermQuoteV1") Then
                                    If (Not checkValue(strPayment)) Then
                                        strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
                                    End If
                                End If
                                If (checkValue(strPayment)) Then
                                    .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
                                If (checkValue(strCommission)) Then
                                    .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strRateType As String = getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_option", "type")
                                If (checkValue(strRateType)) Then
                                    .WriteLine("<td class=""smlc"">" & shortenShowTitle(strRateType, 3) & "</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
                                If (strExtranetAvailable = "True") Then
                                    Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
                                    Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
                                    .WriteLine("<td class=""smlc""><a href=""#"" onClick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y';"" title=""View Documents""><button class= ""btn btn-mini btn-primary"">Docs</button></a>&nbsp;<a href=""#"" onClick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "';"" title=""Proceed""><button class=""btn btn-mini btn-primary"">Proceed</button></a></td>")
                                Else
                                    Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
                                    Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
                                    .WriteLine("<td class=""smlc""><a href=""#"" onClick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y';"" title=""View Documents""><button class= ""btn btn-mini btn-primary"">Docs</button></a></td>")
                                End If

                                Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
                                Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
                                If (checkValue(strCommissionNotes)) Then
                                    If (checkValue(strNotes)) Then
                                        strNotes += "<br />" & strCommissionNotes
                                    Else
                                        strNotes += strCommissionNotes
                                    End If
                                End If

                                If (checkValue(strNotes)) Then
                                    .WriteLine("<td class=""info smlc"">")
                                    .WriteLine("<i style=""font-size:20px;"" class=""icon-info-circle ui-tooltip"" title=""" & Replace(Replace(Replace(Replace(strNotes, ". ", "." & VbCrLf), "~~", VbCrLf), "~", " "), "<br />", VBCrLf) & """ data-placement=""left""></i>")
                                    .WriteLine("</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">&nbsp;</td>")
                                End If

                            Else
                                .WriteLine("<td class=""sml"" colspan=""9""><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
                            End If
                            .WriteLine("</tr>")
                            strClass = nextClass(strClass)
                            x += 1
                        Next
                        .WriteLine("</tbody>")
                        .WriteLine("</table>")
                        .WriteLine("</div>")
                    Else
                        .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
                    End If

                    objInputXMLDoc = Nothing

                Else
                    .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
                End If
            End If

        End With
        Return objStringWriter.ToString
    End Function

    Public Function getAveloPreviousQuotesText(ByVal cache As Web.Caching.Cache, ByVal AppID As String, serviceid As String) As String
        Dim strNS As String = "", strRootNode As String = "", strCode As String = ""
        Select Case serviceid
            Case "GetWOLQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetWOLQuoteResponse/v1"
                strRootNode = "get_wol_quote_response"
                strCode = "101"
            Case "GetTermQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetTermQuoteResponse/v1"
                strRootNode = "get_term_quote_response"
                strCode = "102"
            Case "GetIPQuoteV1"
                strNS = "http://www.exchange.co.uk/schema/GetIPQuoteResponse/v1"
                strRootNode = "get_ip_quote_response"
                strCode = "103"
        End Select
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (Not checkValue(HttpContext.Current.Request("XMLReceivedID"))) Then
                Dim strSumAssured As String = "", strMonthlyBenefit As String = "", strTerm As String = ""
                Dim strClass As String = "row1"
                Dim strSQL As String = "SELECT XMLReceivedID, XMLReceived, XMLReceivedDate FROM tblxmlreceived WHERE XMLReceivedResult = '" & strCode & "' AND XMLReceivedAppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY XMLReceivedDate DESC"
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                .WriteLine("<div id=""hotkeyBox"">")
                .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped table-highlight sortable"" width=""100%"">")
                .WriteLine("<thead>")
                .WriteLine("<tr>")
                .WriteLine("<th>Sum Assured</th>")
                .WriteLine("<th>Term</th>")
                .WriteLine("<th>Monthly Benefit</th>")
                .WriteLine("<th>Lowest Payment</th>")
                .WriteLine("<th>Quote Date</th>")
                .WriteLine("<th>&nbsp;</th>")
                .WriteLine("</tr>")
                .WriteLine("</thead>")
                .WriteLine("<tbody>")
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        Dim listPayments As New List(Of Single)

                        Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
                        objInputXMLDoc.XmlResolver = Nothing
                        objInputXMLDoc.LoadXml(Row.Item("XMLReceived"))
                        objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                        objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                        objNSM.AddNamespace("quo", strNS)
                        objNSM.AddNamespace("def", "http://www.origoservices.com")

                        Select Case serviceid
                            Case "GetWOLQuoteV1"
                                strSumAssured = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
                                strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:risk_contribution/def:contribution/def:amount")
                                strTerm = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
                            Case "GetTermQuoteV1"
                                strSumAssured = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
                                strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:risk_contribution/def:contribution/def:amount")
                                strTerm = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
                            Case "GetIPQuoteV1"
                                strMonthlyBenefit = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")
                                strTerm = getNodeText(objInputXMLDoc, objNSM, "//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse/quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:payment_period/def:term/def:months")
                        End Select

                        Dim objQuotes As XmlNodeList = Nothing
                        If (serviceid = "GetWOLQuoteV1") Then
                            objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
                        Else
                            objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
                        End If
                        If (objQuotes.Count > 0) Then
                            For Each objQuote As XmlNode In objQuotes
                                Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
                                If (Not checkValue(strPayment)) Then
                                    strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
                                End If
                                If (serviceid = "GetTermQuoteV1") Then
                                    If (Not checkValue(strPayment)) Then
                                        strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
                                    End If
                                End If
                                If (checkValue(strPayment)) Then
                                    listPayments.Add(strPayment)
                                End If
                            Next
                            listPayments.Sort()
                        End If
                        objInputXMLDoc = Nothing

                        .WriteLine("<tr class=""" & strClass & """>")
                        If (checkValue(strSumAssured)) Then
                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
                        Else
                            .WriteLine("<td class=""smlc"">N/A</td>")
                        End If
                        If (Not checkValue(strTerm)) Then
                            strTerm = "N/A"
                        Else
                            strTerm = Fix(CInt(strTerm) / 12)
                        End If
                        .WriteLine("<td class=""smlc"">" & strTerm & "</td>")
                        If (checkValue(strMonthlyBenefit)) Then
                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strMonthlyBenefit)) & "</td>")
                        Else
                            .WriteLine("<td class=""smlc"">N/A</td>")
                        End If
                        If (listPayments.Count > 0) Then
                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(listPayments.First)) & "</td>")
                        Else
                            .WriteLine("<td class=""smlc"">N/A</td>")
                        End If
                        .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("XMLReceivedDate")) & "<span class=""sort-key displayNone"">" & Row.Item("XMLReceivedDate") & "</span></td>")
                        .WriteLine("<td class=""smlc""><a href=""#"" onClick=""javascript: location.href = '/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&XMLReceivedID=" & Row.Item("XMLReceivedID") & "';""><button class=""btn btn-mini btn-primary"">View</button></a></td>")
                        .WriteLine("</tr>")
                        strClass = nextClass(strClass)
                    Next
                Else
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""sml"" colspan=""6"">No previous quotes found.</td>")
                    .WriteLine("</tr>")
                End If
                .WriteLine("</tbody>")
                .WriteLine("</table>")
                .WriteLine("</div>")
                dsCache = Nothing
            Else
                Dim objInputXMLDoc As XmlDocument = New XmlDocument, objNSM As XmlNamespaceManager
                Dim boolSuccess As Boolean = True, strErrorMessage As String = ""

                Dim strXML As String = getAnyField("XMLReceived", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))
                Dim dteXMLDate As String = getAnyField("XMLReceivedDate", "tblxmlreceived", "XMLReceivedID", HttpContext.Current.Request("XMLReceivedID"))

                objInputXMLDoc.XmlResolver = Nothing
                objInputXMLDoc.LoadXml(strXML)
                objNSM = New XmlNamespaceManager(objInputXMLDoc.NameTable)
                objNSM.AddNamespace("esresp", "http://www.exchange.co.uk/schema/ExchangeServicesResponse/v1")
                objNSM.AddNamespace("quo", strNS)
                objNSM.AddNamespace("def", "http://www.origoservices.com")

                Dim objSuccess As XmlNode = objInputXMLDoc.SelectSingleNode("//quo:" & strRootNode & "/quo:status", objNSM)
                If Not (objSuccess Is Nothing) Then
                    If (objSuccess.InnerText <> "Success") Then
                        boolSuccess = False
                        strErrorMessage = "Quote error. Please contact Engaged Solutions.<br /><br />Reason: Inconsistent data found."
                    End If
                Else
                    boolSuccess = False
                    strErrorMessage = "Data error. Please contact Engaged Solutions."
                End If
                If (boolSuccess) Then
                    Dim objQuotes As XmlNodeList = Nothing
                    If (serviceid = "GetWOLQuoteV1") Then
                        objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[(quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning') and (quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:regulated = 'N')]", objNSM)
                    Else
                        objQuotes = objInputXMLDoc.SelectNodes("//quo:" & strRootNode & "/quo:quotexml/quo:quoteresponse[quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Success' or quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status/text() = 'Warning']", objNSM)
                    End If
                    If (objQuotes.Count > 0) Then

                        .WriteLine("<a href=""#"" onclick=""javascript:history.back()""><button class=""btn btn-primary btn-mini"" title=""Back""><i class=""icon-arrow-left""></i> Back</button></a><br /><br />")

                        Dim strClass As String = "row1"
                        '.WriteLine("<div class=""loadingOverlay displayNone""><div class=""loadingAnim"" style=""left: 46%; top: 39%;"">Please Wait</div></div>")
                        .WriteLine("<div id=""hotkeyBox"">")
                        .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped table-highlight sortable"" width=""100%"">")
                        .WriteLine("<thead>")
                        .WriteLine("<tr>")
                        .WriteLine("<th>Provider</th>")
                        .WriteLine("<th>Product</th>")
                        .WriteLine("<th>Sum Ass.</th>")
                        .WriteLine("<th>Term</th>")
                        .WriteLine("<th>Prem.</th>")
                        .WriteLine("<th>Comm.</th>")
                        .WriteLine("<th>Rates</th>")
                        .WriteLine("<th>&nbsp;</th>")
                        .WriteLine("<th>&nbsp;</th>")
                        .WriteLine("</tr>")
                        .WriteLine("</thead>")
                        .WriteLine("<tbody>")
                        Dim x As Integer = 0
                        For Each objQuote As XmlNode In objQuotes
                            .WriteLine("<tr class=""" & strClass & """>")
                            .WriteLine("<td class=""smlc"">" & shortenShowTitle(getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction", "originator_ID"), 20) & "</td>")
                            Dim strStatus As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:quote_response_status")
                            If (strStatus = "Success" Or strStatus = "Warning") Then
                                Dim strProduct As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_name")
                                If (Not checkValue(strProduct)) Then
                                    strProduct = "N/A"
                                End If
                                .WriteLine("<td class=""smlc"">" & shortenShowTitle(strProduct, 30) & "</td>")

                                Select Case serviceid
                                    Case "GetWOLQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:lump_sum_benefit/def:amount")
                                        If (Not checkValue(strSumAssured)) Then
                                            strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (checkValue(strSumAssured)) Then
                                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                    Case "GetTermQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:original_value")
                                        Dim strNewSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:tpsdata/def:exweb_varied[@varied_name = 'Sum Assured']/def:new_value")
                                        If (checkValue(strNewSumAssured)) Then
                                            strSumAssured = strNewSumAssured
                                        End If
                                        Dim strCISumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[def:risk_cover/def:risk_event/text() = 'Critical Illness']/def:risk_cover/def:lump_sum_benefit/def:amount")
                                        Dim strAccelerated As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:benefits_required/def:death_or_earlier_cic_ind")
                                        If (Not checkValue(strSumAssured)) Then
                                            strSumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[1]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (Not checkValue(strCISumAssured)) Then
                                            strCISumAssured = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:risk_benefit[2]/def:lump_sum_benefit/def:amount")
                                        End If
                                        If (checkValue(strSumAssured)) Then
                                            Dim strTitle As String = ""
                                            If (checkValue(strCISumAssured)) Then
                                                strTitle = "Life = " & displayCurrency(CDec(strSumAssured)) & ", CI = " & displayCurrency(CDec(strCISumAssured))
                                                If (strAccelerated = "Yes") Then
                                                    strTitle += " Accelerator Policy"
                                                Else
                                                    strTitle += " Additional Cover"
                                                End If
                                            Else
                                                strTitle = ""
                                            End If
                                            .WriteLine("<td class=""smlc"" title=""" & strTitle & """>" & displayCurrency(CDec(strSumAssured)) & "<span class=""sort-key displayNone"">" & strSumAssured & "</span></td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                    Case "GetIPQuoteV1"
                                        Dim strSumAssured As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:amount")
                                        If (checkValue(strSumAssured)) Then
                                            .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strSumAssured)) & "</td>")
                                        Else
                                            .WriteLine("<td class=""smlc"">N/A</td>")
                                        End If
                                End Select

                                Dim strTerm As String = ""
                                If (serviceid = "GetIPQuoteV1") Then
                                    strTerm = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit/def:risk_cover/def:income_benefit/def:payment_period/def:term/def:months")
                                Else
                                    strTerm = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:number_of_contributions")
                                End If
                                If (Not checkValue(strTerm)) Then
                                    strTerm = "N/A"
                                Else
                                    strTerm = Fix(CInt(strTerm) / 12)
                                End If
                                .WriteLine("<td class=""smlc"">" & strTerm & "</td>")

                                Dim strPayment As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:total_contribution/def:gross_amount")
                                If (Not checkValue(strPayment)) Then
                                    strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:risk_benefit[@id='rb1']/def:risk_contribution/def:contribution/def:amount")
                                End If
                                If (serviceid = "GetTermQuoteV1") Then
                                    If (Not checkValue(strPayment)) Then
                                        strPayment = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:multiple_risk_contribution/def:contribution/def:amount")
                                    End If
                                End If
                                If (checkValue(strPayment)) Then
                                    .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strPayment)) & "<span class=""sort-key displayNone"">" & strPayment & "</span></td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strCommission As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement[@type='Initial']/def:payment/def:amount")
                                If (checkValue(strCommission)) Then
                                    .WriteLine("<td class=""smlc"">" & displayCurrency(CDec(strCommission)) & "</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strRateType As String = getNodeAttribute(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:product_option", "type")
                                If (checkValue(strRateType)) Then
                                    .WriteLine("<td class=""smlc"">" & shortenShowTitle(strRateType, 3) & "</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">N/A</td>")
                                End If

                                Dim strExtranetAvailable As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:tpsdata/def:exchange_extranet_available")
                                If (strExtranetAvailable = "True" And DateDiff(DateInterval.Day, CDate(dteXMLDate), Config.DefaultDateTime) <= 30) Then
                                    Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
                                    Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
                                    .WriteLine("<td class=""smlc""><a href=""#"" onClick=""javascript:location.href='/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y';"" title=""View Documents""><button class=""btn btn-mini btn-primary"">Docs</button></a>&nbsp;<a href=""#"" onClick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "';"" title=""Proceed""><button class= ""btn btn-mini btn-primary"">Proceed</button></a></td>")
                                Else
                                    Dim strQuoteID As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_control/quo:initiator_orchestration_id") & "_" & objQuote.Attributes("quotesequence").Value
                                    Dim strICRN As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:b_control/def:intermediary_case_reference_number")
                                    .WriteLine("<td class=""smlc""><a href=""#"" onClick=""javascript:location.href='/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & strQuoteID & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & strICRN & "&strView=Y';"" title=""View Documents""><button class=""btn btn-mini btn-primary"">Docs</button></a></td>")
                                End If

                                Dim strNotes As String = getNodeListText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:note")
                                Dim strCommissionNotes As String = getNodeText(objQuote, objNSM, "quo:message/quo:m_content/quo:transaction/def:message/def:m_content/def:application/def:product/def:client_specific_illustration/def:commission_entitlement/def:free_text")
                                If (checkValue(strCommissionNotes)) Then
                                    If (checkValue(strNotes)) Then
                                        strNotes += vbcrlf & strCommissionNotes
                                    Else
                                        strNotes += strCommissionNotes
                                    End If
                                End If

                                If (checkValue(strNotes)) Then
                                    .WriteLine("<td class=""info smlc"">")
                                    .WriteLine("<i style=""font-size:20px;"" class=""icon-info-circle ui-tooltip"" title=""" & Replace(Replace(Replace(Replace(strNotes, ". ", "." & VbCrLf), "~~", VbCrLf), "~", " "), "<br />", VBCrLf) & """ data-placement=""left""></i>")
                                    .WriteLine("</td>")
                                Else
                                    .WriteLine("<td class=""smlc"">&nbsp;</td>")
                                End If

                            Else
                                .WriteLine("<td class=""sml"" colspan=""9""><i class=""icon-exclamation-sign""></i><span style=""color:red;font-weight:bold;"">Error: could not provide quote</span></td>")
                            End If
                            .WriteLine("</tr>")
                            strClass = nextClass(strClass)
                            x += 1
                        Next
                        .WriteLine("</tbody>")
                        .WriteLine("</table>")
                        .WriteLine("</div>")
                    Else
                        .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
                    End If

                    objInputXMLDoc = Nothing

                Else
                    .WriteLine("<div class=""medc""><br /><br /><img src=""/images/error.png"" width=""16"" height=""16"" />&nbsp;<span style=""color:red;font-weight:bold;"">An error occured whilst connecting. Please try again later.<br />If the problem persists, please contact Engaged Solutions.</span></div>")
                End If
            End If

        End With
        Return objStringWriter.ToString
    End Function

    Public Function getAveloNewBusiness(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            'responseWrite(Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & HttpContext.Current.Request("ICRN") & "&intMode=4&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            'responseEnd()
            Dim objResponse As Net.HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/outbound/avelo/", "AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & HttpContext.Current.Request("ICRN") & "&intMode=4&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            If (checkResponse(objResponse, "AppID=" & AppID)) Then
                HttpContext.Current.Response.Clear()
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                .WriteLine(objReader.ReadToEnd())
                objReader.Close()
                objReader = Nothing
            Else
                .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
            End If
            objResponse.Close()
            objResponse = Nothing
        End With
        responseWrite(objStringWriter.ToString)
        responseEnd()
        Return ""
    End Function

    Public Function saveAveloDocuments(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            'responseWrite(Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&intMode=6&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            'responseEnd()
            Dim objResponse As Net.HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/outbound/avelo/", "AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&intMode=6&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            If (checkResponse(objResponse, "AppID=" & AppID)) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                .WriteLine("<a href=""#"" onclick=""javascript:history.back()""><button class=""btn btn-primary btn-mini"" title=""Back""><i class=""icon-arrow-left""></i> Back</button></a>")
                .WriteLine("&nbsp;<a href=""#"" onclick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & HttpContext.Current.Request("ICRN") & "';""><button class= ""btn btn-mini btn-primary"">Proceed</button></a><br /><br />")
                .WriteLine("<div class=""alert alert-success""><h4 class=""alert-heading"">Documents Saved</h4><p>Documents successfully saved</p></div>")
                objReader.Close()
                objReader = Nothing
            Else
                .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
            End If
            objResponse.Close()
            objResponse = Nothing
        End With
        responseWrite(objStringWriter.ToString)
        responseEnd()
        Return ""
    End Function

    Public Function viewAveloDocuments(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            'responseWrite(Config.ApplicationURL & "/webservices/outbound/avelo/?AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&intMode=5&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            'responseEnd()
            Dim objResponse As Net.HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/outbound/avelo/", "AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&intMode=5&UserID=" & DefaultUserID & "&UserSessionID=" & UserSessionID)
            If (checkResponse(objResponse, "AppID=" & AppID)) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                .WriteLine("<a href=""#"" onclick=""javascript:history.back()""><button class=""btn btn-primary btn-mini"" title=""Back""><i class=""icon-arrow-left""></i> Back</button></a>")
                .WriteLine("&nbsp;<a href=""#"" onclick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & HttpContext.Current.Request("ICRN") & "&strSave=Y';""><button class=""btn btn-primary btn-mini"" title=""Save All"">Save</button></a></a>")
                .WriteLine("&nbsp;<a href=""#"" onclick=""javascript: location.href = '/application/panel.aspx?ApplicationTemplateID=" & ApplicationTemplateID & "&AppID=" & AppID & "&QuoteID=" & HttpContext.Current.Request("QuoteID") & "&XMLReceivedID=" & HttpContext.Current.Request("XMLReceivedID") & "&ICRN=" & HttpContext.Current.Request("ICRN") & "';""><button class= ""btn btn-mini btn-primary"">Proceed</button></a><br /><br />")
                .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered table-striped"" width=""100%"">")
                .WriteLine(objReader.ReadToEnd())
                .WriteLine("</table>")
                objReader.Close()
                objReader = Nothing
            Else
                .WriteLine("<div class=""alert alert-error""><h4 class=""alert-heading"">Error</h4><p>An error occured whilst connecting. Please try again later. If the problem persists, please contact Engaged Solutions.</p></div>")
            End If
            objResponse.Close()
            objResponse = Nothing
        End With
        responseWrite(objStringWriter.ToString)
        responseEnd()
        Return ""
    End Function

    Public Function getCbkCalendar(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim dteStartDate As String = HttpContext.Current.Request("frmStartDate")
        Dim strSQL As String = ""
        If (Not checkValue(dteStartDate)) Then
            dteStartDate = Config.DefaultDate
        End If
        Dim dteEndDate As Date = CDate(dteStartDate).AddDays(7)
        Dim objCalendar As Calendar = New Calendar(Nothing, CDate(dteStartDate).Date, dteEndDate.Date, 8, 20, 0.5, True)
        Dim strSQL2 As String = "SELECT AppID, App1FullName, NextCallDate FROM vwexportapplication " & _
             "WHERE CompanyID = '" & CompanyID & "' AND (SubStatusCode = 'CBK' OR SubStatusCode = '3BK') " & _
             "AND NextCalldate >= " & formatField(objCalendar.StartDate(), "DTTM", Config.DefaultDate) & " " & _
             "AND NextCallDate <= " & formatField(objCalendar.EndDate() & " 23:59:59", "DTTM", Config.DefaultDate) & " "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objCalendar.addEvent(Row.Item("AppID"), "<strong>" & Row.Item("UserFullName") & "</strong><br />" & Row.Item("DiaryTypeDescription"), Row.Item("DiaryDueDate"), Row.Item("DiaryLength"), Row.Item("DiaryComplete"))
            Next
        End If
        dsCache = Nothing
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmEditAppID"" name=""frmEditAppID"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmApp1FullName"" name=""frmApp1FullName"" value="""" />")
            .WriteLine("<input type=""hidden"" id=""frmNextCallDate"" name=""frmDiaryDueDate"" value="""" />")
            .WriteLine("<div style=""padding: 5px;"">")
            .WriteLine("<div class=""lrg"">")
            .WriteLine("<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(-7) & """ title=""Previous Week""><img src=""/images/icons/arrow_prev.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine(FormatDateTime(objCalendar.StartDate(), DateFormat.LongDate) & " - " & FormatDateTime(objCalendar.EndDate().AddDays(-1), DateFormat.LongDate))
            .WriteLine("&nbsp;<a href=""/application/panel.aspx?ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&AppID=" & HttpContext.Current.Request("AppID") & "&UserID=" & HttpContext.Current.Request("UserID") & "&frmStartDate=" & CDate(dteStartDate).AddDays(7) & """ title=""Next Week""><img src=""/images/icons/arrow_next.png"" width=""16"" height=""16"" border=""0"" /></a>")
            .WriteLine("</div><br />")
            .WriteLine("<table class=""table calendar"" style=""width: 100%; height: 100%"" align=""center"">")
            .WriteLine(objCalendar.displayCalendar)
            .WriteLine("</table>")
            .WriteLine("</div>")
            '.WriteLine("<a href=""#"" onclick=""parent.jPrompt('Please specify a time', '', 'Specify Time', 'Ok', 'Cancel', function (r) { if (r) document.form1.submit(); })"">Book</a>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getPaymentHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"">")
            Dim strSQL As String = "SELECT PaymentHistoryID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryMethod, PaymentHistoryReference, PaymentHistoryDate, PaymentHistoryStatus FROM tblpaymenthistory WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' ORDER BY PaymentHistoryDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim intTotalBalance As String = 0
                .WriteLine("<tr class=""tablehead"">")
                .WriteLine("<th class=""smlc"">&nbsp;</th>")
                .WriteLine("<th class=""smlc"">Amount</th>")
                .WriteLine("<th class=""smlc"">Type</th>")
                .WriteLine("<th class=""smlc"">Ref</th>")
                .WriteLine("<th class=""smlc"">Date</th>")
                .WriteLine("<th class=""smlc"">Status</th>")
                .WriteLine("<th class=""smlc"">&nbsp;</th>")
                .WriteLine("</tr>")
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""smlc""><span class=""badge"">" & getPaymentMethod(Row.Item("PaymentHistoryMethod")) & "</span></td>")
                    .WriteLine("<td class=""smlc"">" & displayCurrency(Row.Item("PaymentHistoryAmount")) & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("PaymentHistoryType") & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("PaymentHistoryReference") & "</td>")
                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PaymentHistoryDate")) & "</td>")
                    .WriteLine("<td class=""smlc"">" & getPaymentStatus(Row.Item("PaymentHistoryStatus")) & "</td>")
                    If (objLeadPlatform.Config.SeniorManager Or objLeadPlatform.Config.SuperAdmin) Then
                        .WriteLine("<td class=""smlc""><a href=""#"" onClick=""confirmAction('/inc/processingsave.aspx?strReturnUrl=" & encodeURL(strReturnURL) & "&strThisPg=paymenthistory&strFrmAction=delete&PaymentHistoryID=" & Row.Item("PaymentHistoryID") & "','Are you sure you want to remove this payment for " & displayCurrency(Row.Item("PaymentHistoryAmount")) & "?')""><button class=""btn btn-danger btn-mini""><i class=""icon-remove""></i> Remove</button></a></td>")
                    Else
                        .WriteLine("<td class=""smlc"">&nbsp;</td>")
                    End If
                    .WriteLine("</tr>")
                    If (Row.Item("PaymentHistoryStatus")) Then
                        intTotalBalance += Row.Item("PaymentHistoryAmount")
                    End If
                    strClass = nextClass(strClass)
                Next
                '.WriteLine("<tr class=""tablehead"">")
                '.WriteLine("<th class=""smlc"">Balance</td>")
                '.WriteLine("<th class=""smlc"" colspan=""4"">&nbsp;</td>")
                '.WriteLine("</tr>")
                '.WriteLine("<tr class=""total"">")
                '.WriteLine("<td class=""smlc"">" & displayCurrency(intTotalBalance) & "</td>")
                '.WriteLine("<td class=""smlc"" colspan=""4"">&nbsp;</td>")
                '.WriteLine("</tr>")
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""7"" class=""smlc"">No payments found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getInterestSummary(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"">")
            Dim strSQL As String = "SELECT Amount, ProductTerm, InterestRate, PaymentFrequency, PaidOut, FirstPaymentDue FROM vwpaymentinfo WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                .WriteLine("<tr class=""tablehead"">")
                .WriteLine("<th class=""smlc"">Date</th>")
                .WriteLine("<th class=""smlc"">Interest</th>")
                .WriteLine("<th class=""smlc"">Balance</th>")
                .WriteLine("</tr>")
                Dim intInterest As Decimal = 0, intBalance As Decimal = 0, intCurrentInterest As Decimal = 0, intTotalInterest As Decimal = 0
                Dim dtePaidOut As Date = Nothing, dteFirstPayment As Date = Nothing, dteCurrent As Date = Nothing
                For Each Row As DataRow In dsCache.Rows
                    dtePaidOut = CDate(Row.Item("PaidOut").ToString)
                    dteFirstPayment = CDate(Row.Item("FirstPaymentDue").ToString)
                    intBalance = Row.Item("Amount")
                    Dim dteLastPayment As Date = Nothing
                    'If (Row.Item("PaymentFrequency") = "Monthly") Then
                    '    dteLastPayment = dtePaidOut.AddMonths(Row.Item("ProductTerm"))
                    'ElseIf (Row.Item("PaymentFrequency") = "Weekly") Then
                    '    dteLastPayment = DateAdd(DateInterval.WeekOfYear, Row.Item("ProductTerm"), dtePaidOut)
                    'End If
                    Dim intDays As Integer = Row.Item("ProductTerm")
                    dteLastPayment = DateAdd(DateInterval.Day, intDays, dtePaidOut)
                    For x As Integer = 1 To intDays
                        intInterest = CDec(Row.Item("Amount")) * CDec(Row.Item("InterestRate"))
                        intCurrentInterest += intInterest
                        intTotalInterest += intInterest
                        intBalance += intInterest
                        dteCurrent = dtePaidOut.AddDays(x)
                        If (dteCurrent.AddDays(1).Month = dteCurrent.Month + 1 Or dteCurrent.AddDays(1) > dteLastPayment) Then
                            .WriteLine("<tr class=""" & strClass & """>")
                            .WriteLine("<td class=""smlc"">" & dteCurrent.Date & "</td>")
                            .WriteLine("<td class=""sml"">" & displayCurrency(intCurrentInterest) & "</td>")
                            .WriteLine("<td class=""smlc"">" & displayCurrency(intBalance) & "</td>")
                            .WriteLine("</tr>")
                            intCurrentInterest = 0
                            strClass = nextClass(strClass)
                        End If
                    Next
                Next
                .WriteLine("<tr class=""total"">")
                .WriteLine("<td class=""smlc""></td>")
                .WriteLine("<td class=""smlc""><strong>" & displayCurrency(intTotalInterest) & "</strong></td>")
                .WriteLine("<td class=""smlc""><strong>" & displayCurrency(intBalance) & "</strong></td>")
                .WriteLine("</tr>")
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""3"" class=""smlc"">No summary found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getCreditHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"">")
            Dim strSQL As String = "SELECT CreditHistoryType, CreditHistoryField, CreditHistoryValue, CreditHistoryDate FROM tblcredithistory WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' ORDER BY CreditHistoryType"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim strLastType As String = ""
                For Each Row As DataRow In dsCache.Rows
                    If (strLastType <> Row.Item("CreditHistoryType")) Then
                        .WriteLine("<tr class=""tablehead"">")
                        .WriteLine("<th colspan=""3"" class=""sml"">" & Row.Item("CreditHistoryType") & "</th>")
                        .WriteLine("</tr>")
                    End If
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""sml"">" & Row.Item("CreditHistoryField") & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("CreditHistoryValue") & "</td>")
                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreditHistoryDate")) & "</td>")
                    .WriteLine("</tr>")
                    strLastType = Row.Item("CreditHistoryType")
                    strClass = nextClass(strClass)
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""3"" class=""smlc"">No history found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getCreditSearch(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim objXMLDoc As XmlDocument = New XmlDocument
        Dim strClass As String = "row1"
        With objStringWriter
            Dim strSQL As String = "SELECT TOP 1 CreditSearchData, CreditSearchType, CreditSearchDate FROM tblcreditsearches WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "' ORDER BY CreditSearchDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim strLastType As String = ""
                For Each Row As DataRow In dsCache.Rows
                    If (checkValue(Row.Item("CreditSearchData").ToString)) Then
                        objXMLDoc.LoadXml(Row.Item("CreditSearchData").ToString)
                        Select Case Row.Item("CreditSearchType").ToString
                            Case "Lendprotect"
                                Dim objQCBThree As XmlNodeList = objXMLDoc.SelectSingleNode("//EquifaxQcbThree").ChildNodes
                                If (Not objQCBThree Is Nothing) Then
                                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped table-credit-search"" width=""100%"">")
                                    For Each node As XmlNode In objQCBThree
                                        If (node.Name <> "RawData") Then
                                            Dim strType As String = "Lendprotect"
                                            If (strLastType <> strType) Then
                                                .WriteLine("<tr class=""tablehead"">")
                                                .WriteLine("<th colspan=""3"" class=""sml""><a href=""javascript:return false;"" onclick=""$('.table-credit-search').find('tr:gt(1)').toggle();$('.table-credit-search').find('.icon-toggle').toggleClass('icon-plus icon-minus');""><i class=""icon-plus icon-toggle""></i> " & strType & " - " & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreditSearchDate")) & "</a></th>")
                                                .WriteLine("</tr>")
                                            End If
                                            .WriteLine("<tr class=""" & strClass & """ style=""display:none"">")
                                            .WriteLine("<td class=""sml"">" & getCreditHistoryType(node.Name, "Equifax-QCB3") & "</td>")
                                            .WriteLine("<td class=""smlc"">" & node.InnerText & "</td>")
                                            .WriteLine("</tr>")
                                            strLastType = Row.Item("CreditSearchType")
                                            strClass = nextClass(strClass)
                                        End If
                                    Next
                                    .WriteLine("</table>")

                                    Dim objRawData As XmlElement = objXMLDoc.SelectSingleNode("//EquifaxQcbThree/RawData")
                                    If (Not objRawData Is Nothing) Then
                                        Dim objCDATA As XmlCDataSection = objRawData.FirstChild
                                        Dim objRawDataXML As XmlDocument = New XmlDocument
                                        objRawDataXML.LoadXml(objCDATA.Value)
                                        Dim x As Integer = 0
                                        For Each node As XmlNode In objRawDataXML.SelectSingleNode("//address_details").ChildNodes
                                            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped table-credit-data" & x & """ width=""100%"">")
                                            .WriteLine("<tr class=""tablehead"">")
                                            .WriteLine("<th colspan=""3"" class=""sml""><a href=""javascript:return false;"" onclick=""$('.table-credit-data" & x & "').find('tr:gt(1)').toggle();$('.table-credit-data" & x & "').find('.icon-toggle').toggleClass('icon-plus icon-minus');""><i class=""icon-plus icon-toggle""></i> " & Replace(formatField(Replace(node.Name, "_", " "), "T", node.Name), "'", "") & "</a></th>")
                                            .WriteLine("</tr>")
                                            For Each attr As XmlAttribute In node.Attributes
                                                .WriteLine("<tr class=""" & strClass & """ style=""display:none"">")
                                                .WriteLine("<td class=""sml"">" & Replace(formatField(Replace(attr.Name, "_", " "), "T", attr.Name), "'", "") & "</td>")
                                                .WriteLine("<td class=""smlc"">" & attr.Value & "</td>")
                                                .WriteLine("</tr>")
                                                strClass = nextClass(strClass)
                                            Next
                                            x += 1
                                        Next
                                        .WriteLine("</table>")
                                    End If
                                End If
                                Dim objLoan As XmlNodeList = objXMLDoc.SelectSingleNode("//LoanPerformance").ChildNodes
                                If (Not objLoan Is Nothing) Then
                                    .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped table-loan-perf"" width=""100%"">")
                                    .WriteLine("<tr class=""tablehead"">")
                                    .WriteLine("<th colspan=""3"" class=""sml""><a href=""javascript:return false;"" onclick=""$('.table-loan-perf').find('tr:gt(1)').toggle();$('.table-loan-perf').find('.icon-toggle').toggleClass('icon-plus icon-minus');""><i class=""icon-plus icon-toggle""></i> Loan Performance - " & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreditSearchDate")) & "</a></th>")
                                    .WriteLine("</tr>")
                                    For Each node As XmlNode In objLoan
                                        If (node.Name <> "RawData") Then
                                            .WriteLine("<tr class=""" & strClass & """ style=""display:none"">")
                                            .WriteLine("<td class=""sml"">" & Regex.Replace(node.Name, "([A-Z0-9])", " $1") & "</td>")
                                            .WriteLine("<td class=""smlc"">" & node.InnerText & "</td>")
                                            .WriteLine("</tr>")
                                            strClass = nextClass(strClass)
                                        End If
                                    Next
                                    .WriteLine("</table>")
                                End If
                        End Select
                    End If
                Next
            Else
                .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"">")
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td class=""smlc"">No history found.</td>")
                .WriteLine("</tr>")
                .WriteLine("</table>")
                .WriteLine("<br />")
            End If
            dsCache = Nothing
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getWebsiteJourney(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strClass As String = "row1"
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"">")
            Dim strSQL As String = "SELECT WebsiteLogURL, WebsiteLogDate FROM tblwebsitelogs LOG INNER JOIN tblapplications APP ON LOG.WebsiteLogIPAddress = APP.ApplicationIPAddress AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' ORDER BY WebsiteLogDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""" & strClass & """>")
                    .WriteLine("<td class=""sml"">" & Row.Item("WebsiteLogURL") & "</td>")
                    .WriteLine("<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("WebsiteLogDate")) & "</td>")
                    .WriteLine("</tr>")
                    strClass = nextClass(strClass)
                Next
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""2"" class=""smlc"">No history found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getSecuredHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">SourceID</th>")
            .WriteLine("<th class=""smlc"">Source Date</th>")
            .WriteLine("<th class=""smlc"">Broker Fee</th>")
            .WriteLine("<th class=""smlc"">Sales User</th>")
            .WriteLine("<th class=""smlc"">View Source</th>")
            .WriteLine("</tr>")
            Dim strSQL As String = "SELECT sourceID, sourceDate, BrokerFee, u.userfullname FROM tblSecuredXMLs s left outer join tblusers u on s.salesuserid = u.userid WHERE s.CompanyID = " & CompanyID & " AND AppID =" & AppID & " ORDER BY sourceID DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row.Item("sourceID") & "</td>")
                    .WriteLine("<td>" & Row.Item("sourceDate") & "</td>")
                    .WriteLine("<td>" & Row.Item("BrokerFee") & "</td>")
                    .WriteLine("<td>" & Row.Item("userfullname") & "</td>")
                    .WriteLine("<td><button class=""btn btn-secondary btn-medium"" href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','SourceHistory', '/prompts/SourceHistory.aspx?SourceID=" & Row.Item("sourceID") & "&BrokerFee=" & Row.Item("BrokerFee") & "', '1600', '800');"">View Plans</button></td>")
                    .WriteLine("</tr>")
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""2"" >No history found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getBridgingHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">QuoteID</th>")
            .WriteLine("<th class=""smlc"">Quote Date</th>")
            .WriteLine("<th class=""smlc"">View Source</th>")
            .WriteLine("</tr>")
            Dim strSQL As String = "SELECT QuoteID, QuoteDate, brokerfee FROM tblquotes WHERE CompanyID = " & CompanyID & " AND AppID =" & AppID & " ORDER BY QuoteID DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row.Item("QuoteID") & "</td>")
                    .WriteLine("<td>" & Row.Item("QuoteDate") & "</td>")
                    .WriteLine("<td><button class=""btn btn-secondary btn-medium"" href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Bridging Source History', '/prompts/SourceBridgingHistory.aspx?SourceID=" & Row.Item("QuoteID") & "&BrokerFee=" & Row.Item("brokerfee") & "&AppID=" & AppID & "', '1600', '800');"">View Plans</button></td>")
                    .WriteLine("</tr>")
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""2"" >No history found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function
	
    Public Function getSecuredLoanHistory(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">QuoteID</th>")
            .WriteLine("<th class=""smlc"">Quote Date</th>")
            .WriteLine("<th class=""smlc"">View Source</th>")
            .WriteLine("</tr>")
            Dim strSQL As String = "SELECT QuoteID, QuoteDate, brokerfee FROM tblquotes WHERE CompanyID = " & CompanyID & " AND AppID =" & AppID & " ORDER BY QuoteID DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row.Item("QuoteID") & "</td>")
                    .WriteLine("<td>" & Row.Item("QuoteDate") & "</td>")
                    .WriteLine("<td><button class=""btn btn-secondary btn-medium"" href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Secured Loan Source History', '/prompts/SourceSecuredHistory.aspx?SourceID=" & Row.Item("QuoteID") & "&BrokerFee=" & Row.Item("brokerfee") & "&AppID=" & AppID & "', '1600', '800');"">View Plans</button></td>")
                    .WriteLine("</tr>")
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""2"" >No history found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("<br />")
        End With
        Return objStringWriter.ToString
    End Function
	
    Public Function getCreditHistoryApp1(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim App1IVADateReg1 As String = ""
            Dim App1IVASat1 As String = ""
            Dim App1IVADateCom1 As String = ""
            Dim App1IVADateReg2 As String = ""
            Dim App1IVASat2 As String = ""
            Dim App1IVADateCom2 As String = ""
            Dim App1IVADateReg3 As String = ""
            Dim App1IVASat3 As String = ""
            Dim App1IVADateCom3 As String = ""
            Dim App1IVADateReg4 As String = ""
            Dim App1IVASat4 As String = ""
            Dim App1IVADateCom4 As String = ""
            Dim App1IVADateReg5 As String = ""
            Dim App1IVASat5 As String = ""
            Dim App1IVADateCom5 As String = ""

            Dim App1ArrearsDateRegistered1 As String = ""
            Dim App1ArrearsCleared1 As String = ""
            Dim App1ArrearsDateRegistered2 As String = ""
            Dim App1ArrearsCleared2 As String = ""
            Dim App1ArrearsDateRegistered3 As String = ""
            Dim App1ArrearsCleared3 As String = ""
            Dim App1ArrearsDateRegistered4 As String = ""
            Dim App1ArrearsCleared4 As String = ""
            Dim App1ArrearsDateRegistered5 As String = ""
            Dim App1ArrearsCleared5 As String = ""

            Dim App1CountyDateRegistered1 As String = ""
            Dim App1CountyAmount1 As String = ""
            Dim App1CountyDateSat1 As String = ""
            Dim App1CountyDateRegistered2 As String = ""
            Dim App1CountyAmount2 As String = ""
            Dim App1CountyDateSat2 As String = ""
            Dim App1CountyDateRegistered3 As String = ""
            Dim App1CountyAmount3 As String = ""
            Dim App1CountyDateSat3 As String = ""
            Dim App1CountyDateRegistered4 As String = ""
            Dim App1CountyAmount4 As String = ""
            Dim App1CountyDateSat4 As String = ""
            Dim App1CountyDateRegistered5 As String = ""
            Dim App1CountyAmount5 As String = ""
            Dim App1CountyDateSat5 As String = ""

            Dim App1DefaultDateRegistered1 As String = ""
            Dim App1DefaultAmount1 As String = ""
            Dim App1DefaultDateSat1 As String = ""
            Dim App1DefaultDateRegistered2 As String = ""
            Dim App1DefaultAmount2 As String = ""
            Dim App1DefaultDateSat2 As String = ""
            Dim App1DefaultDateRegistered3 As String = ""
            Dim App1DefaultAmount3 As String = ""
            Dim App1DefaultDateSat3 As String = ""
            Dim App1DefaultDateRegistered4 As String = ""
            Dim App1DefaultAmount4 As String = ""
            Dim App1DefaultDateSat4 As String = ""
            Dim App1DefaultDateRegistered5 As String = ""
            Dim App1DefaultAmount5 As String = ""
            Dim App1DefaultDateSat5 As String = ""

            Dim App1PayDayLoanRegistered1 As String = ""
            Dim App1PayDayLoanRegistered2 As String = ""
            Dim App1PayDayLoanRegistered3 As String = ""
            Dim App1PayDayLoanRegistered4 As String = ""
            Dim App1PayDayLoanRegistered5 As String = ""








            Dim strSQL As String = "Select Top(1) * FROM vw27tecadverse where AppId = " & AppID & ""
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    App1IVADateReg1 = Row.Item("IVADateReg1").ToString()
                    App1IVASat1 = Row.Item("IVASat1").ToString()
                    App1IVADateCom1 = Row.Item("IVADateCom1").ToString()
                    App1IVADateReg2 = Row.Item("IVADateReg2").ToString()
                    App1IVASat2 = Row.Item("IVASat2").ToString()
                    App1IVADateCom2 = Row.Item("IVADateCom2").ToString()
                    App1IVADateReg3 = Row.Item("IVADateReg3").ToString()
                    App1IVASat3 = Row.Item("IVASat3").ToString()
                    App1IVADateCom3 = Row.Item("IVADateCom3").ToString()
                    App1IVADateReg4 = Row.Item("IVADateReg4").ToString()
                    App1IVASat4 = Row.Item("IVASat4").ToString()
                    App1IVADateCom4 = Row.Item("IVADateCom4").ToString()
                    App1IVADateReg5 = Row.Item("IVADateReg5").ToString()
                    App1IVASat5 = Row.Item("IVASat5").ToString()
                    App1IVADateCom5 = Row.Item("IVADateCom5").ToString()

                    App1ArrearsDateRegistered1 = Row.Item("ArrearsDateReg1").ToString()
                    App1ArrearsCleared1 = Row.Item("ArrearsDateSat1").ToString()
                    App1ArrearsDateRegistered2 = Row.Item("ArrearsDateReg2").ToString()
                    App1ArrearsCleared2 = Row.Item("ArrearsDateSat2").ToString()
                    App1ArrearsDateRegistered3 = Row.Item("ArrearsDateReg3").ToString()
                    App1ArrearsCleared3 = Row.Item("ArrearsDateSat3").ToString()
                    App1ArrearsDateRegistered4 = Row.Item("ArrearsDateReg4").ToString()
                    App1ArrearsCleared4 = Row.Item("ArrearsDateSat4").ToString()
                    App1ArrearsDateRegistered5 = Row.Item("ArrearsDateReg5").ToString()
                    App1ArrearsCleared5 = Row.Item("ArrearsDateSat5").ToString()

                    App1CountyDateRegistered1 = Row.Item("CountyCourtDateReg1").ToString()
                    App1CountyAmount1 = Row.Item("CountyCourtAmount1").ToString()
                    App1CountyDateSat1 = Row.Item("CountyCourtDateSat1").ToString()
                    App1CountyDateRegistered2 = Row.Item("CountyCourtDateReg2").ToString()
                    App1CountyAmount2 = Row.Item("CountyCourtAmount2").ToString()
                    App1CountyDateSat2 = Row.Item("CountyCourtDateSat2").ToString()
                    App1CountyDateRegistered3 = Row.Item("CountyCourtDateReg3").ToString()
                    App1CountyAmount3 = Row.Item("CountyCourtAmount3").ToString()
                    App1CountyDateSat3 = Row.Item("CountyCourtDateSat3").ToString()
                    App1CountyDateRegistered4 = Row.Item("CountyCourtDateReg4").ToString()
                    App1CountyAmount4 = Row.Item("CountyCourtAmount4").ToString()
                    App1CountyDateSat4 = Row.Item("CountyCourtDateSat4").ToString()
                    App1CountyDateRegistered5 = Row.Item("CountyCourtDateReg5").ToString()
                    App1CountyAmount5 = Row.Item("CountyCourtAmount5").ToString()
                    App1CountyDateSat5 = Row.Item("CountyCourtDateSat5").ToString()

                    App1DefaultDateRegistered1 = Row.Item("DefaultDateReg1").ToString()
                    App1DefaultAmount1 = Row.Item("DefaultAmount1").ToString()
                    App1DefaultDateSat1 = Row.Item("DefaultDateSat1").ToString()
                    App1DefaultDateRegistered2 = Row.Item("DefaultDateReg2").ToString()
                    App1DefaultAmount2 = Row.Item("DefaultAmount2").ToString()
                    App1DefaultDateSat2 = Row.Item("DefaultDateSat2").ToString()
                    App1DefaultDateRegistered3 = Row.Item("DefaultDateReg3").ToString()
                    App1DefaultAmount3 = Row.Item("DefaultAmount3").ToString()
                    App1DefaultDateSat3 = Row.Item("DefaultDateSat3").ToString()
                    App1DefaultDateRegistered4 = Row.Item("DefaultDateReg4").ToString()
                    App1DefaultAmount4 = Row.Item("DefaultAmount4").ToString()
                    App1DefaultDateSat4 = Row.Item("DefaultDateSat4").ToString()
                    App1DefaultDateRegistered5 = Row.Item("DefaultDateReg5").ToString()
                    App1DefaultAmount5 = Row.Item("DefaultAmount5").ToString()
                    App1DefaultDateSat5 = Row.Item("DefaultDateSat5").ToString()

                    App1PayDayLoanRegistered1 = Row.Item("PayDayDateReg1").ToString()
                    App1PayDayLoanRegistered2 = Row.Item("PayDayDateReg2").ToString()
                    App1PayDayLoanRegistered3 = Row.Item("PayDayDateReg3").ToString()
                    App1PayDayLoanRegistered4 = Row.Item("PayDayDateReg4").ToString()
                    App1PayDayLoanRegistered5 = Row.Item("PayDayDateReg5").ToString()







                Next
            End If

            'Involuntary Arrangements (IVA)
            .WriteLine("<label>Involuntary Arrangements (IVA)</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">IVA Date Registered</th>")
            .WriteLine("<th class=""smlc"">Satisfactory Conduct</th>")
            .WriteLine("<th class=""smlc"">IVA Date Completed</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""IVADateReg1"" name=""IVADateReg1"" value=""" & App1IVADateReg1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""IVASat1"" name=""IVASat1"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""IVADateCom1"" name=""IVADateCom1"" value=""" & App1IVADateCom1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""IVADateReg2"" name=""IVADateReg2"" value=""" & App1IVADateReg2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""IVASat2"" name=""IVASat2"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""IVADateCom2"" name=""IVADateCom2"" value=""" & App1IVADateCom2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""IVADateReg3"" name=""IVADateReg3"" value=""" & App1IVADateReg3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""IVASat3"" name=""IVASat3"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""IVADateCom3"" name=""IVADateCom3"" value=""" & App1IVADateCom3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""IVADateReg4"" name=""IVADateReg4"" value=""" & App1IVADateReg4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""IVASat4"" name=""IVASat4"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""IVADateCom4"" name=""IVADateCom4"" value=""" & App1IVADateCom4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""IVADateReg5"" name=""IVADateReg5"" value=""" & App1IVADateReg5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""IVASat5"" name=""IVASat5"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""IVADateCom5"" name=""IVADateCom5"" value=""" & App1IVADateCom5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Arrears
            .WriteLine("<label>Arrears</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Arrears Date Registered</th>")
            .WriteLine("<th class=""smlc"">Arrears Cleared</th>")
            .WriteLine("<th class=""smlc"">Arrears Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""ArrearsDateReg1"" name=""ArrearsDateReg1"" value=""" & App1ArrearsDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""ArrearsCleared1"" name=""ArrearsCleared1"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""ArrearsDateSat1"" name=""ArrearsDateSat1"" value=""" & App1ArrearsCleared1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""ArrearsDateReg2"" name=""ArrearsDateReg2"" value=""" & App1ArrearsDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""ArrearsCleared2"" name=""ArrearsCleared2"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""ArrearsDateSat2"" name=""ArrearsDateSat2"" value=""" & App1ArrearsCleared2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""ArrearsDateReg3"" name=""ArrearsDateReg3"" value=""" & App1ArrearsDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""ArrearsCleared3"" name=""ArrearsCleared3"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""ArrearsDateSat3"" name=""ArrearsDateSat3"" value=""" & App1ArrearsCleared3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""ArrearsDateReg4"" name=""ArrearsDateReg4"" value=""" & App1ArrearsDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""ArrearsCleared4"" name=""ArrearsCleared4"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""ArrearsDateSat4"" name=""ArrearsDateSat4"" value=""" & App1ArrearsCleared4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""ArrearsDateReg5"" name=""ArrearsDateReg5"" value=""" & App1ArrearsDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""ArrearsCleared5"" name=""ArrearsCleared5"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""ArrearsDateSat5"" name=""ArrearsDateSat5"" value=""" & App1ArrearsCleared5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'County Court Judgments
            .WriteLine("<label>County Court Judgments</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">County Court Date Registered</th>")
            .WriteLine("<th class=""smlc"">County Court Amount</th>")
            .WriteLine("<th class=""smlc"">County Court Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""CountyCourtDateReg1"" name=""CountyCourtDateReg1"" value=""" & App1CountyDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtAmount1"" name=""CountyCourtAmount1"" value=""" & App1CountyAmount1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtDateSat1"" name=""CountyCourtDateSat1"" value=""" & App1CountyDateSat1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""CountyCourtDateReg2"" name=""CountyCourtDateReg2"" value=""" & App1CountyDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtAmount2"" name=""CountyCourtAmount2"" value=""" & App1CountyAmount2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtDateSat2"" name=""CountyCourtDateSat2"" value=""" & App1CountyDateSat2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""CountyCourtDateReg3"" name=""CountyCourtDateReg3"" value=""" & App1CountyDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtAmount3"" name=""CountyCourtAmount3"" value=""" & App1CountyAmount3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtDateSat3"" name=""CountyCourtDateSat3"" value=""" & App1CountyDateSat3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""CountyCourtDateReg4"" name=""CountyCourtDateReg4"" value=""" & App1CountyDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtAmount4"" name=""CountyCourtAmount4"" value=""" & App1CountyAmount4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtDateSat4"" name=""CountyCourtDateSat4"" value=""" & App1CountyDateSat4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""CountyCourtDateReg5"" name=""CountyCourtDateReg5"" value=""" & App1CountyDateRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtAmount5"" name=""CountyCourtAmount5"" value=""" & App1CountyAmount5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""CountyCourtDateSat5"" name=""CountyCourtDateSat5"" value=""" & App1CountyDateSat5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Defaults
            .WriteLine("<label>Defaults</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Default Date Registered</th>")
            .WriteLine("<th class=""smlc"">Default Amount</th>")
            .WriteLine("<th class=""smlc"">Default Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""DefaultDateReg1"" name=""DefaultDateReg1"" value=""" & App1DefaultDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultAmount1"" name=""DefaultAmount1"" value=""" & App1DefaultAmount1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultDateSat1"" name=""DefaultDateSat1"" value=""" & App1DefaultDateSat1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""DefaultDateReg2"" name=""DefaultDateReg2"" value=""" & App1DefaultDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultAmount2"" name=""DefaultAmount2"" value=""" & App1DefaultAmount2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultDateSat2"" name=""DefaultDateSat2"" value=""" & App1DefaultDateSat2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""DefaultDateReg3"" name=""DefaultDateReg3"" value=""" & App1DefaultDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultAmount3"" name=""DefaultAmount3"" value=""" & App1DefaultAmount3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultDateSat3"" name=""DefaultDateSat3"" value=""" & App1DefaultDateSat3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""DefaultDateReg4"" name=""DefaultDateReg4"" value=""" & App1DefaultDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultAmount4"" name=""DefaultAmount4"" value=""" & App1DefaultAmount4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultDateSat4"" name=""DefaultDateSat4"" value=""" & App1DefaultDateSat4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""DefaultDateReg5"" name=""DefaultDateReg5"" value=""" & App1DefaultDateRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultAmount5"" name=""DefaultAmount5"" value=""" & App1DefaultAmount5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""DefaultDateSat5"" name=""DefaultDateSat5"" value=""" & App1DefaultDateSat5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Pay Day Loans
            .WriteLine("<label>Pay Day Loans</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Pay Day Loans Date Registered</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""PayDayDateReg1"" name=""PayDayDateReg1"" value=""" & App1PayDayLoanRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""PayDayDateReg2"" name=""PayDayDateReg2"" value=""" & App1PayDayLoanRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""PayDayDateReg3"" name=""PayDayDateReg3"" value=""" & App1PayDayLoanRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""PayDayDateReg4"" name=""PayDayDateReg4"" value=""" & App1PayDayLoanRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""PayDayDateReg5"" name=""PayDayDateReg5"" value=""" & App1PayDayLoanRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getCreditHistoryApp2(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim App2IVADateReg1 As String = ""
            Dim App2IVASat1 As String = ""
            Dim App2IVADateCom1 As String = ""
            Dim App2IVADateReg2 As String = ""
            Dim App2IVASat2 As String = ""
            Dim App2IVADateCom2 As String = ""
            Dim App2IVADateReg3 As String = ""
            Dim App2IVASat3 As String = ""
            Dim App2IVADateCom3 As String = ""
            Dim App2IVADateReg4 As String = ""
            Dim App2IVASat4 As String = ""
            Dim App2IVADateCom4 As String = ""
            Dim App2IVADateReg5 As String = ""
            Dim App2IVASat5 As String = ""
            Dim App2IVADateCom5 As String = ""

            Dim App2ArrearsDateRegistered1 As String = ""
            Dim App2ArrearsCleared1 As String = ""
            Dim App2ArrearsDateRegistered2 As String = ""
            Dim App2ArrearsCleared2 As String = ""
            Dim App2ArrearsDateRegistered3 As String = ""
            Dim App2ArrearsCleared3 As String = ""
            Dim App2ArrearsDateRegistered4 As String = ""
            Dim App2ArrearsCleared4 As String = ""
            Dim App2ArrearsDateRegistered5 As String = ""
            Dim App2ArrearsCleared5 As String = ""

            Dim App2CountyDateRegistered1 As String = ""
            Dim App2CountyAmount1 As String = ""
            Dim App2CountyDateSat1 As String = ""
            Dim App2CountyDateRegistered2 As String = ""
            Dim App2CountyAmount2 As String = ""
            Dim App2CountyDateSat2 As String = ""
            Dim App2CountyDateRegistered3 As String = ""
            Dim App2CountyAmount3 As String = ""
            Dim App2CountyDateSat3 As String = ""
            Dim App2CountyDateRegistered4 As String = ""
            Dim App2CountyAmount4 As String = ""
            Dim App2CountyDateSat4 As String = ""
            Dim App2CountyDateRegistered5 As String = ""
            Dim App2CountyAmount5 As String = ""
            Dim App2CountyDateSat5 As String = ""


            Dim App2DefaultDateRegistered1 As String = ""
            Dim App2DefaultAmount1 As String = ""
            Dim App2DefaultDateSat1 As String = ""
            Dim App2DefaultDateRegistered2 As String = ""
            Dim App2DefaultAmount2 As String = ""
            Dim App2DefaultDateSat2 As String = ""
            Dim App2DefaultDateRegistered3 As String = ""
            Dim App2DefaultAmount3 As String = ""
            Dim App2DefaultDateSat3 As String = ""
            Dim App2DefaultDateRegistered4 As String = ""
            Dim App2DefaultAmount4 As String = ""
            Dim App2DefaultDateSat4 As String = ""
            Dim App2DefaultDateRegistered5 As String = ""
            Dim App2DefaultAmount5 As String = ""
            Dim App2DefaultDateSat5 As String = ""

            Dim App2PayDayLoanRegistered1 As String = ""
            Dim App2PayDayLoanRegistered2 As String = ""
            Dim App2PayDayLoanRegistered3 As String = ""
            Dim App2PayDayLoanRegistered4 As String = ""
            Dim App2PayDayLoanRegistered5 As String = ""

            Dim strSQL As String = "Select Top(1) * FROM vw27tecadverse where AppId = " & AppID & ""
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    App2IVADateReg1 = Row.Item("App2IVADateReg1").ToString()
                    App2IVASat1 = Row.Item("App2IVASat1").ToString()
                    App2IVADateCom1 = Row.Item("App2IVADateCom1").ToString()
                    App2IVADateReg2 = Row.Item("App2IVADateReg2").ToString()
                    App2IVASat2 = Row.Item("App2IVASat2").ToString()
                    App2IVADateCom2 = Row.Item("App2IVADateCom2").ToString()
                    App2IVADateReg3 = Row.Item("App2IVADateReg3").ToString()
                    App2IVASat3 = Row.Item("App2IVASat3").ToString()
                    App2IVADateCom3 = Row.Item("App2IVADateCom3").ToString()
                    App2IVADateReg4 = Row.Item("App2IVADateReg4").ToString()
                    App2IVASat4 = Row.Item("App2IVASat4").ToString()
                    App2IVADateCom4 = Row.Item("App2IVADateCom4").ToString()
                    App2IVADateReg5 = Row.Item("App2IVADateReg5").ToString()
                    App2IVASat5 = Row.Item("App2IVASat5").ToString()
                    App2IVADateCom5 = Row.Item("App2IVADateCom5").ToString()

                    App2ArrearsDateRegistered1 = Row.Item("App2ArrearsDateReg1").ToString()
                    App2ArrearsCleared1 = Row.Item("App2ArrearsDateSat1").ToString()
                    App2ArrearsDateRegistered2 = Row.Item("App2ArrearsDateReg2").ToString()
                    App2ArrearsCleared2 = Row.Item("App2ArrearsDateSat2").ToString()
                    App2ArrearsDateRegistered3 = Row.Item("App2ArrearsDateReg3").ToString()
                    App2ArrearsCleared3 = Row.Item("App2ArrearsDateSat3").ToString()
                    App2ArrearsDateRegistered4 = Row.Item("App2ArrearsDateReg4").ToString()
                    App2ArrearsCleared4 = Row.Item("App2ArrearsDateSat4").ToString()
                    App2ArrearsDateRegistered5 = Row.Item("App2ArrearsDateReg5").ToString()
                    App2ArrearsCleared5 = Row.Item("App2ArrearsDateSat5").ToString()

                    App2CountyDateRegistered1 = Row.Item("App2CountyCourtDateReg1").ToString()
                    App2CountyAmount1 = Row.Item("App2CountyCourtAmount1").ToString()
                    App2CountyDateSat1 = Row.Item("App2CountyCourtDateSat1").ToString()
                    App2CountyDateRegistered2 = Row.Item("App2CountyCourtDateReg2").ToString()
                    App2CountyAmount2 = Row.Item("App2CountyCourtAmount2").ToString()
                    App2CountyDateSat2 = Row.Item("App2CountyCourtDateSat2").ToString()
                    App2CountyDateRegistered3 = Row.Item("App2CountyCourtDateReg3").ToString()
                    App2CountyAmount3 = Row.Item("App2CountyCourtAmount3").ToString()
                    App2CountyDateSat3 = Row.Item("App2CountyCourtDateSat3").ToString()
                    App2CountyDateRegistered4 = Row.Item("App2CountyCourtDateReg4").ToString()
                    App2CountyAmount4 = Row.Item("App2CountyCourtAmount4").ToString()
                    App2CountyDateSat4 = Row.Item("App2CountyCourtDateSat4").ToString()
                    App2CountyDateRegistered5 = Row.Item("App2CountyCourtDateReg5").ToString()
                    App2CountyAmount5 = Row.Item("App2CountyCourtAmount5").ToString()
                    App2CountyDateSat5 = Row.Item("App2CountyCourtDateSat5").ToString()

                    App2DefaultDateRegistered1 = Row.Item("App2DefaultDateReg1").ToString()
                    App2DefaultAmount1 = Row.Item("App2DefaultAmount1").ToString()
                    App2DefaultDateSat1 = Row.Item("App2DefaultDateSat1").ToString()
                    App2DefaultDateRegistered2 = Row.Item("App2DefaultDateReg2").ToString()
                    App2DefaultAmount2 = Row.Item("App2DefaultAmount2").ToString()
                    App2DefaultDateSat2 = Row.Item("App2DefaultDateSat2").ToString()
                    App2DefaultDateRegistered3 = Row.Item("App2DefaultDateReg3").ToString()
                    App2DefaultAmount3 = Row.Item("App2DefaultAmount3").ToString()
                    App2DefaultDateSat3 = Row.Item("App2DefaultDateSat3").ToString()
                    App2DefaultDateRegistered4 = Row.Item("App2DefaultDateReg4").ToString()
                    App2DefaultAmount4 = Row.Item("App2DefaultAmount4").ToString()
                    App2DefaultDateSat4 = Row.Item("App2DefaultDateSat4").ToString()
                    App2DefaultDateRegistered5 = Row.Item("App2DefaultDateReg5").ToString()
                    App2DefaultAmount5 = Row.Item("App2DefaultAmount5").ToString()
                    App2DefaultDateSat5 = Row.Item("App2DefaultDateSat5").ToString()

                    App2PayDayLoanRegistered1 = Row.Item("App2PayDayDateReg1").ToString()
                    App2PayDayLoanRegistered2 = Row.Item("App2PayDayDateReg2").ToString()
                    App2PayDayLoanRegistered3 = Row.Item("App2PayDayDateReg3").ToString()
                    App2PayDayLoanRegistered4 = Row.Item("App2PayDayDateReg4").ToString()
                    App2PayDayLoanRegistered5 = Row.Item("App2PayDayDateReg5").ToString()



                Next
            End If
            'Involuntary Arrangements (IVA)
            .WriteLine("<label>Involuntary Arrangements (IVA)</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">IVA Date Registered</th>")
            .WriteLine("<th class=""smlc"">Satisfactory Conduct</th>")
            .WriteLine("<th class=""smlc"">IVA Date Completed</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2IVADateReg1"" name=""App2IVADateReg1"" value=""" & App2IVADateReg1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2IVASat1"" name=""App2IVASat1"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2IVADateCom1"" name=""App2IVADateCom1"" value=""" & App2IVADateCom1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2IVADateReg2"" name=""App2IVADateReg2"" value=""" & App2IVADateReg2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2IVASat2"" name=""App2IVASat2"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2IVADateCom2"" name=""App2IVADateCom2"" value=""" & App2IVADateCom2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2IVADateReg3"" name=""App2IVADateReg3"" value=""" & App2IVADateReg3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2IVASat3"" name=""App2IVASat3"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2IVADateCom3"" name=""App2IVADateCom3"" value=""" & App2IVADateCom3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2IVADateReg4"" name=""App2IVADateReg4"" value=""" & App2IVADateReg4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2IVASat4"" name=""App2IVASat4"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2IVADateCom4"" name=""App2IVADateCom4"" value=""" & App2IVADateCom4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2IVADateReg5"" name=""App2IVADateReg5"" value=""" & App2IVADateReg5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2IVASat5"" name=""App2IVASat5"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Satisfactory Conduct"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2IVADateCom5"" name=""App2IVADateCom5"" value=""" & App2IVADateCom5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Arrears
            .WriteLine("<label>Arrears</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Arrears Date Registered</th>")
            .WriteLine("<th class=""smlc"">Arrears Cleared</th>")
            .WriteLine("<th class=""smlc"">Arrears Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2ArrearsDateReg1"" name=""App2ArrearsDateReg1"" value=""" & App2ArrearsDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2ArrearsCleared1"" name=""App2ArrearsCleared1"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2ArrearsDateSat1"" name=""App2ArrearsDateSat1"" value=""" & App2ArrearsCleared1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2ArrearsDateReg2"" name=""App2ArrearsDateReg2"" value=""" & App2ArrearsDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2ArrearsCleared2"" name=""App2ArrearsCleared2"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2ArrearsDateSat2"" name=""App2ArrearsDateSat2"" value=""" & App2ArrearsCleared2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2ArrearsDateReg3"" name=""App2ArrearsDateReg3"" value=""" & App2ArrearsDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2ArrearsCleared3"" name=""App2ArrearsCleared3"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2ArrearsDateSat3"" name=""App2ArrearsDateSat3"" value=""" & App2ArrearsCleared3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2ArrearsDateReg4"" name=""App2ArrearsDateReg4"" value=""" & App2ArrearsDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2ArrearsCleared4"" name=""App2ArrearsCleared4"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2ArrearsDateSat4"" name=""App2ArrearsDateSat4"" value=""" & App2ArrearsCleared4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2ArrearsDateReg5"" name=""App2ArrearsDateReg5"" value=""" & App2ArrearsDateRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><select id=""App2ArrearsCleared5"" name=""App2ArrearsCleared5"" onchange="""" onblur=""saveField('" & AppID & "','11521',this);"" class=""text select-small"" onmouseout="""" title=""Arrears Cleared"" data-placement=""right""><option value = """" > --Y/N--</Option><option value = ""Y"" > Yes</Option><option value = ""N"" > No</Option></select></td>")
            .WriteLine("<td><input id=""App2ArrearsDateSat5"" name=""App2ArrearsDateSat5"" value=""" & App2ArrearsCleared5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'County Court Judgments
            .WriteLine("<label>County Court Judgments</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">County Court Date Registered</th>")
            .WriteLine("<th class=""smlc"">County Court Amount</th>")
            .WriteLine("<th class=""smlc"">County Court Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2CountyCourtDateReg1"" name=""App2CountyCourtDateReg1"" value=""" & App2CountyDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtAmount1"" name=""App2CountyCourtAmount1"" value=""" & App2CountyAmount1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtDateSat1"" name=""App2CountyCourtDateSat1"" value=""" & App2CountyDateSat1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2CountyCourtDateReg2"" name=""App2CountyCourtDateReg2"" value=""" & App2CountyDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtAmount2"" name=""App2CountyCourtAmount2"" value=""" & App2CountyAmount2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtDateSat2"" name=""App2CountyCourtDateSat2"" value=""" & App2CountyDateSat2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2CountyCourtDateReg3"" name=""App2CountyCourtDateReg3"" value=""" & App2CountyDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtAmount3"" name=""App2CountyCourtAmount3"" value=""" & App2CountyAmount3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtDateSat3"" name=""App2CountyCourtDateSat3"" value=""" & App2CountyDateSat3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2CountyCourtDateReg4"" name=""App2CountyCourtDateReg4"" value=""" & App2CountyDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtAmount4"" name=""App2CountyCourtAmount4"" value=""" & App2CountyAmount4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtDateSat4"" name=""App2CountyCourtDateSat4"" value=""" & App2CountyDateSat4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2CountyCourtDateReg5"" name=""App2CountyCourtDateReg5"" value=""" & App2CountyDateRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtAmount5"" name=""App2CountyCourtAmount5"" value=""" & App2CountyAmount5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2CountyCourtDateSat5"" name=""App2CountyCourtDateSat5"" value=""" & App2CountyDateSat5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Defaults
            .WriteLine("<label>Defaults</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Default Date Registered</th>")
            .WriteLine("<th class=""smlc"">Default Amount</th>")
            .WriteLine("<th class=""smlc"">Default Date Satisfied</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2DefaultDateReg1"" name=""App2DefaultDateReg1"" value=""" & App2DefaultDateRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultAmount1"" name=""App2DefaultAmount1"" value=""" & App2DefaultAmount1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultDateSat1"" name=""App2DefaultDateSat1"" value=""" & App2DefaultDateSat1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2DefaultDateReg2"" name=""App2DefaultDateReg2"" value=""" & App2DefaultDateRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultAmount2"" name=""App2DefaultAmount2"" value=""" & App2DefaultAmount2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultDateSat2"" name=""App2DefaultDateSat2"" value=""" & App2DefaultDateSat2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2DefaultDateReg3"" name=""App2DefaultDateReg3"" value=""" & App2DefaultDateRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultAmount3"" name=""App2DefaultAmount3"" value=""" & App2DefaultAmount3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultDateSat3"" name=""App2DefaultDateSat3"" value=""" & App2DefaultDateSat3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2DefaultDateReg4"" name=""App2DefaultDateReg4"" value=""" & App2DefaultDateRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultAmount4"" name=""App2DefaultAmount4"" value=""" & App2DefaultAmount4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultDateSat4"" name=""App2DefaultDateSat4"" value=""" & App2DefaultDateSat4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2DefaultDateReg5"" name=""App2DefaultDateReg5"" value=""" & App2DefaultDateRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultAmount5"" name=""App2DefaultAmount5"" value=""" & App2DefaultAmount5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("<td><input id=""App2DefaultDateSat5"" name=""App2DefaultDateSat5"" value=""" & App2DefaultDateSat5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
            'Pay Day Loans
            .WriteLine("<label>Pay Day Loans</label>")
            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"">")
            .WriteLine("<tr class=""tablehead"">")
            .WriteLine("<th class=""smlc"">Pay Day Loans Date Registered</th>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2PayDayDateReg1"" name=""App2PayDayDateReg1"" value=""" & App2PayDayLoanRegistered1 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2PayDayDateReg2"" name=""App2PayDayDateReg2"" value=""" & App2PayDayLoanRegistered2 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2PayDayDateReg3"" name=""App2PayDayDateReg3"" value=""" & App2PayDayLoanRegistered3 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2PayDayDateReg4"" name=""App2PayDayDateReg4"" value=""" & App2PayDayLoanRegistered4 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("<tr>")
            .WriteLine("<td><input id=""App2PayDayDateReg5"" name=""App2PayDayDateReg5"" value=""" & App2PayDayLoanRegistered5 & """ class=""text input-xlarge ui-tooltip"" onblur=""saveField('" & AppID & "','11521',this);"" onmouseout="" data-placement=""right"" type=""text""></td>")
            .WriteLine("</tr>")
            .WriteLine("</table>")
        End With
        Return objStringWriter.ToString
    End Function


End Class