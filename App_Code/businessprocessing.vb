﻿Imports Config, Common, CallInterface
Imports Microsoft.VisualBasic

Public Class BusinessProcessing

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Public BusinessObjectID As String = HttpContext.Current.Request("BusinessObjectID"), strBusinessTelephone As String = "", strMobileTelephone As String = "", strStatusDescription As String = "", strSubStatusDescription As String = "", strStatusCode As String = "", strSubStatusCode As String = ""
    Public strCallTelephoneNumber As String = HttpContext.Current.Request("TelephoneNumber"), DiaryID As String = HttpContext.Current.Request("DiaryID")
    Private strAction As String = HttpContext.Current.Request("Action")
    Public intWorkflowID As String = "", intWorkflowTypeID As String = "", intWorkflowAccessLevel As String = "", intWorkflowActivityID As String = "", intCallTime As String = ""
    Public strLockUserName As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property CaseInformation() As String
        Get
            Dim strCaseInformation As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "BusinessCaseInfo")
            If (Not checkValue(strCaseInformation)) Then
                strCaseInformation = "<span class=""text-medium""><strong>{BusinessObjectProductType} ({BusinessObjectType})</strong></span> - {BusinessObjectBusinessName}, {BusinessObjectClientAddressPostCode}"
            End If
            Return replaceBusinessTags(strCaseInformation, BusinessObjectID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property BusinessTelephone() As String
        Get
            Return strBusinessTelephone
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property MobileTelephone() As String
        Get
            Return strMobileTelephone
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property Status() As String
        Get
            Return strStatusDescription
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SubStatus() As String
        Get
            Return strSubStatusDescription
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowID() As String
        Get
            Return intWorkflowID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowTypeID() As String
        Get
            Return intWorkflowTypeID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowAccessLevel() As String
        Get
            Return intWorkflowAccessLevel
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowActivityID() As String
        Get
            Return intWorkflowActivityID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialer() As Boolean
        Get
            Return getAnyFieldByCompanyID("WorkflowDialer", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property WorkflowDialerIdleTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerIdleTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialerWrapTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerWrapTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialerDelayTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerDelayTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property CallTime() As String
        Get
            Return intCallTime
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property StatusCode() As String
        Get
            Return strStatusCode
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SubStatusCode() As String
        Get
            Return strSubStatusCode
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Sub initialise()
        Dim dteCallStartTime As String = "", dtePreCallStartTime As String = ""
        If (checkKickOut(Config.DefaultUserID, True)) Then
            HttpContext.Current.Response.Redirect("/workflow/businessworkflow.aspx?intWorkflowTypeID=" & intWorkflowTypeID)
        End If
        If (strAction = ActivityType.EndCall) Then
            'Call executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)
        End If
        callLogging(BusinessObjectID, strAction, strCallTelephoneNumber, Config.DefaultUserID)

        Dim strSQL As String = "SELECT UserActiveWorkflowTelephoneNumber, UserActiveWorkflowTypeID, UserActiveWorkflowTypeUserLevel, UserActiveWorkflowID, UserActiveWorkflowActivityID, UserActiveWorkflowStartTime, UserPreCallStartTime FROM tblusers WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                intWorkflowTypeID = Row.Item("UserActiveWorkflowTypeID").ToString
                intWorkflowAccessLevel = Row.Item("UserActiveWorkflowTypeUserLevel").ToString
                intWorkflowID = Row.Item("UserActiveWorkflowID").ToString
                intWorkflowActivityID = Row.Item("UserActiveWorkflowActivityID")
                dteCallStartTime = Row.Item("UserActiveWorkflowStartTime").ToString
                dtePreCallStartTime = Row.Item("UserPreCallStartTime").ToString
            Next
        End If
        dsCache = Nothing

        If (checkValue(dtePreCallStartTime)) Then dteCallStartTime = dtePreCallStartTime
        If (Not checkValue(dteCallStartTime)) Then dteCallStartTime = Config.DefaultDateTime
        intCallTime = DateDiff("s", CDate(dteCallStartTime), Config.DefaultDateTime) * 1000

        strSQL = "SELECT BusinessObjectContactBusinessTelephone, BusinessObjectContactMobileTelephone, BusinessObjectStatusCode, BusinessObjectSubStatusCode, StatusDescription, SubStatusDescription FROM vwbusinessobjects WHERE BusinessObjectID = " & BusinessObjectID & " AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(CacheObject(), strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strBusinessTelephone = Row.Item("BusinessObjectContactBusinessTelephone").ToString
                strMobileTelephone = Row.Item("BusinessObjectContactMobileTelephone").ToString
                strStatusDescription = Row.Item("StatusDescription").ToString
                strSubStatusDescription = Row.Item("SubStatusDescription").ToString
                strStatusCode = Row.Item("BusinessObjectStatusCode").ToString
                strSubStatusCode = Row.Item("BusinessObjectSubStatusCode").ToString
            Next
        End If
        dsCache = Nothing

        If (checkValue(DiaryID)) Then
            strSQL = "UPDATE tblusers SET UserActiveWorkflowDiaryID = '" & DiaryID & "' WHERE UserID = " & Config.DefaultUserID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        End If

        strLockUserName = checkBusinessCaseLock(Config.DefaultUserID, BusinessObjectID)
        'unlockCases(Config.DefaultUserID, AppID)
        lockBusinessCase(Config.DefaultUserID, BusinessObjectID)
    End Sub

    Public Sub getProcessingScreen()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT ProcessingScreenTemplateApplicationTemplateID, ProcessingScreenTemplateCSSClass, ProcessingScreenTemplateDisplay, ProcessingScreenTemplateColumn, ApplicationTemplateName, ApplicationTemplateIcon, ProcessingScreenTemplateHeight FROM vwbusinessprocessingscreen WHERE BusinessObjectID = '" & BusinessObjectID & "'"
            'If (Not objLeadPlatform.Config.SuperAdmin) Then
            Dim strPrefix As String = " AND ", strSuffix As String = ""
            If (objLeadPlatform.Config.UserActiveWorkflowTypeUserLevel > 0) Then
                strSQL += " AND (ProcessingScreenUserLevel LIKE N'%|" & objLeadPlatform.Config.UserActiveWorkflowTypeUserLevel & "|%')"
            ElseIf (objLeadPlatform.Config.UserActiveLevelID > 0) Then
                strSQL += " AND (ProcessingScreenUserLevel LIKE N'%|" & objLeadPlatform.Config.UserActiveLevelID & "|%')"
            Else
                strSQL += " AND (ProcessingScreenUserLevel = N'||')"
            End If
            strSQL += " ORDER BY ProcessingScreenTemplateOrder"
			
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row As DataRow In dsCache.Rows

                    Dim strHeight As String = ""
                    If (Row.Item("ProcessingScreenTemplateHeight") > 0) Then
                        strHeight = "height=""" & Row.Item("ProcessingScreenTemplateHeight") & """"
                    End If
                    If (Row.Item("ProcessingScreenTemplateCSSClass") <> strLastClass Or InStr(Row.Item("ProcessingScreenTemplateCSSClass"), "new ") > 0) Then
                        If (strLastClass <> "") Then
                            .WriteLine("</div>") ' /span
                        End If
                        .WriteLine("<div class=""" & Row.Item("ProcessingScreenTemplateCSSClass") & """>")
                    End If

                    .WriteLine("<div class=""widget"">")
                    .WriteLine("<div class=""widget-header"">")
                    .WriteLine("<h3><i class=""" & Row.Item("ApplicationTemplateIcon") & """></i>" & Row.Item("ApplicationTemplateName") & "</h3>")
                    If (getPageCount(Row.Item("ProcessingScreenTemplateApplicationTemplateID")) > 1) Then
                        .WriteLine("<div class=""widget-actions"">")
                        .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-file""></i> <span class=""title"">Pages</span> <span class=""caret""></span></button>")
                        .WriteLine("<ul class=""dropdown-menu"">")
                        .WriteLine(getPages(Row.Item("ProcessingScreenTemplateApplicationTemplateID")))
                        .WriteLine("</ul>")
                        .WriteLine("</div>")
                    End If
                    .WriteLine("</div>")
                    .WriteLine("<div class=""widget-content"">")
                    .WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/business/panel.aspx?BusinessObjectID=" & BusinessObjectID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                    .WriteLine("</div>") ' /widget content
                    .WriteLine("</div>") ' /widget
                    strLastClass = Replace(Row.Item("ProcessingScreenTemplateCSSClass"), "new ", "")
                    x += 1
                Next
            Else
                strSQL = "SELECT ProcessingScreenTemplateApplicationTemplateID, ProcessingScreenTemplateCSSClass, ProcessingScreenTemplateDisplay, ProcessingScreenTemplateColumn, ApplicationTemplateName, ApplicationTemplateIcon, ProcessingScreenTemplateHeight FROM vwbusinessprocessingscreen WHERE BusinessObjectID = '" & BusinessObjectID & "' " & _
                    "AND (ProcessingScreenUserLevel = N'||') " & _
                    "ORDER BY ProcessingScreenTemplateOrder "
					
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = ""
                    For Each Row As DataRow In dsCache.Rows

                        Dim strHeight As String = ""
                        If (Row.Item("ProcessingScreenTemplateHeight") > 0) Then
                            strHeight = "height=""" & Row.Item("ProcessingScreenTemplateHeight") & """"
                        End If
                        If (Row.Item("ProcessingScreenTemplateCSSClass") <> strLastClass Or InStr(Row.Item("ProcessingScreenTemplateCSSClass"), "new ") > 0) Then
                            If (strLastClass <> "") Then
                                .WriteLine("</div>") ' /span
                            End If
                            .WriteLine("<div class=""" & Row.Item("ProcessingScreenTemplateCSSClass") & """>")
                        End If

                        .WriteLine("<div class=""widget"">")
                        .WriteLine("<div class=""widget-header"">")
                        .WriteLine("<h3><i class=""" & Row.Item("ApplicationTemplateIcon") & """></i>" & Row.Item("ApplicationTemplateName") & "</h3>")
                        If (getPageCount(Row.Item("ProcessingScreenTemplateApplicationTemplateID")) > 1) Then
                            .WriteLine("<div class=""widget-actions"">")
                            .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-file""></i> <span class=""title"">Pages</span> <span class=""caret""></span></button>")
                            .WriteLine("<ul class=""dropdown-menu"">")
                            .WriteLine(getPages(Row.Item("ProcessingScreenTemplateApplicationTemplateID")))
                            .WriteLine("</ul>")
                            .WriteLine("</div>")
                        End If
                        .WriteLine("</div>")
                        .WriteLine("<div class=""widget-content"">")
                        .WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/business/panel.aspx?BusinessObjectID=" & BusinessObjectID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                        .WriteLine("</div>") ' /widget content
                        .WriteLine("</div>") ' /widget
                        strLastClass = Replace(Row.Item("ProcessingScreenTemplateCSSClass"), "new ", "")
                        x += 1
                    Next
                Else
                    .WriteLine("<div class=""span12"">")
                    .WriteLine("<h3>Sorry, no layout is available. Please check your user permissions.</h3>")
                    .WriteLine("</div>")
                End If
            End If
            dsCache = Nothing
            HttpContext.Current.Response.Write(objStringWriter.ToString)
        End With
    End Sub

    Private Function getPageCount(ByVal templateID As String) As Integer
        Return 0
        'Dim intNumPages As String = ""
        'Dim strSQL As String = "SELECT MAX(ApplicationPageOrder) + 1 FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "'AND ApplicationTemplateID = '" & templateID & "' AND CompanyID = '" & CompanyID & "'"
        'intNumPages = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        'If (checkValue(intNumPages)) Then
        '    intNumPages = CInt(intNumPages)
        'Else
        '    intNumPages = 0
        'End If
        'Return intNumPages
    End Function

    Private Function getPages(ByVal templateID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT ApplicationPageName, ApplicationPageOrder FROM tblapplicationpages WHERE ApplicationPageTemplateID = '" & templateID & "' AND CompanyID = '" & CompanyID & "' AND ApplicationPageActive = 1 ORDER BY ApplicationPageOrder"
            responseWrite(strSQL)
            Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<li><a href=""#"" onclick=""changePage($(this)," & BusinessObjectID & "," & templateID & "," & Row.Item("ApplicationPageOrder") & ",'" & Row.Item("ApplicationPageName").ToString & "');return false;"">" & Row.Item("ApplicationPageName").ToString & "</a></li>")
                Next
            End If
            dsCache = Nothing
        End With
        Return objStringWriter.ToString
    End Function

End Class