﻿Imports Config, Common, CommonSave, CallInterface
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Threading.Thread
Imports System.Net
Imports System.Xml

' ** Revision history **
'
' 30/01/2012    - saveChangeStatus, saveChangeMediaCampaign and saveChangeUser updated to allow multiple cases to be updated at the same time
' 30/03/2012	- Added save routine saveSetDashboard
' ** End Revision History **

Public Class ProcessingSave

    Private objLeadPlatform As LeadPlatform = Nothing
    Private AppID As String = HttpContext.Current.Request("frmAppID"), MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID"), BusinessObjectID As String = HttpContext.Current.Request("frmBusinessObjectID")
    Private strUpdateStatusCode As String = HttpContext.Current.Request("frmUpdateStatusCode"), strUpdateSubStatusCode As String = HttpContext.Current.Request("frmUpdateSubStatusCode"), strUpdateDateType As String = HttpContext.Current.Request("frmUpdateDateType"), strClearDateType As String = HttpContext.Current.Request("frmClearDateType")
    Private strHiddenNote As String = HttpContext.Current.Request("frmHiddenNote"), strNote As String = HttpContext.Current.Request("frmNote")
    Private dteNextCallDate As String = HttpContext.Current.Request("frmNextCallDate")
    Private strThisPg As String = HttpContext.Current.Request("strThisPg")
    Private strButtonClick As String = HttpContext.Current.Request("strButtonClick"), strEndCall As String = HttpContext.Current.Request("strEndCall")
    Private strFrmAction As String = HttpContext.Current.Request("strFrmAction")
    Private strFrmActionParameters As String = "", strSendEmails As String = "", strSendSMS As String = "", strCreateDiaries As String = "", strCompleteDiaries As String = "", strCancelDiaries As String = "", strCreateReminders As String = "", strCreateRemindersUserID As String = "", strSocialPost As String = ""
    Private strReturnUrl As String = HttpContext.Current.Request("strReturnUrl")
    Private strReturnPg As String = ""
    Private strReturnUrlMessage As String = "", strReturnUrlMessageTitle As String = "", strReturnUrlVariables As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Sub save()
        Select Case strThisPg
            Case "outsidecriteria"
                saveOutsideCriteria()
            Case "return"
                saveReturn()
            Case "hotkey"
                Select Case strFrmAction
                    Case "cancel"
                        saveHotkeyCancel()
                    Case "return"
                        saveHotkeyReturn()
                End Select
            Case "workflow"
                Select Case strFrmAction
                    Case "kick"
                        saveWorkflowKickOut()
                    Case "userlevel"
                        saveUserLevel()
                End Select
            Case "changecase"
                Select Case strFrmAction
                    Case "status"
                        saveChangeStatus()
                    Case "mc"
                        saveChangeMediaCampaign()
                    Case "user"
                        saveChangeUser()
                    Case "new"
                        saveApplicationReset(AppID)
                    Case "delete"
                        saveApplicationActive(AppID)
                    Case "productchange"
                        saveChangeProduct()
                End Select
            Case "changebusinesscase"
                Select Case strFrmAction
                    Case "status"
                        saveBusinessChangeStatus()
                    Case "mc"
                        saveBusinessChangeMediaCampaign()
                    Case "user"
                        saveBusinessChangeUser()
                    Case "delete"
                        saveBusinessActive(BusinessObjectID)
                End Select
            Case "processing"
                If (checkValue(strFrmAction)) Then
                    Dim strFrmActionName As String = ""
                    If (strEndCall = "Y") Then
                        Dim strSQL As String = "SELECT ProcessingScreenFormAction, ProcessingScreenFormActionParameters FROM tblprocessingscreens WHERE ProcessingScreenID = '" & strFrmAction & "' AND CompanyID = '" & CompanyID & "'"
                        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            For Each Row As DataRow In dsCache.Rows
                                strFrmActionName = Row.Item("ProcessingScreenFormAction").ToString
                                strFrmActionParameters = Row.Item("ProcessingScreenFormActionParameters").ToString
                            Next
                        End If
                        dsCache = Nothing
                    Else
                        If (strButtonClick <> "Y") Then
                            Dim strSQL As String = "SELECT ApplicationPageFormAction, ApplicationPageFormActionParameters, ApplicationPageCreateDiaries, ApplicationPageCompleteDiaries, ApplicationPageCancelDiaries, ApplicationPageSendEmails, ApplicationPageSendSMS, ApplicationPageSocialPost FROM tblapplicationpages WHERE ApplicationPageID = '" & strFrmAction & "' AND CompanyID = '" & CompanyID & "'"
                            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                            If (dsCache.Rows.Count > 0) Then
                                For Each Row As DataRow In dsCache.Rows
                                    strFrmActionName = Row.Item("ApplicationPageFormAction").ToString
                                    strFrmActionParameters = Row.Item("ApplicationPageFormActionParameters").ToString
                                    strCreateDiaries = Row.Item("ApplicationPageCreateDiaries").ToString
                                    strCompleteDiaries = Row.Item("ApplicationPageCompleteDiaries").ToString
                                    strCancelDiaries = Row.Item("ApplicationPageCancelDiaries").ToString
                                    strSendEmails = Row.Item("ApplicationPageSendEmails").ToString
                                    strSendSMS = Row.Item("ApplicationPageSendSMS").ToString
                                    strSocialPost = Row.Item("ApplicationPageSocialPost").ToString
                                Next
                            End If
                            dsCache = Nothing
                        Else
                            Dim strSQL As String = "SELECT AdditionalFieldButtonFormAction, AdditionalFieldButtonFormActionParameters, AdditionalFieldButtonSendEmails, AdditionalFieldButtonSendSMS, AdditionalFieldButtonCreateDiaries, AdditionalFieldButtonCompleteDiaries, AdditionalFieldButtonCancelDiaries, AdditionalFieldButtonCreateReminders, AdditionalFieldButtonCreateRemindersUserID, AdditionalFieldButtonSocialPost FROM tblapplicationpageadditionalfields WHERE AdditionalFieldID = '" & strFrmAction & "' AND CompanyID = '" & CompanyID & "'"
                            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                            If (dsCache.Rows.Count > 0) Then
                                For Each Row As DataRow In dsCache.Rows
                                    strFrmActionName = Row.Item("AdditionalFieldButtonFormAction").ToString
                                    strFrmActionParameters = Row.Item("AdditionalFieldButtonFormActionParameters").ToString
                                    strSendEmails = Row.Item("AdditionalFieldButtonSendEmails").ToString
                                    strSendSMS = Row.Item("AdditionalFieldButtonSendSMS").ToString
                                    strCreateDiaries = Row.Item("AdditionalFieldButtonCreateDiaries").ToString
                                    strCompleteDiaries = Row.Item("AdditionalFieldButtonCompleteDiaries").ToString
                                    strCancelDiaries = Row.Item("AdditionalFieldButtonCancelDiaries").ToString
                                    strCreateReminders = Row.Item("AdditionalFieldButtonCreateReminders").ToString
                                    strCreateRemindersUserID = Row.Item("AdditionalFieldButtonCreateRemindersUserID").ToString
                                    strSocialPost = Row.Item("AdditionalFieldButtonSocialPost").ToString
                                Next
                            End If
                            dsCache = Nothing
                        End If
                    End If

                    If (checkValue(strFrmActionName)) Then
                        Dim arrFrmActionParameters As Array = Split(strFrmActionParameters, ",")
                        Dim arrFrmAction As Array = Split(strFrmActionName, ",")
                        For x As Integer = 0 To UBound(arrFrmAction)
                            Dim arrParams As String() = Nothing, arrTempParams As String() = Nothing
                            'responseWrite(x & ". " & arrFrmAction(x) & "(")
                            arrTempParams = Split(arrFrmActionParameters(x), "|")
                            For y As Integer = 0 To UBound(arrTempParams)
                                If (checkValue(arrTempParams(y))) Then
                                    ReDim Preserve arrParams(y)
                                    arrParams(y) = arrTempParams(y)
                                    'responseWrite(arrParams(y))
                                End If
                            Next
                            'responseWrite(") ")
                            executeSub(Me, arrFrmAction(x), arrParams)
                        Next
                    End If

                    Dim strEmailMessage As String = "", strSMSMessage As String = ""
                    If (checkValue(strSendEmails)) Then
                        strEmailMessage = sendEmails(AppID, strSendEmails, objLeadPlatform.Config.CompanyName)
                    End If
                    If (checkValue(strSendSMS)) Then
                        strSMSMessage = sendSMS(AppID, strSendSMS)
                    End If
                    If (checkValue(strSMSMessage) Or checkValue(strEmailMessage)) Then
                        strReturnUrlMessageTitle = "Customer Communication"
                        strReturnUrlMessage = strEmailMessage & strSMSMessage
                    End If
                    If (checkValue(strCreateDiaries)) Then
                        createDiaries(AppID, strCreateDiaries)
                    End If
                    If (checkValue(strCompleteDiaries)) Then
                        completeDiaries(AppID, strCompleteDiaries)
                    End If
                    If (checkValue(strCancelDiaries)) Then
                        cancelDiaries(AppID, strCancelDiaries)
                    End If
                    If (checkValue(strCreateReminders)) Then
                        createReminders(AppID, strCreateReminders, strCreateRemindersUserID)
                    End If
                    If (checkValue(strSocialPost)) Then
                        saveSocialPost(AppID, "", strSocialPost)
                    End If
					
					If (strUpdateStatusCode = "QUO")Then 
						saveCaseCallUsers()
				
					End If
					
					If (strUpdateStatusCode = "PKR") Then
						saveCaseAdminUsers()
					End If

                End If
            Case "endcall"
                Select Case strFrmAction
                    Case "callback"
                        If (HttpContext.Current.Request("frmWorkflowDiary") = "Y") Then
                            saveCallBack(AppID, Config.DefaultUserID, dteNextCallDate, objLeadPlatform.Config.WorkflowDiary, objLeadPlatform.Config.UserActiveWorkflowDiaryID)
                        Else
                            saveCallBack(AppID, Config.DefaultUserID, dteNextCallDate)
                        End If
                    Case "cancelcallback"
                        saveCallBackCancel(AppID)
                End Select
            Case "businessendcall"
                Select Case strFrmAction
                    Case "callback"
                        If (HttpContext.Current.Request("frmWorkflowDiary") = "Y") Then
                            saveBusinessCallBack(BusinessObjectID, Config.DefaultUserID, dteNextCallDate, objLeadPlatform.Config.WorkflowDiary, objLeadPlatform.Config.UserActiveWorkflowDiaryID)
                        Else
                            saveBusinessCallBack(BusinessObjectID, Config.DefaultUserID, dteNextCallDate)
                        End If
                    Case "cancelcallback"
                        saveBusinessCallBackCancel(BusinessObjectID)
                End Select
            Case "diary"
                Select Case strFrmAction
                    Case "update"
                        saveDiary()
                    Case "active"
                        saveDiaryActive()
                    Case "complete"
                        saveDiaryComplete()
                End Select
            Case "document"
                Select Case strFrmAction
                    Case "delete"
                        saveDocumentDelete()
                    Case "echosign"
                        sendEchoSign()
                End Select
            Case "email"
                sendCustomerEmail()
            Case "sms"
                sendCustomerSMS()
            Case "reminder"
                Select Case strFrmAction
                    Case "add"
                        saveReminderAdd()
                    Case "snooze"
                        saveReminderSnooze()
                    Case "clear"
                        saveReminderDelete()
                End Select
            Case "dashboard"
                Select Case strFrmAction
                    Case "set"
                        saveSetDashboard()
                End Select
            Case "outofoffice"
                Select Case strFrmAction
                    Case "set"
                        Dim intOutOfOfficeID As Integer = OutOfOfficeStatus.Ready
                        If (checkValue(HttpContext.Current.Request("frmOutOfOfficeID"))) Then
                            intOutOfOfficeID = HttpContext.Current.Request("frmOutOfOfficeID")
                        End If
                        saveOutOfOffice(CompanyID, Config.DefaultUserID, Config.DefaultUserID, intOutOfOfficeID)
                End Select
            Case "password"
                Select Case strFrmAction
                    Case "change"
                        Dim strUserPassword As String = HttpContext.Current.Request("txtUserPassword"), strChangePassword As String = HttpContext.Current.Request("txtChangePassword")
                        Dim strSQL As String = "SELECT UserID, CompanyID, UserPassword FROM tblusers WHERE UserID = '" & Config.DefaultUserID & "'"
                        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            For Each Row As DataRow In dsCache.Rows
                                If (strUserPassword = Encryption.encrypt(Row.Item("UserPassword"))) Then
                                    If (passwordPolicyCheck(strChangePassword)) Then
                                        Dim strQry As String = "UPDATE tblusers SET " & _
                                            "UserPassword			= @UserPassword, " & _
                                            "UserPasswordChangeDate	= " & formatField(Config.DefaultDateTime.AddDays(60), "DTTM", DefaultDateTime) & " " & _
                                            "WHERE UserID 		    = @UserID " & _
                                            "AND CompanyID          = @CompanyID "
                                        Dim arrParams As SqlParameter() = {New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, Row.Item("UserID")), _
                                                                            New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, Row.Item("CompanyID")), _
                                                                            New SqlParameter("@UserPassword", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "UserPassword", DataRowVersion.Current, Encryption.encrypt(strChangePassword))}
                                        executeNonQuery(strQry, CommandType.Text, arrParams)
                                        strReturnUrlMessageTitle = "Success"
                                        strReturnUrlMessage = "Your password has been updated."
                                    Else
                                        strReturnUrlMessageTitle = "Error"
                                        strReturnUrlMessage = "Please ensure that the password conforms to the password policy. Passwords will need to be a minimum of 7 characters, a maximum of 12 characters and will need to contain a combination of the following options: English uppercase or lowercase letters [A-Z] and at least 2 Numbers [0-9]"
                                    End If
                                Else
                                    strReturnUrlMessageTitle = "Error"
                                    strReturnUrlMessage = "Current password is incorrect. Please input the correct password and try again."
                                End If
                            Next
                        End If
                        dsCache = Nothing
                End Select
            Case "paymenthistory"
                Select Case strFrmAction
                    Case "delete"
                        savePaymentHistoryActive()
                End Select
            Case Else
                strReturnPg = "/"
        End Select
        If (checkValue(strUpdateStatusCode) Or checkValue(strUpdateSubStatusCode)) Then
            If (checkValue(AppID)) Then
                saveStatus(AppID, Config.DefaultUserID, strUpdateStatusCode, strUpdateSubStatusCode, objLeadPlatform.Config.UserActiveWorkflowID, objLeadPlatform.Config.WorkflowDiary, objLeadPlatform.Config.UserActiveWorkflowDiaryID)
            Else
                saveBusinessStatus(BusinessObjectID, Config.DefaultUserID, strUpdateStatusCode, strUpdateSubStatusCode, objLeadPlatform.Config.UserActiveWorkflowID, objLeadPlatform.Config.WorkflowDiary, objLeadPlatform.Config.UserActiveWorkflowDiaryID)
            End If
        End If
        If (checkValue(strUpdateDateType)) Then
            If (checkValue(AppID)) Then
                saveUpdatedDate(AppID, Config.DefaultUserID, strUpdateDateType)
            Else
                saveBusinessUpdatedDate(BusinessObjectID, Config.DefaultUserID, strUpdateDateType)
            End If
        End If
        If (checkValue(strClearDateType)) Then
            If (checkValue(AppID)) Then
                clearUpdatedDate(AppID, strClearDateType)
            Else
                clearBusinessUpdatedDate(BusinessObjectID, strClearDateType)
            End If
        End If
        If (checkValue(strNote)) Then
            If (checkValue(AppID)) Then
                saveNote(AppID, Config.DefaultUserID, strNote)
            Else
                saveBusinessNote(BusinessObjectID, Config.DefaultUserID, strNote)
            End If
        End If
        If (checkValue(strHiddenNote)) Then
            If (checkValue(AppID)) Then
                saveNote(AppID, Config.DefaultUserID, strHiddenNote)
            Else
                saveBusinessNote(BusinessObjectID, Config.DefaultUserID, strHiddenNote)
            End If
        End If
        If (checkValue(strReturnUrl)) Then strReturnPg = strReturnUrl
        If (checkValue(strReturnUrlMessage)) Then
            If (InStr(strReturnPg, "?")) Then
                strReturnPg = strReturnPg & "&strMessage=" & strReturnUrlMessage & "&strMessageTitle=" & strReturnUrlMessageTitle
            Else
                strReturnPg = strReturnPg & "?strMessage=" & strReturnUrlMessage & "&strMessageTitle=" & strReturnUrlMessageTitle
            End If
        End If
        If (checkValue(strReturnUrlVariables)) Then
            If (InStr(strReturnPg, "?")) Then
                strReturnPg = strReturnPg & "&" & strReturnUrlVariables
            Else
                strReturnPg = strReturnPg & "?" & strReturnUrlVariables
            End If
        End If

        If (checkValue(strReturnPg)) Then
            HttpContext.Current.Response.Redirect(strReturnPg)
        Else
            HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))
        End If

    End Sub

    Private Sub saveOutsideCriteria()
        Dim boolOutsideCriteria As Boolean = HttpContext.Current.Request("frmOutsideCriteria")
        Dim boolMediaCampaignAutoReturn As Boolean = getAnyField("MediaCampaignAutoReturn", "tblmediacampaigns", "MediaCampaignID", HttpContext.Current.Request("frmMediaCampaignIDInbound"))
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            If (boolOutsideCriteria) Then
                strQry = "UPDATE tblapplicationstatus SET  " & _
                   "OutsideCriteriaAcceptedDate	    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                   "OutsideCriteriaDeclinedDate	    = " & formatField("", "DTTM", "NULL") & ", " & _
                   "OutsideCriteriaSubStatusCode    = " & formatField(HttpContext.Current.Request("frmOutsideCriteriaSubStatusCode"), "U", "NULL") & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
                If (Not boolMediaCampaignAutoReturn) Then
                    updateSingleDatabaseField(AppID, "tblapplicationstatus", "ReturnedPreSaleDate", "DTTM", Config.DefaultDateTime, Config.DefaultDateTime)
                End If
            Else
                strQry = "UPDATE tblapplicationstatus SET  " & _
                   "OutsideCriteriaAcceptedDate	    = " & formatField("", "DTTM", "NULL") & ", " & _
                   "OutsideCriteriaDeclinedDate	    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveReturn()
        Dim boolReturn As Boolean = HttpContext.Current.Request("frmReturn")
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            If (boolReturn) Then
                strQry = "UPDATE tblapplicationstatus SET  " & _
                   "ReturnAcceptedDate	            = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                   "ReturnDeclinedDate	            = " & formatField("", "DTTM", "NULL") & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
                updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignCostInbound", "N", 0, 0)
            Else
                strQry = "UPDATE tblapplicationstatus SET  " & _
                   "ReturnAcceptedDate	            = " & formatField("", "DTTM", "NULL") & ", " & _
                   "ReturnDeclinedDate	            = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveHotkeyCancel()
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            Dim strMediaCampaignIDOutbound As String = getAnyFieldByCompanyID("MediaCampaignIDOutbound", "tblapplications", "AppID", AppID)
            Dim strUpdateDate As String = getAnyField("MediaCampaignUpdateDateType", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignIDOutbound)
            If Not checkValue(strUpdateDate) Then strUpdateDate = "Transferred"
            strQry = "UPDATE tblapplications SET  " & _
                   "MediaCampaignIDOutbound         = " & formatField(0, "N", 0) & ", " & _
                   "ClientReferenceOutbound         = " & formatField("", "", "NULL") & ", " & _
                   "MediaUserRef                    = " & formatField("", "", "NULL") & ", " & _
                   "MediaCampaignCostOutbound       = " & formatField(0, "N", 0) & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            strQry = "UPDATE tblapplicationstatus SET  " & _
                   "StatusCode                      = " & formatField("NEW", "U", "NEW") & ", " & _
                   "SalesUserID                     = " & formatField(0, "N", 0) & ", " & _
                   "AdministratorUserID             = " & formatField(0, "N", 0) & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            clearUpdatedDate(AppID, strUpdateDate)
            strQry = "UPDATE tblstatushistory SET StatusHistoryStatusCode = '', StatusHistoryUserID = 0 WHERE AppID = '" & AppID & "' AND StatusHistoryStatusCode = 'TFR' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveHotkeyReturn()
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            Dim strMediaCampaignIDOutbound As String = getAnyFieldByCompanyID("MediaCampaignIDOutbound", "tblapplications", "AppID", AppID)
            Dim strUpdateDate As String = getAnyField("MediaCampaignUpdateDateType", "tblmediacampaigns", "MediaCampaignID", strMediaCampaignIDOutbound)
            If Not checkValue(strUpdateDate) Then strUpdateDate = "Transferred"
            strQry = "UPDATE tblapplications SET  " & _
                   "MediaCampaignIDOutbound         = " & formatField(0, "N", 0) & ", " & _
                   "ClientReferenceOutbound         = " & formatField("", "", "NULL") & ", " & _
                   "MediaUserRef                    = " & formatField("", "", "NULL") & ", " & _
                   "MediaCampaignCostOutbound       = " & formatField(0, "N", 0) & " " & _
                   "WHERE AppID	                    = '" & AppID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            updateSingleDatabaseField(AppID, "tblapplicationstatus", "StatusCode", "U", "NEW", "NEW")
            updateSingleDatabaseField(AppID, "tblapplicationstatus", strUpdateDate, "DTTM", "", "NULL")
            updateSingleDatabaseField(AppID, "tblapplicationstatus", "ReturnedPostSaleDate", "DTTM", Config.DefaultDateTime, Config.DefaultDateTime)
            updateSingleDatabaseField(AppID, "tblapplicationstatus", "TransferredUserID", "N", 0, 0)
            strQry = "UPDATE tblstatushistory SET StatusHistoryStatusCode = '', StatusHistoryUserID = 0 WHERE AppID = '" & AppID & "' AND StatusHistoryStatusCode = 'TFR' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveWorkflowKickOut()
        Dim intUserID As String = HttpContext.Current.Request("frmUserID")
        Dim intWorkflowActivityID As Integer = HttpContext.Current.Request("frmWorkflowActivityID")
        Dim intWorkflowID As String = HttpContext.Current.Request("frmWorkflowID")
        If (checkValue(intUserID)) Then
            Select Case intWorkflowActivityID
                Case ActivityType.StartCase
                    insertIdleHistory(AppID, intUserID)
                    insertInCallHistory(AppID, "", "", intUserID, "0")
                    saveNextCallTime(AppID, intWorkflowID)
                    insertWrapHistory(AppID, intUserID, "0")
                Case ActivityType.StartCall
                    insertInCallHistory(AppID, "", "", intUserID)
                    saveNextCallTime(AppID, intWorkflowID)
                    insertWrapHistory(AppID, intUserID, "0")
                Case ActivityType.EndCall
                    insertWrapHistory(AppID, intUserID)
            End Select
            Dim strQry As String = "UPDATE tblusers SET " & _
                "UserActiveWorkflowID               = " & formatField(0, "N", 0) & ", " & _
                "UserActiveWorkflowActivityID       = " & formatField(0, "N", 0) & ", " & _
                "UserActiveWorkflowAppID            = " & formatField(0, "N", 0) & ", " & _
                "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                "UserActiveWorkflowStartTime        = " & formatField("", "DTTM", "NULL") & ", " & _
                "UserActiveWorkflowCallRecording    = " & formatField(0, "B", 0) & ", " & _
                "UserWorkflowKickOut                = " & formatField(0, "B", 0) & " " & _
                "WHERE UserID 	                    = '" & intUserID & "' " & _
                "AND CompanyID                      = '" & CompanyID & "'"
            executeNonQuery(strQry)
            CommonSave.unlockCases(intUserID)
        End If
    End Sub

    Private Sub saveUserLevel()
        updateAnyDatabaseField("UserID", Config.DefaultUserID, "tblusers", "UserActiveLevelID", "N", HttpContext.Current.Request("ddUserAccessLevel"), 2)
    End Sub

    Private Sub saveChangeStatus()
        Dim strStatusCode As String = HttpContext.Current.Request("ddStatusCode")
        Dim strSubStatusCode As String = HttpContext.Current.Request("ddSubStatusCode")
        Dim arrAppID As Array = Split(AppID, ",") ' 30/01/2012 - Updated to allow multiple cases to be changed at the same time
        Dim strQry As String = ""
        For i As Integer = 0 To UBound(arrAppID)
            If (checkValue(arrAppID(i))) Then
                'strQry = "UPDATE tblapplicationstatus SET " & _
                '    "StatusCode     = " & formatField(strStatusCode, "U", "NEW") & ", " & _
                '    "SubStatusCode  = " & formatField(strSubStatusCode, "U", "NULL") & ", " & _
                '    "UpdatedDate    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                '    "WHERE AppID    = " & arrAppID(i) & " " & _
                '    "AND CompanyID  = '" & CompanyID & "'"
                'executeNonQuery(strQry)
                saveStatus(arrAppID(i), Config.DefaultUserID, strStatusCode, strSubStatusCode)
                saveNote(arrAppID(i), Config.DefaultUserID, "Case status changed: " & HttpContext.Current.Request("frmChangeNote"))
                If (strStatusCode = "WTD" Or strStatusCode = "TUD" Or strStatusCode = "INV") Then
                    saveUpdatedDate(arrAppID(i), Config.DefaultUserID, strStatusCode)
                End If
            End If
        Next
        'If (UBound(arrAppID) = 0) Then
        'strReturnUrlMessage = "Case status NOT changed, no AppIDs selected"
        'Else
        strReturnUrlMessage = "Case status changed to " & strStatusCode & " " & strSubStatusCode
        'End If
		
		    	
		If (strStatusCode = "QUO")Then 
			saveCaseCallUsers()
		
		Else If (strStatusCode = "PKR") Then
			saveCaseAdminUsers()
		End If
		
    End Sub
	
	Private Sub saveCaseCallUsers()
	
		Dim strQry as string = ""
	
		strQry = "Update tblapplicationstatus Set CallCentreUserID = '" & Config.DefaultUserID  & "' WHERE AppID= '" & AppID &  "' AND CompanyID = '" & CompanyID & "'"
		executeNonQuery(strQry)
	End Sub
	
	Private Sub saveCaseAdminUsers()
		Dim strQry as string = ""
		strQry = "Update tblapplicationstatus Set AdministratorUserID = '" & Config.DefaultUserID  & "' WHERE AppID= '" & AppID &  "' AND CompanyID = '" & CompanyID & "'"
		executeNonQuery(strQry)
		
	End Sub
	
    Private Sub saveChangeProduct()
        Dim strProductType As String = HttpContext.Current.Request("ddproductType")
        'Dim AppID As Integer = HttpContext.Current.Request("AppID")
        Dim strQry As String = ""
        If (checkValue(AppID = True)) Then
            strQry = "UPDATE tblapplications SET ProductType = '" & strProductType & "' WHERE AppID= '" & AppID & "' AND CompanyID= '" & CompanyID & "'"
            executeNonQuery(strQry)
            strReturnUrlMessage = "Product Type changed to " & strProductType


        End If
        'If (UBound(arrAppID) = 0) Then
        'strReturnUrlMessage = "Case status NOT changed, no AppIDs selected"
        'Else
        'strReturnUrlMessage = "Product Type changed to " & strProductType & AppID

        'End If
    End Sub

    Private Sub saveChangeMediaCampaign()
        Dim intMediaCampaignIDInbound As String = HttpContext.Current.Request("ddMediaCampaignIDInbound")
        Dim arrAppID As Array = Split(AppID, ",")
        Dim strQry As String = ""
        If (checkValue(intMediaCampaignIDInbound)) Then
            For i As Integer = 0 To UBound(arrAppID)
                strQry = "UPDATE tblapplications SET MediaCampaignIDInbound = " & formatField(intMediaCampaignIDInbound, "N", 0) & " WHERE AppID = " & arrAppID(i) & " AND CompanyID  = '" & CompanyID & "'"
                executeNonQuery(strQry)
                saveNote(arrAppID(i), Config.DefaultUserID, "Source changed: " & HttpContext.Current.Request("frmChangeNote"))
            Next
            strReturnUrlMessage = "Case(s) successfully updated"
        End If
    End Sub

    Private Sub saveChangeUser()
        Dim arrAppID As Array = Split(AppID, ",")
        Dim strQry As String = ""
        For i As Integer = 0 To UBound(arrAppID)
            strQry = "UPDATE tblapplicationstatus SET " & _
                                    "CallCentreUserID       = " & formatField(HttpContext.Current.Request("frmCallCentreUserID"), "N", "CallCentreUserID") & ", " & _
                                    "SalesUserID            = " & formatField(HttpContext.Current.Request("frmSalesUserID"), "N", "SalesUserID") & ", " & _
                                    "AdministratorUserID    = " & formatField(HttpContext.Current.Request("frmAdministratorUserID"), "N", "AdministratorUserID") & " " & _
                                    "WHERE AppID            = " & arrAppID(i) & " " & _
                                    "AND CompanyID          = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(arrAppID(i), Config.DefaultUserID, "Users changed: Call Centre - " & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("frmCallCentreUserID")) & ", Sales - " & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("frmSalesUserID")) & ", Administrator - " & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("frmAdministratorUserID")))
        Next
        strReturnUrlMessage = "Case(s) successfully updated"
    End Sub

    Private Sub saveBusinessChangeStatus()
        Dim strStatusCode As String = HttpContext.Current.Request("ddStatusCode")
        Dim strSubStatusCode As String = HttpContext.Current.Request("ddSubStatusCode")
        Dim arrAppID As Array = Split(BusinessObjectID, ",") ' 30/01/2012 - Updated to allow multiple cases to be changed at the same time
        Dim strQry As String = ""
        For i As Integer = 0 To UBound(arrAppID)
            If (checkValue(arrAppID(i))) Then
                saveBusinessStatus(arrAppID(i), Config.DefaultUserID, strStatusCode, strSubStatusCode)
                saveBusinessNote(arrAppID(i), Config.DefaultUserID, "Case status changed: " & HttpContext.Current.Request("frmChangeNote"))
                If (strStatusCode = "WTD" Or strStatusCode = "TUD" Or strStatusCode = "INV") Then
                    saveBusinessUpdatedDate(arrAppID(i), Config.DefaultUserID, strStatusCode)
                End If
            End If
        Next
        'If (UBound(arrAppID) = 0) Then
        'strReturnUrlMessage = "Case status NOT changed, no AppIDs selected"
        'Else
        strReturnUrlMessage = "Case status changed to " & strStatusCode & " " & strSubStatusCode
        'End If
    End Sub

    Private Sub saveBusinessChangeMediaCampaign()
        Dim intMediaCampaignID As String = HttpContext.Current.Request("ddMediaCampaignID")
        Dim arrAppID As Array = Split(BusinessObjectID, ",")
        Dim strQry As String = ""
        If (checkValue(intMediaCampaignID)) Then
            For i As Integer = 0 To UBound(arrAppID)
                strQry = "UPDATE tblbusinessobjects SET BusinessObjectMediaCampaignID = " & formatField(intMediaCampaignID, "N", 0) & " WHERE BusinessObjectID = " & arrAppID(i) & " AND CompanyID  = '" & CompanyID & "'"
                executeNonQuery(strQry)
                saveBusinessNote(arrAppID(i), Config.DefaultUserID, "Source changed: " & HttpContext.Current.Request("frmChangeNote"))
            Next
            strReturnUrlMessage = "Case(s) successfully updated"
        End If
    End Sub

    Private Sub saveBusinessChangeUser()
        Dim arrAppID As Array = Split(BusinessObjectID, ",")
        Dim strQry As String = ""
        For i As Integer = 0 To UBound(arrAppID)
            strQry = "UPDATE tblbusinessobjectstatus SET " & _
                            "BusinessObjectAssignedUserID   = " & formatField(HttpContext.Current.Request("frmAssignedUserID"), "N", "BusinessObjectAssignedUserID") & " " & _
                            "WHERE BusinessObjectID         = " & arrAppID(i) & " " & _
                            "AND CompanyID          = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(arrAppID(i), Config.DefaultUserID, "Users changed: Assigned - " & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("frmAssignedUserID")))
        Next
        strReturnUrlMessage = "Case(s) successfully updated"
    End Sub

    Private Sub sendCustomerEmail()
        Dim strEmailSubject As String = decodeURL(HttpContext.Current.Request.Form("frmEmailSubject"))
        Dim strEmailBody As String = decodeURL(HttpContext.Current.Request.Form("frmEmailBody"))
        Dim strEmailAttachments As String = decodeURL(HttpContext.Current.Request.Form("frmEmailAttachments"))
        Dim strSelectedAttachments As String = decodeURL(HttpContext.Current.Request.Form("frmSelectedAttachments"))
        Dim strStatus As String = postEmailWithProfile("", HttpContext.Current.Request("frmEmailAddress"), strEmailSubject, strEmailBody, True, strEmailAttachments & strSelectedAttachments, HttpContext.Current.Request.Form("frmEmailProfileID"), HttpContext.Current.Request.Form("frmEmailReplyTo"))
        getWebRequest(Config.ApplicationURL & "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & HttpContext.Current.Request("LetterID") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID)
        'saveNote(AppID, Config.DefaultUserID, "Email sent to <strong>" & HttpContext.Current.Request("frmEmailAddress") & "</strong>: " & strEmailBody)
		If (strStatus = "1") Then
			strReturnUrlVariables = "strSuccessMessage=" & encodeURL("Email successfully sent to <strong>" & HttpContext.Current.Request("frmEmailAddress") & "</strong>")
			CommonSave.archivePDF(AppID, Config.DefaultUserID, strEmailSubject, strEmailBody, "")
		Else
		    strReturnUrlVariables = "strFailureMessage=" & encodeURL("Email unsuccessfully sent")
		End If
    End Sub

    Private Sub sendCustomerSMS()
        Dim strSMSBody As String = decodeURL(HttpContext.Current.Request.Form("frmSMSBody"))
		'responseWrite(Config.ApplicationURL & "/webservices/outbound/sms.aspx?AppID=" & AppID & "&frmTelephoneTo=" & HttpContext.Current.Request("frmTelephoneNumber") & "&frmSMSBody=" & strSMSBody & "&frmEmailProfileID=" & HttpContext.Current.Request.Form("frmEmailProfileID") & "&UserSessionID=" & Config.UserSessionID)
		'responseEnd()
        Dim objResponse As Net.HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/sms.aspx?AppID=" & AppID & "&frmTelephoneTo=" & HttpContext.Current.Request("frmTelephoneNumber") & "&frmSMSBody=" & strSMSBody & "&frmEmailProfileID=" & HttpContext.Current.Request.Form("frmEmailProfileID") & "&UserSessionID=" & Config.UserSessionID)
		
        If (checkResponse(objResponse, "AppID=" & AppID)) Then
            HttpContext.Current.Response.Clear()
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            Dim arrResponse As Array = Split(strResponse, "|")
            Dim strSmsID As String = arrResponse(2)
            If (arrResponse(0) = "0") Then
                'postEmail("", "ben.snaize@engaged-solutions.co.uk", "SMS", Config.ApplicationURL & "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & HttpContext.Current.Request("LetterID") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"), True, "")
                strReturnUrlVariables = "strSuccessMessage=" & encodeURL("SMS successfully sent to " & HttpContext.Current.Request("frmTelephoneNumber") & "</strong>")
                'getWebRequest(Config.ApplicationURL & "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & HttpContext.Current.Request("LetterID") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID)
                CommonSave.archivePDF(AppID, Config.DefaultUserID, "SMS.pdf", strSMSBody, strSmsID)
                saveNote(AppID, Config.DefaultUserID, "SMS sent to " & HttpContext.Current.Request("frmTelephoneNumber") & ": " & strSMSBody)
            Else
                strReturnUrlVariables = "strFailureMessage=" & encodeURL("SMS unsuccessfully sent (Reason: " & Replace(arrResponse(1), "_", "") & ")</strong>")
            End If
            objReader.Close()
            objReader = Nothing
        Else
            strReturnUrlVariables = "strFailureMessage=" & encodeURL("SMS unsuccessfully sent, please contact Engaged Solutions</strong>")
        End If
        If (checkResponse(objResponse, "AppID=" & AppID)) Then
            objResponse.Close()
        End If
        objResponse = Nothing
    End Sub

    Private Sub archivePDF(ByVal AppID As String, ByVal userid As String, ByVal pdftitle As String, ByVal pdfbody As String, smsid As String)
        Dim strQry As String = _
            "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryName, PDFHistoryBody, PDFHistoryCreatedUserID, PDFHistorySMSID) " & _
            "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
            formatField(AppID, "N", 0) & ", " & _
            formatField(pdftitle, "T", "") & ", " & _
            formatField(pdfbody, "", "") & ", " & _
            formatField(userid, "N", getSystemUser()) & ", " & _
            formatField(smsid, "N", 0) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub saveReminderAdd()
        saveReminder(AppID, Config.DefaultUserID, HttpContext.Current.Request("frmReminderAssignedToUserID"), HttpContext.Current.Request("frmReminderDate"), HttpContext.Current.Request("frmReminderDescription"))
        saveNote(AppID, Config.DefaultUserID, "Reminder for " & HttpContext.Current.Request("frmReminderDate") & " assigned to " & getAnyField("UserFullName", "tblusers", "UserID", HttpContext.Current.Request("frmReminderAssignedToUserID")))
    End Sub

    Private Sub saveReminderSnooze()
        Dim ReminderID As String = HttpContext.Current.Request("ReminderID")
        Dim strQry As String = ""
        If (InStr(ReminderID, ",") = 0) Then
            strQry = "UPDATE tblreminders SET ReminderDateTime = DATEADD(N," & HttpContext.Current.Request("frmSnooze") & ",ReminderDateTime) WHERE ReminderID	= '" & ReminderID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        Else
            Dim arrReminderID As Array = Split(ReminderID, ",")
            For x As Integer = 0 To UBound(arrReminderID)
                If (checkValue(arrReminderID(x))) Then
                    strQry = "UPDATE tblreminders SET ReminderDateTime = DATEADD(N," & HttpContext.Current.Request("frmSnooze") & ",ReminderDateTime) WHERE ReminderID	= '" & arrReminderID(x) & "' AND CompanyID = '" & CompanyID & "'"
                    executeNonQuery(strQry)
                End If
            Next
        End If
    End Sub

    Private Sub saveReminderDelete()
        Dim ReminderID As String = HttpContext.Current.Request("ReminderID")
        Dim strQry As String = ""
        If (InStr(ReminderID, ",") = 0) Then
            strQry = "DELETE FROM tblreminders WHERE ReminderID	= '" & ReminderID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        Else
            Dim arrReminderID As Array = Split(ReminderID, ",")
            For x As Integer = 0 To UBound(arrReminderID)
                If (checkValue(arrReminderID(x))) Then
                    strQry = "DELETE FROM tblreminders WHERE ReminderID	= '" & arrReminderID(x) & "' AND CompanyID = '" & CompanyID & "'"
                    executeNonQuery(strQry)
                End If
            Next
        End If
    End Sub

    Private Sub saveSetDashboard()
        updateUserStoreField(Config.DefaultUserID, "ActiveDashboardID", HttpContext.Current.Request("frmActiveDashboardID"), "N", 0)
    End Sub

    Private Sub saveDiary()
        Dim DiaryID As String = HttpContext.Current.Request("DiaryID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(DiaryID)) Then
            strQry = "UPDATE tbldiaries SET  " & _
               "DiaryTypeDescription    = " & formatField(HttpContext.Current.Request("frmDiaryTypeDescription"), "", "") & " " & _
               "WHERE DiaryID	= '" & DiaryID & "' " & _
               "AND CompanyID       = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tbldiaries (CompanyID, AppID, DiaryType, DiaryTypeDescription, DiaryShowInCalendar) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(AppID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmDiaryType"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("frmDiaryTypeDescription"), "", "") & ", " & _
               formatField(0, "N", 0) & ") "
            executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveDiaryActive()
        Dim DiaryID As String = HttpContext.Current.Request("DiaryID")
        Dim DiaryActive As String = HttpContext.Current.Request("DiaryActive")
        Dim strDiaryTypeName As String = getAnyField("DiaryType", "tbldiaries", "DiaryID", DiaryID)
        Dim strQry As String = ""
        If (checkValue(DiaryID)) Then
            If (DiaryActive) Then
                strQry = "UPDATE tbldiaries SET  " & _
                   "DiaryActive	    = 0 " & _
                   "WHERE DiaryID	= '" & DiaryID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
                Dim strSQL As String = "SELECT DiaryCreateDiaries, DiaryCompleteDiaries, DiaryCancelDiaries FROM tbldiarytypes WHERE DiaryTypeName = '" & strDiaryTypeName & "' AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        strCreateDiaries = Row.Item("DiaryCreateDiaries").ToString
                        strCompleteDiaries = Row.Item("DiaryCompleteDiaries").ToString
                        strCancelDiaries = Row.Item("DiaryCancelDiaries").ToString
                    Next
                End If
                dsCache = Nothing
                If (checkValue(strCreateDiaries)) Then
                    createDiaries(AppID, strCreateDiaries)
                End If
                If (checkValue(strCompleteDiaries)) Then
                    completeDiaries(AppID, strCompleteDiaries)
                End If
                If (checkValue(strCancelDiaries)) Then
                    cancelDiaries(AppID, strCancelDiaries)
                End If
            Else
                strQry = "UPDATE tbldiaries SET  " & _
                   "DiaryActive	    = 1 " & _
                   "WHERE DiaryID	= '" & DiaryID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
        End If
    End Sub

    Private Sub saveDiaryComplete()
        Dim DiaryID As String = HttpContext.Current.Request("DiaryID")
        Dim DiaryComplete As String = HttpContext.Current.Request("DiaryComplete")
        Dim strDiaryTypeName As String = getAnyField("DiaryType", "tbldiaries", "DiaryID", DiaryID)
        Dim strQry As String = ""
        If (checkValue(DiaryID)) Then
            If (DiaryComplete) Then
                strQry = "UPDATE tbldiaries SET  " & _
                   "DiaryComplete	    = 0 " & _
                   "WHERE DiaryID	    = '" & DiaryID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tbldiaries SET  " & _
                   "DiaryComplete	    = 1 " & _
                   "WHERE DiaryID	    = '" & DiaryID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
        End If
        Dim strSQL As String = "SELECT DiaryCreateDiaries, DiaryCompleteDiaries, DiaryCancelDiaries FROM tbldiarytypes WHERE DiaryTypeName = '" & strDiaryTypeName & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strCreateDiaries = Row.Item("DiaryCreateDiaries").ToString
                strCompleteDiaries = Row.Item("DiaryCompleteDiaries").ToString
                strCancelDiaries = Row.Item("DiaryCancelDiaries").ToString
            Next
        End If
        dsCache = Nothing
        If (checkValue(strCreateDiaries)) Then
            createDiaries(AppID, strCreateDiaries)
        End If
        If (checkValue(strCompleteDiaries)) Then
            completeDiaries(AppID, strCompleteDiaries)
        End If
        If (checkValue(strCancelDiaries)) Then
            cancelDiaries(AppID, strCancelDiaries)
        End If
        strSQL = "SELECT TOP(1) DiaryID FROM tbldiaries WHERE ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & CompanyID & "') AND (DiaryAssignedToUserID = '" & Config.DefaultUserID & "') AND (AppID = '" & AppID & "')) " & _
            " OR ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & CompanyID & "') AND (DiaryAssignedToUserID = 0) AND (AppID = '" & AppID & "')) ORDER BY DiaryAssignedToUserID DESC, DiaryDueDate"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strSQL = "UPDATE tblusers SET UserActiveWorkflowDiaryID = '" & objResult.ToString & "' WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Private Sub saveDocumentDelete()
        Dim PDFHistoryID As String = HttpContext.Current.Request("PDFHistoryID")
        Dim strQry As String = ""
        If (checkValue(PDFHistoryID)) Then
            strQry = "DELETE FROM tblpdfhistory WHERE PDFHistoryID = '" & PDFHistoryID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        If (checkValue(HttpContext.Current.Request("PDFHistoryFileName"))) Then
            File.Delete("F:/data/attachments" & HttpContext.Current.Request("PDFHistoryFileName"))
        End If
    End Sub

    Private Sub sendEchoSign()
        Dim objResponse As HttpWebResponse = Nothing
        If (checkValue(HttpContext.Current.Request("PDFID"))) Then
            objResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/echosign/?AppID=" & AppID & "&intActionType=1&MediaCampaignID=" & MediaCampaignID & "&PDFID=" & HttpContext.Current.Request("PDFID"))
        ElseIf (checkValue(HttpContext.Current.Request("LetterID"))) Then
            objResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/echosign/?AppID=" & AppID & "&intActionType=1&MediaCampaignID=" & MediaCampaignID & "&LetterID=" & HttpContext.Current.Request("LetterID"))
        End If
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim arrResult As Array = Split(strResult, "|")
        If (arrResult.Length = 2) Then
            If (arrResult(0) = "1") Then
                strReturnUrlVariables = "strMessageType=success&strMessageTitle=Send+Document&strMessage=" & arrResult(1)
            Else
                strReturnUrlVariables = "strMessageType=error&strMessageTitle=Send+Document&strMessage=" & arrResult(1)
            End If
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Send+Document&strMessage=" & encodeURL("Failed to send document via EchoSign")
        End If
    End Sub

    Private Sub savePaymentHistoryActive()
        Dim strSQL As String = "UPDATE tblpaymenthistory SET PaymentHistoryStatus = 0 WHERE PaymentHistoryID = '" & HttpContext.Current.Request("PaymentHistoryID") & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strSQL)
    End Sub

    Public Sub sendProcessingEmail(ByVal CommunicationTemplateID As String, ByVal emailto As String, ByVal subject As String)
        Dim strEmailTo As String = replaceTags(emailto, AppID), strEmailSubject As String = replaceTags(subject, AppID), strEmailBody As String = "", strEmailTemplate As String = ""
        Dim strSQL As String = "SELECT EmailTemplateBody, CommunicationTemplateText FROM tblcommunicationtemplates INNER JOIN tblemailtemplates ON EmailTemplateID = CommunicationTemplateEmailTemplateID " & _
            "WHERE CommunicationTemplateID = '" & CommunicationTemplateID & "' AND tblcommunicationtemplates.CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strEmailTemplate = Row.Item("EmailTemplateBody")
                strEmailBody = replaceTags(Row.Item("CommunicationTemplateText"), AppID)
            Next
        End If
        dsCache = Nothing
        postEmail("", "ben.snaize@engaged-solutions.co.uk", strEmailSubject, Replace(strEmailTemplate, "xxx[Content]xxx", strEmailBody), True, "")
        saveNote(AppID, Config.DefaultUserID, "Email sent to <strong>" & strEmailTo & "</strong>: " & strEmailBody)
    End Sub

    Public Sub sendDetailedProcessingEmail(ByVal CommunicationTemplateID As String, ByVal emailto As String, ByVal subject As String, ByVal notes As String, ByVal documents As String)
        Dim strEmailTo As String = replaceTags(emailto, AppID), strEmailSubject As String = replaceTags(subject, AppID), strEmailBody As String = "", strAttachments As String = "", strEmailTemplate As String = ""
        Dim strSQL As String = "SELECT EmailTemplateBody, CommunicationTemplateText FROM tblcommunicationtemplates INNER JOIN tblemailtemplates ON EmailTemplateID = CommunicationTemplateEmailTemplateID " & _
            "WHERE CommunicationTemplateID = '" & CommunicationTemplateID & "' AND tblcommunicationtemplates.CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strEmailTemplate = Row.Item("EmailTemplateBody")
                strEmailBody = replaceTags(Row.Item("CommunicationTemplateText"), AppID)
            Next
        End If
        dsCache = Nothing
        saveNote(AppID, Config.DefaultUserID, "Email sent to <strong>" & strEmailTo & "</strong>: " & strEmailBody)
        If (documents = "Y") Then
            Dim strAttachmentsDescription As String = ""
            strSQL = "SELECT PDFHistoryFileName, PDFHistoryName, PDFHistoryNote FROM tblpdfhistory WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND NOT PDFHistoryFileName IS NULL AND PDFHistoryFileName <> ''"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strAttachmentsDescription += "<p><strong>File Attachments</strong></p>"
                Dim x As Integer = 0
                For Each Row As DataRow In dsCache.Rows
                    If (x = 0) Then
                        strAttachments += Row.Item("PDFHistoryFileName")
                    Else
                        strAttachments += "," & Row.Item("PDFHistoryFileName")
                    End If
                    strAttachmentsDescription += "<strong>" & Row.Item("PDFHistoryName") & "</strong> (" & Row.Item("PDFHistoryFileName") & ") - " & Row.Item("PDFHistoryNote") & "<br />"
                    x += 1
                Next
                strEmailBody += "<p>" & strAttachmentsDescription & "</p>"
            End If
            dsCache = Nothing
            Dim strInstructions As String = ""
            strSQL = "SELECT DiaryTypeDescription FROM tbldiaries WHERE AppID = '" & AppID & "' AND DiaryType = 'RepInstruction' AND DiaryActive = 1"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strInstructions += "<p><strong>Instructions</strong></p>"
                Dim x As Integer = 0
                For Each Row As DataRow In dsCache.Rows
                    strInstructions += x + 1 & ". " & Row.Item("DiaryTypeDescription") & "<br />"
                    x += 1
                Next
                strEmailBody += "<p>" & strInstructions & "</p>"
            End If
            dsCache = Nothing
        End If
        If (notes = "Y") Then
            Dim strNote As String = ""
            strSQL = "SELECT NTS.Note, NTS.CreatedDate, USR.UserFullName FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
               "WHERE NTS.CompanyID  = '" & CompanyID & "' AND AppID = '" & AppID & "' ORDER BY NTS.CreatedDate DESC"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strNote += "<p><strong>Case Notes</strong></p>"
                For Each Row As DataRow In dsCache.Rows
                    strNote += "<p style=""font-family: Calibri, Verdana, Arial; font-size: 12px;text-align: left; padding: 5px 5px 5px 5px;width: 70%;white-space: normal;""><span style=""color: #142B94;font-weight: bold;"">" & Row.Item("UserFullName") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span style=""color: #999999;"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</span></p>"
                Next
                strEmailBody += strNote
            End If
            dsCache = Nothing
        End If
        postEmail("", "ben.snaize@engaged-solutions.co.uk", strEmailSubject, Replace(strEmailTemplate, "xxx[Content]xxx", strEmailBody), True, strAttachments)
    End Sub

    Public Sub saveRepAppointment()
        Dim intEditAppID As String = HttpContext.Current.Request("frmEditAppID")
        Dim UserID As String = HttpContext.Current.Request("frmUserID")
        Dim strQry As String = ""
        If (checkValue(intEditAppID)) Then
            strQry = "UPDATE tbldiaries SET " & _
                "DiaryAssignedToUserID  = " & formatField(UserID, "N", getSystemUser()) & ", " & _
                "DiaryDueDate           = " & formatField(HttpContext.Current.Request("frmDiaryDueDate"), "DTTM", "NULL") & " " & _
                "WHERE AppID 	        = '" & intEditAppID & "' " & _
                "AND DiaryComplete      = 0 " & _
                "AND CompanyID          = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(intEditAppID, Config.DefaultUserID, "Rep appointment re-arranged to " & HttpContext.Current.Request("frmDiaryDueDate") & " with " & HttpContext.Current.Request("frmUserFullName"))
        Else
            Dim intRowsAffected As Integer = 0
            strQry = "UPDATE tbldiaries SET " & _
                "DiaryAssignedToUserID  = " & formatField(UserID, "N", getSystemUser()) & ", " & _
                "DiaryDueDate           = " & formatField(HttpContext.Current.Request("frmDiaryDueDate"), "DTTM", "NULL") & " " & _
                "WHERE AppID 	        = '" & AppID & "' " & _
                "AND DiaryType          = " & formatField(HttpContext.Current.Request("frmDiaryType"), "", HttpContext.Current.Request("frmDiaryType")) & " " & _
                "AND DiaryComplete      = 0 " & _
                "AND CompanyID          = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
            If (intRowsAffected = 0) Then
                Dim strDiaryTypeDescription As String = ""
                Dim strSQL As String = "SELECT DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeName = '" & HttpContext.Current.Request("frmDiaryType") & "' AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                    Next
                End If
                dsCache = Nothing
                strQry = "INSERT INTO tbldiaries(AppID, CompanyID, DiaryAssignedToUserID, DiaryDueDate, DiaryType, DiaryTypeDescription, DiaryLength) " & _
                    "VALUES(" & AppID & ", " & _
                    CompanyID & ", " & _
                    formatField(UserID, "N", getSystemUser()) & ", " & _
                    formatField(HttpContext.Current.Request("frmDiaryDueDate"), "DTTM", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("frmDiaryType"), "", HttpContext.Current.Request("frmDiaryType")) & ", " & _
                    formatField(strDiaryTypeDescription, "", "") & ", " & _
                    formatField(1, "N", 1) & ") "
                executeNonQuery(strQry)
            End If
            strQry = "UPDATE tblapplicationstatus SET RepUserID = " & formatField(UserID, "N", getSystemUser()) & " WHERE AppID	= '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(AppID, Config.DefaultUserID, "Rep appointment arranged for " & HttpContext.Current.Request("frmDiaryDueDate") & " with " & HttpContext.Current.Request("frmUserFullName"))
        End If
        strQry = "UPDATE tblapplicationstatus SET  " & _
            "CallCentreUserID		        = " & formatField(Config.DefaultUserID, "N", 0) & " " & _
            "WHERE AppID	                = '" & AppID & "' " & _
            "AND CallCentreUserID           = 0 " & _
            "AND CompanyID                  = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Public Sub saveRepHoliday()
        Dim intEditAppID As String = HttpContext.Current.Request("frmEditAppID")
        Dim UserID As String = HttpContext.Current.Request("frmUserID")
        Dim strQry As String = ""
        If (checkValue(intEditAppID)) Then
            strQry = "UPDATE tbldiaries SET " & _
                "DiaryAssignedToUserID  = " & formatField(UserID, "N", getSystemUser()) & ", " & _
                "DiaryDueDate           = " & formatField(HttpContext.Current.Request("frmDiaryDueDate"), "DTTM", "NULL") & " " & _
                "WHERE AppID 	        = '" & intEditAppID & "' " & _
                "AND DiaryComplete      = 0 " & _
                "AND CompanyID          = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(intEditAppID, Config.DefaultUserID, "Rep holiday re-arranged to " & HttpContext.Current.Request("frmDiaryDueDate") & " with " & HttpContext.Current.Request("frmUserFullName"))
        Else
            Dim strDiaryTypeDescription As String = ""
            Dim strSQL As String = "SELECT DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeName = '" & HttpContext.Current.Request("frmDiaryType") & "' AND CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                Next
            End If
            dsCache = Nothing
            strQry = "INSERT INTO tbldiaries(AppID, CompanyID, DiaryAssignedToUserID, DiaryDueDate, DiaryType, DiaryTypeDescription, DiaryLength) " & _
                "VALUES(" & AppID & ", " & _
                CompanyID & ", " & _
                formatField(UserID, "N", getSystemUser()) & ", " & _
                formatField(HttpContext.Current.Request("frmDiaryDueDate"), "DTTM", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("frmDiaryType"), "", HttpContext.Current.Request("frmDiaryType")) & ", " & _
                formatField(strDiaryTypeDescription, "", "") & ", " & _
                formatField(1, "N", 1) & ") "
            executeNonQuery(strQry)
            strQry = "UPDATE tblapplicationstatus SET RepUserID = " & formatField(UserID, "N", getSystemUser()) & " WHERE AppID	= '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            saveNote(AppID, Config.DefaultUserID, "Rep holiday arranged for " & HttpContext.Current.Request("frmDiaryDueDate") & " with " & HttpContext.Current.Request("frmUserFullName"))
        End If
        strQry = "UPDATE tblapplicationstatus SET  " & _
            "CallCentreUserID		        = " & formatField(Config.DefaultUserID, "N", 0) & " " & _
            "WHERE AppID	                = '" & AppID & "' " & _
            "AND CallCentreUserID           = 0 " & _
            "AND CompanyID                  = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Public Sub saveRepAppointmentComplete()
        Dim strQry As String = ""
        strQry = "UPDATE tbldiaries SET " & _
            "DiaryComplete            = " & formatField(1, "N", 1) & " " & _
            "WHERE AppID 	        = '" & AppID & "' " & _
            "AND DiaryType          = 'RepAppointment' " & _
            "AND DiaryComplete      = 0 " & _
            "AND CompanyID          = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Public Sub saveRepAppointmentCancel()
        Dim strQry As String = ""
        strQry = "UPDATE tbldiaries SET " & _
            "DiaryAssignedToUserID  = " & formatField(0, "N", 0) & " " & _
            "WHERE AppID 	        = '" & AppID & "' " & _
            "AND DiaryType          = 'RepAppointment' " & _
            "AND DiaryComplete      = 0 " & _
            "AND CompanyID          = '" & CompanyID & "'"
        executeNonQuery(strQry)
        strQry = "UPDATE tblapplicationstatus SET RepUserID = 0, CallCentreUserID = 0 WHERE AppID	= '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Public Sub cloneApplication(ByVal action As String)
        Dim intCloneAppID As String = "", ParentAppID As String = "", intAction As String = action
        ParentAppID = getAnyField("ParentAppID", "tblapplications", "AppID", AppID)
        If (ParentAppID <> 0) Then
            intCloneAppID = ParentAppID
        Else
            intCloneAppID = AppID
        End If
        Dim intCloneMediaCampaignID As String = HttpContext.Current.Request("frmCloneMediaCampaignID")
        Dim strProductType As String = getAnyField("MediaCampaignProductType", "tblmediacampaigns", "MediaCampaignID", intCloneMediaCampaignID)
        Dim strStatusCode As String = HttpContext.Current.Request("frmStatusCode")
        Dim intMediaCampaignIDOutbound As String = HttpContext.Current.Request("frmMediaCampaignIDOutbound")
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/inbound/cloneapplication.aspx", "Action=" & intAction & "&AppID=" & intCloneAppID & "&MediaCampaignID=" & MediaCampaignID & "&CloneMediaCampaignID=" & intCloneMediaCampaignID & "&ProductType=" & strProductType & "&frmStatusCode=" & strStatusCode & "&MediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&UserID=" & Config.DefaultUserID & "&ForceClone=Y&UserSessionID=" & UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponseText As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        Dim arrResponseText As Array = Split(strResponseText, "|")
        If (arrResponseText(0) = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Case+Clone+Success&strMessage=" & encodeURL("Case successfully cloned to " & strProductType & " - " & arrResponseText(1))
            'strReturnUrlMessage = "Case successfully cloned to " & strProductType & " - " & arrResponseText(1)
            'strReturnUrlMessageTitle = "Case Clone Success"
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Case+Clone+Failure&strMessage=" & encodeURL("Case not successfully cloned. Please correct the following fields: " & arrResponseText(1))
            'strReturnUrlMessage = "Case <strong>NOT</strong> successfully cloned. Please correct the following fields: " & arrResponseText(1)
            'strReturnUrlMessageTitle = "Case Clone Failure"
        End If
    End Sub

    Public Sub cloneApplicationByMediaCampaign(ByVal action As String, intCloneMediaCampaignID As String)
        Dim intCloneAppID As String = "", ParentAppID As String = "", intAction As String = action
        ParentAppID = getAnyField("ParentAppID", "tblapplications", "AppID", AppID)
        If (ParentAppID <> 0) Then
            intCloneAppID = ParentAppID
        Else
            intCloneAppID = AppID
        End If
        Dim strProductType As String = getAnyField("MediaCampaignProductType", "tblmediacampaigns", "MediaCampaignID", intCloneMediaCampaignID)
        Dim strStatusCode As String = HttpContext.Current.Request("frmStatusCode")
        Dim intMediaCampaignIDOutbound As String = HttpContext.Current.Request("frmMediaCampaignIDOutbound")
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/inbound/cloneapplication.aspx", "Action=" & intAction & "&AppID=" & intCloneAppID & "&MediaCampaignID=" & MediaCampaignID & "&CloneMediaCampaignID=" & intCloneMediaCampaignID & "&ProductType=" & strProductType & "&frmStatusCode=" & strStatusCode & "&MediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&UserID=" & Config.DefaultUserID & "&ForceClone=Y&UserSessionID=" & UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponseText As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        Dim arrResponseText As Array = Split(strResponseText, "|")
        If (arrResponseText(0) = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Case+Clone+Success&strMessage=" & encodeURL("Case successfully cloned to " & strProductType & " - " & arrResponseText(1))
            'strReturnUrlMessage = "Case successfully cloned to " & strProductType & " - " & arrResponseText(1)
            'strReturnUrlMessageTitle = "Case Clone Success"
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Case+Clone+Failure&strMessage=" & encodeURL("Case not successfully cloned. Please correct the following fields: " & arrResponseText(1))
            'strReturnUrlMessage = "Case <strong>NOT</strong> successfully cloned. Please correct the following fields: " & arrResponseText(1)
            'strReturnUrlMessageTitle = "Case Clone Failure"
        End If
    End Sub

    Public Sub returnLead()
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignScheduleID FROM tblmediacampaignschedules WHERE MediaCampaignID = '" & getAnyField("MediaCampaignIDOutbound", "tblapplications", "AppID", AppID) & "' " & _
            "AND MediaCampaignScheduleYear = " & Config.DefaultDateTime.Year & " " & _
            "AND MediaCampaignScheduleWeek = " & CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(Config.DefaultDateTime, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday) & " " & _
            "AND MediaCampaignScheduleWeekDay = " & Weekday(Config.DefaultDateTime, Microsoft.VisualBasic.FirstDayOfWeek.Sunday) & " " & _
            "AND MediaCampaignScheduleStartHour <= " & Config.DefaultDateTime.Hour & " " & _
            "AND MediaCampaignScheduleEndHour >= " & Config.DefaultDateTime.Hour & " ORDER BY MediaCampaignScheduleDelivered DESC"

        Dim intMediaCampaignScheduleID As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        If (checkValue(intMediaCampaignScheduleID)) Then
            incrementField("MediaCampaignScheduleID", intMediaCampaignScheduleID, "MediaCampaignScheduleDelivered", "tblmediacampaignschedules", -1)
            updateSingleDatabaseField(AppID, "tblapplications", "MediaCampaignCostOutbound", "N", 0, 0)
        End If
    End Sub

    Public Sub previousStatusCode()
        Dim strPrevStatusCode As String = "", strPrevSubStatusCode As String = ""
        Dim strSQL As String = "SELECT TOP 1 StatusHistoryStatusCode, StatusHistorySubStatusCode FROM tblstatushistory WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "' AND StatusHistoryStatusCode <> 'COH' AND StatusHistoryStatusCode <> 'PEN' ORDER BY StatusHistoryDate DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strPrevStatusCode = Row.Item("StatusHistoryStatusCode").ToString
                strPrevSubStatusCode = Row.Item("StatusHistorySubStatusCode").ToString
            Next
        End If
        dsCache = Nothing
        If (checkValue(strPrevStatusCode)) Then
            saveStatus(AppID, Config.DefaultUserID, strPrevStatusCode, strPrevSubStatusCode)
        End If
    End Sub

    Public Sub manualPayout()
'        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/scheduled/payoutloans.aspx?frmMode=1&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
'        Dim objReader As New StreamReader(objResponse.GetResponseStream())
'        Dim strResult As String = objReader.ReadToEnd()
'        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = ""
'        If (checkValue(strResult)) Then
'            objXML.LoadXml(strResult)
'            strStatus = objXML.SelectSingleNode("//Status").InnerText
'            strMessage = objXML.SelectSingleNode("//Message").InnerText
'        End If
		strStatus = "PENDING"
        If (strStatus = "PENDING") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Faster+Payment+Request&strMessage=" & encodeURL("Faster Payment requested")
            'strReturnUrlMessageTitle = "Faster Payment Request"
            'strReturnUrlMessage = "Faster Payment requested - " & strMessage
            'Dim strAssignUserID As String = getAnyField("MediaCampaignAssignUserID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
            'If (checkValue(strAssignUserID)) Then
            '    Dim strQry As String = "SELECT TOP 1 UserID FROM tblusers WHERE UserID IN(" & strAssignUserID & ") AND UserCallCentre = 1 AND UserSuperAdmin = 0 AND UserActive = 1 AND UserLockedOut = 0 AND CompanyID = " & CompanyID & " ORDER BY (SELECT COUNT(AppID) AS Expr1 FROM dbo.tblapplicationstatus INNER JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = tblapplicationstatus.AppID AND ApplicationStatusDateName = 'PaymentRequested' WHERE (CallCentreUserID = dbo.tblusers.UserID) AND (DATEDIFF(DD, ApplicationStatusDate, GETDATE()) = 0))"
            '    Dim strResult2 As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            '    executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = " & formatField(strResult2, "N", 0) & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            'End If
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Faster+Payment+Success&strMessage=" & encodeURL("Faster Payment not successfully requested")
            'strReturnUrlMessageTitle = "Faster Payment Request"
            'strReturnUrlMessage = "Faster Payment not successfully requested"
        End If
    End Sub

    Public Sub manualPayment()
        Dim intAmount As String = HttpContext.Current.Request("ManualPaymentAmount")
        Dim intRepayment As String = HttpContext.Current.Request("UnderwritingRepaymentFees")
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(intAmount, "M", 0) & ", " & _
                        formatField("Repayment", "", "") & ", " & _
                        formatField("", "", "") & ", " & _
                        formatField(1, "B", 0) & ") "
        executeNonQuery(strQry)
        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
        If (IsNumeric(strValue)) Then
            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + " & CDbl(intAmount) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strUpdate)
        End If
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Manual+Payment&strMessage=" & encodeURL("Payment has been successfully recorded")
        'strReturnUrlMessageTitle = "Manual Payment"
        'strReturnUrlMessage = "Payment has been successfully recorded"
        If (Not checkValue(intRepayment)) Then
            intRepayment = 0
        End If
        If (CInt(intAmount) = CInt(intRepayment)) Then
            saveStatus(AppID, Config.DefaultUserID, "SET", "")
            saveUpdatedDate(AppID, Config.DefaultUserID, "PaymentReceived")
        Else
            ' Logic to settle part payments here
            saveStatus(AppID, Config.DefaultUserID, "PPY", "")
            saveUpdatedDate(AppID, Config.DefaultUserID, "PartPayment")
        End If
    End Sub

    Public Sub manualPlanPayment()
        Dim intAmount As String = HttpContext.Current.Request("ManualPaymentAmount")
        Dim intRepayment As String = HttpContext.Current.Request("UnderwritingRepaymentFees")
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(intAmount, "M", 0) & ", " & _
                        formatField("Plan Payment", "", "") & ", " & _
                        formatField("", "", "") & ", " & _
                        formatField(1, "B", 0) & ") "
        executeNonQuery(strQry)
        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
        If (IsNumeric(strValue)) Then
            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + " & CDbl(intAmount) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strUpdate)
        End If
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Plan+Payment&Request=" & encodeURL("Payment has been successfully recorded")
        'strReturnUrlMessageTitle = "Plan Payment"
        'strReturnUrlMessage = "Payment to plan has been successfully recorded"
    End Sub

    Public Sub manualIdentityCheck()
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/experian/idhub/?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
            strMessage = objXML.SelectSingleNode("//Reason").InnerText
        End If
        If (strStatus = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Identity+Check&strMessage=" & encodeURL("Identity check result - " & strMessage)
            'strReturnUrlMessageTitle = "Identity Check"
            'strReturnUrlMessage = "Identity check result - " & strMessage
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Identity+Check&strMessage=" & encodeURL("Identity check failed")
            'strReturnUrlMessageTitle = "Identity Check"
            'strReturnUrlMessage = "Identity check failed"
        End If
    End Sub

    Public Sub manualCreditCheck()
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/experian/scems/?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
            strMessage = objXML.SelectSingleNode("//CreditScore").InnerText
        End If
        If (strStatus = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Credit+Check&strMessage=" & encodeURL("Credit check result - " & strMessage)
            'strReturnUrlMessageTitle = "Credit Check"
            'strReturnUrlMessage = "Credit check result - " & strMessage
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Credit+Check&strMessage=" & encodeURL("Credit check unavailable")
            'strReturnUrlMessageTitle = "Credit Check"
            'strReturnUrlMessage = "Credit check unavailable"
        End If
    End Sub

    Public Sub manualPaymentDip()
        Dim intAmount As String = HttpContext.Current.Request("ManualPaymentAmount")
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/scheduled/takepayments.aspx?frmMode=3&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&frmAmount=" & intAmount & "&UserSessionID=" & Config.UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
            strMessage = objXML.SelectSingleNode("//Message").InnerText
        End If
        If (strStatus = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Payment+Request&strMessage=" & encodeURL("Payment request successful")
            'strReturnUrlMessageTitle = "Payment Request"
            'strReturnUrlMessage = "Payment request successful"
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Payment+Request&strMessage=" & encodeURL("Payment request unsuccessful - " & strMessage)
            'strReturnUrlMessageTitle = "Payment Request"
            'strReturnUrlMessage = "Payment request unsuccessful - " & strMessage
        End If
    End Sub

    Public Sub downSell()
        executeNonQuery("UPDATE tblapplications SET Amount = (Amount - 50) WHERE Amount >= 100 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
        executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & AppID & "'")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Downsell&strMessage=" & encodeURL("Downsell made - please view lending plans for outcome")
        'strReturnUrlMessageTitle = "Downsell"
        'strReturnUrlMessage = "Downsell made - please view lending plans for outcome"
    End Sub

    Public Sub calcRepayment()
        executeNonQuery("EXECUTE spcalculaterepayment @TheAppID = '" & AppID & "'")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Repayment+Updated&strMessage=" & encodeURL("Repayment and APR successfully updated")
        'strReturnUrlMessageTitle = "Repayment Updated"
        'strReturnUrlMessage = "Repayment and APR successfully updated"
    End Sub

    Public Sub recalcInterest()
        executeNonQuery("EXECUTE spfixdailyinterest @TheAppID = '" & AppID & "'")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Interest+Re-Calculated&strMessage=" & encodeURL("Interest successfully re-calculated")
        'strReturnUrlMessageTitle = "Repayment Updated"
        'strReturnUrlMessage = "Repayment and APR successfully updated"
    End Sub

    Public Sub applyExtension()
        Dim intAmount As String = HttpContext.Current.Request("ManualPaymentAmount")
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(intAmount, "M", 0) & ", " & _
                        formatField("Extension Fee", "", "") & ", " & _
                        formatField("", "", "") & ", " & _
                        formatField(1, "B", 0) & ") "
        executeNonQuery(strQry)
        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
        If (IsNumeric(strValue)) Then
            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + " & CDbl(intAmount) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strUpdate)
        End If
        executeNonQuery("EXECUTE spcalculateextensionapr @TheAppID = '" & AppID & "'")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Extension&strMessage=" & encodeURL("Extension has been successfully applied")
        'strReturnUrlMessageTitle = "Extension"
        'strReturnUrlMessage = "Extension has been successfully applied"
    End Sub

    Public Sub cancelExtension()
        Dim intFee As String = getAnyFieldFromDataStore("ExtensionFee", AppID)
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                        "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(-CDbl(intFee), "M", 0) & ", " & _
                        formatField("Void Extension Fee", "", "") & ", " & _
                        formatField("", "", "") & ", " & _
                        formatField(1, "B", 0) & ") "
        executeNonQuery(strQry)
        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
        If (IsNumeric(strValue)) Then
            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) - " & CDbl(intFee) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strUpdate)
        End If
        clearUpdatedDate(AppID, "ExtensionPaymentDue")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Extension&strMessage=" & encodeURL("Extension has been successfully cancelled")
        'strReturnUrlMessageTitle = "Extension"
        'strReturnUrlMessage = "Extension has been successfully cancelled"
    End Sub

    Public Sub emailDocuments(ByVal emailaddress As String, ByVal id As String, ByVal emailsubject As String, ByVal emailbody As String)
        Dim strEmailAttachments As String = ""
        Dim arrAttachments As Array = Split(id, ";")
        For x As Integer = 0 To UBound(arrAttachments)
            Dim strURL As String = ""
            If (Left(arrAttachments(x), 1) = "P") Then
                strURL = "/letters/generatepdf.aspx?AppID=" & AppID & "&PDFID=" & Replace(arrAttachments(x), "P", "") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID
            ElseIf (Left(arrAttachments(x), 1) = "L") Then
                strURL = "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & Replace(arrAttachments(x), "L", "") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID
            End If
            Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & strURL)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponseText As String = objReader.ReadToEnd()
            If (x = 0) Then
                strEmailAttachments += strResponseText
            Else
                strEmailAttachments += "," & strResponseText
            End If
            objReader.Close()
            objReader = Nothing
        Next
        postEmail("", emailaddress, replaceTags(emailsubject, AppID), replaceTags(emailbody, AppID), True, strEmailAttachments)
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Email+Successfully+Sent&strMessage=" & encodeURL("Complaint form email sent to " & emailaddress)
        'strReturnUrlMessageTitle = "Email Successfully Sent"
        'strReturnUrlMessage = "Complaint form email sent to " & emailaddress
    End Sub

    Public Sub reportAffiliatePayout()
        Dim strURL As String = lookupValue(MediaCampaignID, "LifeboatLoans", "tblaffiliates")
        If (checkValue(strURL)) Then
            Dim objResponse As HttpWebResponse = getWebRequest(strURL & AppID)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResponse As String = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing
            objResponse = Nothing
        End If
    End Sub

    Public Sub noticeOfDefault()
        Dim strQry As String = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference, PaymentHistoryStatus) " & _
                "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                formatField(CompanyID, "N", 0) & ", " & _
                formatField(-CDbl(25), "M", 0) & ", " & _
                formatField("Notice Of Default Fee", "", "") & ", " & _
                formatField("", "", "") & ", " & _
                formatField(1, "B", 0) & ") "
        executeNonQuery(strQry)
        Dim strValue As String = getAnyFieldFromDataStore("CurrentBalance", AppID)
        If (IsNumeric(strValue)) Then
            Dim strUpdate As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + " & -CDbl(25) & " WHERE StoredDataName = 'CurrentBalance' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strUpdate)
        End If
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Notice+Of&Default=" & encodeURL("NOD issued, balance updated")
    End Sub

    Public Sub quidMarketCardAuth()
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/pacnet/takepayments.aspx?frmMode=1&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = "", strTracking As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
            strMessage = objXML.SelectSingleNode("//Message").InnerText
            strTracking = objXML.SelectSingleNode("//Tracking").InnerText
        End If
        If (strStatus = "1") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Card+Pre-Auth+Request&strMessage=" & encodeURL("Card Pre-Auth approved: token #" & strTracking)
            schedulePayments()
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Card+Pre-Auth+Request&strMessage=" & encodeURL("Card Pre-Auth not approved")
        End If
        objResponse = Nothing
    End Sub

    Public Sub quidMarketPayout()
        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/pacnet/payoutloans.aspx?frmMode=1&AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResult As String = objReader.ReadToEnd()
        Dim objXML As XmlDocument = New XmlDocument
        Dim strStatus As String = "", strMessage As String = ""
        If (checkValue(strResult)) Then
            objXML.LoadXml(strResult)
            strStatus = objXML.SelectSingleNode("//Status").InnerText
            strMessage = objXML.SelectSingleNode("//Message").InnerText
        End If
        If (strStatus = "PENDING") Then
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Faster+Payment+Request&strMessage=" & encodeURL("Faster Payment requested")
            schedulePayments()
        Else
            strReturnUrlVariables = "strMessageType=error&strMessageTitle=Faster+Payment+Success&strMessage=" & encodeURL("Faster Payment not successfully requested")
        End If
        objResponse = Nothing
    End Sub

    Public Sub schedulePayments()

        Dim strFirstPayment As String = getDateField(AppID, "FirstPaymentDue")
        Dim intFirstPayment As String = getAnyFieldFromDataStore("UnderwritingFirstMonthlyPayment", AppID)
        Dim intMonthlyPayment As String = getAnyFieldFromDataStore("UnderwritingRegularMonthlyPayment", AppID)
        If (checkValue(strFirstPayment) And checkValue(intFirstPayment) And checkValue(intMonthlyPayment)) Then
            Dim dteFirstPayment As Date = CDate(strFirstPayment)
            insertPayment(AppID, CDec(intFirstPayment), "Loan Repayment", 2, dteFirstPayment)
            Dim strFrequency As String = getAnyFieldFromDataStore("PaymentFrequency", AppID)
            Dim intTerm As String = getAnyFieldByCompanyID("ProductTerm", "tblapplications", "AppID", AppID)
            If (checkValue(intTerm)) Then
                For x As Integer = 1 To CInt(intTerm) - 1
                    If (strFrequency = "Monthly") Then
                        Dim dteNextPayment As Date = DateAdd(DateInterval.Month, x, dteFirstPayment)
                        insertPayment(AppID, CDec(intMonthlyPayment), "Loan Repayment", 2, dteNextPayment)
                    ElseIf (strFrequency = "Weekly") Then
                        Dim dteNextPayment As Date = DateAdd(DateInterval.WeekOfYear, x, dteFirstPayment)
                        insertPayment(AppID, CDec(intMonthlyPayment), "Loan Repayment", 2, dteNextPayment)
                    End If
                Next
            End If
        End If

    End Sub

    Public Sub savePaymentPlan()

        Dim arrPlanAmounts As String() = split(getAnyFieldFromDataStore("PaymentPlanAmount", AppID),",")
        Dim arrPlanDates As String() = split(getAnyFieldFromDataStore("PaymentPlanRepaymentDate", AppID),",")
		Dim paymentType As String = getAnyFieldFromDataStore("PaymentPlanPaymentMethod", AppID)
		
		
		
		 If (checkValue(paymentType)) Then
            Select Case paymentType		
                    
                Case "DIRECT DEBIT"
                 paymentType = "1"
                Case "DEBIT CARD"	
				 paymentType = "2"
				Case Else
				 paymentType = "4"
                    
            End Select
		
		End IF
		
	
        Dim counter As integer = getAnyFieldFromDataStore("PaymentPlanRepayments", AppID) - 1
        Dim x As Integer = 0

        Dim strSQL As String = "Update tblpaymenthistory Set paymentHistoryStatus = 4 WHERE AppID = " & AppID & " and PaymentHistoryDate >= " & Today & ""
        executeNonQuery(strSQL)

        Dim strSQL2 As String = ""	
	

        While x <= counter

            strSQL2 = "INSERT INTO tblpaymenthistory (AppID, CompanyID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryReference,PaymentHistoryDate, PaymentHistoryCreated,PaymentHistoryMethod, PaymentHistoryStatus) " & _
                "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                formatField(CompanyID, "N", 0) & ", " & _
                formatField(CDbl(arrPlanAmounts(x)), "M", 0) & ", " & _
                formatField("Plan Payment", "", "") & ", " & _
                formatField("", "", "") & ", " & _
                formatField(arrPlanDates(x) & " 00:00:00" , "DTTM", "") & ", " & _
                formatField(Today, "DTTM", "") & ", " & _
				formatField(paymentType, "N", 4) & ", " & _
                formatField(0, "B", 0) & ") "
								
            executeNonQuery(strSQL2)
            x = x + 1
        End While
    End Sub
	
	Public Sub sendBrokerLogin()

        Dim strSQL As String = "SELECT dbo.tblusers.*FROM dbo.tblbusinessobjects INNER JOIN dbo.tblbusinessobjects AS tblbusinessobjects_1 ON dbo.tblbusinessobjects.BusinessObjectParentClientID = tblbusinessobjects_1.BusinessObjectID INNER JOIN dbo.tblusers ON dbo.tblbusinessobjects.BusinessObjectID = dbo.tblusers.UserClientID WHERE (tblbusinessobjects_1.BusinessObjectID =" & BusinessObjectID & ") AND (dbo.tblbusinessobjects.BusinessObjectTypeID = 11)"

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("UserEmailAddress").ToString)) Then
                    Dim strNewPassword As String = generatePassword(8)
                    Dim strUsername As String = Row.Item("Username")
                    Dim strQry2 As String = "UPDATE tblusers SET " & _
                                        "UserPassword			= '" & hashString256(LCase(strUsername), strNewPassword) & "', " & _
                                        "UserPasswordChangeDate	= " & formatField(Config.DefaultDateTime.AddMinutes(-5), "DTTM", DefaultDateTime) & " " & _
                                        "WHERE UserID 		    = " & Row.Item("userID") & " " & _
                                        "AND CompanyID          = " & CompanyID & ""
                    executeNonQuery(strQry2)
					
					dim strEmailHTML as string = "<html><body leftmargin=""0"" marginwidth=""0"" topmargin=""0"" marginheight=""0"" offset=""0"" style=""background-color: #E1E1E1;"" ><table width=""100%"" cellpadding=""0"" cellspacing=""0"" ><tr><td valign=""top"" align=""left""><table width=""648"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><img src=""http://positive.engagedcrm.co.uk/img/positive.jpg"" /></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td valign=""top"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 10px 30px;""> <font face='Calibri, Verdana, Arial' size='2' color='#000000'><br /><h2>Welcome To 360 Loan Sourcing</h2><p>The team at Positive Lending have confirmed your registration details and have accepted your application to submit leads to Positive Lending. Please find your login details below.</p><p>Username: <strong>" & strUserName & "</strong></p><p>Password: <strong>" & strNewPassword & "</strong></p><a href=""360loansourcing.co.uk"">360loansourcing.co.uk</a><br /><p>Regards,</p><p>Positive Lending</p>Arena Business Centre<br />9 Nimrod Way,<br />Ferndown Industrial Estate,<br />Ferndown<br />Dorset<br />BH21 7UH<br /><img src=""http://positive.engagedcrm.co.uk/img/positive.jpg"" width=""150"" height=""74"" /></font></div></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td bgcolor=""#F6F7F7""><hr style=""border: 1px solid #CCCCCC; width:100%; margin:5px""></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 0px 30px;""><font face=""Calibri, Verdana, Arial"" size=""2"" color=""#999999""><p>This private and confidential e-mail has been sent to you by Positive Lending (UK) Limited.  Registered office: 41 Freemantle Road, Southampton, Hampshire SO51 6AS .  Positive Lending (UK) Limited is a licensed credit broker under the Consumer Credit Act (license no 622042) and is a company registered in England reg no 6700848.  Positive Lending (UK) Limited trading as Positive Lending is authorised and regulated by the Financial Conduct Authority, number 607682</p><p>This e-mail is confidential and for use by the addressee only. If you are not the intended recipient of this e-mail and have received it in error, please return the message to the sender by replying to it and then delete it from your mailbox. Internet e-mails are not necessarily secure. Positive Lending (UK) Limited will not accept responsibility for changes made to this message after it was sent.</p><p>Whilst all reasonable care has been taken to avoid the transmission of viruses, it is the responsibility of the recipient to ensure that the onward transmission, opening or use of this message and any attachments will not adversely affect its systems or data. No responsibility is accepted by Positive Lending (UK) Limited in this regard and the recipient should carry out such virus and other checks as it considers appropriate.</p><p>This communication does not create or modify any contract.</p><p>If you would prefer not to receive information about other products and services from us, please send an email to ask@positivelending.co.uk with 'opt-out' in the subject line.</p></font><br /></div></td><td align=""center"">&nbsp;</td></tr><tr><td width=""648"" height=""32"" colspan=""3"" align=""center"">&nbsp;</td></tr></table></td></tr></table></body></html>"
					
			
                    postEmail("", Row.Item("UserEmailAddress").ToString, "360 Loan Sourcing Logons",strEmailHTML, True, "", True)
				End If
                	strReturnUrlVariables = "strMessageType=sucess&strMessageTitle=Registration+Complete&strMessage="& encodeURL("Email+Send+Successful")

            Next
		Else
				  strReturnUrlVariables = "strMessageType=sucess&strMessageTitle=Registration+Failed&strMessage="& encodeURL("Email+unsuccesfully+sent")

        End If
    End Sub


    '*******************************************************
    ' Execute any method from this class
    '*******************************************************
    Shared Sub executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array)
        Dim objMethodType As Type = GetType(ProcessingSave)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        objMethodInfo.Invoke(inst, params)
    End Sub

End Class