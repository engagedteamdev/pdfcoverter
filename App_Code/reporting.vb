﻿Imports Config, Common, CommonDropdowns
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Collections

' ** Revision history **
'
' 03/10/2012	- Added Supplier/Rep check to Case History
' ** End Revision History **

Public Class Reporting

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Public strTitle As String = HttpContext.Current.Request("frmTitle")
    Public strInformation As String = HttpContext.Current.Request("frmInformation")
    Private strClass As String = "row1"
    Public dteStartDate As String = HttpContext.Current.Request("frmStartDate")
    Public dteEndDate As String = HttpContext.Current.Request("frmEndDate")
    Private dteAdditionalStartDate As String = HttpContext.Current.Request("frmAdditionalStartDate")
    Private dteAdditionalEndDate As String = HttpContext.Current.Request("frmAdditionalEndDate")
    Private strExcludeDate As String = HttpContext.Current.Request("frmExcludeDate")
    Private intAppID As String = HttpContext.Current.Request("frmAppID")
    Private strSurname As String = HttpContext.Current.Request("frmSurname")
    Public dteDOB As String = HttpContext.Current.Request("frmDOB")
    Private strTelephoneNumber As String = HttpContext.Current.Request("frmTelephoneNumber")
    Private strEmailAddress As String = HttpContext.Current.Request("frmEmailAddress")
    Private strPostCode As String = HttpContext.Current.Request("frmPostCode")
    Private strAnySearch As String = Trim(HttpContext.Current.Request("frmAnySearch"))
    Private intMediaIDInbound As String = HttpContext.Current.Request("frmMediaIDInbound")
    Private intMediaIDOutbound As String = HttpContext.Current.Request("frmMediaIDOutbound")
    Private intPurchaseMediaID As String = HttpContext.Current.Request("frmPurchaseMediaID")
    Public intMediaCampaignIDInbound As String = HttpContext.Current.Request("frmMediaCampaignIDInbound")
    Private intMediaCampaignIDOutbound As String = HttpContext.Current.Request("frmMediaCampaignIDOutbound")
    Private intMediaCampaignDialerGradeID As String = HttpContext.Current.Request("frmMediaCampaignDialerGrade")
    Private intMediaCampaignApplicationTemplateID As String = HttpContext.Current.Request("frmMediaCampaignApplicationTemplateID")
    Private strDateType As String = HttpContext.Current.Request("frmDateType")
    Private strAdditionalDateType As String = HttpContext.Current.Request("frmAdditionalDateType")
    Private strStatusCode As String = HttpContext.Current.Request("frmStatusCode")
    Private strSubStatusCode As String = HttpContext.Current.Request("frmSubStatusCode")
    Private strExcludeStatusCode As String = HttpContext.Current.Request("frmExcludeStatusCode")
    Private strMediaCampaignProductType As String = HttpContext.Current.Request("frmMediaCampaignProductType")
    Private strProductType As String = HttpContext.Current.Request("frmProductType")
    Private strDiaryType As String = HttpContext.Current.Request("frmDiaryType")
    Public strReportType As String = HttpContext.Current.Request("frmReportType")
    Public strCallCentreUser As String = HttpContext.Current.Request("frmCallCentreUser")
    Public strCallCentreTeam As String = HttpContext.Current.Request("frmCallCentreTeam")
    Public strSalesUser As String = HttpContext.Current.Request("frmSalesUser")
    Public strSalesTeam As String = HttpContext.Current.Request("frmSalesTeam")
    Public strAdministratorUser As String = HttpContext.Current.Request("frmAdministratorUser")
    Public strAdministratorTeam As String = HttpContext.Current.Request("frmAdministratorTeam")
    Public strRepUser As String = HttpContext.Current.Request("frmRepUser")
    Public strList As String = HttpContext.Current.Request("frmList")
    Public strGroup As String = HttpContext.Current.Request("frmGroup")
    Public strReportGroupType As String = HttpContext.Current.Request("frmReportGroupType")
    Public strReportGroupBy As String = HttpContext.Current.Request("frmReportGroupBy")
    Public strReportOrderBy As String = HttpContext.Current.Request("frmReportOrderBy")
    Private strShowTransferred As String = HttpContext.Current.Request("frmShowTransferred")
    Public strSearch As String = HttpContext.Current.Request("frmSearch")
    Private intSelectTop As String = HttpContext.Current.Request("frmSelectTop")
    Private intMinDialAttempts As String = HttpContext.Current.Request("frmMinDialAttempts"), intMaxDialAttempts As String = HttpContext.Current.Request("frmMaxDialAttempts")
    Private intMinAmount As String = HttpContext.Current.Request("frmMinAmount"), intMaxAmount As String = HttpContext.Current.Request("frmMaxAmount")
    Private intWorkflowID As String = HttpContext.Current.Request("frmWorkflowID")
    Private intReportBuilderID As String = HttpContext.Current.Request("frmReportBuilderID")
    Private intCustID As String = HttpContext.Current.Request("frmCustID")
    Private intClientID As String = HttpContext.Current.Request("frmClientID")
    Private intClientContactID As String = HttpContext.Current.Request("frmClientContactID")	
	Private strdatastorename As String = HttpContext.Current.Request("frmdatstorename")
    Private strdatastorevalue As String = HttpContext.Current.Request("frmdatastorevalue")
    Private intNetworkID As String = HttpContext.Current.Request("frmNetworkID")	
    Private strWorkflowQuery As String = "", strWorkflowOrderBy As String = "", strWorkflowToleranceFactor As String = ""
    Private arrReportBuilderFilterAccessTemp As String() = {}, arrReportBuilderFilterNamesTemp As String() = {}, arrReportBuilderFilterAccess As String() = {}, arrReportBuilderFilterNames As String() = {}
    Private arrReportBuilderColumnAccessTemp As String() = {}, arrReportBuilderColumnNamesTemp As String() = {}, arrReportBuilderColumnSortTemp As String() = {}, arrReportBuilderColumnValuesTemp As String() = {}, arrReportBuilderColumnFormatsTemp As String() = {}, arrReportBuilderColumnTotalsTemp As String() = {}, arrReportBuilderColumnAccess As String() = {}
    Private arrReportBuilderColumnNames As String() = {}, arrReportBuilderColumnSort As String() = {}, arrReportBuilderColumnValues As String() = {}, arrReportBuilderColumnFormats As String() = {}, arrReportBuilderViewColumnValues As String() = {}, arrReportBuilderDataStoreColumnValues As String() = {}, arrReportBuilderStatusDateColumnValues As String() = {}, arrReportBuilderColumnTotals As String() = {}
    Private arrReportBuilderClickThrough As String() = {}, arrReportBuilderClickThroughTemp As String() = {}, arrReportBuilderClickThroughQueryString As String() = {}, arrReportBuilderClickThroughQueryStringTemp As String() = {}
    Public strReportBuilderJavascriptFunctions As String = ""
    Private intRowsCount As Integer = 0
    Private boolView As Boolean = False, boolDataStore As Boolean = False, boolStatusDate As Boolean = False, boolStatusDateSearch As Boolean = False, boolStatusAdditionalDateSearch As Boolean = False, boolStatusExcludeDateSearch As Boolean = False
    Private arrTotals As Decimal() = {}
    Private intColumn As Integer = 0
    Public strReportSQL As String = ""
    Private intCachedMinutes As String = HttpContext.Current.Request("frmCachedMinutes"), strMode As String = HttpContext.Current.Request("frmMode"), strDaily As String = HttpContext.Current.Request("frmDaily"), strWeekly As String = HttpContext.Current.Request("frmWeekly"), strDailySummary As String = HttpContext.Current.Request("frmDailySummary")
    Private strAdhocFilter As String = HttpContext.Current.Request("frmAdhocFilter"), strAdhocQuery As String = HttpContext.Current.Request("frmAdhocQuery"), strSearchAdhoc As String = HttpContext.Current.Request("frmSearchAdhoc")
    Private strNoLogging As String = HttpContext.Current.Request("frmNoLogging"), strNoDateJoin As String = HttpContext.Current.Request("frmNoDateJoin"), strNoDataStoreJoin As String = HttpContext.Current.Request("frmNoDataStoreJoin")

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Sub New()
        If (Not checkValue(dteStartDate)) Then
            dteStartDate = Config.DefaultDate
        Else
            dteStartDate = dateTags(dteStartDate)
        End If
        If (Not checkValue(dteEndDate)) Then
            dteEndDate = Config.DefaultDate
        Else
            dteEndDate = dateTags(dteEndDate)
        End If
        strAnySearch = Replace(strAnySearch, "'", "")
        If (checkValue(strAnySearch)) Then
            Dim arrAnySearch As Array = Split(strAnySearch, ",")
            For x As Integer = 0 To UBound(arrAnySearch)
                If (regexTest("^[0-9]{7}$", arrAnySearch(x))) Then
                    If (checkValue(intAppID)) Then
                        intAppID += "," & arrAnySearch(x)
                    Else
                        intAppID = arrAnySearch(x)
                    End If
                ElseIf (regexTest("^[0-9 ]{1,13}$", arrAnySearch(x))) Then
                    strTelephoneNumber = arrAnySearch(x)
                ElseIf (InStr(arrAnySearch(x), "/") > 0) Then
                    dteDOB = arrAnySearch(x)
                ElseIf (InStr(arrAnySearch(x), "@") > 0) Then
                    strEmailAddress = arrAnySearch(x)
                ElseIf (regexCount("^([A-Za-z]{1,2})([0-9]{1})", arrAnySearch(x))) Then
                    strPostCode = arrAnySearch(x)
                ElseIf (regexCount("^[a-z' -‘]{3,50}$", arrAnySearch(x))) Then
                    strSurname = arrAnySearch(x)
                End If
            Next
        End If
        If (Not checkValue(intSelectTop)) Then
            intSelectTop = "500"
        End If
    End Sub

    Public Sub getFilterValues()
        Dim arrAdminLevels As String() = {"UserReports", "UserWorkflows", "UserManager", "UserSeniorManager", "UserCallCentre", "UserSales", "UserAdministrator", "UserRep", "Supplier", "Partner"}
        Dim arrAdminAccess As String() = {LeadPlatform.Config.Reports, LeadPlatform.Config.Workflows, LeadPlatform.Config.Manager, LeadPlatform.Config.SeniorManager, LeadPlatform.Config.CallCentre, LeadPlatform.Config.Sales, LeadPlatform.Config.Administrator, LeadPlatform.Config.Rep, LeadPlatform.Config.Supplier, LeadPlatform.Config.Partner}
        If Not checkValue(intReportBuilderID) Then
            If (strReportType = "Search") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderSearchID
            ElseIf (strReportType = "Diary") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderDiaryID
            ElseIf (strReportType = "Status") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderStatusSummaryID
            ElseIf (strReportType = "CaseHistory") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderCaseHistoryID
            Else
                Dim intWorkflowReportBuilderID As String = getAnyFieldCached(CacheObject, "WorkflowReportBuilderID", "tblworkflows", "WorkflowID", intWorkflowID)
                If checkValue(intWorkflowReportBuilderID) Then
                    intReportBuilderID = intWorkflowReportBuilderID
                Else
                    intReportBuilderID = LeadPlatform.Config.ReportBuilderID
                End If
            End If
        End If
        Dim intColumns As Integer = 0
        Dim strSQL As String = "SELECT TOP(1) ReportBuilderFilterAccess, ReportBuilderFilterNames FROM tblreportbuilder WHERE CompanyID = '" & CompanyID & "' AND ReportBuilderID = '" & intReportBuilderID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                arrReportBuilderFilterAccessTemp = Split(Row.Item("ReportBuilderFilterAccess").ToString, "|")
                arrReportBuilderFilterNamesTemp = Split(Row.Item("ReportBuilderFilterNames").ToString, "|")
            Next
            For x As Integer = 0 To UBound(arrReportBuilderFilterAccessTemp)
                For a As Integer = 0 To UBound(arrAdminLevels)
                    If ((arrAdminAccess(a) = True) And InStr(arrReportBuilderFilterAccessTemp(x), arrAdminLevels(a))) Or (arrReportBuilderFilterAccessTemp(x) = "") Then
                        ReDim Preserve arrReportBuilderFilterNames(intColumns)
                        arrReportBuilderFilterNames(intColumns) = arrReportBuilderFilterNamesTemp(x)
                        intColumns += 1
                        GoTo FilterAccessLoop
                    End If
                Next
FilterAccessLoop: Next
        End If

    End Sub

    Public Sub getColumnValues()
        Dim arrAdminLevels As String() = {"UserReports", "UserWorkflows", "UserManager", "UserSeniorManager", "UserCallCentre", "UserSales", "UserAdministrator", "UserRep", "Supplier", "Partner"}
        Dim arrAdminAccess As String() = {LeadPlatform.Config.Reports, LeadPlatform.Config.Workflows, LeadPlatform.Config.Manager, LeadPlatform.Config.SeniorManager, LeadPlatform.Config.CallCentre, LeadPlatform.Config.Sales, LeadPlatform.Config.Administrator, LeadPlatform.Config.Rep, LeadPlatform.Config.Supplier, LeadPlatform.Config.Partner}
        If Not checkValue(intReportBuilderID) Then
            If (strReportType = "Search") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderSearchID
            ElseIf (strReportType = "Diary") Then
                intReportBuilderID = LeadPlatform.Config.ReportBuilderDiaryID
            Else
                Dim intWorkflowReportBuilderID As String = getAnyFieldCached(CacheObject, "WorkflowReportBuilderID", "tblworkflows", "WorkflowID", intWorkflowID)
                If checkValue(intWorkflowReportBuilderID) Then
                    intReportBuilderID = intWorkflowReportBuilderID
                Else
                    intReportBuilderID = LeadPlatform.Config.ReportBuilderID
                End If
            End If
        End If

        Dim intColumns As Integer = 0, intViewColumns As Integer = 0, intDataStoreColumns As Integer = 0, intStatusDateColumns As Integer = 0
        Dim strSQL As String = "SELECT TOP(1) ReportBuilderColumnAccess, ReportBuilderColumnNames, ReportBuilderColumnSort, ReportBuilderColumnValues, ReportBuilderColumnFormats, ReportBuilderColumnTotals, ReportBuilderClickThrough, ReportBuilderClickThroughQueryString, ReportBuilderJavascriptFunctions, ReportBuilderTypeID FROM tblreportbuilder WHERE CompanyID = '" & CompanyID & "' AND ReportBuilderID = '" & intReportBuilderID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
        Dim intReportBuilderTypeID As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                arrReportBuilderColumnAccessTemp = Split(Row.Item("ReportBuilderColumnAccess").ToString, "|")
                arrReportBuilderColumnNamesTemp = Split(Row.Item("ReportBuilderColumnNames").ToString, "|")
                arrReportBuilderColumnSortTemp = Split(Row.Item("ReportBuilderColumnSort").ToString, "|")
                arrReportBuilderColumnValuesTemp = Split(Row.Item("ReportBuilderColumnValues").ToString, "|")
                arrReportBuilderColumnFormatsTemp = Split(Row.Item("ReportBuilderColumnFormats").ToString, "|")
                arrReportBuilderColumnTotalsTemp = Split(Row.Item("ReportBuilderColumnTotals").ToString, "|")
                arrReportBuilderClickThroughTemp = Split(Row.Item("ReportBuilderClickThrough").ToString, "|")
                arrReportBuilderClickThroughQueryStringTemp = Split(Row.Item("ReportBuilderClickThroughQueryString").ToString, "|")
                intReportBuilderTypeID = Row.Item("ReportBuilderTypeID")
                strReportBuilderJavascriptFunctions = Row.Item("ReportBuilderJavascriptFunctions").ToString
            Next
            For x As Integer = 0 To UBound(arrReportBuilderColumnAccessTemp)
                For a As Integer = 0 To UBound(arrAdminLevels)
                    If ((arrAdminAccess(a) = True) And InStr(arrReportBuilderColumnAccessTemp(x), arrAdminLevels(a))) Or (arrReportBuilderColumnAccessTemp(x) = "") Then
                        ReDim Preserve arrReportBuilderColumnNames(intColumns)
                        ReDim Preserve arrReportBuilderColumnSort(intColumns)
                        ReDim Preserve arrReportBuilderColumnValues(intColumns)
                        ReDim Preserve arrReportBuilderColumnFormats(intColumns)
                        ReDim Preserve arrReportBuilderColumnTotals(intColumns)
                        ReDim Preserve arrReportBuilderClickThrough(intColumns)
                        ReDim Preserve arrReportBuilderClickThroughQueryString(intColumns)
                        ReDim Preserve arrTotals(intColumns)
                        arrReportBuilderColumnNames(intColumns) = arrReportBuilderColumnNamesTemp(x)
                        arrReportBuilderColumnSort(intColumns) = arrReportBuilderColumnSortTemp(x)
                        If InStr(arrReportBuilderColumnValuesTemp(x), "VW:") Then
                            intViewColumns = UBound(arrReportBuilderViewColumnValues) + 1
                            ReDim Preserve arrReportBuilderViewColumnValues(intViewColumns)
                            arrReportBuilderViewColumnValues(intViewColumns) = arrReportBuilderColumnValuesTemp(x)
                            boolView = True
                        End If
                        If InStr(arrReportBuilderColumnValuesTemp(x), "DS:") Then
                            intDataStoreColumns = UBound(arrReportBuilderDataStoreColumnValues) + 1
                            ReDim Preserve arrReportBuilderDataStoreColumnValues(intDataStoreColumns)
                            arrReportBuilderDataStoreColumnValues(intDataStoreColumns) = arrReportBuilderColumnValuesTemp(x)
                            boolDataStore = True
                        End If
                        If InStr(arrReportBuilderColumnValuesTemp(x), "SD:") Then
                            intStatusDateColumns = UBound(arrReportBuilderStatusDateColumnValues) + 1
                            ReDim Preserve arrReportBuilderStatusDateColumnValues(intStatusDateColumns)
                            arrReportBuilderStatusDateColumnValues(intStatusDateColumns) = arrReportBuilderColumnValuesTemp(x)
                            boolStatusDate = True
                        End If
                        If (intReportBuilderTypeID = 3) Then ' Case History Only
                            boolStatusDate = True
                        End If
                        arrReportBuilderColumnValues(intColumns) = arrReportBuilderColumnValuesTemp(x)
                        arrReportBuilderColumnFormats(intColumns) = arrReportBuilderColumnFormatsTemp(x)
                        arrReportBuilderColumnTotals(intColumns) = arrReportBuilderColumnTotalsTemp(x)
                        If (arrReportBuilderClickThroughTemp.Length > 0) Then
                            arrReportBuilderClickThrough(intColumns) = arrReportBuilderClickThroughTemp(x)
                        End If
                        If (arrReportBuilderClickThroughQueryStringTemp.Length > 0) Then
                            arrReportBuilderClickThroughQueryString(intColumns) = arrReportBuilderClickThroughQueryStringTemp(x)
                        End If
                        intColumns += 1
                        GoTo ColumnAccessLoop
                    End If
                Next
ColumnAccessLoop: Next
        End If
    End Sub

    Public Sub buildFilters()
        Call getFilterValues()
        Dim arrParams As Object() = {}
        For x As Integer = 0 To UBound(arrReportBuilderFilterNames)
            If (checkValue(arrReportBuilderFilterNames(x))) Then
                If (InStr(arrReportBuilderFilterNames(x), "adhocSearch")) Then
                    Dim strAdhocSearchName As String = Replace(arrReportBuilderFilterNames(x), "adhocSearch", "")
                    responseWrite(adhocSearch("", HttpContext.Current.Request("frmSearchAdhoc" & strAdhocSearchName), strAdhocSearchName))
                ElseIf (InStr(arrReportBuilderFilterNames(x), "adhoc")) Then
                    Dim strAdhocFilterName As String = Replace(arrReportBuilderFilterNames(x), "adhoc", "")
                    responseWrite(adhocFilter("", HttpContext.Current.Request("frmAdhoc" & strAdhocFilterName), strAdhocFilterName))
                Else
                    Try
                        responseWrite(executeSub(Me, arrReportBuilderFilterNames(x), arrParams))
                    Catch ex As Exception

                    End Try

                End If
            End If
        Next
    End Sub

    Public Sub listCases()
        Dim intColSpan As Integer = 0
        Dim strSelectTop As String = ""
        Dim strSQL As String = "", strView As String = ""
        Dim strFields As String = "", strAdditionalFields As String = "", strTempField As String = ""
        Dim arrStatusDatesFields As New List(Of String)
        Dim arrDataStoreFields As New List(Of String)
        Dim arrViewFields As New List(Of String)
        Dim arrDynamicFields As New List(Of String)
        Dim arrDynamicField As String() = {}, strDynamicField As String = "", strDynamicFieldInnerQuery As String = ""

        If Not checkValue(strDateType) Then ' Define date type if not found
            If (strReportType = "Diary") Or (strReportType = "DiaryWorkflow") Then
                strDateType = "DiaryDue"
            Else
                strDateType = "Created"
            End If
        End If

        If Not checkValue(strAdditionalDateType) Then ' Define the second date type if not found
            If (strReportType = "Diary") Or (strReportType = "DiaryWorkflow") Then
                strAdditionalDateType = "DiaryDue"
            Else
                strAdditionalDateType = "Created"
            End If
        End If

        Call getColumnValues()

        Select Case strDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock", "DiaryDue"
            Case Else
                boolStatusDateSearch = True
        End Select

        Select Case strAdditionalDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock", "DiaryDue"
            Case Else
                boolStatusAdditionalDateSearch = True
        End Select

        If (checkValue(strExcludeDate)) Then
            Select Case strExcludeDate
                Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock", "DiaryDue"
                Case Else
                    boolStatusExcludeDateSearch = True
            End Select
        End If

        If (strReportType = "OutsideCriteria") Then ' Consuder dropping the outside criteria process
            strView = "vwreportoutsidecriteria"
        ElseIf (strReportType = "Diary") Or (strReportType = "DiaryWorkflow") Then ' Use the diary view
            strView = "vwreportdiarylist"
        Else ' Use the standard view
            strView = "vwreportcaselist"
        End If

        If (strReportType = "Diary") Or (strReportType = "DiaryWorkflow") Then ' Use diary fields
            strFields = strView & ".AppID, DiaryID, StatusCode, SubStatusCode, StatusDescription, SubStatusDescription, " & _
                          "DiaryDueDate, DiaryType, DiaryTypeDescription, DiaryLength, DiaryComplete,  DiaryActive, DiaryAssignedToUserID, DiaryUserReference,  " & _
                          "DiaryUserName, LockUserName, DiarySubStatusCode, RepUserName "
        Else ' Use standard fields
            strFields += strView & ".AppID, Amount, AmountOriginal, DialAttempts, " & _
                     "CreatedDate, NextCallDate, StatusCode, StatusDescription, SubStatusCode, SubStatusDescription, OutsideCriteriaSubStatusCode, OutsideCriteriaSubStatusDescription, " & _
                     "LockUserName, CallCentreUserReference, CallCentreUserName, SalesUserReference, SalesUserName, AdministratorUserReference, AdministratorUserName, RepUserReference, RepUserName "
        End If

        If (boolDataStore = True) Or (boolStatusDate = True) Then ' Pivot start
            strSQL = "SELECT TOP(" & intSelectTop & ") " & Replace(strFields, strView & ".", "") & " "

            For i As Integer = 0 To UBound(arrReportBuilderColumnValues)  ' ** Start lists - Build list of values to be returned on inner SQL query
                If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                    arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteStartDate & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteEndDate & " 23:59:59", "DTTM", Now)), "¬")
                    strDynamicField = arrDynamicField(0).ToString
                    If checkValue(arrDynamicField(1).ToString) Then
                        If InStr(arrDynamicField(1).ToString, "StoredDataValue") Then boolDataStore = True ' Make sure that the data store inner join is added
                        If InStr(arrDynamicField(1).ToString, "ApplicationStatusDate") Then boolStatusDate = True ' Make sure that the status date inner join is added
                        arrDynamicFields.Add(arrDynamicField(1).ToString)
                    End If
                    strSQL += ", " & Replace(Replace(Replace(strDynamicField, "VW:", ""), "DS:", ""), "SD:", "") & " "

                    If InStr(strDynamicField, "VW:") Then
                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "VW:[A-Za-z0-9]{0,100}")
                        For x As Integer = 0 To objMatches.Count - 1
                            If Not arrViewFields.Contains(Replace(objMatches.Item(x).Value, "VW:", "")) Then
                                arrViewFields.Add(Replace(objMatches.Item(x).Value, "VW:", ""))
                            End If
                        Next
                    End If
                    If InStr(strDynamicField, "DS:") Then
                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "DS:[A-Za-z0-9]{0,100}")
                        For x As Integer = 0 To objMatches.Count - 1
                            If Not arrDataStoreFields.Contains(Replace(objMatches.Item(x).Value, "DS:", "")) Then
                                arrDataStoreFields.Add(Replace(objMatches.Item(x).Value, "DS:", ""))
                            End If
                        Next
                    End If
                    If InStr(strDynamicField, "SD:") Then
                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "SD:[A-Za-z0-9]{0,100}")
                        For x As Integer = 0 To objMatches.Count - 1
                            If Not arrStatusDatesFields.Contains(Replace(objMatches.Item(x).Value, "SD:", "")) Then
                                arrStatusDatesFields.Add(Replace(objMatches.Item(x).Value, "SD:", ""))
                            End If
                        Next
                    End If
                ElseIf InStr(arrReportBuilderColumnValues(i), "VW:") Then
                    strTempField = Replace(arrReportBuilderColumnValues(i), "VW:", "")
                    strSQL += ", " & strTempField & " "
                    If Not arrViewFields.Contains(strTempField) Then
                        arrViewFields.Add(strTempField)
                    End If
                ElseIf InStr(arrReportBuilderColumnValues(i), "DS:") Then
                    strTempField = Replace(arrReportBuilderColumnValues(i), "DS:", "")
                    strSQL += ", " & strTempField & " "
                    If Not arrDataStoreFields.Contains(strTempField) Then
                        arrDataStoreFields.Add(strTempField)
                    End If
                ElseIf InStr(arrReportBuilderColumnValues(i), "SD:") Then
                    strTempField = Replace(arrReportBuilderColumnValues(i), "SD:", "")
                    strSQL += ", " & strTempField & " "
                    If Not arrStatusDatesFields.Contains(strTempField) Then
                        arrStatusDatesFields.Add(strTempField)
                    End If
                End If
            Next ' ** End lists

            strSQL += "FROM (SELECT " & strFields & " "

            For x As Integer = 0 To arrViewFields.Count - 1 ' Add view fields to inner query
                strSQL += ", " & arrViewFields.Item(x) & " "
            Next
            For x As Integer = 0 To arrDynamicFields.Count - 1 ' Add dynamic fields to inner query
                strSQL += ", " & arrDynamicFields.Item(x) & " "
            Next
            If (boolDataStore = True) Then ' Add data store pivot fields
                strSQL += ", StoredDataName, StoredDataValue "
            End If
            If (boolStatusDate = True) Then ' Add status date pivot fields
                strSQL += ", ApplicationStatusDateName, ApplicationStatusDate "
            End If

        ElseIf (boolView = True) Then ' Without Pivot
            For i As Integer = 0 To UBound(arrReportBuilderViewColumnValues)
                If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                    arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteStartDate & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteEndDate & " 23:59:59", "DTTM", Now)), "¬")
                    strDynamicField = arrDynamicField(0).ToString
                    strAdditionalFields += ", " & Replace(strDynamicField, "VW:", "") & " "
                Else
                    strAdditionalFields += ", " & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & " AS " & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & " "
                End If
            Next
            strSQL = "SELECT TOP (" & intSelectTop & ") " & strFields & strAdditionalFields & " "
        End If

        strSQL += " FROM " & strView & " "

        If (strReportType = "Returned") Then ' Consider dropping this section
            strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_4 ON tblapplicationstatusdates_4.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_4.ApplicationStatusDateName = 'ReturnedPreSale' AND NOT tblapplicationstatusdates_4.ApplicationStatusDate IS NULL "
            strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_5 ON tblapplicationstatusdates_5.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_5.ApplicationStatusDateName = 'ReturnAccepted' AND tblapplicationstatusdates_5.ApplicationStatusDate IS NULL "
            strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_6 ON tblapplicationstatusdates_6.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_6.ApplicationStatusDateName = 'ReturnDeclined' AND tblapplicationstatusdates_6.ApplicationStatusDate IS NULL "
        End If

        If (boolDataStore = True) Then ' Data store join
            strSQL += "LEFT JOIN tbldatastore ON tbldatastore.AppID = " & strView & ".AppID "
        End If
        If (boolStatusDate = True) Or (boolStatusDateSearch = True) Or (boolStatusAdditionalDateSearch = True) Then ' Status date join
            strSQL += "LEFT JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = " & strView & ".AppID "
        End If

        strSQL += "WHERE Active = 1 AND " & strView & ".CompanyID = '" & CompanyID & "' "
        If (strReportType = "Diary") Or (strReportType = "DiaryWorkflow") Then
            strSQL += "AND DiaryActive = 1 "
        End If
        If (LeadPlatform.Config.Supplier = True) Or (LeadPlatform.Config.Partner = True) Then
            strSQL += "AND ( "
            If (LeadPlatform.Config.Supplier = True) Then strSQL += "MediaIDInbound = '" & LeadPlatform.Config.SupplierCompanyID & "' "
            If (LeadPlatform.Config.Partner = True) Then
                If (LeadPlatform.Config.Supplier = True) Then strSQL += "OR "
                strSQL += "MediaIDOutbound = '" & LeadPlatform.Config.PartnerCompanyID & "' "
            End If
            If (LeadPlatform.Config.MasterMedia = True) Then
                If (LeadPlatform.Config.Supplier = True Or LeadPlatform.Config.Partner = True) Then strSQL += "OR "
                strSQL += "MediaCampaignPurchaseMediaID = '" & LeadPlatform.Config.MasterMediaID & "' "
            End If
            strSQL += ") "
        End If
        If (LeadPlatform.Config.Rep = True) Then
            strSQL += "AND (RepUserID = '" & Config.DefaultUserID & "') "
        End If

        If (strReportType = "Workflow") Or (strReportType = "DiaryWorkflow") Then ' Add in the workflow query if found
            strWorkflowQuery = regexReplace("tbl[a-z]{0,50}\.AppID", strView & ".AppID", getAnyField("WorkflowQuery", "tblworkflows", "WorkflowID", intWorkflowID)) ' 27/02/2012 - replace reference to tbl.AppID with view name
            strWorkflowQuery = regexReplace("tbl[a-z]{0,50}\.", "", strWorkflowQuery)
            strWorkflowQuery = Replace(strWorkflowQuery, "{UserID}", Config.DefaultUserID)
            strWorkflowQuery = Replace(strWorkflowQuery, " AppID", strView & ".AppID") ' 27/02/2012 - replace the AppID with the view name
            If checkValue(strWorkflowQuery) Then
                strSQL += "AND (" & strWorkflowQuery & ") "
            End If
        Else ' Or run report from filters
            If (boolStatusDateSearch = True) Then ' N.B. These date searches should live outside the inner query, but they are too resource hungry
                If (strReportType <> "Search") Then
                    Dim arrDateType As Array = Split(strDateType, ",")
                    strSQL += "AND ("
                    For x As Integer = 0 To UBound(arrDateType)
                        If (x > 0) Then
                            strSQL += " OR ApplicationStatusDateName = '" & arrDateType(x) & "'"
                        Else
                            strSQL += "ApplicationStatusDateName = '" & arrDateType(x) & "'"
                        End If
                    Next
                    strSQL += ") AND ApplicationStatusDate >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND ApplicationStatusDate <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
                Else
                    Dim arrDateType As Array = Split(strDateType, ",")
                    strSQL += "AND ("
                    For x As Integer = 0 To UBound(arrDateType)
                        If (x > 0) Then
                            strSQL += " OR ApplicationStatusDateName = '" & arrDateType(x) & "'"
                        Else
                            strSQL += "ApplicationStatusDateName = '" & arrDateType(x) & "'"
                        End If
                    Next
                    strSQL += ") AND NOT ApplicationStatusDate IS NULL "
                End If
            Else
                If (strReportType <> "Search") Then
                    strSQL += "AND " & _
                    strDateType & "Date >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
                    strDateType & "Date <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
                Else
                    strSQL += "AND NOT " & strDateType & "Date IS NULL "
                End If
            End If
            If (boolStatusAdditionalDateSearch = True) Then
                If (checkValue(dteAdditionalStartDate) And checkValue(dteAdditionalEndDate)) Then
                    strSQL += "AND ApplicationStatusDateName = '" & strAdditionalDateType & "' AND ApplicationStatusDate >= " & formatField(dteAdditionalStartDate & " 00:00:00", "DTTM", Now) & " AND ApplicationStatusDate <= " & formatField(dteAdditionalEndDate & " 23:59:59", "DTTM", Now) & " "
                Else
                    strSQL += "AND ApplicationStatusDateName = '" & strAdditionalDateType & "' AND NOT ApplicationStatusDate IS NULL "
                End If
            Else
                If (checkValue(dteAdditionalStartDate) And checkValue(dteAdditionalEndDate)) Then
                    strSQL += "AND " & _
                                strAdditionalDateType & "Date >= " & formatField(dteAdditionalStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
                                strAdditionalDateType & "Date <= " & formatField(dteAdditionalEndDate & " 23:59:59", "DTTM", Now) & " "
                Else
                    strSQL += "AND NOT " & strAdditionalDateType & "Date IS NULL "
                End If
            End If

            If (checkValue(strExcludeDate)) Then
                If (boolStatusExcludeDateSearch = True) Then
                    strSQL += "AND (SELECT COUNT(*) FROM tblapplicationstatusdates WHERE ApplicationStatusDateAppID = " & strView & ".AppID AND (ApplicationStatusDateName = '" & strExcludeDate & "' AND NOT ApplicationStatusDate IS NULL)) = 0 "
                Else
                    strSQL += "AND " & strExcludeDate & "Date IS NULL "
                End If
            End If
        End If

        ' ** Start Search Filters
        If checkValue(intAppID) Then
            Dim arrAppID As Array = Split(intAppID, ",")
            strSQL += "AND ("
            For x As Integer = 0 To UBound(arrAppID)
                If (x = 0) Then
                    strSQL += "(" & strView & ".AppID = '" & arrAppID(x) & "' OR ClientReferenceInbound = '" & arrAppID(x) & "') "
                Else
                    strSQL += "OR (" & strView & ".AppID = '" & arrAppID(x) & "' OR ClientReferenceInbound = '" & arrAppID(x) & "') "
                End If
            Next
            strSQL += ") "
        End If

        If checkValue(strSurname) Then strSQL += "AND (App1Surname like '%" & strSurname & "%' OR App2Surname like '%" & strSurname & "%') "
        If checkValue(dteDOB) Then strSQL += "AND App1DOB = " & formatField(dteDOB & " 00:00:00", "DTTM", Now) & " "
        If checkValue(strTelephoneNumber) Then
            strTelephoneNumber = Replace(strTelephoneNumber, " ", "")
            strSQL += "AND (App1HomeTelephone = '" & strTelephoneNumber & "' OR App1MobileTelephone = '" & strTelephoneNumber & "' OR App1WorkTelephone = '" & strTelephoneNumber & "') "
        End If
        If checkValue(strEmailAddress) Then strSQL += "AND (App1EmailAddress = '" & strEmailAddress & "') "
        If checkValue(strPostCode) Then strSQL += "AND AddressPostCode = '" & Replace(strPostCode, " ", "") & "' "
        If checkValue(intCustID) Then strSQL += "AND CustID = " & intCustID & " "

        If checkValue(intClientID) Then
            Dim arrClientID = Split(intClientID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
		 If checkValue(intNetworkID) Then
            Dim arrNetworkID = Split(intNetworkID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrNetworkID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " NetworkID = '" & arrNetworkID(i) & "' "
            Next
            strSQL += ") "
        End If
		 
        If checkValue(intClientContactID) Then
            Dim arrClientID = Split(intClientContactID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientContactID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaIDInbound) Then
            Dim arrMediaIDInbound = Split(intMediaIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDInbound = '" & arrMediaIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intPurchaseMediaID) Then
            Dim arrPurchaseMediaID = Split(intPurchaseMediaID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrPurchaseMediaID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignPurchaseMediaID = '" & arrPurchaseMediaID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaIDOutbound) Then
            Dim arrMediaIDOutbound = Split(intMediaIDOutbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDOutbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDOutbound = '" & arrMediaIDOutbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDInbound) Then
            Dim arrMediaCampaignIDInbound = Split(intMediaCampaignIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDInbound = '" & arrMediaCampaignIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intClientID) Then
            Dim arrClientID = Split(intClientID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intNetworkID) Then
            Dim arrNetworkID = Split(intNetworkID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrNetworkID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " NetworkID = '" & arrNetworkID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDOutbound) Then
            Dim arrMediaCampaignIDOutbound = Split(intMediaCampaignIDOutbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDOutbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDOutbound = '" & arrMediaCampaignIDOutbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignDialerGradeID) Then
            Dim arrMediaCampaignDialerGradeID = Split(intMediaCampaignDialerGradeID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignDialerGradeID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignDialerGradeID = '" & arrMediaCampaignDialerGradeID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignApplicationTemplateID) Then
            Dim arrMediaCampaignApplicationTemplateID = Split(intMediaCampaignApplicationTemplateID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignApplicationTemplateID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignApplicationTemplateID = '" & arrMediaCampaignApplicationTemplateID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strStatusCode) Then
            Dim arrStatusCode = Split(strStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrStatusCode)
                If (i > 0) Then strSQL += "OR "
                strSQL += " StatusCode = '" & arrStatusCode(i) & "' "
            Next
            strSQL += ") "
            If checkValue(strSubStatusCode) Then
                Dim arrSubStatusCode = Split(strSubStatusCode, ",")
                strSQL += "AND ("
                For i As Integer = 0 To UBound(arrSubStatusCode)
                    If (i > 0) Then strSQL += "OR "
                    strSQL += " SubStatusCode = '" & arrSubStatusCode(i) & "' "
                Next
                strSQL += ") "
            End If
        End If
        If checkValue(strExcludeStatusCode) Then
            Dim arrExcludeStatusCode = Split(strExcludeStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrExcludeStatusCode)
                If (i > 0) Then strSQL += "AND "
                strSQL += " StatusCode <> '" & arrExcludeStatusCode(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strMediaCampaignProductType) Then
            Dim arrMediaCampaignProductType = Split(strMediaCampaignProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignProductType = '" & arrMediaCampaignProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strProductType) Then
            Dim arrProductType = Split(strProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ProductType = '" & arrProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strDiaryType) Then
            Dim arrDiaryType = Split(strDiaryType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrDiaryType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " DiaryType = '" & arrDiaryType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strCallCentreUser) Then
            Dim arrCallCentreUserID = Split(strCallCentreUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrCallCentreUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " CallCentreUserID = '" & arrCallCentreUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strCallCentreTeam) Then
            Dim arrCallCentreTeamID = Split(strCallCentreTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrCallCentreTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " CallCentreTeamID = '" & arrCallCentreTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strSalesUser) Then
            Dim arrSalesUserID = Split(strSalesUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrSalesUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " SalesUserID = '" & arrSalesUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strSalesTeam) Then
            Dim arrSalesTeamID = Split(strSalesTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrSalesTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " SalesTeamID = '" & arrSalesTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strAdministratorUser) Then
            Dim arrAdministratorUserID = Split(strAdministratorUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrAdministratorUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " AdministratorUserID = '" & arrAdministratorUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strAdministratorTeam) Then
            Dim arrAdminTeamID = Split(strAdministratorTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrAdminTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " AdministratorTeamID = '" & arrAdminTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strRepUser) Then
            Dim arrRepUserID = Split(strRepUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrRepUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " RepUserID = '" & arrRepUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If (checkValue(intMinDialAttempts)) Then
            strSQL += "AND (DialAttempts >= '" & intMinDialAttempts & "') "
        End If
        If (checkValue(intMaxDialAttempts)) Then
            strSQL += "AND (DialAttempts <= '" & intMaxDialAttempts & "') "
        End If
        If (checkValue(intMinAmount)) Then
            strSQL += "AND (Amount >= '" & intMinAmount & "') "
        End If
        If (checkValue(intMaxAmount)) Then
            strSQL += "AND (Amount <= '" & intMaxAmount & "') "
        End If
		 If (checkValue(strdatastorename)) Then
            strSQL += "AND (workflowname = '" & strdatastorename & "') "
        End If
		 If (checkValue(strdatastorevalue)) Then
            strSQL += "AND (workflowvalue LIKE " & strdatastorevalue & "') "
        End If
		
        Dim z As Integer = 0
        For Each Item In HttpContext.Current.Request.QueryString
            If (InStr(Item, "frmAdhoc") And checkValue(HttpContext.Current.Request.QueryString(Item))) Then
                If (z > 0) Then
                    strAdhocFilter += " AND ("
                Else
                    strAdhocFilter = " ("
                End If
                Dim arrAdhocFilter = Split(HttpContext.Current.Request.QueryString(Item), ",")
                For i As Integer = 0 To UBound(arrAdhocFilter)
                    If (i > 0) Then strAdhocFilter += "OR "
                    strAdhocFilter += " " & Replace(Item, "frmAdhoc", "") & " = '" & arrAdhocFilter(i) & "' "
                Next
                strAdhocFilter += ") "
                z += 1
            End If
        Next

        z = 0
        For Each Item In HttpContext.Current.Request.QueryString
            If (InStr(Item, "frmSearchAdhoc") And checkValue(HttpContext.Current.Request.QueryString(Item))) Then
                If (z > 0 Or checkValue(strAdhocFilter)) Then
                    strAdhocFilter += " AND ("
                Else
                    strAdhocFilter = " ("
                End If
                Dim arrSearchAdhoc = Split(HttpContext.Current.Request.QueryString(Item), ",")
                For i As Integer = 0 To UBound(arrSearchAdhoc)
                    If (i > 0) Then strAdhocFilter += "OR "
                    strAdhocFilter += " " & Replace(Item, "frmSearchAdhoc", "") & " LIKE '%" & arrSearchAdhoc(i) & "%' "
                Next
                strAdhocFilter += ") "
                z += 1
            End If
        Next
		
		If checkValue(strAdhocFilter) Then
            strSQL += "AND " & strAdhocFilter & " " ' Add ad hoc filter

        End If

        If checkValue(strAdhocQuery) Then
            If (checkValue(strAdhocFilter)) Then
                strSQL += "AND (" & strAdhocQuery & ")" ' Add ad hoc filter
            Else
                strSQL += "WHERE " & strAdhocQuery & " " ' Add ad hoc filter
            End If
        End If
		

        ' ** End Search Filters

        If (boolDataStore = True) Or (boolStatusDate = True) Then
            strSQL += ") A "
        End If

        If (boolDataStore = True) And (arrDataStoreFields.Count > 0) Then
            strSQL += "PIVOT (MIN(StoredDataValue) FOR StoredDataName IN ("
            For x As Integer = 0 To arrDataStoreFields.Count - 1 ' Add data store fields to pivot
                If (x > 0) Then strSQL += ", "
                strSQL += "[" & arrDataStoreFields.Item(x) & "] "
            Next
            strSQL += ")) B "
        End If

        If ((boolStatusDate = True) Or (boolStatusDateSearch = True) Or (boolStatusExcludeDateSearch = True)) And (arrStatusDatesFields.Count > 0) Then
            strSQL += " PIVOT (MIN(ApplicationStatusDate) FOR ApplicationStatusDateName IN ("
            For x As Integer = 0 To arrStatusDatesFields.Count - 1 ' Add status dates fields to pivot
                If (x > 0) Then strSQL += ", "
                strSQL += "[" & arrStatusDatesFields.Item(x) & "]"
            Next
            strSQL += ")) C "
        End If



'        If checkValue(strAdhocFilter) Then
'            strSQL += "WHERE " & strAdhocFilter & " " ' Add ad hoc filter
'
'        End If
'
'        If checkValue(strAdhocQuery) Then
'            If (checkValue(strAdhocFilter)) Then
'                strSQL += "AND (" & strAdhocQuery & ")" ' Add ad hoc filter
'            Else
'                strSQL += "WHERE " & strAdhocQuery & " " ' Add ad hoc filter
'            End If
'        End If

        strSQL += "ORDER BY "
        If (strReportType = "Workflow") Or (strReportType = "DiaryWorkflow") Then
            Dim intSubWorkflowID As String = getAnyFieldCached(CacheObject, "WorkflowSubWorkflowID", "tblworkflows", "WorkflowID", intWorkflowID)
            If (checkValue(intSubWorkflowID)) Then
                If (CInt(intSubWorkflowID) > 0) Then
                    strWorkflowOrderBy = regexReplace("tbl[a-z]{0,50}\.AppID", strView & ".AppID", getAnyField("SubWorkflowOrderBy", "tblsubworkflows", "SubWorkflowID", intSubWorkflowID)) ' 27/02/2012 - replace reference to tbl.AppID with view name
                Else
                    strWorkflowOrderBy = regexReplace("tbl[a-z]{0,50}\.AppID", strView & ".AppID", getAnyField("WorkflowOrderBy", "tblworkflows", "WorkflowID", intWorkflowID)) ' 27/02/2012 - replace reference to tbl.AppID with view name
                End If
            End If
            strWorkflowOrderBy = Replace(strWorkflowOrderBy, "CallPriority", "CASE WHEN SubStatusCode = 'CBK' OR SubStatusCode = '3BK' THEN 1 WHEN DialAttempts = 0 THEN 2 ELSE 3 END")
            strSQL += strWorkflowOrderBy
        Else
            If (strReportType = "Diary") Then
                strSQL += " DiaryDueDate"
            Else
                strSQL += "CreatedDate DESC"
            End If
        End If

        strReportSQL = strSQL

       'responseWrite(strReportSQL)
       'responseEnd()

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (strMode = "csv") Then
                '.Write("Categories")
            Else
                .WriteLine(startHeaderRow("tablehead"))
            End If

            If UBound(arrReportBuilderColumnNames) = -1 Then
                .WriteLine(startCol("smlc", "th"))
                .WriteLine("No columns found")
                .WriteLine(endCol("th"))
            Else
                For x As Integer = 0 To UBound(arrReportBuilderColumnNames)
                    If (strMode = "csv") Then
                        If (x = 0) Then
                            .Write(arrReportBuilderColumnNames(x))
                        Else
                            .Write("," & arrReportBuilderColumnNames(x))
                        End If
                    Else
                        .WriteLine(startCol("smlc " & arrReportBuilderColumnSort(x), "th"))
                        .WriteLine(arrReportBuilderColumnNames(x))
                        .WriteLine(endCol("th"))
                    End If
                    intColSpan += 1
                Next
            End If
            If (strMode = "csv") Then
                .Write(vbCrLf) ' ** End header with a line break
            Else
                .WriteLine(endHeaderRow())
            End If

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                If (Not checkValue(strNoLogging)) Then
                    If (strReportType <> "Workflow" And strReportType <> "DiaryWorkflow") Then
                        logReport(dsCache.Rows.Count)
                    End If
                End If
                If (strMode = "csv") Then
                    ' No need to start body
                Else
                    .WriteLine(startBody())
                End If
                For Each Row As DataRow In dsCache.Rows
                    If (strMode = "csv") Then
                        ' No need to start row
                    Else
                        .WriteLine(startRow(strClass))
                    End If
                    Dim arrParams As Object() = {Row}
                    For x As Integer = 0 To UBound(arrReportBuilderColumnValues)
                        intColumn = x
                        If InStr(arrReportBuilderColumnValues(x), "¬") Then
                            Dim arrField = Split(arrReportBuilderColumnValues(x), "¬")
                            strDynamicField = arrField(0).ToString
                            Dim arrAlias As String() = Split(strDynamicField, " AS ")
                            ReDim Preserve arrParams(2)
                            arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match 
                            arrParams(2) = arrReportBuilderColumnFormats(x)
                            If (strMode = "csv") Then
                                If (x = 0) Then
                                    .Write(Row.Item(arrParams(1)))
                                Else
                                    .Write("," & Row.Item(arrParams(1)))
                                End If
                            Else
                                .WriteLine(executeSub(Me, "colDataStore", arrParams))
                            End If
                            ReDim Preserve arrParams(0)
                        ElseIf InStr(arrReportBuilderColumnValues(x), "VW:") Or InStr(arrReportBuilderColumnValues(x), "DS:") Or InStr(arrReportBuilderColumnValues(x), "SD:") Then
                            ReDim Preserve arrParams(2)
                            arrParams(1) = Replace(Replace(Replace(arrReportBuilderColumnValues(x), "VW:", ""), "DS:", ""), "SD:", "")
                            arrParams(2) = arrReportBuilderColumnFormats(x)
                            If (strMode = "csv") Then
                                If (x = 0) Then
                                    .Write(Row.Item(arrParams(1)))
                                Else
                                    .Write("," & Row.Item(arrParams(1)))
                                End If
                            Else
                                .WriteLine(executeSub(Me, "colDataStore", arrParams))
                            End If
                            ReDim Preserve arrParams(0)
                        Else
                            If (strMode = "csv") Then
                                If (x = 0) Then
                                    .Write(Row.Item(arrParams(1)))
                                Else
                                    .Write("," & Row.Item(arrParams(1)))
                                End If
                            Else
                                .WriteLine(executeSub(Me, arrReportBuilderColumnValues(x), arrParams))
                            End If
                        End If
                    Next
                    If (strMode = "csv") Then
                        If (dsCache.Rows.IndexOf(Row) < dsCache.Rows.Count - 1) Then
                            .Write(vbCrLf)
                        End If
                    Else
                        .WriteLine(endRow())
                    End If
                    strClass = nextClass(strClass)
                    If (strMode = "csv") Then
                        ' No total row
                    Else
                        If (dsCache.Rows.IndexOf(Row) = (dsCache.Rows.Count - 1)) Then ' Total row
                            .WriteLine(endBody())
                            .WriteLine(startFooter(""))
                            .WriteLine(startRow(strClass, "", "TotalRow"))
                            For x As Integer = 0 To UBound(arrReportBuilderColumnTotals)
                                intColumn = x
                                If (checkValue(arrReportBuilderColumnTotals(x))) Then
                                    .WriteLine(executeSub(Me, arrReportBuilderColumnTotals(x), arrParams))
                                Else
                                    .WriteLine(colNBSP(Row))
                                End If
                            Next
                            .WriteLine(endRow())
                            strClass = nextClass(strClass)
                        End If
                    End If

                    intRowsCount += 1
                Next

                If (strMode = "csv") Then
                    ' No record count
                Else
                    .WriteLine(startRow(strClass))
                    .WriteLine(startCol("smlr", "td", intColSpan))
                    If (dsCache.Rows.Count >= CInt(intSelectTop)) Then
                        .WriteLine("<strong>Top " & intSelectTop & " record(s) returned.</strong>")
                    Else
                        .WriteLine("<strong>" & dsCache.Rows.Count & " record(s) returned.</strong>")
                    End If
                    .WriteLine(endCol("td"))
                    .WriteLine(endRow())
                    .WriteLine(endFooter())
                End If
            Else
                If (strMode = "csv") Then
                    ' No message
                Else
                    .WriteLine(startBody())
                    .WriteLine(startRow(strClass))
                    .WriteLine(startCol("smlc", "td", intColSpan))
                    .WriteLine("No results found.")
                    .WriteLine(endCol("td"))
                    .WriteLine(endRow())
                    .WriteLine(endBody())
                End If
            End If
        End With
        responseWrite(objStringWriter.ToString)
    End Sub

    Public Sub groupCases()

        Dim strGroupCases As String = ""
        Dim blnQuery As Boolean = False
        Dim intCols As String = 3
        Dim intTotalNo As Integer = 0
        If Not checkValue(strDateType) Then strDateType = "Created"
        If Not checkValue(strAdditionalDateType) Then strAdditionalDateType = "Created"
        If (strShowTransferred = "Y") Then intCols = 4

        Dim strSQL As String = "SELECT StatusCode, StatusDescription, SubStatusCode, SubStatusDescription, "
        If (strShowTransferred = "Y") Then strSQL += "MediaOutboundName, MediaCampaignIDOutbound, MediaCampaignOutboundName, "
        strSQL += _
            "COUNT(*) AS No FROM vwreportcaselist "
        Select Case strDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
            Case Else
                strSQL += "INNER JOIN tblapplicationstatusdates ON ApplicationStatusDateAppID = AppID AND ApplicationStatusDateName = '" & strDateType & "' AND ApplicationStatusDate >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND ApplicationStatusDate <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
        End Select
        If (checkValue(dteAdditionalStartDate) And checkValue(dteAdditionalEndDate)) Then
            Select Case strAdditionalDateType
                Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                Case Else
                    strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_2 ON tblapplicationstatusdates_2.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_2.ApplicationStatusDateName = '" & strAdditionalDateType & "' AND tblapplicationstatusdates_2.ApplicationStatusDate >= " & formatField(dteAdditionalStartDate & " 00:00:00", "DTTM", Now) & " AND tblapplicationstatusdates_2.ApplicationStatusDate <= " & formatField(dteAdditionalEndDate & " 23:59:59", "DTTM", Now) & " "
            End Select
        End If
        If (checkValue(strExcludeDate)) Then
            Select Case strExcludeDate
                Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                Case Else
                    strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_3 ON tblapplicationstatusdates_3.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_3.ApplicationStatusDateName = '" & strExcludeDate & "' AND tblapplicationstatusdates_3.ApplicationStatusDate IS NULL "
            End Select
        End If
        Select Case strAdditionalDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
            Case Else
                strSQL += "INNER JOIN tblapplicationstatusdates AS tblapplicationstatusdates_4 ON tblapplicationstatusdates_4.ApplicationStatusDateAppID = AppID AND tblapplicationstatusdates_4.ApplicationStatusDateName = '" & strAdditionalDateType & "' AND NOT tblapplicationstatusdates_4.ApplicationStatusDate IS NULL "
        End Select
        strSQL += "WHERE Active = 1 AND vwreportcaselist.CompanyID = '" & CompanyID & "' "
        Select Case strDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                strSQL += _
                    "AND " & _
                    strDateType & "Date >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
                    strDateType & "Date <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
        End Select
        If (checkValue(dteAdditionalStartDate) And checkValue(dteAdditionalEndDate)) Then
            Select Case strAdditionalDateType
                Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                    strSQL += _
                        "AND " & _
                        strAdditionalDateType & "Date >= " & formatField(dteAdditionalStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
                        strAdditionalDateType & "Date <= " & formatField(dteAdditionalEndDate & " 23:59:59", "DTTM", Now) & " "
            End Select
        End If
        If (checkValue(strExcludeDate)) Then
            Select Case strExcludeDate
                Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                    strSQL += "AND " & strExcludeDate & "Date IS NULL "
            End Select
        End If

        ' ** Start filters
        If checkValue(intMediaIDInbound) Then
            Dim arrMediaIDInbound = Split(intMediaIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDInbound = '" & arrMediaIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaIDInbound) Then
            Dim arrMediaIDInbound = Split(intMediaIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDInbound = '" & arrMediaIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDInbound) Then
            Dim arrMediaCampaignIDInbound = Split(intMediaCampaignIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDInbound = '" & arrMediaCampaignIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intClientID) Then
            Dim arrClientID = Split(intClientID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intNetworkID) Then
            Dim arrNetworkID = Split(intNetworkID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrNetworkID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " NetworkID = '" & arrNetworkID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDOutbound) Then
            Dim arrMediaCampaignIDOutbound = Split(intMediaCampaignIDOutbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDOutbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDOutbound = '" & arrMediaCampaignIDOutbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignApplicationTemplateID) Then
            Dim arrMediaCampaignApplicationTemplateID = Split(intMediaCampaignApplicationTemplateID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignApplicationTemplateID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignApplicationTemplateID = '" & arrMediaCampaignApplicationTemplateID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strStatusCode) Then
            Dim arrStatusCode = Split(strStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrStatusCode)
                If (i > 0) Then strSQL += "OR "
                strSQL += " StatusCode = '" & arrStatusCode(i) & "' "
            Next
            strSQL += ") "
            If checkValue(strSubStatusCode) Then
                Dim arrSubStatusCode = Split(Replace(strSubStatusCode, "BLANK", ""), ",")
                strSQL += "AND ("
                For i As Integer = 0 To UBound(arrSubStatusCode)
                    If (i > 0) Then strSQL += "OR "
                    strSQL += " SubStatusCode = '" & arrSubStatusCode(i) & "' "
                Next
                strSQL += ") "
            End If
        End If
        If checkValue(strExcludeStatusCode) Then
            Dim arrExcludeStatusCode = Split(strExcludeStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrExcludeStatusCode)
                If (i > 0) Then strSQL += "AND "
                strSQL += " StatusCode <> '" & arrExcludeStatusCode(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strMediaCampaignProductType) Then
            Dim arrMediaCampaignProductType = Split(strMediaCampaignProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignProductType = '" & arrMediaCampaignProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strProductType) Then
            Dim arrProductType = Split(strProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ProductType = '" & arrProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strSalesUser) Then
            Dim arrSalesUserID = Split(strSalesUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrSalesUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " SalesUserID = '" & arrSalesUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strRepUser) Then
            Dim arrRepUserID = Split(strRepUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrRepUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " RepUserID = '" & arrRepUserID(i) & "' "
            Next
            strSQL += ") "
        End If

        Select Case strAdditionalDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
                strSQL += "AND NOT " & strAdditionalDateType & "Date IS NULL "
        End Select
        ' ** End filters

        strSQL += "GROUP BY StatusCode, StatusDescription, SubStatusCode, SubStatusDescription "
        If (strShowTransferred = "Y") Then strSQL += ",MediaOutboundName, MediaCampaignIDOutbound, MediaCampaignOutboundName "
        strSQL += "ORDER BY StatusCode, SubStatusCode "

        strReportSQL = strSQL

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter

            .WriteLine(startHeaderRow("tablehead"))
            .WriteLine("<th class=""smlc"">Status</th>")
            .WriteLine("<th class=""smlc"">Sub Status</th>")
            If (strShowTransferred = "Y") Then WriteLine("<th class=""smlc"">Transferred To</th>")
            .WriteLine("<th class=""smlc"">No.</th>")
            .WriteLine(endHeaderRow())
            .WriteLine(startBody())

            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "vwreportcaselist")
            Dim dsCases As DataTable = objDataSet.Tables("vwreportcaselist")
            If (dsCases.Rows.Count > 0) Then
                Dim strClass As String = "row1"
                Dim x As Integer = 0
                For Each Row As DataRow In dsCases.Rows
                    .WriteLine(startRow(strClass))
                    .WriteLine("<td class=""smlc"">" & Row.Item("StatusDescription") & "</td>")
                    .WriteLine("<td class=""smlc"">" & Row.Item("SubStatusDescription") & "</td>")
                    If (strShowTransferred = "Y") Then .WriteLine("<td class=""smlc"">" & Row.Item("MediaOutboundName") & " " & Row.Item("MediaCampaignOutboundName") & "</td>")
                    .WriteLine("<td class=""smlc""><a href=""reportbuilder.aspx?frmList=Y&frmDateType=" & strDateType & "&frmStartDate=" & dteStartDate & "&frmEndDate=" & dteEndDate & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmClientID=" & intClientID & "&frmNetworkID=" & intClientID & "&frmClientContactID=" & intClientContactID & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaIDInbound=" & intMediaIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmAdditionalDateType=" & strAdditionalDateType & "&frmAdditionalStartDate=" & dteAdditionalStartDate & "&frmAdditionalEndDate=" & dteAdditionalEndDate & "&frmExcludeDate=" & strExcludeDate & "&frmStatusCode=" & Row.Item("StatusCode").ToString & "&frmSubStatusCode=" & Row.Item("SubStatusCode").ToString & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType & "&frmTitle=View Cases"">" & Row.Item("No") & "</a></td>")
                    .WriteLine(endRow())
                    strClass = nextClass(strClass)
                    intTotalNo += Row.Item("No")
                    x = x + 1
                Next
                .WriteLine("<tr class=""total"">")
                .WriteLine("<td class=""smlc"">&nbsp;</td>")
                If (strShowTransferred = "Y") Then .WriteLine("<td class=""smlc"">&nbsp;</td>")
                .WriteLine("<td class=""smlc"">&nbsp;</td>")
                .WriteLine("<td class=""smlc"">" & intTotalNo & "</td>")
                .WriteLine("</tr>")
                .WriteLine("<tr class=""" & nextClass(strClass) & """>")
                .WriteLine("<td colspan=""" & intCols & """ class=""smlr"">" & x & " record(s) returned.</td>")
                .WriteLine("</tr>")
            Else
                .WriteLine("<tr class=""" & strClass & """>")
                .WriteLine("<td colspan=""" & intCols & """ class=""smlc"">No results found.</td>")
                .WriteLine("</tr>")
            End If
            .WriteLine(endBody())
            dsCases.Clear()
            dsCases = Nothing
            objDataSet = Nothing
            objDatabase = Nothing
        End With
        responseWrite(objStringWriter.ToString)
    End Sub

    Public Sub listCaseHistory()
        Dim intColSpan As Integer = 0
        Dim strFields As String = "", strFieldsInner As String = "", strAdditionalFields As String = "", strGroupBy As String = "", strOrderBy As String = "", strFirstColumn As String = "", strTempField As String = ""
        If Not checkValue(strDateType) Then strDateType = "Created"
        Dim strSQL As String = ""
        Dim arrStatusDatesFields As New List(Of String)
        Dim arrDataStoreFields As New List(Of String)
        Dim arrViewFields As New List(Of String)
        Dim arrDynamicFields As New List(Of String)
        Dim strFilter As String = ""
        Dim boolDynamic As Boolean = False
        Dim arrDynamicField As String() = {}, strDynamicField As String = "", strDynamicFieldInnerQuery As String = ""
        'strDynamicFieldAlias As String = "", 

        Call getColumnValues() ' Builds arrays

        'If UBound(arrReportBuilderColumnValues) >= 0 Then boolDynamic = True ' Use a default layout if fields aren't found
        If intReportBuilderID <> objLeadPlatform.Config.ReportBuilderCaseHistoryID Then boolDynamic = True

        If (boolDynamic = False) Then ' If not dynamaic, use company specific date fields
            strSQL = "SELECT DateType FROM tbldatetypes WHERE (DateTypeActive = 1) AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY DateTypeOrder"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    arrStatusDatesFields.Add(Row.Item("DateType"))
                Next
            End If
            dsCache = Nothing
        End If

        Select Case strDateType
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "StatusChange", "RepAppointment", "Lock"
            Case Else
                boolStatusDateSearch = True
        End Select

        Select Case strReportGroupType ' Generic grouping fields
            Case "Grade"
                strFields = "MediaCampaignDialerGradeID, MediaCampaignGradeName, "
                strFieldsInner = strFields
                strGroupBy = "MediaCampaignDialerGradeID, MediaCampaignGradeName "
                strOrderBy = "MediaCampaignGradeName"
                strFirstColumn = "MediaCampaignGradeName"
            Case "Media"
                strFields = "MediaIDInbound, MediaInboundName, "
                strFieldsInner = strFields
                strGroupBy = "MediaIDInbound, MediaInboundName "
                strOrderBy = "MediaInboundName"
                strFirstColumn = "MediaInboundName"
            Case "MediaOutbound"
                strFields = "MediaIDOutbound, MediaOutboundName, "
                strFieldsInner = strFields
                strGroupBy = "MediaIDOutbound, MediaOutboundName "
                strOrderBy = "MediaOutboundName"
                strFirstColumn = "MediaOutboundName"
            Case "MediaCampaignOutbound"
                strFields = "MediaCampaignIDOutbound, MediaCampaignOutboundName, "
                strFieldsInner = strFields
                strGroupBy = "MediaCampaignIDOutbound, MediaCampaignOutboundName "
                strOrderBy = "MediaCampaignOutboundName"
                strFirstColumn = "MediaCampaignOutboundName"
            Case "SalesUser"
                strFields = "SalesUserID, SalesUserName, "
                strFieldsInner = strFields
                strGroupBy = "SalesUserID, SalesUserName "
                strOrderBy = "SalesUserName"
                strFirstColumn = "SalesUserName"
            Case "CallCentreUser"
                strFields = "CallCentreUserID, CallCentreUserName, "
                strFieldsInner = strFields
                strGroupBy = "CallCentreUserID, CallCentreUserName "
                strOrderBy = "CallCentreUserName"
                strFirstColumn = "CallCentreUserName"
            Case "AdministratorUser"
                strFields = "AdministratorUserID, AdministratorUserName, "
                strFieldsInner = strFields
                strGroupBy = "AdministratorUserID, AdministratorUserName "
                strOrderBy = "AdministratorUserName"
                strFirstColumn = "AdministratorUserName"
            Case "RepUser"
                strFields = "RepUserID, RepUserName, "
                strFieldsInner = strFields
                strGroupBy = "RepUserID, RepUserName "
                strOrderBy = "RepUserName"
                strFirstColumn = "RepUserName"
            Case "Status"
                strFields = "StatusCode, "
                strFieldsInner = strFields
                strGroupBy = "StatusCode "
                strOrderBy = "StatusCode"
                strFirstColumn = "StatusCode"
            Case "Product"
                strFields = "ProductType, "
                strFieldsInner = strFields
                strGroupBy = "ProductType "
                strOrderBy = "ProductType"
                strFirstColumn = "ProductType"
            Case "Dynamic"
                If (checkValue(strReportGroupBy)) Then
                    strFields = strReportGroupBy & ", "
                End If
                strFieldsInner = ""
                strGroupBy = ""
                strOrderBy = ""
                strFirstColumn = strReportGroupBy
            Case "Static"
                If (checkValue(strReportGroupBy)) Then
                    strFields = strReportGroupBy & ", "
                End If
                strFieldsInner = strFields
                strGroupBy = ""
                strOrderBy = ""
                strFirstColumn = strReportGroupBy
            Case Else
                strFields = "MediaCampaignIDInbound, MediaCampaignInboundName, "
                strFieldsInner = strFields
                strGroupBy = "MediaCampaignIDInbound, MediaCampaignInboundName "
                strOrderBy = "MediaCampaignInboundName"
                strFirstColumn = "MediaCampaignInboundName"
        End Select

        If (checkValue(strGroupBy) And checkValue(strReportGroupBy)) Then
            strGroupBy += ", " & strReportGroupBy
        Else
            strGroupBy += strReportGroupBy
        End If

        If (checkValue(strOrderBy) And checkValue(strReportOrderBy)) Then
            strOrderBy += ", " & strReportOrderBy
        Else
            strOrderBy += strReportOrderBy
        End If

        If (boolDataStore = True) Or (boolStatusDate = True) Or (boolDynamic = False) Then ' Pivot start
            strSQL = "SELECT " & strFields & " Count(*) AS Created "

            If (boolDynamic = False) Then ' If not dynamaic 
                For x As Integer = 0 To arrStatusDatesFields.Count - 1
                    strSQL += ", SUM(ISNULL(" & arrStatusDatesFields.Item(x) & ",0)) AS " & arrStatusDatesFields.Item(x) & " "
                Next
            Else ' If dynamic
                If (strDaily = "Y" Or strWeekly = "Y" Or strDailySummary = "Y") Then
                    Dim dteCurrent As Date = CDate(dteStartDate)
                    If (strWeekly = "Y") Then
                        For i As Integer = 0 To UBound(arrReportBuilderColumnValues)  ' ** Start lists - Build list of values to be returned on inner SQL query
                            If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                                arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteCurrent & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteEndDate & " 23:59:59", "DTTM", Now)), "¬")
                                strDynamicField = arrDynamicField(0).ToString
                                If checkValue(arrDynamicField(1).ToString) Then
                                    If InStr(arrDynamicField(1).ToString, "StoredDataValue") Then boolDataStore = True ' Make sure that the data store inner join is added
                                    If InStr(arrDynamicField(1).ToString, "ApplicationStatusDate") Then boolStatusDate = True ' Make sure that the status date inner join is added
                                    arrDynamicFields.Add(arrDynamicField(1).ToString)
                                End If
                                strSQL += ", " & Replace(Replace(Replace(strDynamicField, "VW:", ""), "DS:", ""), "SD:", "") & " "

                                If InStr(strDynamicField, "VW:") Then
                                    Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "VW:[A-Za-z0-9]{0,100}")
                                    For x As Integer = 0 To objMatches.Count - 1
                                        If Not arrViewFields.Contains(Replace(objMatches.Item(x).Value, "VW:", "")) Then
                                            arrViewFields.Add(Replace(objMatches.Item(x).Value, "VW:", ""))
                                        End If
                                    Next
                                End If
                                If InStr(strDynamicField, "DS:") Then
                                    Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "DS:[A-Za-z0-9]{0,100}")
                                    For x As Integer = 0 To objMatches.Count - 1
                                        If Not arrDataStoreFields.Contains(Replace(objMatches.Item(x).Value, "DS:", "")) Then
                                            arrDataStoreFields.Add(Replace(objMatches.Item(x).Value, "DS:", ""))
                                        End If
                                    Next
                                End If
                                If InStr(strDynamicField, "SD:") Then
                                    Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "SD:[A-Za-z0-9]{0,100}")
                                    For x As Integer = 0 To objMatches.Count - 1
                                        If Not arrStatusDatesFields.Contains(Replace(objMatches.Item(x).Value, "SD:", "")) Then
                                            arrStatusDatesFields.Add(Replace(objMatches.Item(x).Value, "SD:", ""))
                                        End If
                                    Next
                                End If
                            ElseIf InStr(arrReportBuilderColumnValues(i), "VW:") Then
                                strTempField = Replace(arrReportBuilderColumnValues(i), "VW:", "")
                                strSQL += ", SUM(" & strTempField & ") AS " & strTempField & " "
                                If Not arrViewFields.Contains(strTempField) Then
                                    arrViewFields.Add(strTempField)
                                End If
                            ElseIf InStr(arrReportBuilderColumnValues(i), "DS:") Then
                                strTempField = Replace(arrReportBuilderColumnValues(i), "DS:", "")
                                strSQL += ", SUM(ISNULL(CONVERT(FLOAT," & strTempField & "),0)) AS " & strTempField & " "
                                If Not arrDataStoreFields.Contains(strTempField) Then
                                    arrDataStoreFields.Add(strTempField)
                                End If
                            ElseIf InStr(arrReportBuilderColumnValues(i), "SD:") Then
                                strTempField = Replace(arrReportBuilderColumnValues(i), "SD:", "")
                                strSQL += ", SUM(ISNULL(" & strTempField & ",0)) AS " & strTempField & " "
                                If Not arrStatusDatesFields.Contains(strTempField) Then
                                    arrStatusDatesFields.Add(strTempField)
                                End If
                            End If
                        Next
                    ElseIf (strDaily = "Y" Or strDailySummary = "Y") Then
                        Dim j As Integer = 0
                        While dteCurrent <= CDate(dteEndDate)
                            For i As Integer = 0 To UBound(arrReportBuilderColumnValues)  ' ** Start lists - Build list of values to be returned on inner SQL query
                                If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                                    arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteCurrent & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteCurrent & " 23:59:59", "DTTM", Now)), "¬")
                                    strDynamicField = arrDynamicField(0).ToString
                                    If checkValue(arrDynamicField(1).ToString) Then
                                        If InStr(arrDynamicField(1).ToString, "StoredDataValue") Then boolDataStore = True ' Make sure that the data store inner join is added
                                        If InStr(arrDynamicField(1).ToString, "ApplicationStatusDate") Then boolStatusDate = True ' Make sure that the status date inner join is added
                                        arrDynamicFields.Add(arrDynamicField(1).ToString)
                                    End If
                                    strSQL += ", " & Replace(Replace(Replace(strDynamicField, "VW:", ""), "DS:", ""), "SD:", "") & j & " "

                                    If InStr(strDynamicField, "VW:") Then
                                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "VW:[A-Za-z0-9]{0,100}")
                                        For x As Integer = 0 To objMatches.Count - 1
                                            If Not arrViewFields.Contains(Replace(objMatches.Item(x).Value, "VW:", "")) Then
                                                arrViewFields.Add(Replace(objMatches.Item(x).Value, "VW:", ""))
                                            End If
                                        Next
                                    End If
                                    If InStr(strDynamicField, "DS:") Then
                                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "DS:[A-Za-z0-9]{0,100}")
                                        For x As Integer = 0 To objMatches.Count - 1
                                            If Not arrDataStoreFields.Contains(Replace(objMatches.Item(x).Value, "DS:", "")) Then
                                                arrDataStoreFields.Add(Replace(objMatches.Item(x).Value, "DS:", ""))
                                            End If
                                        Next
                                    End If
                                    If InStr(strDynamicField, "SD:") Then
                                        Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "SD:[A-Za-z0-9]{0,100}")
                                        For x As Integer = 0 To objMatches.Count - 1
                                            If Not arrStatusDatesFields.Contains(Replace(objMatches.Item(x).Value, "SD:", "")) Then
                                                arrStatusDatesFields.Add(Replace(objMatches.Item(x).Value, "SD:", ""))
                                            End If
                                        Next
                                    End If
                                ElseIf InStr(arrReportBuilderColumnValues(i), "VW:") Then
                                    strTempField = Replace(arrReportBuilderColumnValues(i), "VW:", "")
                                    strSQL += ", SUM(" & strTempField & ") AS " & strTempField & j & " "
                                    If Not arrViewFields.Contains(strTempField) Then
                                        arrViewFields.Add(strTempField)
                                    End If
                                ElseIf InStr(arrReportBuilderColumnValues(i), "DS:") Then
                                    strTempField = Replace(arrReportBuilderColumnValues(i), "DS:", "")
                                    strSQL += ", SUM(ISNULL(CONVERT(FLOAT," & strTempField & "),0)) AS " & strTempField & j & " "
                                    If Not arrDataStoreFields.Contains(strTempField) Then
                                        arrDataStoreFields.Add(strTempField)
                                    End If
                                ElseIf InStr(arrReportBuilderColumnValues(i), "SD:") Then
                                    strTempField = Replace(arrReportBuilderColumnValues(i), "SD:", "")
                                    strSQL += ", SUM(ISNULL(" & strTempField & ",0)) AS " & strTempField & j & " "
                                    If Not arrStatusDatesFields.Contains(strTempField) Then
                                        arrStatusDatesFields.Add(strTempField)
                                    End If
                                End If
                            Next ' ** End lists
                            j += 1
                            dteCurrent = dteCurrent.AddDays(1)
                        End While
                    End If
                Else
                    For i As Integer = 0 To UBound(arrReportBuilderColumnValues)  ' ** Start lists - Build list of values to be returned on inner SQL query
                        If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                            arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteStartDate & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteEndDate & " 23:59:59", "DTTM", Now)), "¬")
                            strDynamicField = arrDynamicField(0).ToString
                            If checkValue(arrDynamicField(1).ToString) Then
                                If InStr(arrDynamicField(1).ToString, "StoredDataValue") Then boolDataStore = True ' Make sure that the data store inner join is added
                                If InStr(arrDynamicField(1).ToString, "ApplicationStatusDate") Then boolStatusDate = True ' Make sure that the status date inner join is added
                                arrDynamicFields.Add(arrDynamicField(1).ToString)
                            End If
                            strSQL += ", " & Replace(Replace(Replace(strDynamicField, "VW:", ""), "DS:", ""), "SD:", "") & " "

                            If InStr(strDynamicField, "VW:") Then
                                Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "VW:[A-Za-z0-9]{0,100}")
                                For x As Integer = 0 To objMatches.Count - 1
                                    If Not arrViewFields.Contains(Replace(objMatches.Item(x).Value, "VW:", "")) Then
                                        arrViewFields.Add(Replace(objMatches.Item(x).Value, "VW:", ""))
                                    End If
                                Next
                            End If
                            If InStr(strDynamicField, "DS:") Then
                                Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "DS:[A-Za-z0-9]{0,100}")
                                For x As Integer = 0 To objMatches.Count - 1
                                    If Not arrDataStoreFields.Contains(Replace(objMatches.Item(x).Value, "DS:", "")) Then
                                        arrDataStoreFields.Add(Replace(objMatches.Item(x).Value, "DS:", ""))
                                    End If
                                Next
                            End If
                            If InStr(strDynamicField, "SD:") Then
                                Dim objMatches As MatchCollection = Regex.Matches(strDynamicField, "SD:[A-Za-z0-9]{0,100}")
                                For x As Integer = 0 To objMatches.Count - 1
                                    If Not arrStatusDatesFields.Contains(Replace(objMatches.Item(x).Value, "SD:", "")) Then
                                        arrStatusDatesFields.Add(Replace(objMatches.Item(x).Value, "SD:", ""))
                                    End If
                                Next
                            End If
                        ElseIf InStr(arrReportBuilderColumnValues(i), "VW:") Then
                            strTempField = Replace(arrReportBuilderColumnValues(i), "VW:", "")
                            strSQL += ", SUM(" & strTempField & ") AS " & strTempField & " "
                            If Not arrViewFields.Contains(strTempField) Then
                                arrViewFields.Add(strTempField)
                            End If
                        ElseIf InStr(arrReportBuilderColumnValues(i), "DS:") Then
                            strTempField = Replace(arrReportBuilderColumnValues(i), "DS:", "")
                            strSQL += ", SUM(ISNULL(CONVERT(FLOAT," & strTempField & "),0)) AS " & strTempField & " "
                            If Not arrDataStoreFields.Contains(strTempField) Then
                                arrDataStoreFields.Add(strTempField)
                            End If
                        ElseIf InStr(arrReportBuilderColumnValues(i), "SD:") Then
                            strTempField = Replace(arrReportBuilderColumnValues(i), "SD:", "")
                            strSQL += ", SUM(ISNULL(" & strTempField & ",0)) AS " & strTempField & " "
                            If Not arrStatusDatesFields.Contains(strTempField) Then
                                arrStatusDatesFields.Add(strTempField)
                            End If
                        End If
                    Next ' ** End lists
                End If

            End If ' End If dynamic

            strSQL += "FROM (SELECT " & strFieldsInner & " CreatedDate "

            For x As Integer = 0 To arrViewFields.Count - 1 ' Add view fields to inner query
                strSQL += ", " & arrViewFields.Item(x) & " "
            Next
            For x As Integer = 0 To arrDynamicFields.Count - 1 ' Add dynamic fields to inner query
                strSQL += ", " & arrDynamicFields.Item(x) & " "
            Next
            If (checkValue(strReportGroupBy)) Then
                If (strNoDataStoreJoin <> "Y") Then
                    boolDataStore = True
                End If
            End If
            If (boolDataStore = True) Then ' Add data store pivot fields
                strSQL += ", StoredDataName, StoredDataValue "
            End If
            If ((boolStatusDate = True) Or (boolDynamic = False)) And (strNoDateJoin <> "N") Then ' Add status date pivot fields
                strSQL += ", ApplicationStatusDateName, CASE WHEN NOT ApplicationStatusDate IS NULL THEN 1.00 ELSE 0.00 END AS ApplicationStatusDate "
            End If

        ElseIf (boolView = True) Then ' Without Pivot
            Dim dteCurrent As Date = CDate(dteStartDate)
            If (strDaily = "Y" Or strDailySummary = "Y") Then
                While dteCurrent <= CDate(dteEndDate)
                    For i As Integer = 0 To UBound(arrReportBuilderViewColumnValues)
                        If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                            arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteCurrent & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteCurrent & " 23:59:59", "DTTM", Now)), "¬")
                            strDynamicField = arrDynamicField(0).ToString
                            strAdditionalFields += ", " & Replace(strDynamicField, "VW:", "") & " "
                        Else
                            strAdditionalFields += ", SUM(" & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & ") AS " & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & " "
                        End If
                    Next
                    dteCurrent = dteCurrent.AddDays(1)
                End While
            ElseIf (strWeekly = "Y") Then
                For i As Integer = 0 To UBound(arrReportBuilderViewColumnValues)
                    If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                        arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteCurrent & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteCurrent & " 23:59:59", "DTTM", Now)), "¬")
                        strDynamicField = arrDynamicField(0).ToString
                        strAdditionalFields += ", " & Replace(strDynamicField, "VW:", "") & " "
                    Else
                        strAdditionalFields += ", SUM(" & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & ") AS " & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & " "
                    End If
                Next
            Else
                For i As Integer = 0 To UBound(arrReportBuilderViewColumnValues)
                    If InStr(arrReportBuilderColumnValues(i), "¬") Then ' Split field if it uses a dynamic value, replace {StartDate} and {EndDate} if found
                        arrDynamicField = Split(Replace(Replace(arrReportBuilderColumnValues(i), "{StartDate}", formatField(dteStartDate & " 00:00:00", "DTTM", Now)), "{EndDate}", formatField(dteEndDate & " 23:59:59", "DTTM", Now)), "¬")
                        strDynamicField = arrDynamicField(0).ToString
                        strAdditionalFields += ", " & Replace(strDynamicField, "VW:", "") & " "
                    Else
                        strAdditionalFields += ", SUM(" & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & ") AS " & Replace(arrReportBuilderViewColumnValues(i), "VW:", "") & " "
                    End If
                Next
            End If
            strSQL = "SELECT " & strFields & strAdditionalFields & " "
        End If

        If (strDaily = "Y" Or strWeekly = "Y") Then
            strSQL += "FROM vwcallhistory "
        Else
            strSQL += "FROM vwreportcaselist "
        End If

        If (checkValue(strReportGroupBy)) Then
            If (strNoDataStoreJoin <> "Y") Then
                boolDataStore = True
            End If
        End If
        If (strDaily = "Y" Or strWeekly = "Y") Then
            If (boolDataStore = True) Then ' Data store join
                strSQL += "LEFT JOIN tbldatastore ON tbldatastore.AppID = vwcallhistory.AppID "
            End If
            If ((boolStatusDate = True) Or (boolDynamic = False) Or (boolStatusDateSearch = True)) And (strNoDateJoin <> "N") Then ' Status dates join
                strSQL += "LEFT JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = vwcallhistory.AppID  "
                strSQL += "WHERE Active = 1 AND vwcallhistory.CompanyID = '" & CompanyID & "' "
            Else
                strSQL += "WHERE Active = 1 AND vwcallhistory.CompanyID = '" & CompanyID & "' "
            End If
        Else
            If (boolDataStore = True) Then ' Data store join
                strSQL += "LEFT JOIN tbldatastore ON tbldatastore.AppID = vwreportcaselist.AppID "
            End If
            If ((boolStatusDate = True) Or (boolDynamic = False) Or (boolStatusDateSearch = True)) And (strNoDateJoin <> "N") Then ' Status dates join
                strSQL += "LEFT JOIN tblapplicationstatusdates ON tblapplicationstatusdates.ApplicationStatusDateAppID = vwreportcaselist.AppID  "
                strSQL += "WHERE Active = 1 AND vwreportcaselist.CompanyID = '" & CompanyID & "' "
            Else
                strSQL += "WHERE Active = 1 AND vwreportcaselist.CompanyID = '" & CompanyID & "' "
            End If
        End If

        ' ** Start Search Filters
        If (boolStatusDateSearch = True) Then
            Dim arrDateType As Array = Split(strDateType, ",")
            strSQL += "AND ("
            For x As Integer = 0 To UBound(arrDateType)
                If (x > 0) Then
                    strSQL += " OR ApplicationStatusDateName = '" & arrDateType(x) & "'"
                Else
                    strSQL += "ApplicationStatusDateName = '" & arrDateType(x) & "'"
                End If
            Next
            strSQL += ") AND ApplicationStatusDate >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND ApplicationStatusDate <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
        Else
            strSQL += "AND " & _
            strDateType & "Date >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
            strDateType & "Date <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "
        End If
        If checkValue(intClientID) Then
            Dim arrClientID = Split(intClientID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
		   If checkValue(intNetworkID) Then
            Dim arrNetworkID = Split(intNetworkID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrNetworkID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " NetworkID = '" & arrNetworkID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intClientContactID) Then
            Dim arrClientID = Split(intClientContactID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientContactID = '" & arrClientID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaIDInbound) Then
            Dim arrMediaIDInbound = Split(intMediaIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDInbound = '" & arrMediaIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intPurchaseMediaID) Then
            Dim arrPurchaseMediaID = Split(intPurchaseMediaID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrPurchaseMediaID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignPurchaseMediaID = '" & arrPurchaseMediaID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaIDOutbound) Then
            Dim arrMediaIDOutbound = Split(intMediaIDOutbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaIDOutbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaIDOutbound = '" & arrMediaIDOutbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDInbound) Then
            Dim arrMediaCampaignIDInbound = Split(intMediaCampaignIDInbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDInbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDInbound = '" & arrMediaCampaignIDInbound(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intClientID) Then
            Dim arrintClientID = Split(intClientID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrintClientID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientID = '" & arrintClientID(i) & "' "
            Next
            strSQL += ") "
        End If
		If checkValue(intNetworkID) Then
            Dim arrNetworkID = Split(intNetworkID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrNetworkID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " NetworID = '" & arrNetworkID(i) & "' "
            Next
            strSQL += ") "
        End If
			If checkValue(intClientContactID) Then
            Dim arrintClientContactID = Split(intClientContactID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrintClientContactID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ClientContactID = '" & arrintClientContactID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignIDOutbound) Then
            Dim arrMediaCampaignIDOutbound = Split(intMediaCampaignIDOutbound, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignIDOutbound)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignIDOutbound = '" & arrMediaCampaignIDOutbound(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignDialerGradeID) Then
            Dim arrMediaCampaignDialerGradeID = Split(intMediaCampaignDialerGradeID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignDialerGradeID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignDialerGradeID = '" & arrMediaCampaignDialerGradeID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(intMediaCampaignApplicationTemplateID) Then
            Dim arrMediaCampaignApplicationTemplateID = Split(intMediaCampaignApplicationTemplateID, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignApplicationTemplateID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignApplicationTemplateID = '" & arrMediaCampaignApplicationTemplateID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strStatusCode) Then
            Dim arrStatusCode = Split(strStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrStatusCode)
                If (i > 0) Then strSQL += "OR "
                strSQL += " StatusCode = '" & arrStatusCode(i) & "' "
            Next
            strSQL += ") "
            If checkValue(strSubStatusCode) Then
                Dim arrSubStatusCode = Split(Replace(strSubStatusCode, "BLANK", ""), ",")
                strSQL += "AND ("
                For i As Integer = 0 To UBound(arrSubStatusCode)
                    If (i > 0) Then strSQL += "OR "
                    strSQL += " SubStatusCode = '" & arrSubStatusCode(i) & "' "
                Next
                strSQL += ") "
            End If
        End If
        If checkValue(strExcludeStatusCode) Then
            Dim arrExcludeStatusCode = Split(strExcludeStatusCode, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrExcludeStatusCode)
                If (i > 0) Then strSQL += "AND "
                strSQL += " StatusCode <> '" & arrExcludeStatusCode(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strMediaCampaignProductType) Then
            Dim arrMediaCampaignProductType = Split(strMediaCampaignProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrMediaCampaignProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " MediaCampaignProductType = '" & arrMediaCampaignProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strProductType) Then
            Dim arrProductType = Split(strProductType, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrProductType)
                If (i > 0) Then strSQL += "OR "
                strSQL += " ProductType = '" & arrProductType(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strCallCentreUser) Then
            Dim arrCallCentreUserID = Split(strCallCentreUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrCallCentreUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " CallCentreUserID = '" & arrCallCentreUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strCallCentreTeam) Then
            Dim arrCallCentreTeamID = Split(strCallCentreTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrCallCentreTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " CallCentreTeamID = '" & arrCallCentreTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strSalesUser) Then
            Dim arrSalesUserID = Split(strSalesUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrSalesUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " SalesUserID = '" & arrSalesUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strSalesTeam) Then
            Dim arrSalesTeamID = Split(strSalesTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrSalesTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " SalesTeamID = '" & arrSalesTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strAdministratorTeam) Then
            Dim arrAdminTeamID = Split(strAdministratorTeam, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrAdminTeamID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " AdministratorTeamID = '" & arrAdminTeamID(i) & "' "
            Next
            strSQL += ") "
        End If
        If checkValue(strRepUser) Then
            Dim arrRepUserID = Split(strRepUser, ",")
            strSQL += "AND ("
            For i As Integer = 0 To UBound(arrRepUserID)
                If (i > 0) Then strSQL += "OR "
                strSQL += " RepUserID = '" & arrRepUserID(i) & "' "
            Next
            strSQL += ") "
        End If
        If (checkValue(intMinAmount)) Then
            strSQL += "AND (Amount >= '" & intMinAmount & "') "
        End If
        If (checkValue(intMaxAmount)) Then
            strSQL += "AND (Amount <= '" & intMaxAmount & "') "
        End If
        ' ** End  Search Filters
        If (LeadPlatform.Config.Supplier = True) Or (LeadPlatform.Config.Partner = True) Then
            strSQL += "AND ( "
            If (LeadPlatform.Config.Supplier = True) Then strSQL += "MediaIDInbound = '" & LeadPlatform.Config.SupplierCompanyID & "' "
            If (LeadPlatform.Config.Partner = True) Then
                If (LeadPlatform.Config.Supplier = True) Then strSQL += "OR "
                strSQL += "MediaIDOutbound = '" & LeadPlatform.Config.PartnerCompanyID & "' "
            End If
            If (LeadPlatform.Config.MasterMedia = True) Then
                If (LeadPlatform.Config.Supplier = True Or LeadPlatform.Config.Partner = True) Then strSQL += "OR "
                strSQL += "MediaCampaignPurchaseMediaID = '" & LeadPlatform.Config.MasterMediaID & "' "
            End If
            strSQL += ") "
        End If
        If (LeadPlatform.Config.Rep = True) Then
            strSQL += "AND (RepUserID = '" & Config.DefaultUserID & "') "
        End If
        If (strDaily = "Y" Or strWeekly = "Y") Then
            strSQL += "AND (UserActive = 1) "
        End If

        If (boolDataStore = True) Or (boolStatusDate = True) Or (boolDynamic = False) Then
            strSQL += ") A "
        End If

        If (checkValue(strReportGroupBy)) Then
            If (strReportGroupType = "Dynamic") Then
                arrDataStoreFields.Add(strReportGroupBy)
                boolDataStore = True
            End If
        End If
        If (boolDataStore = True) And (arrDataStoreFields.Count > 0) Then
            strSQL += "PIVOT (MIN(StoredDataValue) FOR StoredDataName IN ("
            For x As Integer = 0 To arrDataStoreFields.Count - 1 ' Add data store fields to pivot
                If (x > 0) Then strSQL += ", "
                strSQL += "[" & arrDataStoreFields.Item(x) & "] "
            Next
            strSQL += ")) B "
        End If

        If ((boolStatusDate = True) Or (boolDynamic = False)) And (arrStatusDatesFields.Count > 0) Then
            strSQL += " PIVOT (SUM(ApplicationStatusDate) FOR ApplicationStatusDateName IN ("
            For x As Integer = 0 To arrStatusDatesFields.Count - 1 ' Add status dates fields to pivot
                If (x > 0) Then strSQL += ", "
                strSQL += "[" & arrStatusDatesFields.Item(x) & "]"
            Next
            strSQL += ")) C "
        End If

        If (boolDynamic = False) Then ' Display the number of cases created if not dynamic
            arrStatusDatesFields.Insert(0, "Created")
        End If

        ' ** Remove null and blank groupings
		If (checkValue(strGroupBy) And checkValue(strOrderBy)) Then
			strSQL += "WHERE (" & strFirstColumn & " <> '' AND NOT " & strFirstColumn & " IS NULL) "
	
			strSQL += "GROUP BY " & strGroupBy & " " & _
					"ORDER BY " & strOrderBy
		End If

        strReportSQL = strSQL

        'responseWrite(strSQL)
        'responseEnd()

		If (strMode = "csv") Then
			'responseWrite(strSQL)
		End If

        Dim intColumnTemp As Integer = 0
        Dim intRowCount As Integer = 0
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (strMode = "csv") Then
                .Write("Categories")
            Else
                If (strDaily = "Y" Or strDailySummary = "Y") Then
                    .WriteLine(startHeaderRow("tablehead")) ' ** Start header
                    .WriteLine(startCol("smlc", "th"))
                    .WriteLine("&nbsp")
                    .WriteLine(endCol("th"))
                    Dim dteCurrent As Date = CDate(dteStartDate)
                    While dteCurrent <= CDate(dteEndDate)
                        .WriteLine(startCol("smlc", "th", arrReportBuilderColumnNames.Count))
                        .WriteLine(WeekdayName(Weekday(dteCurrent), True, FirstDayOfWeek.Sunday) & " " & dteCurrent.Day & " " & MonthName(Month(dteCurrent), True))
                        .WriteLine(endCol("th"))
                        dteCurrent = dteCurrent.AddDays(1)
                    End While
                    .WriteLine(endHeaderRow()) ' ** End header
                ElseIf (strWeekly = "Y") Then
                    .WriteLine(startHeaderRow("tablehead")) ' ** Start header
                    .WriteLine(startCol("smlc", "th"))
                    .WriteLine("&nbsp")
                    .WriteLine(endCol("th"))
                    Dim dteCurrent As Date = CDate(dteStartDate)
                    .WriteLine(startCol("smlc", "th", arrReportBuilderColumnNames.Count))
                    .WriteLine(WeekdayName(Weekday(dteCurrent), True, FirstDayOfWeek.Sunday) & " " & dteCurrent.Day & " " & MonthName(Month(dteCurrent), True) & " - " & WeekdayName(Weekday(CDate(dteEndDate)), True, FirstDayOfWeek.Sunday) & " " & CDate(dteEndDate).Day & " " & MonthName(Month(CDate(dteEndDate)), True))
                    .WriteLine(endCol("th"))
                    .WriteLine(endHeaderRow()) ' ** End header
                End If
                .WriteLine(startHeaderRow("tablehead")) ' ** Start header
                If (checkValue(strFirstColumn)) Then ' If no first automatic column defined, don't render
                    .WriteLine(startCol("smlc", "th"))
                    .WriteLine("&nbsp")
                    .WriteLine(endCol("th"))
                End If
            End If
            If (checkValue(strFirstColumn)) Then ' If no first automatic column defined, don't increment
                intColSpan += 1
            End If
            'intColSpan += 1
            If (boolDynamic = False) Then ' If not dynamaic 
                For x As Integer = 0 To arrStatusDatesFields.Count - 1
                    .WriteLine(startCol("smlc", "th"))
                    .WriteLine(Regex.Replace(arrStatusDatesFields.Item(x), "([A-Z0-9])", " $1"))
                    .WriteLine(endCol("th"))
                    intColSpan += 1
                Next
            Else
                If (strDaily = "Y" Or strDailySummary = "Y") Then
                    Dim dteCurrent As Date = CDate(dteStartDate)
                    While dteCurrent <= CDate(dteEndDate)
                        For x As Integer = 0 To UBound(arrReportBuilderColumnNames)
                            If (strMode = "csv") Then
                                .Write("," & arrReportBuilderColumnNames(x))
                            Else
                                .WriteLine(startCol("smlc " & arrReportBuilderColumnSort(x), "th"))
                                .WriteLine(arrReportBuilderColumnNames(x))
                                .WriteLine(endCol("th"))
                            End If
                            intColSpan += 1
                        Next
                        dteCurrent = dteCurrent.AddDays(1)
                    End While
                ElseIf (strWeekly = "Y") Then
                    Dim dteCurrent As Date = CDate(dteStartDate)
                    For x As Integer = 0 To UBound(arrReportBuilderColumnNames)
                        If (strMode = "csv") Then
                            .Write("," & arrReportBuilderColumnNames(x))
                        Else
                            .WriteLine(startCol("smlc " & arrReportBuilderColumnSort(x), "th"))
                            .WriteLine(arrReportBuilderColumnNames(x))
                            .WriteLine(endCol("th"))
                        End If
                        intColSpan += 1
                    Next
                Else
                    For x As Integer = 0 To UBound(arrReportBuilderColumnNames)
                        If (strMode = "csv") Then
                            .Write("," & arrReportBuilderColumnNames(x))
                        Else
                            .WriteLine(startCol("smlc " & arrReportBuilderColumnSort(x), "th"))
                            .WriteLine(arrReportBuilderColumnNames(x))
                            .WriteLine(endCol("th"))
                        End If
                        intColSpan += 1
                    Next
                End If
            End If
            If (strMode = "csv") Then
                .Write(vbCrLf) ' ** End header with a line break
            Else
                .WriteLine(endHeaderRow()) ' ** End header
            End If

            Dim dsCache As DataTable
            If (checkValue(intCachedMinutes)) Then
                'If (strMode = "csv") Then
                'responseWrite(strReportSQL & " - " & intCachedMinutes)
                'responseEnd()
                'End If
                dsCache = New Caching(CacheObject(), strSQL, "", Config.DefaultDateTime.AddMinutes(intCachedMinutes), "").returnCache()
            Else
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            End If
            If (dsCache.Rows.Count > 0) Then

                For Each Row As DataRow In dsCache.Rows ' ** Start rows
                    If (strMode = "csv") Then
                        ' No need to start row
                    Else
                        .WriteLine(startRow(strClass))
                    End If
                    ' Row headings
                    Dim arrParams As Object() = {Row}
                    intColumn = 0
                    If (checkValue(strFirstColumn)) Then ' If no first automatic column defined, don't render
                        ReDim Preserve arrParams(2)

                        arrParams(1) = strFirstColumn
                        arrParams(2) = ""
                        If (strMode = "csv") Then
                            .Write(Row.Item(arrParams(1)))
                        Else
                            .WriteLine(executeSub(Me, "colDataStore", arrParams))
                        End If
                    End If
                    ReDim Preserve arrParams(0)

                    If (boolDynamic = True) Then ' If dynamaic
                        If (strDaily = "Y" Or strDailySummary = "Y") Then
                            Dim dteCurrent As Date = CDate(dteStartDate)
                            Dim j As Integer = 0
                            While dteCurrent <= CDate(dteEndDate)
                                For x As Integer = 0 To UBound(arrReportBuilderColumnValues)
                                    Dim arrField = Split(arrReportBuilderColumnValues(x), "¬")
                                    strDynamicField = arrField(0).ToString
                                    Dim arrAlias As String() = Split(strDynamicField, " AS ")
                                    ReDim Preserve arrParams(2)
                                    arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match 
                                    arrParams(2) = arrReportBuilderColumnFormats(x)
                                    If (strMode = "csv") Then
                                        .Write("," & Row.Item(arrParams(1)))
                                    Else
                                        If (checkValue(arrReportBuilderClickThrough(x))) Then
                                            ReDim Preserve arrParams(3)
                                            arrParams(1) = arrAlias(UBound(arrAlias)) & j ' Gets the last match 
                                            arrParams(2) = Replace(arrReportBuilderClickThroughQueryString(x), "{CallCentreUserID}", Row.Item("CallCentreUserID")) & "&frmStartDate=" & dteCurrent & "&frmEndDate=" & dteCurrent
                                            arrParams(3) = arrReportBuilderColumnFormats(x)
                                            .WriteLine(executeSub(Me, arrReportBuilderClickThrough(x), arrParams))
                                        Else
                                            ReDim Preserve arrParams(2)
                                            arrParams(1) = arrAlias(UBound(arrAlias)) & j ' Gets the last match
                                            arrParams(2) = arrReportBuilderColumnFormats(x)
                                            .WriteLine(executeSub(Me, "colDataStore", arrParams))
                                        End If
                                    End If
                                    ReDim Preserve arrParams(0)
                                    intColumn += 1
                                Next
                                j += 1
                                dteCurrent = dteCurrent.AddDays(1)
                            End While
                        ElseIf (strWeekly = "Y") Then
                            Dim dteCurrent As Date = CDate(dteStartDate)
                            For x As Integer = 0 To UBound(arrReportBuilderColumnValues)
                                Dim arrField = Split(arrReportBuilderColumnValues(x), "¬")
                                strDynamicField = arrField(0).ToString
                                Dim arrAlias As String() = Split(strDynamicField, " AS ")
                                ReDim Preserve arrParams(2)
                                arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match 
                                arrParams(2) = arrReportBuilderColumnFormats(x)
                                If (strMode = "csv") Then
                                    .Write("," & Row.Item(arrParams(1)))
                                Else
                                    If (checkValue(arrReportBuilderClickThrough(x))) Then
                                        ReDim Preserve arrParams(3)
                                        arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match 
                                        arrParams(2) = Replace(arrReportBuilderClickThroughQueryString(x), "{CallCentreUserID}", Row.Item("CallCentreUserID")) & "&frmStartDate=" & dteCurrent & "&frmEndDate=" & dteEndDate
                                        arrParams(3) = arrReportBuilderColumnFormats(x)
                                        .WriteLine(executeSub(Me, arrReportBuilderClickThrough(x), arrParams))
                                    Else
                                        ReDim Preserve arrParams(2)
                                        arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match
                                        arrParams(2) = arrReportBuilderColumnFormats(x)
                                        .WriteLine(executeSub(Me, "colDataStore", arrParams))
                                    End If
                                End If
                                ReDim Preserve arrParams(0)
                                intColumn += 1
                            Next
                        Else
                            For x As Integer = 0 To UBound(arrReportBuilderColumnValues)
                                intColumn = x
                                If InStr(arrReportBuilderColumnValues(x), "¬") Then
                                    Dim arrField = Split(arrReportBuilderColumnValues(x), "¬")
                                    strDynamicField = arrField(0).ToString
                                    Dim arrAlias As String() = Split(strDynamicField, " AS ")
                                    ReDim Preserve arrParams(2)
                                    arrParams(1) = arrAlias(UBound(arrAlias)) ' Gets the last match 
                                    arrParams(2) = arrReportBuilderColumnFormats(x)
                                    If (strMode = "csv") Then
                                        .Write("," & Row.Item(arrParams(1)))
                                    Else
                                        If (checkValue(arrReportBuilderClickThrough(x))) Then
                                            ReDim Preserve arrParams(3)
                                            arrParams(1) = arrAlias(UBound(arrAlias))
                                            arrParams(2) = "&frmStartDate=" & dteStartDate & "&frmEndDate=" & dteEndDate & clickThroughReplace(arrReportBuilderClickThroughQueryString(x), Row)
                                            arrParams(3) = arrReportBuilderColumnFormats(x)
                                            .WriteLine(executeSub(Me, arrReportBuilderClickThrough(x), arrParams))
                                        Else
                                            .WriteLine(executeSub(Me, "colDataStore", arrParams))
                                        End If
                                    End If
                                    ReDim Preserve arrParams(0)
                                ElseIf InStr(arrReportBuilderColumnValues(x), "VW:") Or InStr(arrReportBuilderColumnValues(x), "DS:") Or InStr(arrReportBuilderColumnValues(x), "SD:") Then
                                    ReDim Preserve arrParams(2)
                                    arrParams(1) = Replace(Replace(Replace(arrReportBuilderColumnValues(x), "VW:", ""), "DS:", ""), "SD:", "")
                                    arrParams(2) = arrReportBuilderColumnFormats(x)
                                    If (strMode = "csv") Then
                                        .Write("," & Row.Item(arrParams(1)))
                                    Else
                                        If (checkValue(arrReportBuilderClickThrough(x))) Then
                                            ReDim Preserve arrParams(3)
                                            arrParams(1) = Replace(Replace(Replace(arrReportBuilderColumnValues(x), "VW:", ""), "DS:", ""), "SD:", "")
                                            arrParams(2) = "&frmStartDate=" & dteStartDate & "&frmEndDate=" & dteEndDate & clickThroughReplace(arrReportBuilderClickThroughQueryString(x), Row)
                                            arrParams(3) = arrReportBuilderColumnFormats(x)
                                            .WriteLine(executeSub(Me, arrReportBuilderClickThrough(x), arrParams))
                                        Else
                                            .WriteLine(executeSub(Me, "colDataStore", arrParams))
                                        End If
                                    End If
                                    ReDim Preserve arrParams(0)
                                Else
                                    If (strMode = "csv") Then
                                        .Write("," & Row.Item(arrReportBuilderColumnValues(x)).ToString)
                                    Else
                                        If (checkValue(arrReportBuilderClickThrough(x))) Then
                                            ReDim Preserve arrParams(3)
                                            arrParams(1) = arrReportBuilderColumnValues(x)
                                            arrParams(2) = "&frmStartDate=" & dteStartDate & "&frmEndDate=" & dteEndDate & clickThroughReplace(arrReportBuilderClickThroughQueryString(x), Row)
                                            arrParams(3) = arrReportBuilderColumnFormats(x)
                                            .WriteLine(executeSub(Me, arrReportBuilderClickThrough(x), arrParams))
                                        Else
                                            .WriteLine(executeSub(Me, arrReportBuilderColumnValues(x), arrParams))
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Else ' Standard report
                        Select Case strReportGroupType
                            Case "Grade"
                                strFilter = "&frmMediaCampaignDialerGrade=" & Row.Item("MediaCampaignDialerGradeID") & "&frmMediaIDInbound=" & intMediaIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "Media"
                                strFilter = "&frmMediaIDInbound=" & Row.Item("MediaIDInbound") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "MediaOutbound"
                                strFilter = "&frmMediaIDInbound=" & intMediaIDOutbound & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & Row.Item("MediaIDOutbound") & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "MediaCampaignOutbound"
                                strFilter = "&frmMediaIDInbound=" & intMediaIDOutbound & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & Row.Item("MediaCampaignIDOutbound") & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "Product"
                                strFilter = "&frmMediaIDInbound=" & intMediaIDOutbound & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & Row.Item("ProductType") & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "SalesUser"
                                strFilter = "&frmSalesUser=" & Row.Item("SalesUserID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "AdministratorUser"
                                strFilter = "&frmAdministratorUser=" & Row.Item("AdministratorUserID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "CallCentreUser"
                                strFilter = "&frmCallCentreUser=" & Row.Item("CallCentreUserID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "ClientID"
                                strFilter = "&frmClientID=" & Row.Item("ClientID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "ClientContactID"
                                strFilter = "&frmClientContactID=" & Row.Item("ClientContactID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                            Case "NetworkID"
                                strFilter = "&frmNetworkID=" & Row.Item("NetworkID") & "&frmMediaCampaignIDInbound=" & intMediaCampaignIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                           
						    Case Else
                                strFilter = "&frmMediaCampaignIDInbound=" & Row.Item("MediaCampaignIDInbound") & "&frmMediaCampaignApplicationTemplateID=" & intMediaCampaignApplicationTemplateID & "&frmMediaIDInbound=" & intMediaIDInbound & "&frmMediaIDOutbound=" & intMediaIDOutbound & "&frmMediaCampaignIDOutbound=" & intMediaCampaignIDOutbound & "&frmStatusCode=" & strStatusCode & "&frmSubStatusCode=" & strSubStatusCode & "&frmExcludeStatusCode=" & strExcludeStatusCode & "&frmProductType=" & strProductType & "&frmMediaCampaignProductType=" & strMediaCampaignProductType
                        End Select
                        For x As Integer = 0 To arrStatusDatesFields.Count - 1
                            intColumn = x + intColumnTemp
                            If (strMode = "csv") Then
                                .Write(Row.Item(arrStatusDatesFields(x)))
                            Else
                                .WriteLine(colClickThroughReportBuilder(Row, arrStatusDatesFields(x), "&frmDateType=" & strDateType & "&frmStartDate=" & dteStartDate & "&frmEndDate=" & dteEndDate & strFilter & "&frmMediaCampaignDialerGrade=" & intMediaCampaignDialerGradeID & "&frmAdditionalDateType=" & arrStatusDatesFields(x), "I"))
                            End If
                        Next
                    End If

                    If (strMode = "csv") Then
                        If (dsCache.Rows.IndexOf(Row) < dsCache.Rows.Count - 1) Then
                            .Write(vbCrLf)
                        End If
                    Else
                        .WriteLine(endRow())
                    End If
                    strClass = nextClass(strClass)

                    If (strMode = "csv") Then
                        ' No total column
                    Else
                        If (dsCache.Rows.IndexOf(Row) = (dsCache.Rows.Count - 1)) Then ' Total row
                            .WriteLine(endBody())
                            .WriteLine(startFooter(""))
                            .WriteLine(startRow(strClass, "", "TotalRow"))
                            If (checkValue(strFirstColumn)) Then ' If no first automatic column defined, don't render
                                .WriteLine(colNBSP(Row))
                            End If

                            If (boolDynamic = False) Then ' If not dynamic
                                If (strDaily = "Y" Or strDailySummary = "Y") Then
                                    Dim dteCurrent As Date = CDate(dteStartDate)
                                    intColumn = 0
                                    While dteCurrent <= CDate(dteEndDate)
                                        For x = 0 To arrStatusDatesFields.Count - 1
                                            .WriteLine(executeSub(Me, "colTotal", arrParams))
                                            intColumn += 1
                                        Next
                                        dteCurrent = dteCurrent.AddDays(1)
                                    End While
                                ElseIf (strWeekly = "Y") Then
                                    Dim dteCurrent As Date = CDate(dteStartDate)
                                    intColumn = 0
                                    For x = 0 To arrStatusDatesFields.Count - 1
                                        .WriteLine(executeSub(Me, "colTotal", arrParams))
                                        intColumn += 1
                                    Next
                                Else
                                    For x = 0 To arrStatusDatesFields.Count - 1
                                        intColumn = x
                                        .WriteLine(executeSub(Me, "colTotal", arrParams))
                                    Next
                                End If
                            Else ' If dynamic
                                If (strDaily = "Y" Or strDailySummary = "Y") Then
                                    Dim dteCurrent As Date = CDate(dteStartDate)
                                    intColumn = 0
                                    While dteCurrent <= CDate(dteEndDate)
                                        For x As Integer = 0 To UBound(arrReportBuilderColumnTotals)
                                            If (checkValue(arrReportBuilderColumnTotals(x))) Then
                                                .WriteLine(executeSub(Me, arrReportBuilderColumnTotals(x), arrParams))
                                                intColumn += 1
                                            Else
                                                .WriteLine(colNBSP(Row))
                                            End If
                                        Next
                                        dteCurrent = dteCurrent.AddDays(1)
                                    End While
                                ElseIf (strWeekly = "Y") Then
                                    Dim dteCurrent As Date = CDate(dteStartDate)
                                    intColumn = 0
                                    For x As Integer = 0 To UBound(arrReportBuilderColumnTotals)
                                        If (checkValue(arrReportBuilderColumnTotals(x))) Then
                                            .WriteLine(executeSub(Me, arrReportBuilderColumnTotals(x), arrParams))
                                            intColumn += 1
                                        Else
                                            .WriteLine(colNBSP(Row))
                                        End If
                                    Next
                                Else
                                    For x As Integer = 0 To UBound(arrReportBuilderColumnTotals)
                                        intColumn = x
                                        If (checkValue(arrReportBuilderColumnTotals(x))) Then
                                            .WriteLine(executeSub(Me, arrReportBuilderColumnTotals(x), arrParams))
                                        Else
                                            .WriteLine(colNBSP(Row))
                                        End If
                                    Next
                                End If
                            End If
                            .WriteLine(endRow())
                            strClass = nextClass(strClass)
                        End If
                    End If

                    intRowsCount += 1

                Next ' ** End rows

                If (strMode = "csv") Then
                    ' No record count
                Else
                    .WriteLine(startRow(strClass))
                    .WriteLine(startCol("smlr", "td", intColSpan))
                    If (dsCache.Rows.Count >= CInt(intSelectTop)) Then
                        .WriteLine("<strong>Top " & intSelectTop & " record(s) returned.</strong>")
                    Else
                        .WriteLine("<strong>" & dsCache.Rows.Count & " record(s) returned.</strong>")
                    End If
                    .WriteLine(endCol("td"))
                    .WriteLine(endRow())
                    .WriteLine(endFooter())
                End If

            Else
                If (strMode = "csv") Then
                    ' Nothing to return
                Else
                    .WriteLine(startBody())
                    .WriteLine(startRow(strClass))
                    .WriteLine(startCol("smlc", "td", intColSpan))
                    .WriteLine("No results found.")
                    .WriteLine(endCol("td"))
                    .WriteLine(endRow())
                    .WriteLine(endBody())
                End If
            End If
        End With
        responseWrite(objStringWriter.ToString)
    End Sub

    Public Sub listStatus()
        Dim intColSpan As Integer = 0
        Dim strSQL As String = ""
        Dim arrStatus As New List(Of String)

        strSQL = "SELECT StatusCode FROM tblstatuses WHERE (StatusActive = 1) AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY StatusOrder"
        Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                arrStatus.Add(Row.Item("StatusCode"))
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT * FROM " & _
            "(SELECT MediaID, MediaName, StatusCode " & _
            "FROM vwreportcasesummary " & _
            "WHERE Active = 1 AND CompanyID = '" & CompanyID & "' " & _
            "AND CreatedDate >= " & formatField(dteStartDate & " 00:00:00", "DTTM", Now) & " AND " & _
            "CreatedDate <= " & formatField(dteEndDate & " 23:59:59", "DTTM", Now) & " "

        ' This next part doesn't work as the view isn't setup correctly.
        If (LeadPlatform.Config.Supplier = True) Or (LeadPlatform.Config.Partner = True) Then
            strSQL += "AND ( "
            If (LeadPlatform.Config.Supplier = True) Then strSQL += "MediaIDInbound = '" & LeadPlatform.Config.SupplierCompanyID & "' "
            If (LeadPlatform.Config.Partner = True) Then
                If (LeadPlatform.Config.Supplier = True) Then strSQL += "OR "
                strSQL += "MediaIDOutbound = '" & LeadPlatform.Config.PartnerCompanyID & "' "
            End If
            If (LeadPlatform.Config.MasterMedia = True) Then
                If (LeadPlatform.Config.Supplier = True Or LeadPlatform.Config.Partner = True) Then strSQL += "OR "
                strSQL += "MediaCampaignPurchaseMediaID = '" & LeadPlatform.Config.MasterMediaID & "' "
            End If
            strSQL += ") "
        End If
        If (LeadPlatform.Config.Rep = True) Then
            strSQL += "AND (RepUserID = '" & Config.DefaultUserID & "') "
        End If

        strSQL += ") A " & _
            "PIVOT (Count(StatusCode) FOR StatusCode IN ("

        For x As Integer = 0 To arrStatus.Count - 1
            If (x > 0) Then strSQL += ", "
            strSQL += "[" & arrStatus.Item(x) & "]"
        Next

        strSQL += ")) B "

        strReportSQL = strSQL

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine(startHeaderRow("tablehead"))
            .WriteLine(startCol("smlc", "th"))
            .WriteLine("&nbsp")
            .WriteLine(endCol("th"))
            intColSpan += 1
            For x As Integer = 0 To arrStatus.Count - 1
                .WriteLine(startCol("smlc", "th"))
                .WriteLine(arrStatus.Item(x))
                .WriteLine(endCol("th"))
                intColSpan += 1
            Next
            .WriteLine(endHeaderRow())
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine(startRow(strClass))
                    .WriteLine(startCol("sml", "td", ""))
                    .WriteLine(Row.Item("MediaName"))
                    .WriteLine(endCol("td"))

                    Dim arrParams As Object() = {Row}
                    For x As Integer = 0 To arrStatus.Count - 1
                        .WriteLine(startCol("smlc", "td", ""))
                        .WriteLine(Row.Item(arrStatus.Item(x)))
                        .WriteLine(endCol("td"))
                    Next
                    .WriteLine(endRow())
                    strClass = nextClass(strClass)
                Next

                .WriteLine(startRow(strClass))
                .WriteLine(startCol("smlr", "td", intColSpan))
                If (dsCache.Rows.Count >= CInt(intSelectTop)) Then
                    .WriteLine("<strong>Top " & intSelectTop & " record(s) returned.</strong>")
                Else
                    .WriteLine("<strong>" & dsCache.Rows.Count & " record(s) returned.</strong>")
                End If
                .WriteLine(endCol("td"))
                .WriteLine(endRow())
                .WriteLine(endFooter())

            Else
                .WriteLine(startBody())
                .WriteLine(startRow(strClass))
                .WriteLine(startCol("smlc", "td", intColSpan))
                .WriteLine("No results found.")
                .WriteLine(endCol("td"))
                .WriteLine(endRow())
                .WriteLine(endBody())

            End If
        End With
        responseWrite(objStringWriter.ToString)

    End Sub

    Private Function executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(Reporting)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        Return objMethodInfo.Invoke(inst, params)
    End Function

    Public Function filterCallCentreUser() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmCallCentreUser"" name=""frmCallCentreUser"" multiple=""multiple"" class=""multiselect text small"" title=""Call Centre"" data-placement=""top"" data-tooltip=""User assigned from Call Centre"">")
            .WriteLine(callCentreUserTextDropdown(CacheObject, "", strCallCentreUser))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterCallCentreTeam() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmCallCentreTeam"" name=""frmCallCentreTeam"" multiple=""multiple"" class=""multiselect text small"" title=""Call Centre Team"" data-placement=""top"" data-tooltip=""Team assigned from Call Centre"">")
            .WriteLine(teamTextDropdown(CacheObject, "", strCallCentreTeam))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSalesUser() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmSalesUser"" name=""frmSalesUser"" multiple=""multiple"" class=""multiselect text small"" title=""Sales"" data-placement=""top"" data-tooltip=""User assigned from Sales"">")
            .WriteLine(salesUserTextDropdown(CacheObject, "", strSalesUser))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterAdministratorUser() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmAdministratorUser"" name=""frmAdministratorUser"" multiple=""multiple"" class=""multiselect text small"" title=""Admin"" data-placement=""top"" data-tooltip=""User assigned from Admin"">")
            .WriteLine(adminUserTextDropdown(CacheObject, "", strAdministratorUser))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSalesTeam() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmSalesTeam"" name=""frmSalesTeam"" multiple=""multiple"" class=""multiselect text small"" title=""Sales Team"" data-placement=""top"" data-tooltip=""Team assigned from Sales"">")
            .WriteLine(teamTextDropdown(CacheObject, "", strCallCentreTeam))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterAdministratorTeam() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmAdministratorTeam"" name=""frmAdministratorTeam"" multiple=""multiple"" class=""multiselect text small"" title=""Admin Team"" data-placement=""top"" data-tooltip=""Team assigned from Admin"">")
            .WriteLine(teamTextDropdown(CacheObject, "", strAdministratorTeam))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterRepUser() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmRepUser"" name=""frmRepUser"" multiple=""multiple"" class=""multiselect text small"" title=""Rep"" data-placement=""top"" data-tooltip=""Rep assigned to case"">")
            .WriteLine(repUserTextDropdown(CacheObject, "", strRepUser))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterReportType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmReportGroupType"" name=""frmReportGroupType"" class=""multiselect text small"" title=""Report Type"" data-placement=""top"" data-tooltip=""Different methods of grouping report"">")
            .WriteLine(reportGroupTextDropdown(CacheObject, "", strReportGroupType))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterClientID() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmClientID"" name=""frmClientID"" multiple=""multiple"" class=""multiselect text small"" title=""Broker"" data-placement=""top"" data-tooltip=""Broker that supplied data"">")
            .WriteLine(brokerDropdown(CacheObject, "", intClientID))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function
	
	Public Function filterNetworkID() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmNetworkID"" name=""frmNetworkID"" multiple=""multiple"" class=""multiselect text small"" title=""Network"" data-placement=""top"" data-tooltip=""Broker that supplied data"">")
            .WriteLine(businessObjectNetworks(CacheObject, "", intNetworkID))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterClientContactID() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmClientContactID"" name=""frmClientContactID"" multiple=""multiple"" class=""multiselect text small"" title=""Contact"" data-placement=""top"" data-tooltip=""Contact that supplied data"">")
            .WriteLine(contactDropdown(CacheObject, "", intClientContactID))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaIDInbound() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaIDInbound"" name=""frmMediaIDInbound"" multiple=""multiple"" class=""multiselect text small"" title=""Inbound Media"" data-placement=""top"" data-tooltip=""Company that supplied data"">")
            .WriteLine(mediaTextDropdown(CacheObject, "", intMediaIDInbound))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterPurchaseMediaID() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmPurchaseMediaID"" name=""frmPurchaseMediaID"" multiple=""multiple"" class=""multiselect text small"" title=""Purchase Media"" data-placement=""top"" data-tooltip=""Data purchased by company"">")
            .WriteLine(mediaTextDropdown(CacheObject, "", intPurchaseMediaID))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaIDOutbound() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaIDOutbound"" name=""frmMediaIDOutbound"" multiple=""multiple"" class=""multiselect text small"" title=""Outbound Media"" data-placement=""top"" data-tooltip=""Company that data has been sent to"">")
            .WriteLine(mediaTextDropdown(CacheObject, "", intMediaIDOutbound))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaCampaignIDInbound() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaCampaignIDInbound"" name=""frmMediaCampaignIDInbound"" multiple=""multiple"" class=""multiselect text small"" title=""Inbound Campaign"" data-placement=""top"" data-tooltip=""Sub source of received data"">")
            .WriteLine(mediaCampaignTextDropdown(CacheObject, "", intMediaCampaignIDInbound, "0"))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaCampaignIDOutbound() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaCampaignIDOutbound"" name=""frmMediaCampaignIDOutbound"" multiple=""multiple"" class=""multiselect text small"" title=""Outbound Campaign"" data-placement=""top"" data-tooltip=""Sub source of data sent to partner"">")
            .WriteLine(mediaCampaignTextDropdown(CacheObject, "", intMediaCampaignIDOutbound, "1"))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaCampaignDialerGrade() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaCampaignDialerGrade"" name=""frmMediaCampaignDialerGrade"" multiple=""multiple"" class=""multiselect text small"" title=""Data Grade"" data-placement=""top"" data-tooltip=""Type of data"">")
            .WriteLine(mediaCampaignGradeTextDropdown(CacheObject, "", intMediaCampaignDialerGradeID))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmStatusCode"" name=""frmStatusCode"" multiple=""multiple"" class=""multiselect text small"" title=""Status"" data-placement=""top"" data-tooltip=""Current case status"">")
            .WriteLine(statusTextDropdown(CacheObject, "", strStatusCode, True))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterExcludeStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmExcludeStatusCode"" name=""frmExcludeStatusCode"" multiple=""multiple"" class=""multiselect text small"" title=""Exclude Status"" data-placement=""top"" data-tooltip=""Exclude current case status"">")
            .WriteLine(statusTextDropdown(CacheObject, "", strExcludeStatusCode, True))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSubStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmSubStatusCode"" name=""frmSubStatusCode"" multiple=""multiple"" class=""multiselect text small"" title=""Sub Status"" data-placement=""top"" data-tooltip=""Current sub-status"">")
            .WriteLine(subStatusTextDropdown(CacheObject, "", strSubStatusCode, True))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterDateType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmDateType"" name=""frmDateType"" multiple=""multiple"" class=""multiselect text small"" title=""Date Type"" data-placement=""top"" data-tooltip=""Milestone date marker"">")
            .WriteLine(dateTypeTextDropdown(CacheObject, "", strDateType, True))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterExcludeDateType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmExcludeDate"" name=""frmExcludeDate"" multiple=""multiple"" class=""multiselect text small"" title=""Exclude Date Type"" data-placement=""top"" data-tooltip=""Exclude milestone date marker"">")
            .WriteLine(dateTypeTextDropdown(CacheObject, "", strExcludeDate, True))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMediaCampaignProductType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmMediaCampaignProductType"" name=""frmMediaCampaignProductType"" multiple=""multiple"" class=""multiselect text small"" title=""Campaign Product"" data-placement=""top"" data-tooltip=""Default product for data source"">")
            .WriteLine(productTypeTextDropdown(CacheObject, "", strMediaCampaignProductType))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterProductType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmProductType"" name=""frmProductType"" multiple=""multiple"" class=""multiselect text small"" title=""Product"" data-placement=""top"" data-tooltip=""Product type on case"">")
            .WriteLine(productTypeTextDropdown(CacheObject, "", strProductType))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterDiaryType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmDiaryType"" name=""frmDiaryType"" multiple=""multiple"" class=""multiselect text small"" title=""Diary Type"" data-placement=""top"" data-tooltip=""Type of task"">")
            .WriteLine(diaryTypeTextDropdown(CacheObject, "", strDiaryType))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterStartDate() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmStartDate"" name=""frmStartDate"" value=""" & dteStartDate & """ class=""text input-small date"" readonly=""readonly"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterEndDate() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmEndDate"" name=""frmEndDate"" value=""" & dteEndDate & """ class=""text input-small date"" readonly=""readonly"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSelectTop() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmSelectTop"" name=""frmSelectTop"" value=""" & intSelectTop & """ class=""text input-tiny regex set-example-text ui-tooltip"" rel=""^[0-9]{1,5}$"" title=""Number of results to return"" placeholder=""Top"" data-placement=""top"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMinDialAttempts() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<span class=""sml"">Min Calls:</span> <input type=""text"" id=""frmMinDialAttempts"" name=""frmMinDialAttempts"" value=""" & intMinDialAttempts & """ class=""text input-tiny regex"" rel=""^[0-9]{0,3}$"" title=""Min Calls"" placeholder=""Min Calls"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMaxDialAttempts() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<span class=""sml"">Max Calls:</span> <input type=""text"" id=""frmMaxDialAttempts"" name=""frmMaxDialAttempts"" value=""" & intMaxDialAttempts & """ class=""text input-tiny regex"" rel=""^[0-9]{0,3}$"" title=""Max Calls"" placeholder=""Max Calls"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMinAmount() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<span class=""sml"">Min Amt:</span> <input type=""text"" id=""frmMinAmount"" name=""frmMinAmount"" value=""" & intMinAmount & """ class=""text input-mini regex"" rel=""^[0-9]{0,10}$"" title=""Min Amt"" placeholder=""Min Amt"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterMaxAmount() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<span class=""sml"">Max Amt:</span> <input type=""text"" id=""frmMaxAmount"" name=""frmMaxAmount"" value=""" & intMaxAmount & """ class=""text input-mini regex"" rel=""^[0-9]{0,10}$"" title=""Max Amt"" placeholder=""Max Amt"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterAppID() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmAppID"" name=""frmAppID"" value=""" & intAppID & """ class=""text input-small regex ui-tooltip"" rel=""^[0-9,]{0,64}$"" title=""AppID(s): separate with commas"" placeholder=""AppID(s)"" data-placement=""top"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSurname() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmSurname"" name=""frmSurname"" value=""" & strSurname & """ class=""text  input-medium regex"" rel=""^([a-z' \-?]{0,50})$"" title=""Surname"" placeholder=""Surname"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterDOB() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmDOB"" name=""frmDOB"" value=""" & dteDOB & """ class=""text input-small regex"" rel=""^((0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d){0,10}$"" title=""Date Of Birth"" placeholder=""Date Of Birth"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterTelephoneNumber() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmTelephoneNumber"" name=""frmTelephoneNumber"" value=""" & strTelephoneNumber & """ class=""text input-medium regex"" rel=""^[0-9 ]{0,13}$"" title=""Telephone Number"" placeholder=""Telephone Number"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterEmailAddress() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmEmailAddress"" name=""frmEmailAddress"" value=""" & strEmailAddress & """ class=""text small regex"" rel=""^([A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}){0,100}$"" title=""Email Address"" placeholder=""Email Address"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterPostCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""text"" id=""frmPostCode"" name=""frmPostCode"" value=""" & strPostCode & """ class=""text input-small regex"" rel=""^[0-9a-z- ]{0,9}$"" title=""Post Code"" placeholder=""Post Code"" />")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function filterSelectColumns() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<select id=""frmSelectColumns"" name=""frmSelectColumns"" multiple=""multiple"" class=""text small"">")
            .WriteLine("<option value="""">--Select Columns--</options>")
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenAdhocFilter() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmAdhocFilter"" name=""frmAdhocFilter"" value=""" & strAdhocFilter & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenAdhocSearch() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmSearchAdhoc"" name=""frmSearchAdhoc"" value=""" & strSearchAdhoc & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenDateType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmDateType"" name=""frmDateType"" value=""" & strDateType & """>")
        End With
        Return objStringWriter.ToString
    End Function
	
    Public Function hiddenStartDate() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmStartDate"" name=""frmStartDate"" value=""" & dteStartDate & """>")
        End With
        Return objStringWriter.ToString
    End Function
	
    Public Function hiddenEndDate() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmEndDate"" name=""frmEndDate"" value=""" & dteEndDate & """>")
        End With
        Return objStringWriter.ToString
    End Function	

    Public Function hiddenReportGroupType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmReportGroupType"" name=""frmReportGroupType"" value=""" & strReportGroupType & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenReportGroupBy() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmReportGroupBy"" name=""frmReportGroupBy"" value=""" & strReportGroupBy & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenReportOrderBy() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmReportOrderBy"" name=""frmReportOrderBy"" value=""" & strReportOrderBy & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenProductType() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmReportGroupType"" name=""frmProductType"" value=""" & strProductType & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenMediaCampaignDialerGrade() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmMediaCampaignDialerGrade"" name=""frmMediaCampaignDialerGrade"" value=""" & intMediaCampaignDialerGradeID & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmStatusCode"" name=""frmStatusCode"" value=""" & strStatusCode & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenSubStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmSubStatusCode"" name=""frmSubStatusCode"" value=""" & strSubStatusCode & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function hiddenExcludeStatusCode() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<input type=""hidden"" id=""frmExcludeStatusCode"" name=""frmExcludeStatusCode"" value=""" & strExcludeStatusCode & """>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function adhocFilter(txt As String, val As String, name As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
			Dim strFriendlyName As String = getAnyField("ApplicationFriendlyName", "tblimportvalidation", "ValName", name)
            Dim strToolTip As String = getAnyField("ApplicationToolTip", "tblimportvalidation", "ValName", name)
            If (strToolTip = "") Then strToolTip = strFriendlyName
			If (strFriendlyName = "") Then strFriendlyName = name
            .WriteLine("<select id=""frmAdhoc" & name & """ name=""frmAdhoc" & name & """ multiple=""multiple"" data-placement=""top"" title=""" & strFriendlyName & """ class= ""multiselect text small ui-tooltip"" data-tooltip=""" & strToolTip & """>")
            .WriteLine(importValidationTextDropdown(CacheObject(), txt, val, name))
            .WriteLine("</select>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function adhocSearch(txt As String, val As String, name As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strFriendlyName As String = getAnyField("ApplicationFriendlyName", "tblimportvalidation", "ValName", name)
            Dim strToolTip As String = getAnyField("ApplicationToolTip", "tblimportvalidation", "ValName", name)
            Dim strRegEx As String = getAnyField("ValRegex", "tblimportvalidation", "ValName", name)
            If (strToolTip = "") Then strToolTip = strFriendlyName
            If (strFriendlyName = "") Then strFriendlyName = name
            .WriteLine("<input type=""text"" id=""frmSearchAdhoc" & name & """ name=""frmSearchAdhoc" & name & """ value=""" & val & """ class=""text input-small ui-tooltip"" rel=""" & strRegEx & """ title=""" & strFriendlyName & """ placeholder=""" & strFriendlyName & """ data-tooltip=""" & strToolTip & """ data-placement=""top"" />")
            Return objStringWriter.ToString
        End With
        Return objStringWriter.ToString
    End Function

    Private Function startBody() As String
        Return "<tbody>"
    End Function

    Private Function endBody() As String
        Return "<script>//resetReportBox();</script></tbody>"
    End Function

    Private Function startHeaderRow(ByVal cls As String) As String
        Dim strHeaderRow As String = ""
        strHeaderRow += "<thead>" & vbCrLf
        strHeaderRow += "<tr class=""" & cls & """>"
        Return strHeaderRow
    End Function

    Private Function endHeaderRow() As String
        Dim strHeaderRow As String = ""
        strHeaderRow += "</tr>"
        strHeaderRow += "</thead>" & vbCrLf
        Return strHeaderRow
    End Function

    Private Function startFooter(ByVal cls As String) As String
        Dim strHeaderRow As String = ""
        strHeaderRow += "<tfoot>" & vbCrLf
        Return strHeaderRow
    End Function

    Private Function endFooter() As String
        Dim strHeaderRow As String = ""
        strHeaderRow += "</tfoot>" & vbCrLf
        Return strHeaderRow
    End Function

    Private Function startRow(ByVal cls As String, Optional ByVal rowspan As String = "", Optional ByVal id As String = "") As String
        Dim strID As String = ""
        If (checkValue(id)) Then
            strID = "ID=""" & id & """"
        End If
        If (checkValue(rowspan)) Then
            Return "<tr " & strID & " rowspan=""" & rowspan & """ class=""" & cls & """>"
        Else
            Return "<tr " & strID & " class=""" & cls & """>"
        End If
    End Function

    Private Function endRow() As String
        Return "</tr>"
    End Function

    Private Function startCol(ByVal cls As String, Optional ByVal typ As String = "td", Optional ByVal colspan As String = "") As String
        If (checkValue(colspan)) Then
            Return "<" & typ & " colspan=""" & colspan & """ class=""" & cls & """>"
        Else
            Return "<" & typ & " class=""" & cls & """>"
        End If
    End Function

    Private Function endCol(Optional ByVal typ As String = "td") As String
        Return "</" & typ & ">"
    End Function

    Public Function colDataStore(ByVal Row As DataRow, strColumnValue As String, strColumnFormat As String) As String
        Dim val As String = Row.Item(strColumnValue).ToString, cls As String = "smlc"
        'If (checkValue(Row.Item(strColumnValue).ToString)) Then
        Call setTotals(val)
        If (strColumnFormat = "formatPercent") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatPercent(val, 1)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatNumber") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = formatNumber(val, 0)
        ElseIf (strColumnFormat = "formatFloat") Then
            If Not checkValue(val) Then val = "0.00"
            If Not IsNumeric(val) Then val = "0.00"
            val = formatNumber(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatCurrency") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatCurrency(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatDate") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = FormatDateTime(val, DateFormat.ShortDate)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatDateTime") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = ddmmyyhhmmss2ddmmhhmm(val)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatShortenShowTitle10") Then
            val = shortenShowTitle(val, 10)
        ElseIf (strColumnFormat = "formatShortenShowTitle25") Then
            val = shortenShowTitle(val, 25)
        End If
        Return "<td class=""" & cls & """>" & val & "</td>"
    End Function

    Public Function colClickThroughReportBuilder(ByVal Row As DataRow, ByVal strColumnValue As String, ByVal querystring As String, ByVal strColumnFormat As String) As String
        Dim val As String = Row.Item(strColumnValue).ToString, cls As String = "smlc"
        Call setTotals(val)
        If (strColumnFormat = "formatPercent") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatPercent(val, 1)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatNumber") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = formatNumber(val, 0)
        ElseIf (strColumnFormat = "formatFloat") Then
            If Not checkValue(val) Then val = "0.00"
            If Not IsNumeric(val) Then val = "0.00"
            val = formatNumber(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatCurrency") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatCurrency(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatDate") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = FormatDateTime(val, DateFormat.ShortDate)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatDateTime") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = ddmmyyhhmmss2ddmmhhmm(val)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatShortenShowTitle10") Then
            val = shortenShowTitle(val, 10)
        ElseIf (strColumnFormat = "formatShortenShowTitle25") Then
            val = shortenShowTitle(val, 25)
        Else
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = formatNumber(val, 0)
        End If
        Return "<td class=""" & cls & """><a href=""#modal-iframe"" data-toggle=""modal"" onClick=""setPrompt('#modal-iframe','View Cases','prompts/reports.aspx?frmSearch=Y&frmHideFilters=Y" & querystring & "',1000,800)"" title=""View Cases"">" & val & "</a></td>" & vbCrLf
    End Function

    Public Function colClickThroughCallAttempts(ByVal Row As DataRow, ByVal strColumnValue As String, ByVal querystring As String, ByVal strColumnFormat As String) As String
        Dim val As String = Row.Item(strColumnValue).ToString, cls As String = "smlc"
        Call setTotals(val)
        If (strColumnFormat = "formatPercent") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatPercent(val, 1)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatNumber") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = formatNumber(val, 0)
        ElseIf (strColumnFormat = "formatFloat") Then
            If Not checkValue(val) Then val = "0.00"
            If Not IsNumeric(val) Then val = "0.00"
            val = formatNumber(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatCurrency") Then
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = FormatCurrency(val, 2)
            cls = "smlr"
        ElseIf (strColumnFormat = "formatDate") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = FormatDateTime(val, DateFormat.ShortDate)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatDateTime") Then
            val = checkDate(val)
            If checkValue(val) Then
                val = ddmmyyhhmmss2ddmmhhmm(val)
            Else
                val = "&nbsp;"
            End If
        ElseIf (strColumnFormat = "formatShortenShowTitle10") Then
            val = shortenShowTitle(val, 10)
        ElseIf (strColumnFormat = "formatShortenShowTitle25") Then
            val = shortenShowTitle(val, 25)
        Else
            If Not checkValue(val) Then val = "0"
            If Not IsNumeric(val) Then val = "0"
            val = formatNumber(val, 0)
        End If
        Return "<td><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Call History', '/prompts/callhistory.aspx?" & querystring & "','900', '600');"">" & val & "</a></td>" & vbCrLf
    End Function

    Private Function clickThroughReplace(url As String, Row As DataRow) As String
        If (InStr(url, "{MediaCampaignID}")) Then
            url = Replace(url, "{MediaCampaignID}", Row.Item("MediaCampaignIDInbound"))
        End If
        If (InStr(url, "{CallCentreUserID}")) Then
            url = Replace(url, "{CallCentreUserID}", Row.Item("CallCentreUserID"))
        End If		
        Return url
    End Function

    Public Function colStatusDate(ByVal Row As DataRow, strColumnValue As String) As String
        If (checkValue(Row.Item(strColumnValue).ToString)) Then
            Return "<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item(strColumnValue).ToString) & " <span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item(strColumnValue).ToString) & "</span></td>"
        Else
            Return "<td class=""smlc"">&nbsp;</td>"
        End If
    End Function

    Public Function colNBSP(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">&nbsp;</td>"
    End Function

    Public Function colDateDiff(ByVal Row As DataRow) As String
        Return "<td class=""smlc"" data-function=""datediff"">&nbsp;</td>"
    End Function

    Public Function colLock(ByVal Row As DataRow) As String
        If (checkValue(Row.Item("LockUserName").ToString)) Then
            Return "<td class=""smlc""><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Status', '/prompts/casestatus.aspx?frmAppID=" & Row.Item("AppID") & "',500,500)"" class=""report-icon ui-tooltip"" data-placement=""right"" title=""View Case Status""><i class=""icon-calendar""></i></a>&nbsp;<a href=""#"" class=""report-icon ui-tooltip"" data-placement=""right"" title=""Case locked by " & Row.Item("LockUserName").ToString & ".""><i class=""icon-lock""></i></a></td>"
        Else
            Return "<td class=""smlc""><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Status', '/prompts/casestatus.aspx?frmAppID=" & Row.Item("AppID") & "',500,500)"" class=""report-icon ui-tooltip"" data-placement=""right"" title=""View Case Status""><i class=""icon-calendar""></i></a></td>"
        End If
    End Function

    Public Function colSelect(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><input type=""checkbox"" data-appid=""" & Row.Item("AppID") & """></td>"
    End Function

    Public Function colAppID(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Application Summary', '/reports/case.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"">" & Row.Item("AppID") & "</a></td>"
          
    End Function

    Public Function colAmount(ByVal Row As DataRow) As String
        Call setTotals(Row.Item("Amount"))
        Dim strText As String = "<td class=""smlr"">" & FormatCurrency(Row.Item("Amount"))
        If (Row.Item("AmountOriginal") > 0) Then
            If (Row.Item("Amount") > Row.Item("AmountOriginal")) Then
                strText += " <a href=""#"" class=""report-icon ui-tooltip"" data-placement=""right"" title=""Original amount: " & displayCurrency(Row.Item("AmountOriginal")) & """><i class=""icon-arrow-up""></i></a>"
            ElseIf (Row.Item("Amount") < Row.Item("AmountOriginal")) Then
                strText += " <a href=""#"" class=""report-icon ui-tooltip"" data-placement=""right"" title=""Original amount: " & displayCurrency(Row.Item("AmountOriginal")) & """><i class=""icon-arrow-down""></i></a>"
            Else
                strText += "&nbsp;"
            End If
        Else
            strText += "&nbsp;"
        End If
        strText += "</td>"
        Return strText
    End Function

    Public Function colTotal(ByVal Row As DataRow) As String
        Return "<td class=""smlc strong"">" & CInt(arrTotals(intColumn).ToString) & "&nbsp;</td>"
    End Function

    Public Function colTotalFloat(ByVal Row As DataRow) As String
        Return "<td class=""smlr strong"">" & formatNumber(arrTotals(intColumn).ToString, 2) & "&nbsp;</td>"
    End Function

    Public Function colTotalCurrency(ByVal Row As DataRow) As String
        Return "<td class=""smlr strong"">" & FormatCurrency(arrTotals(intColumn).ToString) & "&nbsp;</td>"
    End Function

    Public Function colTotalAverage(ByVal Row As DataRow) As String
        Return "<td class=""smlc strong"">" & divideRound(arrTotals(intColumn).ToString, intRowsCount) & "</td>"
    End Function

    Public Function colTotalAverageCurrency(ByVal Row As DataRow) As String
        Return "<td class=""smlr strong"">" & FormatCurrency(divideRound(arrTotals(intColumn).ToString, intRowsCount)) & "&nbsp;</td>"
    End Function

    Public Function colTotalAveragePercent(ByVal Row As DataRow) As String
		Return "<td class=""smlr strong"">" & FormatPercent(divideRound(arrTotals(intColumn).ToString, (intRowsCount + 1), 2)) & "</td>"
    End Function

    Public Sub setTotals(ByVal val As String)
        If (intColumn > UBound(arrTotals)) Then ReDim Preserve arrTotals(intColumn)
        If checkValue(val) Then
            If IsNumeric(val) Then
                arrTotals(intColumn) += CDec(val)
            End If
        End If
    End Sub

    Public Function colMediaCampaignInbound(ByVal Row As DataRow) As String
        Return "<td class=""smlc"" title=""" & Row.Item("MediaInboundName") & """>" & shortenShowTitle(Row.Item("MediaCampaignInboundName"), 25) & "</td>"
    End Function

    Public Function colCreatedDate(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate").ToString) & " <span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("CreatedDate").ToString) & "</span></td>"
    End Function

    Public Function colNextCallDate(ByVal Row As DataRow) As String
        Return "<td class=""smlc" & calcNextCallColour(Row.Item("NextCallDate")) & """>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("NextCallDate").ToString) & " <span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("NextCallDate").ToString) & "</span></td>"
    End Function

    Public Function colDialAttempts(ByVal Row As DataRow) As String
        Call setTotals(Row.Item("DialAttempts"))
        Return "<td><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Call History', '/prompts/callhistory.aspx?frmAppID=" & Row.Item("AppID") & "&frmExport=Y ','1200', '600');"">" & Row.Item("DialAttempts") & "</a></td>"
    End Function

    Public Function colStatusCode(ByVal Row As DataRow) As String
        Return "<td><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Status History', '/prompts/statushistory.aspx?frmAppID=" & Row.Item("AppID") & "&frmExport=Y ','800', '500');"" class=""ui-tooltip"" data-placement=""left"" title=""" & Row.Item("StatusDescription") & " " & Row.Item("SubStatusDescription") & """>" & Row.Item("StatusCode") & "&nbsp;" & Row.Item("SubStatusCode") & "</a></td>"
	End Function

    Public Function colStatusDescription(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & Row.Item("StatusDescription") & "</td>"
    End Function

    Public Function colSubStatusDescription(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & Row.Item("SubStatusDescription") & "</td>"
    End Function

    Public Function colOutsideCriteriaStatus(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("OutsideCriteriaSubStatusDescription") & """>" & Row.Item("OutsideCriteriaSubStatusCode") & "</span></td>"
    End Function

    Public Function colOutsideCriteriaDate(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (checkValue(Row.Item("OutsideCriteriaAcceptedDate").ToString)) Then
            strText += ddmmyyhhmmss2ddmmhhmm(Row.Item("OutsideCriteriaAcceptedDate").ToString) & "<span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("OutsideCriteriaAcceptedDate").ToString) & "</span>"
        ElseIf (checkValue(Row.Item("OutsideCriteriaDeclinedDate").ToString)) Then
            strText += ddmmyyhhmmss2ddmmhhmm(Row.Item("OutsideCriteriaDeclinedDate").ToString) & "<span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("OutsideCriteriaDeclinedDate").ToString) & "</span>"
        Else
            strText += ""
        End If
        strText += "</td>"
        Return strText
    End Function

    Public Function colOutsideCriteriaReturnPreSaleDate(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("ReturnedPreSaleDate").ToString) & "<span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("ReturnedPreSaleDate").ToString) & "</span></td>"
    End Function

    Public Function colCallCentreUserReference(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("CallCentreUserName").ToString & """>" & Row.Item("CallCentreUserReference").ToString & "</span></td>"
    End Function

    Public Function colCallCentreUserName(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("CallCentreUserName").ToString, 25) & "</td>"
    End Function

    Public Function colSalesUserReference(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("SalesUserName").ToString & """>" & Row.Item("SalesUserReference").ToString & "</span></td>"
    End Function

    Public Function colSalesUserName(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("SalesUserName").ToString, 25) & "</td>"
    End Function

    Public Function colAdministratorUserReference(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("AdministratorUserName").ToString & """>" & Row.Item("AdministratorUserReference").ToString & "</span></td>"
    End Function

    Public Function colAdministratorUserName(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("AdministratorUserName").ToString, 25) & "</td>"
    End Function

    Public Function colRepUserReference(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("RepUserName").ToString & """>" & Row.Item("RepUserReference").ToString & "</span></td>"
    End Function

    Public Function colRepUserName(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("RepUserName").ToString, 25) & "</td>"
    End Function

    Public Function colDiaryUserReference(ByVal Row As DataRow) As String
        Return "<td class=""smlc""><span title=""" & Row.Item("DiaryUserName").ToString & """>" & Row.Item("DiaryUserReference").ToString & "</span></td>"
    End Function

    Public Function colDiaryUserName(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("DiaryUserName").ToString, 25) & "</td>"
    End Function

    Public Function colDiaryDueDate(ByVal Row As DataRow) As String
        Return "<td class=""smlc" & calcNextCallColour(Row.Item("DiaryDueDate")) & """>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("DiaryDueDate").ToString) & " <span class=""sort-key"" style=""display: none;"">" & ddmmyyhhmmss2number(Row.Item("DiaryDueDate").ToString) & "</span></td>"
    End Function

    Public Function colDiaryType(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & Row.Item("DiaryType") & "</td>"
    End Function

    Public Function colDiaryTypeDescription(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & shortenShowTitle(Row.Item("DiaryTypeDescription").ToString, 25) & "</a></td>"
    End Function

    Public Function colDiarySubStatusCode(ByVal Row As DataRow) As String
        Return "<td class=""smlc"">" & Row.Item("DiarySubStatusCode").ToString & "</a></td>"
    End Function

    Public Function colActions(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (Not checkValue(Row.Item("LockUserName").ToString)) Then
            strText += "<a href=""/workflow/nextworkflow.aspx?frmAction=2&frmWorkflowID=" & objLeadPlatform.Config.RemindersWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """ class=""report-icon"" target=""_top""><i class=""icon-pencil""></i></a>"
            strText += "&nbsp;<a href=""/workflow/nextworkflow.aspx?frmAction=3&frmWorkflowID=" & objLeadPlatform.Config.InboundWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """ class=""report-icon"" target=""_top""><i class=""icon-phone""></i></a>"
        Else
            strText += "&nbsp;"
        End If
        If ((strReportType = "OutsideCriteria" Or strReportType = "Returned") And (objLeadPlatform.Config.Reports Or objLeadPlatform.Config.SuperAdmin)) Then
            'strText += "&nbsp;<a href=""/prompts/rules.aspx?frmAppID=" & Row.Item("AppID") & "&frmView=OutsideCriteria&TB_iframe=true&width=500&height=340"" title=""Outside Criteria"" class=""thickbox""><img src=""/net/images/icons/money_delete.gif"" width=""16"" height=""16"" border=""0"" title=""Outside Criteria""></a>"
        End If
        'If (objLeadPlatform.Config.Reports Or objLeadPlatform.Config.SuperAdmin) Then
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></i></a>"
        'End If
        strText += "&nbsp; <a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Create Letter', '/prompts/letters.aspx?AppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-envelope-o""></i></a>"

        strText += "&nbsp; <a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Document History', '/prompts/letterhistory.aspx?AppID=" & Row.Item("AppID") & "','800', '400');"" class=""report-icon""><i class=""icon-folder-open-o""></i></a>"

        strText += "</td>"
        Return strText
    End Function

    Public Function colActionsDiary(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (Not checkValue(Row.Item("LockUserName").ToString)) Then
            strText += "<a href=""/processing.aspx?Action=2&DiaryID=" & Row.Item("DiaryID") & "&AppID=" & Row.Item("AppID") & """ class=""report-icon""><i class=""icon-pencil"" target=""_top""></i></a>"
            'strText += "<a href=""/net/workflow/nextworkflow.aspx?frmDiaryID=" & Row.Item("DiaryID") & "&frmWorkflowID=" & intWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """><img src=""/net/images/edit.gif"" width=""16"" height=""16"" border=""0"" title=""Edit Application""></a>"
        Else
            strText += "&nbsp;"
        End If
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></a>"
        strText += "</td>"
        Return strText
    End Function

    Public Function colActionsWorkflow(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (objLeadPlatform.Config.Workflows And Not checkValue(Row.Item("LockUserName").ToString)) Then
            'strText += "<a href=""/processing.aspx?Action=2&AppID=" & Row.Item("AppID") & """><img src=""/net/images/edit.gif"" width=""16"" height=""16"" border=""0"" title=""Edit Application""></a>"
            strText += "<a href=""/workflow/nextworkflow.aspx?frmWorkflowID=" & intWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """ target=""_top"" class=""report-icon""><i class=""icon-pencil"" target=""_top""></i></a>"
        Else
            strText += "&nbsp;"
        End If
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></a>"
        'If (objLeadPlatform.Config.Manager Or objLeadPlatform.Config.SuperAdmin) Then
        '    strText += "&nbsp;<a href=""#"" class=""contextMenuButton"" data-appid=""" & Row.Item("AppID") & """><img src=""/net/images/icons/admin.png"" width=""16"" height=""16"" border=""0"" title=""Case Admin""></a>"
        'End If
        strText += "</td>"
        Return strText
    End Function

    Public Function colActionsWorkflowDiary(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (Not checkValue(Row.Item("LockUserName").ToString)) Then
            strText += "<a href=""/workflow/nextworkflow.aspx?frmDiaryID=" & Row.Item("DiaryID") & "&frmWorkflowID=" & intWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """ class=""report-icon"" target=""_top""><i class=""icon-pencil""></i></a>"

        Else
            strText += "&nbsp;"
        End If
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></a>"
        strText += "</td>"
        Return strText
    End Function

    Public Function colActionsSupplier(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></a>"
        strText += "</td>"
        Return strText
    End Function

    Public Function colActionsPartner(ByVal Row As DataRow) As String
        Dim strText As String = "<td class=""smlc"">"
        If (Not checkValue(Row.Item("LockUserName").ToString)) Then
            strText += "<a href=""/workflow/nextworkflow.aspx?frmAction=2&frmWorkflowID=" & objLeadPlatform.Config.RemindersWorkflowID & "&frmNextAppID=" & Row.Item("AppID") & """ class=""report-icon"" target=""_top""><i class=""icon-pencil""></i></a>"
        Else
            strText += "&nbsp;"
        End If
        strText += "&nbsp;<a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Case Notes', '/prompts/casenotes.aspx?frmAppID=" & Row.Item("AppID") & "','600', '600');"" class=""report-icon""><i class=""icon-edit""></a>"
        strText += "</td>"
        Return strText
    End Function

    Private Sub logReport(records As Integer)
        Dim strQry As String = "INSERT INTO tblreportlogs (CompanyID, ReportLogUserID, ReportLogNoRecords, ReportLogQueryString) " & _
                        "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                        formatField(Config.DefaultUserID, "N", 0) & ", " & _
                        formatField(records, "N", 0) & ", " & _
                        formatField(HttpContext.Current.Request.ServerVariables("QUERY_STRING"), "", "") & ") "
        executeNonQuery(strQry)
        cacheDependency(HttpContext.Current.Cache, "tblreportlogs")
    End Sub

End Class