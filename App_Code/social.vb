﻿Imports Microsoft.VisualBasic
Imports System.Net
Imports Common

Public Class Social

    Shared Function postToTwitter(ByVal post As String, ByVal apiKey As String) As HttpWebResponse
        Dim strGuid As String = System.Guid.NewGuid().ToString
        Try
            Dim objURI As New Uri("https://api.twitter.com/1/statuses/update.json")
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            Dim intTimeStamp As Integer = ddmmyyhhss2unix(Config.DefaultDateTime)
            Dim strParametersString As String = ""
            Dim arrResponse As Array = Split(post, "&")
            For x As Integer = 0 To UBound(arrResponse)
                Dim arrKeyValues As Array = Split(arrResponse(x), "=")
                If (x = 0) Then
                    strParametersString += encodeURL(arrKeyValues(0)) & "=" & encodeURL(arrKeyValues(1))
                Else
                    strParametersString += "&" & encodeURL(arrKeyValues(0)) & "=" & encodeURL(arrKeyValues(1))
                End If
            Next
            strParametersString += "&" & encodeURL("oauth_consumer_key") & "=" & encodeURL(apiKey)
            strParametersString += "&" & encodeURL("oauth_nonce") & "=" & encodeURL(strGuid)
            strParametersString += "&" & encodeURL("oauth_signature_method") & "=" & encodeURL("HMAC-SHA1")
            strParametersString += "&" & encodeURL("oauth_timestamp") & "=" & encodeURL(intTimeStamp)
            strParametersString += "&" & encodeURL("oauth_token") & "=" & encodeURL("")
            strParametersString += "&" & encodeURL("oauth_version") & "=" & encodeURL("1.0")
            responseWrite(strParametersString)
            responseEnd()
            Dim strSignString As String = ""
            Dim strSignature As String = hashString("sEgNwFQ377oT69uDIhYjELL9JaaN9cULKxQu6FCzqFTJGwfMxV", strSignString)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                .ContentType = "application/x-www-form-urlencoded"
                .Headers.Add("Authorization", "OAuth oauth_consumer_key = """ & apiKey & """," & _
                      "oauth_nonce = """ & strGuid & """," & _
                      "oauth_signature = """ & strSignature & """," & _
                      "oauth_signature_method = ""HMAC-SHA1""," & _
                      "oauth_timestamp = """ & intTimeStamp & """," & _
                      "oauth_token = ""370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb""," & _
                      "oauth_version = ""1.0""")
            End With
            responseWrite("post: " & post & "<br>")
            For Each item In objRequest.Headers
                responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            Next
            responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Shared Function hashString(ByVal key As String, ByVal text As String) As String
        Dim objEncoder As New System.Text.UTF8Encoding
        Dim bKey() As Byte = objEncoder.GetBytes(key)
        Dim bText() As Byte = objEncoder.GetBytes(text)
        Dim objHash As New System.Security.Cryptography.HMACSHA1(bKey, True)
        Dim bHash As Byte() = objHash.ComputeHash(bText)
        Dim hash As String = Replace(BitConverter.ToString(bHash), "-", "")
        Return hash.ToLower
    End Function

End Class
