﻿Imports Config, Common, CommonDropdowns, CallInterface
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Xml

' ** Revision history **
'
' ** End Revision History **

Public Class BusinessPanel

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Public BusinessObjectID As String = HttpContext.Current.Request("BusinessObjectID")
    Public MediaCampaignID As String = getAnyField("BusinessObjectMediaCampaignID", "tblbusinessobjects", "BusinessObjectID", BusinessObjectID)
    Public ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID"), ApplicationPageID As String = ""
    Public ApplicationPageOrder As String = HttpContext.Current.Request("Page"), intNextPageOrder As String = "", intPrevPageOrder As String = "", strPageName As String = "", strNextPageName As String = "", strPrevPageName As String = ""
    Public strOnSubmit As String = "", strOnLoadFunctions As String = "", strJavascriptFunctions As String = "", strFormAction As String = "", strDefaultReturnURL As String = "", strReturnURL As String = "", strTargetFrame As String = ""
    Public strUpdateStatusCode As String = "", strUpdateSubStatusCode As String = "", strUpdateDateType As String = "", strNote As String = "", boolSubmit As Boolean = False, boolMandatory As Boolean = False
    Public strLockUserName As String = "", intWorkflowActivityID As Integer = 0
    Public strFirstname As String = ""
    Public strSurname As String = "", strPostCode As String = "", strDOB As String = "", strAccountNumber As String = "", strSortCode As String = "", strCardNum As String = ""
    Public strHomeTel As String = "", strMobTel As String = "", strEmail As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property JavascriptFunctions() As String
        Get
            Return strJavascriptFunctions
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property OnLoadFunctions() As String
        Get
            Return strOnLoadFunctions
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property NumPages() As Integer
        Get
            Dim intNumPages As String = ""
            Dim strSQL As String = "SELECT MAX(ApplicationPageOrder) + 1 FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND CompanyID = '" & CompanyID & "'"
            intNumPages = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
            If (checkValue(intNumPages)) Then
                intNumPages = CInt(intNumPages)
            Else
                intNumPages = 0
            End If
            Return intNumPages
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Private Enum InputType
        Hidden = 0
        Text = 1
        SelectBox = 2
        Multi = 3
        TextArea = 4
        CheckBox = 5
        Radio = 6
        Quote = 7
    End Enum

    Public Sub checkConditions(ByVal frm As HtmlForm)

        intWorkflowActivityID = getAnyField("UserActiveWorkflowActivityID", "tblusers", "UserID", Config.DefaultUserID)
        If (Not checkValue(ApplicationPageOrder)) Then ApplicationPageOrder = 0
        strLockUserName = checkCaseLock(Config.DefaultUserID, BusinessObjectID)

        Dim strRedirect As Boolean = False
        Dim strSQL As String = "SELECT ApplicationPageID, ApplicationPageName, ApplicationPageSQLConditions, ApplicationPageFunctionOnSubmitEvent, ApplicationPageJavascriptFunctions, ApplicationPageFormAction, ApplicationPageReturnURL, ApplicationPageTargetFrame, ApplicationPageUpdateStatusCode, " & _
            "ApplicationPageUpdateSubStatusCode, ApplicationPageUpdateDateType, ApplicationPageNote, ApplicationPageSubmit, ApplicationPageMandatory " & _
            "FROM vwbusinesspanel " & _
            "WHERE BusinessObjectID = '" & BusinessObjectID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder = '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("ApplicationPageSQLConditions").ToString)) Then
                    strSQL = "SELECT BusinessObjectID FROM tblapplications WHERE BusinessObjectID = '" & BusinessObjectID & "' AND " & Row.Item("ApplicationPageSQLConditions").ToString & " AND tblapplications.CompanyID = '" & CompanyID & "'"
                    Dim dsCache2 As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                    If (dsCache2.Rows.Count = 0) Then
                        strRedirect = True
                    End If
                    dsCache2 = Nothing
                End If
                ApplicationPageID = Row.Item("ApplicationPageID")
                strPageName = Row.Item("ApplicationPageName")
                strOnSubmit = Row.Item("ApplicationPageFunctionOnSubmitEvent").ToString
                strOnSubmit = Replace(strOnSubmit, "{BusinessObjectID}", BusinessObjectID)
                strJavascriptFunctions = dateTags(Row.Item("ApplicationPageJavascriptFunctions").ToString)
                strFormAction = Row.Item("ApplicationPageFormAction").ToString
                strDefaultReturnURL = "/business/panel.aspx?BusinessObjectID=" & BusinessObjectID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&Page=" & ApplicationPageOrder
                strReturnURL = Row.Item("ApplicationPageReturnURL").ToString
                If (Not checkValue(strReturnURL)) Then
                    strReturnURL = strDefaultReturnURL
                Else
                    strReturnURL = Replace(strReturnURL, "{BusinessObjectID}", BusinessObjectID)
                    strReturnURL = Replace(strReturnURL, "{ApplicationTemplateID}", ApplicationTemplateID)
                    strReturnURL = Replace(strReturnURL, "{NextPage}", getNextPage())
                    strReturnURL = Replace(strReturnURL, "/net/business/processing.aspx", "/processing.aspx")
                    strReturnURL = Replace(strReturnURL, "/net/business/panel.aspx", "/business/panel.aspx")
                End If
                strTargetFrame = Row.Item("ApplicationPageTargetFrame").ToString
                If (strTargetFrame = "content") Then strTargetFrame = "_top"
                If (checkValue(strTargetFrame)) Then frm.Target = strTargetFrame
                strUpdateStatusCode = Row.Item("ApplicationPageUpdateStatusCode").ToString
                strUpdateSubStatusCode = Row.Item("ApplicationPageUpdateSubStatusCode").ToString
                strUpdateDateType = Row.Item("ApplicationPageUpdateDateType").ToString
                strNote = Row.Item("ApplicationPageNote").ToString
                boolSubmit = Row.Item("ApplicationPageSubmit")
                boolMandatory = Row.Item("ApplicationPageMandatory")
            Next
        End If
        dsCache = Nothing
        If (strRedirect) Then
            responseRedirect(getNextPageURL())
        End If
        If (Not checkValue(strReturnURL)) Then
            strReturnURL = HttpContext.Current.Request.ServerVariables("PATH_INFO")
            If (checkValue(HttpContext.Current.Request.ServerVariables("QUERY_STRING"))) Then
                strReturnURL += "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            End If
        End If
    End Sub

    Public Sub getApplicationFields()
        Dim strFields As String = "<div class=""btn-toolbar"">", strSelect As String = "", strData As String = ""
        Dim strSQL As String = "SELECT ValName, ValTable FROM tblapplicationpages PGS INNER JOIN tblapplicationpagemappings MAP ON MAP.ApplicationPageID = PGS.ApplicationPageID INNER JOIN tblimportvalidation IMP ON IMP.ValID = MAP.ApplicationFieldID " & _
            " WHERE PGS.ApplicationPageTemplateID = '" & ApplicationTemplateID & "' AND PGS.ApplicationPageOrder = '" & ApplicationPageOrder & "' AND PGS.CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("ValTable") = "tblbusinessdatastore") Then
                    strSelect += ", (SELECT TOP 1 StoredDataValue FROM dbo.tbldatastore WHERE (AppID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "') AND (StoredDataName = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                ElseIf (Row.Item("ValTable") = "tblapplicationstatusdates") Then
                    strSelect += ", (SELECT TOP 1 BusinessObjectStatusDate FROM dbo.tblbusinessobjectstatusdates WHERE (BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "') AND (ApplicationStatusDateName = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                ElseIf (Row.Item("ValTable") = "tbldiaries") Then
                    strSelect += ", (SELECT TOP 1 DiaryDueDate FROM dbo.tblbusinessdiaries WHERE (BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "' AND DiaryActive = 1 AND DiaryComplete = 0) AND (DiaryType = '" & Row.Item("ValName") & "')) AS " & Row.Item("ValName")
                Else
                    strSelect += ", " & Row.Item("ValName")
                End If
            Next
        End If
        dsCache = Nothing

        strSQL = "SELECT ValID, ValName, AdditionalFieldButtonFormActionParameters, ApplicationFriendlyName, ApplicationTooltip, ApplicationMandatory, ApplicationInputType, ApplicationDropdownFunction, ApplicationDropdownValues, ApplicationDataGridColumn, ApplicationDataGridTableClass, ApplicationDataGridColumnClass, ApplicationFunctionOnChangeEvent, ApplicationFunctionOnBlurEvent, " & _
                "ApplicationDateType, AdditionalFieldType, AdditionalFieldID, AdditionalFieldHTML, AdditionalFieldRoutine, AdditionalFieldRoutineParameters, AdditionalFieldButtonText, AdditionalFieldButtonStatusCode, AdditionalFieldButtonSubStatusCode, AdditionalFieldButtonDateType, AdditionalFieldButtonClearDateType, AdditionalFieldButtonPanelID, AdditionalFieldButtonPanelWidth, AdditionalFieldButtonPanelHeight, AdditionalFieldButtonShowNote, AdditionalFieldButtonNote, ScriptColour, ScriptText, AdditionalFieldName " & strSelect & " FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder = '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageMappingID"

        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim y As Integer = 0, intColSpan As Integer = 0, boolWriteLabel As Boolean = True, boolNewRow As Boolean = True
            'strFields += "<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""margin: 0px; padding: 0px"">" & vbCrLf
            For Each Row As DataRow In dsCache.Rows
                Dim strName As String = "", strTitle As String = "", strClass As String = "", strScriptClass As String = "", strChecked As String = "", strTip As String = ""
                If checkValue(Row.Item("ValID").ToString) Then
                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        If (Row.Item("ApplicationInputType") <> InputType.Hidden) Then
                            strFields += "<div id=""" & Row.Item("ValName") & "Container"" class=""control-group"">" & vbCrLf
                        Else
                            strFields += "<div id=""" & Row.Item("ValName") & "Container"" class=""control-group"">" & vbCrLf
                        End If
                    End If
                    If (checkValue(Row.Item("ApplicationTooltip"))) Then
                        strTitle = Row.Item("ApplicationTooltip")
                        strClass = " ui-tooltip"
                    ElseIf (checkValue(Row.Item("ApplicationFriendlyName"))) Then
                        strTitle = Row.Item("ApplicationFriendlyName")
                        strName = Row.Item("ValName")
                    End If
                    If (checkValue(Row.Item("ApplicationFriendlyName"))) Then
                        strName = Row.Item("ApplicationFriendlyName")
                    Else
                        strName = Row.Item("ValName")
                    End If
                    'If (checkValue(Row.Item("ApplicationTooltip"))) Then
                    'strTip = "<div class=""tooltip displayNone"">" & Row.Item("ApplicationTooltip") & "</div>"
                    'End If
                    'strFields += strTip
                    'intColSpan = 2
                    'boolNewRow = True
                    'strFields += "<tr>" & vbCrLf
                    'strFields += "<td class=""smlr"" style=""width: 40%"">"
                    'strFields += "<div style=""width: 40%;"" class=""label"">"
                    'strFields += "<span style=""position: absolute; left: 15%;margin-top: -8px;"" class=""label"">"
                    If (Row.Item("ApplicationInputType") <> InputType.Hidden And boolWriteLabel And Not (Row.Item("ApplicationDataGridColumn"))) Then
                        'strFields += "<span id=""" & Row.Item("ValName") & "Label"">" & strName & ":</span>"
                        strFields += "<label class=""control-label ui-tooltip"" for=""" & Row.Item("ValName") & """ data-placement=""right"" title=""" & strTitle & """>" & strName & "</label>"
                    Else
                        strFields += "&nbsp;"
                    End If
                    'strFields += "</span>"
                    'strFields += "</div>" & vbCrLf
                    'strFields += "</td>" & vbCrLf
                    'strFields += "<td class=""sml"" style=""width: 60%"">"
                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        strFields += "<div class=""controls"">"
                    End If
                    If (Row.Item("ApplicationDataGridColumn")) Then
                        If (Row.Item("ApplicationMandatory")) Then
                            strClass += " mandatory"
                        End If
                        If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                            strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" class=""" & Row.Item("ApplicationDataGridTableClass") & strClass & """ value=""" & Row.Item(Row.Item("ValName")) & """ title=""" & Row.Item("ApplicationFriendlyName") & """ data-friendly-name=""" & Row.Item("ApplicationFriendlyName") & """ data-input-type=""" & Row.Item("ApplicationInputType") & """ data-dropdown-values=""" & Row.Item("ApplicationDropdownValues").ToString & """ data-column-class=""" & Row.Item("ApplicationDataGridColumnClass").ToString & """ />" & vbCrLf
                        Else
                            strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" class=""" & Row.Item("ApplicationDataGridTableClass") & strClass & """ value=""" & Row.Item(Row.Item("ValName")) & """ title=""" & Row.Item("ApplicationFriendlyName") & """ data-friendly-name=""" & Row.Item("ApplicationFriendlyName") & """ data-input-type=""" & Row.Item("ApplicationInputType") & """ data-dropdown-values=""" & Row.Item("ApplicationDropdownValues").ToString & """ data-column-class=""" & Row.Item("ApplicationDataGridColumnClass").ToString & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf
                        End If
                    Else
                        Select Case Row.Item("ApplicationInputType")
                            Case InputType.Hidden
                                strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                            Case InputType.Text
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & Row.Item(Row.Item("ValName")) & """ class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & BusinessObjectID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ data-placement=""right"" />" & vbCrLf  ' hideTooltip();
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & BusinessObjectID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" value=""" & Row.Item(Row.Item("ValName")) & """ class=""text input-xlarge" & strClass & """ title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" />" & vbCrLf  ' hideTooltip();
                                End If
                            Case InputType.TextArea
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & """ />" & vbCrLf
                                strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & """ />" & vbCrLf
                                If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall  Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strFields += "<textarea id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" class=""text input-xlarge" & strClass & """ onblur=""" & Row.Item("ApplicationFunctionOnBlurEvent") & "saveField('" & BusinessObjectID & "','" & MediaCampaignID & "',this);"" onmouseout="""" title=""" & strTitle & """ rows=""3"" data-placement=""right"" />" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & "</textarea>" & vbCrLf  ' hideTooltip();
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & BusinessObjectID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    strFields += "<textarea id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""text"" class=""text input-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"" rows=""3"" />" & Replace(Row.Item(Row.Item("ValName")).ToString, "<br />", vbCrLf) & "</textarea>" & vbCrLf  ' hideTooltip();
                                End If
                            Case InputType.SelectBox
                                Dim strOptions As String = ""
                                If (checkValue(Row.Item("ApplicationDropdownFunction").ToString)) Then
                                    Dim arrParams As Object() = {CacheObject(), "--" & strName & "--", Row.Item(Row.Item("ValName")).ToString}
                                    strOptions = executeDropdownByName(Me, Row.Item("ApplicationDropdownFunction"), arrParams)
                                ElseIf (checkValue(Row.Item("ApplicationDropdownValues").ToString)) Then
                                    Dim strValue As String = Row.Item(Row.Item("ValName")).ToString
                                    Dim arrOptions As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                                    strOptions += "<option value="""">--" & strName & "--</option>"
                                    For x As Integer = 0 To UBound(arrOptions)
                                        strOptions += "<option value=""" & arrOptions(x) & """"
                                        If (checkValue(strValue)) Then
                                            If (strValue = arrOptions(x)) Then
                                                strOptions += " selected=""selected"""
                                            End If
                                        End If
                                        strOptions += ">" & arrOptions(x) & "</option>" & vbCrLf
                                    Next
                                End If
                                If (Row.Item("ApplicationMandatory")) Then
                                    strClass += " mandatory"
                                End If
                                strFields += "<input id=""Current" & Row.Item("ValName") & """ name=""Current" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                strFields += "<input id=""Undo" & Row.Item("ValName") & """ name=""Undo" & Row.Item("ValName") & """ type=""hidden"" value=""" & Row.Item(Row.Item("ValName")) & """ />" & vbCrLf
                                If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall  Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strClass = Replace(strClass, "ui-tooltip", "")
                                    strFields += "<select id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ onchange=""" & Row.Item("ApplicationFunctionOnChangeEvent") & """ onblur=""saveField('" & BusinessObjectID & "','" & MediaCampaignID & "',this);"" class=""text select-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ data-placement=""right"">" & vbCrLf
                                    strFields += strOptions & vbCrLf
                                    strFields += "</select>" ' hideTooltip();
                                    strFields += "<span id=""" & Row.Item("ValName") & "UndoButton"" style=""cursor: pointer"" class=""displayNone""><i style=""position: absolute; margin: 12px 0px 0px 15px;"" onclick=""undoSaveField('" & BusinessObjectID & "','" & MediaCampaignID & "','" & Row.Item("ValName") & "')"" class=""icon-refresh"" style=""display: none"" title=""Undo"" /></i></span>"
                                Else
                                    strFields += "<select id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ class=""text select-xlarge" & strClass & """ onmouseout="""" title=""" & strTitle & """ readonly=""readonly"" disabled=""disabled"">" & vbCrLf
                                    strFields += strOptions & vbCrLf
                                    strFields += "</select>" ' hideTooltip();
                                End If
                            Case InputType.CheckBox
                                If (Row.Item(Row.Item("ValName")).ToString = "1") Then
                                    strChecked = " checked=""checked"""
                                End If
                                If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""checkbox"" value=""1"" onclick=""saveField('" & BusinessObjectID & "','" & MediaCampaignID & "',this);""" & strChecked & " />" & vbCrLf
                                Else
                                    strFields += "<input id=""" & Row.Item("ValName") & """ name=""" & Row.Item("ValName") & """ type=""checkbox"" value=""1"" readonly=""readonly""" & strChecked & " />" & vbCrLf
                                End If
                        End Select
                    End If

                    If (Not Row.Item("ApplicationDataGridColumn")) Then
                        strFields += _
                        "<span class=""displayNone""><i id=""" & Row.Item("ValName") & "Error"" class=""icon-ok"" style=""position: absolute; margin: 12px 0px 0px 0px;"" class=""displayNone"" /></i></span><br />"
                        strFields += "</div></div>" & vbCrLf
                    End If
                    'strFields += "</td>" & vbCrLf
                    'strFields += "</tr>" & vbCrLf
                    strOnLoadFunctions += Row.Item("ApplicationFunctionOnChangeEvent")
                Else
                    Select Case Row.Item("AdditionalFieldType").ToString
                        Case 1 ' HTML
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td>&nbsp;</td>" & vbCrLf
                            '    strFields += "<td class=""sml"">"
                            'End If
                            strFields += Row.Item("AdditionalFieldHTML")
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 2 ' Script
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            'If (y = 0) Then strFields += "<br />"
                            If (Row.Item("ScriptColour") = "Green" Or Row.Item("ScriptColour") = "Red") Then
                                strFields += "<blockquote class=""no-triangle-right displayBlock"" id=""" & Replace(Row.Item("AdditionalFieldName").ToString, " ", "") & """><span id=""script" & Row.Item("ScriptColour") & "Icon"" /></span>" & replaceTags(Row.Item("ScriptText"), BusinessObjectID) & "</blockquote>"
                            Else
                                strFields += "<blockquote class=""triangle-right displayBlock"" id=""" & Replace(Row.Item("AdditionalFieldName").ToString, " ", "") & """><span id=""quoteOpen""></span><span id=""script" & Row.Item("ScriptColour") & "Icon"" /></span>" & replaceTags(Row.Item("ScriptText"), BusinessObjectID) & "<span id=""quoteClose""></span></blockquote>"
                            End If
                        Case 3 ' Subroutine
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (checkValue(Row.Item("AdditionalFieldRoutine").ToString)) Then
                                Dim arrParams As Object() = {CacheObject(), BusinessObjectID}
                                If (checkValue(Row.Item("AdditionalFieldRoutineParameters").ToString)) Then
                                    Dim arrFrmActionParameters As String() = Nothing
                                    arrFrmActionParameters = Split(Row.Item("AdditionalFieldRoutineParameters").ToString, ",")
                                    For y = 0 To UBound(arrFrmActionParameters)
                                        ReDim Preserve arrParams(2 + y)
                                        arrParams(2 + y) = arrFrmActionParameters(y)
                                    Next
                                End If
                                strFields += executeSub(Me, Row.Item("AdditionalFieldRoutine").ToString, arrParams)
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 4 ' Progress Case Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(BusinessObjectID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-success btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                        Else
                                            If (Row.Item("AdditionalFieldButtonShowNote")) Then
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', true)"" class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                                '"<a id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.jPrompt('This action will progress the case. Please enter a note and confirm.', '', '" & Row.Item("AdditionalFieldButtonText") & "', '" & Row.Item("AdditionalFieldButtonText") & "', 'Cancel', function(r) { if (r) { setStatusDate('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "');$('#frmHiddenNote').val($('#frmHiddenNote').val() + ': ' + r);setFormAction('" & Row.Item("AdditionalFieldID") & "');submitform(document.form1) } } );"" class=""salesButtonGreen"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                            Else
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 5 ' Regress Case Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(BusinessObjectID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-danger btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button>"
                                        Else
                                            If (Row.Item("AdditionalFieldButtonShowNote")) Then
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', true)"" class=""btn btn-danger btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            Else
                                                strFields += _
                                                    "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-danger btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If
                        Case 6 ' Utility Button
                            'If (boolNewRow) Then
                            '    strFields += "<tr>" & vbCrLf
                            '    strFields += "<td colspan=""" & intColSpan & """>"
                            'End If
                            If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(BusinessObjectID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then
                                        If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                            strFields += _
                                                "<div class=""btn-group""><button class=""btn btn-tertiary btn-medium ui-tooltip"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                            '"<a id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onclick=""parent.parent.tb_show('" & Row.Item("AdditionalFieldButtonText") & "', '/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "&TB_iframe=true&width=" & Row.Item("AdditionalFieldButtonPanelWidth") & "&height=" & Row.Item("AdditionalFieldButtonPanelHeight") & "', '')"" class=""salesButton"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                        Else
                                            strFields += _
                                                "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "');parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)"" class=""btn btn-tertiary btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button></div>"
                                        End If
                                    End If
                                End If
                            End If
                            'If (boolNewRow) Then
                            '    strFields += "</td>" & vbCrLf
                            '    strFields += "</tr> " & vbCrLf
                            '    boolNewRow = False
                            'End If

                        Case 7 ' multi Select Progress Button 
                            Dim strParams As String() = Split(Row.Item("AdditionalFieldButtonFormActionParameters"), "|")
                            Dim strStatuses As String() = Split(strParams(0), ",")
                            Dim strSubStatuses As String() = Split(strParams(1), ",")
                            Dim strDateTypes As String() = Split(strParams(2), ",")
                            Dim strRemoveDateTypes As String() = Split(strParams(3), ",")
                            Dim strFriendlyNames As String() = Split(strParams(4), ",")
                            Dim strcount As Integer = strStatuses.Count - 1

                            If (boolNewRow) Then
                                strFields += "<tr>" & vbCrLf
                                strFields += "<td colspan=""" & intColSpan & """>"

                            End If
                            If (intWorkflowActivityID = ActivityType.StartCall Or ActivityType.StartCase Or ActivityType.EndCall Or objLeadPlatform.Config.CallInterface = "N") Then
                                If (checkDateField(BusinessObjectID, Row.Item("ApplicationDateType"))) Then
                                    If (checkValue(Row.Item("AdditionalFieldButtonText").ToString)) Then

                                        If (checkValue(Row.Item("AdditionalFieldButtonFormActionParameters"))) Then

                                            Dim x As Integer = 0
                                            If (checkValue(Row.Item("AdditionalFieldButtonPanelID").ToString)) Then
                                                strFields += _
                                               "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ class=""btn btn-primary btn-small dropdown-toggle"" data-toggle=""dropdown"">" & Row.Item("AdditionalFieldButtonText") & "<span class=""caret""></span></button>" & vbCrLf & _
                                               "<ul class=""dropdown-menu"">"
                                                While x <= strcount
                                                    strFields += "<li><a href=""onClick=""onClick=""$('#frmHiddenNote').val('" & strNote(x) & "'); parent.progressCase('" & strStatuses(x).ToString & "','" & strSubStatuses(x).ToString & "','" & strDateTypes(x).ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)""" & strFriendlyNames(x) & """></a></li>" & vbCrLf & _
                                                    x = x + 1
                                                End While
                                                strFields += "</ul></div>"
                                                '"<a id= & Row.Item("AdditionalFieldName") & """ href=""#"" onclick=""parent.parent.tb_show('" & Row.Item("AdditionalFieldButtonText") & "', '/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "&TB_iframe=true&width=" & Row.Item("AdditionalFieldButtonPanelWidth") & "&height=" & Row.Item("AdditionalFieldButtonPanelHeight") & "', '')"" class=""salesButton"">" & Row.Item("AdditionalFieldButtonText") & "</a>"
                                            Else
                                                strFields += _
                                                "<div class=""btn-group""><button id=""" & Row.Item("AdditionalFieldName") & """ class=""btn btn-primary btn-small dropdown-toggle"" data-toggle=""dropdown"">" & Row.Item("AdditionalFieldButtonText") & "<span class=""caret""></span></button>" & vbCrLf & _
                                                "<ul class=""dropdown-menu"">"
                                                While x <= strcount
                                                    strFields += "<li><a href=""onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "'); parent.progressCase('" & strStatuses(x) & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)""" & strFriendlyNames(x) & """></a></li>" & vbCrLf & _
                                                    x = x + 1
                                                End While
                                                strFields += "</ul></div>"
                                            End If
                                            ' "<button id=""" & Row.Item("AdditionalFieldName") & """ href=""#"" onClick=""$('#frmHiddenNote').val('" & Row.Item("AdditionalFieldButtonNote") & "'); 
                                            '										 parent.progressCase('" & Row.Item("AdditionalFieldButtonStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonSubStatusCode").ToString & "','" & Row.Item("AdditionalFieldButtonDateType").ToString & "','" & Row.Item("AdditionalFieldButtonClearDateType").ToString & "','" & Row.Item("AdditionalFieldID") & "','" & ApplicationTemplateID & "', false)""
                                            '										 class=""btn btn-success btn-medium ui-tooltip"" onclick="""" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button>"
                                            '                                          
                                        End If

                                        strFields += _
                                         "<div class=""btn-group""><button class=""btn btn-primary btn-small dropdown-toggle"" onclick=""parent.setPrompt('#modal-iframe','" & Row.Item("AdditionalFieldButtonText") & "','/business/panel.aspx?ApplicationTemplateID=" & Row.Item("AdditionalFieldButtonPanelID").ToString & "&BusinessObjectID=" & BusinessObjectID & "', " & Row.Item("AdditionalFieldButtonPanelWidth") & ", " & Row.Item("AdditionalFieldButtonPanelHeight") & ");parent.$('#modal-iframe').modal('show');return false;"" style=""margin-right: 10px; margin-top: 10px;"">" & Row.Item("AdditionalFieldButtonText") & "</button>" & vbCrLf & _
                                                "<ul class=""dropdown-menu"">" & vbCrLf & _
                                                   "<li><a href=""#"">" & strStatuses(1) & "</a></li>" & vbCrLf & _
                                                   " <li><a href=""#""> " & strFriendlyNames(1) & "</a></li>" & vbCrLf & _
                                                   "<li><a href=""#""> " & strSubStatuses(1) & "</a></li>" & vbCrLf & _
                                                   "<li><a href=""#""> " & strDateTypes(1) & "</a></li>" & vbCrLf & _
                                        "</ul></div>"
                                    End If
                                End If
                            End If
                            If (boolNewRow) Then
                                strFields += "</td>" & vbCrLf
                                strFields += "</tr> " & vbCrLf
                                boolNewRow = False
                            End If
                    End Select
                End If
                y += 1
            Next
            'strFields += "</table>"
            strFields += "</div>"
        End If
        dsCache = Nothing
        HttpContext.Current.Response.Write(strFields)
    End Sub

    Public Function getNextPage() As String
        Dim strPageOrder As String = ""
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder > '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder"
        strPageOrder = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        Return strPageOrder
    End Function

    Public Function getPreviousPage() As String
        Dim strPageOrder As String = ""
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "'AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder < '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder DESC"
        strPageOrder = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        Return strPageOrder
    End Function

    Public Function getNextPageURL() As String
        Dim strLink As String = "", intNextPage As String = getNextPage()
        If (checkValue(intNextPage)) Then
            strLink += "panel.aspx?BusinessObjectID=" & BusinessObjectID & "&ApplicationTemplateID=" & ApplicationTemplateID & "&Page=" & intNextPage
        End If
        Return strLink
    End Function

    Public Sub setNavigation()
        Dim strSQL As String = "SELECT TOP 1 ApplicationPageOrder, ApplicationPageName FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageOrder > '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intNextPageOrder = Row.Item("ApplicationPageOrder")
                strNextPageName = Row.Item("ApplicationPageName").ToString
            Next
        End If
        dsCache = Nothing
        strSQL = "SELECT TOP 1 ApplicationPageOrder, ApplicationPageName FROM vwbusinesspanel WHERE BusinessObjectID = '" & BusinessObjectID & "' AND ApplicationTemplateID = '" & ApplicationTemplateID & "'AND ApplicationPageOrder < '" & ApplicationPageOrder & "' AND CompanyID = '" & CompanyID & "' ORDER BY ApplicationPageOrder DESC"
        dsCache = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intPrevPageOrder = Row.Item("ApplicationPageOrder")
                strPrevPageName = Row.Item("ApplicationPageName").ToString
            Next
        End If
        dsCache = Nothing
    End Sub

    'Private Function replaceParameters(ByVal params As String) As Object()
    '    Dim arrObjects As Object() = Nothing
    '    Dim arrParams As Array = Split(params)
    '    For Each param As Object In arrParams
    '        Dim x As Integer = 0
    '        If (param = "{BusinessObjectID}") Then
    '            param = BusinessObjectID
    '        End If
    '        If (param = "{Cache}") Then
    '            param = CacheObject()
    '        End If
    '        arrObjects(x) = Nothing
    '    Next
    '    Return arrObjects
    'End Function
    '*******************************************************
    ' Execute any method from this class
    '*******************************************************
    Shared Function executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(BusinessPanel)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        Return objMethodInfo.Invoke(inst, params)
    End Function

    Public Function getNotes(ByVal cache As Web.Caching.Cache, ByVal BusinessObjectID As String) As String
        Dim objStringWriter As New StringWriter
        Dim strReturnURL As String = "/business/panel.aspx?BusinessObjectID=" & BusinessObjectID & "&ApplicationTemplateID=" & HttpContext.Current.Request("ApplicationTemplateID") & "&Page=" & HttpContext.Current.Request("Page")
        With objStringWriter
            .WriteLine("<div id=""notes-container"" style=""overflow: auto;"">")
            .WriteLine("<table id=""notes-table"" class=""table sortable"">")
            Dim strSQL As String = "SELECT NTS.NoteID, NTS.Note, NTS.NoteType, NTS.CreatedDate, USR.UserFullName, USR.UserID FROM tblbusinessobjectnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                "WHERE BusinessObjectID = '" & BusinessObjectID & "' AND NTS.CompanyID = '" & CompanyID & "' AND NoteActive = 1 ORDER BY NTS.NoteType, NTS.CreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            Dim x As Integer = 0
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr class=""tablehead"">")
                    '.WriteLine("<td class=""smlc"" style=""padding: 5px 5px 5px 5px;""><img src=""/images/icons/note.gif"" width=""16"" height=""16"" border=""0"" /></td>")
                    If (Row.Item("NoteType") = 1) Then ' Sticky note
                        .WriteLine("<td class=""sml sticky"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        .WriteLine("<i class=""icon-exclamation-sign""></i>&nbsp;<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;<span class=""stickyNote"">" & Row.Item("Note") & "</span><br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                        .WriteLine("<span style=""float: right; margin: -2px 4px 0px 0px;""><a href=""#"" onclick=""editStickyNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-pencil"" title=""Edit Note""></i></a></span>")
                    Else
                        .WriteLine("<td class=""sml"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;""><div>")
                        If (objLeadPlatform.Config.Partner Or objLeadPlatform.Config.Supplier) Then
                            .WriteLine("<span class=""title"">User " & Row.Item("UserID") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        Else
                            .WriteLine("<span class=""title"">" & Row.Item("UserFullName") & "</span>&nbsp;" & Row.Item("Note") & "<br /><span class=""date"">" & Row.Item("CreatedDate") & "</span>")
                        End If
                        If (getUserAccessLevel(Config.DefaultUserID, "Delete Notes")) Then
                            .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeNote(this," & Row.Item("NoteID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                        End If
                    End If
                    .WriteLine("</div></td>")
                    .WriteLine("</tr>")
                    x += 1
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No notes found.</td>")
                .WriteLine("</tr>")
            End If

            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            If (Not objLeadPlatform.Config.Supplier) Then
                .WriteLine("<div id=""input-container"" class=""input-append"">")
                .WriteLine("<br /><textarea name=""frmNote"" id=""frmNote"" class=""text mandatory note"" title=""Note""></textarea><button class=""btn btn-primary btn-small"" type=""button"" onclick=""if (checkPageMandatory()) { dynamicNote(" & BusinessObjectID & "," & Config.DefaultUserID & "); }"">Go</button><input id=""frmSticky"" name=""frmSticky"" type=""hidden"" value=""0""><button id=""sticky"" name=""sticky"" class=""btn btn-primary btn-small"" data-toggle=""button"" onclick=""toggleStickyNote()""><i class=""icon-exclamation-sign""></i></button>")
                .WriteLine("</div>")
            End If
            If (objLeadPlatform.Config.SuperAdmin Or objLeadPlatform.Config.Manager) Then
                .WriteLine("<script type=""text/javascript"">")
                .WriteLine("$(document).ready(function () {")
                .WriteLine("    $('textarea.note').autoResize({ extraSpace: 0, callBack: 'resizeNotesPrompt()' }); resizeNotesPrompt();")
                .WriteLine("    $('.rollover').rollover( { fade:false } );")
                .WriteLine("    $('#notes-table tr td div').mouseover(function() {")
                .WriteLine("        $(this).children('span').children('.delete').removeClass('displayNone').addClass('displayBlock');")
                .WriteLine("        $(this).parent().addClass('rowHighlight');")
                .WriteLine("    });")
                .WriteLine("    $('#notes-table tr td div').mouseout(function() {")
                .WriteLine("        $(this).children('span').children('.delete').removeClass('displayBlock').addClass('displayNone');")
                .WriteLine("        $(this).parent().removeClass('rowHighlight');")
                .WriteLine("    });")
                .WriteLine("});")
                .WriteLine("</script>")
            End If
        End With
        Return objStringWriter.ToString
    End Function

    Public Function getReport(ByVal cache As Web.Caching.Cache, ByVal AppID As String, ByVal strURL As String) As String
        If InStr(strURL, "{AppID}") Then strURL = Replace(strURL, "{AppID}", AppID)
        If InStr(strURL, "{MediaCampaignIDInbound}") Then strURL = Replace(strURL, "{MediaCampaignIDInbound}", MediaCampaignID)
        If InStr(strURL, "{MediaIDInbound}") Then strURL = Replace(strURL, "{MediaIDInbound}", getAnyField("MediaID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID))
        If InStr(strURL, "{MediaCampaignDialerGradeID}") Then strURL = Replace(strURL, "{MediaCampaignDialerGradeID}", getAnyField("MediaCampaignDialerGradeID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID))
        strURL = Replace(strURL, "|", ",")

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<iframe id=""reportiframe"" name=""reportiframe"" frameborder=""0"" width=""100%"" height=""100px"" src=""" & strURL & "&frmProcessingReport=Y""></iframe>")
        End With
        Return objStringWriter.ToString
    End Function   
	
	 Public Sub sendBrokerLogin()

        Dim strSQL As String = "SELECT dbo.tblusers.*FROM dbo.tblbusinessobjects INNER JOIN dbo.tblbusinessobjects AS tblbusinessobjects_1 ON dbo.tblbusinessobjects.BusinessObjectParentClientID = tblbusinessobjects_1.BusinessObjectID INNER JOIN dbo.tblusers ON dbo.tblbusinessobjects.BusinessObjectID = dbo.tblusers.UserClientID WHERE (tblbusinessobjects_1.BusinessObjectID =" & BusinessObjectID & ") AND (dbo.tblbusinessobjects.BusinessObjectTypeID = 11)"

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("UserEmailAddress").ToString)) Then
                    Dim strNewPassword As String = generatePassword(8)
                    Dim strUsername As String = Row.Item("Username")
                    Dim strQry2 As String = "UPDATE tblusers SET " & _
                                        "UserPassword			= '" & hashString256(LCase(strUsername), strNewPassword) & "', " & _
                                        "UserPasswordChangeDate	= " & formatField(Config.DefaultDateTime.AddMinutes(-5), "DTTM", DefaultDateTime) & " " & _
                                        "WHERE UserID 		    = " & Row.Item("userID") & " " & _
                                        "AND CompanyID          = " & CompanyID & ""
                    executeNonQuery(strQry2)
					
					dim strEmailHTML as string = "<html><body leftmargin=""0"" marginwidth=""0"" topmargin=""0"" marginheight=""0"" offset=""0"" style=""background-color: #E1E1E1;"" ><table width=""100%"" cellpadding=""0"" cellspacing=""0"" ><tr><td valign=""top"" align=""left""><table width=""648"" border=""0"" cellspacing=""0"" cellpadding=""0""><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><img src=""https://positive.engagedcrm.co.uk/img/fp-positivelending.png"" width=""648"" height=""121"" /></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td valign=""top"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 10px 30px;""> <font face='Calibri, Verdana, Arial' size='2' color='#000000'><br /><h2>Welcome To 360 Loan Sourcing</h2><p>The team at Positive Lending have confirmed your registration details and have accepted your application to submit leads to Positive Lending. Please find your login details below.</p><p>Username: <strong>" & strUserName & "</strong></p><p>Password: <strong>" & strNewPassword & "</strong></p><br />Arena Business Centre<br />9 Nimrod Way,<br />Ferndown Industrial Estate,<br />Ferndown<br /><br />Dorset<br /><br />BH21 7UH<br /><img src=""https://positive.engagedcrm.co.uk/img/fp-positivelending.png"" width=""150"" height=""74"" /></font></div></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td bgcolor=""#F6F7F7""><hr style=""border: 1px solid #CCCCCC; width:100%; margin:5px""></td><td width=""26"">&nbsp;</td></tr><tr><td width=""27"">&nbsp;</td><td align=""center"" bgcolor=""#F6F7F7""><div style=""margin:10px 20px 0px 30px;""><font face=""Calibri, Verdana, Arial"" size=""2"" color=""#999999""><p>This private and confidential e-mail has been sent to you by Positive Lending (UK) Limited.  Registered office: 41 Freemantle Road, Southampton, Hampshire SO51 6AS .  Positive Lending (UK) Limited is a licensed credit broker under the Consumer Credit Act (license no 622042) and is a company registered in England reg no 6700848.  Positive Lending (UK) Limited trading as Positive Lending is authorised and regulated by the Financial Conduct Authority, number 607682</p><p>This e-mail is confidential and for use by the addressee only. If you are not the intended recipient of this e-mail and have received it in error, please return the message to the sender by replying to it and then delete it from your mailbox. Internet e-mails are not necessarily secure. Positive Lending (UK) Limited will not accept responsibility for changes made to this message after it was sent.</p><p>Whilst all reasonable care has been taken to avoid the transmission of viruses, it is the responsibility of the recipient to ensure that the onward transmission, opening or use of this message and any attachments will not adversely affect its systems or data. No responsibility is accepted by Positive Lending (UK) Limited in this regard and the recipient should carry out such virus and other checks as it considers appropriate.</p><p>This communication does not create or modify any contract.</p><p>If you would prefer not to receive information about other products and services from us, please send an email to ask@positivelending.co.uk with 'opt-out' in the subject line.</p></font><br /></div></td><td align=""center"">&nbsp;</td></tr><tr><td width=""648"" height=""32"" colspan=""3"" align=""center"">&nbsp;</td></tr></table></td></tr></table></body></html>"
					
					
                    postEmail("", Row.Item("UserEmailAddress").ToString, "360 Sourcing Logons",strEmailHTML, True, "", True)
                '	strReturnUrlVariables = "strMessageType=sucess&strMessageTitle=Registration+Complete&strMessage="& encodeURL("Email+sent+to+"&row.item("useremailaddress"))
				End If

            Next
		  'strReturnUrlVariables = "strMessageType=sucess&strMessageTitle=Registration+Failed&strMessage="& encodeURL("Email+unsuccesfully+sent+to+"&row.item("useremailaddress"))

        End If
    End Sub
	
	 Public Function AddPrimaryContact(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
		Dim ContactID as string = "", strMessage as string = "", strSQL2 as string = "", strSQL3 as string = "", strSQL4 as string = "", username as string = ""
            Dim strQry As String = "select businessobjectid from tblbusinessobjects where BusinessObjectparentclientID = '" & AppID & "' and businessobjecttypeid = '11'"
            Dim Cache2 As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache()
            If (Cache2.Rows.Count = 0) Then
				Dim strSQL As String = "select businessobjectcontactfirstname, BusinessObjectContactSurname, BusinessObjectContactEmailAddress, BusinessObjectContactMobileTelephone, BusinessObjectContactBusinessTelephone from tblbusinessobjects where BusinessObjectID = '" & AppID & "'"
				Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
				If (dsCache.Rows.Count > 0) Then
					For Each Row As DataRow In dsCache.Rows
					   strSQL2  = "INSERT INTO tblbusinessobjects (CompanyID, BusinessObjectMediaCampaignID, BusinessObjectTypeID,BusinessObjectParentClientID,BusinessObjectContactFirstName,BusinessObjectContactSurname,BusinessObjectContactEmailAddress,BusinessObjectContactMobileTelephone,BusinessObjectContactBusinessTelephone) VALUES ('1430', '11525', '11','" & AppID & "','" & Row.Item("businessobjectcontactfirstname") & "','" & Row.Item("BusinessObjectContactSurname") & "','" & Row.Item("BusinessObjectContactEmailAddress") & "','" & Row.Item("BusinessObjectContactMobileTelephone") & "','" & Row.Item("BusinessObjectContactBusinessTelephone") & "')"
					   ContactID = executeIdentityQuery(strSQL2)
					   Dim UsernameUsed as boolean = false
					   Dim PotentialUserName = Row.Item("businessobjectcontactfirstname") & "." & Row.Item("BusinessObjectContactSurname")
					   dim x as integer = 0
					   while (UsernameUsed = False)
						   strSQL3 = "select userid from tblusers where username = '" & PotentialUserName & "'" 
						   Dim dsCache2 As DataTable = New Caching(Nothing, strSQL3, "", "", "").returnCache
							   If (dsCache2.Rows.Count > 0) Then
							   x = x + 1
							   PotentialUserName = Row.Item("businessobjectcontactfirstname") & "." & Row.Item("BusinessObjectContactSurname") + "" & x & ""
							   Else
							   username = PotentialUserName
							   UsernameUsed = True
							   End If
					   End While
					  dim UserReference as string = Left(Row.Item("businessobjectcontactfirstname"),1) + Left(Row.Item("businessobjectcontactsurname"),1) + Right(Row.Item("businessobjectcontactsurname"),1)
					 
					 strSQL4 = "insert into tblusers (UserReference,UserName,UserPassword,UserFullName,CompanyID,UserEmailAddress,UserClientID) Values ('"&UCase(UserReference) &"','"& username &"',"& formatField(hashString256(LCase(UserName), "Pa55word"), "", "") &",'" & Row.Item("businessobjectcontactfirstname") &" "& Row.Item("businessobjectcontactsurname")&"','1430','"& Row.Item("BusinessObjectContactEmailAddress") & "','" & ContactID & "')"
					 executeNonQuery(strSQL4)
	
					Next
				strMessage = "User Successfully Added"	
				
				Else
				
				strMessage = "User Unsuccessfully Added"			
				   
				End If
			Else
			strMessage = "Primary contact already exists"	
			End If        	
        Return strMessage
    End Function
	

Public Function forgottenPassword(ByVal cache As Web.Caching.Cache, ByVal AppID As String) As String
Dim ContactID as string = "", strMessage as string = "", strSQL2 as string = "", strSQL3 as string = "", strSQL4 as string = "", username as string = ""
Dim strQry As String = "select businessobjectid from tblbusinessobjects where BusinessObjectID = '" & AppID & "'"
Dim Cache2 As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache()
If (Cache2.Rows.Count > 0) Then
	For Each Row As DataRow In Cache2.Rows
		Dim strSQL As String = "select UserID,username from tblusers where userclientID = '" & row.Item("businessobjectid") & "'"
		Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
		If (dsCache.Rows.Count > 0) Then
					strMessage = "<table class = table table-striped table-highlight><tr><th>username</th><th>Password</th></tr>"							
			For Each Rows As DataRow In dsCache.Rows							
				Dim strNewPassword As String = generatePassword(8)				
				 Dim strQry2 As String = "UPDATE tblusers SET " & _
                                        "UserPassword			= '" & hashString256(LCase(rows.Item("username")), strNewPassword) & "', " & _
                                        "UserPasswordChangeDate	= " & formatField(Config.DefaultDateTime.AddMinutes(-5), "DTTM", DefaultDateTime) & " " & _
                                        "WHERE UserID 		    = " & Rows.Item("userID") & " " & _
                                        "AND CompanyID          = " & CompanyID & ""
                    executeNonQuery(strQry2)				
				
				strMessage += "<tr><td>"& rows.item("username") & "</td><td>" & strNewPassword & "</td></tr>"
			Next
			strMessage += "</table>"
		End If		
	Next	
End If        	
Return strMessage
End Function

End Class