﻿Imports Config, Common, CallInterface
Imports Microsoft.VisualBasic

Public Class Processing

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Public AppID As String = HttpContext.Current.Request("AppID"), strApp1FullName As String = "", strApp2FullName As String = "", strFullAddressLine1 As String = "", strAddressLine2 As String = "", strAddressLine3 As String = "", strAddressPostCode As String = "", strApp1HomeTelephone As String = "", strApp1MobileTelephone As String = "", strApp1WorkTelephone As String = "", strStatusDescription As String = "", strSubStatusDescription As String = "", strStatusCode As String = "", strSubStatusCode As String = "", strDialableStatus As String = ""
    Public strCallTelephoneNumber As String = HttpContext.Current.Request("TelephoneNumber"), DiaryID As String = HttpContext.Current.Request("DiaryID")
    Private strAction As String = HttpContext.Current.Request("Action")
    Public intWorkflowID As String = "", intWorkflowTypeID As String = "", intWorkflowAccessLevel As String = "", intWorkflowActivityID As String = "", intCallTime As String = ""
    Public strLockUserName As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property CaseInformation() As String
        Get
            Dim strCaseInformation As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "CaseInfo")
            If (Not checkValue(strCaseInformation)) Then
                strCaseInformation = "<span class=""text-medium""><strong>{ProductType}</strong></span> - {App1FullName} (DOB: {App1DOB}), {FullAddressLine1}, {AddressPostCode}"
            End If
            Return replaceTags(strCaseInformation, AppID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property HomeTelephone() As String
        Get
            Return strApp1HomeTelephone
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property MobileTelephone() As String
        Get
            Return strApp1MobileTelephone
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkTelephone() As String
        Get
            Return strApp1WorkTelephone
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property Status() As String
        Get
            Return strStatusDescription
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SubStatus() As String
        Get
            Return strSubStatusDescription
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property DialableStatus() As String
        Get
            Return strDialableStatus
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowID() As String
        Get
            Return intWorkflowID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowTypeID() As String
        Get
            Return intWorkflowTypeID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowAccessLevel() As String
        Get
            Return intWorkflowAccessLevel
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowActivityID() As String
        Get
            Return intWorkflowActivityID
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialer() As Boolean
        Get
            Return getAnyFieldByCompanyID("WorkflowDialer", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property WorkflowDialerIdleTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerIdleTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialerWrapTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerWrapTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property WorkflowDialerDelayTime() As String
        Get
            Return getAnyFieldByCompanyID("WorkflowDialerDelayTime", "tblworkflows", "WorkflowID", intWorkflowID)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property CallTime() As String
        Get
            Return intCallTime
        End Get
        Set(ByVal value As String)

        End Set
    End Property
	
	Public Property StatusCode() As String
        Get
            Return strStatusCode
        End Get
        Set(ByVal value As String)

        End Set
    End Property
	
	Public Property SubStatusCode() As String
        Get
            Return strSubStatusCode
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Sub initialise()
        Dim dteCallStartTime As String = "", dtePreCallStartTime As String = ""
        If (checkKickOut(Config.DefaultUserID, True)) Then
            HttpContext.Current.Response.Redirect("/workflow/workflow.aspx?intWorkflowTypeID=" & intWorkflowTypeID)
        End If
        If (strAction = ActivityType.EndCall) Then
            'Call executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)
        End If
        callLogging(AppID, strAction, strCallTelephoneNumber, Config.DefaultUserID)

        Dim strSQL As String = "SELECT UserActiveWorkflowTelephoneNumber, UserActiveWorkflowTypeID, UserActiveWorkflowTypeUserLevel, UserActiveWorkflowID, UserActiveWorkflowActivityID, UserActiveWorkflowStartTime, UserPreCallStartTime FROM tblusers WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim ds As DataTable = objDataSet.Tables("tblusers")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                intWorkflowTypeID = Row.Item("UserActiveWorkflowTypeID").ToString
                intWorkflowAccessLevel = Row.Item("UserActiveWorkflowTypeUserLevel").ToString
                intWorkflowID = Row.Item("UserActiveWorkflowID").ToString
                intWorkflowActivityID = Row.Item("UserActiveWorkflowActivityID")
                dteCallStartTime = Row.Item("UserActiveWorkflowStartTime").ToString
                dtePreCallStartTime = Row.Item("UserPreCallStartTime").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing

        If (checkValue(dtePreCallStartTime)) Then dteCallStartTime = dtePreCallStartTime
        If (Not checkValue(dteCallStartTime)) Then dteCallStartTime = Config.DefaultDateTime
        intCallTime = DateDiff("s", CDate(dteCallStartTime), Config.DefaultDateTime) * 1000

        strSQL = "SELECT App1FullName, App2FullName, FullAddressLine1, AddressLine2, AddressLine3, AddressPostCode, App1HomeTelephone, App1WorkTelephone, App1MobileTelephone, StatusCode, SubStatusCode, StatusDescription, SubStatusDescription, DialableStatus FROM vwexportapplication WHERE AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
        objDataSet = objDatabase.executeReader(strSQL, "vwexportapplication")
        ds = objDataSet.Tables("vwexportapplication")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                strApp1FullName = Row.Item("App1FullName").ToString
                strApp2FullName = Row.Item("App2FullName").ToString
                strFullAddressLine1 = Row.Item("FullAddressLine1").ToString
                strAddressLine2 = Row.Item("AddressLine2").ToString
                strAddressLine3 = Row.Item("AddressLine3").ToString
                strAddressPostCode = Row.Item("AddressPostCode").ToString
                strApp1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                strApp1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                strApp1WorkTelephone = Row.Item("App1WorkTelephone").ToString
                strStatusDescription = Row.Item("StatusDescription").ToString
                strSubStatusDescription = Row.Item("SubStatusDescription").ToString
                strDialableStatus = Row.Item("DialableStatus").ToString
				strStatusCode = Row.Item("StatusCode").ToString
                strSubStatusCode = Row.Item("SubStatusCode").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        If (checkValue(DiaryID)) Then
            strSQL = "UPDATE tblusers SET UserActiveWorkflowDiaryID = '" & DiaryID & "' WHERE UserID = " & Config.DefaultUserID & " AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        End If

        strLockUserName = checkCaseLock(Config.DefaultUserID, AppID)
        'unlockCases(Config.DefaultUserID, AppID)
        lockCase(Config.DefaultUserID, AppID)
    End Sub

    Public Sub getProcessingScreen()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT ProcessingScreenTemplateApplicationTemplateID, ProcessingScreenTemplateCSSClass, ProcessingScreenTemplateDisplay, ProcessingScreenTemplateColumn, ApplicationTemplateName, ApplicationTemplateIcon, ProcessingScreenTemplateHeight FROM vwprocessingscreen WHERE AppID = '" & AppID & "'"
            'If (Not objLeadPlatform.Config.SuperAdmin) Then
            Dim strPrefix As String = " AND ", strSuffix As String = ""
            If (objLeadPlatform.Config.UserActiveWorkflowTypeUserLevel > 0) Then
                strSQL += " AND (ProcessingScreenUserLevel LIKE N'%|" & objLeadPlatform.Config.UserActiveWorkflowTypeUserLevel & "|%')"
            ElseIf (objLeadPlatform.Config.UserActiveLevelID > 0) Then
                strSQL += " AND (ProcessingScreenUserLevel LIKE N'%|" & objLeadPlatform.Config.UserActiveLevelID & "|%')"
            Else
                strSQL += " AND (ProcessingScreenUserLevel = N'||')"
            End If
            strSQL += " ORDER BY ProcessingScreenTemplateOrder"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row As DataRow In dsCache.Rows

                    Dim strHeight As String = ""
                    If (Row.Item("ProcessingScreenTemplateHeight") > 0) Then
                        strHeight = "height=""" & Row.Item("ProcessingScreenTemplateHeight") & """"
                    End If
                    If (Row.Item("ProcessingScreenTemplateCSSClass") <> strLastClass Or InStr(Row.Item("ProcessingScreenTemplateCSSClass"), "new ") > 0) Then
                        If (strLastClass <> "") Then
                            .WriteLine("</div>") ' /span
                        End If
                        .WriteLine("<div class=""" & Row.Item("ProcessingScreenTemplateCSSClass") & """>")
                    End If

                    .WriteLine("<div class=""widget"">")
                    .WriteLine("<div class=""widget-header"">")
                    .WriteLine("<h3><i class=""" & Row.Item("ApplicationTemplateIcon") & """></i>" & Row.Item("ApplicationTemplateName") & "</h3>")
                    If (getPageCount(Row.Item("ProcessingScreenTemplateApplicationTemplateID")) > 1) Then
                        .WriteLine("<div class=""widget-actions"">")
                        .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-file-o""></i> <span class=""title"">Pages</span> <span class=""caret""></span></button>")
                        .WriteLine("<ul class=""dropdown-menu"">")
                        .WriteLine(getPages(Row.Item("ProcessingScreenTemplateApplicationTemplateID")))
                        .WriteLine("</ul>")
                        .WriteLine("</div>")
                    End If
                    .WriteLine("</div>")
                    .WriteLine("<div class=""widget-content"">")
                    .WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                    .WriteLine("</div>") ' /widget content
                    .WriteLine("</div>") ' /widget
                    strLastClass = Replace(Row.Item("ProcessingScreenTemplateCSSClass"), "new ", "")
                    x += 1

                    '.WriteLine("<div id=""panelContent" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""panel col" & Row.Item("ProcessingScreenTemplateColumn") & """ style=""display: " & Row.Item("ProcessingScreenTemplateDisplay") & """>")
                    '.WriteLine("<div class=""content " & Row.Item("ProcessingScreenTemplateCSSClass").ToString & """>")
                    '.WriteLine("<h4>" & Row.Item("ApplicationTemplateName") & "</h4>")
                    '.WriteLine("<div class=""innerContent"" id=""content" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                    '.WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/net/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0""></iframe>")
                    '.WriteLine("<div id=""pageNavigation" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""pageNavigation displayNone smlc"">")
                    '.WriteLine("<a href="""" id=""prevPageLink" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""prevPage"" target=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                    '.WriteLine("<span id=""prevPageName" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """></span>")
                    '.WriteLine("</a>")
                    '.WriteLine("<select name=""frmPageID" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ id=""frmPageID" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""text displayNone"" onchange=""selectPage(" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & ", this.value);"">")
                    '.WriteLine("</select>")
                    '.WriteLine("<a href="""" id=""nextPageLink" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""nextPage"" target=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                    '.WriteLine("<span id=""nextPageName" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """></span>")
                    '.WriteLine("</a>")
                    '.WriteLine("</div>") ' pageNavigation
                    '.WriteLine("</div>") ' innerContent x
                    '.WriteLine("</div>") ' content class
                    '.WriteLine("</div>") ' panel
                Next
            Else
                strSQL = "SELECT ProcessingScreenTemplateApplicationTemplateID, ProcessingScreenTemplateCSSClass, ProcessingScreenTemplateDisplay, ProcessingScreenTemplateColumn, ApplicationTemplateName, ApplicationTemplateIcon, ProcessingScreenTemplateHeight FROM vwprocessingscreen WHERE AppID = '" & AppID & "' " & _
                    "AND (ProcessingScreenUserLevel = N'||') " & _
                    "ORDER BY ProcessingScreenTemplateOrder "
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = ""
                    For Each Row As DataRow In dsCache.Rows

                        Dim strHeight As String = ""
                        If (Row.Item("ProcessingScreenTemplateHeight") > 0) Then
                            strHeight = "height=""" & Row.Item("ProcessingScreenTemplateHeight") & """"
                        End If
                        If (Row.Item("ProcessingScreenTemplateCSSClass") <> strLastClass Or InStr(Row.Item("ProcessingScreenTemplateCSSClass"), "new ") > 0) Then
                            If (strLastClass <> "") Then
                                .WriteLine("</div>") ' /span
                            End If
                            .WriteLine("<div class=""" & Row.Item("ProcessingScreenTemplateCSSClass") & """>")
                        End If

                        .WriteLine("<div class=""widget"">")
                        .WriteLine("<div class=""widget-header"">")
                        .WriteLine("<h3><i class=""" & Row.Item("ApplicationTemplateIcon") & """></i>" & Row.Item("ApplicationTemplateName") & "</h3>")
                        If (getPageCount(Row.Item("ProcessingScreenTemplateApplicationTemplateID")) > 1) Then
                            .WriteLine("<div class=""widget-actions"">")
                            .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-file-o""></i> <span class=""title"">Pages</span> <span class=""caret""></span></button>")
                            .WriteLine("<ul class=""dropdown-menu"">")
                            .WriteLine(getPages(Row.Item("ProcessingScreenTemplateApplicationTemplateID")))
                            .WriteLine("</ul>")
                            .WriteLine("</div>")
                        End If
                        .WriteLine("</div>")
                        .WriteLine("<div class=""widget-content"">")
                        .WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                        .WriteLine("</div>") ' /widget content
                        .WriteLine("</div>") ' /widget
                        strLastClass = Replace(Row.Item("ProcessingScreenTemplateCSSClass"), "new ", "")
                        x += 1

                        '.WriteLine("<div id=""panelContent" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""panel col" & Row.Item("ProcessingScreenTemplateColumn") & """ style=""display: " & Row.Item("ProcessingScreenTemplateDisplay") & """>")
                        '.WriteLine("<div class=""content " & Row.Item("ProcessingScreenTemplateCSSClass").ToString & """>")
                        '.WriteLine("<h4>" & Row.Item("ApplicationTemplateName") & "</h4>")
                        '.WriteLine("<div class=""innerContent"" id=""content" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                        '.WriteLine("<iframe id=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ name=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ src=""/net/application/panel.aspx?AppID=" & AppID & "&ApplicationTemplateID=" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ width=""100%"" frameborder=""0""></iframe>")
                        '.WriteLine("<div id=""pageNavigation" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""pageNavigation displayNone smlc"">")
                        '.WriteLine("<a href="""" id=""prevPageLink" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""prevPage"" target=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                        '.WriteLine("<span id=""prevPageName" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """></span>")
                        '.WriteLine("</a>")
                        '.WriteLine("<select name=""frmPageID" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ id=""frmPageID" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""text displayNone"" onchange=""selectPage(" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & ", this.value);"">")
                        '.WriteLine("</select>")
                        '.WriteLine("<a href="""" id=""nextPageLink" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """ class=""nextPage"" target=""panel" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """>")
                        '.WriteLine("<span id=""nextPageName" & Row.Item("ProcessingScreenTemplateApplicationTemplateID") & """></span>")
                        '.WriteLine("</a>")
                        '.WriteLine("</div>") ' pageNavigation
                        '.WriteLine("</div>") ' innerContent x
                        '.WriteLine("</div>") ' content class
                        '.WriteLine("</div>") ' panel
                    Next
                Else
                    .WriteLine("<div class=""span12"">")
                    .WriteLine("<h3>Sorry, no layout is available. Please check your user permissions.</h3>")
                    .WriteLine("</div>")
                End If
            End If
            dsCache = Nothing
            HttpContext.Current.Response.Write(objStringWriter.ToString)
        End With
    End Sub

    Private Function getPageCount(ByVal templateID As String) As Integer
        Dim intNumPages As String = ""
        Dim strSQL As String = "SELECT MAX(ApplicationPageOrder) + 1 FROM vwpanel WHERE AppID = '" & AppID & "'AND ApplicationTemplateID = '" & templateID & "' AND CompanyID = '" & CompanyID & "'"
        intNumPages = New Caching(CacheObject(), strSQL, "", "", "").returnCacheString()
        If (checkValue(intNumPages)) Then
            intNumPages = CInt(intNumPages)
        Else
            intNumPages = 0
        End If
        Return intNumPages
    End Function

    Private Function getPages(ByVal templateID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT ApplicationPageName, ApplicationPageOrder FROM tblapplicationpages WHERE ApplicationPageTemplateID = '" & templateID & "' AND CompanyID = '" & CompanyID & "' AND ApplicationPageActive = 1 ORDER BY ApplicationPageOrder"
            Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<li><a href=""#"" onclick=""changePage($(this)," & AppID & "," & templateID & "," & Row.Item("ApplicationPageOrder") & ",'" & Row.Item("ApplicationPageName").ToString & "');return false;"">" & Row.Item("ApplicationPageName").ToString & "</a></li>")
                Next
            End If
            dsCache = Nothing
        End With
        Return objStringWriter.ToString
    End Function

    Public Sub transfercase()
        Dim transferSubStatus As String = getAnyFieldFromDataStore("TransferToAdvisor", AppID)
        Dim StrQry As String = ""
        If (transferSubStatus = "Continue Call With Advisor") Then
            StrQry = "UPDATE tblapplicationstatus Set SubStatusCode='CQA' where AppID='" & AppID & "'"
            executeNonQuery(StrQry)

        End If
        If (transferSubStatus = "Continue Transfer To Advisor") Then
            StrQry = "UPDATE tblapplicationstatus Set SubStatusCode='TQA' where AppID='" & AppID & "'"
            executeNonQuery(StrQry)
        End If
    End Sub

End Class