﻿Imports Microsoft.VisualBasic
Imports Common, Config

' ** Revision history **
'
' 25/09/2012	- productTermTextDropdown increased to 40 - BS
' ** End Revision History **

Public Class CommonDropdowns

    Shared Sub productTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        importValidationDropdown(cache, list, txt, val, "ProductType")
    End Sub

    Shared Sub importValidationDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, ByVal name As String)
        Dim strSQL As String = "SELECT TOP(1) ApplicationDropdownValues FROM tblimportvalidation WHERE ValName = '" & name & "' AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValStatus = 'A' ORDER BY CompanyID DESC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim arrOptions As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                For x As Integer = 0 To UBound(arrOptions)
                    list.Items.Add(New ListItem(arrOptions(x), arrOptions(x)))
                    If (checkValue(val)) Then
                        If (val = arrOptions(x)) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If
        dsCache = Nothing
    End Sub
	
	        Shared Function contactDropdownAppointments(ByVal cache As Cache, ByVal txt As String, ByVal val As String, ByVal businessobjectid As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT APP.BusinessObjectID, APP.BusinessObjectContactFirstName, APP.BusinessObjectContactSurname FROM tblbusinessobjects APP WHERE APP.BusinessObjectTypeID in(3,11) AND APP.CompanyID = '1430'  and APP.BusinessObjectParentClientID = '"& businessobjectid &"' ORDER BY BusinessObjectBusinessName ASC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()        
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname") & """"
             	If (checkValue(val)) Then
                    If (val = Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname") &"</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function
	
	        Shared Function contactDropdownBrokerQuickApp(ByVal cache As Cache, ByVal txt As String, ByVal val As String, ByVal businessobjectid As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT APP.BusinessObjectID, APP.BusinessObjectContactFirstName, APP.BusinessObjectContactSurname FROM tblbusinessobjects APP WHERE APP.BusinessObjectTypeID in(3,11) AND APP.CompanyID = '1430'  and APP.BusinessObjectParentClientID = '"& businessobjectid &"' ORDER BY BusinessObjectBusinessName ASC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()        
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectID") & """"
             	If (checkValue(val)) Then
                    If (val = Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname") &"</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function


    Shared Function repUserTextDropdownAppointments(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserRep = 1 AND CompanyID = " & CompanyID & " AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache() 
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserFullName") & """"
                If (val = Row.Item("UserFullName")) Then
                        strOptions += " selected=""selected"""
                    End If
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function importValidationTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, ByVal name As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT TOP(1) ApplicationDropdownValues FROM tblimportvalidation WHERE ValName = '" & name & "' AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValStatus = 'A' ORDER BY CompanyID DESC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim arrOptions As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                For x As Integer = 0 To UBound(arrOptions)
                    strOptions += "<option value=""" & arrOptions(x) & """"
                    If (checkValue(val)) Then
                        Dim arrValues As Array = Split(val, ",")
                        For y As Integer = 0 To UBound(arrValues)
                            If (arrValues(y) = arrOptions(x)) Then
                                strOptions += " selected=""selected"""
                            End If
                        Next
                    End If
                    strOptions += ">" & arrOptions(x) & "</option>" & vbCrLf
                Next
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub importValidationListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String, ByVal name As String)
        Dim strSQL As String = "SELECT TOP(1) ApplicationDropdownValues FROM tblimportvalidation WHERE ValName = '" & name & "' AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ApplicationInputType = 2 AND ValStatus = 'A' ORDER BY CompanyID DESC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim arrOptions As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                For x As Integer = 0 To UBound(arrOptions)
                    list.Items.Add(New ListItem(arrOptions(x), arrOptions(x)))
                    For y As Integer = 0 To UBound(arrVal)
                        If (arrVal(y) = arrOptions(x)) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    Next
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub livingArrangementListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        importValidationListBox(cache, list, txt, val, "AddressLivingArrangement")
    End Sub

    Shared Sub employmentStatusListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        importValidationListBox(cache, list, txt, val, "App1EmploymentStatus")
    End Sub

    Shared Sub productTypeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        importValidationListBox(cache, list, txt, val, "ProductType")
    End Sub

    Shared Sub mediaDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaID, MediaName FROM tblmedia WHERE MediaActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY MediaName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.item("MediaName"), Row.Item("MediaID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function mediaTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaID, MediaName FROM tblmedia WHERE MediaActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY MediaName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("MediaName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub mediaCampaignListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String, ByVal MediaCampaignID As Integer)
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim strSQL As String = "SELECT MediaCampaignID, MediaCampaignName FROM tblmediacampaigns WHERE MediaCampaignActive = 1 AND MediaCampaignID <> '" & MediaCampaignID & "' AND MediaCampaignOutbound = 0 AND CompanyID  = '" & CompanyID & "' "
        If (intSupplierCompanyID > 0) Then
            strSQL += "AND MediaID = '" & intSupplierCompanyID & "' "
        End If
        strSQL += "ORDER BY MediaCampaignName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("MediaCampaignName"), Row.Item("MediaCampaignID")))
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaCampaignID")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub mediaCampaignDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, ByVal typ As String)
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim strSQL As String = "SELECT MediaCampaignID, MediaCampaignName FROM tblmediacampaigns WHERE MediaCampaignActive = 1"
        If checkValue(typ) Then strSQL += " AND MediaCampaignOutbound = '" & typ & "'"
        If (intSupplierCompanyID > 0) Then
            strSQL += " AND MediaID = '" & intSupplierCompanyID & "' "
        End If
        strSQL += " AND CompanyID  = '" & CompanyID & "' ORDER BY MediaCampaignName "
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.item("MediaCampaignName"), Row.Item("MediaCampaignID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function mediaCampaignTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, ByVal typ As String) As String
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim intMasterMediaID As Integer = loggedInUserValue("UserMasterMediaID")
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaCampaignID, MediaCampaignName FROM tblmediacampaigns WHERE MediaCampaignActive = 1"
        If checkValue(typ) Then strSQL += " AND MediaCampaignOutbound = '" & typ & "'"
        If (intSupplierCompanyID > 0 Or intMasterMediaID > 0) Then
            strSQL += " AND ("
        End If
        If (intSupplierCompanyID > 0) Then
            strSQL += " MediaID = '" & intSupplierCompanyID & "' "
        End If
        If (intMasterMediaID > 0) Then
            If (intSupplierCompanyID > 0) Then
                strSQL += " OR MediaCampaignPurchaseMediaID = '" & intMasterMediaID & "' "
            Else
                strSQL += " MediaCampaignPurchaseMediaID = '" & intMasterMediaID & "' "
            End If
        End If
        If (intSupplierCompanyID > 0 Or intMasterMediaID > 0) Then
            strSQL += " )"
        End If
        strSQL += " AND CompanyID  = '" & CompanyID & "' ORDER BY MediaCampaignName "
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaCampaignID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("MediaCampaignName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function mediaCampaignOutboundTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return mediaCampaignTextDropdown(cache, txt, val, 1)
    End Function

    Shared Function sourceTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaCampaignID, MediaCampaignName FROM tblmediacampaigns WHERE MediaCampaignActive = 1 AND MediaCampaignOutbound = '0'"
        If (intSupplierCompanyID > 0) Then
            strSQL += " AND MediaID = '" & intSupplierCompanyID & "' "
        End If
        strSQL += " AND CompanyID  = '" & CompanyID & "' ORDER BY MediaCampaignName "
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        'strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignID") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignID")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("MediaCampaignName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function cloneMediaCampaignTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaCampaignID, MediaCampaignName, MediaCampaignProductType FROM tblmediacampaigns WHERE MediaCampaignActive = 1 AND MediaCampaignOutbound = 0 AND MediaCampaignCloneableTo = 1"
        If (intSupplierCompanyID > 0) Then
            strSQL += " AND MediaID = '" & intSupplierCompanyID & "' "
        End If
        strSQL += " AND CompanyID  = '" & CompanyID & "' ORDER BY MediaCampaignName "
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        'strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignID") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignID")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("MediaCampaignName") & " - " & Row.Item("MediaCampaignProductType").ToString & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub mediaCampaignByTelNoDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim intSupplierCompanyID As Integer = loggedInUserValue("UserSupplierCompanyID")
        Dim strSQL As String = "SELECT MC.MediaCampaignID, MC.MediaCampaignFriendlyNameInbound FROM tblmediacampaigns MC INNER JOIN tblmediacampaigntelephonenumbers TEL ON TEL.MediaCampaignTelephoneNumberID = MC.MediaCampaignTelephoneNumberIDInbound " & _
            "WHERE NOT MC.MediaCampaignFriendlyNameInbound IS NULL AND MC.MediaCampaignActive = 1 AND CompanyID  = '" & CompanyID & "' "
        If (intSupplierCompanyID > 0) Then
            strSQL += "AND MediaID = '" & intSupplierCompanyID & "' "
        End If
        strSQL += "ORDER BY MC.MediaCampaignName "
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblmediacampaigns")
        Dim dsMediaCampaigns As DataTable = objDataSet.Tables("tblmediacampaigns")
        list.Items.Add(New ListItem(txt, ""))
        If (dsMediaCampaigns.Rows.Count > 0) Then
            For Each Row As DataRow In dsMediaCampaigns.Rows
                list.Items.Add(New ListItem(Row.item("MediaCampaignFriendlyNameInbound"), Row.Item("MediaCampaignID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsMediaCampaigns = Nothing
        objDataBase = Nothing
    End Sub

    Shared Function reportGroupTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrValues As String() = {"Grade", "Media", "MediaOutbound", "MediaCampaign", "MediaCampaignOutbound", "Product", "SalesUser", "CallCentreUser", "AdministratorUser"}
        Dim arrText As String() = {"Grade", "By Media", "By Outbound Media", "By Media Campaign", "By Outbound Campaign", "By Product", "Sales User", "Call Centre User", "Administrator User"}
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        For x As Integer = 0 To UBound(arrValues)
            strOptions += "<option value=""" & arrValues(x) & """"
            If (checkValue(val)) Then
                If (val = arrValues(x)) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & arrText(x) & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Function businessObjectReportGroupTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrValues As String() = {"Grade", "Media", "MediaCampaign", "Product", "User", "Type", "Business"}
        Dim arrText As String() = {"By Grade", "By Media", "By Media Campaign", "By Product", "By User", "By Type", "By Business"}
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        For x As Integer = 0 To UBound(arrValues)
            strOptions += "<option value=""" & arrValues(x) & """"
            If (checkValue(val)) Then
                If (val = arrValues(x)) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & arrText(x) & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Sub propertyTypeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        importValidationListBox(cache, list, txt, val, "PropertyType")
    End Sub

    Shared Sub propertyConstructionTypeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        importValidationListBox(cache, list, txt, val, "PropertyConstructionType")
    End Sub

    Shared Sub hotkeyUserDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal MediaID As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaUserRef, MediaUserName FROM tblmediausers WHERE MediaCompanyID = '" & MediaID & "' AND MediaID  = '" & CompanyID & "' AND MediaUserActive = 1 ORDER BY MediaUserName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("MediaUserName"), Row.Item("MediaUserRef")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaUserRef")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub countryCodesListBox(ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim arrVal As Array = Split(val, ",")
        Dim arrCountryCodes As String() = {"ENG", "NI", "SCO", "WAL"}
        list.Items.Add(New ListItem(txt, ""))
        For Each Item As String In arrCountryCodes
            list.Items.Add(New ListItem(Item, Item))
            For x As Integer = 0 To UBound(arrVal)
                If (arrVal(x) = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        Next
    End Sub

    Shared Sub numberDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String, ByVal start As Integer, ByVal finish As Integer, Optional ByVal stepping As Integer = 1)
        list.Items.Add(New ListItem(txt, ""))
        For x As Integer = start To finish Step stepping
            list.Items.Add(New ListItem(x, x))
            If (x = val) Then
                list.Items(list.Items.Count - 1).Selected = True
            End If
        Next
    End Sub

    Shared Function numberTextDropdown(ByVal txt As String, ByVal val As String, ByVal start As Integer, ByVal finish As Integer, Optional ByVal stepping As Integer = 1) As String
        Dim strOptions As String = ""
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For x As Integer = start To finish Step stepping
            strOptions += "<option value=""" & x & """"
            If (x = val) Then
                strOptions += " selected=""selected"""
            End If
            strOptions += ">" & x & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Sub numberListBox(ByVal list As ListBox, ByVal txt As String, ByVal val As String, ByVal start As Integer, ByVal finish As Integer, Optional ByVal stepping As Integer = 1)
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        For x As Integer = start To finish Step stepping
            list.Items.Add(New ListItem(x, x))
            For y As Integer = 0 To UBound(arrVal)
                If (arrVal(y).ToString = x.ToString) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        Next
    End Sub

    Shared Sub yesNoDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        list.Items.Add(New ListItem(txt, ""))
        list.Items.Add(New ListItem("Yes", "Y"))
        If (val = "Y") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("No", "N"))
        If (val = "N") Then list.Items(list.Items.Count - 1).Selected = True
    End Sub

    Shared Sub mediaCampaignGradeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaCampaignGradeID, MediaCampaignGradeName FROM tblmediacampaigngrades WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignGradeActive = 1 ORDER BY MediaCampaignGradeName" ' WHERE MediaCampaignParentGradeID <> 0 ORDER BY MediaCampaignGradeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("MediaCampaignGradeName"), Row.Item("MediaCampaignGradeID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignGradeID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub mediaCampaignGradeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaCampaignGradeID, MediaCampaignGradeName FROM tblmediacampaigngrades WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignGradeActive = 1 ORDER BY MediaCampaignGradeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("MediaCampaignGradeName"), Row.Item("MediaCampaignGradeID")))
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaCampaignGradeID")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function mediaCampaignGradeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaCampaignGradeID, MediaCampaignGradeName FROM tblmediacampaigngrades WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignGradeActive = 1 ORDER BY MediaCampaignGradeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignGradeID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaCampaignGradeID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("MediaCampaignGradeName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectMediaCampaignGradeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT MediaCampaignGradeID, MediaCampaignGradeName FROM tblmediacampaigngrades WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignGradeActive = 1 AND MediaCampaignGradeBusinessObject = 1 ORDER BY MediaCampaignGradeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignGradeID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("MediaCampaignGradeID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("MediaCampaignGradeName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub mediaCampaignParentGradeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaCampaignParentGradeID, MediaCampaignGradeID, MediaCampaignGradeName FROM tblmediacampaigngrades WHERE CompanyID = '" & CompanyID & "' ORDER BY MediaCampaignGradeName"
        Dim strDropdownName As String = ""
        Dim strDropdownValue As String = ""
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (Row.Item("MediaCampaignParentGradeID") = 0) Then
                    strDropdownName = Row.Item("MediaCampaignGradeName")
                    strDropdownValue = Row.Item("MediaCampaignGradeID") & ","
                Else
                    strDropdownName = " - " & Row.Item("MediaCampaignGradeName")
                    strDropdownValue = Row.Item("MediaCampaignParentGradeID") & "," & Row.Item("MediaCampaignGradeID")
                End If
                list.Items.Add(New ListItem(strDropdownName, strDropdownValue))
                If (checkValue(val)) Then
                    If (val = strDropdownValue) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function productTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "ProductType")
    End Function

    Shared Function productTermTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        'strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For x As Integer = 1 To 40
            strOptions += "<option value=""" & (x * 12) & """"
            If (checkValue(val)) Then
                If (CInt(val) = x * 12) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & x & " Years" & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function
	
	  Shared Function productTermMonthsTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        'strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For x As Integer = 1 To 60
            strOptions += "<option value=""" & (x) & """"
            If (checkValue(val)) Then
                If (CInt(val) = x ) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & x & " Months" & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Function productPurposeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "ProductPurpose")
    End Function

    Shared Function titleTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "App1Title")
    End Function

    Shared Function maritalStatusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "App1MaritalStatus")
    End Function

    Shared Function genderTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "App1Sex")
    End Function

    Shared Function livingArrangementTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "AddressLivingArrangement")
    End Function

    Shared Function propertyTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "PropertyType")
    End Function

    Shared Function constructionTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "PropertyConstructionType")
    End Function

    Shared Function yesNoTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        strOptions += "<option value=""Y"""
        If (checkValue(val)) Then
            If (val = "Y") Then
                strOptions += " selected=""selected"""
            End If
        End If
        strOptions += ">Yes</option>" & vbCrLf
        strOptions += "<option value=""N"""
        If (checkValue(val)) Then
            If (val = "N") Then
                strOptions += " selected=""selected"""
            End If
        End If
        strOptions += ">No</option>" & vbCrLf
        Return strOptions
    End Function

    Shared Function callBackTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrValues As String() = {"1", "2", "3", "4", "5"}
        Dim arrText As String() = {"Daytime 09:00 - 12:00", "Daytime 12:00 - 18:00", "Evenings 18:00 - 21:00", "Weekends 09:00 - 12:00", "Weekends 12:00 - 18:00"}
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For x As Integer = 0 To UBound(arrValues)
            strOptions += "<option value=""" & arrValues(x) & """"
            If (checkValue(val)) Then
                If (val = arrValues(x)) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & arrText(x) & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Function employmentStatusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Return importValidationTextDropdown(cache, txt, val, "App1EmploymentStatus")
    End Function

    Shared Sub dateTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, Optional ByVal friendly As Boolean = False)
        Dim strSQL As String = "SELECT DateType FROM tbldatetypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DateTypeActive = 1 ORDER BY DateType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (friendly) Then
                    list.Items.Add(New ListItem(Regex.Replace(Row.Item("DateType"), "([A-Z0-9])", " $1"), Row.Item("DateType")))
                Else
                    list.Items.Add(New ListItem(Row.Item("DateType"), Row.Item("DateType")))
                End If
                If (checkValue(val)) Then
                    If (val = Row.Item("DateType")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub dateTypeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT DateType FROM tbldatetypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DateTypeActive = 1 ORDER BY DateType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("DateType") & "Date", Row.Item("DateType")))
                For x As Integer = 0 To UBound(arrVal)
                    If (arrVal(x) = Row.Item("DateType")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function dateTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal friendly As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT DateType FROM tbldatetypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DateTypeActive = 1 ORDER BY DateType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("DateType") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("DateType")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                If (friendly) Then
                    strOptions += ">" & Regex.Replace(Row.Item("DateType"), "([A-Z0-9])", " $1") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("DateType") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectDateTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal friendly As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT DateType FROM tbldatetypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DateTypeActive = 1 AND DateTypeBusinessObject = 1 ORDER BY DateType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("DateType") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("DateType")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                If (friendly) Then
                    strOptions += ">" & Regex.Replace(Row.Item("DateType"), "([A-Z0-9])", " $1") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("DateType") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function diaryTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT DiaryTypeName, DiaryTypeDescription FROM tbldiarytypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DiaryTypeActive = 1 ORDER BY DiaryTypeDescription"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("DiaryTypeName") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("DiaryTypeName")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("DiaryTypeDescription") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub userTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT COLS.name AS ColumnName FROM dbo.sysobjects AS OBJ INNER JOIN dbo.syscolumns AS COLS ON OBJ.id = COLS.id INNER JOIN dbo.systypes AS TYP ON COLS.xusertype = TYP.xusertype WHERE (OBJ.name = N'tblapplicationstatus') AND (COLS.name LIKE N'%UserID%') ORDER BY ColumnName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ColumnName"), Row.Item("ColumnName")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ColumnName")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub inputTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT InputTypeID, InputType FROM tblinputtypes"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("InputType"), Row.Item("InputTypeID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("InputTypeID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub
	
	    Shared Sub businessObjectBrokers(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT BusinessObjectBusinessName FROM tblbusinessobjects where businessobjecttypeid = 5"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("BusinessObjectBusinessName")))
                If (checkValue(val)) Then
                    If (val = Row.Item("BusinessObjectBusinessName")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub	
	
	Shared Function businessObjectNetworks(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT BusinessObjectBusinessName, b.BusinessObjectID FROM tblbusinessobjects b inner join tblbusinessobjectstatus s on b.businessobjectid = s.businessobjectid where b.businessobjecttypeid = 8 and s.businessobjectactive = 1 order by BusinessObjectBusinessName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",") 
		Dim strOptions As String  
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then 
                        If (arrVal(x) = Row.Item("BusinessObjectID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("BusinessObjectBusinessName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function	
	
	Shared Function mortgageDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strSQL As String =  "SELECT  DISTINCT LenderName, LenderID FROM tbllenders where LenderType = 'Mortgage' ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",") 
                                Dim strOptions As String  
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("LenderName") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then 
                        If (arrVal(x) = Row.Item("LenderName")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("LenderName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function  

	
		Shared Sub businessObjectContacts(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT BusinessObjectBusinessName FROM tblbusinessobjects where businessobjecttypeid = 3"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("BusinessObjectBusinessName")))
                If (checkValue(val)) Then
                    If (val = Row.Item("BusinessObjectBusinessName")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub	

    Shared Sub priorityDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT PriorityID, PriorityName FROM tblpriorities"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("PriorityName"), Row.Item("PriorityID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("PriorityID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub statusListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT StatusID, StatusCode, StatusDescription FROM tblstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND StatusActive = 1 ORDER BY StatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("StatusCode") & " - " & Row.Item("StatusDescription"), Row.Item("StatusCode")))
                For x As Integer = 0 To UBound(arrVal)
                    If (Trim(arrVal(x)) = Trim(Row.Item("StatusCode").ToString)) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub subStatusListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String, Optional ByVal novalue As Boolean = False)
        Dim strSQL As String = "SELECT SubStatusID, SubStatusCode, SubStatusDescription FROM tblsubstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND SubStatusActive = 1 ORDER BY SubStatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (novalue) Then
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription"), Row.Item("SubStatusCode")))
                    For x As Integer = 0 To UBound(arrVal)
                        If (Trim(arrVal(x)) = Trim(Row.Item("SubStatusCode").ToString)) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    Next
                Else
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription"), Row.Item("SubStatusID")))
                    For x As Integer = 0 To UBound(arrVal)
                        If (arrVal(x) = Row.Item("SubStatusID").ToString) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    Next
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub statusDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False)
        Dim strSQL As String = "SELECT StatusID, StatusCode, StatusDescription FROM tblstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND StatusActive = 1 ORDER BY StatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (lng) Then
                    list.Items.Add(New ListItem(Row.Item("StatusCode") & " - " & Row.Item("StatusDescription"), Row.Item("StatusCode")))
                Else
                    list.Items.Add(New ListItem(Row.Item("StatusCode"), Row.Item("StatusCode")))
                End If
                If (checkValue(val)) Then
                    If (val = Trim(Row.Item("StatusCode"))) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub subStatusDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False)
        Dim strSQL As String = "SELECT SubStatusID, SubStatusCode, SubStatusDescription FROM tblsubstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND SubStatusActive = 1 ORDER BY SubStatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (lng) Then
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription"), Row.Item("SubStatusCode")))
                Else
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode"), Row.Item("SubStatusCode")))
                End If
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function statusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT StatusID, StatusCode, StatusDescription FROM tblstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND StatusActive = 1 ORDER BY StatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("StatusCode") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("StatusCode")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                If (lng) Then
                    strOptions += ">" & Row.Item("StatusCode") & " - " & Row.Item("StatusDescription") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("StatusCode") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function subStatusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT SubStatusID, SubStatusCode, SubStatusDescription FROM tblsubstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND SubStatusActive = 1 ORDER BY SubStatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("SubStatusCode") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                If (lng) Then
                    strOptions += ">" & Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("SubStatusCode") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectStatusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT StatusID, StatusCode, StatusDescription FROM tblstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND StatusActive = 1 AND StatusBusinessObject = 1 ORDER BY StatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("StatusCode") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("StatusCode")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                If (lng) Then
                    strOptions += ">" & Row.Item("StatusCode") & " - " & Row.Item("StatusDescription") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("StatusCode") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectSubStatusTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String, Optional ByVal lng As Boolean = False) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT SubStatusID, SubStatusCode, SubStatusDescription FROM tblsubstatuses WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND SubStatusActive = 1 AND SubStatusBusinessObject = 1 ORDER BY SubStatusCode"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("SubStatusCode") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                If (lng) Then
                    strOptions += ">" & Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription") & "</option>" & vbCrLf
                Else
                    strOptions += ">" & Row.Item("SubStatusCode") & "</option>" & vbCrLf
                End If
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub subStatusMappedDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal StatusID As String, ByVal val As String, Optional ByVal lng As Boolean = False)
        Dim strSQL As String = "SELECT SST.SubStatusCode, SST.SubStatusDescription FROM tblsubstatuses SST " & _
                     "INNER JOIN tblstatusmapping MAP ON MAP.SubStatusID = SST.SubStatusID " & _
                     "WHERE SST.CompanyID = '" & CompanyID & "' AND MAP.StatusID = '" & StatusID & "' AND SubStatusActive = 1 " & _
                     "ORDER BY SubStatusDescription"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (lng) Then
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode") & " - " & Row.Item("SubStatusDescription"), Row.Item("SubStatusCode")))
                Else
                    list.Items.Add(New ListItem(Row.Item("SubStatusCode"), Row.Item("SubStatusCode")))
                End If
                If (checkValue(val)) Then
                    If (val = Row.Item("SubStatusCode")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub applicationTemplateDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT ApplicationTemplateID, ApplicationTemplateName FROM tblapplicationtemplates WHERE CompanyID = '" & CompanyID & "' AND ApplicationTemplateActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ApplicationTemplateName"), Row.Item("ApplicationTemplateID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ApplicationTemplateID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function applicationTemplateTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT ApplicationTemplateID, ApplicationTemplateName FROM tblapplicationtemplates WHERE CompanyID = '" & CompanyID & "' AND ApplicationTemplateActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("ApplicationTemplateID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("ApplicationTemplateID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("ApplicationTemplateName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub daysListbox(ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        If (checkValue(txt)) Then
            list.Items.Add(New ListItem(txt, ""))
        End If
        Dim arrDayNames As String() = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}
        Dim arrDayValues As String() = {1, 2, 3, 4, 5, 6, 7}
        For a As Integer = 0 To UBound(arrDayNames)
            list.Items.Add(New ListItem(arrDayNames(a), arrDayValues(a)))
            If (checkValue(val)) Then
                If (val = arrDayValues(a)) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            End If
        Next
    End Sub

    Shared Function daysTextDropdown(ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrDayNames As String() = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}
        Dim arrDayValues As String() = {1, 2, 3, 4, 5, 6, 7}
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        For x As Integer = 0 To UBound(arrDayNames)
            strOptions += "<option value=""" & arrDayValues(x) & """"
            If (checkValue(val)) Then
                Dim arrVal As Array = Split(val, ",")
                For y As Integer = 0 To UBound(arrVal)
                    If (arrVal(y) = arrDayValues(x)) Then
                        strOptions += " selected=""selected"""
                    End If
                Next
            End If
            strOptions += ">" & arrDayNames(x) & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Sub weeksDropdown(ByVal list As DropDownList, ByVal val As String)
        Dim dteWeekStart As Date = Config.DefaultDate.AddDays(Weekday(Config.DefaultDate) - 2)
        For i As Integer = 0 To 26
            Dim dteWeek As Date = DateAdd(DateInterval.WeekOfYear, -i, dteWeekStart)
            list.Items.Add(New ListItem(dteWeek, dteWeek))
            If (checkValue(val)) Then
                If (val = dteWeek) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            End If
        Next
    End Sub

    Shared Function weeksTextDropdown(ByVal cache As Cache, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim dteWeekStart As Date = Config.DefaultDate.AddDays(2 - Weekday(Config.DefaultDate))
        For i As Integer = 0 To 26
            Dim dteWeek As Date = DateAdd(DateInterval.WeekOfYear, -i, dteWeekStart)
            strOptions += "<option value=""" & dteWeek & """"
            If (checkValue(val)) Then
                If (val = dteWeek) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            strOptions += ">" & dteWeek & "</option>" & vbCrLf
        Next
        Return strOptions
    End Function

    Shared Function startMonthsTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For i As Integer = 1 To 13
            strOptions += "<option value=""" & i & """"
            If (checkValue(val)) Then
                If (val = i) Then
                    strOptions += " selected=""selected"""
                End If
            End If
            If (i = 13) Then
                strOptions += ">After " & i - 1 & " months</option>" & vbCrLf
            Else
                strOptions += ">Within " & i & " months</option>" & vbCrLf
            End If
        Next
        Return strOptions
    End Function

    Shared Sub mediaCampaignTelephoneNumberDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT MediaCampaignTelephoneNumberID, MediaCampaignTelephoneNumber, MediaCampaignTelephoneNumberFriendlyName FROM tblmediacampaigntelephonenumbers WHERE CompanyID = '" & CompanyID & "' AND MediaCampaignTelephoneNumberActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("MediaCampaignTelephoneNumber") & " - " & Row.Item("MediaCampaignTelephoneNumberFriendlyName"), Row.Item("MediaCampaignTelephoneNumberID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaCampaignTelephoneNumberID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub commTemplateDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT CommunicationTemplateID, CommunicationTemplateName FROM tblcommunicationtemplates WHERE CompanyID = '" & CompanyID & "' AND CommunicationTemplateActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("CommunicationTemplateName"), Row.Item("CommunicationTemplateID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("CommunicationTemplateID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub userLevelListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UserLevelID, UserLevel FROM tbluserlevels WHERE UserLevelActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserLevel"), Row.Item("UserLevelID")))
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserLevelID")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub userLevelDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UserLevelID, UserLevel FROM tbluserlevels WHERE UserLevelActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserLevel"), Row.Item("UserLevelID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("UserLevelID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub displayDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        list.Items.Add(New ListItem(txt, ""))
        list.Items.Add(New ListItem("block", "block"))
        If (val = "Y") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("none", "none"))
        If (val = "N") Then list.Items(list.Items.Count - 1).Selected = True
    End Sub

    Shared Sub appManagementDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT AppManagementID, AppManagementName FROM tblapplicationmanagement WHERE CompanyID = '" & CompanyID & "' AND AppManagementActive = 1 ORDER BY AppManagementName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.item("AppManagementName"), Row.Item("AppManagementID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("AppManagementID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub panelDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT ApplicationTemplateID, ApplicationTemplateName FROM tblapplicationtemplates WHERE CompanyID = '" & CompanyID & "' AND ApplicationTemplateActive = 1 ORDER BY ApplicationTemplateName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.item("ApplicationTemplateName"), Row.Item("ApplicationTemplateID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ApplicationTemplateID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub applicationPageDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, ByVal ApplicationTemplateID As String)
        Dim strSQL As String = "SELECT ApplicationPageOrder, ApplicationPageName FROM tblapplicationpages WHERE CompanyID = '" & CompanyID & "' AND ApplicationPageTemplateID = '" & ApplicationTemplateID & "' ORDER BY ApplicationPageOrder"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ApplicationPageOrder") + 1 & ". " & Row.Item("ApplicationPageName"), Row.Item("ApplicationPageOrder")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ApplicationPageOrder")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function pageSelectDropdown(ByVal cache As Cache, ByVal val As String, ByVal ApplicationTemplateID As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT ApplicationPageOrder, ApplicationPageName FROM tblapplicationpages WHERE ApplicationPageTemplateID = '" & ApplicationTemplateID & "' AND ApplicationPageActive = 1 ORDER BY ApplicationPageOrder"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("ApplicationPageOrder") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("ApplicationPageOrder")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("ApplicationPageName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub dataTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT DataTypeCode, DataTypeName, DataTypeRegEx FROM tbldatatypes WHERE DataTypeActive = 1 ORDER BY DataTypeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("DataTypeName"), Row.Item("DataTypeCode")))
                If (checkValue(val)) Then
                    If (val = Row.Item("DataTypeCode")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
                list.Items(list.Items.Count - 1).Attributes.Item("rel") = Row.Item("DataTypeRegEx").ToString
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub letterDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, Optional ByVal ltype As Integer = 1)
        Dim strSQL As String = "SELECT LetterID, LetterName FROM tbllettertemplates WHERE CompanyID = '" & CompanyID & "' AND LetterType = " & ltype & " AND LetterActive = 1 ORDER BY LetterName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("LetterName"), Row.Item("LetterID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("LetterID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub emailSMSDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT LetterID, LetterName FROM tbllettertemplates WHERE CompanyID = '" & CompanyID & "' AND (LetterType = 3 OR LetterType = 2) AND LetterActive = 1 ORDER BY LetterName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("LetterName"), Row.Item("LetterID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("LetterID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub pdfDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT PDFID, PDFName FROM tblpdfs WHERE CompanyID = '" & CompanyID & "' AND PDFActive = 1 ORDER BY PDFName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("PDFName"), Row.Item("PDFID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("PDFID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub uploadTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UploadTypeID, UploadTypeName FROM tbluploadtypes WHERE (UploadTypeActive = 1) AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY UploadTypeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UploadTypeName"), Row.Item("UploadTypeName")))
                If (checkValue(val)) Then
                    If (val = Row.Item("UploadTypeName")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function transferUserList(ByVal cache As Web.Caching.Cache, ByVal txt As String, ByVal MediaID As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserSales = 1 AND CompanyID = " & CompanyID & " AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("UserID") & """")
                If (checkValue(val)) Then
                    If (val = Row.Item("UserID")) Then
                        objStringBuilder.Append(" selected=""selected""")
                    End If
                End If
                objStringBuilder.Append(">" & Row.Item("UserFullName") & "</option>")
            Next
        End If
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

    Shared Function hotkeyUserList(ByVal cache As Web.Caching.Cache, ByVal txt As String, ByVal MediaID As String, ByVal val As String) As String
        Dim strSQL As String = "SELECT MediaUserID, MediaUserName FROM tblmediausers WHERE CompanyID = '" & CompanyID & "' AND MediaUserMediaID = '" & MediaID & "' AND MediaUserActive = 1 ORDER BY MediaUserName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim objStringBuilder As New StringBuilder
        objStringBuilder.Append("<option value="""">" & txt & "</option>")
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringBuilder.Append("<option value=""" & Row.Item("MediaUserID") & """")
                If (checkValue(val)) Then
                    If (val = Row.Item("MediaUserID")) Then
                        objStringBuilder.Append(" selected=""selected""")
                    End If
                End If
                objStringBuilder.Append(">" & Row.Item("MediaUserName") & "</option>")
            Next
        End If
        dsCache = Nothing
        Return objStringBuilder.ToString
    End Function

    Shared Function callCentreUserTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserCallCentre = 1 AND CompanyID = " & CompanyID & " AND UserSuperAdmin = 0 AND UserRep = 0 AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub callCentreUserListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserCallCentre = 1 AND CompanyID = " & CompanyID & " AND UserSuperAdmin = 0 AND UserRep = 0 AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            list.Items.Add(New ListItem(txt, ""))
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserFullName"), Row.Item("UserID")))
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Function salesUserTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserSales = 1 AND UserSuperAdmin = 0 AND UserRep = 0 AND CompanyID = " & CompanyID & " AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
			strOptions += "<option value=""0"">Unassigned</option>" & vbCrLf
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub salesUserListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserSales = 1 AND UserSuperAdmin = 0 AND UserRep = 0 AND CompanyID = " & CompanyID & " AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            list.Items.Add(New ListItem(txt, ""))
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserFullName"), Row.Item("UserID")))
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            Next
        End If
        dsCache = Nothing
    End Sub
	
    Shared Function adminUserTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserAdministrator = 1 AND CompanyID = " & CompanyID & " AND UserSuperAdmin = 0 AND UserRep = 0 AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function repUserTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE UserRep = 1 AND CompanyID = " & CompanyID & " AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value=""0"">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub letterTypeDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        list.Items.Add(New ListItem(txt, ""))
        list.Items.Add(New ListItem("Letter", "1"))
        If (val = "1") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("Email", "2"))
        If (val = "2") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("SMS", "3"))
        If (val = "3") Then list.Items(list.Items.Count - 1).Selected = True
    End Sub

    Shared Sub letterBackgroundDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim objStringBuilder As New StringBuilder
        Dim arrFiles As String() = Directory.GetFiles(HttpContext.Current.Server.MapPath("\letters\backgrounds"))
        list.Items.Add(New ListItem(txt, ""))
        For Each File As String In arrFiles
            Dim strFile As String = Replace(File, HttpContext.Current.Server.MapPath("\letters\backgrounds") & "\", "")
            list.Items.Add(New ListItem(strFile, strFile))
            If (checkValue(val)) Then
                If (val = strFile) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            End If
        Next
    End Sub

    Shared Sub letterParagraphDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT ParagraphID, ParagraphName FROM tblletterparagraphs WHERE CompanyID = '" & CompanyID & "' AND ParagraphActive = 1 ORDER BY ParagraphName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ParagraphName"), Row.Item("ParagraphID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ParagraphID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
		
    End Sub
	
    Shared Sub lenderTypeDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        list.Items.Add(New ListItem(txt, ""))
        list.Items.Add(New ListItem("Secured", "Secured"))
        If (val = "Secured") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("Mortgage", "Mortgage"))
        If (val = "Mortgage") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("Insurance", "Insurance"))
        If (val = "Insurance") Then list.Items(list.Items.Count - 1).Selected = True
		list.Items.Add(New ListItem("CreditCard", "CreditCard"))
        If (val = "CreditCard") Then list.Items(list.Items.Count - 1).Selected = True
    End Sub

    Shared Sub emailAddressDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal AppID As String)
        Dim strSQL As String = "SELECT App1EmailAddress, App2EmailAddress, brokercontactemail FROM vwexportapplication WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("App1EmailAddress").ToString)) Then
                    list.Items.Add(New ListItem(Row.Item("App1EmailAddress"), Row.Item("App1EmailAddress")))
                End If
                If (checkValue(Row.Item("App2EmailAddress").ToString)) Then
                    list.Items.Add(New ListItem(Row.Item("App2EmailAddress"), Row.Item("App2EmailAddress")))
                End If
				 If (checkValue(Row.Item("brokercontactemail").ToString)) Then
                    list.Items.Add(New ListItem(Row.Item("brokercontactemail") & " (Broker Email)", Row.Item("brokercontactemail")))
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub telephoneNumberDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal AppID As String)
        Dim strSQL As String = "SELECT App1MobileTelephone, App2MobileTelephone, BusinessObjectContactMobileTelephone FROM vwexportapplication WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("App1MobileTelephone").ToString)) Then
                    list.Items.Add(New ListItem(Row.Item("App1MobileTelephone"), Row.Item("App1MobileTelephone")))
                End If
				If (checkValue(Row.Item("App2MobileTelephone").ToString)) Then
					list.Items.Add(New ListItem(Row.Item("App2MobileTelephone"), Row.Item("App2MobileTelephone")))
				End If
				If (checkValue(Row.Item("BusinessObjectContactMobileTelephone").ToString)) Then
					list.Items.Add(New ListItem(Row.Item("BusinessObjectContactMobileTelephone") & " (Broker Telephone)", Row.Item("BusinessObjectContactMobileTelephone")))
				End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub userDropdown(ByVal cache As Web.Caching.Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE CompanyID = '" & CompanyID & "' AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserFullName"), Row.Item("UserID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("UserID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function userTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE CompanyID = " & CompanyID & " AND UserFullName NOT LIKE N'%wall board%' AND UserFullName NOT LIKE N'%wallboard%' AND UserSupplierCompanyID = 0 AND UserPartnerCompanyID = 0 AND UserActive = 1 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub userDropdownByRole(ByVal cache As Web.Caching.Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String, Optional role As String = "CallCentre")
        Dim strSQL As String = "SELECT UserID, UserFullName FROM tblusers WHERE CompanyID = '" & CompanyID & "' AND User" & role & " = 1 AND UserActive = 1 AND UserSuperAdmin = 0 AND UserRep = 0 ORDER BY UserFullName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("UserFullName"), Row.Item("UserID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("UserID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function securedLenderTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT LenderName FROM tbllenders WHERE LenderType = 'Secured' AND LenderActive = 1 AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("LenderName") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("LenderName")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("LenderName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function mortgageLenderTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT LenderName FROM tbllenders WHERE LenderType = 'Mortgage' AND LenderActive = 1 AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("LenderName") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("LenderName")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("LenderName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function insuranceLenderTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT LenderName FROM tbllenders WHERE LenderType = 'Insurance' AND LenderActive = 1 AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("LenderName") & """"
                If (checkValue(val)) Then
                    If (val = Row.Item("LenderName")) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & Row.Item("LenderName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub reportFilterDropdown(ByVal list As DropDownList, ByVal view As String, ByVal txt As String, ByVal val As String)
        Dim strConfigName As String = "ReportBuilderFilters"
        If (view = "vwbusinessobjects" Or view = "vwbusinessobjectsdiarylist") Then
            strConfigName = "BusinessObjectReportBuilderFilters"
        End If
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND SystemConfigurationName = N'" & strConfigName & "' ORDER BY CompanyID DESC"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportColumnDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal view As String, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "", boolGroup = False
        If (view = "vwreportcaselist") Or (view = "vwreportdiarylist") Or (view = "vwreportcasehistory") Or (view = "vwbusinessobjects") Or (view = "vwbusinessobjectsdiarylist") Then

            If (view <> "vwbusinessobjects" And view <> "vwbusinessobjectsdiarylist") Then
                strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderColumns'"
                Dim objDatabase As DatabaseManager = New DatabaseManager
                Dim objResult As Object = objDatabase.executeScalar(strSQL)
                list.Items.Add(New ListItem(txt, ""))
                If (objResult IsNot Nothing) Then
                    Dim arrSort As Array = Split(objResult.ToString, "|")
                    For Each Item As String In arrSort
                        If (view = "vwreportcaselist") And Not InStr(Item, "colDiary") Or (view = "vwreportdiarylist") Then
                            list.Items.Add(New ListItem(Item, Item))
                            If (val = Item) Then
                                list.Items(list.Items.Count - 1).Selected = True
                            End If
                        End If
                    Next
                End If
                objResult = Nothing
                objDatabase = Nothing
            Else
                strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'BusinessObjectReportBuilderColumns'"
                Dim objDatabase As DatabaseManager = New DatabaseManager
                Dim objResult As Object = objDatabase.executeScalar(strSQL)
                list.Items.Add(New ListItem(txt, ""))
                If (objResult IsNot Nothing) Then
                    Dim arrSort As Array = Split(objResult.ToString, "|")
                    For Each Item As String In arrSort
                        If (view = "vwbusinessobjects") And Not InStr(Item, "colDiary") Or (view = "vwbusinessobjectsdiarylist") Then
                            list.Items.Add(New ListItem(Item, Item))
                            If (val = Item) Then
                                list.Items(list.Items.Count - 1).Selected = True
                            End If
                        End If
                    Next
                End If
                objResult = Nothing
                objDatabase = Nothing
            End If

            If (view = "vwreportcasehistory") Then
                view = "vwreportcaselist"
                boolGroup = True
            End If

            strSQL = "SELECT ColumnName FROM vwsysfields WHERE (TableName = N'" & view & "') "
            If boolGroup = True Then
                strSQL += "AND (FieldType = N'int' OR FieldType = N'money' OR FieldType = N'float') "
            Else
                strSQL += "AND ColumnName <> N'AppID' " & _
                "AND ColumnName <> N'Amount' " & _
                "AND ColumnName <> N'AmountOriginal' " & _
                "AND ColumnName <> N'DialAttempts' " & _
                "AND ColumnName <> N'CreatedDate' " & _
                "AND ColumnName <> N'NextCallDate' " & _
                "AND ColumnName <> N'StatusCode' " & _
                "AND ColumnName <> N'StatusDescription' " & _
                "AND ColumnName <> N'SubStatusCode' " & _
                "AND ColumnName <> N'SubStatusDescription' " & _
                "AND ColumnName <> N'OutsideCriteriaSubStatusCode' " & _
                "AND ColumnName <> N'OutsideCriteriaSubStatusDescription' " & _
                "AND ColumnName <> N'LockUserName' " & _
                "AND ColumnName <> N'SubStatusDescription' "
            End If
            strSQL += "ORDER BY ColumnName"

            Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    list.Items.Add(New ListItem("VW:" & Row.Item("ColumnName"), "VW:" & Row.Item("ColumnName")))
                    If (checkValue(val)) Then
                        If (val = Row.Item("DateType")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            End If
            dsCache = Nothing

            If (view <> "vwbusinessobjects" And view <> "vwbusinessobjectsdiarylist") Then
                strSQL = "SELECT ValName FROM tblimportvalidation WHERE ValTable = N'tbldatastore' AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValStatus = 'A' "
                If boolGroup = True Then
                    strSQL += "AND (ValType = N'N' OR ValType = N'M') "
                End If
                strSQL += "ORDER BY ValName"
                dsCache = New Caching(cache, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        list.Items.Add(New ListItem("DS:" & Row.Item("ValName"), "DS:" & Row.Item("ValName")))
                        If (checkValue(val)) Then
                            If (val = Row.Item("ValName")) Then
                                list.Items(list.Items.Count - 1).Selected = True
                            End If
                        End If
                    Next
                End If
                dsCache = Nothing
            Else
                strSQL = "SELECT ValName FROM tblimportvalidation WHERE ValTable = N'tblbusinessdatastore' AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValStatus = 'A' "
                If boolGroup = True Then
                    strSQL += "AND (ValType = N'N' OR ValType = N'M') "
                End If
                strSQL += "ORDER BY ValName"
                dsCache = New Caching(cache, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        list.Items.Add(New ListItem("DS:" & Row.Item("ValName"), "DS:" & Row.Item("ValName")))
                        If (checkValue(val)) Then
                            If (val = Row.Item("ValName")) Then
                                list.Items(list.Items.Count - 1).Selected = True
                            End If
                        End If
                    Next
                End If
                dsCache = Nothing
            End If

            strSQL = "SELECT DateType FROM tbldatetypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) AND DateTypeActive = 1 ORDER BY DateType"
            dsCache = New Caching(cache, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    list.Items.Add(New ListItem("SD:" & Row.Item("DateType"), "SD:" & Row.Item("DateType")))
                    If (checkValue(val)) Then
                        If (val = Row.Item("DateType")) Then
                            list.Items(list.Items.Count - 1).Selected = True
                        End If
                    End If
                Next
            End If
            dsCache = Nothing

        End If
    End Sub

    Shared Sub reportSortTypeDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderSortTypes'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportTotalDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderTotals'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportAccessLevelsListBox(ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderAccessLevels'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportFieldFormatsDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderFieldFormats'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportClickThroughDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL = "SELECT TOP(1) SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = 0 AND SystemConfigurationName = N'ReportBuilderClickThroughs'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        list.Items.Add(New ListItem(txt, ""))
        If (objResult IsNot Nothing) Then
            Dim arrSort As Array = Split(objResult.ToString, "|")
            For Each Item As String In arrSort
                list.Items.Add(New ListItem(Item, Item))
                If (val = Item) Then
                    list.Items(list.Items.Count - 1).Selected = True
                End If
            Next
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Sub

    Shared Sub reportTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT ReportBuilderTypeID, ReportBuilderTypeName FROM tblreportbuildertypes WHERE ReportBuilderTypeActive = 1 ORDER BY ReportBuilderTypeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ReportBuilderTypeName"), Row.Item("ReportBuilderTypeID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ReportBuilderTypeID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub repPostCodeRangeListBox(ByVal cache As Cache, ByVal list As ListBox, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT PostCodeRange FROM tblpostcodelookup ORDER BY PostCodePrefix"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Dim arrOptions As Array = Split(Row.Item("PostCodeRange").ToString, ",")
                For x As Integer = 0 To UBound(arrOptions)
                    If (checkValue(arrOptions(x))) Then
                        list.Items.Add(New ListItem(arrOptions(x), arrOptions(x)))
                        For y As Integer = 0 To UBound(arrVal)
                            If (arrVal(y) = arrOptions(x)) Then
                                list.Items(list.Items.Count - 1).Selected = True
                            End If
                        Next
                    End If
                Next
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub emailTemplateDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT EmailTemplateID, EmailTemplateName FROM tblemailtemplates WHERE EmailTemplateActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY EmailTemplateName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("EmailTemplateName"), Row.Item("EmailTemplateID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("EmailTemplateID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function transferStatusTextDropdown(ByVal cache As Cache, ByVal AppID As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignID, MediaCampaignName FROM vwdistributionclone " & _
                            "WHERE (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') AND CompanyID = '" & CompanyID & "' " & _
                            "ORDER BY MediaCampaignScheduleRequiredRate DESC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        strOptions += "<option value=""NEW"" selected=""selected"">New</option>" & vbCrLf
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("MediaCampaignID") & """"
                strOptions += ">Transfer: " & Row.Item("MediaCampaignName") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub diaryTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String)
        Dim strSQL As String = "SELECT DiaryTypeID, DiaryTypeName, DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY DiaryTypeName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("DiaryTypeName") & " - " & Row.Item("DiaryTypeDescription"), Row.Item("DiaryTypeID")))
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub reportBuilderDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT ReportBuilderID, ReportBuilderName FROM tblreportbuilder WHERE CompanyID = '" & CompanyID & "' ORDER BY ReportBuilderName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("ReportBuilderName"), Row.Item("ReportBuilderID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ReportBuilderID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub statisticTypeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT StatisticTypeID, StatisticTypeName FROM tblstatistictypes WHERE StatisticTypeActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("StatisticTypeName"), Row.Item("StatisticTypeID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("StatisticTypeID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub outOfOfficeDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT OutOfOfficeID, OutOfOfficeStatus FROM tbloutofoffice WHERE OutOfOfficeActive = 1"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("OutOfOfficeStatus"), Row.Item("OutOfOfficeID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("OutOfOfficeID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub directionDropdown(ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        list.Items.Add(New ListItem(txt, ""))
        list.Items.Add(New ListItem("Ascending", "ASC"))
        If (val = "ASC") Then list.Items(list.Items.Count - 1).Selected = True
        list.Items.Add(New ListItem("Descending", "DESC"))
        If (val = "DESC") Then list.Items(list.Items.Count - 1).Selected = True
    End Sub

    Shared Sub workflowFieldsDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT ColumnName FROM vwsysfields WHERE (TableName = N'vwworkflow') ORDER BY ColumnName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Regex.Replace(Row.Item("ColumnName"), "([A-Z0-9])", " $1"), Row.Item("ColumnName")))
                If (checkValue(val)) Then
                    If (val = Row.Item("ColumnName")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub teamDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT TeamID, TeamName FROM tblteams WHERE TeamActive = 1 AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("TeamName"), Row.Item("TeamID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("TeamID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Function teamTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT TeamID, TeamName FROM tblteams WHERE TeamActive = 1 AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("TeamID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("TeamID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("TeamName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Sub emailProfileDropdown(ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim strSQL As String = "SELECT EmailProfileID, EmailProfileFriendlyName FROM tblemailprofiles WHERE CompanyID = '" & CompanyID & "' AND EmailProfileActive = 1 ORDER BY EmailProfileFriendlyName"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        list.Items.Add(New ListItem(txt, ""))
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                list.Items.Add(New ListItem(Row.Item("EmailProfileFriendlyName"), Row.Item("EmailProfileID")))
                If (checkValue(val)) Then
                    If (val = Row.Item("EmailProfileID")) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            Next
        End If

        dsCache = Nothing
    End Sub

    Shared Sub userActiveLevelDropdown(objLeadPlatform As LeadPlatform, ByVal cache As Cache, ByVal list As DropDownList, ByVal txt As String, ByVal val As String)
        Dim arrUserLevels As String() = {objLeadPlatform.Config.CallCentre, objLeadPlatform.Config.Sales, objLeadPlatform.Config.Administrator}
        Dim arrUserLevelNames As String() = {"Call Centre", "Sales", "Administrator"}
        Dim arrUserLevelIDs As String() = {"2", "1", "3"}
        For a As Integer = 0 To UBound(arrUserLevels)
            If (arrUserLevels(a) = True) Then
                list.Items.Add(New ListItem(arrUserLevelNames(a), arrUserLevelIDs(a)))
                If (checkValue(val)) Then
                    If (val = arrUserLevelIDs(a)) Then
                        list.Items(list.Items.Count - 1).Selected = True
                    End If
                End If
            End If
        Next
    End Sub

    Shared Function userActiveLevelTextDropdown(objLeadPlatform As LeadPlatform, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrUserLevels As String() = {objLeadPlatform.Config.CallCentre, objLeadPlatform.Config.Sales, objLeadPlatform.Config.Administrator}
        Dim arrUserLevelNames As String() = {"Call Centre", "Sales", "Administrator"}
        Dim arrUserLevelIDs As String() = {"2", "1", "3"}
        For a As Integer = 0 To UBound(arrUserLevels)
            If (arrUserLevels(a) = True) Then
                strOptions += "<option value=""" & arrUserLevelIDs(a) & """"
                If (checkValue(val)) Then
                    If (val = arrUserLevelIDs(a)) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
                strOptions += ">" & arrUserLevelNames(a) & "</option>" & vbCrLf
            End If
        Next
        Return strOptions
    End Function

    'Calendar filter Event Type Dropdown -- LS 07/03/2014
    Shared Function eventTypeDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT CalendarEventTypeID, CalendarEventType FROM tblcalendareventtypes WHERE (CompanyID = '" & CompanyID & "' OR CompanyID = 0) ORDER BY CalendarEventType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("CalendarEventTypeID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("TeamID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("CalendarEventType") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    'Calendar filter to show teams assigned to logged in team leader
    Shared Function teamLeaderTeamTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT TeamID, TeamName FROM tblteams WHERE TeamActive = 1 AND (TeamLeaderUserID LIKE N'%," & Config.DefaultUserID & ",%') AND CompanyID = '" & CompanyID & "'"

        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("TeamID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("TeamID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("TeamName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    'calendar filter users within team leaders teams
    Shared Function teamLeadersTeamUsersTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT dbo.tblusers.UserID, dbo.tblusers.UserFullName FROM dbo.tblusers INNER JOIN dbo.tblteams ON dbo.tblusers.UserTeamID = dbo.tblteams.TeamID WHERE (TeamLeaderUserID LIKE N'%," & Config.DefaultUserID & ",%') AND (dbo.tblusers.UserActive = 1) AND (NOT (dbo.tblusers.UserFullName LIKE N'%wallboard%')) AND (NOT (dbo.tblusers.UserFullName LIKE N'%wall board%')) AND (dbo.tblusers.UserSuperAdmin = 0) AND (dbo.tblusers.UserLockedOut = 0) AND (dbo.tblusers.CompanyID = '" & CompanyID & "')"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("UserID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("UserID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("UserFullName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectProductTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT BusinessObjectProductTypeID, BusinessObjectProductType FROM tblbusinessobjectproducttypes WHERE BusinessObjectProductTypeActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY BusinessObjectProductType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectProductTypeID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("BusinessObjectProductTypeID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("BusinessObjectProductType") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    Shared Function businessObjectTypeTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT BusinessObjectTypeID, BusinessObjectType FROM tblbusinessobjecttypes WHERE CompanyID = '" & CompanyID & "' ORDER BY BusinessObjectType"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectTypeID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("BusinessObjectTypeID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("BusinessObjectType") & "</option>" & vbCrLf
            Next
        End If

        dsCache = Nothing
        Return strOptions
    End Function

    'pick a template when adding a wiki page
    Shared Function wikiTemplateTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT WikiTemplateID, WikiTemplateName FROM tblWikiTemplates"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("WikiTemplateID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("WikiTemplateID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("WikiTemplateName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    'pick a template when adding a wiki page
    Shared Function wikiSectionTextDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT WikiSectionID, WikiSectionName FROM tblWikiSections"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("WikiSectionID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("WikiSectionID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("WikiSectionName") & "</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

    'pick an image when adding a wiki page
    Shared Function wikiPageImageTextDropdown(ByVal cache As Cache, ByVal objLeadPlatform As LeadPlatform, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        Dim strDir As String = HttpContext.Current.Server.MapPath("/img/")
        Dim strCompanyDir As String = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))
        For Each strFile As String In Directory.GetFiles(strDir & "users\" & strCompanyDir)
            Dim objFile As FileInfo = New FileInfo(strFile)
            strOptions += "<option value=""users/" & strCompanyDir & "/medium/" & objFile.Name & """"
            For x As Integer = 0 To UBound(arrVal)
                If (checkValue(arrVal(x))) Then
                    If (arrVal(x) = "users/" & strCompanyDir & "/medium/" & objFile.Name) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
            Next
            strOptions += ">" & objFile.Name & "</option>" & vbCrLf
        Next
        For Each strFile As String In Directory.GetFiles(strDir & "upload\" & strCompanyDir & "\medium")
            Dim objFile As FileInfo = New FileInfo(strFile)
            strOptions += "<option value=""upload/" & strCompanyDir & "/medium/" & objFile.Name & """"
            For x As Integer = 0 To UBound(arrVal)
                If (checkValue(arrVal(x))) Then
                    If (arrVal(x) = "upload/" & strCompanyDir & "/medium/" & objFile.Name) Then
                        strOptions += " selected=""selected"""
                    End If
                End If
            Next
            strOptions += ">" & objFile.Name & "</option>" & vbCrLf

        Next
        Return strOptions
    End Function

    'pick a broker when adding a case
    Shared Function brokerDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT APP.BusinessObjectID, BusinessObjectBusinessName, BusinessObjectContactFirstName, BusinessObjectContactSurname, BusinessObjectClientAddressPostCode FROM tblbusinessobjects APP INNER JOIN tblbusinessobjectstatus APS ON APS.BusinessObjectID = APP.BusinessObjectID WHERE BusinessObjectTypeID = 5 AND APP.CompanyID = '" & CompanyID & "' AND APS.BusinessObjectActive = 1 AND APS.BusinessObjectStatusCode in ('ACT','KEY','SPD') ORDER BY BusinessObjectBusinessName ASC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("BusinessObjectID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("BusinessObjectBusinessName") & " (" & Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname") & " - " & Row.Item("BusinessObjectClientAddressPostCode").ToString & ")</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function
	
	 'pick a broker when adding a case
    Shared Function contactDropdown(ByVal cache As Cache, ByVal txt As String, ByVal val As String) As String
        Dim strOptions As String = ""
        Dim strSQL As String = "SELECT APP.BusinessObjectID, APP.BusinessObjectContactFirstName, APP.BusinessObjectContactSurname, PAR.BusinessObjectBusinessName FROM tblbusinessobjects APP INNER JOIN tblbusinessobjectstatus APS ON APS.BusinessObjectID = APP.BusinessObjectParentClientID INNER JOIN tblbusinessobjects PAR ON PAR.BusinessObjectID = APP.BusinessObjectParentClientID WHERE  APP.BusinessObjectTypeID in(3,11) AND APP.CompanyID = '1430' AND APP.BusinessObjectParentClientID <> '' AND APS.businessobjectstatuscode in ('KEY','ACT') ORDER BY BusinessObjectBusinessName ASC"
        Dim dsCache As DataTable = New Caching(cache, strSQL, "", "", "").returnCache()
        Dim arrVal As Array = Split(val, ",")
        If (checkValue(txt)) Then
            strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        End If
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectID") & """"
                For x As Integer = 0 To UBound(arrVal)
                    If (checkValue(arrVal(x))) Then
                        If (arrVal(x) = Row.Item("BusinessObjectID")) Then
                            strOptions += " selected=""selected"""
                        End If
                    End If
                Next
                strOptions += ">" & Row.Item("BusinessObjectContactFirstName") & " " & Row.Item("BusinessObjectContactSurname") & " (" & Row.Item("BusinessObjectBusinessName") & ")</option>" & vbCrLf
            Next
        End If
        dsCache = Nothing
        Return strOptions
    End Function

End Class
