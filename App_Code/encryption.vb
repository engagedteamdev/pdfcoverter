﻿Imports Config
Imports Common
Imports Microsoft.VisualBasic

Public Class Encryption

    Shared Function encrypt(ByVal val As String) As String
        ' Encrypts/decrypts the passed string using 
        ' a simple ASCII value-swapping algorithm
        Dim strTempChar As String = ""
        For i As Integer = 1 To Len(val)
            If Asc(Mid(val, i, 1)) < 128 Then
                strTempChar = CType(Asc(Mid(val, i, 1)) + 128, String)
            ElseIf Asc(Mid(val, i, 1)) > 128 Then
                strTempChar = CType(Asc(Mid(val, i, 1)) - 128, String)
            End If
            Mid(val, i, 1) = Chr(CType(strTempChar, Integer))
        Next
        Return val
    End Function

    Shared Function encryptCypher(ByVal val As String) As String
        Dim strEncryptionKey As String = getAnyField("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "EncryptionKey")
        Dim strChar As String = "", intKeyChar As Integer = 0, intStringChar As Integer = 0, intCryptChar As String = "", strEncrypted As String = ""
        For i As Integer = 1 To Len(val)
            intKeyChar = Asc(Mid(strEncryptionKey, i, 1))
            intStringChar = Asc(Mid(val, i, 1))
            ' *** uncomment below to encrypt with addition,
            ' iCryptChar = iStringChar + iKeyChar
            intCryptChar = intKeyChar Xor intStringChar
            strEncrypted = strEncrypted & Chr(intCryptChar)
        Next
        Return strEncrypted
    End Function

    Shared Function decryptCypher(ByVal val As String) As String
        Dim strEncryptionKey As String = getAnyField("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "EncryptionKey")
        Dim strChar As String = "", intKeyChar As Integer = 0, intStringChar As Integer = 0, intDecryptChar As String = "", strDecrypted As String = ""
        For i As Integer = 1 To Len(val)
            intKeyChar = (Asc(Mid(strEncryptionKey, i, 1)))
            intStringChar = Asc(Mid(val, i, 1))
            ' *** uncomment below to decrypt with subtraction	
            ' iDeCryptChar = iStringChar - iKeyChar 
            intDecryptChar = intKeyChar Xor intStringChar
            strDecrypted += Chr(intDecryptChar)
        Next
        Return strDecrypted
    End Function

End Class
