﻿Imports Config, Common, CommonSave, Encryption
Imports Microsoft.VisualBasic
Imports System.Threading.Thread
Imports System.Net

' ** Revision history **
'
' ** End Revision History **

Public Class AdminSave

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As System.Web.Caching.Cache
    Private AppID As String = ""
    Private strThisPg As String = HttpContext.Current.Request("strThisPg")
    Private strFrmAction As String = HttpContext.Current.Request("strFrmAction")
    Private strNoRedirect As String = HttpContext.Current.Request("strNoRedirect")
    Private strReturnUrl As String = HttpContext.Current.Request("strReturnUrl")
    Private strReturnPg As String = ""
    Private strReturnUrlMessage As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As System.Web.Caching.Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As System.Web.Caching.Cache)
            objCache = value
        End Set
    End Property

    Public Sub save()
        Select Case strThisPg
            Case "caseadmin"
                saveCaseAdmin()
            Case "appmanagement"
                Select Case strFrmAction
                    Case "add", "update"
                        saveAppManagement()
                    Case "active"
                        saveAppManagementActive()
                    Case "delete"
                        saveAppManagementDelete()
                End Select
            Case "media"
                Select Case strFrmAction
                    Case "add", "update"
                        saveMedia()
                    Case "delete"
                        saveMediaDelete()
                    Case "active"
                        saveMediaActive()
                End Select
            Case "mediacampaign"
                Select Case strFrmAction
                    Case "add", "update"
                        saveMediaCampaign()
                    Case "delete"
                        saveMediaCampaignDelete()
                    Case "active"
                        saveMediaCampaignActive()
                    Case "cost"
                        saveMediaCampaignCost()
                End Select
            Case "mediacampaignschedule"
                Select Case strFrmAction
                    Case "add", "update"
                        saveMediaCampaignSchedule()
                    Case "paused"
                        saveMediaCampaignSchedulePaused()
                End Select
            Case "applicationtemplate"
                Select Case strFrmAction
                    Case "add", "update"
                        saveApplicationTemplate()
                    Case "delete"
                        saveApplicationTemplateDelete()
                    Case "active"
                        saveApplicationTemplateActive()
                End Select
            Case "applicationpage"
                Select Case strFrmAction
                    Case "add", "update"
                        saveApplicationPage()
                    Case "delete"
                        saveApplicationPageDelete()
                    Case "active"
                        saveApplicationPageActive()
                End Select
            Case "processingscreen"
                Select Case strFrmAction
                    Case "add", "update"
                        saveProcessingScreen()
                    Case "delete"
                        saveProcessingScreenDelete()
                    Case "active"
                        saveProcessingScreenActive()
                End Select
            Case "script"
                Select Case strFrmAction
                    Case "add", "update"
                        saveScript()
                    Case "delete"
                        saveScriptDelete()
                    Case "active"
                        saveScriptActive()
                End Select
            Case "field"
                Select Case strFrmAction
                    Case "add", "update"
                        saveField()
                    Case "delete"
                        saveFieldDelete()
                    Case "active"
                        saveFieldActive()
                End Select
            Case "statistic"
                Select Case strFrmAction
                    Case "add", "update"
                        saveStatistic()
                    Case "delete"
                        saveStatisticDelete()
                    Case "active"
                        saveStatisticActive()
                End Select
            Case "dashboard"
                Select Case strFrmAction
                    Case "add", "update"
                        saveDashboard()
                    Case "delete"
                        saveDashboardDelete()
                    Case "active"
                        saveDashboardActive()
                End Select
            Case "dashboardpanel"
                Select Case strFrmAction
                    Case "add", "update"
                        saveDashboardPanel()
                    Case "delete"
                        saveDashboardPanelDelete()
                    Case "active"
                        saveDashboardPanelActive()
                End Select
            Case "validation"
                Select Case strFrmAction
                    Case "add", "update"
                        saveValidation()
                    Case "delete"
                        saveValidationDelete()
                    Case "active"
                        saveValidationActive()
                End Select
            Case "status"
                Select Case strFrmAction
                    Case "add", "update"
                        saveStatus()
                    Case "delete"
                        saveStatusDelete()
                    Case "active"
                        saveStatusActive()
                End Select
            Case "substatus"
                Select Case strFrmAction
                    Case "add", "update"
                        saveSubStatus()
                    Case "delete"
                        saveSubStatusDelete()
                    Case "active"
                        saveSubStatusActive()
                End Select
            Case "lender"
                Select Case strFrmAction
                    Case "add", "update"
                        saveLender()
                    Case "delete"
                        saveLenderDelete()
                    Case "active"
                        saveLenderActive()
                End Select
            Case "statusmapping"
                Select Case strFrmAction
                    Case "update"
                        saveStatusMapping()
                End Select
            Case "substatusgroup"
                Select Case strFrmAction
                    Case "add", "update"
                        saveSubStatusGroup()
                    Case "delete"
                        saveSubStatusGroupDelete()
                    Case "active"
                        saveSubStatusGroupActive()
                End Select
            Case "user"
                Select Case strFrmAction
                    Case "add", "update"
                        saveUser()
                    Case "delete"
                        saveUserDelete()
                    Case "active"
                        saveUserActive()
                    Case "passwordchange"
                        saveUserPasswordChange()
                    Case "logout"
                        saveUserLogout()
                    Case "lockout"
                        saveUserLockout()
                    Case "store"
                        saveUserStore()
                End Select
            Case "applicationflow"
                saveApplicationFlow()
            Case "workflow"
                Select Case strFrmAction
                    Case "add", "update"
                        saveWorkflow()
                    Case "delete"
                        saveWorkflowDelete()
                    Case "active"
                        saveWorkflowActive()
                End Select
            Case "subworkflow"
                Select Case strFrmAction
                    Case "add", "update"
                        saveSubWorkflow()
                    Case "delete"
                        saveSubWorkflowDelete()
                    Case "active"
                        saveSubWorkflowActive()
                    Case "selected"
                        saveSubWorkflowSelected()
                    Case "deselected"
                        saveSubWorkflowDeselected()
                End Select
            Case "assignworkflowsbyuser"
                Select Case strFrmAction
                    Case "update"
                        saveWorkflowUser()
                    Case "copy"
                        saveWorkflowUserCopy()
                End Select
            Case "assigndashboards"
                Select Case strFrmAction
                    Case "update"
                        saveDashboards()
                    Case "copy"
                        saveDashboardsCopy()
                End Select
            Case "useraccess"
                Select Case strFrmAction
                    Case "update"
                        saveUserAccess()
                    Case "copy"
                        saveUserAccessCopy()
                End Select
            Case "recyclestrategy"
                Select Case strFrmAction
                    Case "add", "update"
                        saveRecycleStrategy()
                    Case "delete"
                        saveRecycleStrategyDelete()
                    Case "active"
                        saveRecycleStrategyActive()
                End Select
            Case "recyclerule"
                Select Case strFrmAction
                    Case "add", "update"
                        saveRecycleRule()
                    Case "delete"
                        saveRecycleRuleDelete()
                    Case "active"
                        saveRecycleRuleActive()
                End Select
            Case "dialer"
                Select Case strFrmAction
                    Case "load"
                        saveLoadedToDialerDate()
                End Select
            Case "caselock"
                deleteCaseLock()
            Case "communicationtemplate"
                Select Case strFrmAction
                    Case "add", "update"
                        saveCommunicationTemplate()
                    Case "delete"
                        saveCommunicationTemplateDelete()
                    Case "active"
                        saveCommunicationTemplateActive()
                End Select
            Case "emailtemplate"
                Select Case strFrmAction
                    Case "add", "update"
                        saveEmailTemplate()
                    Case "delete"
                        saveEmailTemplateDelete()
                    Case "active"
                        saveEmailTemplateActive()
                End Select
            Case "duplicatecase"
                duplicateCase()
            Case "cache"
                Select Case strFrmAction
                    Case "delete"
                        saveCacheDelete()
                    Case "remove"
                        saveCacheRemove()
                End Select
            Case "pdf"
                Select Case strFrmAction
                    Case "add", "update"
                        savePDF()
                    Case "delete"
                        savePDFDelete()
                    Case "active"
                        savePDFActive()
                End Select
            Case "lettertemplate"
                Select Case strFrmAction
                    Case "add", "update"
                        saveLetterTemplate()
                    Case "delete"
                        saveLetterTemplateDelete()
                    Case "active"
                        saveLetterTemplateActive()
                End Select
            Case "letterparagraph"
                Select Case strFrmAction
                    Case "add", "update"
                        saveLetterParagraph()
                    Case "delete"
                        saveLetterParagraphDelete()
                    Case "active"
                        saveLetterParagraphActive()
                End Select
            Case "reportbuilder"
                Select Case strFrmAction
                    Case "add", "update"
                        saveReportBuilder()
                    Case "delete"
                        saveReportBuilderDelete()
                    Case "active"
                        saveReportBuilderActive()
                End Select
            Case "dataimportschedule"
                Select Case strFrmAction
                    Case "add", "update"
                        saveImportSchedule()
                    Case "delete"
                        saveImportScheduleDelete()
                End Select
            Case "dataimport"
                Select Case strFrmAction
                    Case "run"
                        runImport()
                    Case "delete"
                        saveImportDelete()
                End Select
            Case "workflowrota"
                Select Case strFrmAction
                    Case "add", "update"
                        saveWorkflowRota()
                    Case "copy"
                        saveWorkflowRotaCopy()
                    Case "order"
                        saveWorkflowRotaOrder()
                    Case "active"
                        saveWorkflowRotaActive()
                End Select
			Case "appointmentdate"
                Select Case strFrmAction
                    Case "add", "update"
                        addAppointment()
                End Select
            Case Else
                strReturnPg = "/"
        End Select

        If (checkValue(strReturnUrl)) Then strReturnPg = strReturnUrl
        If (checkValue(strReturnUrlMessage)) Then
            If (InStr(strReturnPg, "?")) Then
                strReturnPg = strReturnPg & "&strMessage=" & strReturnUrlMessage
            Else
                strReturnPg = strReturnPg & "?strMessage=" & strReturnUrlMessage
            End If
        End If

        If (strNoRedirect <> "Y") Then
            HttpContext.Current.Response.Redirect(strReturnPg)
        Else
            HttpContext.Current.Response.Write(strReturnUrlMessage)
        End If

    End Sub

    Private Sub saveCaseAdmin()
        Dim AppIDs As String = HttpContext.Current.Request("txtAppIDs")
        If (Not checkValue(AppIDs)) Then
            AppIDs = HttpContext.Current.Request("AppID")
        End If
        Dim arrAppIDs As Array = Split(AppIDs, ",")
        Dim arrDateTypes As Array = Split(getDateTypes(), ",")
        Dim strQry As String = "", strSep As String = ""
        If (checkValue(AppIDs)) Then
            For Each Item As String In arrAppIDs
                strSep = " "
                If (checkValue(HttpContext.Current.Request("ddMediaCampaignIDInbound")) Or checkValue(HttpContext.Current.Request("ddMediaCampaignIDOutbound")) Or checkValue(HttpContext.Current.Request("cbClearMediaCampaign")) Or checkValue(HttpContext.Current.Request("txtClientReferenceOutbound")) Or checkValue(HttpContext.Current.Request("cbClearClientReference")) Or checkValue(HttpContext.Current.Request("txtMediaCampaignCostInbound")) Or checkValue(HttpContext.Current.Request("cbClearCost"))) Then
                    strQry = "UPDATE tblapplications SET"
                    If (checkValue(HttpContext.Current.Request("ddMediaCampaignIDInbound"))) Then
                        strQry += strSep & "MediaCampaignIDInbound = " & formatField(HttpContext.Current.Request("ddMediaCampaignIDInbound"), "N", 0)
                        strSep = ", "
                    End If
                    If (checkValue(HttpContext.Current.Request("ddMediaCampaignIDOutbound"))) Then
                        strQry += strSep & "MediaCampaignIDOutbound = " & formatField(HttpContext.Current.Request("ddMediaCampaignIDOutbound"), "N", 0)
                        strSep = ", "
                    Else
                        If (HttpContext.Current.Request("cbClearMediaCampaign") = "on") Then
                            strQry += strSep & "MediaCampaignIDOutbound = 0"
                            strSep = ", "
                        End If
                    End If
                    If (checkValue(HttpContext.Current.Request("txtClientReferenceOutbound"))) Then
                        strQry += strSep & "ClientReferenceOutbound = " & formatField(HttpContext.Current.Request("txtClientReferenceOutbound"), "", "NULL")
                        strSep = ", "
                    Else
                        If (HttpContext.Current.Request("cbClearClientReference") = "on") Then
                            strQry += strSep & "ClientReferenceOutbound = NULL"
                            strSep = ", "
                        End If
                    End If
                    If (checkValue(HttpContext.Current.Request("txtMediaCampaignCostInbound"))) Then
                        strQry += strSep & "MediaCampaignCostInbound = " & formatField(HttpContext.Current.Request("txtMediaCampaignCostInbound"), "N", 0)
                    Else
                        If (HttpContext.Current.Request("cbClearCost") = "on") Then
                            strQry += strSep & "MediaCampaignCostInbound = 0"
                        End If
                    End If
                    strQry += " WHERE AppID = '" & Item & "'"
                    executeNonQuery(strQry)
                End If
                strSep = " "
                strQry = "UPDATE tblapplicationstatus SET"
                If (checkValue(HttpContext.Current.Request("ddStatusCode"))) Then
                    strQry += strSep & "StatusCode = " & formatField(HttpContext.Current.Request("ddStatusCode"), "U", "")
                    strSep = ", "
                End If
                If (checkValue(HttpContext.Current.Request("ddSubStatusCode"))) Then
                    strQry += strSep & "SubStatusCode = " & formatField(HttpContext.Current.Request("ddSubStatusCode"), "U", "")
                    strSep = ", "
                Else
                    If (HttpContext.Current.Request("cbClearSubStatusCode") = "on") Then
                        strQry += strSep & "SubStatusCode = NULL"
                        strSep = ", "
                    End If
                End If
                For Each DateType As String In arrDateTypes
                    If (checkValue(HttpContext.Current.Request("txt" & DateType))) Then
                        strQry += strSep & DateType & " = " & formatField(HttpContext.Current.Request("txt" & DateType), "DTTM", "NULL")
                        strSep = ", "
                    Else
                        If (HttpContext.Current.Request("cbClear" & DateType) = "1") Then
                            strQry += strSep & DateType & " = NULL"
                            strSep = ", "
                        End If
                    End If
                Next
                strQry += strSep & "Active = " & formatField(HttpContext.Current.Request("cbActive"), "B", 0) & " " & _
                    "WHERE AppID = '" & Item & "' AND CompanyID = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Next
        End If
        strReturnUrlMessage = encodeURL("Application(s) successfully updated")
    End Sub

    Private Sub saveAppManagement()
        Dim AppManagementID As String = HttpContext.Current.Request("AppManagementID")
        Dim AppManagementName As String = HttpContext.Current.Request("txtAppManagementName")
        Dim strAppManagementDays As String = getDayNumbers()
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(AppManagementID)) Then
            strQry = "UPDATE tblapplicationmanagement SET  " & _
               "AppManagementName		                    = " & formatField(AppManagementName, "T", "") & ", " & _
               "AppManagementRemarketing                    = " & formatField(HttpContext.Current.Request("AppManagementRemarketing"), "B", 0) & ", " & _
               "AppManagementRemarketingMediaCampaignID     = " & formatField(HttpContext.Current.Request("ddAppManagementRemarketingMediaCampaignID"), "N", 0) & ", " & _
               "AppManagementRemarketingProductType         = " & formatField(HttpContext.Current.Request("ddAppManagementRemarketingProductType"), "", "") & ", " & _
			   "AppManagementRemarketingStatusCode         	= " & formatField(HttpContext.Current.Request("ddAppManagementRemarketingStatusCode"), "U", "") & ", " & _
			   "AppManagementRemarketingSubStatusCode       = " & formatField(HttpContext.Current.Request("ddAppManagementRemarketingSubStatusCode"), "U", "") & ", " & _
			   "AppManagementRemarketingMaintainSubStatus   = " & formatField(HttpContext.Current.Request("cbAppManagementRemarketingMaintainSubStatus"), "B", 0) & ", " & _
               "AppManagementStatusCode                     = " & formatField(HttpContext.Current.Request("ddAppManagementStatusCode"), "U", "") & ", " & _
               "AppManagementSubStatusCode                  = " & formatField(HttpContext.Current.Request("ddAppManagementSubStatusCode"), "U", "") & ", " & _
               "AppManagementDateType                       = " & formatField(HttpContext.Current.Request("lstAppManagementDateType"), "", "") & ", " & _
               "AppManagementNote                      	 	= " & formatField(HttpContext.Current.Request("txtAppManagementNote"), "", "") & ", " & _
               "AppManagementMediaCampaignCostInboundReset  = " & formatField(HttpContext.Current.Request("cbAppManagementMediaCampaignCostInboundReset"), "B", 0) & ", " & _
               "AppManagementCSVType                        = " & formatField(HttpContext.Current.Request("ddAppManagementCSVType"), "U", "") & ", " & _
               "AppManagementEmailToAddress                 = " & formatField(HttpContext.Current.Request("txtAppManagementEmailToAddress"), "", "") & ", " & _
               "AppManagementEmailSubject                   = " & formatField(HttpContext.Current.Request("txtAppManagementEmailSubject"), "", "") & ", " & _
               "AppManagementWorkflowID                     = " & formatField(HttpContext.Current.Request("AppManagementWorkflowID"), "N", 0) & ", " & _
               "AppManagementLetterID                 		= " & formatField(HttpContext.Current.Request("ddAppManagementLetterID"), "N", 0) & ", " & _
               "AppManagementDays                           = " & formatField("," & strAppManagementDays & ",", "", "") & ", " & _
               "AppManagementHours                          = " & formatField("," & HttpContext.Current.Request("lstAppManagementHours") & ",", "", "") & ", " & _
               "AppManagementMinutes                        = " & formatField("," & HttpContext.Current.Request("lstAppManagementMinutes") & ",", "", "") & ", " & _
               "AppManagementRunOnce                        = " & formatField(HttpContext.Current.Request("cbAppManagementRunOnce"), "B", 0) & " " & _
               "WHERE AppManagementID	                    = '" & AppManagementID & "' " & _
               "AND CompanyID                               = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(AppManagementName)) Then
                strQry = _
                "INSERT INTO tblapplicationmanagement (CompanyID, AppManagementName, AppManagementRemarketing, AppManagementRemarketingMediaCampaignID, AppManagementRemarketingProductType, AppManagementRemarketingStatusCode, AppManagementRemarketingSubStatusCode, AppManagementRemarketingMaintainSubStatus, AppManagementStatusCode, AppManagementSubStatusCode, AppManagementDateType, AppManagementNote, AppManagementMediaCampaignCostInboundReset, AppManagementCSVType, " & _
                "AppManagementEmailToAddress, AppManagementEmailSubject, AppManagementWorkflowID, AppManagementLetterID, AppManagementDays, AppManagementHours, AppManagementMinutes, AppManagementRunOnce) " & _
                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                formatField(AppManagementName, "T", "") & ", " & _
                formatField(HttpContext.Current.Request("AppManagementRemarketing"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementRemarketingMediaCampaignID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementRemarketingProductType"), "", "") & ", " & _
				formatField(HttpContext.Current.Request("ddAppManagementRemarketingStatusCode"), "U", "") & ", " & _
				formatField(HttpContext.Current.Request("ddAppManagementRemarketingSubStatusCode"), "U", "") & ", " & _
				formatField(HttpContext.Current.Request("cbAppManagementRemarketingMaintainSubStatus"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementStatusCode"), "U", "") & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementSubStatusCode"), "U", "") & ", " & _
                formatField(HttpContext.Current.Request("lstAppManagementDateType"), "", "") & ", " & _
    			formatField(HttpContext.Current.Request("txtAppManagementNote"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("cbAppManagementMediaCampaignCostInboundReset"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementCSVType"), "U", "") & ", " & _
                formatField(HttpContext.Current.Request("txtAppManagementEmailToAddress"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("txtAppManagementEmailSubject"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("AppManagementWorkflowID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddAppManagementLetterID"), "N", 0) & ", " & _
                formatField("," & strAppManagementDays & ",", "", "") & ", " & _
                formatField("," & HttpContext.Current.Request("lstAppManagementHours") & ",", "", "") & ", " & _
                formatField("," & HttpContext.Current.Request("lstAppManagementMinutes") & ",", "", "") & ", " & _
                formatField(HttpContext.Current.Request("cbAppManagementRunOnce"), "B", 0) & ")"
                executeNonQuery(strQry)
            End If
        End If
    End Sub

    Private Sub saveAppManagementDelete()
        Dim AppManagementID As String = HttpContext.Current.Request("AppManagementID")
        Dim strQry As String = ""
        If (checkValue(AppManagementID)) Then
            strQry = "DELETE FROM tblapplicationmanagement WHERE AppManagementID = '" & AppManagementID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveAppManagementActive()
        Dim AppManagementID As String = HttpContext.Current.Request("AppManagementID")
        Dim AppManagementActive As Boolean = HttpContext.Current.Request("AppManagementActive")
        Dim strQry As String = ""
        If (checkValue(AppManagementID)) Then
            If (AppManagementActive) Then
                strQry = "UPDATE tblapplicationmanagement SET  " & _
                    "AppManagementActive		= 0 " & _
                    "WHERE AppManagementID      = '" & AppManagementID & "' " & _
                    "AND CompanyID              = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblapplicationmanagement SET  " & _
                    "AppManagementActive		= 1 " & _
                    "WHERE AppManagementID	    = '" & AppManagementID & "' " & _
                    "AND CompanyID              = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveMedia()
        Dim MediaID As String = HttpContext.Current.Request("MediaID")
        Dim MediaName As String = HttpContext.Current.Request("txtMediaName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(MediaID)) Then
            strQry = "UPDATE tblmedia SET  " & _
                        "MediaName		            = " & formatField(MediaName, "T", "") & " " & _
                        "WHERE MediaID	            = '" & MediaID & "' " & _
                        "AND CompanyID              = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(MediaName)) Then
                strQry = "INSERT INTO tblmedia (CompanyID, MediaName) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(MediaName, "T", "") & ") "
                MediaID = executeIdentityQuery(strQry)

                If (HttpContext.Current.Request("cbUserSupplier") = "on") Then
                    strQry = "INSERT INTO tblmediacampaigns (MediaID, CompanyID, MediaCampaignOutbound, MediaCampaignName) " & _
                        "VALUES(" & formatField(MediaID, "N", 0) & ", " & _
                        formatField(CompanyID, "N", 0) & ", " & _
                        formatField(0, "B", 0) & ", " & _
                        formatField(MediaName, "T", "") & ") "
                    executeNonQuery(strQry)

                    Dim objUserNameResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/userlookup.aspx?UserSessionID=" & Config.UserSessionID & "&intMode=2&strUserFullName=" & MediaName)
                    Dim objUserNameReader As New StreamReader(objUserNameResponse.GetResponseStream())
                    Dim strUserName As String = objUserNameReader.ReadToEnd
                    Dim objUserRefResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/userlookup.aspx?UserSessionID=" & Config.UserSessionID & "&intMode=1&strUserFullName=" & MediaName)
                    Dim objUserRefReader As New StreamReader(objUserRefResponse.GetResponseStream())
                    Dim strUserRef As String = objUserRefReader.ReadToEnd
                    Dim strUserPassword As String = generatePassword(8)
                    strReturnUrlMessage = encodeURL("The username " & strUserName & " and password " & strUserPassword & " have been generated")

                    strQry = "INSERT INTO tblusers (UserReference, UserName, UserPassword, UserFullName, UserSupplierCompanyID, CompanyID, UserReports, UserRemoteAccess) " & _
                       "VALUES(" & formatField(strUserRef, "U", "") & ", " & _
                       formatField(strUserName, "", "") & ", " & _
                       formatField(hashString256(LCase(strUserName), strUserPassword), "", "") & ", " & _
                       formatField(MediaName, "T", "") & ", " & _
                       formatField(MediaID, "N", 0) & ", " & _
                       formatField(CompanyID, "N", 0) & ", " & _
                       formatField(1, "B", 0) & ", " & _
                       formatField(1, "B", 0) & ") "
                    executeNonQuery(strQry)

                    cacheDependency(CacheObject, "tblmediacampaigns")
                    cacheDependency(CacheObject, "tblusers")

                End If
            End If
        End If
        cacheDependency(CacheObject, "tblmedia")
    End Sub

    Private Sub saveMediaDelete()
        Dim MediaID As String = HttpContext.Current.Request("MediaID")
        Dim strQry As String = ""
        If (checkValue(MediaID)) Then
            strQry = "DELETE FROM tblmedia WHERE MediaID = '" & MediaID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblmedia")
        End If
    End Sub

    Private Sub saveMediaActive()
        Dim MediaID As String = HttpContext.Current.Request("MediaID")
        Dim MediaActive As Boolean = HttpContext.Current.Request("MediaActive")
        Dim strQry As String = ""
        If (checkValue(MediaID)) Then
            If (MediaActive) Then
                strQry = "UPDATE tblmedia SET  " & _
                   "MediaActive		= 0 " & _
                   "WHERE MediaID	= '" & MediaID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblmedia SET  " & _
                   "MediaActive     = 1 " & _
                   "WHERE MediaID	= '" & MediaID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblmedia")
        End If
    End Sub

    Private Sub saveMediaCampaign()
        Dim MediaID As String = HttpContext.Current.Request("MediaID")
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim MediaCampaignName As String = HttpContext.Current.Request("txtMediaCampaignName")
        Dim MediaCampaignStatusCodes As String = HttpContext.Current.Request("lstMediaCampaignStatusCodes")
        If (Not checkValue(MediaCampaignStatusCodes)) Then
            MediaCampaignStatusCodes = "NEW"
        End If
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(MediaCampaignID)) Then
            strQry = "UPDATE tblmediacampaigns SET  " & _
            "MediaCampaignOutbound                          = " & formatField(HttpContext.Current.Request("cbMediaCampaignOutbound"), "B", 0) & ", " & _
            "MediaCampaignName		                        = " & formatField(MediaCampaignName, "T", "") & ", " & _
            "MediaCampaignFriendlyNameInbound	            = " & formatField(HttpContext.Current.Request("txtMediaCampaignFriendlyNameInbound"), "T", "") & ", " & _
            "MediaCampaignFriendlyNameOutbound	            = " & formatField(HttpContext.Current.Request("txtMediaCampaignFriendlyNameOutbound"), "T", "") & ", " & _
            "MediaCampaignPurchaseMediaID                   = " & formatField(HttpContext.Current.Request("ddMediaCampaignPurchaseMediaID"), "N", 0) & ", " & _
            "MediaCampaignApplicationTemplateID             = " & formatField(HttpContext.Current.Request("ddMediaCampaignApplicationTemplateID"), "N", 0) & ", " & _
            "MediaCampaignCloneableTo                       = " & formatField(HttpContext.Current.Request("cbMediaCampaignCloneableTo"), "B", 0) & ", " & _
            "MediaCampaignAutomaticTransfer                 = " & formatField(HttpContext.Current.Request("cbMediaCampaignAutomaticTransfer"), "B", 0) & ", " & _
            "MediaCampaignLetterID                          = " & formatField(HttpContext.Current.Request("MCLetters"), "", "") & ", " & _
            "MediaCampaignDuplicateChecker                  = " & formatField(HttpContext.Current.Request("cbMediaCampaignDuplicateChecker"), "B", 0) & ", " & _
            "MediaCampaignDuplicateCheckerDays              = " & formatField(HttpContext.Current.Request("txtMediaCampaignDuplicateCheckerDays"), "N", 7) & ", " & _
            "MediaCampaignReturnsPolicy                     = " & formatField(HttpContext.Current.Request("cbMediaCampaignReturnsPolicy"), "B", 0) & ", " & _
            "MediaCampaignAutoReturn                        = " & formatField(HttpContext.Current.Request("cbMediaCampaignAutoReturn"), "B", 0) & ", " & _
            "MediaCampaignOutsideCriteriaDays               = " & formatField(HttpContext.Current.Request("txtMediaCampaignOutsideCriteriaDays"), "N", 5) & ", " & _
            "MediaCampaignAutoOutsideCriteriaSubStatus      = " & formatField(HttpContext.Current.Request("ddMediaCampaignAutoOutsideCriteriaSubStatus"), "U", "NULL") & ", " & _
            "MediaCampaignDataExpiryDays                    = " & formatField(HttpContext.Current.Request("txtMediaCampaignDataExpiryDays"), "N", "NULL") & ", " & _
            "MediaCampaignNoRemarkets                       = " & formatField(HttpContext.Current.Request("txtMediaCampaignNoRemarkets"), "N", "NULL") & ", " & _
            "MediaCampaignDPAOverride                       = " & formatField(HttpContext.Current.Request("cbMediaCampaignDPAOverride"), "B", "NULL") & ", " & _
            "MediaCampaignRemarketableProductTypes          = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignRemarketableProductTypes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignDialerLoadTypeID	                = " & formatField(HttpContext.Current.Request("ddMediaCampaignDialerLoadTypeID"), "N", 0) & ", " & _
            "MediaCampaignDialerGradeID	                    = " & formatField(HttpContext.Current.Request("ddMediaCampaignDialerGradeID"), "N", 0) & ", " & _
            "MediaCampaignStatusCodes                       = " & formatField("|" & Replace(MediaCampaignStatusCodes, ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignSubStatusGroupID                  = " & formatField(HttpContext.Current.Request("ddMediaCampaignSubStatusGroupID"), "N", 0) & ", " & _
            "MediaCampaignCostInbound                       = " & formatField(HttpContext.Current.Request("txtMediaCampaignCostInbound"), "N", 0) & ", " & _
            "MediaCampaignCostOutbound                      = " & formatField(HttpContext.Current.Request("txtMediaCampaignCostOutbound"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolume                   = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolume"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeSun                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeSun"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeMon                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeMon"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeTue                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeTue"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeWed                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeWed"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeThu                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeThu"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeFri                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeFri"), "N", 0) & ", " & _
            "MediaCampaignPredictedVolumeSat                = " & formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeSat"), "N", 0) & ", " & _
            "MediaCampaignProductType                       = " & formatField(HttpContext.Current.Request("ddMediaCampaignProductType"), "", "NULL") & ", " & _
            "MediaCampaignDeliveryTypeID                    = " & formatField(HttpContext.Current.Request("ddMediaCampaignDeliveryTypeID"), "N", 1) & ", " & _
            "MediaCampaignDeliveryXMLPath                   = " & formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryXMLPath"), "", "NULL") & ", " & _
            "MediaCampaignDeliveryEmailAddress              = " & formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryEmailAddress"), "", "NULL") & ", " & _
            "MediaCampaignDeliveryBatch		                = " & formatField(HttpContext.Current.Request("cbMediaCampaignDeliveryBatch"), "B", 0) & ", " & _
            "MediaCampaignDeliveryToUser                    = " & formatField(HttpContext.Current.Request("cbMediaCampaignDeliveryToUser"), "B", 0) & ", " & _
            "MediaCampaignDeliveryRequired                  = " & formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryRequired"), "N", 0) & ", " & _
            "MediaCampaignDeliveryOverflowPercent           = " & formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryOverflowPercent"), "N", 0) & ", " & _
            "MediaCampaignOutboundWeighting                 = " & formatField(HttpContext.Current.Request("txtMediaCampaignOutboundWeighting"), "M", 1) & ", " & _
            "MediaCampaignCampaignReference		            = " & formatField(HttpContext.Current.Request("txtMediaCampaignCampaignReference"), "", "NULL") & ", " & _
            "MediaCampaignTelephoneNumberIDInbound	        = " & formatField(HttpContext.Current.Request("ddMediaCampaignTelephoneNumberIDInbound"), "N", 0) & ", " & _
            "MediaCampaignTelephoneNumberOutbound	        = " & formatField(HttpContext.Current.Request("txtMediaCampaignTelephoneNumberOutbound"), "", "NULL") & ", " & _
            "MediaCampaignClientReferenceRequired           = " & formatField(HttpContext.Current.Request("cbMediaCampaignClientReferenceRequired"), "B", 0) & ", " & _
            "MediaCampaignStatusCode		                = " & formatField(HttpContext.Current.Request("ddMediaCampaignStatusCode"), "U", "TFR") & ", " & _
            "MediaCampaignSubStatusCode		                = " & formatField(HttpContext.Current.Request("ddMediaCampaignSubStatusCode"), "U", "NULL") & ", " & _
            "MediaCampaignUpdateDateType		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignUpdateDateType"), "", "NULL") & ", " & _
            "MediaCampaignUpdateProductType		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignUpdateProductType"), "", "NULL") & ", " & _
            "MediaCampaignUpdateUserType		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignUpdateUserType"), "", "NULL") & ", " & _
            "MediaCampaignExemptMediaCampaignID             = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignExemptMediaCampaignID"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignMediaCampaignID                   = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignMediaCampaignID"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignProductTypes                      = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignProductTypes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignProductPurposes                   = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignProductPurposes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignMinPropertyBuyToLetMonthlyYield   = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinPropertyBuyToLetMonthlyYield"), "N", "NULL") & ", " & _
            "MediaCampaignPropertyTypes                     = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPropertyTypes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignPropertyConstructionTypes         = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPropertyConstructionTypes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignPropertyNewBuild		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignPropertyNewBuild"), "U", "NULL") & ", " & _
            "MediaCampaignPropertyOnMarket		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignPropertyOnMarket"), "U", "NULL") & ", " & _
            "MediaCampaignMinLTV                            = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinLTV"), "N", "NULL") & ", " & _
            "MediaCampaignMaxLTV                            = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxLTV"), "N", "NULL") & ", " & _
            "MediaCampaignMinAddressYears                   = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinAddressYears"), "N", "NULL") & ", " & _
            "MediaCampaignMinAmount                         = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinAmount"), "N", "NULL") & ", " & _
            "MediaCampaignMaxAmount                         = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxAmount"), "N", "NULL") & ", " & _
            "MediaCampaignMinTerm                           = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinTerm"), "N", "NULL") & ", " & _
            "MediaCampaignMaxTerm                           = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxTerm"), "N", "NULL") & ", " & _
            "MediaCampaignMinMortgagePaymentsMissed3        = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed3"), "N", "NULL") & ", " & _
            "MediaCampaignMaxMortgagePaymentsMissed3        = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed3"), "N", "NULL") & ", " & _
            "MediaCampaignMinMortgagePaymentsMissed6        = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed6"), "N", "NULL") & ", " & _
            "MediaCampaignMaxMortgagePaymentsMissed6        = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed6"), "N", "NULL") & ", " & _
            "MediaCampaignMinMortgagePaymentsMissed12       = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
            "MediaCampaignMaxMortgagePaymentsMissed12       = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
            "MediaCampaignMinNonMortgagePaymentsMissed12    = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinNonMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
            "MediaCampaignMaxNonMortgagePaymentsMissed12    = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxNonMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
            "MediaCampaignMinDefaultTotal                   = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinDefaultTotal"), "N", "NULL") & ", " & _
            "MediaCampaignMaxDefaultTotal                   = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxDefaultTotal"), "N", "NULL") & ", " & _
            "MediaCampaignMinCCJTotal                       = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinCCJTotal"), "N", "NULL") & ", " & _
            "MediaCampaignMaxCCJTotal                       = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxCCJTotal"), "N", "NULL") & ", " & _
            "MediaCampaignMinCreditScore                    = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinCreditScore"), "N", "NULL") & ", " & _
            "MediaCampaignMaxCreditScore                    = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxCreditScore"), "N", "NULL") & ", " & _
            "MediaCampaignBankruptcyAllowed		            = " & formatField(HttpContext.Current.Request("ddMediaCampaignBankruptcyAllowed"), "U", "NULL") & ", " & _
            "MediaCampaignDMPAllowed		                = " & formatField(HttpContext.Current.Request("ddMediaCampaignDMPAllowed"), "U", "NULL") & ", " & _
            "MediaCampaignEmploymentStatuses                = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignEmploymentStatuses"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignMinIncome                         = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinIncome"), "N", "NULL") & ", " & _
            "MediaCampaignMaxIncome                         = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxIncome"), "N", "NULL") & ", " & _
            "MediaCampaignIncomeMultiplier                  = " & formatField(HttpContext.Current.Request("txtMediaCampaignIncomeMultiplier"), "N", "NULL") & ", " & _
            "MediaCampaignMaxMonthlyOverdraftPercentage     = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxMonthlyOverdraftPercentage"), "N", "NULL") & ", " & _
            "MediaCampaignMinEmployerYears                  = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinEmployerYears"), "N", "NULL") & ", " & _
            "MediaCampaignMinPropertyValue                  = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinPropertyValue"), "N", "NULL") & ", " & _
            "MediaCampaignMaxPropertyValue                  = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxPropertyValue"), "N", "NULL") & ", " & _
            "MediaCampaignMinMortgageBalance                = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgageBalance"), "N", "NULL") & ", " & _
            "MediaCampaignMaxMortgageBalance                = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgageBalance"), "N", "NULL") & ", " & _
            "MediaCampaignMinMortgageMonths                 = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgageMonths"), "N", "NULL") & ", " & _
            "MediaCampaignLivingArrangement                 = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignLivingArrangement"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignMinAge                            = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinAge"), "N", "NULL") & ", " & _
            "MediaCampaignMaxAge                            = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxAge"), "N", "NULL") & ", " & _
            "MediaCampaignPostCodeRange                     = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPostCodeRange"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignAssignUserID                      = " & formatField(HttpContext.Current.Request("lstMediaCampaignAssignUserID"), "", "") & ", " & _
            "MediaCampaignAssignSalesUserID                 = " & formatField(HttpContext.Current.Request("lstMediaCampaignAssignSalesUserID"), "", "") & ", " & _
            "MediaCampaignCountry                           = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignCountry"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignMinUnsecuredCreditors             = " & formatField(HttpContext.Current.Request("txtMediaCampaignMinUnsecuredCreditors"), "N", "NULL") & ", " & _
            "MediaCampaignMaxUnsecuredCreditors             = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxUnsecuredCreditors"), "N", "NULL") & ", " & _
            "MediaCampaignProductStartMonths                = " & formatField(HttpContext.Current.Request("txtMediaCampaignProductStartMonths"), "N", "NULL") & ", " & _
            "MediaCampaignMaxCostInbound                    = " & formatField(HttpContext.Current.Request("txtMediaCampaignMaxCostInbound"), "N", "NULL") & ", " & _
            "MediaCampaignXMLCodes                          = " & formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignXMLCodes"), ",", "|") & "|", "", "NULL") & ", " & _
            "MediaCampaignStartDate                         = " & formatField(HttpContext.Current.Request("txtMediaCampaignStartDate"), "DTTM", "NULL") & ", " & _
            "MediaCampaignEndDate                           = " & formatField(HttpContext.Current.Request("txtMediaCampaignEndDate"), "DTTM", "NULL") & ", " & _
            "MediaCampaignPaused                            = " & formatField(HttpContext.Current.Request("cbMediaCampaignPaused"), "B", 0) & " " & _
            "WHERE MediaCampaignID	                        = '" & MediaCampaignID & "' " & _
            "AND CompanyID                                  = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(MediaCampaignName)) Then
                strQry = "INSERT INTO tblmediacampaigns (MediaID, CompanyID, MediaCampaignOutbound, MediaCampaignName, MediaCampaignFriendlyNameInbound, MediaCampaignFriendlyNameOutbound, " & _
                "MediaCampaignPurchaseMediaID, MediaCampaignApplicationTemplateID, MediaCampaignLetterID, MediaCampaignCloneableTo, MediaCampaignAutomaticTransfer, MediaCampaignDuplicateChecker, MediaCampaignDuplicateCheckerDays, MediaCampaignReturnsPolicy, MediaCampaignAutoReturn, MediaCampaignOutsideCriteriaDays, MediaCampaignAutoOutsideCriteriaSubStatus, MediaCampaignDataExpiryDays, MediaCampaignNoRemarkets, MediaCampaignDPAOverride, MediaCampaignRemarketableProductTypes, " & _
                "MediaCampaignDialerLoadTypeID, MediaCampaignDialerGradeID, MediaCampaignStatusCodes, MediaCampaignSubStatusGroupID, MediaCampaignCostInbound, MediaCampaignCostOutbound, MediaCampaignPredictedVolume, MediaCampaignPredictedVolumeSun, MediaCampaignPredictedVolumeMon, MediaCampaignPredictedVolumeTue, MediaCampaignPredictedVolumeWed, MediaCampaignPredictedVolumeThu, MediaCampaignPredictedVolumeFri, MediaCampaignPredictedVolumeSat, MediaCampaignProductType, MediaCampaignDeliveryTypeID, " & _
                "MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress, MediaCampaignDeliveryBatch, MediaCampaignDeliveryToUser, MediaCampaignDeliveryRequired, MediaCampaignDeliveryOverflowPercent, MediaCampaignOutboundWeighting, " & _
                "MediaCampaignCampaignReference, MediaCampaignTelephoneNumberIDInbound, MediaCampaignTelephoneNumberOutbound, MediaCampaignClientReferenceRequired, MediaCampaignStatusCode, MediaCampaignSubStatusCode, MediaCampaignUpdateDateType, MediaCampaignUpdateProductType, MediaCampaignUpdateUserType, MediaCampaignExemptMediaCampaignID, MediaCampaignMediaCampaignID, MediaCampaignProductTypes, MediaCampaignProductPurposes, " & _
                "MediaCampaignMinPropertyBuyToLetMonthlyYield, MediaCampaignPropertyTypes, MediaCampaignPropertyConstructionTypes, MediaCampaignPropertyNewBuild, MediaCampaignPropertyOnMarket,  MediaCampaignMinLTV, " & _
                "MediaCampaignMaxLTV, MediaCampaignMinAddressYears, MediaCampaignMinAmount, MediaCampaignMaxAmount, MediaCampaignMinTerm, MediaCampaignMaxTerm, MediaCampaignMinMortgagePaymentsMissed3, MediaCampaignMaxMortgagePaymentsMissed3, MediaCampaignMinMortgagePaymentsMissed6, MediaCampaignMaxMortgagePaymentsMissed6, MediaCampaignMinMortgagePaymentsMissed12, MediaCampaignMaxMortgagePaymentsMissed12, " & _
                "MediaCampaignMinNonMortgagePaymentsMissed12, MediaCampaignMaxNonMortgagePaymentsMissed12, MediaCampaignMinDefaultTotal, MediaCampaignMaxDefaultTotal, MediaCampaignMinCCJTotal, MediaCampaignMaxCCJTotal, MediaCampaignMinCreditScore, MediaCampaignMaxCreditScore, MediaCampaignBankruptcyAllowed, MediaCampaignDMPAllowed, MediaCampaignEmploymentStatuses, MediaCampaignMinIncome, MediaCampaignMaxIncome, " & _
                "MediaCampaignIncomeMultiplier, MediaCampaignMaxMonthlyOverdraftPercentage, MediaCampaignMinEmployerYears, MediaCampaignMinPropertyValue, MediaCampaignMaxPropertyValue, MediaCampaignMinMortgageBalance, MediaCampaignMaxMortgageBalance, " & _
                "MediaCampaignMinMortgageMonths, MediaCampaignLivingArrangement, MediaCampaignMinAge, MediaCampaignMaxAge, MediaCampaignPostCodeRange, MediaCampaignAssignUserID, MediaCampaignAssignSalesUserID, MediaCampaignCountry, MediaCampaignMinUnsecuredCreditors, " & _
                "MediaCampaignMaxUnsecuredCreditors, MediaCampaignProductStartMonths, MediaCampaignMaxCostInbound, MediaCampaignXMLCodes, MediaCampaignStartDate, MediaCampaignEndDate, MediaCampaignPaused) " & _
                "VALUES(" & formatField(MediaID, "N", 0) & ", " & _
                formatField(CompanyID, "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignOutbound"), "B", 0) & ", " & _
                formatField(MediaCampaignName, "T", "") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignFriendlyNameInbound"), "T", "") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignFriendlyNameOutbound"), "T", "") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignPurchaseMediaID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignApplicationTemplateID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("MCLetters"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignCloneableTo"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignAutomaticTransfer"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignDuplicateChecker"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDuplicateCheckerDays"), "N", 7) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignReturnsPolicy"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignAutoReturn"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignOutsideCriteriaDays"), "N", 5) & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignAutoOutsideCriteriaSubStatus"), "U", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDataExpiryDays"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignNoRemarkets"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignDPAOverride"), "B", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignRemarketableProductTypes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignDialerLoadTypeID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignDialerGradeID"), "N", 0) & ", " & _
                formatField("|" & Replace(MediaCampaignStatusCodes, ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignSubStatusGroupID"), "", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignCostInbound"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignCostOutbound"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolume"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeSun"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeMon"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeTue"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeWed"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeThu"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeFri"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignPredictedVolumeSat"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignProductType"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignDeliveryTypeID"), "N", 1) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryXMLPath"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryEmailAddress"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignDeliveryBatch"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignDeliveryToUser"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryRequired"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignDeliveryOverflowPercent"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignOutboundWeighting"), "M", 1) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignCampaignReference"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignTelephoneNumberIDInbound"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignTelephoneNumberOutbound"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignClientReferenceRequired"), "B", 0) & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignStatusCode"), "U", "TFR") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignSubStatusCode"), "U", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignUpdateDateType"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignUpdateProductType"), "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignUpdateUserType"), "", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignExemptMediaCampaignID"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignMediaCampaignID"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignProductTypes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignProductPurposes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinPropertyBuyToLetMonthlyYield"), "N", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPropertyTypes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPropertyConstructionTypes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignPropertyNewBuild"), "U", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignPropertyOnMarket"), "U", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinLTV"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxLTV"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinAddressYears"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinAmount"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxAmount"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinTerm"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxTerm"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed3"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed3"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed6"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed6"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinNonMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxNonMortgagePaymentsMissed12"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinDefaultTotal"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxDefaultTotal"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinCCJTotal"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxCCJTotal"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinCreditScore"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxCreditScore"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignBankruptcyAllowed"), "U", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("ddMediaCampaignDMPAllowed"), "U", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignEmploymentStatuses"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinIncome"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxIncome"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignIncomeMultiplier"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxMonthlyOverdraftPercentage"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinEmployerYears"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinPropertyValue"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxPropertyValue"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgageBalance"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxMortgageBalance"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinMortgageMonths"), "N", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignLivingArrangement"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinAge"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxAge"), "N", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignPostCodeRange"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("lstMediaCampaignAssignUserID"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("lstMediaCampaignAssignSalesUserID"), "", "") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignCountry"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMinUnsecuredCreditors"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxUnsecuredCreditors"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignProductStartMonths"), "N", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignMaxCostInbound"), "N", "NULL") & ", " & _
                formatField("|" & Replace(HttpContext.Current.Request("lstMediaCampaignXMLCodes"), ",", "|") & "|", "", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignStartDate"), "DTTM", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("txtMediaCampaignEndDate"), "DTTM", "NULL") & ", " & _
                formatField(HttpContext.Current.Request("cbMediaCampaignPaused"), "B", 0) & ") "
				
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblmediacampaigns,vwscheduledrules")
    End Sub

    Private Sub saveMediaCampaignActive()
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim MediaCampaignActive As Boolean = HttpContext.Current.Request("MediaCampaignActive")
        Dim strQry As String = ""
        If (checkValue(MediaCampaignID)) Then
            If (MediaCampaignActive) Then
                strQry = "UPDATE tblmediacampaigns SET  " & _
                   "MediaCampaignActive	    = 0 " & _
                   "WHERE MediaCampaignID	= '" & MediaCampaignID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblmediacampaigns SET  " & _
                   "MediaCampaignActive		= 1 " & _
                   "WHERE MediaCampaignID	= '" & MediaCampaignID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblmediacampaigns,vwscheduledrules")
        End If
    End Sub

    Private Sub saveMediaCampaignDelete()
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim strQry As String = ""
        If (checkValue(MediaCampaignID)) Then
            strQry = "DELETE FROM tblmediacampaigns WHERE MediaCampaignID = '" & MediaCampaignID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblmediacampaigns,vwscheduledrules")
        End If
    End Sub

    Private Sub saveMediaCampaignCost()
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim intDistributionPercentage As String = HttpContext.Current.Request("MediaCampaignDistributionPercentage")
        'Dim arrDistributionPercentage As Array = Split(strDistributionPercentage, ",")
        'Dim strPercentageList As String = "|"
        'For x As Integer = CInt(arrDistributionPercentage(0)) To CInt(arrDistributionPercentage(1)) Step 10
        '    'If (x > 0) Then
        '    strPercentageList += x / 10 & "|"
        '    'End If
        'Next
        If (checkValue(MediaCampaignID)) Then
            Dim strQry As String = "UPDATE tblmediacampaigns SET  " & _
            "MediaCampaignCostInbound                       = " & formatField(HttpContext.Current.Request("txtMediaCampaignCostInbound"), "N", 0) & ", " & _
            "MediaCampaignCostOutbound                      = " & formatField(HttpContext.Current.Request("txtMediaCampaignCostOutbound"), "N", 0) & ", " & _
            "MediaCampaignOutboundWeighting                 = " & formatField(HttpContext.Current.Request("MediaCampaignOutboundWeighting"), "N", 0) & ", " & _
            "MediaCampaignDeliveryRequired                  = " & formatField(HttpContext.Current.Request("MediaCampaignDeliveryRequired"), "N", 0) & ", " & _
            "MediaCampaignDeliveryOverflowPercent           = " & formatField(HttpContext.Current.Request("MediaCampaignDeliveryOverflowPercent"), "N", 0) & " " & _
            "WHERE MediaCampaignID	                        = '" & MediaCampaignID & "' " & _
            "AND CompanyID 				                    = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveMediaCampaignSchedule()
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim MediaCampaignScheduleID As String = HttpContext.Current.Request("MediaCampaignScheduleID")
        Dim MediaCampaignScheduleBatchNo As Integer = 0
        If (checkValue(HttpContext.Current.Request("MediaCampaignScheduleBatchNo"))) Then
            MediaCampaignScheduleBatchNo = HttpContext.Current.Request("MediaCampaignScheduleBatchNo")
        End If
        Dim NextMediaCampaignScheduleBatchNo As Integer = getNextBatchNumber()
        Dim intStartYear As Integer = 0, intStartWeek As Integer = 0, intStartWeekDay As Integer = 0, intEndYear As Integer = 0, intEndWeek As Integer = 0, intEndWeekDay As Integer = 0
        Dim intSkipWeeks As Integer = 1, intRecurWeeks As Integer = 0, intCurrentYear As Integer = 0, intCurrentWeek As Integer = 0, intCurrentWeekDay As Integer = 0, intCurrentRecur As Integer = 0
        Dim dteStartDate As Date, dteEndDate As Date, dteCurrentDate As Date
        If checkValue(HttpContext.Current.Request("txtMediaCampaignScheduleBatchStartDate")) Then
            dteStartDate = CDate(HttpContext.Current.Request("txtMediaCampaignScheduleBatchStartDate"))
            dteCurrentDate = dteStartDate
            intStartYear = dteStartDate.Year
            intStartWeek = CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(dteStartDate, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
            intStartWeekDay = Weekday(dteStartDate, Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
        Else
            dteStartDate = Now
            dteCurrentDate = dteStartDate
            intStartYear = dteStartDate.Year
            intStartWeek = CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(dteStartDate, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
            intStartWeekDay = Weekday(dteStartDate, Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
        End If
        If checkValue(HttpContext.Current.Request("txtMediaCampaignScheduleBatchEndDate")) Then
            dteEndDate = CDate(HttpContext.Current.Request("txtMediaCampaignScheduleBatchEndDate"))
            intEndYear = dteEndDate.Year
            intEndWeek = CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(dteEndDate, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
            intEndWeekDay = Weekday(dteEndDate, Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
        End If
        If checkValue(HttpContext.Current.Request("txtMediaCampaignScheduleBatchSkipWeeks")) Then
            intSkipWeeks = CInt(HttpContext.Current.Request("txtMediaCampaignScheduleBatchSkipWeeks"))
        End If
        If checkValue(HttpContext.Current.Request("txtMediaCampaignScheduleBatchRecurWeeks")) Then
            intRecurWeeks = CInt(HttpContext.Current.Request("txtMediaCampaignScheduleBatchRecurWeeks"))
        End If
        Dim strValidDays As String = getDayNumbers()

        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0

        Dim intDelivered As Integer = resetBatch(MediaCampaignScheduleBatchNo, intStartYear, intStartWeek, intStartWeekDay)

        Dim x As Integer = 0, MediaCampaignScheduleDelivered As Integer = 0

        If (intRecurWeeks > 0) Then
            Do While intCurrentRecur <= intRecurWeeks - 1
                For intCurrentWeekDay = intStartWeekDay To 7
                    If (intCurrentRecur > intRecurWeeks - 1) Then Exit For
                    intCurrentYear = dteCurrentDate.Year
                    intCurrentWeek = CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(dteCurrentDate, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
                    If (InStr(strValidDays, intCurrentWeekDay) > 0) Then
                        If (x = 0) Then
                            MediaCampaignScheduleDelivered = intDelivered
                        Else
                            MediaCampaignScheduleDelivered = 0
                        End If
                        strQry = "INSERT INTO tblmediacampaignschedules(CompanyID, MediaCampaignID, MediaCampaignScheduleBatchNo, MediaCampaignScheduleYear, MediaCampaignScheduleWeek, MediaCampaignScheduleWeekDay, " & _
                                 "MediaCampaignScheduleStartHour, MediaCampaignScheduleEndHour, MediaCampaignScheduleDelivered, " & _
                                 "MediaCampaignSchedulePaused) " & _
                                 "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                 formatField(MediaCampaignID, "N", 0) & ", " & _
                                 NextMediaCampaignScheduleBatchNo & ", " & _
                                 formatField(intCurrentYear, "N", 0) & ", " & _
                                 formatField(intCurrentWeek, "N", 0) & ", " & _
                                 formatField(intCurrentWeekDay, "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchStartHour"), "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchEndHour"), "N", 0) & ", " & _
                                 formatField(MediaCampaignScheduleDelivered, "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("cbMediaCampaignScheduleBatchPaused"), "B", 0) & ") "
                        executeNonQuery(strQry)
                        x += 1
                    End If
                    dteCurrentDate = DateAdd(DateInterval.Weekday, 1, dteCurrentDate)
                Next
                intStartWeekDay = 1
                If (intSkipWeeks > 1) Then
                    dteCurrentDate = DateAdd(DateInterval.WeekOfYear, intSkipWeeks, DateAdd(DateInterval.Weekday, 1 - intCurrentWeekDay, dteCurrentDate))
                End If
                intCurrentRecur = intCurrentRecur + 1
            Loop
        Else
            Do While dteCurrentDate <= dteEndDate
                For intCurrentWeekDay = intStartWeekDay To 7
                    If (dteCurrentDate > dteEndDate) Then Exit For
                    intCurrentYear = dteCurrentDate.Year
                    intCurrentWeek = CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(dteCurrentDate, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
                    If (InStr(strValidDays, intCurrentWeekDay) > 0) Then
                        If (x = 0) Then
                            MediaCampaignScheduleDelivered = intDelivered
                        Else
                            MediaCampaignScheduleDelivered = 0
                        End If
                        strQry = "INSERT INTO tblmediacampaignschedules(CompanyID, MediaCampaignID, MediaCampaignScheduleBatchNo, MediaCampaignScheduleYear, MediaCampaignScheduleWeek, MediaCampaignScheduleWeekDay, " & _
                                 "MediaCampaignScheduleStartHour, MediaCampaignScheduleEndHour, MediaCampaignScheduleDelivered, " & _
                                 "MediaCampaignSchedulePaused) " & _
                                 "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                 formatField(MediaCampaignID, "N", 0) & ", " & _
                                 NextMediaCampaignScheduleBatchNo & ", " & _
                                 formatField(intCurrentYear, "N", 0) & ", " & _
                                 formatField(intCurrentWeek, "N", 0) & ", " & _
                                 formatField(intCurrentWeekDay, "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchStartHour"), "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchEndHour"), "N", 0) & ", " & _
                                 formatField(MediaCampaignScheduleDelivered, "N", 0) & ", " & _
                                 formatField(HttpContext.Current.Request("cbMediaCampaignScheduleBatchPaused"), "B", 0) & ") "
                        executeNonQuery(strQry)
                        x += 1
                    End If
                    dteCurrentDate = DateAdd(DateInterval.Weekday, 1, dteCurrentDate)
                Next
                intStartWeekDay = 1
                If (intSkipWeeks > 1) Then
                    dteCurrentDate = DateAdd(DateInterval.WeekOfYear, intSkipWeeks, DateAdd(DateInterval.Weekday, 1 - intCurrentWeekDay, dteCurrentDate))
                End If
            Loop
        End If
        strQry = "UPDATE tblmediacampaignschedules SET MediaCampaignScheduleDelivered = " & formatField(intDelivered, "N", 0) & " " & _
        "WHERE MediaCampaignScheduleBatchNo = '" & NextMediaCampaignScheduleBatchNo & "' AND " & _
        "MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek = " & intStartWeek & " AND MediaCampaignScheduleWeekDay = " & intStartWeekDay & " " & _
        "AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        updateBatch(NextMediaCampaignScheduleBatchNo, strValidDays)
        cacheDependency(CacheObject, "vwscheduledrules")
    End Sub

    Private Sub updateBatch(ByVal NextMediaCampaignScheduleBatchNo As Integer, ByVal strValidDays As String)
        Dim MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
        Dim MediaCampaignScheduleBatchID As String = HttpContext.Current.Request("MediaCampaignScheduleBatchID")
        Dim MediaCampaignScheduleBatchStartDate As String = HttpContext.Current.Request("txtMediaCampaignScheduleBatchStartDate")
        Dim MediaCampaignScheduleBatchEndDate As String = HttpContext.Current.Request("txtMediaCampaignScheduleBatchEndDate")
        Dim strQry As String = ""
        If (checkValue(MediaCampaignScheduleBatchID)) Then
            strQry = "UPDATE tblmediacampaignschedulebatches SET  " & _
                "MediaCampaignScheduleBatchEndDate      = " & formatField(DateAdd(DateInterval.Day, -1, CDate(MediaCampaignScheduleBatchStartDate)), "DTTM", "NULL") & " " & _
                "WHERE MediaCampaignScheduleBatchID	    = '" & MediaCampaignScheduleBatchID & "' " & _
                "AND CompanyID                          = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        strQry = "INSERT INTO tblmediacampaignschedulebatches(CompanyID, MediaCampaignID, MediaCampaignScheduleBatchNo, MediaCampaignScheduleBatchStartDate, " & _
                 "MediaCampaignScheduleBatchEndDate, MediaCampaignScheduleBatchStartHour, MediaCampaignScheduleBatchEndHour, MediaCampaignScheduleBatchSkipWeeks, " & _
                 "MediaCampaignScheduleBatchRecurWeeks, MediaCampaignScheduleBatchValidDays, MediaCampaignScheduleBatchRecurring, MediaCampaignScheduleBatchPaused)" & _
                 "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                 formatField(MediaCampaignID, "N", 0) & ", " & _
                 NextMediaCampaignScheduleBatchNo & ", " & _
                 formatField(MediaCampaignScheduleBatchStartDate, "DTTM", "NULL") & ", " & _
                 formatField(MediaCampaignScheduleBatchEndDate, "DTTM", "NULL") & ", " & _
                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchStartHour"), "N", 0) & ", " & _
                 formatField(HttpContext.Current.Request("ddMediaCampaignScheduleBatchEndHour"), "N", 0) & ", " & _
                 formatField(HttpContext.Current.Request("txtMediaCampaignScheduleBatchSkipWeeks"), "N", "1") & ", " & _
                 formatField(HttpContext.Current.Request("txtMediaCampaignScheduleBatchRecurWeeks"), "N", "NULL") & ", " & _
                 formatField(strValidDays, "", "NULL") & ", " & _
                 formatField(HttpContext.Current.Request("cbMediaCampaignScheduleBatchRecurring"), "B", 0) & ", " & _
                 formatField(HttpContext.Current.Request("cbMediaCampaignScheduleBatchPaused"), "B", 0) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Function resetBatch(ByVal MediaCampaignScheduleBatchNo As String, ByVal intStartYear As String, ByVal intStartWeek As String, ByVal intStartWeekDay As String) As Integer
        Dim MediaCampaignScheduleID As String = "", intDelivered As String = 0
        Dim strSQL As String = "SELECT MediaCampaignScheduleID, MediaCampaignScheduleDelivered FROM tblmediacampaignschedules WHERE MediaCampaignScheduleBatchNo = '" & MediaCampaignScheduleBatchNo & "' AND " & _
                                "MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek = " & intStartWeek & " AND MediaCampaignScheduleWeekDay = " & intStartWeekDay & " " & _
                                "AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblmediacampaignschedules")
        Dim dsSchedules As DataTable = objDataSet.Tables("tblmediacampaignschedules")
        If (dsSchedules.Rows.Count > 0) Then
            For Each Row As DataRow In dsSchedules.Rows
                MediaCampaignScheduleID = Row.Item("MediaCampaignScheduleID")
                intDelivered = Row.Item("MediaCampaignScheduleDelivered")
            Next
        Else
            intDelivered = 0
            MediaCampaignScheduleID = ""
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsSchedules = Nothing
        objDatabase = Nothing
        Dim strQry As String = "UPDATE tblmediacampaignschedules SET MediaCampaignScheduleBatchNo = 0 WHERE MediaCampaignScheduleBatchNo = '" & MediaCampaignScheduleBatchNo & "' AND (" & _
         "(MediaCampaignScheduleYear > " & intStartYear & ") OR " & _
         "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek > " & intStartWeek & ") OR " & _
         "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek = " & intStartWeek & " AND MediaCampaignScheduleWeekDay >= " & intStartWeekDay & ")" & _
         ") " & _
         "AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        strQry = "UPDATE tblmediacampaignschedules SET MediaCampaignScheduleDelivered = 0 WHERE MediaCampaignScheduleID = '" & MediaCampaignScheduleID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        Return intDelivered
    End Function

    Private Function getDayNumbers() As String
        Dim strValidDays As String = ""
        Dim strComma As String = ""
        Dim strCheckboxDays As String = _
            HttpContext.Current.Request("cbSunday") & "," & _
            HttpContext.Current.Request("cbMonday") & "," & _
            HttpContext.Current.Request("cbTuesday") & "," & _
            HttpContext.Current.Request("cbWednesday") & "," & _
            HttpContext.Current.Request("cbThursday") & "," & _
            HttpContext.Current.Request("cbFriday") & "," & _
            HttpContext.Current.Request("cbSaturday")
        Dim arrDays As Array = Split(strCheckboxDays, ",")
        For x As Integer = 0 To UBound(arrDays)
            If (x < UBound(arrDays)) Then
                strComma = ","
            Else
                strComma = ""
            End If
            If (arrDays(x) = "on") Then
                strValidDays += x + 1 & strComma
            Else
                strValidDays += strComma
            End If
        Next
        Return strValidDays
    End Function

    Private Sub saveMediaCampaignSchedulePaused()
        Dim MediaCampaignScheduleBatchID As String = HttpContext.Current.Request("MediaCampaignScheduleBatchID")
        Dim MediaCampaignScheduleBatchNo As String = HttpContext.Current.Request("MediaCampaignScheduleBatchNo")
        Dim MediaCampaignScheduleBatchPaused As Boolean = HttpContext.Current.Request("MediaCampaignScheduleBatchPaused")
        Dim intStartYear As Integer = 0, intStartWeek As Integer = 0, intStartWeekDay As Integer = 0
        intStartYear = Now.Year
        intStartWeek = System.Threading.Thread.CurrentThread.CurrentCulture.Calendar.GetWeekOfYear(Now, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
        intStartWeekDay = Weekday(Now, Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
        Dim strQry As String = ""
        If (checkValue(MediaCampaignScheduleBatchID)) Then
            If (MediaCampaignScheduleBatchPaused) Then
                strQry = "UPDATE tblmediacampaignschedulebatches SET  " & _
                   "MediaCampaignScheduleBatchPaused    = 0 " & _
                   "WHERE MediaCampaignScheduleBatchID	= '" & MediaCampaignScheduleBatchID & "' " & _
                   "AND CompanyID                       = '" & CompanyID & "'"
                executeNonQuery(strQry)
                strQry = "UPDATE tblmediacampaignschedules SET MediaCampaignSchedulePaused = 0 WHERE MediaCampaignScheduleBatchNo = '" & MediaCampaignScheduleBatchNo & "' AND (" & _
                 "(MediaCampaignScheduleYear > " & intStartYear & ") OR " & _
                 "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek > " & intStartWeek & ") OR " & _
                 "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek = " & intStartWeek & " AND MediaCampaignScheduleWeekDay >= " & intStartWeekDay & ")" & _
                 ")" & _
                 "AND CompanyID = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblmediacampaignschedulebatches SET  " & _
                   "MediaCampaignScheduleBatchPaused    = 1 " & _
                   "WHERE MediaCampaignScheduleBatchID	= '" & MediaCampaignScheduleBatchID & "' " & _
                   "AND CompanyID                       = '" & CompanyID & "'"
                executeNonQuery(strQry)
                strQry = "UPDATE tblmediacampaignschedules SET MediaCampaignSchedulePaused = 1 WHERE MediaCampaignScheduleBatchNo = '" & MediaCampaignScheduleBatchNo & "' AND (" & _
                 "(MediaCampaignScheduleYear > " & intStartYear & ") OR " & _
                 "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek > " & intStartWeek & ") OR " & _
                 "(MediaCampaignScheduleYear = " & intStartYear & " AND MediaCampaignScheduleWeek = " & intStartWeek & " AND MediaCampaignScheduleWeekDay >= " & intStartWeekDay & ")" & _
                 ")" & _
                 "AND CompanyID = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "vwscheduledrules")
        End If
    End Sub

    Private Sub saveApplicationTemplate()
        Dim ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID")
        Dim ApplicationTemplateName As String = HttpContext.Current.Request("txtApplicationTemplateName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ApplicationTemplateID)) Then
            strQry = "UPDATE tblapplicationtemplates SET  " & _
               "ApplicationTemplateName	            = " & formatField(ApplicationTemplateName, "T", "") & ", " & _
               "ApplicationTemplateGradeID          = " & formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateGradeID"), ",", "|") & "|", "", "NULL") & ", " & _
               "ApplicationTemplateStatusCode       = " & formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
               "ApplicationTemplateSubStatusCode    = " & formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateSubStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
               "ApplicationTemplateUserLevel        = " & formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateUserLevel"), ",", "|") & "|", "", "NULL") & " " & _
               "WHERE ApplicationTemplateID         = '" & ApplicationTemplateID & "' " & _
               "AND CompanyID                       = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(ApplicationTemplateName)) Then
                strQry = "INSERT INTO tblapplicationtemplates (CompanyID, ApplicationTemplateName, ApplicationTemplateGradeID, ApplicationTemplateStatusCode, ApplicationTemplateSubStatusCode, ApplicationTemplateUserLevel) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(ApplicationTemplateName, "T", "") & ", " & _
                            formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateGradeID"), ",", "|") & "|", "", "NULL") & ", " & _
                            formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
                            formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateSubStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
                            formatField("|" & Replace(HttpContext.Current.Request("lstApplicationTemplateUserLevel"), ",", "|") & "|", "", "NULL") & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblapplicationtemplates")
    End Sub

    Private Sub saveApplicationTemplateDelete()
        Dim ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID")
        Dim strQry As String = ""
        If (checkValue(ApplicationTemplateID)) Then
            strQry = "DELETE FROM tblapplicationtemplates WHERE ApplicationTemplateID = '" & ApplicationTemplateID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblapplicationtemplates")
        End If
    End Sub

    Private Sub saveApplicationTemplateActive()
        Dim ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID")
        Dim ApplicationTemplateActive As Boolean = HttpContext.Current.Request("ApplicationTemplateActive")
        Dim strQry As String = ""
        If (checkValue(ApplicationTemplateID)) Then
            If (ApplicationTemplateActive) Then
                strQry = "UPDATE tblapplicationtemplates SET  " & _
                   "ApplicationTemplateActive	= 0 " & _
                   "WHERE ApplicationTemplateID	= '" & ApplicationTemplateID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblapplicationtemplates SET  " & _
                   "ApplicationTemplateActive	= 1 " & _
                   "WHERE ApplicationTemplateID	= '" & ApplicationTemplateID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblapplicationtemplates")
        End If
    End Sub

    Private Sub saveApplicationPage()
        Dim ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID")
        Dim ApplicationPageID As String = HttpContext.Current.Request("ApplicationPageID")
        Dim ApplicationPageName As String = HttpContext.Current.Request("txtApplicationPageName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ApplicationTemplateID)) Then
            strQry = "UPDATE tblapplicationpages SET  " & _
               "ApplicationPageName	                    = " & formatField(ApplicationPageName, "T", "") & ", " & _
               "ApplicationPageSQLConditions	        = " & formatField(HttpContext.Current.Request("txtApplicationPageSQLConditions"), "", "NULL") & ", " & _
               "ApplicationPageFunctionOnSubmitEvent    = " & formatField(HttpContext.Current.Request("txtApplicationPageFunctionOnSubmitEvent"), "", "NULL") & ", " & _
               "ApplicationPageJavascriptFunctions      = " & formatField(HttpContext.Current.Request("txtApplicationPageJavascriptFunctions"), "", "NULL") & ", " & _
               "ApplicationPageFormAction               = " & formatField(HttpContext.Current.Request("txtApplicationPageFormAction"), "", "") & ", " & _
               "ApplicationPageFormActionParameters     = " & formatField(HttpContext.Current.Request("txtApplicationPageFormActionParameters"), "", "") & ", " & _
               "ApplicationPageCreateDiaries            = " & formatField(HttpContext.Current.Request("CreateDiaries"), "", "") & ", " & _
               "ApplicationPageCompleteDiaries          = " & formatField(HttpContext.Current.Request("CompleteDiaries"), "", "") & ", " & _
               "ApplicationPageCancelDiaries            = " & formatField(HttpContext.Current.Request("CancelDiaries"), "", "") & ", " & _
               "ApplicationPageReturnURL                = " & formatField(HttpContext.Current.Request("txtApplicationPageReturnURL"), "", "") & ", " & _
               "ApplicationPageTargetFrame              = " & formatField(HttpContext.Current.Request("txtApplicationPageTargetFrame"), "", "") & ", " & _
               "ApplicationPageUpdateStatusCode         = " & formatField(HttpContext.Current.Request("ddApplicationPageUpdateStatusCode"), "", "") & ", " & _
               "ApplicationPageUpdateSubStatusCode      = " & formatField(HttpContext.Current.Request("ddApplicationPageUpdateSubStatusCode"), "", "") & ", " & _
               "ApplicationPageUpdateDateType           = " & formatField(HttpContext.Current.Request("ddApplicationPageUpdateDateType"), "", "") & ", " & _
               "ApplicationPageNote                     = " & formatField(HttpContext.Current.Request("txtApplicationPageNote"), "", "") & ", " & _
               "ApplicationPageSocialPost               = " & formatField(HttpContext.Current.Request("txtApplicationPageSocialPost"), "", "") & ", " & _
               "ApplicationPageMandatory                = " & formatField(HttpContext.Current.Request("cbApplicationPageMandatory"), "B", 0) & ", " & _
               "ApplicationPageSubmit                   = " & formatField(HttpContext.Current.Request("cbApplicationPageSubmit"), "B", 0) & ", " & _
               "ApplicationPageOrder	                = " & formatField(HttpContext.Current.Request("txtApplicationPageOrder"), "N", 0) & " " & _
               "WHERE ApplicationPageID                 = '" & ApplicationPageID & "' " & _
               "AND ApplicationPageTemplateID           = '" & ApplicationTemplateID & "'" & _
               "AND CompanyID                           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(ApplicationPageName)) Then
                strQry = "INSERT INTO tblapplicationpages (CompanyID, ApplicationPageTemplateID, ApplicationPageName, ApplicationPageSQLConditions, ApplicationPageFunctionOnSubmitEvent, ApplicationPageJavascriptFunctions, " & _
                    "ApplicationPageFormAction, ApplicationPageFormActionParameters, ApplicationPageCreateDiaries, ApplicationPageCompleteDiaries, ApplicationPageCancelDiaries, ApplicationPageReturnURL, ApplicationPageTargetFrame, ApplicationPageUpdateStatusCode, ApplicationPageUpdateSubStatusCode, ApplicationPageUpdateDateType, ApplicationPageNote, ApplicationPageSocialPost, ApplicationPageMandatory, ApplicationPageSubmit, ApplicationPageOrder) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(ApplicationTemplateID, "N", 0) & ", " & _
                    formatField(ApplicationPageName, "T", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageSQLConditions"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageFunctionOnSubmitEvent"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageJavascriptFunctions"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageFormAction"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageFormActionParameters"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CreateDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CompleteDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CancelDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageReturnURL"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageTargetFrame"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddApplicationPageUpdateStatusCode"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddApplicationPageUpdateSubStatusCode"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddApplicationPageUpdateDateType"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageNote"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageSocialPost"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("cbApplicationPageMandatory"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbApplicationPageSubmit"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationPageOrder"), "N", 0) & ") "
                ApplicationPageID = executeIdentityQuery(strQry)
            End If
        End If
        strQry = "DELETE FROM tblapplicationpagemappings WHERE ApplicationPageID = '" & ApplicationPageID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        If checkValue(HttpContext.Current.Request("AssignedFields")) Then
            Dim arrAssignedFields As Array = Split(HttpContext.Current.Request("AssignedFields"), ",")
            Dim arrOptions As Array
            For Each Item As String In arrAssignedFields
                arrOptions = Split(HttpContext.Current.Request("Options" & Item), "|")
				If (UBound(arrOptions) = 7) Then
					strQry = "INSERT INTO tblapplicationpagemappings (CompanyID, ApplicationPageID, ApplicationFieldID, ApplicationMandatory, ApplicationFunctionOnClickEvent, ApplicationFunctionOnFocusEvent, ApplicationFunctionOnBlurEvent, ApplicationFunctionOnChangeEvent, ApplicationStatusCode, ApplicationSubStatusCode, ApplicationDateType) " & _
					   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
					   formatField(ApplicationPageID, "N", 0) & ", " & _
					   formatField(Item, "N", 0) & ", " & _
					   formatField(arrOptions(0), "B", 0) & ", " & _
					   formatField(arrOptions(1), "", "") & ", " & _
					   formatField(arrOptions(2), "", "") & ", " & _
					   formatField(arrOptions(3), "", "") & ", " & _
					   formatField(arrOptions(4), "", "") & ", " & _
					   formatField(Replace(arrOptions(5), ",", "|"), "", "||") & ", " & _
					   formatField(Replace(arrOptions(6), ",", "|"), "", "||") & ", " & _
					   formatField(arrOptions(7), "", "") & ") "
					executeNonQuery(strQry)
				Else
					strQry = "INSERT INTO tblapplicationpagemappings (CompanyID, ApplicationPageID, ApplicationFieldID) " & _
					   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
					   formatField(ApplicationPageID, "N", 0) & ", " & _
					   formatField(Item, "N", 0) & ") "
					executeNonQuery(strQry)				
				End If
            Next
        End If
        cacheDependency(CacheObject, "tblapplicationpages,tblapplicationpagemappings,vwassignedapplicationfields")
    End Sub

    Private Sub saveApplicationPageDelete()
        Dim ApplicationPageID As String = HttpContext.Current.Request("ApplicationPageID")
        Dim strQry As String = ""
        If (checkValue(ApplicationPageID)) Then
            strQry = "DELETE FROM tblapplicationpages WHERE ApplicationPageID = '" & ApplicationPageID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblapplicationpages")
        End If
    End Sub

    Private Sub saveApplicationPageActive()
        Dim ApplicationPageID As String = HttpContext.Current.Request("ApplicationPageID")
        Dim ApplicationPageActive As Boolean = HttpContext.Current.Request("ApplicationPageActive")
        Dim strQry As String = ""
        If (checkValue(ApplicationPageID)) Then
            If (ApplicationPageActive) Then
                strQry = "UPDATE tblapplicationpages SET  " & _
                   "ApplicationPageActive	= 0 " & _
                   "WHERE ApplicationPageID	= '" & ApplicationPageID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblapplicationpages SET  " & _
                   "ApplicationPageActive	= 1 " & _
                   "WHERE ApplicationPageID	= '" & ApplicationPageID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblapplicationpages")
        End If
    End Sub

    Private Sub saveProcessingScreen()
        Dim ProcessingScreenID As String = HttpContext.Current.Request("ProcessingScreenID")
        Dim ProcessingScreenName As String = HttpContext.Current.Request("txtProcessingScreenName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ProcessingScreenID)) Then
            strQry = "UPDATE tblprocessingscreens SET  " & _
               "ProcessingScreenName	                = " & formatField(ProcessingScreenName, "T", "") & ", " & _
               "ProcessingScreenGradeID	                = " & formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenGradeID"), ",", "|") & "|", "", "||") & ", " & _
               "ProcessingScreenProductType             = " & formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenProductType"), ",", "|") & "|", "", "||") & ", " & _
               "ProcessingScreenStatusCode	            = " & formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenStatusCode"), ",", "|") & "|", "", "||") & ", " & _
               "ProcessingScreenSubStatusCode	        = " & formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenSubStatusCode"), ",", "|") & "|", "", "||") & ", " & _
               "ProcessingScreenFormAction              = " & formatField(HttpContext.Current.Request("txtProcessingScreenFormAction"), "", "") & ", " & _
               "ProcessingScreenFormActionParameters    = " & formatField(HttpContext.Current.Request("txtProcessingScreenFormActionParameters"), "", "") & ", " & _
               "ProcessingScreenUserLevel               = " & formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenUserLevel"), ",", "|") & "|", "", "||") & " " & _
               "WHERE ProcessingScreenID                = '" & ProcessingScreenID & "' " & _
               "AND CompanyID                           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(ProcessingScreenName)) Then
                strQry = "INSERT INTO tblprocessingscreens (CompanyID, ProcessingScreenName, ProcessingScreenGradeID, ProcessingScreenProductType, ProcessingScreenStatusCode, ProcessingScreenSubStatusCode, ProcessingScreenFormAction, ProcessingScreenFormActionParameters, ProcessingScreenUserLevel) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ProcessingScreenName, "T", "") & ", " & _
                   formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenGradeID"), ",", "|") & "|", "", "NULL") & ", " & _
                   formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenProductType"), ",", "|") & "|", "", "NULL") & ", " & _
                   formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
                   formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenSubStatusCode"), ",", "|") & "|", "", "NULL") & ", " & _
                   formatField(HttpContext.Current.Request("txtProcessingScreenFormAction"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtProcessingScreenFormActionParameters"), "", "") & ", " & _
                   formatField("|" & Replace(HttpContext.Current.Request("lstProcessingScreenUserLevel"), ",", "|") & "|", "", "NULL") & ") "
                ProcessingScreenID = executeIdentityQuery(strQry)
            End If
        End If
        strQry = "DELETE FROM tblprocessingscreentemplates WHERE ProcessingScreenTemplateProcessingScreenID = '" & ProcessingScreenID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        If checkValue(HttpContext.Current.Request("AssignedTemplates")) Then
            Dim arrAssignedTemplates As Array = Split(HttpContext.Current.Request("AssignedTemplates"), ",")
            Dim arrOptions As Array
            For Each Item As String In arrAssignedTemplates
                arrOptions = Split(HttpContext.Current.Request("Options" & Item), "|")
                strQry = "INSERT INTO tblprocessingscreentemplates (CompanyID, ProcessingScreenTemplateProcessingScreenID, ProcessingScreenTemplateApplicationTemplateID, ProcessingScreenTemplateStatusCode, ProcessingScreenTemplateSubStatusCode, ProcessingScreenTemplateCSSClass, ProcessingScreenTemplateDisplay, ProcessingScreenTemplateColumn, ProcessingScreenTemplateHeight, ProcessingScreenTemplateOrder) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ProcessingScreenID, "N", 0) & ", " & _
                   formatField(Item, "N", 0) & ", " & _
                   formatField(Replace(arrOptions(3), ",", "|"), "", "||") & ", " & _
                   formatField(Replace(arrOptions(4), ",", "|"), "", "||") & ", " & _
                   formatField(arrOptions(0), "", "") & ", " & _
                   formatField(arrOptions(1), "", "block") & ", " & _
                   formatField(arrOptions(2), "N", 0) & ", " & _
                   formatField(arrOptions(5), "N", 0) & ", " & _
                   formatField(arrOptions(6), "N", 0) & ") "
                executeNonQuery(strQry)
            Next
        End If
        cacheDependency(CacheObject, "tblprocessingscreens,tblprocessingscreentemplates,vwassignedtemplates")
    End Sub

    Private Sub saveProcessingScreenDelete()
        Dim ProcessingScreenID As String = HttpContext.Current.Request("ProcessingScreenID")
        Dim strQry As String = ""
        If (checkValue(ProcessingScreenID)) Then
            strQry = "DELETE FROM tblprocessingscreens WHERE ProcessingScreenID = '" & ProcessingScreenID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblprocessingscreens")
        End If
    End Sub

    Private Sub saveProcessingScreenActive()
        Dim ProcessingScreenID As String = HttpContext.Current.Request("ProcessingScreenID")
        Dim ProcessingScreenActive As Boolean = HttpContext.Current.Request("ProcessingScreenActive")
        Dim strQry As String = ""
        If (checkValue(ProcessingScreenID)) Then
            If (ProcessingScreenActive) Then
                strQry = "UPDATE tblprocessingscreens SET  " & _
                   "ProcessingScreenActive	    = 0 " & _
                   "WHERE ProcessingScreenID    = '" & ProcessingScreenID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblprocessingscreens SET  " & _
                   "ProcessingScreenActive	    = 1 " & _
                   "WHERE ProcessingScreenID	= '" & ProcessingScreenID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblprocessingscreens")
        End If
    End Sub

    Private Sub saveDashboard()
        Dim DashboardID As String = HttpContext.Current.Request("DashboardID")
        Dim DashboardName As String = HttpContext.Current.Request("txtDashboardName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(DashboardID)) Then
            strQry = "UPDATE tbldashboards SET  " & _
               "DashboardName	                = " & formatField(DashboardName, "T", "") & ", " & _
               "DashboardDefault	            = " & formatField(HttpContext.Current.Request("cbDashboardDefault"), "B", 0) & ", " & _
               "DashboardTicker	                = " & formatField(HttpContext.Current.Request("cbDashboardTicker"), "B", 0) & ", " & _
               "DashboardJavascriptFunctions    = " & formatField(HttpContext.Current.Request("txtDashboardJavascriptFunctions"), "", "") & " " & _
               "WHERE DashboardID               = '" & DashboardID & "' " & _
               "AND CompanyID                   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(DashboardName)) Then
                strQry = "INSERT INTO tbldashboards (CompanyID, DashboardName, DashboardDefault, DashboardTicker, DashboardJavascriptFunctions) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(DashboardName, "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("cbDashboardDefault"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbDashboardTicker"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtDashboardJavascriptFunctions"), "", "") & ") "
                DashboardID = executeIdentityQuery(strQry)
            End If
        End If
        strQry = "DELETE FROM tbldashboardpanelmappings WHERE DashboardPanelMappingDashboardID = '" & DashboardID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        If checkValue(HttpContext.Current.Request("AssignedPanels")) Then
            Dim arrAssignedPanels As Array = Split(HttpContext.Current.Request("AssignedPanels"), ",")
            Dim arrOptions As Array
            For Each Item As String In arrAssignedPanels
                arrOptions = Split(HttpContext.Current.Request("Options" & Item), "|")
                strQry = "INSERT INTO tbldashboardpanelmappings (CompanyID, DashboardPanelMappingDashboardID, DashboardPanelMappingDashboardPanelID, DashboardPanelMappingCSSClass, DashboardPanelMappingDisplay, DashboardPanelMappingColumn) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(DashboardID, "N", 0) & ", " & _
                   formatField(Item, "N", 0) & ", " & _
                   formatField(arrOptions(0), "", "") & ", " & _
                   formatField(arrOptions(1), "", "block") & ", " & _
                   formatField(arrOptions(2), "N", 0) & ") "
                executeNonQuery(strQry)
            Next
        End If
        cacheDependency(CacheObject, "tbldashboards,vwdashboard,vwdashboardpanel")
    End Sub

    Private Sub saveDashboardDelete()
        Dim DashboardID As String = HttpContext.Current.Request("DashboardID")
        Dim strQry As String = ""
        If (checkValue(DashboardID)) Then
            strQry = "DELETE FROM tbldashboards WHERE DashboardID = '" & DashboardID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tbldashboards,vwdashboard,vwdashboardpanel")
        End If
    End Sub

    Private Sub saveDashboardActive()
        Dim DashboardID As String = HttpContext.Current.Request("DashboardID")
        Dim DashboardActive As Boolean = HttpContext.Current.Request("DashboardActive")
        Dim strQry As String = ""
        If (checkValue(DashboardID)) Then
            If (DashboardActive) Then
                strQry = "UPDATE tbldashboards SET  " & _
                   "DashboardActive	    = 0 " & _
                   "WHERE DashboardID    = '" & DashboardID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tbldashboards SET  " & _
                   "DashboardActive	    = 1 " & _
                   "WHERE DashboardID	= '" & DashboardID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tbldashboards,vwdashboard,vwdashboardpanel")
        End If
    End Sub

    Private Sub saveDashboardPanel()
        Dim DashboardPanelID As String = HttpContext.Current.Request("DashboardPanelID")
        Dim DashboardPanelName As String = HttpContext.Current.Request("txtDashboardPanelName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(DashboardPanelID)) Then
            strQry = "UPDATE tbldashboardpanels SET  " & _
               "DashboardPanelName	    = " & formatField(DashboardPanelName, "", "") & ", " & _
               "DashboardPanelRefreshMS	= " & formatField(HttpContext.Current.Request("txtDashboardPanelRefreshMS"), "N", 0) & " " & _
               "WHERE DashboardPanelID  = '" & DashboardPanelID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(DashboardPanelName)) Then
                strQry = "INSERT INTO tbldashboardpanels (CompanyID, DashboardPanelName, DashboardPanelRefreshMS) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(DashboardPanelName, "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtDashboardPanelRefreshMS"), "N", 0) & ") "
                DashboardPanelID = executeIdentityQuery(strQry)
            End If
        End If
        strQry = "DELETE FROM tbldashboardpanelstatistics WHERE DashboardPanelStatisticDashboardPanelID = '" & DashboardPanelID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        If checkValue(HttpContext.Current.Request("AssignedStatistics")) Then
            Dim arrAssignedStatistics As Array = Split(HttpContext.Current.Request("AssignedStatistics"), ",")
            For Each Item As String In arrAssignedStatistics
                strQry = "INSERT INTO tbldashboardpanelstatistics (CompanyID, DashboardPanelStatisticDashboardPanelID, DashboardPanelStatisticStatisticID) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(DashboardPanelID, "N", 0) & ", " & _
                   formatField(Item, "N", 0) & ") "
                executeNonQuery(strQry)
            Next
        End If
        cacheDependency(CacheObject, "tbldashboardpanels,vwdashboard,vwdashboardpanel,vwassignedstatistics")
    End Sub

    Private Sub saveDashboardPanelDelete()
        Dim DashboardPanelID As String = HttpContext.Current.Request("DashboardPanelID")
        Dim strQry As String = ""
        If (checkValue(DashboardPanelID)) Then
            strQry = "DELETE FROM tbldashboardpanels WHERE DashboardPanelID = '" & DashboardPanelID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tbldashboardpanels,vwdashboard,vwdashboardpanel,vwassignedstatistics")
        End If
    End Sub

    Private Sub saveDashboardPanelActive()
        Dim DashboardPanelID As String = HttpContext.Current.Request("DashboardPanelID")
        Dim DashboardPanelActive As Boolean = HttpContext.Current.Request("DashboardPanelActive")
        Dim strQry As String = ""
        If (checkValue(DashboardPanelID)) Then
            If (DashboardPanelActive) Then
                strQry = "UPDATE tbldashboardpanels SET  " & _
                   "DashboardPanelActive	= 0 " & _
                   "WHERE DashboardPanelID  = '" & DashboardPanelID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tbldashboardpanels SET  " & _
                   "DashboardPanelActive	= 1 " & _
                   "WHERE DashboardPanelID	= '" & DashboardPanelID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tbldashboardpanels,vwdashboard,vwdashboardpanel,vwassignedstatistics")
        End If
    End Sub

    Private Sub saveScript()
        Dim ScriptID As String = HttpContext.Current.Request("ScriptID")
        Dim AdditionalFieldID As String = getAnyField("AdditionalFieldID", "tblapplicationpageadditionalfields", "AdditionalFieldScriptID", ScriptID)
        Dim ScriptText As String = HttpContext.Current.Request("txtScriptText")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ScriptID)) Then
            strQry = "UPDATE tblapplicationscripts SET  " & _
               "ScriptText		        = " & formatField(ScriptText, "", "") & ", " & _
               "ScriptInputTypeID       = " & formatField(HttpContext.Current.Request("ddScriptInputTypeID"), "N", 0) & ", " & _
               "ScriptPriorityID        = " & formatField(HttpContext.Current.Request("ddScriptPriorityID"), "N", 0) & ", " & _
               "ScriptDropdownFunction	= " & formatField(HttpContext.Current.Request("txtScriptDropdownFunction"), "", "") & ", " & _
               "ScriptMandatory         = " & formatField(HttpContext.Current.Request("cbScriptMandatory"), "B", 0) & " " & _
               "WHERE ScriptID	        = '" & ScriptID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
            If (checkValue(AdditionalFieldID)) Then
                strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                   "AdditionalFieldName	    = " & formatField(HttpContext.Current.Request("txtAdditionalFieldName"), "", "") & " " & _
                   "WHERE AdditionalFieldID	= '" & AdditionalFieldID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
            End If
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(ScriptText)) Then
                strQry = "INSERT INTO tblapplicationscripts (CompanyID, ScriptText, ScriptInputTypeID, ScriptPriorityID, ScriptDropdownFunction, ScriptMandatory) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(ScriptText, "", "") & ", " & _
                            formatField(HttpContext.Current.Request("ddScriptInputTypeID"), "N", 0) & ", " & _
                            formatField(HttpContext.Current.Request("ddScriptPriorityID"), "N", 0) & ", " & _
                            formatField(HttpContext.Current.Request("txtScriptDropdownFunction"), "", "") & ", " & _
                            formatField(HttpContext.Current.Request("cbScriptMandatory"), "B", 0) & ")"
                ScriptID = executeIdentityQuery(strQry)
            End If
            If (checkValue(HttpContext.Current.Request("txtAdditionalFieldName"))) Then
                strQry = "INSERT INTO tblapplicationpageadditionalfields (CompanyID, AdditionalFieldName, AdditionalFieldType, AdditionalFieldHTML, AdditionalFieldScriptID) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldName"), "T", "") & ", " & _
                    formatField(2, "N", 2) & ", " & _
                    formatField("", "", "") & ", " & _
                    formatField(ScriptID, "N", 0) & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveScriptDelete()
        Dim ScriptID As String = HttpContext.Current.Request("ScriptID")
        Dim AdditionalFieldID As String = getAnyField("AdditionalFieldID", "tblapplicationpageadditionalfields", "AdditionalFieldScriptID", ScriptID)
        Dim strQry As String = ""
        If (checkValue(ScriptID)) Then
            strQry = "DELETE FROM tblapplicationscripts WHERE ScriptID = '" & ScriptID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        If (checkValue(AdditionalFieldID)) Then
            strQry = "DELETE FROM tblapplicationpageadditionalfields WHERE AdditionalFieldID = '" & AdditionalFieldID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveScriptActive()
        Dim ScriptID As String = HttpContext.Current.Request("ScriptID")
        Dim AdditionalFieldID As String = getAnyField("AdditionalFieldID", "tblapplicationpageadditionalfields", "AdditionalFieldScriptID", ScriptID)
        Dim ScriptActive As Boolean = HttpContext.Current.Request("ScriptActive")
        Dim strQry As String = ""
        If (checkValue(ScriptID)) Then
            If (ScriptActive) Then
                strQry = "UPDATE tblapplicationscripts SET  " & _
                   "ScriptActive	        = 0 " & _
                   "WHERE ScriptID	        = '" & ScriptID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
                strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                   "AdditionalFieldActive	= 0 " & _
                   "WHERE AdditionalFieldID	= '" & AdditionalFieldID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblapplicationscripts SET  " & _
                   "ScriptActive	        = 1 " & _
                   "WHERE ScriptID	        = '" & ScriptID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
                strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                   "AdditionalFieldActive	= 1 " & _
                   "WHERE AdditionalFieldID	= '" & AdditionalFieldID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
        End If
    End Sub

    Private Sub saveField()
        Dim FieldID As String = HttpContext.Current.Request("FieldID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(FieldID)) Then
            strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
               "AdditionalFieldName	                        = " & formatField(HttpContext.Current.Request("txtAdditionalFieldName"), "", "") & ", " & _
               "AdditionalFieldType	                        = " & formatField(HttpContext.Current.Request("ddAdditionalFieldType"), "N", 0) & ", " & _
               "AdditionalFieldHTML	                        = " & formatField(HttpContext.Current.Request("txtAdditionalFieldHTML"), "", "") & ", " & _
               "AdditionalFieldRoutine	                    = " & formatField(HttpContext.Current.Request("txtAdditionalFieldRoutine"), "", "NULL") & ", " & _
               "AdditionalFieldRoutineParameters            = " & formatField(HttpContext.Current.Request("txtAdditionalFieldRoutineParameters"), "", "") & ", " & _
               "AdditionalFieldButtonText	                = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonText"), "", "NULL") & ", " & _
               "AdditionalFieldButtonStatusCode	            = " & formatField(HttpContext.Current.Request("ddAdditionalFieldButtonStatusCode"), "U", "NULL") & ", " & _
               "AdditionalFieldButtonSubStatusCode	        = " & formatField(HttpContext.Current.Request("ddAdditionalFieldButtonSubStatusCode"), "U", "NULL") & ", " & _
               "AdditionalFieldButtonDateType	            = " & formatField(HttpContext.Current.Request("ddAdditionalFieldButtonDateType"), "", "") & ", " & _
               "AdditionalFieldButtonClearDateType	        = " & formatField(HttpContext.Current.Request("ddAdditionalFieldButtonClearDateType"), "", "") & ", " & _
               "AdditionalFieldButtonFormAction             = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonFormAction"), "", "NULL") & ", " & _
               "AdditionalFieldButtonFormActionParameters   = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonFormActionParameters"), "", "") & ", " & _
               "AdditionalFieldButtonSendEmails             = " & formatField(HttpContext.Current.Request("SendEmails"), "", "") & ", " & _
               "AdditionalFieldButtonSendSMS                = " & formatField(HttpContext.Current.Request("SendSMS"), "", "") & ", " & _
               "AdditionalFieldButtonCreateDiaries	        = " & formatField(HttpContext.Current.Request("CreateDiaries"), "", "") & ", " & _
               "AdditionalFieldButtonCompleteDiaries	    = " & formatField(HttpContext.Current.Request("CompleteDiaries"), "", "") & ", " & _
               "AdditionalFieldButtonCancelDiaries	        = " & formatField(HttpContext.Current.Request("CancelDiaries"), "", "") & ", " & _
               "AdditionalFieldButtonCreateReminders	    = " & formatField(HttpContext.Current.Request("CreateReminders"), "", "") & ", " & _
               "AdditionalFieldButtonCreateRemindersUserID	= " & formatField(HttpContext.Current.Request("CreateRemindersUserID"), "", "") & ", " & _
               "AdditionalFieldButtonShowNote	            = " & formatField(HttpContext.Current.Request("cbAdditionalFieldButtonShowNote"), "B", 0) & ", " & _
               "AdditionalFieldButtonNote	                = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonNote"), "", "") & ", " & _
               "AdditionalFieldButtonSocialPost	            = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonSocialPost"), "", "") & ", " & _
               "AdditionalFieldButtonPanelID	            = " & formatField(HttpContext.Current.Request("ddAdditionalFieldButtonPanelID"), "N", "NULL") & ", " & _
               "AdditionalFieldButtonPanelWidth	            = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonPanelWidth"), "N", 630) & ", " & _
               "AdditionalFieldButtonPanelHeight	        = " & formatField(HttpContext.Current.Request("txtAdditionalFieldButtonPanelHeight"), "N", 440) & " " & _
               "WHERE AdditionalFieldID	                    = '" & FieldID & "' " & _
               "AND CompanyID                               = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(HttpContext.Current.Request("txtAdditionalFieldName"))) Then
                strQry = "INSERT INTO tblapplicationpageadditionalfields (CompanyID, AdditionalFieldName, AdditionalFieldType, AdditionalFieldHTML, AdditionalFieldRoutine, AdditionalFieldRoutineParameters," & _
                    "AdditionalFieldButtonText, AdditionalFieldButtonStatusCode, AdditionalFieldButtonSubStatusCode, AdditionalFieldButtonDateType, AdditionalFieldButtonClearDateType, AdditionalFieldButtonFormAction, AdditionalFieldButtonFormActionParameters, AdditionalFieldButtonSendEmails, AdditionalFieldButtonSendSMS, AdditionalFieldButtonCreateDiaries, AdditionalFieldButtonCompleteDiaries, AdditionalFieldButtonCancelDiaries, AdditionalFieldButtonCreateReminders, AdditionalFieldButtonCreateRemindersUserID, AdditionalFieldButtonShowNote, AdditionalFieldButtonNote, AdditionalFieldButtonSocialPost, AdditionalFieldButtonPanelID, AdditionalFieldButtonPanelWidth, AdditionalFieldButtonPanelHeight) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldName"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldType"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldHTML"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldRoutine"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldRoutineParameters"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonText"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldButtonStatusCode"), "U", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldButtonSubStatusCode"), "U", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldButtonDateType"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldButtonClearDateType"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonFormAction"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonFormActionParameters"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("SendEmails"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("SendSMS"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CreateDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CompleteDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CancelDiaries"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CreateReminders"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("CreateRemindersUserID"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("cbAdditionalFieldButtonShowNote"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonNote"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonSocialPost"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddAdditionalFieldButtonPanelID"), "N", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonPanelWidth"), "N", 630) & ", " & _
                    formatField(HttpContext.Current.Request("txtAdditionalFieldButtonPanelHeight"), "N", 440) & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveFieldDelete()
        Dim FieldID As String = HttpContext.Current.Request("FieldID")
        Dim strQry As String = ""
        If (checkValue(FieldID)) Then
            strQry = "DELETE FROM tblapplicationpageadditionalfields WHERE AdditionalFieldID = '" & FieldID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveFieldActive()
        Dim FieldID As String = HttpContext.Current.Request("FieldID")
        Dim FieldActive As Boolean = HttpContext.Current.Request("FieldActive")
        Dim strQry As String = ""
        If (checkValue(FieldID)) Then
            If (FieldActive) Then
                strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                   "AdditionalFieldActive	= 0 " & _
                   "WHERE AdditionalFieldID	= '" & FieldID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                   "AdditionalFieldActive	= 1 " & _
                   "WHERE AdditionalFieldID	= '" & FieldID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblapplicationpageadditionalfields")
        End If
    End Sub

    Private Sub saveStatistic()
        Dim StatisticID As String = HttpContext.Current.Request("StatisticID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(StatisticID)) Then
            strQry = "UPDATE tblstatistics SET  " & _
               "StatisticName	                    = " & formatField(HttpContext.Current.Request("txtStatisticName"), "", "") & ", " & _
               "StatisticTitle	                    = " & formatField(HttpContext.Current.Request("txtStatisticTitle"), "", "") & ", " & _
               "StatisticSubTitle	                = " & formatField(HttpContext.Current.Request("txtStatisticSubTitle"), "", "") & ", " & _
               "StatisticType	                    = " & formatField(HttpContext.Current.Request("ddStatisticType"), "N", 0) & ", " & _
               "StatisticRoutine	                = " & formatField(HttpContext.Current.Request("txtStatisticRoutine"), "", "") & ", " & _
               "StatisticText	                    = " & formatField(HttpContext.Current.Request("txtStatisticText"), "", "") & ", " & _
               "StatisticSQL	                    = " & formatField(HttpContext.Current.Request("txtStatisticSQL"), "", "") & ", " & _
               "StatisticReportBuilderID            = " & formatField(HttpContext.Current.Request("ddStatisticReportBuilderID"), "N", 0) & ", " & _
               "StatisticReportBuilderQueryString	= " & formatField(HttpContext.Current.Request("txtStatisticReportBuilderQueryString"), "", "") & ", " & _
               "StatisticJavascriptFunctions	    = " & formatField(HttpContext.Current.Request("txtStatisticJavascriptFunctions"), "", "") & ", " & _
               "StatisticShowTickerMS	            = " & formatField(HttpContext.Current.Request("txtStatisticShowTickerMS"), "N", 0) & ", " & _
               "StatisticMinValue	                = " & formatField(HttpContext.Current.Request("txtStatisticMinValue"), "N", 0) & ", " & _
               "StatisticMaxValue	                = " & formatField(HttpContext.Current.Request("txtStatisticMaxValue"), "N", 0) & ", " & _
               "StatisticTargetValue	            = " & formatField(HttpContext.Current.Request("txtStatisticTargetValue"), "N", 0) & ", " & _
               "StatisticCachedMinutes	            = " & formatField(HttpContext.Current.Request("txtStatisticCachedMinutes"), "N", 0) & " " & _
               "WHERE StatisticID	                = '" & StatisticID & "' " & _
               "AND CompanyID                       = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(HttpContext.Current.Request("txtStatisticName"))) Then
                strQry = "INSERT INTO tblstatistics (CompanyID, StatisticName, StatisticTitle, StatisticSubTitle, StatisticType, StatisticRoutine, StatisticText, StatisticSQL, StatisticReportBuilderID, StatisticReportBuilderQueryString, StatisticJavascriptFunctions, StatisticShowTickerMS, StatisticMinValue, StatisticMaxValue, StatisticTargetValue, StatisticCachedMinutes) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticName"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticTitle"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticSubTitle"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddStatisticType"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticRoutine"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticText"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticSQL"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddStatisticReportBuilderID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticReportBuilderQueryString"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticJavascriptFunctions"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticShowTickerMS"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticMinValue"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticMaxValue"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticTargetValue"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatisticCachedMinutes"), "N", 0) & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "vwdashboard,vwdashboardpanel,tblstatistics,vwassignedstatistics")
    End Sub

    Private Sub saveStatisticDelete()
        Dim StatisticID As String = HttpContext.Current.Request("StatisticID")
        Dim strQry As String = ""
        If (checkValue(StatisticID)) Then
            strQry = "DELETE FROM tblstatistics WHERE StatisticID = '" & StatisticID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "vwdashboard,vwdashboardpanel,tblstatistics,vwassignedstatistics")
    End Sub

    Private Sub saveStatisticActive()
        Dim StatisticID As String = HttpContext.Current.Request("StatisticID")
        Dim StatisticActive As Boolean = HttpContext.Current.Request("StatisticActive")
        Dim strQry As String = ""
        If (checkValue(StatisticID)) Then
            If (StatisticActive) Then
                strQry = "UPDATE tblstatistics SET  " & _
                   "StatisticActive     = 0 " & _
                   "WHERE StatisticID	= '" & StatisticID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblstatistics SET  " & _
                   "StatisticActive	    = 1 " & _
                   "WHERE StatisticID	= '" & StatisticID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "vwdashboard,vwdashboardpanel,tblstatistics,vwassignedstatistics")
        End If
    End Sub

    Private Sub saveValidation()
        Dim ValID As String = HttpContext.Current.Request("ValID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ValID)) Then
            strQry = "UPDATE tblimportvalidation SET  " & _
               "ValName	                        = " & formatField(HttpContext.Current.Request("txtValName"), "", "") & ", " & _
               "ValColumn	                    = " & formatField(HttpContext.Current.Request("txtValName"), "", "") & ", " & _
               "ValType	                        = " & formatField(HttpContext.Current.Request("ddValType"), "U", "NULL") & ", " & _
               "ValRegex	                    = " & formatField(HttpContext.Current.Request("txtValRegEx"), "", "NULL") & ", " & _
               "ValRegexMessage                 = " & formatField(HttpContext.Current.Request("txtValRegExMessage"), "", "NULL") & ", " & _
               "ValFunction	                    = " & formatField(HttpContext.Current.Request("txtValFunction"), "", "NULL") & ", " & _
               "ValDefault	                    = " & formatField(HttpContext.Current.Request("txtValDefault"), "", "NULL") & ", " & _
               "ApplicationFriendlyName	        = " & formatField(HttpContext.Current.Request("txtApplicationFriendlyName"), "", "") & ", " & _
               "ApplicationInputType	        = " & formatField(HttpContext.Current.Request("ddApplicationInputType"), "N", "1") & ", " & _
               "ApplicationDropdownFunction	    = " & formatField(HttpContext.Current.Request("txtApplicationDropdownFunction"), "", "NULL") & ", " & _
               "ApplicationDropdownValues	    = " & formatField(HttpContext.Current.Request("txtApplicationDropdownValues"), "", "NULL") & ", " & _
               "ApplicationDataGridColumn	    = " & formatField(HttpContext.Current.Request("cbApplicationDataGridColumn"), "B", 0) & ", " & _
               "ApplicationDataGridTableClass	= " & formatField(HttpContext.Current.Request("txtApplicationDataGridTableClass"), "", "") & ", " & _
               "ApplicationDataGridColumnClass	= " & formatField(HttpContext.Current.Request("txtApplicationDataGridColumnClass"), "", "") & ", " & _
               "ApplicationTooltip	            = " & formatField(HttpContext.Current.Request("txtApplicationTooltip"), "", "") & " " & _
               "WHERE ValID	                    = '" & ValID & "' " & _
               "AND CompanyID                   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(HttpContext.Current.Request("txtValName"))) Then
                strQry = "INSERT INTO tblimportvalidation (CompanyID, ValName, ValTable, ValColumn, ValType, ValRegex, ValRegexMessage, ValFunction, ValDefault, " & _
                    "ApplicationFriendlyName, ApplicationInputType, ApplicationDropdownFunction, ApplicationDropdownValues, ApplicationDataGridColumn, ApplicationDataGridTableClass, ApplicationDataGridColumnClass, ApplicationTooltip, ValStatus) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtValName"), "", "") & ", " & _
                    formatField("tbldatastore", "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtValName"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddValType"), "U", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtValRegEx"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtValRegExMessage"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtValFunction"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtValDefault"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationFriendlyName"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddApplicationInputType"), "N", "1") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationDropdownFunction"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationDropdownValues"), "", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("cbApplicationDataGridColumn"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationDataGridTableClass"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationDataGridColumnClass"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtApplicationTooltip"), "", "") & ", " & _
                    formatField("A", "U", "A") & ")"
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblimportvalidation,tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveValidationDelete()
        Dim ValID As String = HttpContext.Current.Request("ValID")
        Dim strQry As String = ""
        If (checkValue(ValID)) Then
            strQry = "DELETE FROM tblimportvalidation WHERE ValID = '" & ValID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblimportvalidation,tblapplicationpageadditionalfields")
    End Sub

    Private Sub saveValidationActive()
        Dim ValID As String = HttpContext.Current.Request("ValID")
        Dim ValStatus As String = HttpContext.Current.Request("ValStatus")
        Dim strQry As String = ""
        If (checkValue(ValID)) Then
            If (ValStatus = "A") Then
                strQry = "UPDATE tblimportvalidation SET  " & _
                   "ValStatus	    = 'D' " & _
                   "WHERE ValID	    = '" & ValID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblimportvalidation SET  " & _
                   "ValStatus	    = 'A' " & _
                   "WHERE ValID	    = '" & ValID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblimportvalidation,tblapplicationpageadditionalfields")
        End If
    End Sub

    Private Sub saveStatus()
        Dim StatusID As String = HttpContext.Current.Request("StatusID")
        Dim strStatusCode As String = HttpContext.Current.Request("txtStatusCode")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(StatusID)) Then
            strQry = "UPDATE tblstatuses SET  " & _
               "StatusCode	        = " & formatField(strStatusCode, "U", "") & ", " & _
               "StatusDescription	= " & formatField(HttpContext.Current.Request("txtStatusDescription"), "T", "") & ", " & _
               "StatusSale	        = " & formatField(HttpContext.Current.Request("cbStatusSale"), "B", 0) & ", " & _
               "StatusOrder	        = " & formatField(HttpContext.Current.Request("txtStatusOrder"), "N", 0) & " " & _
               "WHERE StatusID	= '" & StatusID & "' " & _
               "AND CompanyID   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(strStatusCode)) Then
                strQry = "INSERT INTO tblstatuses (CompanyID, StatusCode, StatusDescription, StatusSale, StatusOrder) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(strStatusCode, "U", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtStatusDescription"), "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("cbStatusSale"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtStatusOrder"), "N", 0) & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveStatusDelete()
        Dim StatusID As String = HttpContext.Current.Request("StatusID")
        Dim strQry As String = ""
        If (checkValue(StatusID)) Then
            strQry = "DELETE FROM tblstatuses WHERE StatusID = '" & StatusID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveStatusActive()
        Dim StatusID As String = HttpContext.Current.Request("StatusID")
        Dim StatusActive As Boolean = HttpContext.Current.Request("StatusActive")
        Dim strQry As String = ""
        If (checkValue(StatusID)) Then
            If (StatusActive) Then
                strQry = "UPDATE tblstatuses SET  " & _
                   "StatusActive    = 0 " & _
                   "WHERE StatusID	= '" & StatusID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblstatuses SET  " & _
                   "StatusActive	= 1 " & _
                   "WHERE StatusID	= '" & StatusID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveLender()
        Dim LenderID As String = HttpContext.Current.Request("LenderID")
        Dim strLenderType As String = HttpContext.Current.Request("ddLenderType")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        Dim intNewCompany As String = "0"
        If (checkValue(lenderID)) Then
            strQry = "UPDATE tbllenders SET  "
            If HttpContext.Current.Request("cbAllCompanies") = "on" Then
                strQry += "CompanyID   = 0,"
            Else
                strQry += "CompanyID   = '" & CompanyID & "',"
            End If
            strQry += _
               "LenderName	    = " & formatField(HttpContext.Current.Request("txtLenderName"), "T", "") & ", " & _
               "LenderType	    = " & formatField(HttpContext.Current.Request("ddLenderType"), "T", "") & " " & _
               "WHERE LenderID	= '" & LenderID & "' "
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(strLenderType)) Then
                If HttpContext.Current.Request("cbAllCompanies") <> "on" Then
                    intNewCompany = CompanyID
                End If
                strQry = "INSERT INTO tbllenders (CompanyID, LenderName, LenderType) " & _
                        "VALUES(" & formatField(intNewCompany, "N", 0) & ", " & _
                         formatField(HttpContext.Current.Request("txtLenderName"), "T", 0) & ", " & _
                         formatField(HttpContext.Current.Request("ddLenderType"), "T", "") & ") "

                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tbllenders")
    End Sub

    Private Sub saveLenderDelete()
        Dim LenderID As String = HttpContext.Current.Request("LenderID")
        Dim strQry As String = ""
        If (checkValue(LenderID)) Then
            strQry = "DELETE FROM tbllenders WHERE LenderID = '" & LenderID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tbllenders")
    End Sub

    Private Sub saveLenderActive()
        Dim LenderID As String = HttpContext.Current.Request("LenderID")
        Dim LenderActive As Boolean = HttpContext.Current.Request("LenderActive")
        Dim strQry As String = ""	
        If (checkValue(LenderID)) Then
            If (LenderActive) Then
                strQry = "UPDATE tbllenders SET  " & _
                   "LenderActive    = 0 " & _
                   "WHERE LenderID	= '" & LenderID & "' "
            Else
                strQry = "UPDATE tblLenders SET  " & _
                   "LenderActive	= 1 " & _
                   "WHERE LenderID	= '" & LenderID & "' " 
            End If			
			
            Call executeNonQuery(strQry)
		
        End If
        cacheDependency(CacheObject, "tbllenders")
    End Sub
	
    Private Sub saveSubStatus()
        Dim SubStatusID As String = HttpContext.Current.Request("SubStatusID")
        Dim strSubStatusCode As String = HttpContext.Current.Request("txtSubStatusCode")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(SubStatusID)) Then
            strQry = "UPDATE tblsubstatuses SET  " & _
               "SubStatusCode	        = " & formatField(strSubStatusCode, "U", "") & ", " & _
               "SubStatusDescription	= " & formatField(HttpContext.Current.Request("txtSubStatusDescription"), "T", "") & " " & _
               "WHERE SubStatusID	    = '" & SubStatusID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(strSubStatusCode)) Then
                strQry = "INSERT INTO tblsubstatuses (CompanyID, SubStatusCode, SubStatusDescription) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(strSubStatusCode, "U", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtSubStatusDescription"), "T", "") & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveSubStatusDelete()
        Dim SubStatusID As String = HttpContext.Current.Request("SubStatusID")
        Dim strQry As String = ""
        If (checkValue(SubStatusID)) Then
            strQry = "DELETE FROM tblsubstatuses WHERE SubStatusID = '" & SubStatusID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveSubStatusActive()
        Dim SubStatusID As String = HttpContext.Current.Request("SubStatusID")
        Dim SubStatusActive As Boolean = HttpContext.Current.Request("SubStatusActive")
        Dim strQry As String = ""
        If (checkValue(SubStatusID)) Then
            If (SubStatusActive) Then
                strQry = "UPDATE tblsubstatuses SET  " & _
                   "SubStatusActive     = 0 " & _
                   "WHERE SubStatusID	= '" & SubStatusID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblsubstatuses SET  " & _
                   "SubStatusActive	    = 1 " & _
                   "WHERE SubStatusID	= '" & SubStatusID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
    End Sub

    Private Sub saveStatusMapping()
        Dim StatusID As String = HttpContext.Current.Request("StatusID")
        Dim strSubStatuses As String = HttpContext.Current.Request("lstSubStatuses")
        Dim strQry As String = ""
        If (checkValue(StatusID)) Then
            strQry = "DELETE FROM tblstatusmapping WHERE StatusID = '" & StatusID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(strSubStatuses)) Then
                Dim arrSubStatuses As Array = Split(strSubStatuses, ",")
                For i As Integer = 0 To UBound(arrSubStatuses)
                    strQry = "INSERT INTO tblstatusmapping (CompanyID, StatusID, SubStatusID) " & _
                                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                formatField(StatusID, "N", 0) & ", " & _
                                formatField(arrSubStatuses(i), "N", 0) & ") "
                    executeNonQuery(strQry)
                Next
            End If
            cacheDependency(CacheObject, "tblstatuses,tblsubstatuses,tblstatusmapping,vwsubstatusgroups")
        End If
    End Sub

    Private Sub saveSubStatusGroup()
        Dim SubStatusGroupID As String = HttpContext.Current.Request("SubStatusGroupID")
        Dim SubStatusGroupName As String = HttpContext.Current.Request("txtSubStatusGroupName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(SubStatusGroupID)) Then
            strQry = "UPDATE tblsubstatusgroups SET  " & _
               "SubStatusGroupName	    = " & formatField(SubStatusGroupName, "T", "") & " " & _
               "WHERE SubStatusGroupID	= '" & SubStatusGroupID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(SubStatusGroupName)) Then
                strQry = "INSERT INTO tblsubstatusgroups (CompanyID, SubStatusGroupName) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(SubStatusGroupName, "T", "") & ") "
                SubStatusGroupID = executeIdentityQuery(strQry)
            End If
        End If
        strQry = "DELETE FROM tblsubstatusgroupmapping WHERE SubStatusGroupID = '" & SubStatusGroupID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        If (checkValue(HttpContext.Current.Request("lstSubStatuses"))) Then
            Dim arrSubStatuses As Array
            arrSubStatuses = Split(HttpContext.Current.Request("lstSubStatuses"), ",")
            For Each Item As String In arrSubStatuses
                strQry = "INSERT INTO tblsubstatusgroupmapping(CompanyID, SubStatusGroupID, SubStatusID) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(SubStatusGroupID, "N", 0) & ", " & _
                    formatField(Item, "N", 0) & ")"
                executeNonQuery(strQry)
            Next
        End If
		cacheDependency(CacheObject, "vwsubstatusgroups")
    End Sub

    Private Sub saveSubStatusGroupDelete()
        Dim SubStatusGroupID As String = HttpContext.Current.Request("SubStatusGroupID")
        Dim strQry As String = ""
        If (checkValue(SubStatusGroupID)) Then
            strQry = "DELETE FROM tblsubstatusgroups WHERE SubStatusGroupID = '" & SubStatusGroupID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            strQry = "DELETE FROM tblsubstatusgroupmapping WHERE SubStatusGroupID = '" & SubStatusGroupID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
		cacheDependency(CacheObject, "vwsubstatusgroups")
    End Sub

    Private Sub saveSubStatusGroupActive()
        Dim SubStatusGroupID As String = HttpContext.Current.Request("SubStatusGroupID")
        Dim SubStatusGroupActive As Boolean = HttpContext.Current.Request("SubStatusGroupActive")
        Dim strQry As String = ""
        If (checkValue(SubStatusGroupID)) Then
            If (SubStatusGroupActive) Then
                strQry = "UPDATE tblsubstatusgroups SET  " & _
                   "SubStatusGroupActive    = 0 " & _
                   "WHERE SubStatusGroupID	= '" & SubStatusGroupID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblsubstatusgroups SET  " & _
                   "SubStatusGroupActive	= 1 " & _
                   "WHERE SubStatusGroupID	= '" & SubStatusGroupID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
		cacheDependency(CacheObject, "vwsubstatusgroups")
    End Sub

    Private Sub saveUser()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim UserName As String = HttpContext.Current.Request("txtUserName")
        Dim intCompanyID As String = HttpContext.Current.Request("ddCompanyID")
        Dim strUserSuperAdmin As String = loggedInUserValue("UserSuperAdmin")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        Dim strUserPassword As String = HttpContext.Current.Request("txtUserPassword")
        If (HttpContext.Current.Request("cbGeneratePassword") = "on") Then
            strUserPassword = generatePassword(8)
            strReturnUrlMessage = encodeURL("Password generated was: " & strUserPassword)
        Else
            strReturnUrlMessage = encodeURL("User successfully updated")
        End If

        If (checkValue(UserID)) Then
            strQry = "UPDATE tblusers SET  " & _
               "UserReference	        = " & formatField(HttpContext.Current.Request("txtUserReference"), "U", "") & ", " & _
               "UserTeamID              = " & formatField(HttpContext.Current.Request("ddUserTeamID"), "N", 0) & ", " & _
               "UserTelephoneNumber	    = " & formatField(HttpContext.Current.Request("txtUserTelephoneNumber"), "", "") & ", " & _
               "UserEmailAddress	    = " & formatField(HttpContext.Current.Request("txtUserEmailAddress"), "", "") & ", " & _
               "UserName	            = " & formatField(UserName, "", "") & ", "
            If (checkValue(strUserPassword)) Then
                strQry += _
                    "UserPassword       = " & formatField(hashString256(LCase(UserName), strUserPassword), "", "") & ", "
            End If
            strQry += _
               "UserPasswordNoExpiry    = " & formatField(HttpContext.Current.Request("cbUserPasswordNoExpiry"), "B", 0) & ", " & _
               "UserFullName            = " & formatField(HttpContext.Current.Request("txtUserFullName"), "T", "") & " "
            If (strUserSuperAdmin = "True") Then
                strQry += ", UserSupplierCompanyID   = " & formatField(HttpContext.Current.Request("ddUserSupplierCompanyID"), "N", 0) & ", " & _
               "UserPartnerCompanyID    = " & formatField(HttpContext.Current.Request("ddUserPartnerCompanyID"), "N", 0) & " "
            End If
            strQry += ", CompanyID             	= " & formatField(CompanyID, "N", 0) & ", " & _
               "UserReports             = " & formatField(HttpContext.Current.Request("cbUserReports"), "B", 0) & ", " & _
               "UserCallCentreManager   = " & formatField(HttpContext.Current.Request("cbUserCallCentreManager"), "B", 0) & ", " & _
               "UserManager   			= " & formatField(HttpContext.Current.Request("cbUserManager"), "B", 0) & ", " & _
               "UserSeniorManager   	= " & formatField(HttpContext.Current.Request("cbUserSeniorManager"), "B", 0) & ", " & _
               "UserWorkflows           = " & formatField(HttpContext.Current.Request("cbUserWorkflows"), "B", 0) & ", " & _
               "UserCallCentre          = " & formatField(HttpContext.Current.Request("cbUserCallCentre"), "B", 0) & ", " & _
               "UserSales               = " & formatField(HttpContext.Current.Request("cbUserSales"), "B", 0) & ", " & _
               "UserAdministrator       = " & formatField(HttpContext.Current.Request("cbUserAdministrator"), "B", 0) & ", " & _
               "UserActiveLevelID       = " & formatField(HttpContext.Current.Request("ddUserActiveLevelID"), "N", 0) & ", " & _
               "UserClickToDial		    = " & formatField(HttpContext.Current.Request("cbUserClickToDial"), "B", 0) & " "
            If (strUserSuperAdmin = "True") Then
                strQry += ", UserRep	= " & formatField(HttpContext.Current.Request("cbUserRep"), "B", 0) & ", " & _
                    "UserRepPostCodeRange    = " & formatField("|" & Replace(HttpContext.Current.Request("lstUserRepPostCodeRange"), ",", "|") & "|", "", "NULL") & ", " & _
                    "UserRemoteAccess	    = " & formatField(HttpContext.Current.Request("cbUserRemoteAccess"), "B", 0) & ", " & _
                    "UserSuperAdmin          = " & formatField(HttpContext.Current.Request("cbUserSuperAdmin"), "B", 0) & ", " & _
                    "UserStaffAdmin          = " & formatField(HttpContext.Current.Request("cbUserStaffAdmin"), "B", 0) & ", " & _
                    "UserDataAdmin           = " & formatField(HttpContext.Current.Request("cbUserDataAdmin"), "B", 0) & ", " & _
                    "UserMediaAdmin          = " & formatField(HttpContext.Current.Request("cbUserMediaAdmin"), "B", 0) & ", " & _
                    "UserApplicationAdmin    = " & formatField(HttpContext.Current.Request("cbUserApplicationAdmin"), "B", 0) & ", " & _
                    "UserCallCentreAdmin     = " & formatField(HttpContext.Current.Request("cbUserCallCentreAdmin"), "B", 0) & ", " & _
                    "UserDialerAdmin         = " & formatField(HttpContext.Current.Request("cbUserDialerAdmin"), "B", 0) & " "
            End If

            strQry += "WHERE UserID     = '" & UserID & "' " & _
                "AND CompanyID          = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
            If (HttpContext.Current.Request("ddOutOfOfficeID") <> HttpContext.Current.Request("frmPrevOutOfOfficeID") And strFrmAction = "update") Then
                saveOutOfOffice(CompanyID, UserID, Config.DefaultUserID, HttpContext.Current.Request("ddOutOfOfficeID"))
            End If
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(UserName)) Then
                strQry = "INSERT INTO tblusers (UserReference, UserTeamID, UserTelephoneNumber, UserEmailAddress, UserName, UserPassword, UserPasswordNoExpiry, UserFullName "
                If (strUserSuperAdmin = "True") Then
                    strQry += ", UserSupplierCompanyID, UserPartnerCompanyID "
                End If
                strQry += ", CompanyID, UserReports, UserCallCentreManager, UserManager, UserSeniorManager, UserWorkflows, UserCallCentre, UserSales, UserAdministrator, UserActiveLevelID, UserClickToDial "
                If (strUserSuperAdmin = "True") Then
                    strQry += ", UserRep, UserRepPostCodeRange, UserRemoteAccess, UserSuperAdmin, UserStaffAdmin, UserDataAdmin, UserMediaAdmin, UserApplicationAdmin, UserCallCentreAdmin, UserDialerAdmin "
                End If
                strQry += ") " & _
                   "VALUES(" & formatField(HttpContext.Current.Request("txtUserReference"), "U", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddUserTeamID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtUserTelephoneNumber"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtUserEmailAddress"), "", "") & ", " & _
                   formatField(UserName, "", "") & ", " & _
                   formatField(encrypt(strUserPassword), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("cbUserPasswordNoExpiry"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtUserFullName"), "T", "") & " "
                If (strUserSuperAdmin = "True") Then
                    strQry += _
                        ", " & formatField(HttpContext.Current.Request("ddUserSupplierCompanyID"), "N", 0) & ", " & _
                        formatField(HttpContext.Current.Request("ddUserPartnerCompanyID"), "N", 0) & " "
                End If
                strQry += _
                    ", " & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserReports"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserCallCentreManager"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserManager"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserSeniorManager"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserWorkflows"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserCallCentre"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserSales"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserAdministrator"), "B", 0) & ", " & _
                    formatField(HttpContext.Current.Request("ddUserActiveLevelID"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("cbUserClickToDial"), "B", 0) & " "
                If (strUserSuperAdmin = "True") Then
                    strQry += _
                        ", " & formatField(HttpContext.Current.Request("cbUserRep"), "B", 0) & ", " & _
                        formatField("|" & Replace(HttpContext.Current.Request("lstUserRepPostCodeRange"), ",", "|") & "|", "", "NULL") & ", " & _
                        formatField(HttpContext.Current.Request("cbUserRemoteAccess"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserSuperAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserStaffAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserDataAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserMediaAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserApplicationAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserCallCentreAdmin"), "B", 0) & ", " & _
                        formatField(HttpContext.Current.Request("cbUserDialerAdmin"), "B", 0) & " "
                End If
                strQry += ") "
                executeNonQuery(strQry)
            End If
        End If
    End Sub

    Private Sub saveUserDelete()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tblusers WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveUserActive()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim UserActive As Boolean = HttpContext.Current.Request("UserActive")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            If (UserActive) Then
                strQry = "UPDATE tblusers SET  " & _
                   "UserActive		= 0 " & _
                   "WHERE UserID	= '" & UserID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblusers SET  " & _
                   "UserActive		= 1 " & _
                   "WHERE UserID	= '" & UserID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveUserLogout()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strQry As String = "UPDATE tblusers SET " & _
            "UserLoggedIn	                    = " & formatField(0, "N", 0) & ", " & _
            "UserSessionID	                    = " & formatField("", "", "") & ", " & _
            "UserActiveWorkflowTypeID           = " & formatField(0, "N", 0) & ", " & _
            "UserActiveWorkflowID               = " & formatField(0, "N", 0) & ", " & _
            "UserActiveWorkflowActivityID       = " & formatField(0, "N", 0) & ", " & _
            "UserActiveWorkflowAppID            = " & formatField(0, "N", 0) & ", " & _
            "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
            "UserActiveWorkflowStartTime        = " & formatField("", "DTTM", "NULL") & " " & _
            "WHERE UserID 	= '" & UserID & "'"
        executeNonQuery(strQry)
    End Sub

    Private Sub saveUserLockout()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim UserLockedOut As Boolean = HttpContext.Current.Request("UserLockedOut")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            If (UserLockedOut) Then
                strQry = "UPDATE tblusers SET  " & _
                   "UserLockedOut	= 0 " & _
                   "WHERE UserID	= '" & UserID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            Else
                saveUserLogout()
                strQry = "UPDATE tblusers SET  " & _
                   "UserLockedOut	= 1 " & _
                   "WHERE UserID	= '" & UserID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveUserPasswordChange()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "UPDATE tblusers SET  " & _
                "UserPasswordChangeDate	= " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                "WHERE UserID	        = '" & UserID & "' " & _
                "AND CompanyID          = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveUserStore()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strUserStoreNames As String = HttpContext.Current.Request("UserStoreNames")
        Dim strUserStoreValues As String = HttpContext.Current.Request("UserStoreValues")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tbluserstore WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(strUserStoreNames)) Then
                Dim arrstrUserStoreNames As Array = Split(strUserStoreNames, "|")
                Dim arrstrUserStoreValues As Array = Split(strUserStoreValues, "|")
                For i As Integer = 0 To UBound(arrstrUserStoreNames)
                    strQry = "INSERT INTO tbluserstore (CompanyID, UserID, StoredUserName, StoredUserValue) " & _
                                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                formatField(UserID, "N", 0) & ", " & _
                                formatField(arrstrUserStoreNames(i), "", "") & ", " & _
                                formatField(arrstrUserStoreValues(i), "", "") & ") "
                    executeNonQuery(strQry)
                Next
            End If
            cacheDependency(CacheObject, "tbluserstore")
        End If
        strReturnUrlMessage = encodeURL("User store successfully updated")
    End Sub

    Private Sub saveApplicationFlow()
        Dim ApplicationTemplateID As String = HttpContext.Current.Request("ApplicationTemplateID")
        Dim ApplicationPageID As Integer = 0, ScriptID As Integer = 0, AdditionalFieldID As Integer = 0, ApplicationPageOrder As Integer = 0, x As String = ""
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "DELETE FROM tblapplicationpages WHERE ApplicationPageTemplateID = '" & ApplicationTemplateID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
        For Each Item As String In HttpContext.Current.Request.Form
            If (InStr(Item, "txtApplicationPageName") > 0) Then
                x = Replace(Item, "txtApplicationPageName", "")
                'ApplicationPageID = HttpContext.Current.Request("ApplicationPageID" & x)
                'If (checkValue(ApplicationPageID)) Then
                '    strQry = "UPDATE tblapplicationpages SET " & _
                '        "ApplicationPageName                = " & formatField(HttpContext.Current.Request(Item), "T", "") & ", " & _
                '        "ApplicationPageOrder               = " & formatField(ApplicationPageOrder, "N", 0) & " " & _
                '        "WHERE ApplicationPageID    = '" & ApplicationPageID & "'"
                '    intRowsAffected = executeRowsAffectedQuery(strQry)
                'End If
                'If (intRowsAffected = 0) Then
                If (checkValue(HttpContext.Current.Request(Item))) Then
                    strQry = "INSERT INTO tblapplicationpages (CompanyID, ApplicationPageTemplateID, ApplicationPageName, ApplicationPageOrder) " & _
                       "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                       formatField(ApplicationTemplateID, "N", 0) & ", " & _
                       formatField(HttpContext.Current.Request(Item), "T", "") & ", " & _
                       formatField(ApplicationPageOrder, "N", 0) & ") "
                    ApplicationPageID = executeIdentityQuery(strQry)
                End If
                'Else
                'strQry = "DELETE FROM tblapplicationpagemappings WHERE ApplicationPageID = '" & ApplicationPageID & "' AND CompanyID = '" & CompanyID & "'"
                'executeNonQuery(strQry)
                'End If
                ApplicationPageOrder += 1
            ElseIf (InStr(Item, "txtParagraph") > 0) Then
                x = Replace(Item, "txtParagraph", "")
                ScriptID = HttpContext.Current.Request("ScriptID" & x)
                AdditionalFieldID = HttpContext.Current.Request("AdditionalFieldID" & x)
                If (checkValue(ScriptID)) Then
                    strQry = "UPDATE tblapplicationscripts SET  " & _
                       "ScriptText		        = " & formatField(HttpContext.Current.Request(Item), "", "") & ", " & _
                       "ScriptPriorityID        = " & formatField(HttpContext.Current.Request("ddPriority" & x), "N", 0) & " " & _
                       "WHERE ScriptID	        = '" & ScriptID & "' " & _
                       "AND CompanyID           = '" & CompanyID & "'"
                    'HttpContext.Current.Response.Write(strQry & "<br />")
                    intRowsAffected = executeRowsAffectedQuery(strQry)
                    If (checkValue(AdditionalFieldID)) Then
                        strQry = "UPDATE tblapplicationpageadditionalfields SET  " & _
                           "AdditionalFieldName	    = " & formatField(Replace(regexReplace("<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)</\1>", "$2", HttpContext.Current.Request(Item)), vbCrLf, ""), "T", "") & " " & _
                           "WHERE AdditionalFieldID	= '" & AdditionalFieldID & "' " & _
                           "AND CompanyID           = '" & CompanyID & "'"
                        'HttpContext.Current.Response.Write(strQry & "<br />")
                        intRowsAffected = executeRowsAffectedQuery(strQry)
                    End If
                End If
                If (intRowsAffected = 0) Then
                    If (checkValue(HttpContext.Current.Request(Item))) Then
                        strQry = "INSERT INTO tblapplicationscripts (CompanyID, ScriptText, ScriptPriorityID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(HttpContext.Current.Request(Item), "", "") & ", " & _
                            formatField(HttpContext.Current.Request("ddPriority" & x), "N", 0) & ")"
                        'HttpContext.Current.Response.Write(strQry & "<br />")
                        ScriptID = executeIdentityQuery(strQry)
                        strQry = "INSERT INTO tblapplicationpageadditionalfields (CompanyID, AdditionalFieldName, AdditionalFieldType, AdditionalFieldScriptID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(Replace(regexReplace("<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)</\1>", "$2", HttpContext.Current.Request(Item)), vbCrLf, ""), "T", "") & ", " & _
                            formatField(2, "N", 2) & ", " & _
                            formatField(ScriptID, "N", 0) & ") "
                        'HttpContext.Current.Response.Write(strQry & "<br />")
                        AdditionalFieldID = executeIdentityQuery(strQry)
                    End If
                End If
                strQry = "INSERT INTO tblapplicationpagemappings (CompanyID, ApplicationPageID, ApplicationFieldID) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(ApplicationPageID, "N", 0) & ", " & _
                    formatField(AdditionalFieldID, "N", 0) & ") "
                executeNonQuery(strQry)
            ElseIf (InStr(Item, "ddField") > 0) Then
                strQry = "INSERT INTO tblapplicationpagemappings (CompanyID, ApplicationPageID, ApplicationFieldID) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(ApplicationPageID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request(Item), "N", 0) & ") "
                executeNonQuery(strQry)
            End If
        Next
        cacheDependency(CacheObject, "tblapplicationpages,tblapplicationpagemappings,vwassignedapplicationfields")
    End Sub

    Private Sub saveWorkflow()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        Dim WorkflowQuery As String = HttpContext.Current.Request("txtWorkflowQuery")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0

        If (checkValue(WorkflowID)) Then
            strQry = "UPDATE tblworkflows SET  " & _
               "WorkflowReference           	= " & formatField(HttpContext.Current.Request("txtWorkflowReference"), "L", "") & ", " & _
               "WorkflowTypeID	            	= " & formatField(HttpContext.Current.Request("ddWorkflowTypeID"), "N", 0) & ", " & _
               "WorkflowShortName	        	= " & formatField(HttpContext.Current.Request("txtWorkflowShortName"), "T", "") & ", " & _
               "WorkflowLongName	        	= " & formatField(HttpContext.Current.Request("txtWorkflowLongName"), "T", "") & ", " & _
               "WorkflowRecycleStrategyID		= " & formatField(HttpContext.Current.Request("ddWorkflowRecycleStrategyID"), "N", 0) & ", " & _
               "WorkflowSelect              	= " & formatField(HttpContext.Current.Request("txtWorkflowSelect"), "", "") & ", " & _
               "WorkflowTable               	= " & formatField(HttpContext.Current.Request("txtWorkflowTable"), "", "") & ", " & _
               "WorkflowQuery               	= " & formatField(HttpContext.Current.Request("txtWorkflowQuery"), "", "") & ", " & _
               "WorkflowOrderBy               	= " & formatField(HttpContext.Current.Request("txtWorkflowOrderBy"), "", "") & ", " & _
               "WorkflowReportBuilderID       	= " & formatField(HttpContext.Current.Request("ddWorkflowReportBuilderID"), "N", 0) & ", " & _
               "WorkflowCallInterface       	= " & formatField(HttpContext.Current.Request("cbWorkflowCallInterface"), "B", 0) & ", " & _
               "WorkflowSpeedFactor             = " & formatField(HttpContext.Current.Request("WorkflowSpeedFactor"), "N", 100) & ", " & _
               "WorkflowToleranceFactor         = " & formatField(HttpContext.Current.Request("txtWorkflowToleranceFactor"), "N", 0) & ", " & _
               "WorkflowTop50                   = " & formatField(HttpContext.Current.Request("cbWorkflowTop50"), "B", 0) & ", " & _
               "WorkflowStats                   = " & formatField(HttpContext.Current.Request("cbWorkflowStats"), "B", 0) & ", " & _
               "WorkflowIndividualStats         = " & formatField(HttpContext.Current.Request("cbWorkflowIndividualStats"), "B", 0) & ", " & _
               "WorkflowFilters                 = " & formatField(HttpContext.Current.Request("cbWorkflowFilters"), "B", 0) & ", " & _
               "WorkflowRota                    = " & formatField(HttpContext.Current.Request("cbWorkflowRota"), "B", 0) & ", " & _
               "WorkflowDiary                   = " & formatField(HttpContext.Current.Request("cbWorkflowDiary"), "B", 0) & ", " & _
               "WorkflowApplicationManagement	= " & formatField(HttpContext.Current.Request("WorkflowApplicationManagement"), "B", 0) & " " & _
               "WHERE WorkflowID            	= '" & WorkflowID & "' " & _
               "AND CompanyID                   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            'If (checkValue(WorkflowQuery)) Then
            strQry = "INSERT INTO tblworkflows (CompanyID, WorkflowReference, WorkflowTypeID, WorkflowShortName, WorkflowLongName, WorkflowRecycleStrategyID, WorkflowSelect, WorkflowTable, WorkflowQuery, WorkflowOrderBy, WorkflowReportBuilderID, WorkflowCallInterface, WorkflowSpeedFactor, WorkflowToleranceFactor, WorkflowTop50, WorkflowStats, WorkflowIndividualStats, WorkflowFilters, WorkflowRota, WorkflowDiary, WorkflowApplicationManagement) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowReference"), "L", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddWorkflowTypeID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowShortName"), "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowLongName"), "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddWorkflowRecycleStrategyID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowSelect"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowTable"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowQuery"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowOrderBy"), "", "") & ", " & _
                   formatField(HttpContext.Current.Request("ddWorkflowReportBuilderID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowCallInterface"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("WorkflowSpeedFactor"), "N", 100) & ", " & _
                   formatField(HttpContext.Current.Request("txtWorkflowToleranceFactor"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowTop50"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowStats"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowIndividualStats"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowFilters"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowRota"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("cbWorkflowDiary"), "B", 0) & ", " & _
                   formatField(HttpContext.Current.Request("WorkflowApplicationManagement"), "B", 0) & ")"
            executeNonQuery(strQry)
            'End If
        End If
        If (checkValue(HttpContext.Current.Request("txtWorkflowToleranceFactor"))) Then
            'executeNonQuery("EXECUTE spupdateworkflowstats @CompanyID = " & CompanyID)
        End If
        cacheDependency(CacheObject, "tblworkflows")
    End Sub

    Private Sub saveWorkflowDelete()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        Dim strQry As String = ""
        If (checkValue(WorkflowID)) Then
            strQry = "DELETE FROM tblworkflows WHERE WorkflowID = '" & WorkflowID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblworkflows")
        End If
    End Sub

    Private Sub saveWorkflowActive()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        Dim WorkflowActive As Boolean = HttpContext.Current.Request("WorkflowActive")
        Dim strQry As String = ""
        If (checkValue(WorkflowID)) Then
            If (WorkflowActive) Then
                strQry = "UPDATE tblworkflows SET  " & _
                   "WorkflowActive	    = 0 " & _
                   "WHERE WorkflowID	= '" & WorkflowID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblworkflows SET  " & _
                   "WorkflowActive		= 1 " & _
                   "WHERE WorkflowID	= '" & WorkflowID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblworkflows")
        End If
    End Sub

    Private Sub saveSubWorkflow()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        Dim SubWorkflowID As String = HttpContext.Current.Request("SubWorkflowID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0

        If (checkValue(SubWorkflowID)) Then
            strQry = "UPDATE tblsubworkflows SET  " & _
               "SubWorkflowName	    = " & formatField(HttpContext.Current.Request("txtSubWorkflowName"), "T", "") & ", " & _
               "SubWorkflowOrderBy  = " & formatField(HttpContext.Current.Request("txtSubWorkflowOrderBy"), "", "") & " " & _
               "WHERE SubWorkflowID = '" & SubWorkflowID & "' " & _
               "AND CompanyID       = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblsubworkflows (CompanyID, WorkflowID, SubWorkflowName, SubWorkflowOrderBy) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(WorkflowID, "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("txtSubWorkflowName"), "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtSubWorkflowOrderBy"), "", "") & ") "
            executeNonQuery(strQry)
        End If
        If (HttpContext.Current.Request("cbSelected") = "on") Then
            executeNonQuery("UPDATE tblworkflows SET WorkflowSubWorkflowID = " & formatField(SubWorkflowID, "N", 0) & " WHERE WorkflowID = '" & WorkflowID & "' AND CompanyID = '" & CompanyID & "'")
        End If
        cacheDependency(CacheObject, "tblworkflows,tblsubworkflows")
    End Sub

    Private Sub saveSubWorkflowDelete()
        Dim SubWorkflowID As String = HttpContext.Current.Request("SubWorkflowID")
        Dim strQry As String = ""
        If (checkValue(SubWorkflowID)) Then
            strQry = "DELETE FROM tblsubworkflows WHERE SubWorkflowID = '" & SubWorkflowID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblworkflows,tblsubworkflows")
        End If
        saveSubWorkflowDeselected()
    End Sub

    Private Sub saveSubWorkflowActive()
        Dim SubWorkflowID As String = HttpContext.Current.Request("SubWorkflowID")
        Dim SubWorkflowActive As Boolean = HttpContext.Current.Request("SubWorkflowActive")
        Dim strQry As String = ""
        If (checkValue(SubWorkflowID)) Then
            If (SubWorkflowActive) Then
                strQry = "UPDATE tblsubworkflows SET  " & _
                   "SubWorkflowActive	= 0 " & _
                   "WHERE SubWorkflowID	= '" & SubWorkflowID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                saveSubWorkflowDeselected()
            Else
                strQry = "UPDATE tblsubworkflows SET  " & _
                   "SubWorkflowActive	= 1 " & _
                   "WHERE SubWorkflowID	= '" & SubWorkflowID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblworkflows,tblsubworkflows")
        End If
    End Sub

    Private Sub saveSubWorkflowSelected()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        Dim SubWorkflowID As String = HttpContext.Current.Request("SubWorkflowID")
        executeNonQuery("UPDATE tblworkflows SET WorkflowSubWorkflowID = " & formatField(SubWorkflowID, "N", 0) & " WHERE WorkflowID = '" & WorkflowID & "' AND CompanyID = '" & CompanyID & "'")
        cacheDependency(CacheObject, "tblworkflows,tblsubworkflows")
    End Sub

    Private Sub saveSubWorkflowDeselected()
        Dim WorkflowID As String = HttpContext.Current.Request("WorkflowID")
        executeNonQuery("UPDATE tblworkflows SET WorkflowSubWorkflowID = " & formatField(0, "N", 0) & " WHERE WorkflowID = '" & WorkflowID & "' AND CompanyID = '" & CompanyID & "'")
        cacheDependency(CacheObject, "tblworkflows,tblsubworkflows")
    End Sub

    Private Sub saveWorkflowUser()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strWorkflows As String = HttpContext.Current.Request("lstWorkflows")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tblworkflowmappings WHERE WorkflowMappingUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(strWorkflows)) Then
                Dim arrWorkflows As Array = Split(strWorkflows, ",")
                For i As Integer = 0 To UBound(arrWorkflows)
                    strQry = "INSERT INTO tblworkflowmappings (CompanyID, WorkflowMappingUserID, WorkflowMappingWorkflowID) " & _
                                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                formatField(UserID, "N", 0) & ", " & _
                                formatField(arrWorkflows(i), "N", 0) & ") "
                    executeNonQuery(strQry)
                Next
            End If
            cacheDependency(CacheObject, "tblworkflowmappings")
        End If
        strReturnUrlMessage = encodeURL("Workflows successfully updated")
    End Sub

    Private Sub saveWorkflowUserCopy()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim intCopyUserID As String = HttpContext.Current.Request("ddCopyUserID")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tblworkflowmappings WHERE WorkflowMappingUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(intCopyUserID)) Then
                Dim strSQL As String = "SELECT WorkflowMappingWorkflowID FROM tblworkflowmappings WHERE WorkflowMappingUserID = '" & intCopyUserID & "' AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim strClass As String = "row1"
                    Dim x As Integer = 0
                    For Each Row As DataRow In dsCache.Rows
                        strQry = "INSERT INTO tblworkflowmappings (CompanyID, WorkflowMappingUserID, WorkflowMappingWorkflowID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(UserID, "N", 0) & ", " & _
                            formatField(Row.Item("WorkflowMappingWorkflowID"), "N", 0) & ") "
                        executeNonQuery(strQry)
                    Next
                End If
                dsCache = Nothing
            End If
            cacheDependency(CacheObject, "tblworkflowmappings")
        End If
        strReturnUrlMessage = encodeURL("Workflows successfully updated")
    End Sub

    Private Sub saveDashboards()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strDashboards As String = HttpContext.Current.Request("lstDashboards")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tbldashboardmappings WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(strDashboards)) Then
                Dim arrDashboards As Array = Split(strDashboards, ",")
                For i As Integer = 0 To UBound(arrDashboards)
                    strQry = "INSERT INTO tbldashboardmappings (CompanyID, UserID, DashboardID) " & _
                                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                formatField(UserID, "N", 0) & ", " & _
                                formatField(arrDashboards(i), "N", 0) & ") "
                    executeNonQuery(strQry)
                Next
            End If
            cacheDependency(CacheObject, "tbldashboardmappings,vwdashboardpanel")
        End If
        strReturnUrlMessage = encodeURL("Dashboards successfully updated")
    End Sub

    Private Sub saveDashboardsCopy()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim intCopyUserID As String = HttpContext.Current.Request("ddCopyUserID")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tbldashboardmappings WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(intCopyUserID)) Then
                Dim strSQL As String = "SELECT DashboardID FROM tbldashboardmappings WHERE UserID = '" & intCopyUserID & "' AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim strClass As String = "row1"
                    Dim x As Integer = 0
                    For Each Row As DataRow In dsCache.Rows
                        strQry = "INSERT INTO tbldashboardmappings (CompanyID, UserID, DashboardID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(UserID, "N", 0) & ", " & _
                            formatField(Row.Item("DashboardID"), "N", 0) & ") "
                        executeNonQuery(strQry)
                    Next
                End If
                dsCache = Nothing
            End If
            cacheDependency(CacheObject, "tbldashboardmappings,vwdashboardpanel")
        End If
        strReturnUrlMessage = encodeURL("Dashboards successfully updated")
    End Sub

    Private Sub saveUserAccess()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim strUserAccess As String = HttpContext.Current.Request("lstUserAccess")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tbluseraccesslevelmappings WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(strUserAccess)) Then
                Dim arrUserAccess As Array = Split(strUserAccess, ",")
                For i As Integer = 0 To UBound(arrUserAccess)
                    strQry = "INSERT INTO tbluseraccesslevelmappings (CompanyID, UserID, UserAccessLevelID) " & _
                                "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                formatField(UserID, "N", 0) & ", " & _
                                formatField(arrUserAccess(i), "N", 0) & ") "
                    executeNonQuery(strQry)
                Next
            End If
            cacheDependency(CacheObject, "tbluseraccesslevelmappings,vwuseraccesslevels")
        End If
        strReturnUrlMessage = encodeURL("Access levels successfully updated")
    End Sub

    Private Sub saveUserAccessCopy()
        Dim UserID As String = HttpContext.Current.Request("UserID")
        Dim intCopyUserID As String = HttpContext.Current.Request("ddCopyUserID")
        Dim strQry As String = ""
        If (checkValue(UserID)) Then
            strQry = "DELETE FROM tbluseraccesslevelmappings WHERE UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            If (checkValue(intCopyUserID)) Then
                Dim strSQL As String = "SELECT UserAccessLevelID FROM tbluseraccesslevelmappings WHERE UserID = '" & intCopyUserID & "' AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim strClass As String = "row1"
                    Dim x As Integer = 0
                    For Each Row As DataRow In dsCache.Rows
                        strQry = "INSERT INTO tbluseraccesslevelmappings (CompanyID, UserID, UserAccessLevelID) " & _
                            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                            formatField(UserID, "N", 0) & ", " & _
                            formatField(Row.Item("UserAccessLevelID"), "N", 0) & ") "
                        executeNonQuery(strQry)
                    Next
                End If
                dsCache = Nothing
            End If
            cacheDependency(CacheObject, "tbluseraccesslevelmappings,vwuseraccesslevels")
        End If
        strReturnUrlMessage = encodeURL("Access levels successfully updated")
    End Sub

    Private Sub saveRecycleStrategy()
        Dim RecycleStrategyID As String = HttpContext.Current.Request("RecycleStrategyID")
        Dim RecycleStrategyName As String = HttpContext.Current.Request("txtRecycleStrategyName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(RecycleStrategyID)) Then
            strQry = "UPDATE tblrecyclestrategies SET  " & _
               "RecycleStrategyName	        = " & formatField(RecycleStrategyName, "T", "") & ", " & _
               "RecycleStrategyStartDate	= " & formatField(HttpContext.Current.Request("txtRecycleStrategyStartDate"), "DT", "NULL") & ", " & _
               "RecycleStrategyEndDate	    = " & formatField(HttpContext.Current.Request("txtRecycleStrategyEndDate"), "DT", "NULL") & " " & _
               "WHERE RecycleStrategyID	    = '" & RecycleStrategyID & "' " & _
               "AND CompanyID               = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(RecycleStrategyName)) Then
                strQry = "INSERT INTO tblrecyclestrategies (CompanyID, RecycleStrategyName, RecycleStrategyStartDate, RecycleStrategyEndDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(RecycleStrategyName, "T", "") & ", " & _
                   formatField(HttpContext.Current.Request("txtRecycleStrategyStartDate"), "DTTM", Config.DefaultDateTime) & ", " & _
                   formatField(HttpContext.Current.Request("txtRecycleStrategyEndDate"), "DTTM", Config.DefaultDateTime) & ")"
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblrecyclestrategies")
    End Sub

    Private Sub saveRecycleStrategyDelete()
        Dim RecycleStrategyID As String = HttpContext.Current.Request("RecycleStrategyID")
        Dim strQry As String = ""
        If (checkValue(RecycleStrategyID)) Then
            strQry = "DELETE FROM tblrecyclestrategies WHERE RecycleStrategyID = '" & RecycleStrategyID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            strQry = "DELETE FROM tblrecyclerules WHERE RecycleRuleStrategyID = '" & RecycleStrategyID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblrecyclestrategies")
        End If
    End Sub

    Private Sub saveRecycleStrategyActive()
        Dim RecycleStrategyID As String = HttpContext.Current.Request("RecycleStrategyID")
        Dim RecycleStrategyActive As Boolean = HttpContext.Current.Request("RecycleStrategyActive")
        Dim strQry As String = ""
        If (checkValue(RecycleStrategyID)) Then
            If (RecycleStrategyActive) Then
                strQry = "UPDATE tblrecyclestrategies SET  " & _
                   "RecycleStrategyActive       = 0 " & _
                   "WHERE RecycleStrategyID	    = '" & RecycleStrategyID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
                Call executeNonQuery(strQry)
                strQry = "UPDATE tblrecyclerules SET  " & _
                   "RecycleRuleActive           = 0 " & _
                   "WHERE RecycleRuleStrategyID	= '" & RecycleStrategyID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
                Call executeNonQuery(strQry)
                cacheDependency(CacheObject, "tblrecyclestrategies")
            Else
                strQry = "UPDATE tblrecyclestrategies SET  " & _
                   "RecycleStrategyActive	    = 1 " & _
                   "WHERE RecycleStrategyID	    = '" & RecycleStrategyID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
                Call executeNonQuery(strQry)
                strQry = "UPDATE tblrecyclerules SET  " & _
                   "RecycleRuleActive           = 1 " & _
                   "WHERE RecycleRuleStrategyID	= '" & RecycleStrategyID & "' " & _
                   "AND CompanyID               = '" & CompanyID & "'"
                Call executeNonQuery(strQry)
                cacheDependency(CacheObject, "tblrecyclestrategies")
            End If
        End If
    End Sub

    Private Sub saveRecycleRule()
        Dim RecycleRuleID As String = HttpContext.Current.Request("RecycleRuleID")
        Dim RecycleStrategyID As String = HttpContext.Current.Request("RecycleStrategyID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(RecycleRuleID)) Then
            strQry = "UPDATE tblrecyclerules SET  "
            If (objLeadPlatform.Config.SuperAdmin) Then
                strQry += _
                "RecycleRuleStatusCode	        = " & formatField(HttpContext.Current.Request("ddRecycleRuleStatusCode"), "U", "") & ", " & _
                "RecycleRuleSubStatusCode	    = " & formatField(HttpContext.Current.Request("ddRecycleRuleSubStatusCode"), "U", "NULL") & ", " & _
                "RecycleRuleMediaCampaignGradeID = " & formatField(HttpContext.Current.Request("ddRecycleRuleMediaCampaignGradeID"), "N", 0) & ", " & _
                "RecycleRuleProductType	        = " & formatField(HttpContext.Current.Request("ddRecycleRuleProductType"), "", "") & ", "
            End If
            strQry += _
               "RecycleRuleRecycleTime	        = " & formatField(HttpContext.Current.Request("txtRecycleRuleRecycleTime"), "N", 0) & ", " & _
               "RecycleRuleStartHour	        = " & formatField(HttpContext.Current.Request("ddRecycleRuleStartHour"), "N", 0) & ", " & _
               "RecycleRuleEndHour	            = " & formatField(HttpContext.Current.Request("ddRecycleRuleEndHour"), "N", 23) & ", " & _
               "RecycleRuleLastUpdated	        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
               "WHERE RecycleRuleID	            = '" & RecycleRuleID & "' " & _
               "AND CompanyID                   = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
            End If
            If (intRowsAffected = 0) Then
                strQry = "INSERT INTO tblrecyclerules (CompanyID, RecycleRuleStrategyID, RecycleRuleStatusCode, RecycleRuleSubStatusCode, RecycleRuleRecycleTime, RecycleRuleMediaCampaignGradeID, RecycleRuleProductType, RecycleRuleStartHour, RecycleRuleEndHour) " & _
                    "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                    formatField(RecycleStrategyID, "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleStatusCode"), "U", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleSubStatusCode"), "U", "NULL") & ", " & _
                    formatField(HttpContext.Current.Request("txtRecycleRuleRecycleTime"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleMediaCampaignGradeID"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleProductType"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleStartHour"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("ddRecycleRuleEndHour"), "N", 23) & ") "
                executeNonQuery(strQry)
            End If
    End Sub

    Private Sub saveRecycleRuleDelete()
        Dim RecycleRuleID As String = HttpContext.Current.Request("RecycleRuleID")
        Dim strQry As String = ""
        If (checkValue(RecycleRuleID)) Then
            strQry = "DELETE FROM tblrecyclerules WHERE RecycleRuleID = '" & RecycleRuleID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveRecycleRuleActive()
        Dim RecycleRuleID As String = HttpContext.Current.Request("RecycleRuleID")
        Dim RecycleRuleActive As Boolean = HttpContext.Current.Request("RecycleRuleActive")
        Dim strQry As String = ""
        If (checkValue(RecycleRuleID)) Then
            If (RecycleRuleActive) Then
                strQry = "UPDATE tblrecyclerules SET  " & _
                   "RecycleRuleActive   = 0 " & _
                   "WHERE RecycleRuleID	= '" & RecycleRuleID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblrecyclerules SET  " & _
                   "RecycleRuleActive	= 1 " & _
                   "WHERE RecycleRuleID	= '" & RecycleRuleID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveLoadedToDialerDate()
        Dim strQry As String = "UPDATE tblapplicationstatus SET " & _
                                "LoadedToDialerDate = " & formatField(HttpContext.Current.Request("txtLoadedToDialerDate") & " " & HttpContext.Current.Request("txtLoadedToDialerTime"), "DTTM", Config.DefaultDateTime) & " " & _
                                "WHERE LoadedToDialerDate IS NULL " & _
                                "AND (CONVERT (NVARCHAR(10), TransferredToDialerDate, 103) + ' ' + CONVERT (NVARCHAR(8), TransferredToDialerDate, 108) = '" & HttpContext.Current.Request("TransferredToDialerDate") & "') AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Private Sub deleteCaseLock()
        Dim strQry As String = "DELETE FROM tbllocks WHERE AppID = '" & HttpContext.Current.Request("AppID") & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strQry)
    End Sub

    Private Sub saveCommunicationTemplate()
        Dim CommunicationTemplateID As String = HttpContext.Current.Request("CommunicationTemplateID")
        Dim CommunicationTemplateName As String = HttpContext.Current.Request("txtCommunicationTemplateName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0

        If (checkValue(CommunicationTemplateID)) Then
            strQry = "UPDATE tblcommunicationtemplates SET  " & _
               "CommunicationTemplateName               = " & formatField(HttpContext.Current.Request("txtCommunicationTemplateName"), "T", "") & ", " & _
               "CommunicationTemplateText	            = " & formatField(HttpContext.Current.Request("txtCommunicationTemplateText"), "", "") & ", " & _
               "CommunicationTemplateEmailTemplateID    = " & formatField(HttpContext.Current.Request("ddCommunicationTemplateEmailTemplateID"), "N", 0) & ", " & _
               "CommunicationTemplateType	            = " & formatField(HttpContext.Current.Request("ddCommunicationTemplateType"), "N", 0) & " " & _
               "WHERE CommunicationTemplateID           = '" & CommunicationTemplateID & "' " & _
               "AND CompanyID                           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(CommunicationTemplateName)) Then
                strQry = "INSERT INTO tblcommunicationtemplates (CompanyID, CommunicationTemplateName, CommunicationTemplateText, CommunicationTemplateEmailTemplateID, CommunicationTemplateType) " & _
                       "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                       formatField(HttpContext.Current.Request("txtCommunicationTemplateName"), "T", "") & ", " & _
                       formatField(HttpContext.Current.Request("txtCommunicationTemplateText"), "", "") & ", " & _
                       formatField(HttpContext.Current.Request("ddCommunicationTemplateEmailTemplateID"), "N", 0) & ", " & _
                       formatField(HttpContext.Current.Request("ddCommunicationTemplateType"), "N", 0) & ")"
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblcommunicationtemplates")
    End Sub

    Private Sub saveCommunicationTemplateDelete()
        Dim CommunicationTemplateID As String = HttpContext.Current.Request("CommunicationTemplateID")
        Dim strQry As String = ""
        If (checkValue(CommunicationTemplateID)) Then
            strQry = "DELETE FROM tblcommunicationtemplates WHERE CommunicationTemplateID = '" & CommunicationTemplateID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblcommunicationtemplates")
        End If
    End Sub

    Private Sub saveCommunicationTemplateActive()
        Dim CommunicationTemplateID As String = HttpContext.Current.Request("CommunicationTemplateID")
        Dim CommunicationTemplateActive As Boolean = HttpContext.Current.Request("CommunicationTemplateActive")
        Dim strQry As String = ""
        If (checkValue(CommunicationTemplateID)) Then
            If (CommunicationTemplateActive) Then
                strQry = "UPDATE tblcommunicationtemplates SET  " & _
                   "CommunicationTemplateActive	    = 0 " & _
                   "WHERE CommunicationTemplateID	= '" & CommunicationTemplateID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblcommunicationtemplates SET  " & _
                   "CommunicationTemplateActive		= 1 " & _
                   "WHERE CommunicationTemplateID	= '" & CommunicationTemplateID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblcommunicationtemplates")
        End If
    End Sub

    Private Sub saveEmailTemplate()
        Dim EmailTemplateID As String = HttpContext.Current.Request("EmailTemplateID")
        Dim EmailTemplateName As String = HttpContext.Current.Request("txtEmailTemplateName")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0

        If (checkValue(EmailTemplateID)) Then
            strQry = "UPDATE tblemailtemplates SET  " & _
               "EmailTemplateName       = " & formatField(HttpContext.Current.Request("txtEmailTemplateName"), "T", "") & ", " & _
               "EmailTemplateBody	    = " & formatField(HttpContext.Current.Request("txtEmailTemplateBody"), "", "") & " " & _
               "WHERE EmailTemplateID   = '" & EmailTemplateID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            If (checkValue(EmailTemplateName)) Then
                strQry = "INSERT INTO tblemailtemplates (CompanyID, EmailTemplateName, EmailTemplateBody) " & _
                       "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                       formatField(HttpContext.Current.Request("txtEmailTemplateName"), "T", "") & ", " & _
                       formatField(HttpContext.Current.Request("txtEmailTemplateBody"), "", "") & ") "
                executeNonQuery(strQry)
            End If
        End If
        cacheDependency(CacheObject, "tblemailtemplates")
    End Sub

    Private Sub saveEmailTemplateDelete()
        Dim EmailTemplateID As String = HttpContext.Current.Request("EmailTemplateID")
        Dim strQry As String = ""
        If (checkValue(EmailTemplateID)) Then
            strQry = "DELETE FROM tblemailtemplates WHERE EmailTemplateID = '" & EmailTemplateID & "' AND CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblemailtemplates")
        End If
    End Sub

    Private Sub saveEmailTemplateActive()
        Dim EmailTemplateID As String = HttpContext.Current.Request("EmailTemplateID")
        Dim EmailTemplateActive As Boolean = HttpContext.Current.Request("EmailTemplateActive")
        Dim strQry As String = ""
        If (checkValue(EmailTemplateID)) Then
            If (EmailTemplateActive) Then
                strQry = "UPDATE tblemailtemplates SET  " & _
                   "EmailTemplateActive	    = 0 " & _
                   "WHERE EmailTemplateID	= '" & EmailTemplateID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblemailtemplates SET  " & _
                   "EmailTemplateActive		= 1 " & _
                   "WHERE EmailTemplateID	= '" & EmailTemplateID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
            cacheDependency(CacheObject, "tblemailtemplates")
        End If
    End Sub

    Private Sub savePDF()
        Dim PDFID As String = HttpContext.Current.Request("PDFID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(PDFID)) Then
            strQry = "UPDATE tblpdfs SET  " & _
               "PDFName	        = " & formatField(HttpContext.Current.Request("txtPDFName"), "T", "") & ", " & _
               "PDFFileName	    = " & formatField(HttpContext.Current.Request("txtPDFFileName"), "", "") & ", " & _
               "PDFLength	    = " & formatField(HttpContext.Current.Request("txtPDFLength"), "N", 0) & ", " & _
               "PDFRoutine	    = " & formatField(HttpContext.Current.Request("txtPDFRoutine"), "", "") & " " & _
               "WHERE PDFID	    = '" & PDFID & "' " & _
               "AND CompanyID   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblpdfs (CompanyID, PDFName, PDFFileName, PDFLength, PDFRoutine) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtPDFName"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("txtPDFFileName"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("txtPDFLength"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtPDFRoutine"), "", "") & ")"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblpdfs")
    End Sub

    Private Sub savePDFDelete()
        Dim PDFID As String = HttpContext.Current.Request("PDFID")
        Dim strQry As String = ""
        If (checkValue(PDFID)) Then
            strQry = "DELETE FROM tblpdfs WHERE PDFID = '" & PDFID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblpdfs")
    End Sub

    Private Sub savePDFActive()
        Dim PDFID As String = HttpContext.Current.Request("PDFID")
        Dim PDFActive As String = HttpContext.Current.Request("PDFActive")
        Dim strQry As String = ""
        If (checkValue(PDFID)) Then
            If (PDFActive) Then
                strQry = "UPDATE tblpdfs SET  " & _
                   "PDFActive	    = 0 " & _
                   "WHERE PDFID	    = '" & PDFID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblpdfs SET  " & _
                   "PDFActive	    = 1 " & _
                   "WHERE PDFID	    = '" & PDFID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblpdfs")
        End If
    End Sub

    Private Sub saveLetterTemplate()
        Dim LetterID As String = HttpContext.Current.Request("LetterID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(LetterID)) Then
            strQry = "UPDATE tbllettertemplates SET  " & _
               "LetterName	            = " & formatField(HttpContext.Current.Request("txtLetterName"), "T", "") & ", " & _
               "LetterType	            = " & formatField(HttpContext.Current.Request("ddLetterType"), "N", 1) & ", " & _
               "LetterEmailProfileID    = " & formatField(HttpContext.Current.Request("ddLetterEmailProfileID"), "N", 0) & ", " & _
               "LetterEmailReplyTo      = " & formatField(HttpContext.Current.Request("cbLetterEmailReplyTo"), "B", 0) & ", " & _
               "LetterEmailTemplateID   = " & formatField(HttpContext.Current.Request("ddLetterEmailTemplateID"), "N", 0) & ", " & _
               "LetterTitle	            = " & formatField(HttpContext.Current.Request("txtLetterTitle"), "", "") & ", " & _
               "LetterBody	            = " & formatField(HttpContext.Current.Request("LetterBody"), "", "") & ", " & _
               "LetterEmailAttachments	= " & formatField(HttpContext.Current.Request("LetterEmailAttachments"), "", "") & ", " & _
               "LetterBackground	    = " & formatField(HttpContext.Current.Request("ddLetterBackground"), "", "") & ", " & _
               "LetterColour	        = " & formatField(HttpContext.Current.Request("txtLetterColour"), "", "#000000") & " " & _
               "WHERE LetterID	        = '" & LetterID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tbllettertemplates (CompanyID, LetterName, LetterType, LetterEmailProfileID, LetterEmailReplyTo, LetterEmailTemplateID, LetterTitle, LetterBody, LetterEmailAttachments, LetterBackground, LetterColour) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtLetterName"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("ddLetterType"), "N", 1) & ", " & _
               formatField(HttpContext.Current.Request("ddLetterEmailProfileID"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("cbLetterEmailReplyTo"), "B", 0) & ", " & _
               formatField(HttpContext.Current.Request("ddLetterEmailTemplateID"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtLetterTitle"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("LetterBody"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("LetterEmailAttachments"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ddLetterBackground"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("txtLetterColour"), "", "#000000") & ") "
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tbllettertemplates")
    End Sub

    Private Sub saveLetterTemplateDelete()
        Dim LetterID As String = HttpContext.Current.Request("LetterID")
        Dim strQry As String = ""
        If (checkValue(LetterID)) Then
            strQry = "DELETE FROM tbllettertemplates WHERE LetterID = '" & LetterID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tbllettertemplates")
    End Sub

    Private Sub saveLetterTemplateActive()
        Dim LetterID As String = HttpContext.Current.Request("LetterID")
        Dim LetterActive As String = HttpContext.Current.Request("LetterActive")
        Dim strQry As String = ""
        If (checkValue(LetterID)) Then
            If (LetterActive) Then
                strQry = "UPDATE tbllettertemplates SET  " & _
                   "LetterActive	= 0 " & _
                   "WHERE LetterID	= '" & LetterID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tbllettertemplates SET  " & _
                   "LetterActive	= 1 " & _
                   "WHERE LetterID	= '" & LetterID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tbllettertemplates")
        End If
    End Sub

    Private Sub saveLetterParagraph()
        Dim ParagraphID As String = HttpContext.Current.Request("ParagraphID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ParagraphID)) Then
            strQry = "UPDATE tblletterparagraphs SET  " & _
               "ParagraphName	    = " & formatField(HttpContext.Current.Request("txtParagraphName"), "T", "") & ", " & _
               "ParagraphText	    = " & formatField(HttpContext.Current.Request("txtParagraphText"), "", "") & " " & _
               "WHERE ParagraphID	= '" & ParagraphID & "' " & _
               "AND CompanyID       = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblletterparagraphs (CompanyID, ParagraphName, ParagraphText) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtParagraphName"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("txtParagraphText"), "", "") & ") "
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblletterparagraphs")
    End Sub

    Private Sub saveLetterParagraphDelete()
        Dim ParagraphID As String = HttpContext.Current.Request("ParagraphID")
        Dim strQry As String = ""
        If (checkValue(ParagraphID)) Then
            strQry = "DELETE FROM tblletterparagraphs WHERE ParagraphID = '" & ParagraphID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblletterparagraphs")
    End Sub

    Private Sub saveLetterParagraphActive()
        Dim ParagraphID As String = HttpContext.Current.Request("ParagraphID")
        Dim ParagraphActive As String = HttpContext.Current.Request("ParagraphActive")
        Dim strQry As String = ""
        If (checkValue(ParagraphID)) Then
            If (ParagraphActive) Then
                strQry = "UPDATE tblletterparagraphs SET  " & _
                   "ParagraphActive	    = 0 " & _
                   "WHERE ParagraphID	= '" & ParagraphID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblletterparagraphs SET  " & _
                   "ParagraphActive	    = 1 " & _
                   "WHERE ParagraphID	= '" & ParagraphID & "' " & _
                   "AND CompanyID       = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblletterparagraphs")
        End If
    End Sub

    Private Sub duplicateCase()
        Dim strQry As String = ""
        If (HttpContext.Current.Request("strDuplicate") = "1") Then
            strQry = "UPDATE tblapplicationstatus SET " & _
                        "StatusCode = 'INV', " & _
                        "SubStatusCode = 'DUP', " & _
                        "OutsideCriteriaSubStatusCode = 'DUP', " & _
                        "UpdatedDate = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        "DuplicateChecked = 1 " & _
                        "WHERE AppID = '" & HttpContext.Current.Request("AppID") & "' " & _
                        "AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
            insertStatusHistory(HttpContext.Current.Request("AppID"), "INV", "DUP", DefaultUserID)
			saveUpdatedDate(HttpContext.Current.Request("AppID"), DefaultUserID, "INV")
            saveNote(HttpContext.Current.Request("AppID"), DefaultUserID, "Duplicate case confirmed")
        Else
            strQry = "UPDATE tblapplicationstatus SET DuplicateAppID = 0, DuplicateChecked = 1 WHERE AppID = '" & HttpContext.Current.Request("AppID") & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
    End Sub

    Private Sub saveReportBuilder()
        Dim ReportBuilderID As String = HttpContext.Current.Request("ReportBuilderID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ReportBuilderID)) Then
            strQry = "UPDATE tblreportbuilder SET  " & _
               "ReportBuilderName	                    = " & formatField(HttpContext.Current.Request("txtReportBuilderName"), "T", "") & ", " & _
               "ReportBuilderJavascriptFunctions	    = " & formatField(HttpContext.Current.Request("txtReportBuilderJavascriptFunctions"), "", "") & ", " & _
               "ReportBuilderFilterAccess	            = " & formatField(HttpContext.Current.Request("ReportBuilderFilterAccess"), "", "") & ", " & _
               "ReportBuilderFilterNames	            = " & formatField(HttpContext.Current.Request("ReportBuilderFilterNames"), "", "") & ", " & _
               "ReportBuilderColumnAccess	            = " & formatField(HttpContext.Current.Request("ReportBuilderColumnAccess"), "", "") & ", " & _
               "ReportBuilderColumnNames	            = " & formatField(HttpContext.Current.Request("ReportBuilderColumnNames"), "", "") & ", " & _
               "ReportBuilderColumnSort	                = " & formatField(HttpContext.Current.Request("ReportBuilderColumnSort"), "", "") & ", " & _
               "ReportBuilderColumnValues	            = " & formatField(HttpContext.Current.Request("ReportBuilderColumnValues"), "", "") & ", " & _
               "ReportBuilderColumnFormats	            = " & formatField(HttpContext.Current.Request("ReportBuilderColumnFormats"), "", "") & ", " & _
               "ReportBuilderColumnTotals	            = " & formatField(HttpContext.Current.Request("ReportBuilderColumnTotals"), "", "") & ", " & _
               "ReportBuilderClickThrough	            = " & formatField(HttpContext.Current.Request("ReportBuilderClickThrough"), "", "") & ", " & _
               "ReportBuilderClickThroughQueryString	= " & formatField(HttpContext.Current.Request("ReportBuilderClickThroughQueryString"), "", "") & " " & _
               "WHERE ReportBuilderID	                = '" & ReportBuilderID & "' " & _
               "AND CompanyID                           = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblreportbuilder (CompanyID, ReportBuilderTypeID, ReportBuilderName, ReportBuilderJavascriptFunctions, ReportBuilderFilterAccess, ReportBuilderFilterNames, ReportBuilderColumnAccess, ReportBuilderColumnNames, ReportBuilderColumnSort, ReportBuilderColumnValues, ReportBuilderColumnFormats, ReportBuilderColumnTotals, ReportBuilderClickThrough, ReportBuilderClickThroughQueryString) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderTypeID"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtReportBuilderName"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("txtReportBuilderJavascriptFunctions"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderFilterAccess"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderFilterNames"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnAccess"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnNames"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnSort"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnValues"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnFormats"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderColumnTotals"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderClickThrough"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("ReportBuilderClickThroughQueryString"), "", "") & ") "
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblreportbuilder")
    End Sub

    Private Sub saveReportBuilderDelete()
        Dim ReportBuilderID As String = HttpContext.Current.Request("ReportBuilderID")
        Dim strQry As String = ""
        If (checkValue(ReportBuilderID)) Then
            strQry = "DELETE FROM tblreportbuilder WHERE ReportBuilderID = '" & ReportBuilderID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblreportbuilder")
    End Sub

    Private Sub saveReportBuilderActive()
        Dim ReportBuilderID As String = HttpContext.Current.Request("ReportBuilderID")
        Dim ReportBuilderActive As String = HttpContext.Current.Request("ReportBuilderActive")
        Dim strQry As String = ""
        If (checkValue(ReportBuilderID)) Then
            If (ReportBuilderActive) Then
                strQry = "UPDATE tblreportbuilder SET  " & _
                   "ReportBuilderActive	    = 0 " & _
                   "WHERE ReportBuilderID	= '" & ReportBuilderID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblreportbuilder SET  " & _
                   "ReportBuilderActive	    = 1 " & _
                   "WHERE ReportBuilderID	= '" & ReportBuilderID & "' " & _
                   "AND CompanyID           = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
            cacheDependency(CacheObject, "tblreportbuilder")
        End If
    End Sub

    Private Sub saveImportSchedule()
        Dim ImportScheduleID As String = HttpContext.Current.Request("ImportScheduleID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        If (checkValue(ImportScheduleID)) Then
            strQry = "UPDATE tblimportschedules SET  " & _
               "ImportScheduleNumber	        = " & formatField(HttpContext.Current.Request("txtImportScheduleNumber"), "N", 0) & ", " & _
               "ImportScheduleDate	            = " & formatField(HttpContext.Current.Request("txtImportScheduleDate"), "DTTM", DefaultDateTime) & ", " & _
               "ImportScheduleCallCentreUserID	= " & formatField(HttpContext.Current.Request("ddImportScheduleCallCentreUserID"), "N", "NULL") & " " & _
               "WHERE ImportScheduleID	        = '" & ImportScheduleID & "' " & _
               "AND CompanyID                   = '" & CompanyID & "'"
            intRowsAffected = executeRowsAffectedQuery(strQry)
        End If
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblimportschedules (CompanyID, ImportScheduleSchemaID, ImportScheduleFileName, ImportScheduleNumber, ImportScheduleDate, ImportScheduleCallCentreUserID) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("ImportScheduleSchemaID"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("ImportScheduleFileName"), "", "") & ", " & _
               formatField(HttpContext.Current.Request("txtImportScheduleNumber"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("txtImportScheduleDate"), "DTTM", DefaultDateTime) & ", " & _
               formatField(HttpContext.Current.Request("ddImportScheduleCallCentreUserID"), "N", "NULL") & ") "
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblimportschedules")
    End Sub

    Private Sub runImport()
        Dim strImportFileName As String = HttpContext.Current.Request("frmImportFileName")
        Dim strSQL As String = "SELECT TOP 1 ImportScheduleSchemaID FROM tblimportschedules WHERE ImportScheduleFileName = '" & strImportFileName & "' AND ImportScheduleActive = 1 AND CompanyID = '" & CompanyID & "' ORDER BY ImportScheduleDate DESC"
        Dim ImportScheduleSchemaID As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
        Dim strQry As String = ""
        If (checkValue(ImportScheduleSchemaID)) Then
            Try
                strQry = "EXECUTE spimportdata @ImportScheduleID = '" & ImportScheduleSchemaID & "'"
                executeNonQuery(strQry)
            Catch e As Exception

            End Try
        End If
        cacheDependency(CacheObject, "tblimportschedules")
    End Sub

    Private Sub saveImportScheduleDelete()
        Dim ImportScheduleID As String = HttpContext.Current.Request("frmImportScheduleID")
        Dim strQry As String = ""
        If (checkValue(ImportScheduleID)) Then
            strQry = "UPDATE tblimportschedules SET ImportScheduleActive = 0 WHERE ImportScheduleID = '" & ImportScheduleID & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If
        cacheDependency(CacheObject, "tblimportschedules")
    End Sub

    Private Sub saveImportDelete()
        Dim ImportFileName As String = HttpContext.Current.Request("frmImportFileName")
        Dim strQry As String = ""
        If (checkValue(ImportFileName)) Then
			executeNonQuery("UPDATE tblimportdata SET ImportActive = 0 WHERE ImportFileName = '" & ImportFileName & "' AND CompanyID = '" & CompanyID & "'")			
			executeNonQuery("UPDATE tblimportschedules SET ImportScheduleActive = 0 WHERE ImportScheduleFileName = '" & ImportFileName & "' AND CompanyID = '" & CompanyID & "'")
        End If
        cacheDependency(CacheObject, "tblimportdata")
		cacheDependency(CacheObject, "tblimportschedules")
    End Sub

    Private Sub saveCacheDelete()
        Dim objCache As Caching = New Caching(CacheObject, "", "", "", 0)
        objCache.cacheDependency(HttpContext.Current.Request("strCacheKey"))
    End Sub

    Private Sub saveCacheRemove()
        Dim objCache As Caching = New Caching(CacheObject, "", "", "", 0)
        objCache.removeAll()
    End Sub

    Private Sub saveWorkflowRota()
        Dim UserID As String = HttpContext.Current.Request("WorkflowRotaUserID")
        Dim WorkflowRotaID As String = HttpContext.Current.Request("WorkflowRotaID")
        Dim strQry As String = ""
        If (strFrmAction = "add") Then
            'If (checkValue(HttpContext.Current.Request("WorkflowRotaCopyUserID"))) Then
            '    strQry = "INSERT INTO tblworkflowrotas (CompanyID, WorkflowRotaUserID, WorkflowRotaWorkflowID, WorkflowRotaStartHour, WorkflowRotaEndHour, WorkflowRotaDays) " & _
            '       "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
            '       formatField(UserID, "N", 0) & ", " & _
            '       formatField(HttpContext.Current.Request("frmWorkflowRotaWorkflowID"), "N", 0) & ", " & _
            '       formatField(HttpContext.Current.Request("frmWorkflowRotaStartHour"), "N", 0) & ", " & _
            '       formatField(HttpContext.Current.Request("frmWorkflowRotaEndHour"), "N", 0) & ", " & _
            '       formatField("," & HttpContext.Current.Request("frmWorkflowRotaDays") & ",", "", ",1,2,3,4,5,6,7,") & ") "
            '    executeNonQuery(strQry)
            'Else
            strQry = "INSERT INTO tblworkflowrotas (CompanyID, WorkflowRotaUserID, WorkflowRotaWorkflowID, WorkflowRotaStartHour, WorkflowRotaEndHour, WorkflowRotaDays) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(UserID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmWorkflowRotaWorkflowID"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmWorkflowRotaStartHour"), "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmWorkflowRotaEndHour"), "N", 0) & ", " & _
               formatField("," & HttpContext.Current.Request("frmWorkflowRotaDays") & ",", "", ",1,2,3,4,5,6,7,") & ") "
            executeNonQuery(strQry)
            'End If
            If (HttpContext.Current.Request("NewUser") = "Y") Then
                strReturnUrl = Replace(strReturnUrl, "WorkflowRotaUserID=", "WorkflowRotaUserID=" & UserID)
            End If
        Else
            Dim intRowsAffected As Integer = 0
            If (checkValue(UserID)) Then
                strQry = "UPDATE tblworkflowrotas SET  " & _
                   "WorkflowRotaWorkflowID	        = " & formatField(HttpContext.Current.Request("frmWorkflowRotaWorkflowID"), "N", 0) & ", " & _
                   "WorkflowRotaStartHour	        = " & formatField(HttpContext.Current.Request("frmWorkflowRotaStartHour"), "N", 0) & ", " & _
                   "WorkflowRotaEndHour	            = " & formatField(HttpContext.Current.Request("frmWorkflowRotaEndHour"), "N", 0) & ", " & _
                   "WorkflowRotaDays	            = " & formatField("," & HttpContext.Current.Request("frmWorkflowRotaDays") & ",", "", ",1,2,3,4,5,6,7,") & " " & _
                   "WHERE WorkflowRotaID	        = '" & WorkflowRotaID & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
            End If
            If (intRowsAffected = 0) Then
                strQry = "INSERT INTO tblworkflowrotas (CompanyID, WorkflowRotaUserID, WorkflowRotaWorkflowID, WorkflowRotaStartHour, WorkflowRotaEndHour, WorkflowRotaDays) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(UserID, "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("frmWorkflowRotaWorkflowID"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("frmWorkflowRotaStartHour"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("frmWorkflowRotaEndHour"), "N", 0) & ", " & _
                   formatField(HttpContext.Current.Request("," & "frmWorkflowRotaDays") & ",", "", ",1,2,3,4,5,6,7,") & ") "
                executeNonQuery(strQry)
            End If
        End If
    End Sub

    Private Sub saveWorkflowRotaCopy()
        Dim UserID As String = HttpContext.Current.Request("WorkflowRotaUserID")
        Dim CopyUserID As String = HttpContext.Current.Request("WorkflowRotaCopyUserID")
        Dim strQry As String = ""
        If (checkValue(UserID) And checkValue(CopyUserID)) Then
            Dim strSQL As String = "SELECT * FROM tblworkflowrotas WHERE WorkflowRotaUserID = '" & CopyUserID & "'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then
                executeNonQuery("DELETE FROM tblworkflowrotas WHERE WorkflowRotaUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'")
                For Each Row As DataRow In dsCache.Rows
                    strQry = "INSERT INTO tblworkflowrotas (CompanyID, WorkflowRotaUserID, WorkflowRotaWorkflowID, WorkflowRotaStartHour, WorkflowRotaEndHour, WorkflowRotaDays, WorkflowRotaPriority) " & _
                       "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                       formatField(UserID, "N", 0) & ", " & _
                       formatField(Row.Item("WorkflowRotaWorkflowID"), "N", 0) & ", " & _
                       formatField(Row.Item("WorkflowRotaStartHour"), "N", 0) & ", " & _
                       formatField(Row.Item("WorkflowRotaEndHour"), "N", 0) & ", " & _
                       formatField("," & Row.Item("WorkflowRotaDays") & ",", "", ",1,2,3,4,5,6,7,") & ", " & _
                       formatField(Row.Item("WorkflowRotaPriority"), "N", 0) & ") "
                    executeNonQuery(strQry)
                Next
            End If
        End If
        If (HttpContext.Current.Request("NewUser") = "Y") Then
            strReturnUrl = Replace(strReturnUrl, "WorkflowRotaUserID=", "WorkflowRotaUserID=" & UserID)
        End If
    End Sub

    Private Sub saveWorkflowRotaOrder()
        Dim strWorkflowRotaOrder As String = HttpContext.Current.Request("frmWorkflowRotaOrder")
        Dim arrOrder As Array = Split(strWorkflowRotaOrder, ",")
        Dim strQry As String = ""
        For x As Integer = 0 To UBound(arrOrder)
            If (checkValue(arrOrder(x))) Then
                strQry = "UPDATE tblworkflowrotas SET  " & _
                   "WorkflowRotaPriority	        = " & formatField(x, "N", 0) & " " & _
                   "WHERE WorkflowRotaID	        = '" & arrOrder(x) & "' " & _
                   "AND CompanyID                   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
        Next
        strReturnUrlMessage = "1"
    End Sub

    Private Sub saveWorkflowRotaActive()
        Dim WorkflowRotaID As String = HttpContext.Current.Request("WorkflowRotaID")
        Dim WorkflowRotaActive As String = HttpContext.Current.Request("WorkflowRotaActive")
        Dim strQry As String = ""
        If (checkValue(WorkflowRotaID)) Then
            If (WorkflowRotaActive) Then
                strQry = "UPDATE tblworkflowrotas SET  " & _
                   "WorkflowRotaActive	= 0 " & _
                   "WHERE WorkflowRotaID	= '" & WorkflowRotaID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            Else
                strQry = "UPDATE tblworkflowrotas SET  " & _
                   "WorkflowRotaActive	= 1 " & _
                   "WHERE WorkflowRotaID	= '" & WorkflowRotaID & "' " & _
                   "AND CompanyID   = '" & CompanyID & "'"
                executeNonQuery(strQry)
            End If
        End If
    End Sub
	
	  Private Sub addAppointment()
        Dim strQry As String = ""
        Dim strQry2 As String = ""
        Dim strQry3 As String = ""
        Dim intRowsAffected As Integer = 0
        Dim AppID As String = HttpContext.Current.Request("AppID")
        Dim AppointmentID As String = HttpContext.Current.Request("AppoinmentID")
        Dim strPost As String = ""
        If (strFrmAction = "add") Then
            If (intRowsAffected = 0) Then
                strQry = "INSERT INTO tblappointments (AppID, ClientsName, BDM, LastAppointmentDate, Notes, Outcome, AppointmentVenue, FutureAppointmentDate, BrokerSupportUserID, CreatedDate) " & _
                "VALUES(" & formatField(HttpContext.Current.Request("AppID"), "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("ClientName"), "T", "") & ", " & _
                formatField(HttpContext.Current.Request("BDMName"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("LastAppointmentDate"), "DTTM", "") & ", " & _
               formatField(HttpContext.Current.Request("Notes"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("Outcome"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("AppointmentVenue"), "T", "") & ", " & _
               formatField(HttpContext.Current.Request("FutureAppointmentDate"), "DTTM", "") & ", " & _
			   "'" & Config.DefaultUserID & "'" & ", " & _
			   "getdate()" & ") " 
                executeNonQuery(strQry)

                strQry2 = "Insert into tblbusinessobjectnotes (CompanyID, BusinessObjectID, NoteType, Note, CreatedDate, CreatedUserID, NoteActive) values (" & CompanyID & "," & formatField(HttpContext.Current.Request("AppID"), "N", 0) & ",  '2', 'Appointment Outcome:'" & formatField(HttpContext.Current.Request("Outcome"), "T", "") & "'', " & Config.DefaultDate & ", " & Config.DefaultUserID & ", '1')"
               executeNonQuery(strQry2)

                strQry3 = "Insert into tblbusinessobjectnotes (CompanyID, BusinessObjectID, NoteType, Note, CreatedDate, CreatedUserID, NoteActive) values (" & CompanyID & "," & formatField(HttpContext.Current.Request("AppID"), "N", 0) & ",  '2', 'Appointment Reason:'" & formatField(HttpContext.Current.Request("Notes"), "T", "") & "'', " & Config.DefaultDate & ", " & Config.DefaultUserID & ", '1')"
                executeNonQuery(strQry3)

               strPost = "BusinessObjectID=" & AppID & "&MediaCampaignID=11517&BusinessObjectBrokerFutureAppointmentDate=" & HttpContext.Current.Request("FutureAppointmentDate") & ""
                postWebRequest("http://positive.engagedcrm.co.uk/webservices/inbound/businessobjects/httppost.aspx", strPost)

                strReturnUrl = "/prompts/appointmentdates.aspx?BusinessObjectID=" & AppID & ""
            End If
        Else
            If (checkValue(AppointmentID)) Then
                strQry = "UPDATE tblappointments SET  " & _
                        "BDM	            = " & formatField(HttpContext.Current.Request("BDMName"), "T", "") & ", " & _
                       "ClientsName	            = " & formatField(HttpContext.Current.Request("ClientName"), "T", "") & ", " & _
                       "LastAppointmentDate	        = " & formatField(HttpContext.Current.Request("LastAppointmentDate"), "DTTM", "") & ", " & _
                       "Notes	            = " & formatField(HttpContext.Current.Request("Notes"), "T", "") & ", " & _
                       "Outcome	            = " & formatField(HttpContext.Current.Request("Outcome"), "T", "") & ", " & _
                       "AppointmentVenue	            = " & formatField(HttpContext.Current.Request("AppointmentVenue"), "T", "") & ", " & _
                       "FutureAppointmentDate	            = " & formatField(HttpContext.Current.Request("FutureAppointmentDate"), "DTTM", "") & " " & _
                       "WHERE ID                   = '" & AppointmentID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
                strReturnUrl = "/businessprocessing.aspx?Action=2&BusinessObjectID=" & AppID & ""

                strQry2 = "Insert into tblbusinessobjectnotes (CompanyID, BusinessObjectID, NoteType, Note, CreatedDate, CreatedUserID, NoteActive) values (" & CompanyID & "," & formatField(HttpContext.Current.Request("AppID"), "N", 0) & ",  '2', 'Appointment Outcome:'" & formatField(HttpContext.Current.Request("Outcome"), "T", "") & "'', " & Config.DefaultDate & ", " & Config.DefaultUserID & ", '1')"
                executeNonQuery(strQry2)

                strQry3 = "Insert into tblbusinessobjectnotes (CompanyID, BusinessObjectID, NoteType, Note, CreatedDate, CreatedUserID, NoteActive) values (" & CompanyID & "," & formatField(HttpContext.Current.Request("AppID"), "N", 0) & ",  '2', 'Appointment Reason:'" & formatField(HttpContext.Current.Request("Notes"), "T", "") & "'', " & Config.DefaultDate & ", " & Config.DefaultUserID & ", '1')"
                executeNonQuery(strQry3)


                strPost = "BusinessObjectID=" & AppID & "&MediaCampaignID=11517&BusinessObjectBrokerFutureAppointmentDate=" & HttpContext.Current.Request("FutureAppointmentDate") & ""
                postWebRequest("http://positive.engagedcrm.co.uk/webservices/inbound/businessobjects/httppost.aspx", strPost)
                strReturnUrl = "/businessprocessing.aspx?Action=2&BusinessObjectID=" & AppID & ""
                strReturnUrlMessage = "Appointment Updated"
            End If
        End If



    End Sub

End Class