﻿Imports Config, Common
Imports Microsoft.VisualBasic

Public Class Prompts

    Private objLeadPlatform As LeadPlatform = Nothing

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

End Class