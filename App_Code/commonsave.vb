﻿Imports Config, Common, CallInterface
Imports System.Net
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Reflection

' ** Revision history **
'
' 30/01/2012    - saveApplicationActive and saveApplicationReset both updated to allow multiple cases to be updated at the same time
' 15/02/2012    - live transfer disabled and placed in store procedure
' 17/02/2012    - tblunderwriting removed
' 09/03/2012	- Duplicate checker added to SOAP insert
' 23/08/2012    - Create customer Logon added
' ** End Revision History **

Public Class CommonSave

    Shared Function saveApplication(ByVal soap As Boolean, ByVal AppID As String, ByVal UserID As String, ByVal CompanyID As String, ByVal MediaCampaignID As String, ByVal MediaCampaignIDOutbound As String, ByVal skiptel As String, _
                                    ByVal tblapplications_flds As ArrayList, _
                                    ByVal tblapplications_vals As ArrayList, _
                                    ByVal tblapplicationstatus_flds As ArrayList, _
                                    ByVal tblapplicationstatus_vals As ArrayList, _
                                    ByVal tbladdresses_flds As ArrayList, _
                                    ByVal tbladdresses_vals As ArrayList, _
                                    ByVal tblemployers_flds As ArrayList, _
                                    ByVal tblemployers_vals As ArrayList, _
                                    ByVal tblnotes_flds As ArrayList, _
                                    ByVal tblnotes_vals As ArrayList, _
                                    ByVal tblreminders_flds As ArrayList, _
                                    ByVal tblreminders_vals As ArrayList, _
                                    ByVal tbldatastore_flds As ArrayList, _
                                    ByVal tbldatastore_vals As ArrayList, _
                                    ByVal tblapplicationstatusdates_flds As ArrayList, _
                                    ByVal tblapplicationstatusdates_vals As ArrayList, _
                                    ByVal tbldiaries_flds As ArrayList, _
                                    ByVal tbldiaries_vals As ArrayList, _
									ByVal tbladversecredit_flds As ArrayList, _
									ByVal tbladversecredit_vals As ArrayList, _
                                    Optional ByVal tblcustomerlogons_flds As ArrayList = Nothing, _
                                    Optional ByVal tblcustomerlogons_vals As ArrayList = Nothing, _
                                    Optional ByVal interactive As String = "") As String
 		

        Dim boolInsert As Boolean = False
        Dim strSessionID As String = HttpContext.Current.Request("SessionID")
        Dim boolLiveTransfer As Boolean = getAnyField("MediaCampaignAutomaticTransfer", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
        Dim strMediaCampaignProductType As String = getAnyField("MediaCampaignProductType", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)

        Dim strQryFieldsValues As String = "", strQryFields As String = "", strQryValues As String = ""
        Dim strProductType As String = ""

        If (Not checkValue(UserID)) Then UserID = returnSystemUser(CompanyID)

        Dim strSQL As String = "", dsCache As DataTable = Nothing

        If (Not checkValue(AppID)) Then
            strSQL = "EXECUTE spcreateapplication @MediaCampaignID = " & MediaCampaignID & ", @CompanyID = " & CompanyID & ", @UserID = " & UserID
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                AppID = objResult.ToString
                boolInsert = True
            End If
            objResult = Nothing
            objDatabase = Nothing
        End If

        ' Only start update if main application array is found

        If (tblapplications_flds.Count > 0) Then

            For i As Integer = 0 To tblapplications_flds.Count - 1
                If (i = 0) Then
                    If (skiptel <> 1) Then skiptel = 1 ' Set variable to skip the update unless a telephone number is found
                    strQryFieldsValues = "UPDATE tblapplications SET "
                    'If boolInsert Then ' Add MediaCampaignCost
                    '    strQryFieldsValues = strQryFieldsValues & " MediaCampaignCostInbound = '" & getAnyField("MediaCampaignCostInbound", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID) & "', "
                    'End If
                End If
                If (tblapplications_flds(i) = "App1HomeTelephone") Or (tblapplications_flds(i) = "App1MobileTelephone") Or (tblapplications_flds(i) = "App1WorkTelephone") Then skiptel = 0
                If (tblapplications_flds(i) = "ProductType") Then strProductType = tblapplications_flds(i)

                strQryFieldsValues = strQryFieldsValues & tblapplications_flds(i) & " = " & tblapplications_vals(i)
                If (i <> tblapplications_flds.Count - 1) Then
                    strQryFieldsValues = strQryFieldsValues & ", "
                Else
                    strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'" ' WHERE (" & strAppIDs & ") AND CompanyID = '" & CompanyID & "'"
                End If
            Next
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""

            ' Set the default product if not found
            If (boolInsert And Not checkValue(strProductType)) Then
                strQryFieldsValues = "UPDATE tblapplications SET ProductType = '" & strMediaCampaignProductType & "' WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'"
                executeNonQuery(strQryFieldsValues)
            End If

            If (boolInsert) Then
                executeNonQuery("UPDATE tblapplications SET MediaCampaignCostInbound = '" & getAnyField("MediaCampaignCostInbound", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID) & "' WHERE MediaCampaignCostInbound = 0 AND AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")
            End If

            ' Correct telephone numbers- only need to run for an insert and when telephone fields updated
            If (skiptel <> "1") Then
                executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)
            End If

            ' LTV - only run for new mortgages or updates
            If ((InStr(strMediaCampaignProductType, "Mortgage") Or InStr(strMediaCampaignProductType, "Secured")) And boolInsert) Or (boolInsert = False) Then
                executeNonQuery("EXECUTE spupdateltv @AppID = " & AppID & ", @CompanyID = " & CompanyID)
            End If

            ' Original amount
            If (boolInsert) Then
                executeNonQuery("UPDATE tblapplications SET AmountOriginal = Amount WHERE AmountOriginal = 0 AND AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")
            End If

            'Set working hour
            If (checkWorkingHour(Config.DefaultDateTime)) Then
                executeNonQuery("UPDATE tblapplicationstatus SET CreatedInsideBusinessHours = 1 WHERE (AppID = '" & AppID & "')")
            End If

            'Set country & geolocate by IP address
            'Dim strGeoLocation As String = New Caching(Nothing, "SELECT SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationName = 'GeoLocation' AND SystemConfigurationActive = 1 AND CompanyID = '" & CompanyID & "'", "", "", "").returnCacheString
            'If (strGeoLocation = "Y") Then
            'getWebRequest(Config.ApplicationURL & "/webservices/outbound/geolocation/?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&ip=" & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
            'Call executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT PostCodeCountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')), 'ENG') WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')")
            'End If

        End If

        ' Update application status table
        If (tblapplicationstatus_flds.Count > 0) Then
            For i As Integer = 0 To tblapplicationstatus_flds.Count - 1
                If (i = 0) Then
                    strQryFieldsValues = "UPDATE tblapplicationstatus SET UpdatedDate = GETDATE(), "
                End If

                If (tblapplicationstatus_flds(i) = "BestContactDate") Then
                    Dim dteBestContactDate As Date = CDate(Replace(tblapplicationstatus_vals(i), "'", ""))
                    If (DateDiff(DateInterval.Minute, dteBestContactDate, Config.DefaultDateTime) > 0) Then
                        If (DateDiff(DateInterval.Hour, CDate(Config.DefaultDate & " " & dteBestContactDate.Hour & ":00:00"), Config.DefaultDateTime) > 5) Then
                            dteBestContactDate = CDate(Config.DefaultDate.AddDays(1) & " " & dteBestContactDate.Hour & ":00:00")
                        Else
                            dteBestContactDate = CDate(Config.DefaultDate & " " & dteBestContactDate.Hour & ":00:00")
                        End If
                    End If
                    strQryFieldsValues = strQryFieldsValues & "NextCallDate = " & formatField(dteBestContactDate, "DTTM", Config.DefaultDateTime) & ", SubStatusCode = 'CBK' "
                Else
                    strQryFieldsValues = strQryFieldsValues & tblapplicationstatus_flds(i) & " = " & tblapplicationstatus_vals(i)
                End If

                If (i <> tblapplicationstatus_flds.Count - 1) Then
                    strQryFieldsValues = strQryFieldsValues & ", "
                Else
                    strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'" ' WHERE (" & strAppIDs & ") AND CompanyID = '" & CompanyID & "'"
                End If
            Next
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""

        Else ' Update UpdatedDate incorporated in query above
            strQryFieldsValues = "UPDATE tblapplicationstatus SET UpdatedDate = GETDATE() WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""
        End If

        '' Update previous address table
        'If (tbladdresses_flds.Count > 0) Then
        '    For i As Integer = 0 To tbladdresses_flds.Count - 1
        '        If (i = 0) Then
        '            strQryFieldsValues = "UPDATE tbladdresses SET "
        '        End If

        '        strQryFieldsValues = strQryFieldsValues & tbladdresses_flds(i) & " = " & tbladdresses_vals(i)
        '        If (i <> tbladdresses_flds.Count - 1) Then
        '            strQryFieldsValues = strQryFieldsValues & ", "
        '        Else
        '            strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'" ' WHERE (" & strAppIDs & ") AND CompanyID = '" & CompanyID & "'"
        '        End If
        '    Next
        '    Call executeNonQuery(strQryFieldsValues)
        '    strQryFieldsValues = ""
        'End If

        '' Update previous employment table
        'If (tblemployers_flds.Count > 0) Then
        '    For i As Integer = 0 To tblemployers_flds.Count - 1
        '        If (i = 0) Then
        '            strQryFieldsValues = "UPDATE tblemployers SET "
        '        End If

        '        strQryFieldsValues = strQryFieldsValues & tblemployers_flds(i) & " = " & tblemployers_vals(i)
        '        If (i <> tblemployers_flds.Count - 1) Then
        '            strQryFieldsValues = strQryFieldsValues & ", "
        '        Else
        '            strQryFieldsValues = strQryFieldsValues & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'" ' WHERE (" & strAppIDs & ") AND CompanyID = '" & CompanyID & "'"
        '        End If
        '    Next
        '    Call executeNonQuery(strQryFieldsValues)
        '    strQryFieldsValues = ""
        'End If

        ' Insert notes
        If (tblnotes_flds.Count > 0) Then
            For i As Integer = 0 To tblnotes_flds.Count - 1
                If (i = 0) Then
                    strQryFields = "INSERT INTO tblnotes (CompanyID, AppID, CreatedUserID, "
                    strQryValues = "VALUES ('" & CompanyID & "','" & AppID & "','" & returnSystemUser(CompanyID) & "', "
                End If

                strQryFields = strQryFields & tblnotes_flds(i)
                strQryValues = strQryValues & tblnotes_vals(i)

                If (i <> tblnotes_flds.Count - 1) Then
                    strQryFields = strQryFields & ", "
                    strQryValues = strQryValues & ", "
                Else
                    strQryFields = strQryFields & ") "
                    strQryValues = strQryValues & ") "
                End If
            Next
            Call executeNonQuery(strQryFields & strQryValues)
            strQryFields = ""
            strQryValues = ""
        End If

        ' Insert reminders
        If (tblreminders_flds.Count > 0) Then
            For i As Integer = 0 To tblreminders_flds.Count - 1
                If (i = 0) Then
                    strQryFields = "INSERT INTO tblreminders (CompanyID, AppID, ReminderAssignedByUserID, "
                    strQryValues = "VALUES ('" & CompanyID & "'," & AppID & ",'" & returnSystemUser(CompanyID) & "', "
                End If

                strQryFields = strQryFields & tblreminders_flds(i)
                strQryValues = strQryValues & tblreminders_vals(i)

                If (i <> tblreminders_flds.Count - 1) Then
                    strQryFields = strQryFields & ", "
                    strQryValues = strQryValues & ", "
                Else
                    strQryFields = strQryFields & ") "
                    strQryValues = strQryValues & ") "
                End If
            Next
            Call executeNonQuery(strQryFields & strQryValues)
            strQryFields = ""
            strQryValues = ""
        End If

        ' Insert stored data
        If (tbldatastore_flds.Count > 0) Then
            For i As Integer = 0 To tbldatastore_flds.Count - 1
                Dim intRowsAffected As Integer = 0
                If Not boolInsert Then ' 12/01/2012 - No need to run this when inserting
                    strQryFieldsValues = "UPDATE tbldatastore SET StoredDataValue = " & tbldatastore_vals(i) & " WHERE StoredDataName = '" & tbldatastore_flds(i) & "' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                    intRowsAffected = executeRowsAffectedQuery(strQryFieldsValues)
                End If
                If (intRowsAffected = 0) Then
                    strQryFieldsValues = "INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & tbldatastore_flds(i) & "'," & tbldatastore_vals(i) & ")"
                    Call executeNonQuery(strQryFieldsValues)
                End If
				'ResponseWrite(strQryFieldsValues)
				'ResponseEnd()
            Next
        End If

        ' Insert milestones
        If (tblapplicationstatusdates_flds.Count > 0) Then
            For i As Integer = 0 To tblapplicationstatusdates_flds.Count - 1
                If (checkValue(tblapplicationstatusdates_vals(i))) Then
                    saveUpdatedDate(AppID, UserID, tblapplicationstatusdates_flds(i), tblapplicationstatusdates_vals(i), CompanyID)
                Else
                    clearUpdatedDate(AppID, tblapplicationstatusdates_flds(i), CompanyID)
                End If
            Next
        End If
		
		  ' Insert Adverse Credit
		 
        If (tbladversecredit_flds.Count > 0) Then
            For i As Integer = 0 To tbladversecredit_flds.Count - 1
                Dim intRowsAffected As Integer = 0
                If Not boolInsert Then ' 12/01/2012 - No need to run this when inserting
                    strQryFieldsValues = "UPDATE tbladversecredit SET " & tbladversecredit_flds(i) & " = " & tbladversecredit_vals(i) & " WHERE AppID = " & AppID 
                    intRowsAffected = executeRowsAffectedQuery(strQryFieldsValues)			
                End If
                If (intRowsAffected = 0) Then
                    strQryFieldsValues = "INSERT INTO tbladversecredit (AppID, " & tbladversecredit_flds(i) & ") VALUES (" & AppID & ",'" & tbladversecredit_vals(i) & "')"
                 	Call executeNonQuery(strQryFieldsValues)
                End If
				
				
            Next
        End If

        ' Insert diary
        If (tbldiaries_flds.Count > 0) Then
            For i As Integer = 0 To tbldiaries_flds.Count - 1
                Dim strDiaryTypeDescription As String = ""
                strSQL = "SELECT DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeName = '" & tbldiaries_flds(i) & "' AND CompanyID = '" & CompanyID & "'"
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                    Next
                End If
                dsCache = Nothing
                Dim intRowsAffected As Integer = 0
                strQryFieldsValues = "UPDATE tbldiaries SET DiaryDueDate = " & tbldiaries_vals(i) & ", DiaryType = '" & tbldiaries_flds(i) & "', DiaryTypeDescription = '" & strDiaryTypeDescription & "' WHERE DiaryType = '" & tbldiaries_flds(i) & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQryFieldsValues)
                If (intRowsAffected = 0) Then
                    strQryFieldsValues = "INSERT INTO tbldiaries (CompanyID, AppID, DiaryDueDate, DiaryType, DiaryTypeDescription) VALUES ('" & CompanyID & "'," & AppID & "," & tbldiaries_vals(i) & ",'" & tbldiaries_flds(i) & "','" & strDiaryTypeDescription & "')"
                    Call executeNonQuery(strQryFieldsValues)
                End If
            Next
        End If

        'Set ip address
        'If (boolInsert) Then
        Dim strIPAddress As String = HttpContext.Current.Request("ApplicationIPAddress")
        If (Not checkValue(strIPAddress)) Then strIPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        If (Not checkValue(strSessionID)) Then strSessionID = System.Guid.NewGuid().ToString
        If (checkValue(strIPAddress)) Then
            executeNonQuery("UPDATE tblapplications SET ApplicationIPAddress = '" & strIPAddress & "' WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'")
        End If
        executeNonQuery("UPDATE tblapplications SET ApplicationSessionID = '" & strSessionID & "' WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "' AND ApplicationSessionID = ''")
        'End If

        'If (tblcustomerlogons_flds.Count > 0) Then
        '    For i As Integer = 0 To tblapplications_flds.Count - 1
        '        If (i = 0) Then
        '            strQryFieldsValues = "UPDATE tblcustomerlogons SET "
        '        End If
        '        strQryFieldsValues = strQryFieldsValues & tblapplications_flds(i) & " = " & tblapplications_vals(i)
        '        If (i <> tblapplications_flds.Count - 1) Then
        '            strQryFieldsValues = strQryFieldsValues & ", "
        '        Else
        '            strQryFieldsValues = strQryFieldsValues & " WHERE CustomerID = '" & HttpContext.Current.Request("CustomerID") & "'"
        '        End If
        '    Next
        '    Call executeNonQuery(strQryFieldsValues)
        '    strQryFieldsValues = ""
        'End If

        ' 13/01/2012 - moved to tblapplicationstatus update
        'strQryFieldsValues = "UPDATE tblapplicationstatus SET UpdatedDate = GETDATE() WHERE AppID = '" & AppID & "' AND  CompanyID = '" & CompanyID & "'"
        'Call executeNonQuery(strQryFieldsValues)

        ' Set COA - 12/01/2012 - moved to tblapplications update
        'Call executeNonQuery("EXECUTE spcostofacquisition @AppID = " & AppID & ", @CompanyID = " & CompanyID)

        ' Set LTV - 13/01/2012 - only run for new mortgages or updates - 17/01/2012 - moved to tblapplications update
        'If (InStr(strMediaCampaignProductType, "Mortgage") And (boolInsert)) Or (boolInsert = False) Then
        '    Call executeNonQuery("EXECUTE spupdateltv @AppID = " & AppID & ", @CompanyID = " & CompanyID)
        'End If

        ' Set BTL Yield - 13/01/2012 - removed
        'Call executeNonQuery("EXECUTE spupdatebtlyield @AppID = " & AppID & ", @CompanyID = " & CompanyID)

        ' Correct telephone numbers- - 17/01/2012 - moved to tblapplications update
        'If (skiptel <> "1") Then
        '    Call executeNonQuery("EXECUTE sptelephonenumbercorrection @AppID = " & AppID & ", @CompanyID = " & CompanyID)
        'End If

        ' Set CountryCode - move to tblapplications - consider removing.
        'Call executeNonQuery("UPDATE tblapplications SET AddressCountry = ISNULL ((SELECT (SELECT PostCodeCountryCode FROM tblpostcodelookup WHERE (PostCodePrefix = LEFT(tblapplications_1.AddressPostCode, CASE WHEN PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) = 0 THEN 1 ELSE PATINDEX('%[0-9]%', tblapplications_1.AddressPostCode) END - 1))) AS AddressCountry FROM tblapplications AS tblapplications_1 WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')), 'ENG') WHERE (AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "')")

        If (boolInsert) Then
            If (boolLiveTransfer) Then ' 12/01/2012 - trap added
                'liveLeadTransfer(CompanyID, AppID, MediaCampaignID, UserID) ' 15/02/2012 - removed
            End If
            If (checkValue(MediaCampaignIDOutbound)) Then
                'cloneTransfer(CompanyID, AppID, MediaCampaignID, MediaCampaignIDOutbound, UserID)
            End If
            'Run blacklist when a live application is received
            Dim strBlackListChecker As String = New Caching(Nothing, "SELECT SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationName = 'BlacklistChecker' AND SystemConfigurationActive = 1 AND CompanyID = '" & CompanyID & "'", "", "", "").returnCacheString
            If (strBlackListChecker = "Y") Then
                Call executeNonQuery("EXECUTE spblacklistcheck @AppID = " & formatField(AppID, "N", 0) & ", @CompanyID = " & CompanyID)
            End If
            'Run duplicate check when a live application is received
            Dim strDuplicateChecker As Boolean = getAnyField("MediaCampaignDuplicateChecker", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
            If (strDuplicateChecker) Then
                Call executeNonQuery("EXECUTE spduplicateappcheck @AppID = " & formatField(AppID, "N", 0) & ", @CompanyID = " & CompanyID)
            End If
            ' Create customer logon
            Dim strStatusCode As String = "", strSubStatusCode As String = ""
            strStatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
            If (strStatusCode <> "INV" And strStatusCode <> "BLA") Then
                Dim strCustomerLogon As String = New Caching(Nothing, "SELECT SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationName = 'CustomerLogon' AND SystemConfigurationActive = 1 AND CompanyID = '" & CompanyID & "'", "", "", "").returnCacheString
                If (strCustomerLogon = "Y") Then
                    If (boolInsert) Then
                        createCustomerLogon(AppID)
                    End If
                End If
            End If
            ' Send customer communication
            'Dim intLetterID As String = getAnyField("MediaCampaignLetterID", "tblmediacampaigns", "MediaCampaignID", MediaCampaignID)
            'If (checkValue(intLetterID)) Then
            '    If (CInt(intLetterID) > 0) Then
            '        Dim intLetterType As String = getAnyField("LetterType", "tbllettertemplates", "LetterID", intLetterID)
            '        If (intLetterType = 2) Then
            '            sendEmails(AppID, intLetterID, CompanyID)
            '        ElseIf (intLetterType = 3) Then
            '            sendSMS(AppID, intLetterID)
            '        End If
            '    End If
            'End If
            If (soap) Then
                Return AppID
            Else
                If (interactive = "Y") Then
                    Return "1|" & AppID & "|" & strSessionID
                Else
                    Return "1|" & AppID
                End If
            End If
        Else
            Return "1"
        End If

    End Function

    Shared Function saveBusinessObject(ByVal BusinessObjectID As String, ByVal UserID As String, ByVal CompanyID As String, ByVal MediaCampaignID As String, ByVal BusinessObjectTypeID As String, ByVal ProductTypeID As String, _
                                    ByVal tblapplications_flds As ArrayList, _
                                    ByVal tblapplications_vals As ArrayList, _
                                    ByVal tblapplicationstatus_flds As ArrayList, _
                                    ByVal tblapplicationstatus_vals As ArrayList, _
                                    ByVal tbldatastore_flds As ArrayList, _
                                    ByVal tbldatastore_vals As ArrayList _
                                    ) As String

        Dim boolInsert As Boolean = False

        Dim strQryFieldsValues As String = "", strQryFields As String = "", strQryValues As String = ""
   		if (Not returnSystemUser(CompanyID)) then
		If (Not checkValue(UserID)) Then UserID = 0
		else
        If (Not checkValue(UserID)) Then UserID = returnSystemUser(CompanyID)
		end if

        Dim strSQL As String = "", dsCache As DataTable = Nothing

        If (Not checkValue(BusinessObjectID)) Then
            strSQL = "EXECUTE spcreatebusinessobject @MediaCampaignID = " & MediaCampaignID & ", @CompanyID =" & CompanyID & ", @UserID = " & UserID & ", @BusinessObjectTypeID= " & 				BusinessObjectTypeID & ", @ProductTypeID=" & ProductTypeID & ""
		
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                BusinessObjectID = objResult.ToString
                boolInsert = True
            End If
            objResult = Nothing
            objDatabase = Nothing
        End If

        ' Only start update if main application array is found

        If (tblapplications_flds.Count > 0) Then

            For i As Integer = 0 To tblapplications_flds.Count - 1
                If (i = 0) Then
                    strQryFieldsValues = "UPDATE tblbusinessobjects SET "
                End If

                strQryFieldsValues = strQryFieldsValues & tblapplications_flds(i) & " = " & tblapplications_vals(i)
                If (i <> tblapplications_flds.Count - 1) Then
                    strQryFieldsValues = strQryFieldsValues & ", "
                Else
                    strQryFieldsValues = strQryFieldsValues & " WHERE BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "'"
                End If
            Next
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""

        End If

        ' Update application status table
        If (tblapplicationstatus_flds.Count > 0) Then
            For i As Integer = 0 To tblapplicationstatus_flds.Count - 1
                If (i = 0) Then
                    strQryFieldsValues = "UPDATE tblbusinessobjectstatus SET BusinessObjectUpdatedDate = GETDATE(), "
                End If

                strQryFieldsValues = strQryFieldsValues & tblapplicationstatus_flds(i) & " = " & tblapplicationstatus_vals(i)
                If (i <> tblapplicationstatus_flds.Count - 1) Then
                    strQryFieldsValues = strQryFieldsValues & ", "
                Else
                    strQryFieldsValues = strQryFieldsValues & " WHERE BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "'"
                End If
            Next
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""
        Else ' Update UpdatedDate incorporated in query above
            strQryFieldsValues = "UPDATE tblbusinessobjectstatus SET BusinessObjectUpdatedDate = GETDATE() WHERE BusinessObjectID = '" & BusinessObjectID & "' AND  CompanyID = '" & CompanyID & "'"
            Call executeNonQuery(strQryFieldsValues)
            strQryFieldsValues = ""
        End If

        ' Insert stored data
        If (tbldatastore_flds.Count > 0) Then
            For i As Integer = 0 To tbldatastore_flds.Count - 1
                Dim intRowsAffected As Integer = 0
                If Not boolInsert Then ' 12/01/2012 - No need to run this when inserting
                    strQryFieldsValues = "UPDATE tbldatastore SET StoredDataValue = " & tbldatastore_vals(i) & " WHERE StoredDataName = '" & tbldatastore_flds(i) & "' AND AppID = " & BusinessObjectID & " AND CompanyID = '" & CompanyID & "'"
                    intRowsAffected = executeRowsAffectedQuery(strQryFieldsValues)
                End If
                If (intRowsAffected = 0) Then
                    strQryFieldsValues = "INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & BusinessObjectID & ",'" & tbldatastore_flds(i) & "'," & tbldatastore_vals(i) & ")"
                    Call executeNonQuery(strQryFieldsValues)
                End If
                'ResponseWrite(strQryFieldsValues)
                'ResponseEnd()
            Next
        End If

        If (boolInsert) Then
            Return "1|" & BusinessObjectID
        Else
            Return "1"
        End If

    End Function

    Shared Sub correctDataStoreGrid(ByVal AppID As String, ByVal strGridName As String)
        Dim intMaxLength As Integer = 0
        Dim strSQL As String = "SELECT     ValName, " & _
            "(SELECT     TOP (1) StoredDataValue " & _
            "FROM tbldatastore  " & _
            "WHERE      (StoredDataName = tblimportvalidation.ValName) AND (AppID = '" & AppID & "')) AS StoredDataValue, ISNULL  " & _
            "((SELECT     TOP (1) LEN(StoredDataValue) - LEN(REPLACE(StoredDataValue, ',', '')) AS Expr1  " & _
            "FROM         tbldatastore AS tbldatastore_1  " & _
            "WHERE     (StoredDataName = tblimportvalidation.ValName) AND (AppID = '" & AppID & "')), 0) AS Length " & _
            "FROM         tblimportvalidation " & _
            "WHERE     (ApplicationDataGridTableClass = N'" & strGridName & "') AND (ValStatus = 'A') AND (CompanyID = '" & CompanyID & "') " & _
            "ORDER BY Length DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            Dim strDataStoreValue As String = ""
            Dim strAdditioanlDataValue As String = ""
            Dim intRowsAffected As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                strDataStoreValue = Row.item("StoredDataValue").ToString
                If (x = 0) Then
                    intMaxLength = Row.item("Length")
                End If
                If (intMaxLength > Row.item("Length")) Then
                    For k As Integer = Row.Item("Length") To intMaxLength - 1
                        strAdditioanlDataValue += ","
                    Next
                    strDataStoreValue = strDataStoreValue & strAdditioanlDataValue
                End If
                intRowsAffected = executeRowsAffectedQuery("UPDATE tbldatastore SET StoredDataValue = '" & strDataStoreValue & "' WHERE StoredDataName = '" & Row.item("ValName") & "' AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'")
                If (intRowsAffected = 0) Then
                    Call executeNonQuery("INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES ('" & CompanyID & "'," & AppID & ",'" & Row.item("ValName") & "','" & strDataStoreValue & "')")
                End If
                strAdditioanlDataValue = ""
                intRowsAffected = 0
                x += 1
            Next
        End If
    End Sub

    Shared Sub liveLeadTransfer(ByVal CompanyID As String, ByVal AppID As String, ByVal MediaCampaignID As String, ByVal UserID As String)
        Dim strURL As String = ""
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignID, MediaCampaignCampaignReference, MediaCampaignScheduleID, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress " & _
                            "FROM vwdistributionsearchlive " & _
                            "WHERE (MediaCampaignAutomaticTransfer = 1) AND (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') AND CompanyID = '" & CompanyID & "' " & _
                            "ORDER BY MediaCampaignScheduleRequiredRate DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "vwdistributionsearchlive")
        Dim dsHotkey As DataTable = objDataSet.Tables("vwdistributionsearchlive")
        If (dsHotkey.Rows.Count > 0) Then
            For Each Row As DataRow In dsHotkey.Rows
                If (Row.Item("MediaCampaignDeliveryTypeID") = "2") Then ' XML Transfer
                    If (checkValue(Row.Item("MediaCampaignDeliveryXMLPath"))) Then
                        strURL = ApplicationURL & "/webservices/outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & AppID & "&MediaCampaignID=" & MediaCampaignID & "&MediaCampaignIDOutbound=" & Row.Item("MediaCampaignID") & "&MediaCampaignCampaignReference=" & Row.Item("MediaCampaignCampaignReference") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&SalesUserID=" & UserID & "&HotkeyUserID=" & UserID & "&UserSessionID=" & getAnyField("UserSessionID", "tblusers", "UserID", UserID)
                    End If
                Else ' Email / Email & CSV
                    strURL = ApplicationURL & "/webservices/outbound/endhotkey.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&MediaCampaignIDOutbound=" & Row.Item("MediaCampaignID") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&MediaCampaignDeliveryEmailAddress=" & Row.Item("MediaCampaignDeliveryEmailAddress") & "&intActionType=" & Row.Item("MediaCampaignDeliveryTypeID") & "&SalesUserID=" & UserID & "&HotkeyUserID=" & UserID & "&UserSessionID=" & getAnyField("UserSessionID", "tblusers", "UserID", UserID)
                End If
                If (checkValue(strURL)) Then
                    getWebRequest(strURL)
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsHotkey = Nothing
        objDataBase = Nothing
    End Sub

    Shared Sub cloneTransfer(ByVal CompanyID As String, ByVal AppID As String, ByVal MediaCampaignID As String, ByVal MediaCampaignIDOutbound As String, ByVal UserID As String)
        Dim strURL As String = ""
        Dim strSQL As String = "SELECT TOP 1 MediaCampaignID, MediaCampaignCampaignReference, MediaCampaignScheduleID, MediaCampaignDeliveryTypeID, MediaCampaignDeliveryXMLPath, MediaCampaignDeliveryEmailAddress " & _
                            "FROM vwdistributionclone " & _
                            "WHERE (MediaCampaignDeliveryBatch = 0) AND (AppID = '" & AppID & "') AND MediaCampaignID = '" & MediaCampaignIDOutbound & "' AND CompanyID = '" & CompanyID & "' " & _
                            "ORDER BY MediaCampaignScheduleRequiredRate DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "vwdistributionclone")
        Dim dsHotkey As DataTable = objDataSet.Tables("vwdistributionclone")
        If (dsHotkey.Rows.Count > 0) Then
            For Each Row As DataRow In dsHotkey.Rows
                If (Row.Item("MediaCampaignDeliveryTypeID") = "2") Then ' XML Transfer
                    If (checkValue(Row.Item("MediaCampaignDeliveryXMLPath"))) Then
                        strURL = ApplicationURL & "/webservices/outbound/" & Row.Item("MediaCampaignDeliveryXMLPath") & AppID & "&MediaCampaignID=" & MediaCampaignID & "&MediaCampaignIDOutbound=" & Row.Item("MediaCampaignID") & "&MediaCampaignCampaignReference=" & Row.Item("MediaCampaignCampaignReference") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&SalesUserID=" & UserID & "&HotkeyUserID=" & UserID & "&UserSessionID=" & getAnyField("UserSessionID", "tblusers", "UserID", UserID)
                    End If
                Else ' Email / Email & CSV / Sales
                    strURL = ApplicationURL & "/webservices/outbound/endhotkey.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&MediaCampaignIDOutbound=" & Row.Item("MediaCampaignID") & "&MediaCampaignScheduleID=" & Row.Item("MediaCampaignScheduleID") & "&MediaCampaignDeliveryEmailAddress=" & Row.Item("MediaCampaignDeliveryEmailAddress") & "&intActionType=" & Row.Item("MediaCampaignDeliveryTypeID") & "&SalesUserID=" & UserID & "&HotkeyUserID=" & UserID & "&UserSessionID=" & getAnyField("UserSessionID", "tblusers", "UserID", UserID)
                End If
                If (checkValue(strURL)) Then
                    getWebRequest(strURL)
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsHotkey = Nothing
        objDataBase = Nothing
    End Sub

    'Shared Sub saveAdditionalFields(ByVal AppID As String)
    '    Dim strQry As String = "", strScriptID As String = ""
    '    Dim intRowsAffected As Integer = 0
    '    For Each Item As String In HttpContext.Current.Request.QueryString
    '        If (Left(Item, 6) = "Script") Then
    '            strScriptID = Replace(Item, "Script", "")
    '            strQry = "UPDATE tblapplicationscriptanswers SET " & _
    '            "ScriptAnswer = " & formatField(HttpContext.Current.Request(Item), "", "") & " " & _
    '           "WHERE ScriptID = '" & strScriptID & "' AND AppID = '" & AppID & "'"
    '            intRowsAffected = executeRowsAffectedQuery(strQry)
    '            If (intRowsAffected = 0) Then
    '                strQry = "INSERT INTO tblapplicationscriptanswers (AppID, ScriptID, ScriptAnswer) " & _
    '                   "VALUES(" & formatField(AppID, "N", 0) & ", " & _
    '                   formatField(strScriptID, "N", 0) & ", " & _
    '                   formatField(HttpContext.Current.Request(Item), "", "") & ")"
    '                executeNonQuery(strQry)
    '            End If
    '        End If
    '    Next
    'End Sub

    Shared Sub saveApplicationActive(AppID As String)
        Dim strSQL As String = ""
        Dim arrAppID As Array = Split(AppID, ",") ' 30/01/2012 - Updated to allow multiple cases to be changed at the same time
        For i As Integer = 0 To UBound(arrAppID)
            strSQL = "UPDATE tblapplicationstatus SET Active = 0 WHERE AppID = '" & arrAppID(i) & "' AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        Next
    End Sub

    Shared Sub saveBusinessActive(BusinessObjectID As String)
        Dim strSQL As String = ""
        Dim arrAppID As Array = Split(BusinessObjectID, ",") ' 30/01/2012 - Updated to allow multiple cases to be changed at the same time
        For i As Integer = 0 To UBound(arrAppID)
            strSQL = "UPDATE tblbusinessobjectstatus SET BusinessObjectActive = 0 WHERE BusinessObjectID = '" & arrAppID(i) & "' AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        Next
    End Sub

    Shared Sub saveApplicationReset(AppID As String)
        Dim strSQL As String = ""
        Dim arrAppID As Array = Split(AppID, ",") ' 30/01/2012 - Updated to allow multiple cases to be changed at the same time
        For i As Integer = 0 To UBound(arrAppID)
            strSQL = "UPDATE tblapplicationstatus SET " & _
                            "StatusCode             = " & formatField("NEW", "U", "NEW") & ", " & _
                            "SubStatusCode          = " & formatField("", "U", "NULL") & ", " & _
                            "CallCentreUserID       = " & formatField(0, "N", 0) & ", " & _
                            "SalesUserID            = " & formatField(0, "N", 0) & ", " & _
                            "AdministratorUserID    = " & formatField(0, "N", 0) & " " & _
                            "WHERE AppID            = " & arrAppID(i) & " " & _
                            "AND CompanyID          = '" & CompanyID & "'"
            executeNonQuery(strSQL)
            insertStatusHistory(arrAppID(i), "NEW", "", Config.DefaultUserID)
            strSQL = "UPDATE tblapplicationstatusdates SET  " & _
                        "ApplicationStatusDate	            = " & formatField("", "DTTM", "NULL") & ", " & _
                        "ApplicationStatusDateUserID	    = " & formatField(0, "N", 0) & " " & _
                        "WHERE ApplicationStatusDateAppID	= '" & arrAppID(i) & "' AND CompanyID = '" & CompanyID & "'"
            executeNonQuery(strSQL)
        Next
    End Sub

    Shared Sub saveStatus(ByVal AppID As String, ByVal UserID As String, ByVal st As String, ByVal sst As String, Optional ByVal WorkflowID As String = "", Optional ByVal WorkflowDiary As String = "N", Optional ByVal DiaryID As String = "", Optional intCompanyID As String = "")
        Dim strExistingStatusCode As String = "", strExistingSubStatusCode As String = "", strExistingCallBackDate As String = "", strDialableStatus As String = ""
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        If (sst = "WRN") Then
            executeNonQuery("EXECUTE spmarkinvalidtelephone @AppID = " & AppID & ", @UserID = " & UserID & ", @CompanyID = " & intCompanyID)
            strDialableStatus = getAnyField("DialableStatus", "tblapplicationstatus", "AppID", AppID)
            If (strDialableStatus = "000") Then
                st = "INV"
            Else
                'callLogging(AppID, ActivityType.EndCall, "", UserID, st, sst)
            End If
            'responseWrite(sst)
            'responseEnd()
        Else
            'callLogging(AppID, ActivityType.EndCall, "", UserID, st, sst)
        End If

        Dim strSQL As String = "SELECT StatusCode, SubStatusCode, CallBackDate FROM tblapplicationstatus WHERE AppID = '" & AppID & "' AND CompanyID = '" & intCompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim ds As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                strExistingStatusCode = Row.Item("StatusCode").ToString
                strExistingSubStatusCode = Row.Item("SubStatusCode").ToString
                strExistingCallBackDate = Row.Item("CallBackDate").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim strQry As String = ""
        strQry += "UPDATE tblapplicationstatus SET "
        If (checkValue(st)) Then
            strQry += _
                "StatusCode     = " & formatField(st, "U", st) & ", "
        Else
            st = strExistingStatusCode
        End If

        If (WorkflowDiary = "Y" And st <> "WTD" And st <> "TUD" And st <> "INV") Then
            If (checkValue(sst)) Then
                Dim strQry2 As String = "UPDATE tbldiaries SET DiarySubStatusCode = " & formatField(sst, "U", sst) & " WHERE AppID = '" & AppID & "' AND DiaryID = '" & DiaryID & "' AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry2)
            End If
        Else

            ' *** CBK logic for call outcomes only *** '
            Select Case sst
                Case "NOA", "ANS", "MSG", "ENG"
                    If (st <> strExistingStatusCode) Then
                        If (checkValue(strExistingCallBackDate)) Then
                            If ((strExistingSubStatusCode = "CBK" Or strExistingSubStatusCode = "3BK") And Config.DefaultDateTime >= CDate(strExistingCallBackDate)) Then
                                strQry += _
                                    "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
                            End If
                        Else
                            strQry += _
                                "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
                        End If
                    Else
                        'If (checkValue(strExistingCallBackDate)) Then
                        '    ' Already CBK
                        'Else
                        strQry += _
                            "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
                        'End If
                    End If
                Case Else
                    If (checkValue(strExistingCallBackDate)) Then
                        If ((strExistingSubStatusCode = "CBK" Or strExistingSubStatusCode = "3BK") And Config.DefaultDateTime >= CDate(strExistingCallBackDate)) Then
                            strQry += _
                                "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
                        End If
                    Else
                        strQry += _
                            "SubStatusCode  = " & formatField(sst, "U", sst) & ", "
                    End If
            End Select

        End If
		
		If (st = "INV" Or st = "TUD" Or st = "WTD") Then
            executeNonQuery("UPDATE tblapplicationstatus SET CallCentreUserID = '" & DefaultUserID & "', UpdatedDate = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " WHERE CallCentreUserID = 0 AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
		End If

        If (st = "INV") Then
            saveUpdatedDate(AppID, UserID, "OutsideCriteriaAccepted")
            strQry += _
                "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
        Else
            If (st = "WTD") Then
                Select Case sst
                    Case "DNS", "SEL", "OOW", "BUS"
                        saveUpdatedDate(AppID, UserID, "OutsideCriteriaAccepted")
                        strQry += _
                            "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
                End Select
            ElseIf (st = "TUD") Then
                Select Case sst
                    Case "OOW", "TPS", "REF", "HNG"
                        saveUpdatedDate(AppID, UserID, "OutsideCriteriaAccepted")
                        strQry += _
                            "OutsideCriteriaSubStatusCode   = " & formatField(sst, "U", sst) & ", "
                End Select
            End If
        End If

        '' *** CBK logic for call outcomes only *** '
        'Select Case sst
        '    Case "NOA", "ANS", "MSG", "ENG"
        '        If (st <> strExistingStatusCode) Then
        '            If (checkValue(strExistingCallBackDate)) Then
        '                If (strExistingSubStatusCode = "CBK" And Config.DefaultDateTime >= CDate(strExistingCallBackDate)) Then
        '                    strQry += _
        '                        "CallBackDate   = " & formatField("", "DTTM", "NULL") & ", "
        '                End If
        '            Else
        '                strQry += _
        '                    "CallBackDate   = " & formatField("", "DTTM", "NULL") & ", "
        '            End If
        '        Else
        '            If (checkValue(strExistingCallBackDate)) Then
        '                ' Already CBK
        '            Else
        '                strQry += _
        '                    "CallBackDate   = " & formatField("", "DTTM", "NULL") & ", "
        '            End If
        '        End If
        '    Case Else
        '        If (checkValue(strExistingCallBackDate)) Then
        '            If (strExistingSubStatusCode = "CBK" And Config.DefaultDateTime >= CDate(strExistingCallBackDate)) Then
        '                strQry += _
        '                      "CallBackDate   = " & formatField("", "DTTM", "NULL") & ", "
        '            End If
        '        Else
        '            strQry += _
        '                    "CallBackDate   = " & formatField("", "DTTM", "NULL") & ", "
        '        End If
        'End Select


        strQry += _
            "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE AppID        = '" & AppID & "' " & _
            "AND CompanyID      = '" & intCompanyID & "'"

        executeNonQuery(strQry)

        If (strExistingStatusCode <> st) Then
            If (st <> "") Then
                saveUpdatedDate(AppID, Config.DefaultUserID, "StatusChanged")
            End If
        End If

        insertStatusHistory(AppID, st, sst, UserID)
        If (WorkflowDiary = "Y") Then
            saveDiariseOn(AppID, DiaryID, WorkflowID)
            strSQL = "SELECT TOP(1) DiaryID FROM tbldiaries WHERE ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & intCompanyID & "') AND (DiaryAssignedToUserID = '" & Config.DefaultUserID & "') AND (AppID = '" & AppID & "')) " & _
            " OR ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & intCompanyID & "') AND (DiaryAssignedToUserID = 0) AND (AppID = '" & AppID & "')) ORDER BY DiaryAssignedToUserID DESC, DiaryDueDate"
            objDatabase = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                strSQL = "UPDATE tblusers SET UserActiveWorkflowDiaryID = '" & objResult.ToString & "' WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strSQL)
            End If
            objResult = Nothing
            objDatabase = Nothing
        ElseIf (checkValue(WorkflowID)) Then
            If (checkValue(strExistingCallBackDate)) Then
                If (st = strExistingStatusCode) Then
                    saveNextCallTime(AppID, WorkflowID, "CBK")
                Else
                    saveNextCallTime(AppID, WorkflowID)
                End If
            Else
                saveNextCallTime(AppID, WorkflowID)
            End If
        End If
        getTrigger(AppID, st, sst)
    End Sub

    Shared Sub saveBusinessStatus(ByVal BusinessObjectID As String, ByVal UserID As String, ByVal st As String, ByVal sst As String, Optional ByVal WorkflowID As String = "", Optional ByVal WorkflowDiary As String = "N", Optional ByVal DiaryID As String = "", Optional intCompanyID As String = "")
        Dim strExistingStatusCode As String = "", strExistingSubStatusCode As String = ""
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID

        Dim strSQL As String = "SELECT BusinessObjectStatusCode, BusinessObjectSubStatusCode FROM tblbusinessobjectstatus WHERE BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & intCompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim ds As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                strExistingStatusCode = Row.Item("BusinessObjectStatusCode").ToString
                strExistingSubStatusCode = Row.Item("BusinessObjectSubStatusCode").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim strQry As String = ""
        strQry += "UPDATE tblbusinessobjectstatus SET "
        If (checkValue(st)) Then
            strQry += _
                "BusinessObjectStatusCode     = " & formatField(st, "U", st) & ", "
        Else
            st = strExistingStatusCode
        End If

        If (WorkflowDiary = "Y" And st <> "WTD" And st <> "TUD" And st <> "INV") Then
            If (checkValue(sst)) Then
                Dim strQry2 As String = "UPDATE tblbusinessobjectdiaries SET DiarySubStatusCode = " & formatField(sst, "U", sst) & " WHERE BusinessObjectID = '" & BusinessObjectID & "' AND DiaryID = '" & DiaryID & "' AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry2)
            End If
        Else

            ' *** CBK logic for call outcomes only *** '
            Select Case sst
                Case "NOA", "ANS", "MSG", "ENG"
                    If (st <> strExistingStatusCode) Then
                        strQry += _
                            "BusinessObjectSubStatusCode  = " & formatField(sst, "U", sst) & ", "
                    Else
                        'If (checkValue(strExistingCallBackDate)) Then
                        '    ' Already CBK
                        'Else
                        strQry += _
                            "BusinessObjectSubStatusCode  = " & formatField(sst, "U", sst) & ", "
                        'End If
                    End If
                Case Else
                    strQry += _
                        "BusinessObjectSubStatusCode  = " & formatField(sst, "U", sst) & ", "
            End Select

        End If

        If (st = "INV" Or st = "TUD" Or st = "WTD") Then
            executeNonQuery("UPDATE tblbusinessobjectstatus SET BusinessObjectAssignedUserID = '" & DefaultUserID & "', BusinessObjectUpdatedDate = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " WHERE BusinessObjectAssignedUserID = 0 AND BusinessObjectID = '" & BusinessObjectID & "' AND CompanyID = '" & CompanyID & "'")
        End If

        strQry += _
            "BusinessObjectUpdatedDate  = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE BusinessObjectID     = '" & BusinessObjectID & "' " & _
            "AND CompanyID              = '" & intCompanyID & "'"

        executeNonQuery(strQry)

        If (strExistingStatusCode <> st) Then
            If (st <> "") Then
                saveBusinessUpdatedDate(BusinessObjectID, Config.DefaultUserID, "StatusChanged")
            End If
        End If

        insertBusinessStatusHistory(BusinessObjectID, st, sst, UserID)
        If (WorkflowDiary = "Y") Then
            saveBusinessDiariseOn(BusinessObjectID, DiaryID, WorkflowID)
            strSQL = "SELECT TOP(1) DiaryID FROM tblbusinessdiaries WHERE ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & intCompanyID & "') AND (DiaryAssignedToUserID = '" & Config.DefaultUserID & "') AND (BusinessObjectID = '" & BusinessObjectID & "')) " & _
            " OR ((DiaryActive = 1) AND (DiaryComplete = 0) AND (CompanyID = '" & intCompanyID & "') AND (DiaryAssignedToUserID = 0) AND (BusinessObjectID = '" & BusinessObjectID & "')) ORDER BY DiaryAssignedToUserID DESC, DiaryDueDate"
            objDatabase = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                strSQL = "UPDATE tblusers SET UserActiveWorkflowDiaryID = '" & objResult.ToString & "' WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strSQL)
            End If
            objResult = Nothing
            objDatabase = Nothing
        ElseIf (checkValue(WorkflowID)) Then
            saveBusinessNextCallTime(BusinessObjectID, WorkflowID)
        End If
    End Sub

    Shared Sub getTrigger(ByVal AppID As String, ByVal st As String, ByVal sst As String)
        Dim strProductType As String = getAnyField("ProductType", "tblapplications", "AppID", AppID)
        Dim strSQL As String = "SELECT TOP 1 TriggerName, TriggerEmailAddress, TriggerTelephoneNumber FROM tbltriggers WHERE (TriggerProductType = '" & strProductType & "' OR TriggerProductType = '') AND ((TriggerStatusCode = '" & st & "' AND TriggerSubStatusCode = '" & sst & "') OR TriggerStatusCode = '" & st & "' OR (TriggerStatusCode = '' AND TriggerSubStatusCode = '" & sst & "')) AND TriggerActive = 1 AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(HttpContext.Current.Cache, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("TriggerEmailAddress").ToString)) Then
                    postEmail("", Row.Item("TriggerEmailAddress").ToString, "Alert - " & Row.Item("TriggerName"), "Application " & AppID & " changed status to " & st & " " & sst, True, "", True)
                End If
                If (checkValue(Row.Item("TriggerTelephoneNumber").ToString)) Then
                    postSMS(Row.Item("TriggerTelephoneNumber"), "SMS Alert - " & Row.Item("TriggerName") & " - Application. " & AppID & " changed status to " & st & " " & sst)
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Shared Sub saveUpdatedDate(ByVal AppID As String, ByVal UserID As String, ByVal datefield As String, Optional ByVal datevalue As String = "", Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        If (Not checkValue(datevalue)) Then datevalue = Config.DefaultDateTime.ToString
        Dim strQry As String = ""
        Select Case datefield
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "Lock", "StatusChange"
                strQry = _
                    "UPDATE tblapplicationstatus SET " & _
                    datefield & "Date   = " & formatField(datevalue, "DTTM", Config.DefaultDateTime) & " " & _
                    "WHERE AppID = " & AppID & " AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
            Case Else
                If (datefield = "Sold") Then
                    If (checkDateField(AppID, datefield)) Then
                        datefield = "Resold"
                    End If
                End If
                Dim intRowsAffected As Integer = 0
                strQry = _
                    "UPDATE tblapplicationstatusdates SET  " & _
                    "ApplicationStatusDate	            = " & formatField(datevalue, "DTTM", Config.DefaultDateTime) & ", " & _
                    "ApplicationStatusDateUserID	    = " & formatField(UserID, "N", getSystemUser()) & " " & _
                    "WHERE ApplicationStatusDateAppID	= '" & AppID & "' AND ApplicationStatusDateName = '" & datefield & "' AND CompanyID = '" & intCompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
                If (intRowsAffected = 0) Then
                    strQry = "INSERT INTO tblapplicationstatusdates (CompanyID, ApplicationStatusDateAppID, ApplicationStatusDateName, ApplicationStatusDate, ApplicationStatusDateUserID) " & _
                        "VALUES(" & formatField(intCompanyID, "N", 0) & ", " & _
                        formatField(AppID, "N", 0) & ", " & _
                        formatField(datefield, "", "") & ", " & _
                        formatField(datevalue, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ")"
                    executeNonQuery(strQry)
                End If
        End Select
    End Sub

    Shared Sub clearUpdatedDate(ByVal AppID As String, ByVal datefield As String, Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        Dim strQry As String = ""
        Select Case datefield
            Case "Created", "Updated", "LastContacted", "NextCall", "CallBack", "LastRemarketed", "Lock", "StatusChange"
                strQry = _
                    "UPDATE tblapplicationstatus SET " & _
                    datefield & "Date   = " & formatField("", "DTTM", "NULL") & ", " & _
                    "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    datefield & "UserID = " & formatField(0, "N", 0) & " " & _
                    "WHERE AppID = " & AppID & " AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
            Case Else
                strQry = _
                    "UPDATE tblapplicationstatusdates SET  " & _
                    "ApplicationStatusDate	    = " & formatField("", "DTTM", "NULL") & ", " & _
                    "ApplicationStatusDateUserID	= " & formatField(0, "N", 0) & " " & _
                    "WHERE ApplicationStatusDateAppID	    = '" & AppID & "' AND ApplicationStatusDateName = '" & datefield & "' AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
        End Select
    End Sub

    Shared Sub saveBusinessUpdatedDate(ByVal BusinessObjectID As String, ByVal UserID As String, ByVal datefield As String, Optional ByVal datevalue As String = "", Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        If (Not checkValue(datevalue)) Then datevalue = Config.DefaultDateTime.ToString
        Dim strQry As String = ""
        Select Case datefield
            Case "Created", "Updated", "NextCall"
                strQry = _
                    "UPDATE tblbusinessobjectstatus SET " & _
                    "BusinessObject" & datefield & "Date   = " & formatField(datevalue, "DTTM", Config.DefaultDateTime) & " " & _
                    "WHERE BusinessObjectID = " & BusinessObjectID & " AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
            Case Else
                Dim intRowsAffected As Integer = 0
                strQry = _
                    "UPDATE tblbusinessobjectstatusdates SET  " & _
                    "BusinessObjectStatusDate	        = " & formatField(datevalue, "DTTM", Config.DefaultDateTime) & ", " & _
                    "BusinessObjectStatusDateUserID	    = " & formatField(UserID, "N", getSystemUser()) & " " & _
                    "WHERE BusinessObjectID	= '" & BusinessObjectID & "' AND BusinessObjectStatusDateName = '" & datefield & "' AND CompanyID = '" & intCompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strQry)
                If (intRowsAffected = 0) Then
                    strQry = "INSERT INTO tblbusinessobjectstatusdates (CompanyID, BusinessObjectID, BusinessObjectStatusDateName, BusinessObjectStatusDate, BusinessObjectStatusDateUserID) " & _
                        "VALUES(" & formatField(intCompanyID, "N", 0) & ", " & _
                        formatField(BusinessObjectID, "N", 0) & ", " & _
                        formatField(datefield, "", "") & ", " & _
                        formatField(datevalue, "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(UserID, "N", getSystemUser()) & ")"
                    executeNonQuery(strQry)
                End If
        End Select
    End Sub

    Shared Sub clearBusinessUpdatedDate(ByVal BusinessObjectID As String, ByVal datefield As String, Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        Dim strQry As String = ""
        Select Case datefield
            Case "Created", "Updated", "NextCall"
                strQry = _
                    "UPDATE tblbusinessobjectstatus SET " & _
                    "BusinessObject" & datefield & "Date   = " & formatField("", "DTTM", "NULL") & ", " & _
                    "BusinessObjectUpdatedDate  = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    "WHERE BusinessObjectID     = " & BusinessObjectID & " AND CompanyID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
            Case Else
                strQry = _
                    "UPDATE tblbusinessobjectstatusdates SET  " & _
                    "BusinessObjectStatusDate	    = " & formatField("", "DTTM", "NULL") & ", " & _
                    "BusinessObjectStatusDateUserID	= " & formatField(0, "N", 0) & " " & _
                    "WHERE BusinessObjectID	        = '" & BusinessObjectID & "' AND BusinessObjectStatusDateName = '" & datefield & "' AND BusinessObjectID = '" & intCompanyID & "'"
                executeNonQuery(strQry)
        End Select
    End Sub

    Shared Sub saveNote(ByVal AppID As String, ByVal UserID As String, ByVal Note As String, Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        Dim strQry As String = _
            "INSERT INTO tblnotes (CompanyID, AppID, Note, CreatedDate, CreatedUserID) " & _
            "VALUES (" & formatField(intCompanyID, "N", 0) & ", " & _
            formatField(AppID, "N", 0) & ", " & _
            formatField(Replace(Note, vbCrLf, "<br />"), "", "") & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", getSystemUser()) & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub saveBusinessNote(ByVal BusinessObjectID As String, ByVal UserID As String, ByVal Note As String, Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        Dim strQry As String = _
            "INSERT INTO tblbusinessobjectnotes (CompanyID, BusinessObjectID, Note, CreatedDate, CreatedUserID) " & _
            "VALUES (" & formatField(intCompanyID, "N", 0) & ", " & _
            formatField(BusinessObjectID, "N", 0) & ", " & _
            formatField(Replace(Note, vbCrLf, "<br />"), "", "") & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", getSystemUser()) & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub saveSocialPost(ByVal AppID As String, ByVal UserID As String, ByVal socialPost As String, Optional intCompanyID As String = "")
        If (Not checkValue(intCompanyID)) Then intCompanyID = CompanyID
        Dim strQry As String = _
            "INSERT INTO tblsocialposts (CompanyID, SocialPostText, SocialPostUserID) " & _
            "VALUES (" & formatField(intCompanyID, "N", 0) & ", " & _
            formatField(Replace(replaceTags(socialPost, AppID), vbCrLf, "<br />"), "", "") & ", " & _
            formatField(UserID, "N", getSystemUser()) & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub saveReminder(ByVal AppID As String, ByVal UserID As String, ByVal AssignedUserID As String, ByVal reminderdate As String, ByVal reminder As String)
        Dim strQry As String = "INSERT INTO tblreminders (CompanyID, AppID, ReminderAssignedByUserID, ReminderAssignedToUserID, ReminderDescription, ReminderDateTime) " & _
              "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
              formatField(AppID, "N", 0) & ", " & _
              formatField(UserID, "N", getSystemUser()) & ", " & _
              formatField(AssignedUserID, "N", 0) & ", " & _
              formatField(reminder, "", "") & ", " & _
              formatField(reminderdate, "DTTM", "NULL") & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub saveMessage(ByVal mType As String, ByVal FromUserID As String, ByVal ToUserID As String, ByVal subject As String, ByVal message As String, ByVal CalendarID As String)
        Dim arrUsers As Array = Split(ToUserID, ",")
        For x As Integer = 0 To UBound(arrUsers)
            Dim strQry As String = "INSERT INTO tblmessages (CompanyID, MessageType, MessageFromUserID, MessageToUserID, MessageSubject, MessageText, MessageApproveCalendarEventID) " & _
                  "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                  formatField(mType, "N", 1) & ", " & _
                  formatField(FromUserID, "N", 0) & ", " & _
                  formatField(arrUsers(x), "N", 0) & ", " & _
                  formatField(subject, "T", "") & ", " & _
                  formatField(message, "", "") & ", " & _
                  formatField(CalendarID, "N", 0) & ") "
            executeNonQuery(strQry)
        Next
    End Sub

    Shared Sub saveCallBack(ByVal AppID As String, ByVal UserID As String, ByVal nextcalldate As String, Optional ByVal WorkflowDiary As String = "N", Optional ByVal DiaryID As String = "")
        Dim strCallBackSubStatusCode As String = "CBK"
        If (HttpContext.Current.Request("cbThirdParty") = "on") Then
            strCallBackSubStatusCode = "3BK"
        End If
        If (WorkflowDiary = "Y") Then
            Dim strQry As String = "UPDATE tbldiaries SET " & _
                "DiaryDueDate       = " & formatField(nextcalldate, "DTTM", Config.DefaultDateTime) & ", " & _
                "DiarySubStatusCode = " & formatField(strCallBackSubStatusCode, "U", "CBK") & " " & _
                "WHERE AppID        = '" & AppID & "' " & _
                "AND DiaryID        = '" & DiaryID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strQry)
        Else
            Dim strQry As String = "UPDATE tblapplicationstatus SET " & _
                "NextCallDate       = " & formatField(nextcalldate, "DTTM", Config.DefaultDateTime) & ", " & _
                "SubStatusCode      = " & formatField(strCallBackSubStatusCode, "U", "CBK") & ", " & _
                "UpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                "WHERE AppID        = '" & AppID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If

        insertStatusHistory(AppID, "", strCallBackSubStatusCode, UserID)
        saveNote(AppID, Config.DefaultUserID, "Call back arranged: " & nextcalldate)
        saveReminder(AppID, UserID, UserID, CDate(nextcalldate).AddMinutes(-5), "Call back due:" & HttpContext.Current.Request("frmNote"))
        callLogging(AppID, ActivityType.EndCall, "", UserID, getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID), strCallBackSubStatusCode)
    End Sub

    Shared Sub saveBusinessCallBack(ByVal BusinessObjectID As String, ByVal UserID As String, ByVal nextcalldate As String, Optional ByVal WorkflowDiary As String = "N", Optional ByVal DiaryID As String = "")
        Dim strCallBackSubStatusCode As String = "CBK"
        If (HttpContext.Current.Request("cbThirdParty") = "on") Then
            strCallBackSubStatusCode = "3BK"
        End If
        If (WorkflowDiary = "Y") Then
            Dim strQry As String = "UPDATE tblbusinessobjectdiaries SET " & _
                "DiaryDueDate       = " & formatField(nextcalldate, "DTTM", Config.DefaultDateTime) & ", " & _
                "DiarySubStatusCode = " & formatField(strCallBackSubStatusCode, "U", "CBK") & " " & _
                "WHERE BusinessObjectID        = '" & BusinessObjectID & "' " & _
                "AND DiaryID        = '" & DiaryID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strQry)
        Else
            Dim strQry As String = "UPDATE tblbusinessobjectstatus SET " & _
                "BusinessObjectNextCallDate       = " & formatField(nextcalldate, "DTTM", Config.DefaultDateTime) & ", " & _
                "BusinessObjectSubStatusCode      = " & formatField(strCallBackSubStatusCode, "U", "CBK") & ", " & _
                "BusinessObjectUpdatedDate        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                "WHERE BusinessObjectID        = '" & BusinessObjectID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
            executeNonQuery(strQry)
        End If

        insertStatusHistory(BusinessObjectID, "", strCallBackSubStatusCode, UserID)
        saveBusinessNote(BusinessObjectID, Config.DefaultUserID, "Call back arranged: " & nextcalldate)
        'saveReminder(BusinessObjectID, UserID, UserID, CDate(nextcalldate).AddMinutes(-5), "Call back due:" & HttpContext.Current.Request("frmNote"))
        callLogging(BusinessObjectID, ActivityType.EndCall, "", UserID, getAnyField("BusinessObjectStatusCode", "tblbusinessobjectstatus", "BusinessObjectID", BusinessObjectID), strCallBackSubStatusCode)
    End Sub

    Shared Sub saveCallBackCancel(AppID As String)
        Dim strQry As String = "UPDATE tblapplicationstatus SET " & _
                "SubStatusCode      = " & formatField("", "U", "") & " " & _
                "WHERE AppID        = '" & AppID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
        executeNonQuery(strQry)
        saveNote(AppID, Config.DefaultUserID, "Call back cancelled")
    End Sub

    Shared Sub saveBusinessCallBackCancel(BusinessObjectID As String)
        Dim strQry As String = "UPDATE tblbusinessobjectstatus SET " & _
                "BusinessObjectSubStatusCode      = " & formatField("", "U", "") & " " & _
                "WHERE BusinessObjectID        = '" & BusinessObjectID & "' " & _
                "AND CompanyID      = '" & CompanyID & "'"
        executeNonQuery(strQry)
        saveBusinessNote(BusinessObjectID, Config.DefaultUserID, "Call back cancelled")
    End Sub

    Shared Sub setTransferState(ByVal AppID As String, ByVal MediaCampaignID As String, ByVal MediaUserRef As String, ByVal SalesUserID As String, ByVal HotkeyUserID As String, ByVal MediaCampaignScheduleID As String)
        Dim intMediaCampaignCostOutbound As String = "", strTransferStatusCode As String = "", strTransferSubStatusCode As String = "", strMediaCampaignUpdateDateType As String = "", strMediaCampaignUpdateProductType As String = "", strMediaCampaignUpdateUserType As String = "", strMediaCampaignAutoOutsideCriteriaSubStatus As String = ""
        Dim strSQL As String = "SELECT MediaCampaignCostOutbound, MediaCampaignStatusCode, MediaCampaignSubStatusCode, MediaCampaignUpdateDateType, MediaCampaignUpdateProductType, MediaCampaignUpdateUserType, MediaCampaignAutoOutsideCriteriaSubStatus FROM tblmediacampaigns WHERE MediaCampaignID = '" & MediaCampaignID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblmediacampaigns")
        Dim ds As DataTable = objDataSet.Tables("tblmediacampaigns")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                intMediaCampaignCostOutbound = Row.Item("MediaCampaignCostOutbound")
                strTransferStatusCode = Row.Item("MediaCampaignStatusCode").ToString
                strTransferSubStatusCode = Row.Item("MediaCampaignSubStatusCode").ToString
                strMediaCampaignUpdateDateType = Row.Item("MediaCampaignUpdateDateType").ToString
                strMediaCampaignUpdateProductType = Row.Item("MediaCampaignUpdateProductType").ToString
                strMediaCampaignUpdateUserType = Row.Item("MediaCampaignUpdateUserType").ToString
                strMediaCampaignAutoOutsideCriteriaSubStatus = Row.Item("MediaCampaignAutoOutsideCriteriaSubStatus").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim strQry As String = "UPDATE tblapplications SET  "
        If checkValue(strMediaCampaignUpdateProductType) Then
            strQry += "ProductType = " & formatField(strMediaCampaignUpdateProductType, "", "NULL") & ", "
        End If
        strQry += "MediaCampaignIDOutbound	= " & formatField(MediaCampaignID, "N", 0) & ", " & _
                "MediaUserRef               = " & formatField(MediaUserRef, "", "NULL") & ", " & _
                "MediaCampaignCostOutbound  = " & formatField(intMediaCampaignCostOutbound, "N", 0) & " " & _
                "WHERE AppID	            = '" & AppID & "' " & _
                "AND CompanyID              = '" & CompanyID & "'"

        executeNonQuery(strQry)

        strQry = "UPDATE tblapplicationstatus SET " & _
                "StatusCode		            = " & formatField(strTransferStatusCode, "U", "TFR") & ", " & _
                "SubStatusCode		        = " & formatField(strTransferSubStatusCode, "U", "NULL") & ", " & _
                "SalesUserID		        = " & formatField(SalesUserID, "N", "SalesUserID") & ", "
        If (checkValue(strMediaCampaignUpdateDateType)) Then
            saveUpdatedDate(AppID, HotkeyUserID, strMediaCampaignUpdateDateType)
        End If
        If (checkValue(strMediaCampaignAutoOutsideCriteriaSubStatus)) Then
            strQry += "OutsideCriteriaSubStatusCode = " & formatField(strMediaCampaignAutoOutsideCriteriaSubStatus, "U", "NULL") & ", "
            saveUpdatedDate(AppID, HotkeyUserID, "OutsideCriteriaAccepted")
        End If
        strQry += _
            "CallBackDate                   = " & formatField("", "DTTM", "NULL") & ", " & _
            "LastContactedDate              = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            "UpdatedDate                    = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
            "WHERE AppID	                = '" & AppID & "' " & _
            "AND CompanyID                  = '" & CompanyID & "'"

        executeNonQuery(strQry)

        strQry = "UPDATE tblapplicationstatus SET  " & _
            "CallCentreUserID		        = " & formatField(HotkeyUserID, "N", 0) & " " & _
            "WHERE AppID	                = '" & AppID & "' " & _
            "AND CallCentreUserID           = 0 " & _
            "AND CompanyID                  = '" & CompanyID & "'"
        executeNonQuery(strQry)

        incrementField("MediaCampaignScheduleID", MediaCampaignScheduleID, "MediaCampaignScheduleDelivered", "tblmediacampaignschedules", 1)
        insertStatusHistory(AppID, strTransferStatusCode, strTransferSubStatusCode, HotkeyUserID)
        If (SalesUserID <> HotkeyUserID) Then
            callLogging(AppID, ActivityType.EndCall, "", HotkeyUserID, strTransferStatusCode, "")
        End If

        'If (Not checkValue(getAnyField("TransferredToDialerDate", "tblapplicationstatus", "AppID", AppID))) Then
        '    updateSingleDatabaseField(AppID, "tblapplicationstatus", "TransferredToDialerDate", "DTTM", Config.DefaultDateTime, Config.DefaultDateTime)
        'End If
    End Sub

    Shared Sub insertStatusHistory(ByVal AppID As String, ByVal StatusCode As String, ByVal SubStatusCode As String, ByVal UserID As String)
        If (Not checkValue(StatusCode)) Then
            StatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
        End If
        Dim strQry As String = "INSERT INTO tblstatushistory(CompanyID, AppID, StatusHistoryStatusCode, StatusHistorySubStatusCode, StatusHistoryUserID) " & _
            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
            formatField(AppID, "N", 0) & ", " & _
            formatField(StatusCode, "U", "") & ", " & _
            formatField(SubStatusCode, "U", "") & ", " & _
            formatField(UserID, "N", getSystemUser()) & ")"
        executeNonQuery(strQry)
    End Sub

    Shared Sub insertBusinessStatusHistory(ByVal BusinessObjectID As String, ByVal StatusCode As String, ByVal SubStatusCode As String, ByVal UserID As String)
        If (Not checkValue(StatusCode)) Then
            StatusCode = getAnyField("BusinessObjectStatusCode", "tblbusinessobjectstatus", "BusinessObjectID", BusinessObjectID)
        End If
        Dim strQry As String = "INSERT INTO tblbusinessobjectstatushistory(CompanyID, BusinessObjectID, StatusHistoryStatusCode, StatusHistorySubStatusCode, StatusHistoryUserID) " & _
            "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
            formatField(BusinessObjectID, "N", 0) & ", " & _
            formatField(StatusCode, "U", "") & ", " & _
            formatField(SubStatusCode, "U", "") & ", " & _
            formatField(UserID, "N", getSystemUser()) & ")"
        executeNonQuery(strQry)
    End Sub

    Shared Sub lockCase(ByVal UserID As String, ByVal AppID As String)
        Dim intLockCount As Integer = 0
        Dim strQry As String = "SELECT COUNT(LockID) AS LockCount FROM tbllocks WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strQry)
        If (objResult IsNot Nothing) Then
            intLockCount = objResult.ToString
        End If
        objResult = Nothing
        objDatabase = Nothing
        If (intLockCount = 0) Then
            unlockCases(UserID)
            strQry = "INSERT INTO tbllocks(CompanyID, AppID, LockUserID) VALUES('" & CompanyID & "'," & AppID & ",'" & UserID & "')"
            executeNonQuery(strQry)
        End If
    End Sub

    Shared Sub unlockCases(ByVal UserID As String, Optional ByVal AppID As String = "")
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            strQry = "DELETE FROM tbllocks WHERE AppID <> '" & AppID & "' AND LockUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
        Else
            strQry = "DELETE FROM tbllocks WHERE LockUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
        End If
        executeNonQuery(strQry)
    End Sub

    Shared Sub blackList(ByVal ip As String)
        ' Check white list before black listing
        Dim boolWhiteListed As Boolean
        Dim strSQL As String = "SELECT CompanyID FROM tblsystemconfiguration WHERE (SystemConfigurationValue LIKE N'%" & ip & ";%') AND (SystemConfigurationName = N'CompanyIPAddresses')"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolWhiteListed = True
        Else
            boolWhiteListed = False
        End If
        objResult = Nothing
        If (boolWhiteListed = False) Then
            strSQL = "SELECT WebServiceID FROM tblwebservices WHERE WebServiceIPAddress LIKE N'%" & ip & ";%' AND WebServiceActive = 1"
            objResult = objDataBase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                boolWhiteListed = True
            Else
                boolWhiteListed = False
            End If
        End If
        objDataBase = Nothing
        If (boolWhiteListed = False) Then
            strSQL = "INSERT INTO tblblacklist(BlackListAddress) " & _
                "VALUES(" & formatField(ip, "", "") & ")"
            executeNonQuery(strSQL)
            postEmail("", "itsupport@engaged-solutions.co.uk", "Blacklist Entry", "Blacklisted IP address: " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
            responseEnd()
        End If
    End Sub

    Shared Function sendEmails(ByVal AppID As String, ByVal emails As String, company As String) As String
        Dim strReturnUrlVariables As String = ""
        Dim arrEmails As Array = Split(emails, ",")
        Dim strEmailSubject As String = "", strEmailBody As String = "", strEmailAttachments As String = "", strLetterColour As String = ""
        Dim strSQL As String = "", dsCache As DataTable = Nothing

        strSQL = "SELECT App1EmailAddress, App2EmailAddress FROM tblapplications WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strEmailAddress As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                If (checkValue(Row.Item("App1EmailAddress").ToString)) Then
                    strEmailAddress = Row.Item("App1EmailAddress")
                ElseIf (checkValue(Row.Item("App2EmailAddress").ToString)) Then
                    strEmailAddress = Row.Item("App2EmailAddress")
                End If
            Next
        End If
        dsCache = Nothing

        If (checkValue(strEmailAddress)) Then
            For x As Integer = 0 To UBound(arrEmails)
                strSQL = "SELECT LetterID, LetterTitle, LetterBody, LetterEmailAttachments, LetterColour, EmailTemplateBody FROM tbllettertemplates " & _
                "LEFT OUTER JOIN tblemailtemplates ON tbllettertemplates.LetterEmailTemplateID = tblemailtemplates.EmailTemplateID WHERE LetterID = '" & arrEmails(x) & "' AND tbllettertemplates.CompanyID = '" & CompanyID & "' "
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                Dim strEmailTemplate As String = "", strLetterBody As String = "", strLetterEmailAttachments As String = "", strCurrentParagraph As String = ""
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        If checkValue(Row.Item("LetterTitle").ToString) Then
                            strEmailSubject = replaceTags(Row.Item("LetterTitle").ToString, AppID)
                        Else
                            strEmailSubject = "Your Application With " & company & " (REF: " & AppID & ")"
                        End If
                        strEmailTemplate = Row.Item("EmailTemplateBody").ToString
                        strLetterBody = Row.Item("LetterBody").ToString
                        strLetterEmailAttachments = Row.Item("LetterEmailAttachments").ToString
                        strLetterColour = Row.Item("LetterColour").ToString
                    Next
                End If
                dsCache = Nothing
                Dim arrParagraphs As Array = Split(strLetterBody, ",")
                strEmailBody = ""
                For y As Integer = 0 To UBound(arrParagraphs)
                    strCurrentParagraph = replaceTags(letterTags(getParagraphText(arrParagraphs(y)), ""), AppID)
                    strEmailBody += strCurrentParagraph
                Next
                If checkValue(strEmailTemplate) Then
                    strEmailBody = Replace(strEmailTemplate, "xxx[Content]xxx", strEmailBody)
                End If
                If (checkValue(strLetterEmailAttachments)) Then
                    Dim arrAttachments As Array = Split(strLetterEmailAttachments, ",")
                    strEmailAttachments = ""
                    For z As Integer = 0 To UBound(arrAttachments)
                        Dim strURL As String = ""
                        If (Left(arrAttachments(z), 1) = "P") Then
                            strURL = "/letters/generatepdf.aspx?AppID=" & AppID & "&PDFID=" & Replace(arrAttachments(z), "P", "") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID")
                        ElseIf (Left(arrAttachments(z), 1) = "L") Then
                            strURL = "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & Replace(arrAttachments(z), "L", "") & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID")
                        End If
                        Dim objResponse As HttpWebResponse = getWebRequest(Config.ApplicationURL & strURL)
                        Dim objReader As New StreamReader(objResponse.GetResponseStream())
                        Dim strResponseText As String = objReader.ReadToEnd()
                        If (z = 0) Then
                            strEmailAttachments += strResponseText
                        Else
                            strEmailAttachments += "," & strResponseText
                        End If
                        objReader.Close()
                        objReader = Nothing
                    Next
                End If
                strEmailBody = Replace(strEmailBody, "<br><br><br><br>", "<br>")
                strEmailBody = Replace(strEmailBody, "<br><br>", "<br>")
                strEmailBody = Replace(strEmailBody, "</p><br>", "</p>")
                strEmailBody = Replace(strEmailBody, "<br><p>", "<p>")

                postEmailWithMediaCampaignID("", strEmailAddress, strEmailSubject, strEmailBody, True, strEmailAttachments)
                getWebRequest(Config.ApplicationURL & "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & arrEmails(x) & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID)
                If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL("Email successfully sent to " & strEmailAddress)
            Next
        Else
            If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL(" Email unsuccessfully sent, no Email Address present")
        End If
        Return strReturnUrlVariables
    End Function

    Shared Function sendSMS(ByVal AppID As String, ByVal sms As String) As String
        Dim strReturnUrlVariables As String = ""
        Dim arrSMS As Array = Split(sms, ",")
        Dim strSMSBody As String = ""
        Dim strSQL As String = "", dsCache As DataTable = Nothing

        strSQL = "SELECT App1MobileTelephone FROM tblapplications WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim strTelephoneNumber As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strTelephoneNumber = Row.Item("App1MobileTelephone")
            Next
        End If
        dsCache = Nothing

        If (checkValue(strTelephoneNumber)) Then
            For x As Integer = 0 To UBound(arrSMS)
                strSQL = "SELECT LetterID, LetterTitle, LetterBody, LetterEmailAttachments, LetterColour, LetterEmailProfileID, EmailTemplateBody FROM tbllettertemplates " & _
                "LEFT OUTER JOIN tblemailtemplates ON tbllettertemplates.LetterEmailTemplateID = tblemailtemplates.EmailTemplateID WHERE LetterID = '" & arrSMS(x) & "' AND tbllettertemplates.CompanyID = '" & CompanyID & "' "
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                Dim strCurrentParagraph As String = "", intProfileID As String = ""
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        strSMSBody = Row.Item("LetterBody").ToString
                        intProfileID = Row.Item("LetterEmailProfileID").ToString
                    Next
                End If
                dsCache = Nothing
                Dim arrParagraphs As Array = Split(strSMSBody, ",")
                strSMSBody = ""
                For y As Integer = 0 To UBound(arrParagraphs)
                    strCurrentParagraph = replaceTags(letterTags(getParagraphText(arrParagraphs(y)), ""), AppID)
                    strSMSBody += strCurrentParagraph
                Next
                strSMSBody = regexReplace("<(.|\n)*?>", "", strSMSBody)

                Dim objResponse As Net.HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/sms.aspx?AppID=" & AppID & "&frmTelephoneTo=" & strTelephoneNumber & "&frmSMSBody=" & strSMSBody & "&UserSessionID=" & Config.UserSessionID & "&frmEmailProfileID=" & intProfileID & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
                If (checkResponse(objResponse, "AppID=" & AppID)) Then
                    HttpContext.Current.Response.Clear()
                    Dim objReader As New StreamReader(objResponse.GetResponseStream())
                    Dim strResponse As String = objReader.ReadToEnd()
                    Dim arrResponse As Array = Split(strResponse, "|")
                    If (arrResponse(0) = "0") Then
                        If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL("SMS successfully sent to " & strTelephoneNumber & "</strong>")
                        getWebRequest(Config.ApplicationURL & "/letters/generateletter.aspx?AppID=" & AppID & "&LetterID=" & arrSMS(x) & "&frmAttach=Y&UserSessionID=" & Config.UserSessionID)
                        'saveNote(AppID, Config.DefaultUserID, "SMS sent to " & strTelephoneNumber & ": " & strSMSBody)
                    Else
                        If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL("SMS unsuccessfully sent (Reason: " & Replace(arrResponse(1), "_", "") & ")</strong><br />")
                    End If
                    objReader.Close()
                    objReader = Nothing
                Else
                    If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL("SMS unsuccessfully sent, please contact Engaged Solutions</strong><br />")
                End If
                If (checkResponse(objResponse, "AppID=" & AppID)) Then
                    objResponse.Close()
                End If
                objResponse = Nothing

            Next
        Else
            If (Not checkValue(strReturnUrlVariables)) Then strReturnUrlVariables = encodeURL("SMS unsuccessfully sent, no Mobile Number present</strong><br />")
        End If
        Return strReturnUrlVariables
    End Function

    Shared Function letterTags(ByVal str As String, colour As String) As String
        Dim objMatches As MatchCollection = Regex.Matches(str, "\{[A-Za-z0-9]{0,100}\}")
        Dim strField As String = "", intOldLength As Integer = 0
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{", "")
            strField = Replace(strField, "}", "")
            Select Case strField
                Case "Colour"
                    str = Replace(str, objMatches.Item(x).Value, colour)
            End Select
        Next
        Return str
    End Function

    Shared Function getParagraphText(ByVal ParagraphID As String) As String
        Return getAnyField("ParagraphText", "tblletterparagraphs", "ParagraphID", ParagraphID)
    End Function

    Shared Sub createDiaries(ByVal AppID As String, ByVal diaries As String)
        Dim arrDiaries As Array = Split(diaries, ",")
        Dim strSQL As String = "", dsCache As DataTable = Nothing
        For x As Integer = 0 To UBound(arrDiaries)
            Dim strDiaryTypeName As String = "", strDiaryTypeDescription As String = "", strDiaryUpdateUserType As String = "", intDiaryAssignedToUserID As String = "", intDiaryRecycleTime As String = "", boolDiaryReminder As Boolean = False
            strSQL = "SELECT DiaryTypeName, DiaryTypeDescription, DiaryUpdateUserType, DiaryRecycleTime, DiaryReminder FROM tbldiarytypes WHERE DiaryTypeID = '" & arrDiaries(x) & "' AND CompanyID = '" & CompanyID & "'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strDiaryTypeName = Row.Item("DiaryTypeName")
                    strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                    strDiaryUpdateUserType = Row.Item("DiaryUpdateUserType").ToString
                    intDiaryRecycleTime = Row.Item("DiaryRecycleTime")
                    If (checkValue(strDiaryUpdateUserType)) Then
                        intDiaryAssignedToUserID = getAnyField(strDiaryUpdateUserType & "UserID", "tblapplicationstatus", "AppID", AppID)
                    End If
                    boolDiaryReminder = Row.Item("DiaryReminder")
                Next
            End If
            dsCache = Nothing
            Dim intRowsAffected As Integer = 0
            If (Len(AppID < 7)) Then ' Business cases
                strSQL = "UPDATE tblbusinessdiaries SET DiaryDueDate = " & formatField(Config.DefaultDateTime.AddDays(intDiaryRecycleTime), "DTTM", Config.DefaultDateTime) & ", DiaryAssignedToUserID = " & formatField(intDiaryAssignedToUserID, "N", 0) & " WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND BusinessObjectID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strSQL)
                If (intRowsAffected = 0) Then
                    strSQL = "INSERT INTO tblbusinessdiaries(BusinessObjectID, CompanyID, DiaryAssignedToUserID, DiaryDueDate, DiaryType, DiaryTypeDescription, DiaryShowInCalendar) " & _
                        "VALUES(" & AppID & ", " & _
                        CompanyID & ", " & _
                        formatField(intDiaryAssignedToUserID, "N", 0) & ", " & _
                        formatField(Config.DefaultDateTime.AddDays(intDiaryRecycleTime), "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(strDiaryTypeName, "", "") & ", " & _
                        formatField(strDiaryTypeDescription, "", "") & ", " & _
                        formatField(0, "N", 0) & ")"
                    Call executeNonQuery(strSQL)
                End If
                If (boolDiaryReminder) Then
                    saveReminder(AppID, Config.DefaultUserID, intDiaryAssignedToUserID, Config.DefaultDateTime.AddDays(intDiaryRecycleTime).AddHours(-1), strDiaryTypeDescription)
                End If
            Else
                strSQL = "UPDATE tbldiaries SET DiaryDueDate = " & formatField(Config.DefaultDateTime.AddDays(intDiaryRecycleTime), "DTTM", Config.DefaultDateTime) & ", DiaryAssignedToUserID = " & formatField(intDiaryAssignedToUserID, "N", 0) & " WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
                intRowsAffected = executeRowsAffectedQuery(strSQL)
                If (intRowsAffected = 0) Then
                    strSQL = "INSERT INTO tbldiaries(AppID, CompanyID, DiaryAssignedToUserID, DiaryDueDate, DiaryType, DiaryTypeDescription, DiaryShowInCalendar) " & _
                        "VALUES(" & AppID & ", " & _
                        CompanyID & ", " & _
                        formatField(intDiaryAssignedToUserID, "N", 0) & ", " & _
                        formatField(Config.DefaultDateTime.AddDays(intDiaryRecycleTime), "DTTM", Config.DefaultDateTime) & ", " & _
                        formatField(strDiaryTypeName, "", "") & ", " & _
                        formatField(strDiaryTypeDescription, "", "") & ", " & _
                        formatField(0, "N", 0) & ")"
                    Call executeNonQuery(strSQL)
                End If
                If (boolDiaryReminder) Then
                    saveReminder(AppID, Config.DefaultUserID, intDiaryAssignedToUserID, Config.DefaultDateTime.AddDays(intDiaryRecycleTime).AddHours(-1), strDiaryTypeDescription)
                End If
            End If
        Next
    End Sub

    Shared Sub completeDiaries(ByVal AppID As String, ByVal diaries As String)
        Dim arrDiaries As Array = Split(diaries, ",")
        Dim strSQL As String = "", dsCache As DataTable = Nothing
        For x As Integer = 0 To UBound(arrDiaries)
            Dim strDiaryTypeName As String = "", strDiaryTypeDescription As String = ""
            strSQL = "SELECT DiaryTypeName, DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeID = '" & arrDiaries(x) & "' AND CompanyID = '" & CompanyID & "'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strDiaryTypeName = Row.Item("DiaryTypeName")
                    strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                Next
            End If
            dsCache = Nothing
            If (Len(AppID < 7)) Then ' Business cases
                strSQL = "UPDATE tblbusinessdiaries SET DiaryComplete = 1 WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND BusinessObjectID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            Else
                strSQL = "UPDATE tbldiaries SET DiaryComplete = 1 WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            End If
            executeNonQuery(strSQL)
        Next
    End Sub

    Shared Sub cancelDiaries(ByVal AppID As String, ByVal diaries As String)
        Dim arrDiaries As Array = Split(diaries, ",")
        Dim strSQL As String = "", dsCache As DataTable = Nothing
        For x As Integer = 0 To UBound(arrDiaries)
            Dim strDiaryTypeName As String = "", strDiaryTypeDescription As String = ""
            strSQL = "SELECT DiaryTypeName, DiaryTypeDescription FROM tbldiarytypes WHERE DiaryTypeID = '" & arrDiaries(x) & "' AND CompanyID = '" & CompanyID & "'"
            dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strDiaryTypeName = Row.Item("DiaryTypeName")
                    strDiaryTypeDescription = Row.Item("DiaryTypeDescription")
                Next
            End If
            dsCache = Nothing
            If (Len(AppID < 7)) Then ' Business cases
                strSQL = "UPDATE tblbusinessdiaries SET DiaryActive = 0 WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND BusinessObjectID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            Else
                strSQL = "UPDATE tbldiaries SET DiaryActive = 0 WHERE DiaryType = '" & strDiaryTypeName & "' AND DiaryComplete = 0 AND DiaryActive = 1 AND AppID = " & AppID & " AND CompanyID = '" & CompanyID & "'"
            End If
            executeNonQuery(strSQL)
        Next
    End Sub

    Shared Sub createReminders(ByVal AppID As String, ByVal reminders As String, users As String)
        Dim arrReminders As Array = Split(reminders, ",")
        Dim arrUsers As Array = Split(users, ",")
        Dim intUserID As String = ""
        For x As Integer = 0 To UBound(arrReminders)
            If (arrUsers(x) = "0") Then
                intUserID = Config.DefaultUserID
            Else
                intUserID = arrUsers(x)
            End If
            saveReminder(AppID, Config.DefaultUserID, intUserID, Config.DefaultDateTime, arrReminders(x))
        Next
    End Sub

    Shared Sub saveNextCallTime(ByVal AppID As String, ByVal WorkflowID As String, Optional overrideSubStatus As String = "")
        Dim dteCreatedDate As String = "", dteStatusDate As String = "", dteLastDialedDate As String = "", intRecycleTime As Integer = 0, intTolerance As Integer = 0, dteCallBackDateTime As String = "", dteNextCallDate As String = "", dteOriginalNextCallDate As String = "", dteAdjustedNextCallDate As String = "", boolUseCallBackTime As Boolean
        Dim strSQL As String = "SELECT TOP 1 CreatedDate, StatusDate, LastDialedDate, CallBackDate, NextCallDate, RecycleTime, WorkflowToleranceFactor FROM vwrecyclerules " & _
                  "WHERE AppID = '" & AppID & "' AND (WorkflowID = '" & WorkflowID & "' OR WorkflowID = 0) AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') " & _
                  "ORDER BY WorkflowID DESC, CompanyID DESC, RecycleRuleMediaCampaignGradeID DESC, RecycleRuleStatusCode DESC, RecycleRuleSubStatusCode DESC"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplications")
        Dim dsNextCallTime = objDataSet.Tables("tblapplications")
        If (dsNextCallTime.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsNextCallTime.Rows
                dteCreatedDate = Row.Item("CreatedDate")
                If (checkValue(Row.Item("StatusDate").ToString)) Then
                    dteStatusDate = Row.Item("StatusDate").ToString
                End If
                If (checkValue(Row.Item("LastDialedDate").ToString)) Then
                    dteLastDialedDate = Row.Item("LastDialedDate").ToString
                End If
                intRecycleTime = Row.Item("RecycleTime")
                intTolerance = Row.Item("WorkflowToleranceFactor")
                If (checkValue(Row.Item("CallBackDate").ToString)) Then
                    dteCallBackDateTime = Row.Item("CallBackDate").ToString
                End If
                If (checkValue(Row.Item("NextCallDate").ToString)) Then
                    dteOriginalNextCallDate = Row.Item("NextCallDate").ToString
                End If
                dteNextCallDate = Config.DefaultDateTime
                boolUseCallBackTime = False
                If (Not checkValue(dteStatusDate)) Then dteStatusDate = dteCreatedDate
                If (Not checkValue(dteLastDialedDate)) Then dteLastDialedDate = dteCreatedDate
                If (checkValue(dteCallBackDateTime)) Then
                    dteNextCallDate = dteCallBackDateTime
                    ' *** Remember to add the recycle time back on if present e.g. CBK > NOA
                    If (intRecycleTime > 0) Then
                        dteNextCallDate = DateAdd("s", intRecycleTime, Config.DefaultDateTime)
                    End If
                    boolUseCallBackTime = True
                End If
                If (Not boolUseCallBackTime) Then
                    If (Not checkValue(intRecycleTime)) Then intRecycleTime = 0
                    If (intRecycleTime = 0) Then
                        dteNextCallDate = dteOriginalNextCallDate
                    Else
                        If (DateDiff("s", CDate(dteStatusDate), CDate(dteLastDialedDate)) > 0) Then
                            dteNextCallDate = DateAdd("s", intRecycleTime, CDate(dteLastDialedDate))
                        ElseIf (DateDiff("s", dteCreatedDate, CDate(dteStatusDate)) > 0) Then
                            dteNextCallDate = DateAdd("s", intRecycleTime, CDate(dteStatusDate))
                        Else
                            dteNextCallDate = DateAdd("s", intRecycleTime, CDate(dteCreatedDate))
                        End If
                        If (intTolerance > 0) Then
                            dteNextCallDate = DateAdd("s", -intTolerance, dteNextCallDate)
                        End If
                        dteAdjustedNextCallDate = dteNextCallDate
                        dteAdjustedNextCallDate = calcWorkingHour(dteAdjustedNextCallDate)
                        Do While (dteAdjustedNextCallDate <> dteNextCallDate)
                            dteNextCallDate = dteAdjustedNextCallDate
                            dteAdjustedNextCallDate = calcWorkingHour(dteNextCallDate)
                        Loop
                        dteNextCallDate = dteAdjustedNextCallDate
                    End If
                End If
            Next
        Else
            dteNextCallDate = Config.DefaultDateTime
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsNextCallTime = Nothing
        objDatabase = Nothing
        updateSingleDatabaseField(AppID, "tblapplicationstatus", "NextCallDate", "DTTM", dteNextCallDate, Config.DefaultDateTime)
        updateSingleDatabaseField(AppID, "tblapplicationstatus", "RecycleTime", "N", intRecycleTime, 0)
        If (checkValue(overrideSubStatus)) Then
            updateSingleDatabaseField(AppID, "tblapplicationstatus", "SubStatusCode", "U", overrideSubStatus, "NULL")
        End If
    End Sub

    Shared Sub saveBusinessNextCallTime(ByVal BusinessObjectID As String, ByVal WorkflowID As String, Optional overrideSubStatus As String = "")
        Dim dteCreatedDate As String = "", dteStatusDate As String = "", intRecycleTime As Integer = 0, intTolerance As Integer = 0, dteNextCallDate As String = "", dteOriginalNextCallDate As String = "", dteAdjustedNextCallDate As String = "", boolUseCallBackTime As Boolean
        Dim strSQL As String = "SELECT TOP 1 BusinessObjectCreatedDate, StatusDate, BusinessObjectNextCallDate, RecycleTime, WorkflowToleranceFactor FROM vwbusinessrecyclerules " & _
                  "WHERE BusinessObjectID = '" & BusinessObjectID & "' AND (WorkflowID = '" & WorkflowID & "' OR WorkflowID = 0) AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') " & _
                  "ORDER BY WorkflowID DESC, CompanyID DESC, RecycleRuleMediaCampaignGradeID DESC, RecycleRuleStatusCode DESC, RecycleRuleSubStatusCode DESC"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplications")
        Dim dsNextCallTime = objDataSet.Tables("tblapplications")
        If (dsNextCallTime.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsNextCallTime.Rows
                dteCreatedDate = Row.Item("BusinessObjectCreatedDate")
                If (checkValue(Row.Item("StatusDate").ToString)) Then
                    dteStatusDate = Row.Item("StatusDate").ToString
                End If
                intRecycleTime = Row.Item("RecycleTime")
                intTolerance = Row.Item("WorkflowToleranceFactor")
                If (checkValue(Row.Item("BusinessObjectNextCallDate").ToString)) Then
                    dteOriginalNextCallDate = Row.Item("BusinessObjectNextCallDate").ToString
                End If
                dteNextCallDate = Config.DefaultDateTime
                boolUseCallBackTime = False
                If (Not checkValue(dteStatusDate)) Then dteStatusDate = dteCreatedDate
                If (Not boolUseCallBackTime) Then
                    If (Not checkValue(intRecycleTime)) Then intRecycleTime = 0
                    If (intRecycleTime = 0) Then
                        dteNextCallDate = dteOriginalNextCallDate
                    Else
                        If (DateDiff("s", dteCreatedDate, CDate(dteStatusDate)) > 0) Then
                            dteNextCallDate = DateAdd("s", intRecycleTime, CDate(dteStatusDate))
                        Else
                            dteNextCallDate = DateAdd("s", intRecycleTime, CDate(dteCreatedDate))
                        End If
                        If (intTolerance > 0) Then
                            dteNextCallDate = DateAdd("s", -intTolerance, dteNextCallDate)
                        End If
                        dteAdjustedNextCallDate = dteNextCallDate
                        dteAdjustedNextCallDate = calcWorkingHour(dteAdjustedNextCallDate)
                        Do While (dteAdjustedNextCallDate <> dteNextCallDate)
                            dteNextCallDate = dteAdjustedNextCallDate
                            dteAdjustedNextCallDate = calcWorkingHour(dteNextCallDate)
                        Loop
                        dteNextCallDate = dteAdjustedNextCallDate
                    End If
                End If
            Next
        Else
            dteNextCallDate = Config.DefaultDateTime
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsNextCallTime = Nothing
        objDatabase = Nothing
        updateAnyDatabaseField("BusinessObjectID", BusinessObjectID, "tblbusinessobjectstatus", "BusinessObjectNextCallDate", "DTTM", dteNextCallDate, Config.DefaultDateTime)
        updateAnyDatabaseField("BusinessObjectID", BusinessObjectID, "tblbusinessobjectstatus", "BusinessObjectRecycleTime", "N", intRecycleTime, 0)
        If (checkValue(overrideSubStatus)) Then
            updateSingleDatabaseField(BusinessObjectID, "tblbusinessobjectstatus", "BusinessObjectSubStatusCode", "U", overrideSubStatus, "NULL")
        End If
    End Sub

    Shared Sub saveDiariseOn(ByVal AppID As String, ByVal DiaryID As String, ByVal WorkflowID As String)
        Dim intRecycleTime As Integer = 0, intTolerance As Integer = 0, strSubStatusCode As String = "", dteDiaryDueDate As String = ""
        Dim strSQL As String = "SELECT TOP 1 RecycleTime, WorkflowToleranceFactor, RecycleRuleSubStatusCode, DiaryDueDate FROM vwdiariseon " & _
                  "WHERE AppID = '" & AppID & "' AND DiaryID = '" & DiaryID & "' AND (WorkflowID = '" & WorkflowID & "' OR WorkflowID = 0) AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') " & _
                  "ORDER BY CompanyID DESC, RecycleRuleStatusCode DESC, RecycleRuleSubStatusCode DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intRecycleTime = Row.Item("RecycleTime")
                intTolerance = Row.Item("WorkflowToleranceFactor")
                If (checkValue(Row.Item("DiaryDueDate").ToString)) Then
                    dteDiaryDueDate = Row.Item("DiaryDueDate").ToString
                End If
                If (checkValue(Row.Item("RecycleRuleSubStatusCode").ToString)) Then
                    strSubStatusCode = Row.Item("RecycleRuleSubStatusCode").ToString
                End If
                If (strSubStatusCode <> "CBK" And strSubStatusCode <> "3BK") Then
                    dteDiaryDueDate = DateAdd("s", intRecycleTime, CDate(Config.DefaultDateTime))
                End If
            Next
        Else
            dteDiaryDueDate = Config.DefaultDateTime
        End If
        dsCache = Nothing
        strSQL = "UPDATE tbldiaries SET DiaryDueDate = " & formatField(dteDiaryDueDate, "DTTM", Config.DefaultDateTime) & " WHERE AppID = '" & AppID & "' AND DiaryID = '" & DiaryID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strSQL)
    End Sub

    Shared Sub saveBusinessDiariseOn(ByVal BusinessObjectID As String, ByVal DiaryID As String, ByVal WorkflowID As String)
        Dim intRecycleTime As Integer = 0, intTolerance As Integer = 0, strSubStatusCode As String = "", dteDiaryDueDate As String = ""
        Dim strSQL As String = "SELECT TOP 1 RecycleTime, WorkflowToleranceFactor, RecycleRuleSubStatusCode, DiaryDueDate FROM vwbusinessdiariseon " & _
                  "WHERE BusinessObjectID = '" & BusinessObjectID & "' AND DiaryID = '" & DiaryID & "' AND (WorkflowID = '" & WorkflowID & "' OR WorkflowID = 0) AND (CompanyID = 0 OR CompanyID = '" & CompanyID & "') " & _
                  "ORDER BY CompanyID DESC, RecycleRuleStatusCode DESC, RecycleRuleSubStatusCode DESC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 0
            For Each Row As DataRow In dsCache.Rows
                intRecycleTime = Row.Item("RecycleTime")
                intTolerance = Row.Item("WorkflowToleranceFactor")
                If (checkValue(Row.Item("DiaryDueDate").ToString)) Then
                    dteDiaryDueDate = Row.Item("DiaryDueDate").ToString
                End If
                If (checkValue(Row.Item("RecycleRuleSubStatusCode").ToString)) Then
                    strSubStatusCode = Row.Item("RecycleRuleSubStatusCode").ToString
                End If
                If (strSubStatusCode <> "CBK" And strSubStatusCode <> "3BK") Then
                    dteDiaryDueDate = DateAdd("s", intRecycleTime, CDate(Config.DefaultDateTime))
                End If
            Next
        Else
            dteDiaryDueDate = Config.DefaultDateTime
        End If
        dsCache = Nothing
        strSQL = "UPDATE tblbusinessobjectdiaries SET DiaryDueDate = " & formatField(dteDiaryDueDate, "DTTM", Config.DefaultDateTime) & " WHERE BusinessObjectID = '" & BusinessObjectID & "' AND DiaryID = '" & DiaryID & "' AND CompanyID = '" & CompanyID & "'"
        executeNonQuery(strSQL)
    End Sub

    Shared Sub saveOutOfOffice(companyid As String, userid As String, setuserid As String, outofoffice As String)
        executeNonQuery("UPDATE tblusers SET UserOutOfOfficeID = " & formatField(outofoffice, "N", OutOfOfficeStatus.Ready) & " WHERE UserID = '" & userid & "' AND CompanyID = '" & companyid & "'")
        Dim strQry As String = "INSERT INTO tbloutofofficelogs (CompanyID, UserID, OutOfOfficeID, OutOfOfficeSetByUserID) " & _
                        "VALUES(" & formatField(companyid, "N", 0) & ", " & _
                        formatField(userid, "N", 0) & ", " & _
                        formatField(outofoffice, "N", OutOfOfficeStatus.Ready) & ", " & _
                        formatField(setuserid, "N", userid) & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub incrementTransferAttempts(AppID As String, MediaCampaignIDOutbound As String)
        Dim strQry As String = "UPDATE tblmediacampaignscheduletransfers SET TransferAttempts = TransferAttempts + 1 WHERE MediaCampaignID = '" & MediaCampaignIDOutbound & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim intRowsAffected As Integer = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblmediacampaignscheduletransfers(CompanyID, AppID, MediaCampaignID, TransferAttempts) " & _
               "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
               formatField(AppID, "N", 0) & ", " & _
               formatField(MediaCampaignIDOutbound, "N", 0) & ", " & _
               formatField(1, "N", 1) & ") "
            executeNonQuery(strQry)
        End If
        incrementField("AppID", AppID, "TransferAttempts", "tblapplicationstatus", 1)
    End Sub

    Shared Sub createCustomerLogon(ByVal AppID As String)
        Dim strApp1FirstName As String = "", strApp1Surname As String = "", dteApp1DOB As String = "", strAddressPostCode As String = ""
        Dim strApp1EmailAddress As String = "", strAddressLine1 As String ="", strAddressLine2 As String ="", strAddressLine3 As String =""
		Dim strAddressHouseNumber As String = "", strAddressHouseName As String ="", strHomeTelephone As String ="", strMobileTelephone As String ="", strWorkTelephone As String ="", strAddressCounty As String =""
		Dim strQury As String = "SELECT App1EmailAddress, App1Firstname, App1Surname, AddressLine1, AddressLine2, AddressLine3, AddressPostcode, AddressHouseNumber, AddressHouseName, App1DOB, App1HomeTelephone, App1MobileTelephone, App1WorkTelephone, AddressCounty FROM tblapplications WHERE AppID = '" & AppID & "' AND CompanyID = '" & companyID & "'"
        Dim dsCache As DataTable = New Caching(Nothing, strQury, "", "", "").returnCache
        'check the data table has data 
        If (dsCache.Rows.Count > 0) Then
            'iterate through the rows and store the table data into the specific variables
            For Each Row As DataRow In dsCache.Rows
                strApp1EmailAddress = Row.Item("App1EmailAddress").ToString
				strApp1Firstname = Row.item("App1Firstname").ToString
				strApp1Surname = Row.item("App1Surname").ToString
				strAddressLine1 = Row.Item("AddressLine1").ToString
				strAddressLine2 = Row.Item("AddressLine2").ToString
				strAddressLine3 = Row.Item("AddressLine3").ToString
				strAddressPostcode = Row.Item("AddressPostcode").ToString
				strAddressHouseNumber = Row.Item("AddressHouseNumber").ToString
				strAddressHouseName = Row.Item("AddressHouseName").ToString
				dteApp1DOB = Row.Item("App1DOB").ToString
				strHomeTelephone = Row.Item("App1HomeTelephone").ToString
				strMobileTelephone = Row.Item("App1MobileTelephone").ToString
				strWorkTelephone = Row.Item("App1WorkTelephone").ToString
				strAddressCounty = Row.Item("AddressCounty").ToString				
            Next
        End If
        'clear cache
        dsCache = Nothing
        'check that the aplication is from a new customer or a previous customer
        If (checkValue(strApp1EmailAddress)) Then
            Dim strQry As String = "SELECT TOP 1 CustomerID FROM tblcustomerlogons WHERE Username = '" & strApp1EmailAddress & "' AND ActiveStatus = 1 AND CompanyID = '" & CompanyID & "'"
            Dim intCustomerID As String = New Caching(Nothing, strQry, "", "", "").returnCacheString
            'if the customer exists add the customer Id to the new app
            If (checkValue(intCustomerID)) Then
                executeNonQuery("UPDATE tblapplications SET CustomerID = " & intCustomerID & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
                'Dim strQry2 As String = "SELECT TOP 1 AppID FROM vwexportapplication WHERE App1EmailAddress = '" & strApp1EmailAddress & "' AND CompanyID = '" & CompanyID & "' ORDER BY CreatedDate ASC"
                'Dim intParentAppID As String = New Caching(Nothing, strQry2, "", "", "").returnCacheString
                'executeNonQuery("UPDATE tblapplications SET ParentAppID = " & intParentAppID & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            Else
                'create a new cusomer ID and add the user to the customerlogons table
                Dim strInsert As String = "INSERT INTO tblcustomerlogons(CompanyID, Username, Password) " & _
                                        "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                        formatField(strApp1EmailAddress, "L", "") & ", " & _
                                        formatField(hashString256(strApp1EmailAddress, generatePassword(8)), "", "") & ")"
                ' CustomerFirstname, CustomerSurname, CustomerAddressLine1, CustomerAddressLine2, CustomerAddressLine3, CustomerAddressPostcode, CustomerAddressHouseNumber, CustomerAddressHouseName, CustomerDOB, CustomerHomeTelephone, CustomerMobileTelephone, CustomerWorkTelephone, CustomerAddressCounty,
                'formatField(strApp1FirstName, "T", "") & ", " & _
                'formatField(strApp1Surname, "T", "") & ", " & _
                'formatField(strAddressLine1, "", "") & ", " & _
                'formatField(strAddressLine2, "", "") & ", " & _
                'formatField(strAddressLine3, "", "") & ", " & _
                'formatField(strAddressPostcode, "U", "") & ", " & _
                'formatField(strAddressHouseNumber, "L", "") & ", " & _
                'formatField(strAddressHouseName, "L", "") & ", " & _
                'formatField(dteApp1DOB, "DTTM", "NULL") & ", " & _
                'formatField(strHomeTelephone, "", "") & ", " & _
                'formatField(strMobileTelephone, "", "") & ", " & _
                'formatField(strWorkTelephone, "", "") & ", " & _
                'formatField(strAddressCounty, "", "") & ", " & _
                intCustomerID = executeIdentityQuery(strInsert)
                executeNonQuery("UPDATE tblapplications SET CustomerID = " & intCustomerID & " WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'")
            End If
        End If
    End Sub

    Shared Sub archivePDF(ByVal AppID As String, ByVal userid As String, ByVal pdftitle As String, ByVal pdfbody As String, smsid As String)
        Dim strQry As String = _
            "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryName, PDFHistoryBody, PDFHistoryCreatedUserID, PDFHistorySMSID) " & _
            "VALUES (" & formatField(CompanyID, "N", 0) & ", " & _
            formatField(AppID, "N", 0) & ", " & _
            formatField(pdftitle, "T", "") & ", " & _
            formatField(pdfbody, "", "") & ", " & _
            formatField(userid, "N", getSystemUser()) & ", " & _
            formatField(smsid, "N", 0) & ") "
        executeNonQuery(strQry)
    End Sub

    Shared Sub insertPayment(ByVal AppID As String, amount As Decimal, ByVal pmtType As String, method As String, pmtDate As String)
        Dim strSQL As String = "INSERT INTO tblpaymenthistory(CompanyID, AppID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryMethod, PaymentHistoryDate) " & _
           "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
           formatField(AppID, "N", 0) & ", " & _
           formatField(amount, "M", 0) & ", " & _
           formatField(pmtType, "T", "") & ", " & _
           formatField(method, "N", 0) & ", " & _
           formatField(pmtDate, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strSQL)
    End Sub

    Shared Function insertPaymentReturnID(ByVal AppID As String, amount As Decimal, ByVal pmtType As String, method As String, pmtDate As String) As String
        Dim strSQL As String = "INSERT INTO tblpaymenthistory(CompanyID, AppID, PaymentHistoryAmount, PaymentHistoryType, PaymentHistoryMethod, PaymentHistoryDate) " & _
           "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
           formatField(AppID, "N", 0) & ", " & _
           formatField(amount, "M", 0) & ", " & _
           formatField(pmtType, "T", "") & ", " & _
           formatField(method, "N", 0) & ", " & _
           formatField(pmtDate, "DTTM", Config.DefaultDateTime) & ") "
        Return executeIdentityQuery(strSQL)
    End Function

    Shared Sub updatePayment(ByVal pmtID As String, ByVal st As String)
        Dim strSQL As String = "UPDATE tblpaymenthistory SET PaymentHistoryStatus = " & st & " WHERE PaymentHistoryID = " & pmtID & " AND CompanyID = " & CompanyID
        executeNonQuery(strSQL)
    End Sub

    Shared Sub updatePaymentReference(ByVal pmtID As String, ByVal ref As String)
        Dim strSQL As String = "UPDATE tblpaymenthistory SET PaymentHistoryReference = '" & ref & "' WHERE PaymentHistoryID = " & pmtID & " AND CompanyID = " & CompanyID
        executeNonQuery(strSQL)
    End Sub

End Class