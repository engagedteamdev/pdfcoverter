﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://api.engaged-solutions.co.uk/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class EngagedAPI
    Inherits System.Web.Services.WebService

    ' General application
    Private tblapplications_flds As ArrayList = New ArrayList, tblapplications_vals As ArrayList = New ArrayList
    ' Application status
    Private tblapplicationstatus_flds As ArrayList = New ArrayList, tblapplicationstatus_vals As ArrayList = New ArrayList
    ' Previous Addresses
    Private tbladdresses_flds As ArrayList = New ArrayList, tbladdresses_vals As ArrayList = New ArrayList
    ' Previous Employers
    Private tblemployers_flds As ArrayList = New ArrayList, tblemployers_vals As ArrayList = New ArrayList
    ' Notes
    Private tblnotes_flds As ArrayList = New ArrayList, tblnotes_vals As ArrayList = New ArrayList
    ' Reminders
    Private tblreminders_flds As ArrayList = New ArrayList, tblreminders_vals As ArrayList = New ArrayList
    ' Data Store
    Private tbldatastore_flds As ArrayList = New ArrayList, tbldatastore_vals As ArrayList = New ArrayList
    ' Milestones
    Private tblapplicationstatusdates_flds As ArrayList = New ArrayList, tblapplicationstatusdates_vals As ArrayList = New ArrayList
 	' Adverse Credit
 	Private tbladversecredit_flds As ArrayList = New ArrayList, tbladversecredit_vals As ArrayList = New ArrayList
    ' Diaries
    Private tbldiaries_flds As ArrayList = New ArrayList, tbldiaries_vals As ArrayList = New ArrayList
    Private strGUID As String = Guid.NewGuid.ToString
    Private AppID As String = ""
    Private UserID As String = ""
    Private MediaCampaignID As String = "", MediaCampaignIDOutbound As String = ""
    Private blnSkipValidation As Boolean = False, intSkipTelephoneCorrection As String = "0"
    Private objInputXMLDoc As XmlDocument = New XmlDocument
    Private CompanyID As String = "0"

    ''' <summary>
    ''' Creates a new Engaged CRM application
    ''' </summary>
    ''' <param name="xmlString">XML document content</param>
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Xml)> _
    Public Function createApplication(ByVal xmlString As String, ByVal APIKey As String) As XmlDocument
        Try
            objInputXMLDoc.LoadXml(xmlString)
        Catch e As System.Xml.XmlException
            Return stringToXML(unsuccessfulMessage("Error in XML file", -1, strGUID))
        End Try
        Dim objHeaderTest As XmlNode = objInputXMLDoc.SelectSingleNode("//Application")
        If (objHeaderTest Is Nothing) Then
            Return stringToXML(unsuccessfulMessage("Data field not found. Field = Application", -2, strGUID))
        End If
        Dim objSkipValidation As XmlNode = objInputXMLDoc.SelectSingleNode("//SkipValidation")
        If (Not objSkipValidation Is Nothing) Then
            If (objSkipValidation.InnerText = "Y") Then
                blnSkipValidation = True
            End If
        End If
        Dim objblnSkipTelephoneCorrection As XmlNode = objInputXMLDoc.SelectSingleNode("//SkipTelephoneCorrection")
        If (Not objblnSkipTelephoneCorrection Is Nothing) Then
            If (objblnSkipTelephoneCorrection.InnerText = "Y") Then
                intSkipTelephoneCorrection = "1"
            End If
        End If
        Dim objMediaCampaignID As XmlNode = objInputXMLDoc.SelectSingleNode("//MediaCampaignID")
        If (Not objMediaCampaignID Is Nothing) Then
            MediaCampaignID = objMediaCampaignID.InnerText
        End If
        Dim objMediaCampaignIDOutbound As XmlNode = objInputXMLDoc.SelectSingleNode("//MediaCampaignIDOutbound")
        If (Not objMediaCampaignIDOutbound Is Nothing) Then
            MediaCampaignIDOutbound = objMediaCampaignIDOutbound.InnerText
        End If
        Dim objUserID As XmlNode = objInputXMLDoc.SelectSingleNode("//UserID")
        If (Not objUserID Is Nothing) Then
            UserID = objUserID.InnerText
        End If
        Dim strAuthority As String = checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), APIKey)
        If (checkValue(strAuthority)) Then
            Return stringToXML(strAuthority)
        End If
        Dim strMandatory As String = checkApplicationMandatory()
        If (checkValue(strMandatory)) Then
            Return stringToXML(strMandatory)
        End If
        Dim strReadData As String = readApplicationData()
        If (checkValue(strReadData)) Then
            Return stringToXML(strReadData)
        End If
        Dim strSaveData As String = saveApplicationData()
        Return stringToXML(strSaveData)
    End Function

    ''' <summary>
    ''' Returns status information about a specified application
    ''' </summary>
    ''' <param name="AppID">Unique application number</param>
    ''' <param name="CampaignID">Media campaign tracking number</param>
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Xml)> _
    Public Function checkLeadStatus(ByVal AppID As String, ByVal CampaignID As String, ByVal APIKey As String) As XmlDocument
        MediaCampaignID = CampaignID
        Dim strAuthority As String = checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), APIKey)
        If (checkValue(strAuthority)) Then
            Return stringToXML(strAuthority)
        End If
        If (checkValue(AppID)) Then
            Dim strXML As String = ""
            Dim strSQL As String = "SELECT TOP 1 * FROM vwleadstatus"
            If (checkValue(AppID)) Then
                strSQL += " WHERE AppID = '" & AppID & "'"
            End If
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<Response>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<Result>Success</Result>"
                    strXML += "<Status>1</Status>"
                    strXML += "<AppID>" & Row.Item("AppID") & "</AppID>"
                    strXML += "<Salutation>" & Row.Item("App1Salutation").ToString & "</Salutation>"
                    strXML += "<StatusCode>" & Row.Item("StatusCode") & "</StatusCode>"
                    strXML += "<ParnerRef>" & Row.Item("ClientReferenceOutbound").ToString & "</ParnerRef>"
                    strXML += "<PartnerName>" & Replace(Row.Item("MediaCampaignFriendlyNameOutbound").ToString, "& ", "&amp; ") & "</PartnerName>"
                    strXML += "<ParnerTelNo>" & Row.Item("MediaCampaignTelephoneNumberOutbound").ToString & "</ParnerTelNo>"
                    strXML += "<Amount>" & Row.Item("Amount") & "</Amount>"
                    strXML += "<ProductTerm>" & Row.Item("ProductTerm").ToString & "</ProductTerm>"
                Next
                strXML += "</Response>"
            Else
                Return stringToXML(unsuccessfulMessage("Application not found", -5, strGUID))
            End If
            dsCache = Nothing
            Return stringToXML(strXML)
        Else
            Return stringToXML(unsuccessfulMessage("No AppID was provided", -2, strGUID))
        End If
    End Function

    ''' <summary>
    ''' Creates a new inbound call to be assigned to a user
    ''' </summary>
    ''' <param name="UserRef">Unique user reference</param>
    ''' <param name="TelNo">Telephone number that the call originated from</param>
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Xml)> _
    Public Function addNewCall(ByVal AppID As String, ByVal UserRef As String, ByVal TelName As String, ByVal TelNo As String, ByVal APIKey As String) As XmlDocument
        Dim strAuthority As String = checkAuthoritySimple(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), APIKey)
        If (checkValue(strAuthority)) Then
            Return stringToXML(strAuthority)
        End If
        Dim strApplication As String = checkApplicationCompany(AppID, CompanyID)
        If (checkValue(strApplication)) Then
            Return stringToXML(strApplication)
        End If
        Dim strUserID As String = getUserIDFromUserStore("UserExternalRef", UserRef)
        If (checkValue(strUserID)) Then
            Dim strAccountNumber As String = getAnyFieldFromUserStore("ClickToDialAccountNumber", strUserID)
            Dim strQry As String = "INSERT INTO tbldialercalls (DialerAppID, CompanyID, DialerTelephoneName, DialerTelephoneNumberFrom, DialerTelephoneNumberTo, DialerIgnore) " & _
                       "VALUES(" & formatField(AppID, "N", 0) & ", " & _
                       formatField(CompanyID, "N", 0) & ", " & _
                       formatField(TelName, "T", "Inbound Call") & ", " & _
                       formatField(TelNo, "", "") & ", " & _
                       formatField(strAccountNumber, "", "") & ", " & _
                       formatField(0, "B", 0) & ") "
            executeNonQuery(strQry)
            Return stringToXML(successfulMessage("Call inserted", 1, strGUID))
        Else
            Return stringToXML(unsuccessfulMessage("User does not exist", -5, strGUID))
        End If
    End Function

    Private Function checkApplicationCompany(ByVal AppID As String, ByVal CompanyID As String) As String
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT AppID FROM tblapplications WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Return unsuccessfulMessage("Application not found", -4, strGUID)
        Else
            Return ""
        End If
    End Function

    Private Function checkAuthority(ByVal ip As String, ByVal apiKey As String) As String
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
              "FROM tblmediacampaigns INNER JOIN " & _
              "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
              "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
              "WHERE (tblmediacampaigns.MediaCampaignID = '" & MediaCampaignID & "') AND (tblwebservices.WebServiceAPIKey = '" & apiKey & "' OR '" & ip & "' = '127.0.0.1') " & _
              "AND (ISNULL(WebServiceActive,1) = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Return unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
        Else
            Return ""
        End If
    End Function

    Private Function checkAuthoritySimple(ByVal ip As String, ByVal apiKey As String) As String
        Dim boolAuthorised As Boolean
        Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
              "FROM tblmediacampaigns INNER JOIN " & _
              "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
              "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
              "WHERE (tblwebservices.WebServiceAPIKey = '" & apiKey & "' OR '" & ip & "' = '127.0.0.1') " & _
              "AND (ISNULL(WebServiceActive,1) = 1) "
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            CompanyID = objResult.ToString
            boolAuthorised = True
        Else
            boolAuthorised = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolAuthorised = False) Then
            Return unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
        Else
            Return ""
        End If
    End Function

    Private Function checkApplicationMandatory() As String
        Dim boolMandatory As Boolean = True
        Dim strMandatoryField As String = ""
        Dim arrMandatory As String() = {"MediaCampaignID", "ProductType", "Amount", "App1HomeTelephone", "AddressLine1", "AddressPostCode"}
        Dim objMandatory As XmlNode
        For i As Integer = 0 To UBound(arrMandatory)
            objMandatory = objInputXMLDoc.SelectSingleNode("//" & arrMandatory(i))
            If (objMandatory Is Nothing) Then
                boolMandatory = False
                strMandatoryField = arrMandatory(i)
                Exit For
            End If
        Next
        If (Not boolMandatory) Then
            Return unsuccessfulMessage("Mandatory Data Missing: " & strMandatoryField, -3, strGUID)
        Else
            Return ""
        End If
    End Function

    Private Function readApplicationData() As String
        Dim strValidate As String = ""
        Dim objChildNodes As XmlNodeList = objInputXMLDoc.DocumentElement.SelectSingleNode("//Application").ChildNodes
        For Each child As XmlNode In objChildNodes
            strValidate = validateApplicationData(child.Name, child.InnerText)
            If (checkValue(strValidate)) Then
                Exit For
            End If
        Next
        Return strValidate
    End Function

    '****************************************************
    ' Validate incoming fld and value and build arrays to
    ' hold vales for each table
    ' fld = field ,val = value
    '****************************************************
    Private Function validateApplicationData(ByVal fld As String, ByVal val As String) As String
        Dim boolValidate As Boolean = True
        Dim strSQL As String = "SELECT TOP 1 * FROM tblimportvalidation WHERE (CompanyID = 0 OR CompanyID = '" & CompanyID & "') AND ValName = '" & fld & "' AND ValStatus  = 'A' ORDER BY CompanyID DESC"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tblimportvalidation")
        Dim dsValidation As DataTable = objDataSet.Tables("tblimportvalidation")
        Dim strRegex As String = "", strMessage As String = "", strTable As String = "", strField As String = "", strFieldType As String = "", strFieldDefault As String = "", strFunction As String = "", strFunctionParameters As String = ""
        Dim strExecute As String = ""
        If (dsValidation.Rows.Count > 0) Then
            For Each Row As DataRow In dsValidation.Rows
                strRegex = Row.Item("ValRegex").ToString
                strMessage = Row.Item("ValRegexMessage").ToString
                strTable = Row.Item("ValTable").ToString
                strField = Row.Item("ValColumn").ToString
                strFieldType = Row.Item("ValType").ToString
                strFieldDefault = Row.Item("ValDefault").ToString
                strFunction = Row.Item("ValFunction").ToString
                strFunctionParameters = Row.Item("ValFunctionParameters").ToString
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsValidation = Nothing
        objDataBase = Nothing

        ' Call regular expression routine
        If (regexTest(strRegex, val) Or blnSkipValidation) Then
            If (checkValue(val)) Then
                If (checkValue(strFunction)) Then
                    If (strFunction = "splitApp1FullName") Then
                        splitApp1FullName(val)
                    ElseIf (strFunction = "splitApp2FullName") Then
                        splitApp2FullName(val)
                    Else
                        Dim arrParams As String() = {val}
                        If (checkValue(strFunctionParameters)) Then
                            Dim arrFunctionParameters As Array = Split(strFunctionParameters, ",")
                            For y As Integer = 0 To UBound(arrFunctionParameters)
                                If (checkValue(arrFunctionParameters(y))) Then
                                    ReDim Preserve arrParams(y + 1)
                                    arrParams(y + 1) = arrFunctionParameters(y)
                                End If
                            Next
                        End If
                        val = executeMethodByName(Me, strFunction, arrParams)
                    End If
                End If
                Select Case strTable
                    Case "tblapplications"
                        tblapplications_flds.Add(strField)
                        tblapplications_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatus"
                        tblapplicationstatus_flds.Add(strField)
                        tblapplicationstatus_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbladdresses"
                        tbladdresses_flds.Add(strField)
                        tbladdresses_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblemployers"
                        tblemployers_flds.Add(strField)
                        tblemployers_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblnotes"
                        tblnotes_flds.Add(strField)
                        tblnotes_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblreminders"
                        tblreminders_flds.Add(strField)
                        tblreminders_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tbldatastore"
                        tbldatastore_flds.Add(strField)
                        tbldatastore_vals.Add(formatField(val, strFieldType, strFieldDefault))
                    Case "tblapplicationstatusdates"
                        tblapplicationstatusdates_flds.Add(strField)
                        tblapplicationstatusdates_vals.Add(val)
                    Case "tbldiaries"
                        tbldiaries_flds.Add(strField)
                        tbldiaries_vals.Add(formatField(val, strFieldType, strFieldDefault))
				  Case "tblAdverseCredit"
				  
                        tbladversecredit_flds.Add(strField)
                        tbladversecredit_vals.Add(formatField(val, strFieldType, strFieldDefault))	
                End Select
            End If
            boolValidate = True
        Else
            boolValidate = False
        End If

        If (boolValidate = False) Then
            Return unsuccessfulMessage("Inconsistent data found. Field = " & fld & ", value = " & val & ", Criteria = " & strMessage, -5, strGUID)
        Else
            Return ""
        End If

    End Function

    '********************************************
    ' Read data arrays and build database queries
    '********************************************
    Private Function saveApplicationData() As String

        AppID = saveApplication(True, "", UserID, CompanyID, MediaCampaignID, MediaCampaignIDOutbound, intSkipTelephoneCorrection, _
                                                    tblapplications_flds, _
                                                    tblapplications_vals, _
                                                    tblapplicationstatus_flds, _
                                                    tblapplicationstatus_vals, _
                                                    tbladdresses_flds, _
                                                    tbladdresses_vals, _
                                                    tblemployers_flds, _
                                                    tblemployers_vals, _
                                                    tblnotes_flds, _
                                                    tblnotes_vals, _
                                                    tblreminders_flds, _
                                                    tblreminders_vals, _
                                                    tbldatastore_flds, _
                                                    tbldatastore_vals, _
                                                    tblapplicationstatusdates_flds, _
                                                    tblapplicationstatusdates_vals, _
                                                    tbldiaries_flds, _
                                                    tbldiaries_vals, _
													tbladversecredit_flds, _
                                                    tbladversecredit_vals, _
                                                    Nothing, _
                                                    Nothing, _
                                                    "N")

        Dim strStatusCode As String = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
        Dim strSubStatusCode As String = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
        If (strStatusCode = "INV") Then
            Select Case strSubStatusCode
                Case "DUP"
                    Return unsuccessfulMessage("Duplicate case", -5, strGUID)
                Case "WRN"
                    Return unsuccessfulMessage("Invalid telephone number", -5, strGUID)
                Case Else
                    Return ""
            End Select
        Else
            Return successfulMessage("Application successfully received", 1, AppID)
        End If

    End Function

    Private Function successfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String) As String
        HttpContext.Current.Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""http://api.engaged-solutions.co.uk/"">")
            .Append("<Result>" & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Return objStringBuilder.ToString
    End Function

    Private Function unsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String) As String
        HttpContext.Current.Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""http://api.engaged-solutions.co.uk/"">")
            .Append("<Result>Invalid request received: " & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append("</Response>")
        End With
        saveXMLReceived(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), app, st, msg)
        Return objStringBuilder.ToString
    End Function

    Private Sub saveXMLReceived(ByVal ip As String, ByVal soapid As String, ByVal result As Integer, ByVal msg As String)
        Dim strQry As String = "INSERT INTO tblxmlreceived (CompanyID, XMLSourceIPAddress, XMLReceivedAppID, XMLReceivedResult, XMLReceivedMessage, XMLReceived, XMLReceivedDate) " & _
                   "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                   formatField(ip, "", "") & ", " & _
                   formatField(soapid, "", 0) & ", " & _
                   formatField(result, "N", 0) & ", " & _
                   formatField(msg, "", "") & ", " & _
                   formatField(objInputXMLDoc.InnerXml, "", "") & ", " & _
                   formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ") "
        executeNonQuery(strQry)
    End Sub

    Private Sub splitApp1FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 1)
    End Sub

    Private Sub splitApp2FullName(ByVal strFullName As String)
        Call splitFullName(strFullName, 2)
    End Sub

    Private Sub splitFullName(ByVal strFullName As String, ByVal intAppNo As Integer)
        Dim boolTitleSet As Boolean = False
        Dim strMiddleNames As String = ""
        Dim arrFullName As Array = Split(strFullName, " ")
        For i As Integer = 0 To UBound(arrFullName)
            If (i = UBound(arrFullName)) Then
                validateApplicationData("App" & intAppNo & "Surname", arrFullName(i))
            ElseIf (i = 0) And (titleLookup(arrFullName(i))) Then
                validateApplicationData("App" & intAppNo & "Title", arrFullName(i))
                boolTitleSet = True
            ElseIf (i = 0) And (Not boolTitleSet) Then
                validateApplicationData("App" & intAppNo & "FirstName", arrFullName(i))
            ElseIf (boolTitleSet) And (i = 1) And (i < UBound(arrFullName)) Then
                validateApplicationData("App" & intAppNo & "FirstName", arrFullName(i))
            Else
                If (strMiddleNames = "") Then
                    strMiddleNames = arrFullName(i)
                Else
                    strMiddleNames = strMiddleNames & " " & arrFullName(i)
                End If
            End If
        Next
        If (strMiddleNames <> "") Then
            validateApplicationData("App" & intAppNo & "MiddleNames", strMiddleNames)
        End If
    End Sub

    Private Function getAnyFieldFromUserStore(ByVal fld As String, ByVal UserID As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT StoredUserValue FROM tbluserstore WHERE StoredUserName = '" & fld & "' AND UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    Private Function getUserIDFromUserStore(ByVal fld As String, ByVal val As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT UserID FROM tbluserstore WHERE StoredUserName = '" & fld & "' AND StoredUserValue = '" & val & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

End Class