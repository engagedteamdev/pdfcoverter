﻿Imports System
Imports System.Net
Imports System.Threading
Imports System.Text
Imports System.IO

Public Class RequestState

    Public objWebRequest As WebRequest = Nothing, objData As Object = Nothing, strSiteURL As String = ""

    Public Property WebRequest() As WebRequest
        Get
            Return objWebRequest
        End Get
        Set(ByVal val As WebRequest)
            objWebRequest = val
        End Set
    End Property

    Public Property RequestData() As Object
        Get
            Return objData
        End Get
        Set(ByVal val As Object)
            objData = val
        End Set
    End Property

    Public Property RequestURL() As String
        Get
            Return strSiteURL
        End Get
        Set(ByVal val As String)
            strSiteURL = val
        End Set
    End Property

    Public Sub New(ByVal request As WebRequest, ByVal data As Object, ByVal siteUrl As String)
        WebRequest() = request
        RequestData() = data
        RequestURL() = siteUrl
    End Sub

End Class