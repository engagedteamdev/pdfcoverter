﻿Imports Microsoft.VisualBasic
Imports LogonCheck, Common

' ** Revision history **
'
' ** End Revision History **

Public Class Config
    '*******************************************************
    ' Define default values if some data values aren't found
    '*******************************************************
    Public Const strEnvironment As String = "live"
    Public objSystemProperties As New Dictionary(Of String, String)
    Private strDefaultUserName As String = "", strDefaultUserFullName As String = "", strDefaultUserTeamID As String = "", strDefaultUserReference As String = "", strDefaultUserDialerReference As String = ""
    Private intUserActiveWorkflowTypeID As String = "", intUserActiveWorkflowTypeUserLevel As String = "", intUserActiveWorkflowID As String = "", intUserActiveWorkflowActivityID As String = "", intUserActiveWorkflowAppID As String = "", intUserActiveWorkflowDiaryID As String = "", strUserActiveWorkflowTelephoneNumber As String = "", intUserActiveLevelID As String = "", strcolor As String = ""
    Private strReportBuilderID As String = "", strReportBuilderSearchID As String = "", strReportBuilderDiaryID As String = "", strReportBuilderStatusSummaryID As String = "", strReportBuilderCaseHistoryID As String = ""
    Private strBusinessObjectReportBuilderID As String = "", strBusinessObjectReportBuilderSearchID As String = "", strBusinessObjectReportBuilderDiaryID As String = "", strBusinessObjectReportBuilderCaseHistoryID As String = ""
    Private boolSuperAdmin As Boolean = False, boolReports As Boolean = False, boolWorkflows As Boolean = False, boolManager As Boolean = False, boolSeniorManager As Boolean = False, boolCallCentre As Boolean = False, boolSales As Boolean = False, boolAdministrator As Boolean = False, boolRep As Boolean = False, boolCallCentreManager As Boolean = False, boolPartner As Boolean = False, boolSupplier As Boolean = False, boolMasterMedia As Boolean = False, intSupplierCompanyID As String = 0, intPartnerCompanyID As String = 0, intMasterMediaID As String = 0, boolClickToDial As String = False, intUserOutOfOfficeID As Integer = 0
    Private Shared strVersionJQuery As String = "", strVersionCSS As String = "", strVersionFormScripts As String = "", strVersionReportScripts As String = "", strDefaultUserEmailAddress As String = ""
    Private intInboundWorkflowID As String = "", intRemindersWorkflowID As String = "", strHolidayRequestUserID As String = ""

    Public Sub New()

    End Sub

    Public Sub assignProperties()

        Dim strSQL As String = "SELECT UserID, UserName, UserFullName, UserReference, UserDialerReference, UserSuperAdmin, UserReports, UserWorkflows, UserManager, UserSeniorManager, UserCallCentre, UserSales, UserAdministrator, UserActiveLevelID, UserRep, UserActiveWorkflowTypeID, UserActiveWorkflowTypeUserLevel, UserActiveWorkflowID, UserActiveWorkflowActivityID, UserActiveWorkflowAppID, UserActiveWorkflowDiaryID, UserActiveWorkflowTelephoneNumber, UserCallCentreManager, CompanyID, UserSupplierCompanyID, UserPartnerCompanyID, UserMasterMediaID, UserClickToDial, UserOutOfOfficeID, UserTeamID, UserEmailAddress FROM tblusers WHERE UserSessionID = '" & cookieValue("ASP.NET_SessionId") & "' AND UserLoggedIn = 1"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                strDefaultUserName = Row.Item("UserName")
                strDefaultUserFullName = Row.Item("UserFullName")
                strDefaultUserTeamID = Row.Item("UserTeamID")
                strDefaultUserReference = Row.Item("UserReference")
                strDefaultUserDialerReference = Row.Item("UserDialerReference")
				strDefaultUserEmailAddress = Row.Item("UserEmailAddress")
                boolSuperAdmin = Row.Item("UserSuperAdmin")
                boolReports = Row.Item("UserReports")
                boolWorkflows = Row.Item("UserWorkflows")
                boolCallCentre = Row.Item("UserCallCentre")
                boolSales = Row.Item("UserSales")
                boolAdministrator = Row.Item("UserAdministrator")
                boolRep = Row.Item("UserRep")
                boolCallCentreManager = Row.Item("UserCallCentreManager")
                boolManager = Row.Item("UserManager")
                boolSeniorManager = Row.Item("UserSeniorManager")
                intUserActiveLevelID = Row.Item("UserActiveLevelID")
                intUserActiveWorkflowTypeID = Row.Item("UserActiveWorkflowTypeID")
                intUserActiveWorkflowTypeUserLevel = Row.Item("UserActiveWorkflowTypeUserLevel")
                intUserActiveWorkflowID = Row.Item("UserActiveWorkflowID")
                intUserActiveWorkflowActivityID = Row.Item("UserActiveWorkflowActivityID")
                intUserActiveWorkflowAppID = Row.Item("UserActiveWorkflowAppID")
                intUserActiveWorkflowDiaryID = Row.Item("UserActiveWorkflowDiaryID")
                strUserActiveWorkflowTelephoneNumber = Row.Item("UserActiveWorkflowTelephoneNumber").ToString
                boolClickToDial = Row.Item("UserClickToDial")
                intSupplierCompanyID = Row.Item("UserSupplierCompanyID")
                intPartnerCompanyID = Row.Item("UserPartnerCompanyID")
                intMasterMediaID = Row.Item("UserMasterMediaID")
                If (intSupplierCompanyID > 0) Then boolSupplier = True
                If (intPartnerCompanyID > 0) Then boolPartner = True
                If (intMasterMediaID > 0) Then boolMasterMedia = True
                intUserOutOfOfficeID = Row.Item("UserOutOfOfficeID")
            Next
        End If
        dsUsers.Clear()
        dsUsers = Nothing
        objDataSet = Nothing

        strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND CompanyID = '" & CompanyID & "'"
        objDataSet = objDatabase.executeReader(strSQL, "tblsystemconfiguration")
        Dim dsConfig As DataTable = objDataSet.Tables("tblsystemconfiguration")
        If (dsConfig.Rows.Count > 0) Then
            For Each Row As DataRow In dsConfig.Rows
				Try
                	objSystemProperties.Add(Row.Item("SystemConfigurationName"), Row.Item("SystemConfigurationValue"))
				Catch e As Exception
					' Do nothing
				End Try
                If (Row.Item("SystemConfigurationName") = "VersionJQuery") Then strVersionJQuery = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "VersionCSS") Then strVersionCSS = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "VersionFormScripts") Then strVersionFormScripts = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "VersionReportScripts") Then strVersionReportScripts = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ReportBuilderID") Then strReportBuilderID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ReportBuilderSearchID") Then strReportBuilderSearchID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ReportBuilderDiaryID") Then strReportBuilderDiaryID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ReportBuilderStatusSummaryID") Then strReportBuilderStatusSummaryID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "ReportBuilderCaseHistoryID") Then strReportBuilderCaseHistoryID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BusinessObjectReportBuilderID") Then strBusinessObjectReportBuilderID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BusinessObjectReportBuilderSearchID") Then strBusinessObjectReportBuilderSearchID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BusinessObjectReportBuilderDiaryID") Then strBusinessObjectReportBuilderDiaryID = Row.Item("SystemConfigurationValue")
                If (Row.Item("SystemConfigurationName") = "BusinessObjectReportBuilderCaseHistoryID") Then strBusinessObjectReportBuilderCaseHistoryID = Row.Item("SystemConfigurationValue")
				If (Row.Item("SystemConfigurationName") = "HolidayRequestUserID") Then strHolidayRequestUserID = Row.Item("SystemConfigurationValue")
				
            Next
        End If
        dsConfig.Clear()
        dsConfig = Nothing
        objDataSet = Nothing
    End Sub

    Public Shared Property DefaultDate() As Date
        Get
            Return DateTime.Today
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Shared Property DefaultTime() As Date
        Get
            Return TimeOfDay
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Shared Property DefaultDateTime() As Date
        Get
            Return Now
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return getSystemProperty("CompanyName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultEmailFromAddress() As String
        Get
            Return getSystemProperty("EmailAddress")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultUserID() As String
        Get
            'If (checkValue(strDefaultUserID)) Then
            '    Return strDefaultUserID
            'Else
            Return cookieValue("LeadPlatform", "UserID")
            'End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserIDNew() As String
        Get
            'If (checkValue(strDefaultUserID)) Then
            '    Return strDefaultUserID
            'Else
            Return cookieValue("LeadPlatform", "UserID")
            'End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserName() As String
        Get
            Return strDefaultUserName
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserFullName() As String
        Get
            Return strDefaultUserFullName
        End Get
        Set(ByVal val As String)

        End Set
    End Property
	
    Public Property DefaultUserFullNameNew() As String
        Get
            'If (checkValue(strDefaultUserID)) Then
            '    Return strDefaultUserID
            'Else
            Return cookieValue("LeadPlatform", "UserFullName")
            'End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserTeamID() As String
        Get
            Return strDefaultUserTeamID
        End Get
        Set(ByVal val As String)

        End Set
    End Property
	
    Public Shared Property DefaultUserEmailAddress() As String
        Get
            Return strDefaultUserEmailAddress
        End Get
        Set(ByVal val As String)

        End Set
    End Property
	
	Public Property HolidayRequestUserID() As String
        Get
            Return strHolidayRequestUserID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultRoleName() As String
        Get
            Dim strRoleName As String = ""
            Select Case intUserActiveLevelID
                Case 1
                    strRoleName = "Sales"
                Case 2
                    strRoleName = "Call Centre"
                Case 3
                    strRoleName = "Admin"
            End Select
            Return strRoleName
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserReference() As String
        Get
            Return strDefaultUserReference
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property DefaultUserDialerReference() As String
        Get
            Return strDefaultUserDialerReference
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property UserSessionID() As String
        Get
            Return cookieValue("ASP.NET_SessionId", "")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowID() As String
        Get
            Return intUserActiveWorkflowID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowTypeID() As String
        Get
            Return intUserActiveWorkflowTypeID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowTypeUserLevel() As String
        Get
            Return intUserActiveWorkflowTypeUserLevel
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowActivityID() As String
        Get
            Return intUserActiveWorkflowActivityID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowAppID() As String
        Get
            Return intUserActiveWorkflowAppID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowDiaryID() As String
        Get
            Return intUserActiveWorkflowDiaryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveWorkflowTelephoneNumber() As String
        Get
            Return strUserActiveWorkflowTelephoneNumber
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserOutOfOfficeID() As String
        Get
            Return intUserOutOfOfficeID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property HTTPS() As Boolean
        Get
            If (UCase(HttpContext.Current.Request.ServerVariables("HTTPS")) = "OFF") Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Shared Property ApplicationURL() As String
        Get
            Dim strURL As String = System.Web.HttpContext.Current.Request.Url.ToString
            strURL = Replace(strURL, "https://", "")
            strURL = Replace(strURL, "http://", "")
            strURL = Replace(strURL, "www.", "")
            Dim arrURL As Array = Split(strURL, ".")
            If (Not HTTPS()) Then
                Return "http://" & CurrentDomain()
            Else
                Return "https://" & CurrentDomain()
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property CurrentDomain() As String
        Get
            Return Replace(HttpContext.Current.Request.ServerVariables("SERVER_NAME"), "www.", "")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property BaseFolder() As String
        Get
            Return getSystemProperty("BaseFolder")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property CallInterface() As String
        Get
            Dim strForceCallInterface As String = getSystemProperty("ForceCallInterface")
            If (strForceCallInterface = "Y") Then
                Return "Y"
            Else
                Dim strCallInterface As String = getAnyField("WorkflowCallInterface", "tblworkflows", "WorkflowID", UserActiveWorkflowID)
                If (strCallInterface = "True") Then Return "Y" Else Return "N"
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property WorkflowDiary() As String
        Get
            Dim strWorkflowDiary As String = getAnyField("WorkflowDiary", "tblworkflows", "WorkflowID", UserActiveWorkflowID)
            If (strWorkflowDiary = "True") Then Return "Y" Else Return "N"
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SuperAdmin() As Boolean
        Get
            Return boolSuperAdmin
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Reports() As Boolean
        Get
            Return boolReports
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Workflows() As Boolean
        Get
            Return boolWorkflows
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property CallCentre() As Boolean
        Get
            Return boolCallCentre
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property CallCentreManager() As Boolean
        Get
            Return boolCallCentreManager
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Manager() As Boolean
        Get
            Return boolManager
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property SeniorManager() As Boolean
        Get
            Return boolSeniorManager
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Sales() As Boolean
        Get
            Return boolSales
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Administrator() As Boolean
        Get
            Return boolAdministrator
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Property Rep() As Boolean
        Get
            Return boolRep
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Property Supplier() As Boolean
        Get
            Return boolSupplier
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Property Partner() As Boolean
        Get
            Return boolPartner
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Property MasterMedia() As Boolean
        Get
            Return boolMasterMedia
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Property SupplierCompanyID() As String
        Get
            Return intSupplierCompanyID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property PartnerCompanyID() As String
        Get
            Return intPartnerCompanyID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property MasterMediaID() As String
        Get
            Return intMasterMediaID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property UserActiveLevelID() As String
        Get
            Return intUserActiveLevelID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ClickToDial() As Boolean
        Get
            Return boolClickToDial
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Shared Property LoggedIn() As Boolean
        Get
            If (cookieValue("LeadPlatform", "Authorised") = "") Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Shared Property CompanyID() As String
        Get
            If checkValue(cookieValue("LeadPlatform", "CompanyID")) Then
                Return cookieValue("LeadPlatform", "CompanyID")
            Else
                If checkValue(HttpContext.Current.Request("AppID")) And checkValue(HttpContext.Current.Request("MediaCampaignID")) Then
                    Dim strSQL As String = "SELECT CompanyID FROM tblapplications WHERE AppID = '" & HttpContext.Current.Request("AppID") & "' AND MediaCampaignIDInbound = '" & HttpContext.Current.Request("MediaCampaignID") & "'"
                    Dim objDatabase As DatabaseManager = New DatabaseManager
                    Dim objResult As Object = objDatabase.executeScalar(strSQL)
                    If (objResult IsNot Nothing) Then
                        Return objResult.ToString
                    Else
                        Return 0
                    End If
                    objResult = Nothing
                    objDatabase = Nothing
                ElseIf (checkValue(HttpContext.Current.Request("MediaCampaignID"))) Then
                    Dim strSQL As String = "SELECT CompanyID FROM tblmediacampaigns WHERE MediaCampaignID = '" & HttpContext.Current.Request("MediaCampaignID") & "'"
                    Dim objDatabase As DatabaseManager = New DatabaseManager
                    Dim objResult As Object = objDatabase.executeScalar(strSQL)
                    If (objResult IsNot Nothing) Then
                        Return objResult.ToString
                    Else
                        Return 0
                    End If
                    objResult = Nothing
                    objDatabase = Nothing
                Else
					If (checkValue(HttpContext.Current.Request("UserSessionID"))) Then
						Dim strSQL As String = "SELECT CompanyID FROM tblusers WHERE UserSessionID = '" & HttpContext.Current.Request("UserSessionID") & "' AND UserLoggedIn = 1"
						Dim objDatabase As DatabaseManager = New DatabaseManager
						Dim objResult As Object = objDatabase.executeScalar(strSQL)
						If (objResult IsNot Nothing) Then
							Return objResult.ToString
						Else
							Return 0
						End If
						objResult = Nothing
						objDatabase = Nothing
					Else
						Return 0
					End If
                End If
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property VersionImages() As String
        Get
            Return getSystemProperty("VersionImages")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property VersionJQuery() As String
        Get
            Return strVersionJQuery

        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property VersionCSS() As String
        Get
            Return strVersionCSS

        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property VersionFormScripts() As String
        Get
            Return strVersionFormScripts

        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property VersionReportScripts() As String
        Get
            Return strVersionReportScripts

        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ReportBuilderID() As String
        Get
            Return strReportBuilderID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ReportBuilderSearchID() As String
        Get
            Return strReportBuilderSearchID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ReportBuilderDiaryID() As String
        Get
            Return strReportBuilderDiaryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ReportBuilderStatusSummaryID() As String
        Get
            Return strReportBuilderStatusSummaryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ReportBuilderCaseHistoryID() As String
        Get
            Return strReportBuilderCaseHistoryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property BusinessObjectReportBuilderID() As String
        Get
            Return strBusinessObjectReportBuilderID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property BusinessObjectReportBuilderSearchID() As String
        Get
            Return strBusinessObjectReportBuilderSearchID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property BusinessObjectReportBuilderDiaryID() As String
        Get
            Return strBusinessObjectReportBuilderDiaryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property BusinessObjectReportBuilderCaseHistoryID() As String
        Get
            Return strBusinessObjectReportBuilderCaseHistoryID
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ClickToDialProvider() As String
        Get
            Return getSystemProperty("ClickToDialProvider")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property ClickToDialProviderURL() As String
        Get
            Return getSystemProperty("ClickToDialProviderURL")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProvider() As String
        Get
            Return getSystemProperty("SMSProvider")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderURL() As String
        Get
            Return getSystemProperty("SMSProviderURL")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderUserName() As String
        Get
            Return getSystemProperty("SMSProviderUserName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderPassword() As String
        Get
            Return getSystemProperty("SMSProviderPassword")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderFrom() As String
        Get
            Return getSystemProperty("SMSProviderFrom")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderMaxSMSLength() As String
        Get
            Return getSystemProperty("SMSProviderMaxSMSLength")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SMSProviderConcatenateSMS() As String
        Get
            Return getSystemProperty("SMSProviderConcatenateSMS")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property SystemExpiryDate() As String
        Get
            Return getSystemProperty("SystemExpiryDate")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property InboundWorkflowID() As String
        Get
            intInboundWorkflowID = getSystemProperty("InboundWorkflowID")
            If (checkValue(intInboundWorkflowID)) Then
                Return intInboundWorkflowID
            Else
                Return "1"
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Property RemindersWorkflowID() As String
        Get
            intRemindersWorkflowID = getSystemProperty("RemindersWorkflowID")
            If (checkValue(intRemindersWorkflowID)) Then
                Return intRemindersWorkflowID
            Else
                Return "2"
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property
	 
	public property CRMColour1 AS String
		 Get
            Return getSystemProperty("CRMColour1")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMColour2 AS String
		 Get
            Return getSystemProperty("CRMColour2")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMMenuFont AS String
		  
		  Get		
			Return getSystemProperty("CRMMenuFontCol")				
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMMenuHoverFont AS String
		 Get
			Return getSystemProperty("CRMMenuFontHoverCol")				       
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMMenuHover AS String
		 Get
            Return getSystemProperty("CRMMenuHoverCol")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	

	public property CRMSubMenuHover AS String
		 Get
            Return getSystemProperty("CRMSubMenuHoverCol")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMSubMenuHoverFont AS String
		 Get
            Return getSystemProperty("CRMSubMenuHovFontCol")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMSubMenuFont AS String
		 Get
            Return getSystemProperty("CRMSubMenuFontCol")
        End Get
        Set(ByVal val As String)

        End Set
	End Property
	
	public property CRMExpanding AS String
		 Get
            Return getSystemProperty("CRMExpanding")
        End Get
        Set(ByVal val As String)

        End Set
	End Property

    Private Function getSystemProperty(ByVal nm As String) As String
        Try
            Return objSystemProperties(nm)
        Catch err As KeyNotFoundException
            Return ""
        End Try
    End Function

End Class
