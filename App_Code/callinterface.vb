﻿Imports Config, Common
Imports Microsoft.VisualBasic
Imports System.Net
Imports System.IO

Public Class CallInterface

    Public Enum ActivityType
        StartWorkflow = 1
        StartCase = 2
        StartCall = 3
        EndCall = 4
        EndCase = 5
        EndWorkflow = 6
		ClearWorkflow = 0
    End Enum

    Shared Sub callLogging(ByVal AppID As String, ByVal action As String, ByVal telno As String, ByVal UserID As String, Optional ByVal StatusCode As String = "", Optional ByVal SubStatusCode As String = "", Optional ByVal WorkflowID As String = "", Optional ByVal WorkflowTypeID As String = "")

        If (Not checkValue(action)) Then Exit Sub
        If (Not checkValue(UserID)) Then Exit Sub

        Dim strTelephoneNumber As String = ""
        Dim intWorkflowID As String = getAnyField("UserActiveWorkflowID", "tblusers", "UserID", UserID)
        Dim intWorkflowTypeID As String = getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", UserID)
        Dim intWorkflowTypeAccesLevel As String = getAnyField("UserActiveWorkflowTypeUserLevel", "tblusers", "UserID", UserID)
        Dim strWorkflowTelephoneNumber As String = getAnyField("UserActiveWorkflowTelephoneNumber", "tblusers", "UserID", UserID)
        Dim intWorkflowActivityID As String = getAnyField("UserActiveWorkflowActivityID", "tblusers", "UserID", UserID)
        Dim intWorkflowAppID As String = getAnyField("UserActiveWorkflowAppID", "tblusers", "UserID", UserID)

        'HttpContext.Current.Response.Write(action & ",")
        'HttpContext.Current.Response.Write(ActivityType.StartWorkflow & ",")
        'HttpContext.Current.Response.Write((action = ActivityType.StartWorkflow) & ",")
        'HttpContext.Current.Response.Write(WorkflowTypeID & ",")
        'HttpContext.Current.Response.Write(intWorkflowTypeID & ",")
        'HttpContext.Current.Response.Write((CInt(intWorkflowActivityID) = 0) & ",")
        'HttpContext.Current.Response.Write((CInt(intWorkflowActivityID) = ActivityType.EndWorkflow) & ",")

        'If (Not checkValue(checkCaseLock(UserID, AppID))) Then
            If (action = ActivityType.StartWorkflow And intWorkflowActivityID = ActivityType.StartCase) Then
                ' *** User clicks back button after starting case *** '
                If (checkValue(intWorkflowTypeID)) Then
                    insertIdleHistory(intWorkflowAppID, UserID)
                End If
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowID               = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowAppID            = " & formatField("", "N", 0) & ", " & _
					"UserActiveWorkflowDiaryID          = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
                If (checkValue(intWorkflowTypeID)) Then
                    insertInCallHistory(intWorkflowAppID, StatusCode, SubStatusCode, UserID)
                    insertWrapHistory(intWorkflowAppID, UserID)
                End If
            ElseIf (action = ActivityType.StartWorkflow And intWorkflowActivityID = ActivityType.StartCall) Then
            ' *** User clicks back button after starting call *** '
            If (Len(AppID) < 7) Then ' Business calls
                responseRedirect("/businessprocessing.aspx?BusinessObjectID=" & intWorkflowAppID)
            Else
                responseRedirect("/processing.aspx?AppID=" & intWorkflowAppID)
            End If
            ElseIf (action = ActivityType.StartWorkflow) Then '(CInt(action) = ActivityType.StartWorkflow And ((CInt(intWorkflowActivityID) = 0) Or (CInt(intWorkflowActivityID) = ActivityType.EndWorkflow))) Then
                ' *** Start of workflow - can only be started when last action is blank *** '
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowTypeID           = " & formatField(WorkflowTypeID, "N", 0) & ", " & _
                    "UserActiveWorkflowID               = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowAppID            = " & formatField("", "N", 0) & ", " & _
					"UserActiveWorkflowDiaryID          = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            ElseIf (action = ActivityType.StartCase And (intWorkflowActivityID = ActivityType.ClearWorkflow Or intWorkflowActivityID = ActivityType.StartWorkflow Or intWorkflowActivityID = ActivityType.EndCase)) Then
                ' *** Start of next case - can only be started from started workflow or ended case *** '
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowID               = " & formatField(WorkflowID, "N", 0) & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowAppID            = " & formatField(AppID, "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            ElseIf (action = ActivityType.StartCall And (intWorkflowActivityID = ActivityType.StartCase Or intWorkflowActivityID = ActivityType.EndCall) And checkValue(telno)) Then
                ' *** Start of call - can only be started from ended call or start of case *** '
                If (checkValue(intWorkflowTypeID)) Then
                    If (intWorkflowActivityID = ActivityType.EndCall) Then
                        insertWrapHistory(AppID, UserID)
                    Else
                        insertIdleHistory(AppID, UserID)
                    End If
                End If
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowAppID            = " & formatField(AppID, "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField(telno, "", "NULL") & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
                If (checkValue(intWorkflowTypeID)) Then
                    If (intWorkflowActivityID = ActivityType.EndCall) Then
                        insertIdleHistory(AppID, UserID)
                    End If
                End If
            ElseIf (action = ActivityType.EndCall And intWorkflowActivityID = ActivityType.StartCall) Then
                ' *** End of call - can only be ended from started call *** '
            If (checkValue(intWorkflowTypeID)) Then
                    insertInCallHistory(AppID, StatusCode, SubStatusCode, UserID)
                End If
                    Dim strSQL As String = "UPDATE tblusers SET " & _
                        "UserActiveWorkflowAppID            = " & formatField(AppID, "N", 0) & ", " & _
                        "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                        "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                        "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                        "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                        "WHERE UserID                       = '" & UserID & "' " & _
                        "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            ElseIf (action = ActivityType.EndCase And intWorkflowActivityID = ActivityType.EndCall) Then
                ' *** End of current case - can only be ended from ended call *** '
                If (checkValue(intWorkflowTypeID)) Then
                    insertWrapHistory(AppID, UserID)
                End If
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowAppID            = " & formatField(AppID, "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            ElseIf (action = ActivityType.EndWorkflow And (intWorkflowActivityID = ActivityType.EndCall Or intWorkflowActivityID = ActivityType.EndCase)) Then
                ' *** End of workflow reached - can only be reached from ended case *** '
                If (intWorkflowActivityID = ActivityType.EndCall) Then
                    insertWrapHistory(intWorkflowAppID, UserID)
                End If
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowID               = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowAppID            = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowDiaryID          = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
                    "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            ElseIf (action = ActivityType.ClearWorkflow) Then
                ' *** Reset workflow *** '
                Dim strSQL As String = "UPDATE tblusers SET " & _
                    "UserActiveWorkflowID               = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowTypeID           = " & formatField(0, "N", 0) & ", " & _
                    "UserActiveWorkflowTypeUserLevel    = " & formatField(0, "N", 0) & ", " & _
                    "UserActiveWorkflowActivityID       = " & formatField(action, "N", action) & ", " & _
                    "UserActiveWorkflowAppID            = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowDiaryID          = " & formatField("", "N", 0) & ", " & _
                    "UserActiveWorkflowTelephoneNumber  = " & formatField("", "", "NULL") & ", " & _
                    "UserActiveWorkflowStartTime        = " & formatField("", "DTTM", "NULL") & ", " & _
                    "UserPreCallStartTime               = " & formatField("", "DTTM", "NULL") & " " & _
                    "WHERE UserID                       = '" & UserID & "' " & _
                    "AND CompanyID                      = '" & CompanyID & "'"
                executeNonQuery(strSQL)
            End If
        'End If

    End Sub

    Shared Sub insertIdleHistory(ByVal AppID As String, ByVal UserID As String, Optional ByVal calltime As String = "")
        Dim boolRedial As Boolean = True
        Dim strListName As String = getAnyField("WorkflowReference", "tblworkflows", "WorkflowID", getAnyField("UserActiveWorkflowID", "tblusers", "UserID", UserID))
        Dim strJobName As String = getAnyField("WorkflowType", "tblworkflowtypes", "WorkflowTypeID", getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", UserID))
        Dim intActivityTime As String = ""
        If (checkValue(calltime)) Then
            intActivityTime = calltime
        Else
            intActivityTime = calculateActivityTime(UserID)
        End If
        If (Len(AppID) < 7) Then ' Business calls
            Dim strQry As String = "INSERT INTO tblbusinessobjectcallhistory(BusinessObjectID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryDialerOutcome, CallHistoryAgentOutcome, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("IDLE", "U", "IDLE") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField("", "U", "NULL") & ", " & _
            formatField("", "U", "NULL") & ")"
            executeNonQuery(strQry)
        Else
            Dim strQry As String = "INSERT INTO tblcallhistory(AppID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryDialerOutcome, CallHistoryAgentOutcome, CallHistoryRedial, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("IDLE", "U", "IDLE") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField(boolRedial, "B", 1) & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField("", "U", "NULL") & ", " & _
            formatField("", "U", "NULL") & ")"
            executeNonQuery(strQry)
            calcTimeToFirstCall(AppID)
            incrementField("AppID", AppID, "CallTime", "tblapplicationstatus", intActivityTime)
        End If
    End Sub

    Shared Sub insertInCallHistory(ByVal AppID As String, ByVal StatusCode As String, ByVal SubStatusCode As String, ByVal UserID As String, Optional ByVal calltime As String = "")
        Dim strTelephoneNumber As String = getAnyField("UserActiveWorkflowTelephoneNumber", "tblusers", "UserID", UserID)
        If (Len(AppID) < 7) Then ' Business calls
            Dim strSQL As String = "SELECT TOP 1 CallHistoryStatus FROM vwbusinesscallhistory WHERE BusinessObjectID = @BusinessObjectID AND CompanyID = @CompanyID AND CallHistoryDialerEvent = 'IN CALL' ORDER BY CallHistoryDate DESC"
            Dim arrParams As SqlParameter() = {New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "BusinessObjectID", DataRowVersion.Current, AppID), _
                                               New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, True, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
            Dim strLastStatusCode As String = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams).returnCacheString()
            StatusCode = getAnyField("BusinessObjectStatusCode", "tblbusinessobjectstatus", "BusinessObjectID", AppID)
            SubStatusCode = getAnyField("BusinessObjectSubStatusCode", "tblbusinessobjectstatus", "BusinessObjectID", AppID)
            If (StatusCode <> "NEW") Then
                If (strLastStatusCode = StatusCode) Then
                    StatusCode = ""
                End If
            End If
            Dim boolRedial As Boolean = True
            If (SubStatusCode = "WRN") Then boolRedial = False
            Dim strListName As String = getAnyField("WorkflowReference", "tblworkflows", "WorkflowID", getAnyField("UserActiveWorkflowID", "tblusers", "UserID", UserID))
            Dim strJobName As String = getAnyField("WorkflowType", "tblworkflowtypes", "WorkflowTypeID", getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", UserID))
            Dim intActivityTime As String = ""
            If (checkValue(calltime)) Then
                intActivityTime = calltime
            Else
                intActivityTime = calculateActivityTime(UserID)
            End If
            Dim strAccountNumber As String = "", strPassword As String = "", strSessionID As String = ""
            If (strTelephoneNumber <> "No Customer Call") Then
                strAccountNumber = getAnyFieldFromUserStore("ClickToDialAccountNumber", UserID)
                strPassword = getAnyFieldFromUserStore("ClickToDialPassword", UserID)
                strSessionID = getAnyFieldFromUserStore("ClickToDialSessionID", UserID)
            End If
            Dim strQry = "INSERT INTO tblbusinessobjectcallhistory(BusinessObjectID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus, CallHistoryAccountNumber, CallHistoryPassword, CallHistorySessionID) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("IN CALL", "U", "IN CALL") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField(strTelephoneNumber, "", "NULL") & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField(StatusCode, "U", "NULL") & ", " & _
            formatField(SubStatusCode, "U", "NULL") & ", " & _
            formatField(strAccountNumber, "", "") & ", " & _
            formatField(strPassword, "", "") & ", " & _
            formatField(strSessionID, "", "") & ")"
            executeNonQuery(strQry)
            If (SubStatusCode <> "CAN" And strTelephoneNumber <> "No Customer Call" And strTelephoneNumber <> "") Then
                incrementField("BusinessObjectID", AppID, "BusinessObjectDialAttempts", "tblbusinessobjectstatus", 1)
                CommonSave.saveBusinessUpdatedDate(AppID, Config.DefaultUserID, "LastDialed")
            End If
        Else
            Dim strSQL As String = "SELECT TOP 1 CallHistoryStatus FROM vwcallhistory WHERE AppID = @AppID AND CompanyID = @CompanyID AND CallHistoryDialerEvent = 'IN CALL' ORDER BY CallHistoryDate DESC"
            Dim arrParams As SqlParameter() = {New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                               New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, True, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
            Dim strLastStatusCode As String = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams).returnCacheString()
            StatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
            SubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
            If (StatusCode <> "NEW") Then
                If (strLastStatusCode = StatusCode) Then
                    StatusCode = ""
                End If
            End If
            Dim boolRedial As Boolean = True
            If (SubStatusCode = "WRN") Then boolRedial = False
            Dim strListName As String = getAnyField("WorkflowReference", "tblworkflows", "WorkflowID", getAnyField("UserActiveWorkflowID", "tblusers", "UserID", UserID))
            Dim strJobName As String = getAnyField("WorkflowType", "tblworkflowtypes", "WorkflowTypeID", getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", UserID))
            Dim intActivityTime As String = ""
            If (checkValue(calltime)) Then
                intActivityTime = calltime
            Else
                intActivityTime = calculateActivityTime(UserID)
            End If
            Dim strAccountNumber As String = "", strPassword As String = "", strSessionID As String = ""
            If (strTelephoneNumber <> "No Customer Call") Then
                strAccountNumber = getAnyFieldFromUserStore("ClickToDialAccountNumber", UserID)
                strPassword = getAnyFieldFromUserStore("ClickToDialPassword", UserID)
                strSessionID = getAnyFieldFromUserStore("ClickToDialSessionID", UserID)
            End If
            Dim strQry = "INSERT INTO tblcallhistory(AppID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryRedial, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus, CallHistoryAccountNumber, CallHistoryPassword, CallHistorySessionID) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("IN CALL", "U", "IN CALL") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField(strTelephoneNumber, "", "NULL") & ", " & _
            formatField(boolRedial, "B", 1) & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField(StatusCode, "U", "NULL") & ", " & _
            formatField(SubStatusCode, "U", "NULL") & ", " & _
            formatField(strAccountNumber, "", "") & ", " & _
            formatField(strPassword, "", "") & ", " & _
            formatField(strSessionID, "", "") & ")"
            executeNonQuery(strQry)
            If (SubStatusCode <> "CAN" And strTelephoneNumber <> "No Customer Call" And strTelephoneNumber <> "") Then
                incrementField("AppID", AppID, "DialAttempts", "tblapplicationstatus", 1)
                CommonSave.saveUpdatedDate(AppID, Config.DefaultUserID, "LastDialed")
            End If
            incrementField("AppID", AppID, "CallTime", "tblapplicationstatus", intActivityTime)
        End If
        updateUserStoreField(UserID, "ClickToDialSessionID", "", "", "")
    End Sub

    Shared Sub insertWrapHistory(ByVal AppID As String, ByVal UserID As String, Optional ByVal calltime As String = "")
        Dim strAgentOutcome As String = ""
        Dim boolRedial As Boolean = True
        Dim strListName As String = getAnyField("WorkflowReference", "tblworkflows", "WorkflowID", getAnyField("UserActiveWorkflowID", "tblusers", "UserID", UserID))
        Dim strJobName As String = getAnyField("WorkflowType", "tblworkflowtypes", "WorkflowTypeID", getAnyField("UserActiveWorkflowTypeID", "tblusers", "UserID", UserID))
        Dim intActivityTime As String = ""
        If (checkValue(calltime)) Then
            intActivityTime = calltime
        Else
            intActivityTime = calculateActivityTime(UserID)
        End If
        If (Len(AppID) < 7) Then ' Business calls
            Dim strQry As String = "INSERT INTO tblbusinessobjectcallhistory(BusinessObjectID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryDialerOutcome, CallHistoryAgentOutcome, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("WRAP", "U", "WRAP") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField(strAgentOutcome, "", "NULL") & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField("", "U", "NULL") & ", " & _
            formatField("", "U", "NULL") & ")"
            executeNonQuery(strQry)
        Else
            Dim strQry As String = "INSERT INTO tblcallhistory(AppID, CompanyID, CallHistoryDate, CallHistoryUserID, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryDialerOutcome, CallHistoryAgentOutcome, CallHistoryRedial, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus) " & _
            "VALUES(" & formatField(AppID, "N", 0) & ", " & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
            formatField(UserID, "N", 0) & ", " & _
            formatField("WRAP", "U", "WRAP") & ", " & _
            formatField(intActivityTime, "N", 0) & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField("", "", "NULL") & ", " & _
            formatField(strAgentOutcome, "", "NULL") & ", " & _
            formatField(boolRedial, "B", 1) & ", " & _
            formatField(strListName, "", "NULL") & ", " & _
            formatField(strJobName, "", "NULL") & ", " & _
            formatField("", "U", "NULL") & ", " & _
            formatField("", "U", "NULL") & ")"
            executeNonQuery(strQry)
            incrementField("AppID", AppID, "CallTime", "tblapplicationstatus", intActivityTime)
        End If
    End Sub

    Shared Sub insertNotReadyHistory(ByVal UserID As String)
        'Dim strUserDialerReference As String = getAnyField("UserDialerReference", "tblusers", "UserID", UserID)
        'Dim boolRedial As Boolean = True
        'Dim strListName As String = cookieValue("LeadPlatform", "WorkflowReference")
        'Dim strJobName As String = cookieValue("LeadPlatform", "WorkflowType")
        'Dim intActionTime As String = calculateNotReadyTime()
        'Dim strQry As String = "INSERT INTO tblcallhistory(AppID, CallHistoryDate, CallHistoryUserDialerReference, CallHistoryDialerEvent, CallHistoryDuration, CallHistoryTelephoneNumber, CallHistoryDialerOutcome, CallHistoryAgentOutcome, CallHistoryRedial, CallHistoryListName, CallHistoryJobName, CallHistoryStatus, CallHistorySubStatus) " & _
        '"VALUES(" & formatField(0, "N", 0) & ", " & _
        '        formatField(Config.DefaultDateTime, "DTTM", Config.DefaultDateTime) & ", " & _
        '        formatField(strUserDialerReference, "U", "NULL") & ", " & _
        '        formatField("IDLE", "U", "IDLE") & ", " & _
        '        formatField(intActionTime, "N", 0) & ", " & _
        '        formatField("", "", "NULL") & ", " & _
        '        formatField("", "", "NULL") & ", " & _
        '        formatField("", "", "NULL") & ", " & _
        '        formatField(boolRedial, "B", 1) & ", " & _
        '        formatField(strListName, "", "NULL") & ", " & _
        '        formatField(strJobName, "", "NULL") & ", " & _
        '        formatField("", "U", "NULL") & ", " & _
        '        formatField("", "U", "NULL") & ")"
        'executeNonQuery(strQry)
    End Sub

    Shared Function calculateActivityTime(ByVal UserID As String) As Integer
        Dim dteActivityTime As String = getAnyField("UserActiveWorkflowStartTime", "tblusers", "UserID", UserID)
        If (IsDate(dteActivityTime) And IsDate(dteActivityTime)) Then
            Return DateDiff(DateInterval.Second, CDate(dteActivityTime), Config.DefaultDateTime)
        Else
            Return 0
        End If
    End Function

    Shared Function calculatePreCallActivityTime(ByVal UserID As String) As Integer
        Dim dteActivityTime As String = getAnyField("UserActiveWorkflowStartTime", "tblusers", "UserID", UserID)
        Dim dtePreCallStartTime As String = getAnyField("UserPreCallStartTime", "tblusers", "UserID", UserID)
        If (checkValue(dtePreCallStartTime)) Then dteActivityTime = dtePreCallStartTime
        If (IsDate(dteActivityTime) And IsDate(dteActivityTime)) Then
            Return DateDiff(DateInterval.Second, CDate(dteActivityTime), Config.DefaultDateTime)
        Else
            Return 0
        End If
    End Function

End Class
