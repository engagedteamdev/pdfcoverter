﻿Imports Config, Common, CallInterface
Imports Microsoft.VisualBasic

Public Class Toolbar

    Private objLeadPlatform As LeadPlatform = Nothing

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Function drawCallTools(ByVal AppID As String, ByVal strTelephoneNumber As String, ByVal ApplicationPageOrder As String) As String
        Dim intWorkflowActivityID As String = "", intWorkflowID As String = "", boolCallRecording As Boolean = False
        Dim strSQL As String = "SELECT UserActiveWorkflowActivityID, UserActiveWorkflowID, UserActiveWorkflowCallRecording FROM tblusers WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim ds As DataTable = objDataSet.Tables("tblusers")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                intWorkflowActivityID = Row.Item("UserActiveWorkflowActivityID")
                intWorkflowID = Row.Item("UserActiveWorkflowID")
                boolCallRecording = Row.Item("UserActiveWorkflowCallRecording")
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<div id=""callToolsContainer"">")
            .WriteLine("<div id=""callToolsOutline"">")
            .WriteLine("<div id=""callToolsButton"" onclick=""showCallTools()"" title=""Click to open""><span class=""callToolsButton"">Call Tools</span></div>")
            .WriteLine("</div>")
            .WriteLine("<div id=""callTools"">")
            .WriteLine("<div id=""callToolsBottom"">")
            .WriteLine("<div id=""callToolsTop"">")
            .WriteLine("<div class=""callToolsContent"">")
            '.WriteLine("<div class=""floatRight"">")
            '.WriteLine("<a href=""#"" onclick=""hideCallTools();""><img src=""/net/images/icons/close.gif"" width=""14"" height=""14"" title=""Click to close"" border=""0"" /></a>")
            '.WriteLine("</div>")
            .WriteLine("<div class=""medc"">")
            If (intWorkflowActivityID = ActivityType.StartCall) Then
                .WriteLine("<span class=""lrgc"">")
                .WriteLine(activeTelephoneNumber(AppID, strTelephoneNumber))
                .WriteLine("</span>")
                .WriteLine(callTime(AppID, intWorkflowActivityID))
                .WriteLine(endCall(AppID, ApplicationPageOrder))
                .WriteLine(statusButtons(AppID, ApplicationPageOrder))
                If (strTelephoneNumber <> "NoCallMade") Then
                    If (Not boolCallRecording) Then
                        .WriteLine(startCallRecording())
                    End If
                End If
            Else
                If (checkValue(checkCaseLock(Config.DefaultUserID, AppID))) Then
                    .WriteLine(caseUnavail("Case locked"))
                    .WriteLine("<div class=""callToolsExit""><input type=""button"" value=""Exit Workflow"" class=""exitButton"" onclick=""document.location.href='/net/workflow/nextworkflow.aspx?frmAppID=" & AppID & "&frmWorkflowID=" & intWorkflowID & "&frmAction=" & ActivityType.EndWorkflow & "'"" /></div>")
                ElseIf (intWorkflowID = 0) Then
                    .WriteLine(caseUnavail("Call unavailable"))
                    .WriteLine("<div class=""callToolsExit""><input type=""button"" value=""Exit Workflow"" class=""exitButton"" onclick=""document.location.href='/net/workflow/nextworkflow.aspx?frmAppID=" & AppID & "&frmWorkflowID=" & intWorkflowID & "&frmAction=" & ActivityType.EndWorkflow & "'"" /></div>")
                Else
                    .WriteLine("<span class=""lrgc"">")
                    .WriteLine(telephoneNumbers(AppID, ApplicationPageOrder))
                    .WriteLine("</span>")
                    .WriteLine(callTime(AppID, intWorkflowActivityID))
                    If (boolCallRecording) Then
                        .WriteLine(endCallRecording(AppID))
                    End If
                    If (intWorkflowActivityID = ActivityType.StartCase Or intWorkflowActivityID = ActivityType.EndCall) Then
                        .WriteLine("<div class=""callToolsExit""><input type=""button"" value=""Exit Workflow"" class=""exitButton"" onclick=""document.location.href='/net/workflow/nextworkflow.aspx?frmAppID=" & AppID & "&frmWorkflowID=" & intWorkflowID & "&frmAction=" & ActivityType.EndWorkflow & "'"" /></div>")
                    End If
                    If (intWorkflowActivityID = ActivityType.EndCall) Then
                        .WriteLine("<div class=""callToolsNext""><input type=""button"" value=""Next Call"" class=""button"" onclick=""document.location.href='/net/workflow/nextworkflow.aspx?frmAppID=" & AppID & "&frmWorkflowID=" & intWorkflowID & "&frmAction=" & ActivityType.EndCase & "'"" /></div>")
                    End If
                End If
            End If
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
        End With
        Return objStringWriter.ToString
    End Function

    Private Function caseUnavail(ByVal msg As String) As String
        Return "<span class=""floatRight""><img src=""/net/images/icons/locked.png"" width=""24"" height=""24"" /></span><span class=""lrgc red"">" & msg & "</span>"
    End Function

    Private Function telephoneNumbers(ByVal AppID As String, ByVal ApplicationPageOrder As String) As String
        Dim strLink As String = "", strTelephoneNumbers As String = "", strStatusCode As String = "", strDialableStatus As String = ""
        Dim App1HomeTelephone As String = "", App1MobileTelephone As String = "", App1WorkTelephone As String = ""
        If (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "hotkey.aspx") > 0) Then
            strLink = "<a href=""/net/webservices/outbound/hotkey.aspx?AppID=" & AppID
            'ElseIf (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "summary.aspx") > 0) Then
            '    strLink = "<a href=""/net/application/summary.aspx?AppID=" & AppID
        ElseIf (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "newapplication.aspx") > 0) Then
            strLink = "<a href=""/net/application/newapplication.aspx?AppID=" & AppID
        Else
            strLink = "<a href=""/net/application/applicationform.aspx?AppID=" & AppID & "&Page=" & ApplicationPageOrder
        End If

        Dim strSQL As String = "SELECT StatusCode, DialableStatus FROM tblapplicationstatus WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblapplicationstatus")
        Dim ds As DataTable = objDataSet.Tables("tblapplicationstatus")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                strStatusCode = Row.Item("StatusCode")
                strDialableStatus = Row.Item("DialableStatus")
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing

        strSQL = "SELECT App1HomeTelephone, App1MobileTelephone, App1WorkTelephone FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        objDataSet = objDatabase.executeReader(strSQL, "vwexportapplication")
        ds = objDataSet.Tables("vwexportapplication")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                App1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                App1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                App1WorkTelephone = Row.Item("App1WorkTelephone").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        Dim strDisabled As String = ""
        If (checkValue(App1HomeTelephone)) Then
            If (Mid(strDialableStatus, 1, 1) = "0" Or strStatusCode <> "NEW") Then
                strDisabled = " disabled=""disabled"""
            Else
                strDisabled = ""
            End If
            strTelephoneNumbers += "<br /><img src=""/net/images/icons/phone_home.png"" width=""16"" height=""16"" title=""Home Telephone"" />&nbsp;" & strLink & "&TelephoneNumber=" & App1HomeTelephone & "&Action=" & ActivityType.StartCall & """ title=""Click to start call""" & strDisabled & ">" & writeTelephoneNumber(App1HomeTelephone) & "</a>"
        End If
        If (checkValue(App1MobileTelephone)) Then
            If (Mid(strDialableStatus, 2, 1) = "0" Or strStatusCode <> "NEW") Then
                strDisabled = " disabled=""disabled"""
            Else
                strDisabled = ""
            End If
            strTelephoneNumbers += "<br /><img src=""/net/images/icons/phone_mobile.png"" width=""16"" height=""16"" title=""Mobile Telephone"" />&nbsp;" & strLink & "&TelephoneNumber=" & App1MobileTelephone & "&Action=" & ActivityType.StartCall & """ title=""Click to start call""" & strDisabled & ">" & writeTelephoneNumber(App1MobileTelephone) & "</a>"
        End If
        If (checkValue(App1WorkTelephone)) Then
            If (Mid(strDialableStatus, 3, 1) = "0" Or strStatusCode <> "NEW") Then
                strDisabled = " disabled=""disabled"""
            Else
                strDisabled = ""
            End If
            strTelephoneNumbers += "<br /><img src=""/net/images/icons/phone_work.png"" width=""16"" height=""16"" title=""Work Telephone"" />&nbsp;" & strLink & "&TelephoneNumber=" & App1WorkTelephone & "&Action=" & ActivityType.StartCall & """ title=""Click to start call""" & strDisabled & ">" & writeTelephoneNumber(App1WorkTelephone) & "</a>"
        End If
        If (LeadPlatform.Config.Reports) Then
            strTelephoneNumbers += "<br /><img src=""/net/images/icons/phone_none.png"" width=""16"" height=""16"" title=""No Call"" />&nbsp;" & strLink & "&TelephoneNumber=NoCallMade&Action=" & ActivityType.StartCall & """ title=""Click to start call"">No customer call</a>"
        End If
        Return strTelephoneNumbers
    End Function

    Private Function activeTelephoneNumber(ByVal AppID As String, ByVal strTelephoneNumber As String) As String
        Dim App1HomeTelephone As String = "", App1MobileTelephone As String = "", App1WorkTelephone As String = ""
        Dim strSQL As String = "SELECT App1HomeTelephone, App1MobileTelephone, App1WorkTelephone FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "vwexportapplication")
        Dim ds As DataTable = objDataSet.Tables("vwexportapplication")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                App1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                App1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                App1WorkTelephone = Row.Item("App1WorkTelephone").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        'Dim strDialableStatus As String = getAnyField("DialableStatus", "tblapplicationstatus", "AppID", AppID)
        Dim strClass As String = ""
        If (strTelephoneNumber = App1HomeTelephone) Then
            'If (Mid(strDialableStatus, 1, 1) = "0") Then
            'strClass = "grey"
            'Else
            strClass = "red"
            'End If
            Return "<br /><img src=""/net/images/icons/phone_home.png"" width=""16"" height=""16"" title=""Home Telephone"" />&nbsp;<span class=""" & strClass & """>" & writeTelephoneNumber(App1HomeTelephone) & "</span>"
        ElseIf (strTelephoneNumber = App1MobileTelephone) Then
            'If (Mid(strDialableStatus, 2, 1) = "0") Then
            'strClass = "grey"
            'Else
            strClass = "red"
            'End If
            Return "<br /><img src=""/net/images/icons/phone_mobile.png"" width=""16"" height=""16"" title=""Mobile Telephone"" />&nbsp;<span class=""" & strClass & """>" & writeTelephoneNumber(App1MobileTelephone) & "</span>"
        ElseIf (strTelephoneNumber = App1WorkTelephone) Then
            'If (Mid(strDialableStatus, 2, 1) = "0") Then
            'strClass = "grey"
            'Else
            strClass = "red"
            'End If
            Return "<br /><img src=""/net/images/icons/phone_work.png"" width=""16"" height=""16"" title=""Work Telephone"" />&nbsp;<span class=""" & strClass & """>" & writeTelephoneNumber(App1WorkTelephone) & "</span>"
        ElseIf (strTelephoneNumber = "NoCallMade" And LeadPlatform.Config.Reports) Then
            Return "<br /><img src=""/net/images/icons/phone_none.png"" width=""16"" height=""16"" title=""No Call"" />&nbsp;<span class=""red"">No customer call</span>"
        ElseIf (strTelephoneNumber = "InboundCall") Then
            Return "<br /><img src=""/net/images/icons/phone_inbound.gif"" width=""16"" height=""16"" title=""Inbound Call"" />&nbsp;<span class=""red"">Inbound Call</span>"
        Else
            Return ""
        End If
    End Function

    Private Function writeTelephoneNumber(ByVal tel As String) As String
        Dim arrTelephone As Array = splitTelephone(tel)
        Return arrTelephone(0) & " " & arrTelephone(1)
    End Function

    Private Function callTime(ByVal AppID As String, ByVal intWorkflowActivityID As String) As String
        Dim dteCallStartTime As String = "", dtePreCallStartTime As String = ""
        Dim strSQL As String = "SELECT UserActiveWorkflowStartTime, UserPreCallStartTime FROM tblusers WHERE UserID = '" & LeadPlatform.Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim ds As DataTable = objDataSet.Tables("tblusers")
        If (ds.Rows.Count > 0) Then
            For Each Row As DataRow In ds.Rows
                dteCallStartTime = Row.Item("UserActiveWorkflowStartTime").ToString
                dtePreCallStartTime = Row.Item("UserPreCallStartTime").ToString
            Next
        End If
        ds.Clear()
        ds = Nothing
        objDataSet = Nothing
        objDatabase = Nothing

        If (checkValue(dtePreCallStartTime)) Then dteCallStartTime = dtePreCallStartTime
        Dim strClass As String = "stopwatch"
        If (intWorkflowActivityID = ActivityType.StartCase Or intWorkflowActivityID = ActivityType.EndCall) Then
            strClass = "stopwatchSmall"
        End If
        Dim strCallTime As String = _
            "<br /><span class=""small"">" & callPart(intWorkflowActivityID) & "</span>" & vbCrLf & _
            "<div id=""stopWatch"" class=""" & strClass & """>" & vbCrLf
        If (IsDate(dteCallStartTime) And IsDate(dteCallStartTime)) Then
            strCallTime += _
                "<script type=""text/javascript"">" & vbCrLf & _
              "stopWatch(" & AppID & "," & DateDiff("s", CDate(dteCallStartTime), Config.DefaultDateTime) * 1000 & ")" & vbCrLf & _
                "</script>" & vbCrLf
        End If
        strCallTime += "</div>"
        Return strCallTime
    End Function

    Private Function callPart(ByVal intWorkflowActivityID As String) As String
        Dim strCallPart As String = ""
        If (intWorkflowActivityID = ActivityType.StartCall) Then
            strCallPart = "IN CALL"
        ElseIf (intWorkflowActivityID = ActivityType.StartCase) Then
            strCallPart = "IDLE"
        ElseIf (intWorkflowActivityID = ActivityType.EndCall) Then
            strCallPart = "WRAP"
        End If
        Return strCallPart
    End Function

    Private Function endCall(ByVal AppID As String, ByVal ApplicationPageOrder As String) As String
        Dim strLink As String = ""
        'strLink = "<br /><a href=""#"" onclick=""jConfirm('Are you sure you want to CANCEL the current call?', 'Cancel Call', 'Yes', 'No', function(r) { if (r) { window.location = '" & getEndCallURL(AppID, ApplicationPageOrder) & "' } })"" title=""Click to cancel call""><span class=""smrc"">Cancel Call</span></a>"
        strLink = "<br /><a href=""" & getEndCallURL(AppID, ApplicationPageOrder) & """ onClick=""if (confirm('Are you sure you want to CANCEL the current call?')){return true;} else {return false;}"" title=""Click to cancel call""><span class=""smrc"">Cancel Call</span></a>"
        Return strLink
    End Function

    Private Function getEndCallURL(ByVal AppID As String, ByVal ApplicationPageOrder As String) As String
        If (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "hotkey.aspx") > 0) Then
            Return "/net/webservices/outbound/hotkey.aspx?AppID=" & AppID & "&Action=" & ActivityType.EndCall
            'ElseIf (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "summary.aspx") > 0) Then
            '    Return "/net/application/summary.aspx?AppID=" & AppID & "&Action=" & ActivityType.EndCall
        ElseIf (InStr(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "newapplication.aspx") > 0) Then
            Return "/net/application/newapplication.aspx?AppID=" & AppID & "&Action=" & ActivityType.EndCall
        Else
            Return "/net/application/applicationform.aspx?AppID=" & AppID & "&Page=" & ApplicationPageOrder & "&Action=" & ActivityType.EndCall
        End If
    End Function

    Private Function statusButtons(ByVal AppID As String, ByVal ApplicationPageOrder As String) As String
        Dim strStatusButtons As String = _
            writeStatusButton(AppID, "Busy / Engaged", "", "", "ENG", ApplicationPageOrder, "") & vbCrLf & _
            writeStatusButton(AppID, "Message Left", "", "", "MSG", ApplicationPageOrder, "") & vbCrLf & _
            writeStatusButton(AppID, "Answerphone", "", "", "ANS", ApplicationPageOrder, "") & vbCrLf & _
            writeStatusButton(AppID, "No Answer", "", "", "NOA", ApplicationPageOrder, "") & vbCrLf & _
            writeStatusButton(AppID, "Wrong Number", "", "", "WRN", ApplicationPageOrder, "") & vbCrLf & _
            writeStatusButton(AppID, "Invalid", "", "INV", "", ApplicationPageOrder, "Prompt") & vbCrLf & _
            writeStatusButton(AppID, "We Turned Down", "", "WTD", "", ApplicationPageOrder, "Prompt") & vbCrLf & _
            writeStatusButton(AppID, "Turned Us Down", "", "TUD", "", ApplicationPageOrder, "Prompt") & vbCrLf & _
            writeStatusButton(AppID, "Call Back", "", "", "CB", ApplicationPageOrder, "Prompt")
        Return strStatusButtons
    End Function

    Private Function writeStatusButton(ByVal AppID As String, ByVal nm As String, ByVal strDateType As String, ByVal StatusCode As String, ByVal SubStatusCode As String, ByVal ApplicationPageOrder As String, ByVal strButtonType As String) As String
        Dim strStatusButton As String = ""
        strStatusButton = "<div class=""statusButton"">"
        If (strButtonType = "Prompt") Then
            strStatusButton += "<a href=""#promptContainer" & StatusCode & SubStatusCode & """ id=""prompt" & StatusCode & SubStatusCode & """ onclick=""displayPrompt('" & nm & "',this.id,'promptContainer" & StatusCode & SubStatusCode & "','/net/prompts/endcall.aspx?AppID=" & AppID & "&HotkeyUserID=" & LeadPlatform.Config.DefaultUserID & "&DateType=" & strDateType & "&StatusCode=" & StatusCode + "&SubStatusCode=" & SubStatusCode & "&intActionType=100&strReturnURL=" & encodeURL(getEndCallURL(AppID, ApplicationPageOrder)) & "',500,310);"">" & nm & "</a><span id=""promptContainer" & StatusCode & SubStatusCode & """></span>"
        Else
            strStatusButton += "<a href=""/net/webservices/outbound/endhotkey.aspx?AppID=" & AppID & "&HotkeyUserID=" & LeadPlatform.Config.DefaultUserID & "&DateType=" & strDateType & "&StatusCode=" & StatusCode + "&SubStatusCode=" & SubStatusCode & "&intActionType=100&strReturnURL=" & encodeURL(getEndCallURL(AppID, ApplicationPageOrder)) & """>" & nm & "</a>"
        End If
        strStatusButton += "</div>"
        Return strStatusButton
    End Function

    Private Function startCallRecording() As String
        updateAnyDatabaseField("UserID", Config.DefaultUserID, "tblusers", "UserActiveWorkflowCallRecording", "B", 1, 1)
        Dim strCallRecordingURL As String = "http://directsql/default.aspx?frmCallStart=Y"
        Return "" '"Start call recording"
        'Return "<iframe src=""" & strCallRecordingURL & """ frameborder=""0"" width=""0"" height=""0"" style=""display: none""></iframe>"
    End Function

    Private Function endCallRecording(ByVal AppID As String) As String
        updateAnyDatabaseField("UserID", Config.DefaultUserID, "tblusers", "UserActiveWorkflowCallRecording", "B", 0, 0)
        Dim strApp1HomeTelephone As String = "", strApp1MobileTelephone As String = "", strApp1FullName As String = "", strApp1DOB As String = ""

        Dim strSQL As String = "SELECT App1HomeTelephone, App1MobileTelephone, App1FullName, App1DOB FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim ds As DataSet = objDatabase.executeReader(strSQL, "vwexportapplication")
        Dim dsCallHistory = ds.Tables("vwexportapplication")
        If (dsCallHistory.Rows.Count > 0) Then
            For Each Row As DataRow In dsCallHistory.Rows
                strApp1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                strApp1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                strApp1FullName = Row.Item("App1FullName").ToString
                strApp1DOB = Row.Item("App1DOB").ToString
            Next
        End If
        dsCallHistory.Clear()
        dsCallHistory = Nothing
        ds = Nothing
        objDatabase = Nothing

        Dim strJobName As String = getAnyField("WorkflowType", "tblworkflowtypes", "WorkflowTypeID", LeadPlatform.Config.UserActiveWorkflowTypeID)

        Dim strCallRecordingURL As String = "http://directsql/default.aspx?frmCallEnd=Y&frmLDMID=" & AppID & "&frmStartTime=" & Config.DefaultDateTime & "&frmCRMID=" & _
            "&frmClient=First2Talk&frmUserID=" & LeadPlatform.Config.DefaultUserDialerReference & "&frmPhone1=" & strApp1HomeTelephone & "&frmPhone2=" & strApp1MobileTelephone & "&frmCustName=" & encodeURL(strApp1FullName) & "&frmCustDOB=" & strApp1DOB & _
            "&frmOutcome=&frmCampaign=" & encodeURL(strJobName) & "&frmCallID=" & getIndexNumber() & ""
        'Return "" 'strCallRecordingURL '"End call recording"
        'Return "<iframe src=""" & strCallRecordingURL & """ frameborder=""0"" width=""0"" height=""0"" style=""display: none""></iframe>"
        Return ""
    End Function

End Class
