﻿Imports Config, Common, CallInterface
Imports Microsoft.VisualBasic
Imports System.Reflection

Public Class Dashboard

    Private objLeadPlatform As LeadPlatform = Nothing, objCache As Cache
    Private DashboardID As String = HttpContext.Current.Request("DashboardID"), PanelID As String = HttpContext.Current.Request("PanelID")
    Private strJavascriptFunctions As String = "", strJavascriptFunctions2 As String = "", intPanelRefreshMS As String = "0", intStatisticType As String = "", boolWallBoard As Boolean = False, boolChangeMenu As Boolean = False

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property PanelRefreshMS() As String
        Get
            Return intPanelRefreshMS
        End Get
        Set(ByVal value As String)
            intPanelRefreshMS = value
        End Set
    End Property

    Public Property StatType() As String
        Get
            Return intStatisticType
        End Get
        Set(ByVal value As String)
            intStatisticType = value
        End Set
    End Property

    Public Property JavascriptFunctions() As String
        Get
            Return strJavascriptFunctions
        End Get
        Set(ByVal value As String)
            strJavascriptFunctions = value
        End Set
    End Property

    Public Property DashboardJavascriptFunctions() As String
        Get
            Return strJavascriptFunctions2
        End Get
        Set(ByVal value As String)
            strJavascriptFunctions2 = value
        End Set
    End Property
	
	Public Property WallBoard() As Boolean
        Get
            Return boolWallBoard
        End Get
        Set(ByVal value As Boolean)
            boolWallBoard = value
        End Set
    End Property

    Public Enum StatisticType
        Text = 1
        Report = 2
        Table = 3
        Chart = 4
        Timeline = 5
        SubRoutine = 6
        BigStat = 7
        BigStatExternal = 8
    End Enum

    Public Sub initialiseDashboard(ByVal wb As Boolean)
        WallBoard() = wb
        Dim strSQL As String = ""
        If (checkValue(DashboardID)) Then
            strSQL = "SELECT TOP 1 DashboardJavascriptFunctions FROM vwdashboardnew WHERE DashboardID = '" & DashboardID & "' AND CompanyID = '" & CompanyID & "'"
        Else
            strSQL = "SELECT TOP 1 DashboardJavascriptFunctions FROM vwdashboardnew WHERE DashboardID = '" & getAnyFieldFromUserStore("ActiveDashboardID", Config.DefaultUserID) & "' AND CompanyID = '" & CompanyID & "'"
        End If
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        Dim strDashboardJavaScriptFunctions As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strDashboardJavaScriptFunctions = Row.Item("DashboardJavascriptFunctions").ToString
                If (WallBoard()) Then
                    DashboardJavascriptFunctions() = Replace(strDashboardJavaScriptFunctions, "/net/dashboard/dashboard.aspx", "/dashboard/")
                Else
                    DashboardJavascriptFunctions() = Replace(strDashboardJavaScriptFunctions, "/net/dashboard/dashboard.aspx", "/")
                End If
            Next
        End If
        dsCache = Nothing
    End Sub

    Public Sub initialise()
        Dim strSQL As String = "SELECT TOP 1 StatisticID, StatisticName, StatisticTitle, StatisticType, StatisticSubTitle, StatisticJavascriptFunctions, StatisticReportBuilderID, StatisticReportBuilderQueryString, StatisticCachedMinutes, DashboardPanelRefreshMS, StatisticTargetValue, StatisticMinValue, StatisticMaxValue FROM vwdashboardpanel WHERE DashboardPanelID = '" & PanelID & "' AND CompanyID = '" & CompanyID & "'"
        Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
        Dim strStatisticJavascriptFunctions As String = "", strReportBuilderQueryString As String = ""
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strStatisticJavascriptFunctions += Row.Item("StatisticJavascriptFunctions").ToString
                strReportBuilderQueryString = menuTags(Row.Item("StatisticReportBuilderQueryString").ToString)
                strReportBuilderQueryString = Replace(strReportBuilderQueryString, "{UserID}", Config.DefaultUserID)
                PanelRefreshMS() = Row.Item("DashboardPanelRefreshMS")
                StatType() = Row.Item("StatisticType")
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticID}", Row.Item("StatisticID"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticName}", Row.Item("StatisticName"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticTitle}", Row.Item("StatisticTitle"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticSubTitle}", Row.Item("StatisticSubTitle"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{ReportBuilderID}", Row.Item("StatisticReportBuilderID"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{CachedMinutes}", Row.Item("StatisticCachedMinutes"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{ReportBuilderQueryString}", strReportBuilderQueryString)
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticTargetValue}", Row.Item("StatisticTargetValue"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticMinValue}", Row.Item("StatisticMinValue"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "{StatisticMaxValue}", Row.Item("StatisticMaxValue"))
                strStatisticJavascriptFunctions = Replace(strStatisticJavascriptFunctions, "/net", "")
                JavascriptFunctions() += strStatisticJavascriptFunctions
            Next
        End If
        dsCache = Nothing
    End Sub

    Public Sub getDashboard()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = ""
            If (getAnyFieldFromUserStore("ActiveDashboardID", Config.DefaultUserID) = "0") Then
                getBusinessHub()
            Else
                If (checkValue(DashboardID)) Then
                    strSQL = "SELECT DashboardID, StatisticID, StatisticText, StatisticSQL, StatisticRoutine, DashboardName, DashboardPanelMappingCSSClass, DashboardPanelMappingDisplay, DashboardPanelMappingColumn, DashboardPanelID, DashboardPanelName, DashboardPanelIcon, DashboardPanelMappingHeight, StatisticType FROM vwdashboardnew WHERE DashboardID = '" & DashboardID & "' AND CompanyID = '" & CompanyID & "' ORDER BY DashboardPanelMappingOrder"
                Else
                    strSQL = "SELECT DashboardID, StatisticID, StatisticText, StatisticSQL, StatisticRoutine, DashboardName, DashboardPanelMappingCSSClass, DashboardPanelMappingDisplay, DashboardPanelMappingColumn, DashboardPanelID, DashboardPanelName, DashboardPanelIcon, DashboardPanelMappingHeight, StatisticType FROM vwdashboardnew WHERE DashboardID = '" & getAnyFieldFromUserStore("ActiveDashboardID", Config.DefaultUserID) & "' AND CompanyID = '" & CompanyID & "' ORDER BY DashboardPanelMappingOrder"
                End If
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = "", intLastID As String = ""
                    For Each Row In dsCache.Rows
                        'If (x = 0) Then
                        '.WriteLine("<div class=""smlc floatRight""><a href=""#"" onclick=""setDashboardPrompt();"">[Change]</a></div>")
                        'End If
                        Dim strHeight As String = ""
                        If (intLastID <> Row.Item("DashboardPanelID").ToString) Then
                            If (Row.Item("DashboardPanelMappingHeight") > 0) Then
                                strHeight = "height=""" & Row.Item("DashboardPanelMappingHeight") & """"
                            End If
                            If (Row.Item("DashboardPanelMappingCSSClass") <> strLastClass Or InStr(Row.Item("DashboardPanelMappingCSSClass"), "new ") > 0) Then
                                If (strLastClass <> "") Then
                                    .WriteLine("</div>") ' /span
                                End If
                                .WriteLine("<div class=""" & Row.Item("DashboardPanelMappingCSSClass") & """>")
                            End If
                            If (Row.Item("StatisticType") = StatisticType.BigStat) Then
                                .WriteLine(getBigStatValues(Row.Item("StatisticID"), Row.Item("StatisticText")))
                                .WriteLine("<script>setInterval(function() { reloadStats(" & Row.Item("StatisticID") & ") },30000);</script>")
                            ElseIf (Row.Item("StatisticType") = StatisticType.BigStatExternal) Then
                                .WriteLine(getBigStatExternalValues(Row.Item("StatisticID"), Row.Item("StatisticText"), Row.Item("StatisticRoutine").ToString))
                                .WriteLine("<script>setInterval(function() { reloadStatsExternal(" & Row.Item("StatisticID") & ",'" & Row.Item("StatisticRoutine").ToString & "') },30000);</script>")
                            Else
                                .WriteLine("<div class=""widget"" style=""display: " & Row.Item("DashboardPanelMappingDisplay").ToString & """>")
                                .WriteLine("<div class=""widget-header"">")
                                .WriteLine("<h3><i class=""" & Row.Item("DashboardPanelIcon") & """></i>" & Row.Item("DashboardPanelName") & "</h3>")
                                If (Not boolChangeMenu) Then
                                    .WriteLine("<div class=""widget-actions"">")
                                    .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-bar-chart-o""></i> <span class=""title"">" & Row.Item("DashboardName") & "</span> <span class=""caret""></span></button>")
                                    .WriteLine("<ul class=""dropdown-menu"">")
                                    .WriteLine(getAvailableDashboards())
                                    .WriteLine("</ul>")
                                    .WriteLine("</div>")
                                End If
                                .WriteLine("</div>")
                                .WriteLine("<div class=""widget-content"">")
                                If (Row.Item("DashboardPanelMappingDisplay").ToString = "block") Then
                                    .WriteLine("<iframe id=""panel" & Row.Item("DashboardPanelID") & """ name=""panel" & Row.Item("DashboardPanelID") & """ src=""/dashboard/panel.aspx?DashboardID=" & Row.Item("DashboardID") & "&PanelID=" & Row.Item("DashboardPanelID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                                End If
                                .WriteLine("</div>") ' /widget content
                                .WriteLine("</div>") ' /widget
                            End If
                        End If
                        intLastID = Row.Item("DashboardPanelID")
                        strLastClass = Replace(Row.Item("DashboardPanelMappingCSSClass"), "new ", "")
                        x += 1
                    Next
                Else
                    strSQL = "SELECT COUNT(*) FROM tbldashboardmappings WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
                    Dim strCount As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
                    If (CInt(strCount) > 0) Then
                        strSQL = "SELECT DashboardID, StatisticID, StatisticText, StatisticSQL, StatisticRoutine, DashboardName, DashboardPanelMappingCSSClass, DashboardPanelMappingDisplay, DashboardPanelMappingColumn, DashboardPanelID, DashboardPanelName, DashboardPanelIcon, DashboardPanelMappingHeight, StatisticType FROM vwdashboardnew WHERE DashboardID = (SELECT TOP 1 DashboardID FROM tbldashboardmappings WHERE UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "') AND CompanyID = '" & CompanyID & "' ORDER BY DashboardPanelMappingOrder"
                        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            Dim x As Integer = 0, strLastClass As String = "", intLastID As String = ""
                            For Each Row In dsCache.Rows
                                Dim strHeight As String = ""
                                If (intLastID <> Row.Item("DashboardPanelID").ToString) Then
                                    If (Row.Item("DashboardPanelMappingHeight") > 0) Then
                                        strHeight = "height=""" & Row.Item("DashboardPanelMappingHeight") & """"
                                    End If
                                    If (Row.Item("DashboardPanelMappingCSSClass") <> strLastClass Or InStr(Row.Item("DashboardPanelMappingCSSClass"), "new ") > 0) Then
                                        If (strLastClass <> "") Then
                                            .WriteLine("</div>") ' /span
                                        End If
                                        .WriteLine("<div class=""" & Row.Item("DashboardPanelMappingCSSClass") & """>")
                                    End If
                                    If (Row.Item("StatisticType") = StatisticType.BigStat) Then
                                        .WriteLine(getBigStatValues(Row.Item("StatisticID"), Row.Item("StatisticText")))
                                        .WriteLine("<script>setInterval(function() { reloadStats(" & Row.Item("StatisticID") & ") },30000);</script>")
                                    ElseIf (Row.Item("StatisticType") = StatisticType.BigStatExternal) Then
                                        .WriteLine(getBigStatExternalValues(Row.Item("StatisticID"), Row.Item("StatisticText"), Row.Item("StatisticRoutine").ToString))
                                        .WriteLine("<script>setInterval(function() { reloadStatsExternal(" & Row.Item("StatisticID") & ",'" & Row.Item("StatisticRoutine").ToString & "') },30000);</script>")
                                    Else
                                        .WriteLine("<div class=""widget"" style=""display: " & Row.Item("DashboardPanelMappingDisplay").ToString & """>")
                                        .WriteLine("<div class=""widget-header"">")
                                        .WriteLine("<h3><i class=""" & Row.Item("DashboardPanelIcon") & """></i>" & Row.Item("DashboardPanelName") & "</h3>")
                                        If (Not boolChangeMenu) Then
                                            .WriteLine("<div class=""widget-actions"">")
                                            .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-bar-chart-o""></i> <span class=""title"">" & Row.Item("DashboardName") & "</span> <span class=""caret""></span></button>")
                                            .WriteLine("<ul class=""dropdown-menu"">")
                                            .WriteLine(getAvailableDashboards())
                                            .WriteLine("</ul>")
                                            .WriteLine("</div>")
                                        End If
                                        .WriteLine("</div>")
                                        .WriteLine("<div class=""widget-content"">")
                                        If (Row.Item("DashboardPanelMappingDisplay").ToString = "block") Then
                                            .WriteLine("<iframe id=""panel" & Row.Item("DashboardPanelID") & """ name=""panel" & Row.Item("DashboardPanelID") & """ src=""/dashboard/panel.aspx?DashboardID=" & Row.Item("DashboardID") & "&PanelID=" & Row.Item("DashboardPanelID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                                        End If
                                        .WriteLine("</div>") ' /widget content
                                        .WriteLine("</div>") ' /widget
                                    End If
                                End If
                                intLastID = Row.Item("DashboardPanelID")
                                strLastClass = Replace(Row.Item("DashboardPanelMappingCSSClass"), "new ", "")
                                x += 1
                            Next
                        End If
                        'ElseIf (CInt(strCount > 1)) Then
                        '    .WriteLine("<script type=""text/javascript"">")
                        '    .WriteLine("    $(document).ready(function() {")
                        '    .WriteLine("        setDashboardPrompt();")
                        '    .WriteLine("    });")
                        '    .WriteLine("</script>")
                    Else
                        strSQL = "SELECT DashboardID, StatisticID, StatisticText, StatisticSQL, StatisticRoutine, DashboardName, DashboardPanelMappingCSSClass, DashboardPanelMappingDisplay, DashboardPanelMappingColumn, DashboardPanelID, DashboardPanelName, DashboardPanelIcon, DashboardPanelMappingHeight, StatisticType FROM vwdashboardnew WHERE DashboardDefault = 1 AND CompanyID = '" & CompanyID & "' ORDER BY DashboardPanelMappingOrder"
                        dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            Dim x As Integer = 0, strLastClass As String = "", intLastID As String = 0
                            For Each Row In dsCache.Rows
                                Dim strHeight As String = ""
                                If (intLastID <> Row.Item("DashboardPanelID")) Then
                                    If (Row.Item("DashboardPanelMappingHeight") > 0) Then
                                        strHeight = "height=""" & Row.Item("DashboardPanelMappingHeight") & """"
                                    End If
                                    If (Row.Item("DashboardPanelMappingCSSClass") <> strLastClass Or InStr(Row.Item("DashboardPanelMappingCSSClass"), "new ") > 0) Then
                                        If (strLastClass <> "") Then
                                            .WriteLine("</div>") ' /span
                                        End If
                                        .WriteLine("<div class=""" & Row.Item("DashboardPanelMappingCSSClass") & """>")
                                    End If
                                    If (Row.Item("StatisticType") = StatisticType.BigStat) Then
                                        .WriteLine(getBigStatValues(Row.Item("StatisticID"), Row.Item("StatisticText")))
                                        .WriteLine("<script>setInterval(function() { reloadStats(" & Row.Item("StatisticID") & ") },30000);</script>")
                                    ElseIf (Row.Item("StatisticType") = StatisticType.BigStatExternal) Then
                                        .WriteLine(getBigStatExternalValues(Row.Item("StatisticID"), Row.Item("StatisticText"), Row.Item("StatisticRoutine").ToString))
                                        .WriteLine("<script>setInterval(function() { reloadStatsExternal(" & Row.Item("StatisticID") & ",'" & Row.Item("StatisticRoutine").ToString & "') },30000);</script>")
                                    Else
                                        .WriteLine("<div class=""widget"" style=""display: " & Row.Item("DashboardPanelMappingDisplay").ToString & """>")
                                        .WriteLine("<div class=""widget-header"">")
                                        .WriteLine("<h3><i class=""" & Row.Item("DashboardPanelIcon") & """></i>" & Row.Item("DashboardPanelName") & "</h3>")
                                        If (Not boolChangeMenu) Then
                                            .WriteLine("<div class=""widget-actions"">")
                                            .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-chart""></i> <span class=""title"">" & Row.Item("DashboardName") & "</span> <span class=""caret""></span></button>")
                                            .WriteLine("<ul class=""dropdown-menu"">")
                                            .WriteLine(getAvailableDashboards())
                                            .WriteLine("</ul>")
                                            .WriteLine("</div>")
                                        End If
                                        .WriteLine("</div>")
                                        .WriteLine("<div class=""widget-content"">")
                                        If (Row.Item("DashboardPanelMappingDisplay").ToString = "block") Then
                                            .WriteLine("<iframe id=""panel" & Row.Item("DashboardPanelID") & """ name=""panel" & Row.Item("DashboardPanelID") & """ src=""/dashboard/panel.aspx?DashboardID=" & Row.Item("DashboardID") & "&PanelID=" & Row.Item("DashboardPanelID") & """ width=""100%"" frameborder=""0"" " & strHeight & "></iframe>")
                                        End If
                                        .WriteLine("</div>") ' /widget content
                                        .WriteLine("</div>") ' /widget
                                    End If
                                End If
                                intLastID = Row.Item("DashboardPanelID")
                                strLastClass = Replace(Row.Item("DashboardPanelMappingCSSClass"), "new ", "")
                                x += 1
                            Next
                        Else
                            getBusinessHub()
                        End If
                    End If
                End If
                dsCache = Nothing
            End If
            HttpContext.Current.Response.Write(objStringWriter.ToString)
        End With
    End Sub

    Private Sub getBusinessHub()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<div class=""span4"">")
            ' Shortcuts
            .WriteLine("<div class=""widget"">")
            .WriteLine("<div class=""widget-header"">")
            .WriteLine("<h3><i class=""icon-bookmark""></i>Shortcuts</h3>")
            .WriteLine("<div class=""widget-actions"">")
            .WriteLine("<button class=""btn btn-small dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-bar-chart-o""></i> <span class=""title"">CRM Hub</span> <span class=""caret""></span></button>")
            .WriteLine("<ul class=""dropdown-menu"">")
            .WriteLine(getAvailableDashboards())
            .WriteLine("</ul>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("<div class=""widget-content"" style=""min-height: 200px"">")
            .WriteLine("<div class=""shortcuts"">")
            If (loggedInUserValue("UserReports") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<a href=""/reports.aspx"" class=""shortcut""><i class=""shortcut-icon icon-signal""></i><span class=""shortcut-label"">Reports</span></a>")
            End If
            If (loggedInUserValue("UserWorkflows") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<a href=""/workflow/workflow.aspx"" class=""shortcut""><i class=""shortcut-icon icon-random""></i><span class=""shortcut-label"">Workflows</span></a>")
            End If
            If (loggedInUserValue("UserWorkflows") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<a href=""/workflow/startworkflow.aspx"" class=""shortcut""><i class=""shortcut-icon icon-play""></i><span class=""shortcut-label"">Start Workflow</span></a>")
            End If
            '.WriteLine("<a href=""javascript:;"" class=""shortcut""><i class=""shortcut-icon icon-calendar""></i><span class=""shortcut-label"">Calendar</span></a>")
            '.WriteLine("<a href=""javascript:;"" class=""shortcut""><i class=""shortcut-icon icon-folder-open""></i><span class=""shortcut-label"">Documents</span></a>")
            '.WriteLine("<a href=""javascript:;"" class=""shortcut""><i class=""shortcut-icon icon-file""></i><span class=""shortcut-label"">Notes</span></a>")
            If (loggedInUserValue("UserStaffAdmin") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<a href=""/admin/usermaintenance.aspx"" class=""shortcut""><i class=""shortcut-icon icon-user""></i><span class=""shortcut-label"">Users</span></a>")
            End If
            If (loggedInUserValue("UserMediaAdmin") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<a href=""/admin/mediamaintenance.aspx"" class=""shortcut""><i class=""shortcut-icon icon-film""></i><span class=""shortcut-label"">Media</span></a>")
            End If
            If (loggedInUserValue("UserDataAdmin") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                '.WriteLine("<a href=""/admin/usermaintenance.aspx"" class=""shortcut""><i class=""shortcut-icon icon-hdd""></i><span class=""shortcut-label"">Data</span></a>")
            End If
            .WriteLine("</div><!-- /.shortcuts -->")
            .WriteLine("</div>")
            .WriteLine("</div>")
            ' Favourites
            .WriteLine("<div class=""widget nopad"">")
            .WriteLine("<div class=""widget-header"">")
            .WriteLine("<h3><i class=""icon-star""></i>Favourites</h3>")
            .WriteLine("</div>")
            .WriteLine("<div class=""widget-tabs"">")
            .WriteLine("<ul class=""nav nav-tabs"">")
            If (loggedInUserValue("UserReports") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<li><a href=""#reports""><span class=""badge badge-success"">" & getFavouritesCount(1) & "</span>&nbsp;&nbsp;&nbsp;Reports</a></li>")
            End If
            If (getUserAccessLevel(Config.DefaultUserID, "AccessBusinessObjects")) Then
                .WriteLine("<li><a href=""#business-reports""><span class=""badge badge-success"">" & getFavouritesCount(3) & "</span>&nbsp;&nbsp;&nbsp;Business</a></li>")
            End If
            .WriteLine("<li class=""active""><a href=""#workflows""><span class=""badge badge-success"">" & getFavouritesCount(2) & "</span>&nbsp;&nbsp;&nbsp;Workflows</a></li>")
            .WriteLine("</ul>")
            .WriteLine("</div> <!-- /.widget-tabs -->")
            .WriteLine("<div class=""widget-content"" style=""min-height: 200px"">")
            .WriteLine("<div class=""tab-content"">")
            If (loggedInUserValue("UserReports") = "True" Or loggedInUserValue("UserSuperAdmin") = "True") Then
                .WriteLine("<div class=""tab-pane"" id=""reports"">")
                .WriteLine("<table class=""table table-bordered table-striped table-small sortable"">")
                .WriteLine(getFavourites(1))
                .WriteLine("</table>")
                .WriteLine("</div>")
            End If
            If (getUserAccessLevel(Config.DefaultUserID, "AccessBusinessObjects")) Then
                .WriteLine("<div class=""tab-pane"" id=""business-reports"">")
                .WriteLine("<table class=""table table-bordered table-striped table-small sortable"">")
                .WriteLine(getFavourites(3))
                .WriteLine("</table>")
                .WriteLine("</div>")
            End If
            .WriteLine("<div class=""tab-pane active"" id=""workflows"">")
            .WriteLine("<table class=""table table-bordered table-striped table-small sortable"">")
            .WriteLine(getFavourites(2))
            .WriteLine("</table>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            .WriteLine("</div>")
            ' Quick stats
            '.WriteLine("<div class=""widget"">")
            '.WriteLine("<div class=""widget-header"">")
            '.WriteLine("<h3><i class=""icon-tasks""></i>Quick Stats</h3>")
            '.WriteLine("</div>")
            '.WriteLine("<div class=""widget-content"" style=""min-height: 200px"">")
            '.WriteLine("<p>Content</p>")
            '.WriteLine("</div>")
            '.WriteLine("</div>")
            .WriteLine("</div>")
            ' CRM Updates
            If (objLeadPlatform.Config.SupplierCompanyID = 0) Then
                .WriteLine("<div class=""span8"">")
                .WriteLine("<div class=""widget"">")
                .WriteLine("<div class=""widget-header"">")
                .WriteLine("<h3><i class=""icon-comments""></i>Social Feed</h3>")
                .WriteLine("</div>")
                .WriteLine("<div class=""widget-content"" style=""min-height: 550px"">")
                '.WriteLine("<img src=""/img/engaged-logo.png"" class=""floatRight"" />")
                .WriteLine("<h3>Engaged CRM News &amp; Updates</h3>")
                .WriteLine(socialFeed(Nothing))
                .WriteLine("</div>")
                .WriteLine("</div>")
                .WriteLine("</div>")
            End If

        End With
        HttpContext.Current.Response.Write(objStringWriter.ToString)
    End Sub

    Private Function getFavouritesCount(ByVal favType As Integer) As String
        Dim strCount As String = "0"
        If (favType = 1 Or favType = 3) Then
            strCount = New Caching(CacheObject, "SELECT COUNT(*) FROM tblfavourites WHERE UserID = '" & Config.DefaultUserID & "' AND FavouriteType = " & favType & " AND FavouriteActive = 1 AND CompanyID = '" & CompanyID & "'", "", "", "").returnCacheString
        ElseIf (favType = 2) Then
            strCount = New Caching(CacheObject, "SELECT COUNT(*) FROM tblfavourites FAV INNER JOIN tblworkflows WF ON WF.WorkflowID = FAV.FavouriteItem INNER JOIN tblworkflowmappings MAP ON MAP.WorkflowMappingWorkflowID = WF.WorkflowID WHERE UserID = '" & Config.DefaultUserID & "' AND MAP.WorkflowMappingUserID = '" & Config.DefaultUserID & "' AND FavouriteType = " & favType & " AND FavouriteActive = 1 AND FAV.CompanyID = '" & CompanyID & "' AND WorkflowActive = 1", "", "", "").returnCacheString
        End If
        Return strCount
    End Function

    Private Function getFavourites(ByVal favType As Integer) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (favType = 1) Then
                Dim strSQL As String = "SELECT FavouriteID, FavouriteName, FavouriteItem FROM tblfavourites WHERE UserID = '" & Config.DefaultUserID & "' AND FavouriteType = " & favType & " AND FavouriteActive = 1 AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = "", intLastID As String = ""
                    For Each Row In dsCache.Rows
                        .WriteLine("<tr>")
                        .WriteLine("<td class=""smlc""><a href=""/reports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & """>" & Row.Item("FavouriteName").ToString & "</a></td>")
                        .WriteLine("<td class=""smlc""><button onclick=""location.href='/reports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & "';"" class=""btn btn-primary btn-mini"">Open</button>")
                        .WriteLine("<button href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','View Report', '/prompts/reports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & "&frmHideFilters=Y', '1600', '800');"" class=""btn btn-primary btn-mini"">Preview</button>")
                        .WriteLine("<button onclick=""confirmDeleteFavourite('/inc/utilitysave.aspx?strThisPg=favourite&strFrmAction=delete&FavouriteID=" & Row.Item("FavouriteID") & "&strReturnURL=/');"" class=""btn btn-danger btn-mini""><i class=""icon-remove""></i> Delete</button></td>")
                        .WriteLine("</tr>")
                    Next
                Else
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""smlc"">No favourite reports found</td>")
                    .WriteLine("</tr>")
                End If
            ElseIf (favType = 2) Then
                Dim strSQL As String = "SELECT FavouriteID, FavouriteName, FavouriteItem, WorkflowID, WorkflowTypeID, WorkflowTotalRecords, WorkflowDueRecords, WorkflowIndividualStats, WorkflowReportBuilderID FROM tblfavourites FAV INNER JOIN tblworkflows WF ON WF.WorkflowID = FAV.FavouriteItem INNER JOIN tblworkflowmappings MAP ON MAP.WorkflowMappingWorkflowID = WF.WorkflowID WHERE UserID = '" & Config.DefaultUserID & "' AND MAP.WorkflowMappingUserID = '" & Config.DefaultUserID & "' AND FavouriteType = " & favType & " AND FavouriteActive = 1 AND FAV.CompanyID = '" & CompanyID & "' AND WorkflowActive = 1"
                Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = "", intLastID As String = ""
                    For Each Row In dsCache.Rows
                        .WriteLine("<tr>")
                        .WriteLine("<td class=""smlc""><a href=""/workflow/workflow.aspx?frmWorkflowID=" & Row.Item("WorkflowID") & "&frmWorkflowTypeID=" & Row.Item("WorkflowTypeID") & """>" & Row.Item("FavouriteName").ToString & "</a></td>")
                        If (Row.Item("WorkflowIndividualStats")) Then
                            Dim strSQL2 As String = "SELECT TOP 1 WorkflowDueRecords, WorkflowTotalRecords FROM tblworkflowstats WHERE WorkflowID = '" & Row.Item("WorkflowID") & "' AND UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
                            Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
                            If (dsCache.Rows.Count > 0) Then
                                For Each InnerRow As DataRow In dsCache2.Rows
                                    .WriteLine("<td class=""smlc"">" & InnerRow.Item("WorkflowTotalRecords") & " (" & InnerRow.Item("WorkflowDueRecords") & ")</td>")
                                Next
                            Else
                                .WriteLine("<td class=""smlc"">0 (0)</td>")
                            End If
                            dsCache2 = Nothing
                        Else
                            .WriteLine("<td class=""smlc"">" & Row.Item("WorkflowTotalRecords") & " (" & Row.Item("WorkflowDueRecords") & ")</td>")
                        End If
                        .WriteLine("<td class=""smlc""><button onclick=""location.href='/workflow/nextworkflow.aspx?frmWorkflowID=" & Row.Item("WorkflowID") & "';"" class=""btn btn-primary btn-mini"">Open</button>")
                        .WriteLine("<button href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','View Workflow', '/prompts/reports.aspx?frmWorkflowID=" & Row.Item("WorkflowID") & "&frmReportType=Workflow&frmList=Y&frmReportBuilderID=" & Row.Item("WorkflowReportBuilderID") & "&frmHideFilters=Y&frmTitle=" & Row.Item("FavouriteName") & "', '1600', '800');"" class=""btn btn-primary btn-mini"">Preview</button>")
                        .WriteLine("<button onclick=""confirmDeleteFavourite('/inc/utilitysave.aspx?strThisPg=favourite&strFrmAction=delete&FavouriteID=" & Row.Item("FavouriteID") & "&strReturnURL=/');"" class=""btn btn-danger btn-mini""><i class=""icon-remove""></i> Delete</button></td>")
                        .WriteLine("</tr>")
                    Next
                Else
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""smlc"">No favourite workflows found</td>")
                    .WriteLine("</tr>")
                End If
            ElseIf (favType = 3) Then
                Dim strSQL As String = "SELECT FavouriteID, FavouriteName, FavouriteItem FROM tblfavourites WHERE UserID = '" & Config.DefaultUserID & "' AND FavouriteType = " & favType & " AND FavouriteActive = 1 AND CompanyID = '" & CompanyID & "'"
                Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    Dim x As Integer = 0, strLastClass As String = "", intLastID As String = ""
                    For Each Row In dsCache.Rows
                        .WriteLine("<tr>")
                        .WriteLine("<td class=""smlc""><a href=""/businessreports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & """>" & Row.Item("FavouriteName").ToString & "</a></td>")
                        .WriteLine("<td class=""smlc""><button onclick=""location.href='/businessreports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & "';"" class=""btn btn-primary btn-mini"">Open</button>")
                        .WriteLine("<button href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','View Report', '/prompts/businessreports.aspx?" & decodeURL(Row.Item("FavouriteItem")) & "&frmTitle=" & Row.Item("FavouriteName").ToString & "&frmHideFilters=Y', '1600', '800');"" class=""btn btn-primary btn-mini"">Preview</button>")
                        .WriteLine("<button onclick=""confirmDeleteFavourite('/inc/utilitysave.aspx?strThisPg=favourite&strFrmAction=delete&FavouriteID=" & Row.Item("FavouriteID") & "&strReturnURL=/');"" class=""btn btn-danger btn-mini""><i class=""icon-remove""></i> Delete</button></td>")
                        .WriteLine("</tr>")
                    Next
                Else
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""smlc"">No favourite reports found</td>")
                    .WriteLine("</tr>")
                End If
            End If
        End With
        Return objStringWriter.ToString
    End Function

    Public Function socialFeed(ByVal cache As Web.Caching.Cache) As String
        Dim objStringWriter As New StringWriter
        With objStringWriter
            If (getUserAccessLevel(Config.DefaultUserID, "Make Posts")) Then
                .WriteLine("<div id=""postContainer"">")
                .WriteLine("<form id=""frmSocial"" name=""frmSocial"" onsubmit=""return false"">")
                .WriteLine("<blockquote class=""triangle-right"">")
                .WriteLine("<textarea name=""frmSocialPost"" id=""frmSocialPost"" class=""text input-xlarge mandatory post"" title=""Post"" placeholder=""Say something...""></textarea>")
                .WriteLine("<span style=""position: absolute; margin: 8px 0px 0px 0px;"">")
                .WriteLine("<input id=""go"" name=""go"" type=""submit"" value=""Post"" onclick=""if (checkPageMandatory('frmSocial')) { dynamicSocialPost('frmSocialPost', " & Config.DefaultUserID & ",0,'" & objLeadPlatform.Config.SuperAdmin & "'); }"" class=""btn btn-secondary btn-mini"" />")
                .WriteLine("</span>")
                .WriteLine("</blockquote>")
                .WriteLine("</form>")
                .WriteLine("</div>")
            Else
                .WriteLine("<br /><br />")
            End If
            .WriteLine("<div id=""notes-container"" style=""overflow: auto;"">")
            .WriteLine("<table id=""notes-table"" name=""notes-table"" class=""table notes"" width=""100%"" align=""center"" cellspacing=""0"" cellpadding=""0"">")
            Dim strSQL As String = "SELECT TOP 25 PST.CompanyID, PST.SocialPostID, PST.SocialPostText, PST.SocialPostDate, USR.UserFullName, (SELECT COUNT(*) AS Expr1 FROM tblsocialposts AS CMT WHERE (ParentSocialPostID = PST.SocialPostID) AND (CompanyID = PST.CompanyID OR CompanyID = 0 OR PST.CompanyID = 0) AND SocialPostActive = 1) AS NoComments FROM tblsocialposts PST INNER JOIN tblusers USR ON USR.UserID = PST.SocialPostUserID " & _
                "WHERE (PST.CompanyID = 0 OR PST.CompanyID = '" & CompanyID & "') AND ParentSocialPostID = 0 AND SocialPostActive = 1 ORDER BY PST.SocialPostDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            Dim x As Integer = 0, dteLast As Date, strUserName As String = ""
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (Row.Item("CompanyID") = 0) Then
                        strUserName = "Engaged CRM"
                    Else
                        strUserName = Row.Item("UserFullName")
                    End If
                    .WriteLine("<tr class=""tablehead"">")
                    .WriteLine("<td data-post-id=""" & Row.Item("SocialPostID") & """ class=""sml"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;"">")
                    If (CDate(Row.Item("SocialPostDate")).Date <> dteLast) Then
                        .WriteLine("<div class=""calendar-page""><div class=""month"">" & UCase(MonthName(CDate(Row.Item("SocialPostDate")).Month, True)) & "</div><div class=""day"">" & padZeros(CDate(Row.Item("SocialPostDate")).Day, 2) & "</div></div>")
                    Else
                        .WriteLine("<div class=""calendar-page calendar-spacer""><div class=""month displayNone"">" & UCase(MonthName(CDate(Row.Item("SocialPostDate")).Month, True)) & "</div><div class=""day displayNone"">" & padZeros(CDate(Row.Item("SocialPostDate")).Day, 2) & "</div></div>")
                    End If
                    If (Row.Item("CompanyID") = 0) Then
                        .WriteLine("<div class=""profile""><div class=""frame""><img src=""/img/companies/Engaged-Solutions.png"" width=""40"" height=""40"" class=""img"" /></div></div>")
                    Else
                        .WriteLine("<div class=""profile""><div class=""frame""><img src=""/img/companies/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & ".png"" width=""40"" height=""40"" class=""img"" /></div></div>")
                    End If
                    If (Row.Item("NoComments") > 0) Then
                        .WriteLine("<div class=""post""><span class=""title"">" & strUserName & "</span>&nbsp;" & Row.Item("SocialPostText") & "<br /><span class=""date"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("SocialPostDate")) & " <a href=""#"" class=""view-comments""><span class=""view"">Show</span> Comments (<span class=""no-comments"">" & Row.Item("NoComments") & "</span>)</a></span>")
                    Else
                        .WriteLine("<div class=""post""><span class=""title"">" & strUserName & "</span>&nbsp;" & Row.Item("SocialPostText") & "<br /><span class=""date"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("SocialPostDate")) & " <a href=""#"" class=""view-comments displayNone""><span class=""view"">Show</span> Comments (<span class=""no-comments"">" & Row.Item("NoComments") & "</span>)</a></span>")
                    End If
                    If (getUserAccessLevel(Config.DefaultUserID, "Delete Posts")) Then
                        .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeSocialPost(this," & Row.Item("SocialPostID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                    End If
                    .WriteLine("</div></td>")
                    .WriteLine("</tr>")
                    strSQL = "SELECT PST.CompanyID, PST.SocialPostID, PST.SocialPostText, PST.SocialPostDate, PST.SocialPostUserID, USR.UserName, USR.UserFullName FROM tblsocialposts PST INNER JOIN tblusers USR ON USR.UserID = PST.SocialPostUserID " & _
                             "WHERE (PST.CompanyID = 0 OR PST.CompanyID = '" & CompanyID & "') AND ParentSocialPostID = " & Row.Item("SocialPostID") & " AND SocialPostActive = 1 ORDER BY PST.SocialPostDate DESC"
                    Dim dsCacheInner As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
                    If (dsCacheInner.Rows.Count > 0) Then
                        .WriteLine("<tr class=""tablehead"">")
                        .WriteLine("<td class=""sml"" style=""width: 70%;padding: 5px 5px 5px 5px; white-space: normal;display: none"">")
                        .WriteLine("<div style=""margin-left: 70px"">")
                        .WriteLine("<table class=""table notes"" width=""100%"" align=""center"" cellspacing=""0"" cellpadding=""0"">")
                        For Each InnerRow As DataRow In dsCacheInner.Rows
                            If (InnerRow.Item("CompanyID") = 0) Then
                                strUserName = "Engaged CRM"
                            Else
                                strUserName = InnerRow.Item("UserFullName")
                            End If
                            .WriteLine("<tr class=""tablehead"">")
                            .WriteLine("<td data-post-id=""" & InnerRow.Item("SocialPostID") & """ class=""sml"" style=""border-left: 2px solid #255b92; width: 70%;padding: 5px 5px 5px 5px; white-space: normal;"">")
                            If (Row.Item("CompanyID") = 0) Then
                                .WriteLine("<div class=""profile""><div class=""frame""><img src=""/img/companies/Engaged-Solutions.png"" width=""40"" height=""40"" class=""photo"" /></div></div>")
                            Else
                                If (getUserAccessLevel(InnerRow.Item("SocialPostUserID"), "Make Posts")) Then
                                    If (InnerRow.Item("CompanyID") = 0) Then
                                        .WriteLine("<div class=""profile""><div class=""frame""><img src=""/img/companies/Engaged-Solutions.png"" width=""40"" height=""40"" class=""img"" /></div></div>")
                                    Else
                                        .WriteLine("<div class=""profile""><div class=""frame""><img src=""/img/companies/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & ".png"" width=""40"" height=""40"" class=""img"" /></div></div>")
                                    End If
                                Else
                                    .WriteLine("<div class=""profile""><div class=""frame""><img src=""" & getCommentAvatarImage(InnerRow.Item("UserName")) & """ width=""40"" height=""40"" class=""img"" /></div></div>")
                                End If
                            End If
                            .WriteLine("<div><span class=""title"">" & strUserName & "</span>&nbsp;" & InnerRow.Item("SocialPostText") & "<br /><span class=""date"">" & ddmmyyhhmmss2ddmmhhmm(InnerRow.Item("SocialPostDate")) & "</span>")
                            If (getUserAccessLevel(Config.DefaultUserID, "Delete Posts")) Then
                                .WriteLine("<span style=""float: right; margin: -2px 0px 0px 0px;""><a href=""#"" onclick=""removeSocialPost(this," & InnerRow.Item("SocialPostID") & ")"" class=""delete displayNone""><i class=""icon-remove"" title=""Delete Note""></i></a></span>")
                            End If
                            .WriteLine("</div></td>")
                            .WriteLine("</tr>")
                        Next
                        .WriteLine("</table>")
                        .WriteLine("</div></td></tr>")
                    End If
                    dteLast = CDate(Row.Item("SocialPostDate")).Date
                    x += 1
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No posts found.</td>")
                .WriteLine("</tr>")
            End If
            dsCache = Nothing
            .WriteLine("</table>")
            .WriteLine("</div>")
            'If (objLeadPlatform.Config.SuperAdmin Or objLeadPlatform.Config.Manager) Then
            .WriteLine("<script type=""text/javascript"">")
            .WriteLine("$(document).ready(function () {")
            .WriteLine("    $('textarea.post').autoResize({ extraSpace: 0 });")
            .WriteLine("    $('.rollover').rollover( { fade:false } );")
            .WriteLine("    $('.view-comments').click(function() {")
            .WriteLine("        $cell = $(this).parent().parent().parent().parent().next().children('td');")
            .WriteLine("        $cell.toggle()")
            .WriteLine("        if ($cell.is(':hidden')) {")
            .WriteLine("            $(this).children('.view').text('Show')")
            .WriteLine("        } else {")
            .WriteLine("            $(this).children('.view').text('Hide')")
            .WriteLine("        }")
            .WriteLine("    });")
            If (getUserAccessLevel(Config.DefaultUserID, "Comment Posts")) Then
                .WriteLine("    $('div.post').click(function() {")
                .WriteLine("        if ($(this).find('form').size() == 0) {")
                .WriteLine("            $postId = $(this).parent().attr('data-post-id');")
                .WriteLine("            $('div.post form').remove();")
                .WriteLine("            $(this).append('<form id=""frmSocial' + $(this).index() + '"" name=""frmSocial"" onsubmit=""return false""><textarea name=""frmSocialPost' + $(this).index() + '"" id=""frmSocialPost' + $(this).index() + '"" class=""text input-xlarge mandatory post"" placeholder=""Make a comment..."" title=""Post""></textarea><span style=""position: absolute; margin: 8px 0px 0px 0px;""><input id=""go"" name=""go"" type=""submit"" value=""Add"" onclick=""if (checkPageMandatory(\'frmSocial' + $(this).index() + '\')) { dynamicSocialPost(\'frmSocialPost' + $(this).index() + '\', " & Config.DefaultUserID & ",' + $postId + ',\'" & objLeadPlatform.Config.SuperAdmin & "\'); }"" class=""btn btn-secondary btn-mini"" /></span></form>')")
                .WriteLine("        }")
                .WriteLine("    });")
            End If
            .WriteLine("    $('table tr td div').mouseover(function() {")
            .WriteLine("        $(this).children('span').children('.delete').removeClass('displayNone').addClass('displayBlock');")
            .WriteLine("        $(this).parent().addClass('row-highlight');")
            .WriteLine("    });")
            .WriteLine("    $('table tr td div').mouseout(function() {")
            .WriteLine("        $(this).children('span').children('.delete').removeClass('displayBlock').addClass('displayNone');")
            .WriteLine("        $(this).parent().removeClass('row-highlight');")
            .WriteLine("    });")
            .WriteLine("});")
            .WriteLine("</script>")
            'End If
        End With
        Return objStringWriter.ToString
    End Function

    Private Function getCommentAvatarImage(ByVal userName As String) As String
        Dim strPath As String = "/img/users/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & "/" & userName & ".jpg"
        If (objLeadPlatform.Config.SuperAdmin) Then
            strPath = "/img/users/" & userName & ".jpg"
        End If
        responseWrite(strPath)
        If (File.Exists(HttpContext.Current.Server.MapPath(strPath))) Then
            Return strPath
        Else
            Return "/img/avatar.jpg"
        End If
    End Function

    Public Function getBigStatValues(ByVal StatisticID As String, ByVal text As String) As String
        Dim strStatValue As String = ""
        Dim objResponse As Net.HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/statistics.aspx?StatisticID=" & StatisticID & "&frmMode=Single&UserSessionID=" & Config.UserSessionID)
        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            strStatValue = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing
        End If
        objResponse.Close()
        objResponse = Nothing

        Dim strStatistics As String = ""
        If (checkValue(strStatValue)) Then
            Dim arrStats As Array = Split(strStatValue, ",")
            For y As Integer = 0 To UBound(arrStats)
                If (y = 0) Then
                    If (Not checkValue(arrStats(y))) Then
                        strStatistics = Replace(text, "{StatValue}", 0)
                    Else
                        strStatistics = Replace(text, "{StatValue}", arrStats(y))
                    End If
                Else
                    strStatistics = Replace(strStatistics, "{StatValue" & y & "}", arrStats(y))
                End If
            Next
        Else
            strStatistics = regexReplace("\{StatValue[0-9]{0,2}\}", 0, text)
        End If
        Return "<div id=""big-stats-container"" class=""widget"" data-id=""" & StatisticID & """><div class=""widget-content""><div id=""big_stats"" class=""cf"">" & strStatistics & "</div></div></div>"
    End Function

    Public Function getBigStatExternalValues(ByVal StatisticID As String, ByVal text As String, ByVal url As String) As String
        Dim strStatValue As String = ""
        Dim objResponse As Net.HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/outbound/" & url & "?UserSessionID=" & Config.UserSessionID)
        If (checkResponse(objResponse, "")) Then
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            strStatValue = objReader.ReadToEnd()
            objReader.Close()
            objReader = Nothing
        End If
        objResponse.Close()
        objResponse = Nothing

        Dim strStatistics As String = ""
        If (checkValue(strStatValue)) Then
            Dim arrStats As Array = Split(strStatValue, ",")
            For y As Integer = 0 To UBound(arrStats)
                If (y = 0) Then
                    If (Not checkValue(arrStats(y))) Then
                        strStatistics = Replace(text, "{StatValue}", 0)
                    Else
                        strStatistics = Replace(text, "{StatValue}", arrStats(y))
                    End If
                Else
                    strStatistics = Replace(strStatistics, "{StatValue" & y & "}", arrStats(y))
                End If
            Next
        Else
            strStatistics = regexReplace("\{StatValue[0-9]{0,2}\}", 0, text)
        End If
        Return "<div id=""big-stats-container"" class=""widget"" data-id=""" & StatisticID & """><div class=""widget-content""><div id=""big_stats"" class=""cf"">" & strStatistics & "</div></div></div>"
    End Function

    Public Sub getStatistics()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT StatisticID, StatisticType, StatisticName, StatisticText, StatisticReportBuilderID, StatisticReportBuilderQueryString, StatisticRoutine, DashboardPanelName FROM vwdashboardpanel WHERE DashboardPanelID = '" & PanelID & "' AND DashboardID = '" & DashboardID & "' AND CompanyID = '" & CompanyID & "'"
            Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
            Dim strReportBuilderQueryString As String = ""
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    Select Case Row.Item("StatisticType")
                        Case StatisticType.Text
                            Dim strStatValue As String = ""
                            Dim objResponse As Net.HttpWebResponse = getWebRequest(Config.ApplicationURL & "/webservices/statistics.aspx?StatisticID=" & Row.Item("StatisticID") & "&frmMode=Single&UserSessionID=" & Config.UserSessionID)
                            If (checkResponse(objResponse, "")) Then
                                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                                strStatValue = objReader.ReadToEnd()
                                objReader.Close()
                                objReader = Nothing
                            End If
                            objResponse.Close()
                            objResponse = Nothing

                            Dim arrStats As Array = Split(strStatValue, ",")
                            Dim strStatistics As String = ""
                            For x As Integer = 0 To UBound(arrStats)
                                If (x = 0) Then
                                    strStatistics = Replace(Row.Item("StatisticText").ToString, "{StatValue}", "<span class=""stat"">" & arrStats(x) & "</span>")
                                Else
                                    strStatistics = Replace(strStatistics, "{StatValue" & x & "}", arrStats(x))
                                End If
                            Next
                            .WriteLine("<p class=""text-huge"">" & strStatistics & "</p>")
                        Case StatisticType.Report
                            strReportBuilderQueryString = Replace(Row.Item("StatisticReportBuilderQueryString").ToString, "/net/reports/reportbuilder.aspx", "/prompts/reports.aspx")
                            strReportBuilderQueryString = menuTags(strReportBuilderQueryString)
                            strReportBuilderQueryString = Replace(strReportBuilderQueryString, "{UserID}", Config.DefaultUserID)
                            .WriteLine("<h3>" & Row.Item("DashboardPanelName") & "</h3><br />")
                            .WriteLine(getReport(strReportBuilderQueryString))
                        Case StatisticType.Table
                            ' Not implemented yet, reports should suffice
                        Case StatisticType.Chart, StatisticType.Timeline
                            .WriteLine("<div id=""" & Row.Item("StatisticName") & """ style=""height: 100%""></div>")
                        Case StatisticType.SubRoutine
                            Dim arrParams As Object() = {CacheObject()}
                            .WriteLine(executeSub(Me, Row.Item("StatisticRoutine").ToString, arrParams))
                    End Select
                Next
            End If
            dsCache = Nothing
        End With
        HttpContext.Current.Response.Write(objStringWriter.ToString)
    End Sub

    Private Function getReport(ByVal strURL As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<iframe id=""reportiframe"" name=""reportiframe"" frameborder=""0"" width=""100%"" height=""100px"" src=""" & strURL & """></iframe>")
        End With
        Return objStringWriter.ToString
    End Function

    Shared Function executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(Dashboard)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        Return objMethodInfo.Invoke(inst, params)
    End Function

    Public Function wikiNews(ByVal cache As Web.Caching.Cache) As String
        Dim strSearch As String = HttpContext.Current.Request("frmSearch")
        Dim objStringWriter As New StringWriter
        With objStringWriter
            .WriteLine("<div class=""row-fluid""><div class=""span12""><div class=""widget nopad"">")
            .WriteLine("<form name=""frmWiki"" id=""frmWiki"" method=""post"">")
            .WriteLine("<div class=""widget-actions""><input id=""frmSearch"" name=""frmSearch"" type=""text"" class=""text input-large mandatory"" placeholder=""Search Wiki"" value=""" & strSearch & """ />&nbsp;<button onclick="""" class=""btn btn-secondary btn-mini"">Go</button></div>")
            .WriteLine("</form>")
            .WriteLine("<table id=""article-table"" class=""table table-bordered table-striped table-small"">")
            Dim strSQL As String = "SELECT WikiPageID, WikiPageTitle, WikiPageContent, WikiPageStartDate, CreatedUserFullName FROM vwwiki WHERE WikiSectionID = 1 AND PageCompanyID = " & CompanyID & " AND (WikiPageStartDate IS NULL OR WikiPageStartDate <= GETDATE()) AND (WikiPageEndDate IS NULL OR WikiPageEndDate >= GETDATE())"
            If (getUserAccessLevel(Config.DefaultUserID, "AccessIntranetAdmin")) Then
                strSQL += " AND (WikiPageLive = 0 OR WikiPageLive = 1)"
            Else
                strSQL += " AND WikiPageLive = 1"
            End If
            If (checkValue(strSearch)) Then
                strSQL += " AND (WikiPageTitle LIKE '%" & strSearch & "%' OR WikiPageContent LIKE '%" & strSearch & "%')"
            End If
            strSQL += " ORDER BY WikiPageStartDate DESC"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>")
                    .WriteLine("<h4>" & Row.Item("WikiPageTitle") & "</h4>")
                    .WriteLine("<div class=""article"">" & shorten(Row.Item("WikiPageContent"), 30) & "</div>")
                    .WriteLine("<p class=""article-info""><span class=""date"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("WikiPageStartDate")) & "</span> by <span class=""title"">" & Row.item("CreatedUserFullName") & "</span></p>")
                    .WriteLine("<button onclick=""viewWikiPage(" & Row.Item("WikiPageID") & ")"" class=""btn btn-primary btn-small"">View</button>")
                    .WriteLine("</td>")
                    .WriteLine("</tr>")
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No articles found</td>")
                .WriteLine("</tr>")
            End If
            .WriteLine("</table>")
            .WriteLine("</div></div></div>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function wikiPageList(ByVal section As String) As String
        Dim strSearch As String = HttpContext.Current.Request("frmSearch")
        Dim objStringWriter As New StringWriter
        With objStringWriter
            .WriteLine("<div class=""row-fluid""><div class=""span12""><div class=""widget nopad"">")
            .WriteLine("<form name=""frmWiki"" id=""frmWiki"" method=""post"">")
            .WriteLine("<div class=""widget-actions""><input id=""frmSearch"" name=""frmSearch"" type=""text"" class=""text input-large mandatory"" placeholder=""Search Wiki"" value=""" & strSearch & """ />&nbsp;<button onclick="""" class=""btn btn-secondary btn-mini"">Go</button></div>")
            .WriteLine("</form>")
            .WriteLine("<table id=""article-table"" class=""table table-bordered table-striped table-small"">")
            Dim strSQL As String = "SELECT WikiPageID, WikiPageTitle, WikiPageStartDate FROM vwwiki WHERE WikiSectionID = " & section & " AND PageCompanyID = " & CompanyID & " AND (WikiPageStartDate IS NULL OR WikiPageStartDate <= GETDATE()) AND (WikiPageEndDate IS NULL OR WikiPageEndDate >= GETDATE())"
            If (getUserAccessLevel(Config.DefaultUserID, "AccessIntranetAdmin")) Then
                strSQL += " AND (WikiPageLive = 0 OR WikiPageLive = 1)"
            Else
                strSQL += " AND WikiPageLive = 1"
            End If
            If (checkValue(strSearch)) Then
                strSQL += " AND (WikiPageTitle LIKE '%" & strSearch & "%' OR WikiPageContent LIKE '%" & strSearch & "%')"
            End If
            strSQL += " ORDER BY WikiPageStartDate DESC"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row.Item("WikiPageTitle") & "</td>")
                    .WriteLine("<td><button onclick=""viewWikiPage(" & Row.Item("WikiPageID") & ")"" class=""btn btn-primary btn-mini"">View</button></td>")
                    .WriteLine("</tr>")
                Next
            Else
                .WriteLine("<tr>")
                .WriteLine("<td class=""smlc"">No articles found</td>")
                .WriteLine("</tr>")
            End If
            .WriteLine("</table>")
            .WriteLine("</div></div></div>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function wikiPolicies(ByVal cache As Web.Caching.Cache) As String
        Return wikiPageList(2)
    End Function

    Public Function wikiStaff(ByVal cache As Web.Caching.Cache) As String
        Return wikiPageList(3)
    End Function

    Public Function wikiKnowledgeBase(ByVal cache As Web.Caching.Cache) As String
        Return wikiPageList(4)
    End Function

    Public Function wikiPageContent(ByVal cache As Web.Caching.Cache) As String
        Dim objStringWriter As New StringWriter
        With objStringWriter
            Dim intPageID As String = HttpContext.Current.Request("WikiPageID")
            .WriteLine("<div class=""row-fluid""><div class=""span12""><div class=""widget nopad"">")
            Dim strSQL As String = ""
            If (checkValue(intPageID)) Then
                strSQL = "SELECT WikiPageTitle, WikiPageContent, WikiTemplateHTML, WikiPageStartDate, CreatedUserFullName FROM vwwiki WHERE WikiPageID = '" & intPageID & "'"
            Else
                strSQL = "SELECT TOP 1 WikiPageID, WikiPageTitle, WikiPageContent, WikiTemplateHTML, WikiPageStartDate, CreatedUserFullName FROM vwwiki WHERE WikiSectionID = 1 AND (WikiPageStartDate IS NULL OR WikiPageStartDate <= GETDATE()) AND (WikiPageEndDate IS NULL OR WikiPageEndDate >= GETDATE()) AND PageCompanyID = " & CompanyID & " AND WikiPageActive = 1"
            End If
            If (getUserAccessLevel(Config.DefaultUserID, "AccessIntranetAdmin")) Then
                strSQL += " AND (WikiPageLive = 0 OR WikiPageLive = 1)"
            Else
                strSQL += " AND WikiPageLive = 1"
            End If
            strSQL += " ORDER BY WikiPageStartDate DESC"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row In dsCache.Rows
                    If (Not checkValue(intPageID)) Then
                        intPageID = Row.Item("WikiPageID")
                    End If
                    wikiPageRead(intPageID)
                    .WriteLine("<h2>" & Row.Item("WikiPageTitle") & "</h2>")
                    .WriteLine("<h5 class=""article-info""><span class=""date"">" & Row.Item("WikiPageStartDate") & "</span> by <span class=""title"">" & Row.item("CreatedUserFullName") & "</span></h5>")
                    Dim strContent As String = wikiPageLinks(Row.item("WikiPageContent"), intPageID, Row.Item("WikiPageTitle"))
                    Dim strPage As String = Replace(Row.Item("WikiTemplateHTML"), "xxx[PageContent]xxx", strContent)
                    strPage = wikiPageImages(strPage, intPageID)
                    .WriteLine("<div class=""article"">" & strPage & "</div>")
                Next
            End If
            .WriteLine("</div></div></div>")
        End With
        Return objStringWriter.ToString
    End Function

    Private Sub wikiPageRead(ByVal id As String)
        Dim strSQL As String = "SELECT WikiPageReadID FROM tblwikipagesread WHERE UserID = " & Config.DefaultUserID & " AND WikiPageID = '" & id & "' AND CompanyID = " & CompanyID
        Dim strResult As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
        If (Not checkValue(strResult)) Then
            executeNonQuery("INSERT INTO tblwikipagesread(CompanyID, UserID, WikiPageID) VALUES(" & CompanyID & ", " & Config.DefaultUserID & "," & id & ")")
        End If
    End Sub

    Private Function wikiPageLinks(ByVal content As String, ByVal id As String, ByVal title As String) As String
        Dim intPageID As String = HttpContext.Current.Request("WikiPageID")
        Dim strSQL As String = "SELECT * FROM vwwiki WHERE PageCompanyID = " & CompanyID & " AND WikiPageID <> '" & id & "'"
        Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim strLinkURL As String = ""
            For Each Row As DataRow In dsCache.Rows
                If (InStr(UCase(content), UCase(Row.Item("WikiPageTitle"))) > 0) Then
                    strLinkURL = "panel.aspx?DashboardID=" & HttpContext.Current.Request("DashboardID") & "&PanelID=" & HttpContext.Current.Request("PanelID") & "&WikiPageID=" & Row.Item("WikiPageID")
                    content = regexReplace("\b(" & Row.Item("WikiPageTitle") & ")\b", "<a href=""" & strLinkURL & """ class=""ui-tooltip"" data-placement=""right"" title=""" & shorten(Row.Item("WikiPageContent"), 100) & """>$1</a>", content)
                End If
            Next
        End If
        dsCache = Nothing
        'content = regexReplace("(0[1-9]|[12]\d|3[01])/(0[1-9]|1[0-2])/((?:19|20)\d{2})", "<a href=""#"" onclick=""top.confirmAction('/inc/utilitysave.aspx?strThisPg=calendar&strFrmAction=addevent&frmCalendarEventType=2&frmCalendarEventName=" & title & "&frmCalendarEventDescription=" & title & "&frmCalendarEventColour=2&frmCalendarEventStartDate=$1/$2/$3&frmCalendarEventAllDay=1&strReturnURL=/','Do you want to add the event " & title & " to your calendar?');"" class=""ui-tooltip"" data-placement=""right"" title=""Click to add event"">$1/$2/$3</a>", content)
        Return content
    End Function

    Private Function wikiPageImages(ByVal content As String, ByVal id As String) As String
        Dim intPageID As String = HttpContext.Current.Request("WikiPageID")
        Dim strSQL As String = "SELECT WikiPageImage FROM tblwikipageimages WHERE CompanyID = " & CompanyID & " AND WikiPageID = '" & id & "'"
        Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            Dim x As Integer = 1
            For Each Row As DataRow In dsCache.Rows
                Dim strPath As String = "/img/" & Row.Item("WikiPageImage")
                content = Replace(content, "xxx[PageImage" & x & "]xxx", "<img src=""" & strPath & """ />")
                x += 1
            Next
        End If
        dsCache = Nothing
        Return content
    End Function

    Private Function getAvailableDashboards() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            If (Not WallBoard()) Then
                .WriteLine("<li><a href=""#"" onclick=""setDashboard($(this).parent().prev(),0,'CRM Hub');return false;"">CRM Hub</a></li>")
            End If
            Dim strSQL As String = "SELECT DSH.DashboardID, DashboardName FROM tbldashboards DSH INNER JOIN tbldashboardmappings MAP ON MAP.DashboardID = DSH.DashboardID WHERE DashboardTicker = 0 AND UserID = '" & Config.DefaultUserID & "' AND DSH.CompanyID = '" & CompanyID & "' ORDER BY DashboardName"
            Dim dsCache As DataTable = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (WallBoard()) Then
                        .WriteLine("<li><a href=""#"" onclick=""setWallboard($(this).parent().prev()," & Row.Item("DashboardID") & ",'" & Row.Item("DashboardName").ToString & "');return false;"">" & Row.Item("DashboardName").ToString & "</a></li>")
                    Else
                        .WriteLine("<li><a href=""#"" onclick=""setDashboard($(this).parent().prev()," & Row.Item("DashboardID") & ",'" & Row.Item("DashboardName").ToString & "');return false;"">" & Row.Item("DashboardName").ToString & "</a></li>")
                    End If
                Next
            End If
            dsCache = Nothing
            strSQL = "SELECT DashboardID, DashboardName FROM tbldashboards WHERE DashboardTicker = 0 AND DashboardDefault = 1 AND CompanyID = '" & CompanyID & "' ORDER BY DashboardName"
            dsCache = New Caching(CacheObject(), strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    If (WallBoard()) Then
                        .WriteLine("<li><a href=""#"" onclick=""setWallboard($(this).parent().prev()," & Row.Item("DashboardID") & ",'" & Row.Item("DashboardName").ToString & "');return false;"">" & Row.Item("DashboardName").ToString & "</a></li>")
                    Else
                        .WriteLine("<li><a href=""#"" onclick=""setDashboard($(this).parent().prev()," & Row.Item("DashboardID") & ",'" & Row.Item("DashboardName").ToString & "');return false;"">" & Row.Item("DashboardName").ToString & "</a></li>")
                    End If
                Next
            End If
            dsCache = Nothing
        End With
        boolChangeMenu = True
        Return objStringWriter.ToString
    End Function

    Public Function activeCalls(ByVal cache As Web.Caching.Cache) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<script src=""/js/pages/calls.js""></script>")
            .WriteLine("<div style=""padding-top: 10px"">")
            Dim strTenant As String = getSystemConfigValue("EngagedPBXTenant")
            Dim strSQL As String = "SELECT st_states.st_state, st_states.st_timestamp, sipfriends.callerid, sipfriends.id FROM st_states INNER JOIN sipfriends ON sipfriends.name = st_states.st_extension INNER JOIN te_tenants ON te_tenants.te_id = sipfriends.te_id WHERE te_code = '" & strTenant & "' AND TIMESTAMPDIFF(MINUTE,st_timestamp,NOW()) <= 30 AND st_state <> 'UNAVAILABLE' ORDER BY st_timestamp DESC"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "", CommandType.Text, Nothing, "pbx").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row In dsCache.Rows
                    .WriteLine("<div class=""call-row"" data-id=""" & Row.Item("id") & """>")
                    .WriteLine("<div class=""call-label call-name""><i class=""icon-phone fa-2x""></i>&nbsp;<span class=""call-name-text"">" & regexReplace("<[0-9]{0,3}>", "", Row.Item("callerid")) & "</span></div>")
                    Select Case Row.Item("st_state")
                        Case "INUSE"
                            .WriteLine("<div class=""call-content call-number""><i class=""icon-arrow-right blue fa-2x " & Row.Item("st_state") & """></i>&nbsp;<span class=""call-number-text""></span></div>")
                        Case "RINGINUSE"
                            .WriteLine("<div class=""call-content call-number""><i class=""icon-arrow-up green fa-2x " & Row.Item("st_state") & """></i>&nbsp;<span class=""call-number-text""></span></div>")
                        Case "NOT_INUSE"
                            .WriteLine("<div class=""call-content call-number""><i class=""icon-arrow-down red fa-2x " & Row.Item("st_state") & """></i>&nbsp;<span class=""call-number-text""></span></div>")
                        Case "UNAVAILABLE"
                            .WriteLine("<div class=""call-content call-number""><i class=""icon-times red fa-2x " & Row.Item("st_state") & """></i>&nbsp;<span class=""call-number-text""></span></div>")
                    End Select
                    .WriteLine("<div class=""call-content call-time"">&nbsp;<span class=""call-time-text"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("st_timestamp")) & "</span></div>")
                    .WriteLine("</div>")
                Next
            Else
                .WriteLine("<p>No active calls found</p>")
            End If
            .WriteLine("</div>")
            .WriteLine("<script>checkCalls(); setInterval(function () { checkCalls() }, 30000);</script>")
        End With
        Return objStringWriter.ToString
    End Function

    Public Function activeAgents(ByVal cache As Web.Caching.Cache) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<script src=""/js/pages/calls.js""></script>")
            .WriteLine("<div style=""padding-top: 10px"">")
            Dim strSQL As String = "SELECT * FROM vwcallactiveusers WHERE CompanyID = '" & CompanyID & "' ORDER BY TotalDuration DESC"
            Dim dsCache As DataTable = New Caching(CacheObject, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                Dim x As Integer = 0, strLastClass As String = ""
                For Each Row In dsCache.Rows
                    .WriteLine("<div class=""call-row"" data-id=""" & Row.Item("UserID") & """>")
                    .WriteLine("<div class=""call-label user-name""><strong><span class=""user-name-text"">" & Row.Item("UserFullName") & "</span></strong></div>")
                    .WriteLine("<div class=""call-content user-inbound""><nobr><strong>Inbound:</strong>&nbsp;<span class=""user-inbound-text"">" & Row.Item("InboundCalls") & "</span></nobr></div>")
                    .WriteLine("<div class=""call-content user-outbound""><nobr><strong>Outbound:</strong>&nbsp;<span class=""user-outbound-text"">" & Row.Item("OutboundCalls") & "</span></nobr></div>")
                    .WriteLine("<div class=""call-content user-time""><nobr><strong>Total Time:</strong>&nbsp;<span class=""user-time-text"">" & ss2hhmmss(Row.Item("TotalDuration")) & "</span></nobr></div>")
                    .WriteLine("</div>")
                Next
            Else
                .WriteLine("<p>No active users found</p>")
            End If
            .WriteLine("</div>")
            .WriteLine("<script>checkUsers(); setInterval(function () { checkUsers() }, 120000);</script>")
        End With
        Return objStringWriter.ToString
    End Function

End Class