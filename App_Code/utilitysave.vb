﻿Imports Config, Common, CommonSave
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Threading.Thread
Imports System.Net
Imports System.Xml
Imports System.Globalization

' ** Revision history **
'
' ** End Revision History **

Public Class UtilitySave

    Private objLeadPlatform As LeadPlatform = Nothing
    Private strThisPg As String = HttpContext.Current.Request("strThisPg")
    Private strFrmAction As String = HttpContext.Current.Request("strFrmAction")
    Private strReturnUrl As String = HttpContext.Current.Request("strReturnUrl")
    Private strNoRedirect As String = HttpContext.Current.Request("strNoRedirect")
    Private strReturnPg As String = ""
    Private strReturnUrlMessage As String = "", strReturnUrlMessageTitle As String = "", strReturnUrlVariables As String = ""

    Public Property LeadPlatform() As LeadPlatform
        Get
            Return objLeadPlatform
        End Get
        Set(ByVal value As LeadPlatform)
            objLeadPlatform = value
        End Set
    End Property

    Public Sub save()
        Select Case strThisPg
            Case "favourite"
                Select Case strFrmAction
                    Case "addreport", "addbusinessreport", "addworkflow"
                        saveFavouriteAdd()
                    Case "delete"
                        saveFavouriteDelete()
                End Select
            Case "trigger"
                Select Case strFrmAction
                    Case "add", "update"
                        saveTrigger()
                    Case "delete"
                        saveTriggerDelete()
                End Select
            Case "calendar"
                Select Case strFrmAction
                    Case "addevent"
                        saveCalendarEvent()
                    Case "editEvent"
                        editCalendarEvent()
                    Case "removeEvent"
                        removeCalendarEvent()
                End Select
            Case "message"
                Select Case strFrmAction
                    Case "add"
                        saveMessage(HttpContext.Current.Request("frmMessageType"), Config.DefaultUserID, HttpContext.Current.Request("MessageToUserID"), HttpContext.Current.Request("frmMessageSubject"), HttpContext.Current.Request("frmMessageText"), "")
                    Case "approve"
                        saveMessageApprove()
                    Case "deny"
                        saveMessageDeny()
                End Select
            Case "device"
                Select Case strFrmAction
                    Case "change"
                        saveDeviceChange()
                End Select
            Case "rota"
                Select Case strFrmAction
                    Case "addevent"
                        addRotaEvents()
                End Select
            Case "template"
                Select Case strFrmAction
                    Case "add", "update"
                        addTemplate()
                End Select
            Case "section"
                Select Case strFrmAction
                    Case "add", "update"
                        addSection()
                End Select
            Case "page"
                Select Case strFrmAction
                    Case "add", "update"
                        addPage()
                    Case "active"
                        savePageActive()
                End Select
            Case "SecuredPolicy"
                Select Case strFrmAction
                    Case "add", "update"
                        addSecuredPolicy()
                End Select
        End Select

        If (checkValue(strReturnUrl)) Then strReturnPg = strReturnUrl
        If (checkValue(strReturnUrlMessage)) Then
            If (InStr(strReturnPg, "?")) Then
                strReturnPg = strReturnPg & "&strMessage=" & strReturnUrlMessage & "&strMessageTitle=" & strReturnUrlMessageTitle
            Else
                strReturnPg = strReturnPg & "?strMessage=" & strReturnUrlMessage & "&strMessageTitle=" & strReturnUrlMessageTitle
            End If
        End If
        If (checkValue(strReturnUrlVariables)) Then
            If (InStr(strReturnPg, "?")) Then
                strReturnPg = strReturnPg & "&" & strReturnUrlVariables
            Else
                strReturnPg = strReturnPg & "?" & strReturnUrlVariables
            End If
        End If

        If (checkValue(strReturnPg)) Then
            HttpContext.Current.Response.Redirect(strReturnPg)
        Else
            HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))
        End If

        If (strNoRedirect <> "Y") Then
            HttpContext.Current.Response.Redirect(strReturnPg)
        Else
            HttpContext.Current.Response.Write(strReturnUrlMessage)
        End If

    End Sub

    Private Sub saveFavouriteAdd()
        Dim strQry As String = ""
        If (strFrmAction = "addreport") Then
            strQry = "INSERT INTO tblfavourites (CompanyID, UserID, FavouriteName, FavouriteType, FavouriteItem) VALUES (" & _
               formatField(CompanyID, "N", 0) & ", " & _
               formatField(Config.DefaultUserID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmFavouriteName"), "T", "") & ", " & _
               formatField(1, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmQueryString"), "", "") & ")"
        ElseIf (strFrmAction = "addbusinessreport") Then
            strQry = "INSERT INTO tblfavourites (CompanyID, UserID, FavouriteName, FavouriteType, FavouriteItem) VALUES (" & _
               formatField(CompanyID, "N", 0) & ", " & _
               formatField(Config.DefaultUserID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmFavouriteName"), "T", "") & ", " & _
               formatField(3, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmQueryString"), "", "") & ")"
        ElseIf (strFrmAction = "addworkflow") Then
            strQry = "INSERT INTO tblfavourites (CompanyID, UserID, FavouriteName, FavouriteType, FavouriteItem) VALUES (" & _
               formatField(CompanyID, "N", 0) & ", " & _
               formatField(Config.DefaultUserID, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmFavouriteName"), "T", "") & ", " & _
               formatField(2, "N", 0) & ", " & _
               formatField(HttpContext.Current.Request("frmWorkflowID"), "", "") & ")"
        End If
        Call executeNonQuery(strQry)
        cacheDependency(HttpContext.Current.Cache, "tblfavourites")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Favourite+successfully+added"
    End Sub

    Private Sub saveFavouriteDelete()
        Dim strQry As String = "UPDATE tblfavourites SET FavouriteActive = 0 WHERE FavouriteID = '" & HttpContext.Current.Request("FavouriteID") & "' AND UserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        cacheDependency(HttpContext.Current.Cache, "tblfavourites")
    End Sub

    Private Sub saveTrigger()
        Dim TriggerID As String = HttpContext.Current.Request("TriggerID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "UPDATE tbltriggers SET  " & _
               "TriggerName	            = " & formatField(HttpContext.Current.Request("frmTriggerName"), "T", "") & ", " & _
               "TriggerProductType	    = " & formatField(HttpContext.Current.Request("frmTriggerProductType"), "", "") & ", " & _
               "TriggerStatusCode	    = " & formatField(HttpContext.Current.Request("frmTriggerStatusCode"), "U", "") & ", " & _
               "TriggerSubStatusCode    = " & formatField(HttpContext.Current.Request("frmTriggerSubStatusCode"), "U", "") & ", " & _
               "TriggerEmailAddress     = " & formatField(HttpContext.Current.Request("frmTriggerEmailAddress"), "", "") & ", " & _
               "TriggerTelephoneNumber  = " & formatField(HttpContext.Current.Request("frmTriggerTelephoneNumber"), "", "") & " " & _
               "WHERE TriggerID	        = '" & TriggerID & "' " & _
               "AND CompanyID           = '" & CompanyID & "'"
        intRowsAffected = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tbltriggers (CompanyID, TriggerUserID, TriggerName, TriggerProductType, TriggerStatusCode, TriggerSubStatusCode, TriggerEmailAddress, TriggerTelephoneNumber) VALUES (" & _
                formatField(CompanyID, "N", 0) & ", " & _
                formatField(Config.DefaultUserID, "N", 0) & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerName"), "T", "") & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerProductType"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerStatusCode"), "U", "") & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerSubStatusCode"), "U", "") & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerEmailAddress"), "", "") & ", " & _
                formatField(HttpContext.Current.Request("frmTriggerTelephoneNumber"), "", "") & ")"
            executeNonQuery(strQry)
        End If
        cacheDependency(HttpContext.Current.Cache, "tbltriggers")
    End Sub

    Private Sub saveTriggerDelete()
        Dim strQry As String = "UPDATE tbltriggers SET TriggerActive = 0 WHERE TriggerID = '" & HttpContext.Current.Request("TriggerID") & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        cacheDependency(HttpContext.Current.Cache, "tbltriggers")
    End Sub

    Public Sub saveCalendarEvent()

        Dim strQry As String = ""
        strQry = "INSERT INTO tblcalendarevents (CompanyID, UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate ,CalendarEventAllDay, CalendarEventURL, CalendarEventColour, CalendarEventApproved) " & _
                           "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                           formatField(DefaultUserID, "N", 0) & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventType"), "N", 0) & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventName"), "T", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventDescription"), "T", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventStartDate"), "DTTM", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventEndDate"), "DTTM", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventAllDay"), "B", 0) & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventURL"), "", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventColour"), "N", "") & ", " & _
                           formatField(HttpContext.Current.Request("frmCalendarEventApproved"), "B", 1) & ") "

        Dim EventID As Integer = executeIdentityQuery(strQry)

        If (HttpContext.Current.Request("frmCalendarEventType") = 1) Then
            Dim strTeamLeader As String = getAnyFieldByCompanyID("TeamLeaderUserID", "tblteams", "TeamID", objLeadPlatform.Config.DefaultUserTeamID)
            Dim arrTeamLeader As Array = Split(strTeamLeader, ",")
            For x As Integer = 0 To UBound(arrTeamLeader)
                If (checkValue(arrTeamLeader(x))) Then
                    saveMessage(2, Config.DefaultUserID, arrTeamLeader(x), "Holiday Request", " I would like to book a holiday from " & HttpContext.Current.Request("frmCalendarEventStartDate") & " until " & HttpContext.Current.Request("frmCalendarEventStartDate"), "" & EventID)
                End If
            Next
        End If

        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Event+successfully+added"

    End Sub

    Public Sub editCalendarEvent()

        Dim newStart As String = regexReplace(" GMT [0-9]{4} \(GMT [A-Z]{1,50} Time\)", "", HttpContext.Current.Request("frmeventstartdate"))
        Dim newEnd As String = regexReplace(" GMT [0-9]{4} \(GMT [A-Z]{1,50} Time\)", "", HttpContext.Current.Request("frmeventenddate"))

        Dim dateTimeStart As String = DateTime.ParseExact(newStart, "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture)
        Dim dateTimeEnd As String = DateTime.ParseExact(newEnd, "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture)

        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "UPDATE tblcalendarevents SET  " & _
                "CalendarEventStartDate	= " & formatField(dateTimeStart, "DTTM", "") & ", " & _
                "CalendarEventEndDate	= " & formatField(dateTimeEnd, "DTTM", "") & " " & _
                "WHERE CalendarEventID	    = '" & formatField(HttpContext.Current.Request("frmEventID"), "N", "") & "' " & _
                "AND CompanyID           	= '" & CompanyID & "'"


        executeNonQuery(strQry)
        strReturnUrlMessage = "1"

    End Sub

    Private Sub removeCalendarEvent()
        Dim EventID As String = HttpContext.Current.Request("frmEventID")
        Dim strQry As String = "UPDATE tblcalendarevents SET  " & _
                    "CalendarEventActive		= 0 " & _
                    "WHERE CalendarEventID      = '" & EventID & "' " & _
                    "AND CompanyID              = '" & CompanyID & "'"

        Call executeNonQuery(strQry)

    End Sub

    Private Sub saveMessageApprove()
        Dim strQry As String = "UPDATE tblmessages SET MessageApproved = 1 WHERE MessageID = '" & HttpContext.Current.Request("MessageID") & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        strQry = "UPDATE tblcalendarevents SET CalendarEventApproved = 1 WHERE CalendarEventID = '" & HttpContext.Current.Request("CalendarEventID") & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        Dim strMessage As String = getAnyField("MessageText", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim strSubject As String = getAnyField("MessageSubject", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim strFromUserID As String = getAnyField("MessageFromUserID", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim arrHolidayUserID As Array = Split(objLeadPlatform.Config.HolidayRequestUserID, ",")
        For x As Integer = 0 To UBound(arrHolidayUserID)
            If (checkValue(arrHolidayUserID(x))) Then
                saveMessage(2, strFromUserID, arrHolidayUserID(x), strSubject & " Confirmed by " & objLeadPlatform.Config.DefaultUserFullName, strMessage, "")
            End If
        Next
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Message+successfully+approved"
    End Sub

    Private Sub saveMessageDeny()
        Dim strQry As String = "UPDATE tblmessages SET MessageApproved = 1 WHERE MessageID = '" & HttpContext.Current.Request("MessageID") & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        strQry = "UPDATE tblcalendarevents SET CalendarEventApproved =0  WHERE CalendarEventID = '" & HttpContext.Current.Request("CalendarEventID") & "' AND CompanyID = '" & CompanyID & "'"
        Call executeNonQuery(strQry)
        Dim strMessage As String = getAnyField("MessageText", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim strSubject As String = getAnyField("MessageSubject", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim strFromUserID As String = getAnyField("MessageFromUserID", "tblmessages", "MessageID", HttpContext.Current.Request("MessageID"))
        Dim arrHolidayUserID As Array = Split(objLeadPlatform.Config.HolidayRequestUserID, ",")
        For x As Integer = 0 To UBound(arrHolidayUserID)
            If (checkValue(arrHolidayUserID(x))) Then
                saveMessage(2, strFromUserID, arrHolidayUserID(x), strSubject & " Declined by " & objLeadPlatform.Config.DefaultUserFullName, strMessage, "")
            End If
        Next
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Message+successfully+denied"
    End Sub

    Private Sub saveDeviceChange()
        updateUserStoreField(Config.DefaultUserID, "ClickToDialDDI", HttpContext.Current.Request("frmDevice"), "", "")
        strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Device+successfully+changed"
    End Sub

    Public Sub addRotaEvents()
        Dim strUserName As String = ""
        Dim strErrors As String = ""

        Dim MonStart As String = ""
        Dim MonEnd As String = ""
        Dim TueStart As String = ""
        Dim TueEnd As String = ""
        Dim WedStart As String = ""
        Dim WedEnd As String = ""
        Dim ThuStart As String = ""
        Dim ThuEnd As String = ""
        Dim FridStart As String = ""
        Dim FridEnd As String = ""
        Dim SatStart As String = ""
        Dim SatEnd As String = ""
        Dim strQry As String = ""

        If (checkValue(HttpContext.Current.Request("frmStartDateMon"))) Then
            MonStart = "" & HttpContext.Current.Request("frmDateMon") & " " & HttpContext.Current.Request("frmStartDateMon") & ":00"
            MonEnd = "" & HttpContext.Current.Request("frmDateMon") & " " & HttpContext.Current.Request("frmEndDateMon") & ":00"
        End If
        If (checkValue(HttpContext.Current.Request("frmStartDateTue"))) Then
            TueStart = "" & HttpContext.Current.Request("frmDateTue") & " " & HttpContext.Current.Request("frmStartDateTue") & ":00"
            TueEnd = "" & HttpContext.Current.Request("frmDateTue") & " " & HttpContext.Current.Request("frmEndDateTue") & ":00"
        End If
        If (checkValue(HttpContext.Current.Request("frmStartDateWed"))) Then
            WedStart = "" & HttpContext.Current.Request("frmDateWed") & " " & HttpContext.Current.Request("frmStartDateWed") & ":00"
            WedEnd = "" & HttpContext.Current.Request("frmDateWed") & " " & HttpContext.Current.Request("frmEndDateWed") & ":00"
        End If
        If (checkValue(HttpContext.Current.Request("frmStartDateThu"))) Then
            ThuStart = "" & HttpContext.Current.Request("frmDateThu") & " " & HttpContext.Current.Request("frmStartDateThu") & ":00"
            ThuEnd = "" & HttpContext.Current.Request("frmDateThu") & " " & HttpContext.Current.Request("frmEndDateThu") & ":00"
        End If
        If (checkValue(HttpContext.Current.Request("frmStartDateFri"))) Then
            FridStart = "" & HttpContext.Current.Request("frmDateFri") & " " & HttpContext.Current.Request("frmStartDateFri") & ":00"
            FridEnd = "" & HttpContext.Current.Request("frmDateFri") & " " & HttpContext.Current.Request("frmEndDateFri") & ":00"
        End If
        If (checkValue(HttpContext.Current.Request("frmStartDateSat"))) Then
            SatStart = "" & HttpContext.Current.Request("frmDateSat") & " " & HttpContext.Current.Request("frmStartDateSat") & ":00"
            SatEnd = "" & HttpContext.Current.Request("frmDateSat") & " " & HttpContext.Current.Request("frmEndDateSat") & ":00"
        End If

        Dim startDate As Date = DateAdd(DateInterval.WeekOfYear, 1, Config.DefaultDate)

        Dim startArray() As String = {MonStart, TueStart, WedStart, ThuStart, FridStart, SatStart}
        Dim endArray() As String = {MonEnd, TueEnd, WedEnd, ThuEnd, FridEnd, SatEnd}
        Dim counter As Integer = 0

        Dim arrUsers As Array = Split(HttpContext.Current.Request("frmCalendarusers"), ",")
        For x As Integer = 0 To UBound(arrUsers)
            counter = 0
            Dim UserSQL As String = "SELECT UserFullName FROM tblUsers WHERE UserId = " & arrUsers(x)
            Dim dsCache As DataTable = New Caching(Nothing, UserSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strUserName = Row.Item("UserFullName").ToString
                Next
            End If
            Dim strSQL As String = "SELECT CalendarEventID FROM tblcalendarevents WHERE CalendarEventType = 3 AND UserID = '" & arrUsers(x) & "' AND DATEPART(WW,CalendarEventStartDate) = " & DatePart(DateInterval.WeekOfYear, startDate) & " AND DATEPART(YY,CalendarEventStartDate) = " & DatePart(DateInterval.Year, startDate) & " AND CalendarEventActive = 1 AND CompanyID = " & CompanyID
            Dim strResult As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
            If (Not checkValue(strResult)) Then
                While counter < 6
                    If checkValue(startArray(counter)) Then
                        strQry = "INSERT INTO tblcalendarevents (CompanyID, UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate ,CalendarEventAllDay, CalendarEventURL, CalendarEventColour, CalendarEventApproved) " & _
                                           "VALUES(" & formatField(CompanyID, "N", 0) & ", " & _
                                           formatField(arrUsers(x), "N", 0) & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventType"), "N", 0) & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventName") & " - " & strUserName, "T", "") & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventDescription"), "T", "") & ", " & _
                                           formatField(startArray(counter), "DTTM", "") & ", " & _
                                           formatField(endArray(counter), "DTTM", "") & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventAllDay"), "B", 0) & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventURL"), "", "") & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventColour"), "N", "") & ", " & _
                                           formatField(HttpContext.Current.Request("frmCalendarEventApproved"), "B", 1) & ") "
                        executeNonQuery(strQry)
                    End If
                    counter += 1
                End While
                saveMessage(4, Config.DefaultUserID, arrUsers(x), "Rota Update", "Your rota for the coming week commencing " & HttpContext.Current.Request("frmDateMon") & " has been posted", "")
            Else
                If (checkValue(strErrors)) Then
                    strErrors += ", " & strUserName
                Else
                    strErrors += strUserName
                End If
            End If
        Next

        If (checkValue(strErrors)) Then
            strReturnUrlVariables = "strMessageType=warning&strMessageTitle=Warning&strMessage=Some+rotas+could+not+be+added: " & strErrors
        Else
            strReturnUrlVariables = "strMessageType=success&strMessageTitle=Success&strMessage=Rota+successfully+added"
        End If

    End Sub

    Public Sub addTemplate()

        Dim TemplateID As String = HttpContext.Current.Request("strTemplateID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "UPDATE tblWikiTemplates SET  " & _
                "WikiTemplateName	  = " & formatField(HttpContext.Current.Request("txtName"), "", "") & ", " & _
                "WikiTemplateHTML	  = " & formatField(HttpContext.Current.Request("txtHTML"), "", "") & " " & _
                "WHERE WikiTemplateID  = '" & TemplateID & "' "
        intRowsAffected = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblWikiTemplates (WikiTemplateName, WikiTemplateHTML) VALUES (" & _
            formatField(HttpContext.Current.Request("txtName"), "T", "") & ", " & _
            formatField(HttpContext.Current.Request("txtHTML"), "T", "") & ")"

            TemplateID = executeIdentityQuery(strQry)
        End If

    End Sub


    Public Sub addSection()

        Dim SectionID As String = HttpContext.Current.Request("strSectionID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "UPDATE tblWikiSections SET  " & _
                "WikiSectionName	  = " & formatField(HttpContext.Current.Request("txtSectionName"), "", "") & " " & _
                "WHERE WikiSectionID  = '" & SectionID & "' " & _
                "AND CompanyID           = '" & CompanyID & "'"

        intRowsAffected = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tblWikiSections (CompanyID,WikiSectionName) VALUES (" & _
            formatField(CompanyID, "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("txtName"), "T", "") & ")"


            SectionID = executeIdentityQuery(strQry)
        End If

    End Sub

    Public Sub addPage()

        Dim PageID As String = HttpContext.Current.Request("strPageID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
       strQry = "UPDATE tblWikiPages SET  " & _
			    "WikiPageSectionID	  = " & formatField(HttpContext.Current.Request("wikiSectiondd"), "N", 0) & ", " & _
		 		"WikiPageTemplateID	  = " & formatField(HttpContext.Current.Request("templatedd"), "N", 0) & ", " & _
                "WikiPageTitle	 	  = " & formatField(HttpContext.Current.Request("txtName"), "", "") & ", " & _
				"WikiPageUpdatedUserID = " & formatField(config.DefaultUserID, "N", 0) & ", " & _
                "WikiPageContent	  = " & formatField(HttpContext.Current.Request("txtScriptText"), "", "") & ", " & _
				"WikiPageLive	 	  = " & formatField(HttpContext.Current.Request("livedd"), "N", 0) & ", " & _
				"WikiPageStartDate	  = " & formatField(HttpContext.Current.Request("frmPageStartDate"), "DTTM", "") & ", " & _
				"WikiPageEndDate	  = " & formatField(HttpContext.Current.Request("frmPageEndDate"), "DTTM", "") & " " & _ 				
                "WHERE WikiPageID  = '" & PageID & "' "			
			
				
        intRowsAffected = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then		 
            strQry = "INSERT INTO tblWikiPages (CompanyID, WikiPageSectionID,WikiPageTemplateID,WikiPageTitle,WikiPageCreatedUserID,WikiPageContent,WikiPageLive,WikiPageStartDate,WikiPageEndDate) VALUES (" & _
			formatField(CompanyID, "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("wikiSectiondd"), "N", 0) & ", " & _
			formatField(HttpContext.Current.Request("templatedd"), "N", 0) & ", " & _
			formatField(HttpContext.Current.Request("txtName"), "T", "") & ", " & _
			formatField(config.DefaultUserID, "N", 0) & ", " & _
			formatField(HttpContext.Current.Request("txtScriptText"), "T", "") & ", " & _
			formatField(HttpContext.Current.Request("livedd"), "N", 0) & ", " & _  
			formatField(HttpContext.Current.Request("frmPageStartDate"), "DTTM", "") & ", " & _			
            formatField(HttpContext.Current.Request("frmPageEndDate"), "DTTM", "") & ")"
			
            PageID = executeIdentityQuery(strQry)
        End If

        Dim strSQL As String = "DELETE FROM tblWikiPageImages WHERE WikiPageID = " & PageID
        executeNonQuery(strSQL)

        Dim strQry2 As String = "INSERT INTO tblWikiPageImages (CompanyID,WikiPageID,WikiPageImage) VALUES (" & _
        						formatField(CompanyID, "N", 0) & ", " & _
                  				formatField(PageID, "N", 0) & ", " & _
        						formatField(HttpContext.Current.Request("WikPageImagedd"), "", "") & ")"

        executeNonQuery(strQry2)
        cacheDependency(HttpContext.Current.Cache, "vwwiki")
    End Sub

    Private Sub savePageActive()
        Dim PageID As String = HttpContext.Current.Request("strPageID")
        Dim PageActive As Boolean = HttpContext.Current.Request("PageActive")
        Dim strQry As String = ""
        If (checkValue(PageID)) Then
            If (PageActive) Then
                strQry = "UPDATE tblWikiPages SET  " & _
                    "WikiPageActive		= 0 " & _
                    "WHERE WikiPageID      = '" & PageID & "' " & _
                    "AND CompanyID              = '" & CompanyID & "'"
            Else
                strQry = "UPDATE tblWikiPages SET  " & _
                    "WikiPageActive		= 1 " & _
                    "WHERE WikiPageID	    = '" & PageID & "' " & _
                    "AND CompanyID              = '" & CompanyID & "'"
            End If
            Call executeNonQuery(strQry)
        End If
    End Sub

    Public Sub addSecuredPolicy()
        Dim ProductID As String = HttpContext.Current.Request("strProductID")
        Dim strQry As String = ""
        Dim intRowsAffected As Integer = 0
        strQry = "UPDATE tblsecuredpolicys SET  " & _
             "PolicyName	  = " & formatField(HttpContext.Current.Request("txtName"), "", "") & ", " & _
           "Lender	  = " & formatField(HttpContext.Current.Request("txtLender"), "", "") & ", " & _
                      "AnnualRate	 	  = " & formatField(HttpContext.Current.Request("txtAnnualRate"), "", "") & ", " & _
          "MinNetLoan = " & formatField(HttpContext.Current.Request("txtMinAmount"), "N", 0) & ", " & _
                      "MaxNetLoan	  = " & formatField(HttpContext.Current.Request("txtMaxAmount"), "N", 0) & ", " & _
          "MinTerm	 	  = " & formatField(HttpContext.Current.Request("txtMinTerm"), "N", 0) & ", " & _
          "MaxTerm	  = " & formatField(HttpContext.Current.Request("txtMaxTerm"), "N", 0) & ", " & _
          "MaxCCJs	  = " & formatField(HttpContext.Current.Request("txtMaxCCJ"), "N", 0) & ", " & _
          "MaxDefaults = " & formatField(HttpContext.Current.Request("txtMaxDefaults"), "N", 0) & ", " & _
           "SecuredPaymentsMissed	  = " & formatField(HttpContext.Current.Request("txtMaxSecuredMissed"), "N", 0) & ", " & _
                      "MinPropertyValue	 	  = " & formatField(HttpContext.Current.Request("txtMinPropertyVal"), "N", 0) & ", " & _
          "MaximumAdverseCredit = " & formatField(HttpContext.Current.Request("txtMaxAdverseCredit"), "N", 0) & ", " & _
                      "MaximumIndividualCCJDefaultAmount	  = " & formatField(HttpContext.Current.Request("txtMaxCCJDefaultAmount"), "N", 0) & ", " & _
          "DTIR	 	  = " & formatField(HttpContext.Current.Request("txtDTIR"), "N", 0) & ", " & _
          "MinimumIncome	  = " & formatField(HttpContext.Current.Request("txtMinIncome"), "N", 0) & ", " & _
          "SecuredMissedPayments3Months	  = " & formatField(HttpContext.Current.Request("txtSecuredMissed3Months"), "N", 0) & ", " & _
          "LenderFee	  = " & formatField(HttpContext.Current.Request("txtMaxLenderFeeVal"), "N", 0) & ", " & _
           "InterestOnlyMaxTerm	  = " & formatField(HttpContext.Current.Request("txtInterestOnlyMaxTerm"), "N", 0) & ", " & _
                      "MinimumRentalCover	 	  = " & formatField(HttpContext.Current.Request("txtMinRentalCover"), "N", 0) & ", " & _
          "ESC = " & formatField(HttpContext.Current.Request("txtESC"), "", "") & ", " & _
                      "TimeAtAddressMonths	  = " & formatField(HttpContext.Current.Request("txtTimeAtAddressMonths"), "N", 0) & ", " & _
          "MaxApplicants	 	  = " & formatField(HttpContext.Current.Request("txtMaxApplicants"), "N", 0) & ", " & _
          "MinimumAge	  = " & formatField(HttpContext.Current.Request("txtMinAge"), "N", 0) & ", " & _
          "MaximumAge	  = " & formatField(HttpContext.Current.Request("txtMaxAge"), "N", 0) & ", " & _
          "MaxAgeAtEndOfTerm	 	  = " & formatField(HttpContext.Current.Request("txtMaxAgeEOTerm"), "N", 0) & ", " & _
          "AdditionalEmployedIncome	  = " & formatField(HttpContext.Current.Request("txtAdditionalEmpInc"), "N", 0) & ", " & _
          "MinCreditScore	  = " & formatField(HttpContext.Current.Request("txtMinCredScore"), "N", 0) & ", " & _
          "AddressHistoryTotalMonths	  = " & formatField(HttpContext.Current.Request("txtAddressHistTotMonths"), "N", 0) & ", " & _
           "MinTimeInEmploymentMonths	  = " & formatField(HttpContext.Current.Request("txtMinTimeEmployment"), "N", 0) & ", " & _
                      "MinTimeSelfEmployed	 	  = " & formatField(HttpContext.Current.Request("txtMinTimeSelfEmployed"), "N", 0) & ", " & _
          "Valuation = " & formatField(HttpContext.Current.Request("txtValuation"), "", "") & ", " & _
                      "NewBuild	  = " & formatField(HttpContext.Current.Request("txtNewBuild"), "", "") & ", " & _
          "MortgageArreasLast12	 	  = " & formatField(HttpContext.Current.Request("txtMortArreas12"), "N", 0) & ", " & _
          "MortgageArreasLast6	  = " & formatField(HttpContext.Current.Request("txtMortArreas6"), "N", 0) & ", " & _
          "MortgageArreasLast3	  = " & formatField(HttpContext.Current.Request("txtMortArreas3"), "N", 0) & ", " & _
          "CommisionPercentage	  = " & formatField(HttpContext.Current.Request("txtCommisionPercentage"), "N", 0) & ", " & _
          "MaxBrokerFee	 	  = " & formatField(HttpContext.Current.Request("txtMaxBrokerFeePerc"), "N", 0) & ", " & _
          "LenderAdministrationfee	  = " & formatField(HttpContext.Current.Request("txtLenderAdminFee"), "N", 0) & ", " & _
          "ValuationMin	  = " & formatField(HttpContext.Current.Request("txtMinValuation"), "N", 0) & ", " & _
          "ValuationMax	  = " & formatField(HttpContext.Current.Request("txtMaxValuation"), "N", 0) & ", " & _
           "MinMortgageHistory	  = " & formatField(HttpContext.Current.Request("txtMinMortHist"), "N", 0) & ", " & _
                      "BTLRentalIncomePercentage	 	  = " & formatField(HttpContext.Current.Request("txtBTLRentIncPerc"), "N", 0) & ", " & _
          "PortfolioOfBTL = " & formatField(HttpContext.Current.Request("txtPortfolioOfBTL"), "N", 0) & ", " & _
                      "ExCouncil	  = " & formatField(HttpContext.Current.Request("txtExCouncil"), "", "") & ", " & _
          "PrivateLHFlat	 	  = " & formatField(HttpContext.Current.Request("txtPLHFlat"), "", "") & ", " & _
          "MaxFlatStories	  = " & formatField(HttpContext.Current.Request("txtFaltStories"), "N", 0) & ", " & _
          "DriveByValuation	  = " & formatField(HttpContext.Current.Request("txtDriveByVal"), "", "") & ", " & _
          "AVMHometrack	  = " & formatField(HttpContext.Current.Request("txtHomeTrackAVM"), "", "") & " " & _
                      "WHERE securedpolicyID  = '" & ProductID & "' "



        intRowsAffected = executeRowsAffectedQuery(strQry)
        If (intRowsAffected = 0) Then

            strQry = "INSERT INTO tblsecuredpolicys (PolicyName, Lender,AnnualRate,MinNetLoan,MaxNetLoan,MinTerm,MaxTerm,MaxCCJs,MaxDefaults,SecuredPaymentsMissed,MinPropertyValue,MaximumAdverseCredit," & _
            "MaximumIndividualCCJDefaultAmount,DTIR,MinimumIncome,SecuredMissedPayments3Months,LenderFee,InterestOnlyMaxTerm,MinimumRentalCover,ESC,TimeAtAddressMonths,MaxApplicants,MinimumAge,MaximumAge," & _
            "MaxAgeAtEndOfTerm,AdditionalEmployedIncome,MinCreditScore,AddressHistoryTotalMonths,MinTimeInEmploymentMonths,MinTimeSelfEmployed,Valuation,NewBuild,MortgageArreasLast12,MortgageArreasLast6,MortgageArreasLast3, " & _
            "CommisionPercentage,MaxBrokerFee,LenderAdministrationfee,ValuationMin,ValuationMax,MinMortgageHistory,BTLRentalIncomePercentage,PortfolioOfBTL,ExCouncil,PrivateLHFlat,MaxFlatStories,DriveByValuation,AVMHometrack) VALUES (" & _
           formatField(HttpContext.Current.Request("txtName"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtLender"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtAnnualRate"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtMinAmount"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxAmount"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMinTerm"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxTerm"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxCCJ"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxDefaults"), "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("txtMaxSecuredMissed"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtMinPropertyVal"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxAdverseCredit"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtMaxCCJDefaultAmount"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtDTIR"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMinIncome"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtSecuredMissed3Months"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxLenderFeeVal"), "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("txtInterestOnlyMaxTerm"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtMinRentalCover"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtESC"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtTimeAtAddressMonths"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxApplicants"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMinAge"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxAge"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxAgeEOTerm"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtAdditionalEmpInc"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMinCredScore"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtAddressHistTotMonths"), "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("txtMinTimeEmployment"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtMinTimeSelfEmployed"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtValuation"), "", "") & ", " & _
                    formatField(HttpContext.Current.Request("txtNewBuild"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtMortArreas12"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMortArreas6"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMortArreas3"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtCommisionPercentage"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxBrokerFeePerc"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtLenderAdminFee"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMinValuation"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtMaxValuation"), "N", 0) & ", " & _
            formatField(HttpContext.Current.Request("txtMinMortHist"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtBTLRentIncPerc"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtPortfolioOfBTL"), "N", 0) & ", " & _
                    formatField(HttpContext.Current.Request("txtExCouncil"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtPLHFlat"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtFaltStories"), "N", 0) & ", " & _
           formatField(HttpContext.Current.Request("txtDriveByVal"), "", "") & ", " & _
           formatField(HttpContext.Current.Request("txtHomeTrackAVM"), "", "") & ")"



            ProductID = executeIdentityQuery(strQry)

        End If




    End Sub




End Class