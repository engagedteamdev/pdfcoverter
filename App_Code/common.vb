﻿Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Net
Imports System.IO
Imports Config
Imports System.Xml
Imports Certificate
Imports System.Diagnostics
Imports System.Security.Cryptography

' ** Revision history **
'
' 01/02/2012    - Open hours widened in calcWorkingHour 09:00 - 20:00 Mon - Fri. Consider adding these to the database
' 23/02/2012    - New date tags added
' 27/02/2012    - NULL value check added to regexTest
' 01/03/2012    - Added space replacement and MID functionality to replaceTags()
' 05/03/2012    - New replacement dates added for future dates
' 19/03/2012    - Reworked titleLookup
' 30/03/2012    - Added getAnyFieldFromUserStore function
' 18/04/2012    - {FirstLastMonth} and {EndLastMonth} added to date replacements
' ** End Revision History **

Public Class Common

    Inherits System.Web.UI.Page

    Public Shared strBodyOnLoad As String = ""
    Public Shared strBodyOnFocus As String = ""
    Public Shared strBodyResize As String = ""
    Public Shared strBodyStyle As String = ""
    Public Shared strBodyTag As String = ""

    Public Enum CloneType
        Lookup = 1
        Maintain = 2
        Free = 3
    End Enum

    Public Enum OutOfOfficeStatus
        Ready = 1
        Break = 2
        Lunch = 3
        Meeting = 4
        NotReady = 5
        Away = 6
        Absent = 7
        Holiday = 8
    End Enum

    Shared Function getOutOfOfficeStatus(objLeadPlatform As LeadPlatform) As String
        Select Case objLeadPlatform.Config.UserOutOfOfficeID
            Case OutOfOfficeStatus.Absent
                Return "Absent"
            Case OutOfOfficeStatus.Away
                Return "Away"
            Case OutOfOfficeStatus.Break
                Return "Break"
            Case OutOfOfficeStatus.Holiday
                Return "Holiday"
            Case OutOfOfficeStatus.Lunch
                Return "Lunch"
            Case OutOfOfficeStatus.Meeting
                Return "Meeting"
            Case OutOfOfficeStatus.NotReady
                Return "Not Ready"
            Case OutOfOfficeStatus.Ready
                Return "Ready"
            Case Else
                Return "Ready"
        End Select
    End Function

    Shared Function getAvatarImage(objLeadPlatform As LeadPlatform) As String
        Dim strPath As String = "/img/users/" & LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-")) & "/" & objLeadPlatform.Config.DefaultUserName & ".jpg"
        If (objLeadPlatform.Config.SuperAdmin) Then
            strPath = "/img/users/" & objLeadPlatform.Config.DefaultUserName & ".jpg"
        End If
        If (File.Exists(HttpContext.Current.Server.MapPath(strPath))) Then
            Return strPath
        Else
            Return "/img/avatar.jpg"
        End If
    End Function

    Shared Function checkValue(ByVal val As String) As Boolean
        If (Not IsDBNull(val) And val <> "") Then
            Return True
        Else
            Return False
        End If
    End Function

    Shared Sub responseWrite(ByVal val As String)
        HttpContext.Current.Response.Write(val)
    End Sub

    Shared Sub responseEnd()
        HttpContext.Current.Response.End()
    End Sub

    Shared Sub responseRedirect(ByVal url As String)
        HttpContext.Current.Response.Redirect(url)
    End Sub

    Shared Sub setPageCache(ByVal strBuffer As Boolean)
        ''*** Buffer Settings***'
        If (checkValue(strBuffer)) Then
            HttpContext.Current.Response.Buffer = True
        Else
            HttpContext.Current.Response.Buffer = False
        End If
        '*** Cache Settings***'
        HttpContext.Current.Response.Expires = -1
        HttpContext.Current.Response.ExpiresAbsolute = DateAdd(DateInterval.Day, -1, Now)
        HttpContext.Current.Response.AddHeader("pragma", "no-cache")
        HttpContext.Current.Response.AddHeader("cache-control", "private")
        HttpContext.Current.Response.CacheControl = "no-cache"
    End Sub

    Shared Function simpleHeader(ByVal strCompany As String, ByVal strBuffer As Boolean, ByVal strTitle As String, ByVal strOnLoad As String, ByVal strOnFocus As String, ByVal strResize As String, ByVal strStyle As String, Optional ByVal jQuery As Boolean = False, Optional ByVal menu As Boolean = False, Optional ByVal fixedHeader As Boolean = False, Optional ByVal javascript As String = "") As String
        Return "" ' Not in use
    End Function

    Shared Function bannerHeader(ByVal banner As Boolean, ByVal menu As Boolean) As String
        Return "" ' Not in use
    End Function

    Shared Function mainMenu() As String
        Dim objStringBuilder As New StringBuilder
        'objStringBuilder.Append("<div id=""menuContainer"">")
        Dim strLoggedInUser As String = ""
        Dim strSQL As String = "SELECT  ParentMenuID, ParentMenuName, ParentMenuURL, ParentMenuFunction, SubMenuID, SubMenuName, SubMenuURL, SubMenuFunction, SubSubMenuID, SubSubMenuName, SubSubMenuURL, SubSubMenuFunction, ParentMenuIcon, SubMenuIcon, SubSubMenuIcon FROM vwmenu WHERE "
        'If (loggedInUserValue("UserSuperAdmin") <> "True") Then
        strSQL += "(ParentMenuAdminLevel = ''"
        Dim arrAdmin As ArrayList = New ArrayList
        With arrAdmin
            .Add("UserReports")
            .Add("UserStaffAdmin")
            .Add("UserDataAdmin")
            .Add("UserMediaAdmin")
            .Add("UserApplicationAdmin")
            .Add("UserDialerAdmin")
            .Add("UserSuperAdmin")
            .Add("UserCallCentreAdmin")
            .Add("UserWorkflows")
            .Add("UserManager")
            .Add("UserSeniorManager")
            .Add("UserCallCentre")
            .Add("UserSales")
            .Add("UserAdministrator")
            .Add("UserRep")
            .Add("UserSupplierCompanyID")
            .Add("UserPartnerCompanyID")
            .Add("UserClickToDial")
        End With
        'Dim arrAdmin2 As String() = {"UserReports", "UserStaffAdmin", "UserDataAdmin", "UserMediaAdmin", "UserApplicationAdmin", "UserDialerAdmin", "UserSuperAdmin", "UserCallCentreAdmin", "UserWorkflows", "UserManager", "UserSeniorManager", "UserCallCentre", "UserSales", "UserAdministrator", "UserRep", "UserSupplierCompanyID", "UserPartnerCompanyID", "UserClickToDial"}
        arrAdmin = getUserAccessLevels(Config.DefaultUserID, arrAdmin)
        For Each Item As String In arrAdmin
            If (Left(Item, 4) <> "User") Then
                strLoggedInUser = 1
            Else
                strLoggedInUser = loggedInUserValue(Item)
            End If
            If IsNumeric(strLoggedInUser) Then
                If strLoggedInUser > 0 Then
                    strLoggedInUser = "True"
                Else
                    strLoggedInUser = "False"
                End If
            End If
            If (strLoggedInUser = "True") Then
                strSQL += " OR ParentMenuAdminLevel LIKE '%|" & Replace(Item, "CompanyID", "") & "|%'"
            End If
        Next
        strSQL += ") "
        strSQL += "AND (SubMenuAdminLevel = '' OR SubMenuAdminLevel IS NULL "
        For Each Item As String In arrAdmin
            If (Left(Item, 4) <> "User") Then
                strLoggedInUser = 1
            Else
                strLoggedInUser = loggedInUserValue(Item)
            End If
            If IsNumeric(strLoggedInUser) Then
                If strLoggedInUser > 0 Then
                    strLoggedInUser = "True"
                Else
                    strLoggedInUser = "False"
                End If
            End If
            If (strLoggedInUser = "True") Then
                strSQL += " OR SubMenuAdminLevel LIKE '%|" & Replace(Item, "CompanyID", "") & "|%'"
            End If
        Next
        strSQL += ") "
        strSQL += "AND (SubSubMenuAdminLevel = '' OR SubSubMenuAdminLevel IS NULL "
        For Each Item As String In arrAdmin
            If (Left(Item, 4) <> "User") Then
                strLoggedInUser = 1
            Else
                strLoggedInUser = loggedInUserValue(Item)
            End If
            If IsNumeric(strLoggedInUser) Then
                If strLoggedInUser > 0 Then
                    strLoggedInUser = "True"
                Else
                    strLoggedInUser = "False"
                End If
            End If
            If (strLoggedInUser = "True") Then
                strSQL += " OR SubSubMenuAdminLevel LIKE '%|" & Replace(Item, "CompanyID", "") & "|%'"
            End If
        Next
        strSQL += ") AND "
        'End If
        strSQL += "(ParentMenuCompanyID = @CompanyID OR ParentMenuCompanyID = 0) " & _
            "AND (ISNULL(SubMenuCompanyID, 0) = @CompanyID OR ISNULL(SubMenuCompanyID, 0) = 0) " & _
            "AND (ISNULL(SubSubMenuCompanyID, 0) = @CompanyID OR ISNULL(SubSubMenuCompanyID, 0) = 0) " & _
            "AND (ParentMenuName <> 'Home') " & _
            "ORDER BY ParentMenuOrder, SubMenuOrder, SubSubMenuOrder"

        'responseWrite(strSQL)

        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
        Dim dsMenu As DataTable = New Caching(HttpContext.Current.Cache, strSQL, "", "", "", CommandType.Text, arrParams).returnCache
        Dim intPreviousParentMenuID As Integer = 0
        Dim intPreviousSubMenuID As Integer = 0
        Dim intPreviousSubSubMenuID As Integer = 0
        Dim x As Integer = 0, strClass As String = ""
        Dim strParentMenuNameOnClick As String = ""
        Dim strSubMenuNameOnClick As String = ""
        Dim strSubSubMenuNameOnClick As String = ""
        Dim strTemp As String = ""

        If (dsMenu.Rows.Count > 0) Then
            If (x = 0) Then strClass = " first"
            objStringBuilder.Append("<ul class=""nav"">" & vbCrLf)

            objStringBuilder.Append("<li class=""nav-icon active""><a href=""/""><i class=""icon-home""></i><span>Home</span></a></li>")

            For Each Row As DataRow In dsMenu.Rows
                strParentMenuNameOnClick = ""
                strSubMenuNameOnClick = ""
                strSubSubMenuNameOnClick = ""
                If checkValue(Row.Item("ParentMenuFunction").ToString) Then
                    strParentMenuNameOnClick = " onclick=""" & Row.Item("ParentMenuFunction") & """"
                End If
                If checkValue(Row.Item("SubMenuFunction").ToString) Then
                    strSubMenuNameOnClick = " onclick=""" & Row.Item("SubMenuFunction") & """"
                End If
                If checkValue(Row.Item("SubSubMenuFunction").ToString) Then
                    strSubSubMenuNameOnClick = " onclick=""" & Row.Item("SubSubMenuFunction") & """"
                End If

                If (Row.Item("ParentMenuID") <> intPreviousParentMenuID) Then
                    If (intPreviousParentMenuID > 0) Then
                        If (intPreviousSubMenuID > 0) Then ' Finish Sub Menu
                            objStringBuilder.Append("</ul>" & vbCrLf)
                            objStringBuilder.Append("</li>" & vbCrLf) ' Finish Parent Menu
                        End If
                    End If
                    ' Start Parent Menu
                    objStringBuilder.Append("   <li class=""dropdown"">")
                    If (Row.Item("SubMenuID") > 0) Then ' Start Sub Menu

                        If (checkValue(Row.Item("ParentMenuName"))) Then
                            objStringBuilder.Append("<a href=""" & Replace(Row.Item("ParentMenuURL"), "/net", "") & """ class=""dropdown-toggle"" data-toggle=""dropdown"" " & strParentMenuNameOnClick & "><i class=""" & Row.Item("ParentMenuIcon") & """></i>" & Row.Item("ParentMenuName") & "<b class=""caret""></b></a>")
                        Else
                            objStringBuilder.Append("<a href=""" & Replace(Row.Item("ParentMenuURL"), "/net", "") & """ class=""dropdown-toggle"" data-toggle=""dropdown"" " & strParentMenuNameOnClick & "><i class=""" & Row.Item("ParentMenuIcon") & """></i><b class=""caret""></b></a>")
                        End If
                        objStringBuilder.Append("<ul class=""dropdown-menu"">")
                        If (Row.Item("SubMenuName") = "Workflows") Then
                            strSQL = "SELECT WorkflowID, WorkflowShortName, WF.WorkflowTypeID, WorkflowTypeDescription FROM tblworkflows WF " & _
                                 "INNER JOIN tblworkflowmappings MAP ON MAP.WorkflowMappingWorkflowID = WF.WorkflowID " & _
                                 "INNER JOIN tblworkflowtypes TYP ON TYP.WorkflowTypeID = WF.WorkflowTypeID " & _
                                 "WHERE MAP.WorkflowMappingUserID = @UserID AND WF.WorkflowActive = 1 AND WF.CompanyID = @CompanyID"
                            If (Not checkValue(loggedInUserValue("UserManager"))) Then
                                strSQL += " AND WorkflowRota = 0"
                            End If
                            strSQL += " ORDER BY WorkflowTypeDescription, WorkflowShortName "
                            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                                New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, loggedInUserValue("UserID"))}
                            Dim dsWorkflows As DataTable = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams2).returnCache
                            If (dsWorkflows.Rows.Count > 0) Then
                                Dim intPreviousWorkflowTypeID As Integer = 0, intPreviousWorkflowID As Integer = 0
                                For Each InnerRow As DataRow In dsWorkflows.Rows

                                    If (intPreviousWorkflowTypeID > 0 And InnerRow.Item("WorkflowTypeID") <> intPreviousWorkflowTypeID) Then
                                        objStringBuilder.Append("</ul></li>")
                                    End If

                                    If (intPreviousWorkflowTypeID <> InnerRow.Item("WorkflowTypeID")) Then
                                        If (InnerRow.Item("WorkflowID") > 0) Then
                                            objStringBuilder.Append("   <li class=""dropdown""><a href=""/workflow/workflow.aspx?frmWorkflowTypeID=" & InnerRow.Item("WorkflowTypeID") & """><i class=""icon-random""></i>" & InnerRow.Item("WorkflowTypeDescription") & "<i class=""icon-chevron-right sub-menu-caret""></i></a>")
                                        Else
                                            objStringBuilder.Append("   <li class=""dropdown""><a href=""/workflow/workflow.aspx?frmWorkflowTypeID=" & InnerRow.Item("WorkflowTypeID") & """><i class=""icon-random""></i>" & InnerRow.Item("WorkflowTypeDescription") & "<i class=""icon-chevron-right sub-menu-caret""></i></a></li>")
                                        End If
                                    End If

                                    If (InnerRow.Item("WorkflowTypeID") <> intPreviousWorkflowTypeID) Then
                                        objStringBuilder.Append("<ul class=""dropdown-menu sub-menu"">")
                                    End If
                                    objStringBuilder.Append("<li><a href=""/workflow/workflow.aspx?frmWorkflowID=" & InnerRow.Item("WorkflowID") & "&frmWorkflowTypeID=" & InnerRow.Item("WorkflowTypeID") & """ " & strSubMenuNameOnClick & ">" & InnerRow.Item("WorkflowShortName") & "</a></li>" & vbCrLf)

                                    intPreviousWorkflowTypeID = InnerRow.Item("WorkflowTypeID")
                                    intPreviousWorkflowID = InnerRow.Item("WorkflowID")
                                Next

                                If (intPreviousWorkflowTypeID > 0) Then ' Finish Type Menu
                                    objStringBuilder.Append("</ul>" & vbCrLf)
                                End If

                            End If
                            dsWorkflows = Nothing
                        ElseIf (Row.Item("SubMenuName") = "Companies" And getAnyField("UserName", "tblusers", "UserID", DefaultUserID) <> "system") Then
                            strSQL = "SELECT MediaName, CompanyID FROM tblmedia WHERE (MediaID = CompanyID) AND (MediaActive = 1) ORDER BY MediaName"
                            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
                            If (dsCache.Rows.Count > 0) Then
                                For Each InnerRow As DataRow In dsCache.Rows
                                    objStringBuilder.Append("   <li><a href=""/inc/processlogon.aspx?txtCompanyID=" & InnerRow.Item("CompanyID") & """><i class=""icon-globe""></i>" & InnerRow.Item("MediaName") & "</a>")
                                Next
                            End If
                            dsCache = Nothing
                        ElseIf (Row.Item("SubMenuName") = "Report History") Then
                            strSQL = "SELECT TOP 10 ReportLogQueryString, ReportLogDate FROM tblreportlogs WHERE ReportLogUserID = '" & Config.DefaultUserID & "' AND CompanyID = '" & CompanyID & "' ORDER BY ReportLogDate DESC"
                            Dim dsReportLogs As DataTable = New Caching(HttpContext.Current.Cache, strSQL, "", "", "", ).returnCache

                            objStringBuilder.Append("<li class=""dropdown""><a href=""" & Replace(Row.Item("SubMenuURL"), "/net", "") & """ " & strSubMenuNameOnClick & "><i class=""icon-history""></i>" & Row.Item("SubMenuName") & "<i class=""icon-chevron-right sub-menu-caret""></i></a>")

                            If (dsReportLogs.Rows.Count > 0) Then
                                objStringBuilder.Append("<ul class=""dropdown-menu sub-menu"">" & vbCrLf)
                                For Each InnerRow As DataRow In dsReportLogs.Rows
                                    objStringBuilder.Append("<li><a href=""/reports.aspx?" & InnerRow.Item("ReportLogQueryString") & "&frmNoLogging=Y"">" & ddmmyyhhmmss2ddmmhhmm(InnerRow.Item("ReportLogDate")) & " - " & formatReportName(InnerRow.Item("ReportLogQueryString")) & "</a></li>" & vbCrLf)
                                Next
                                objStringBuilder.Append("</ul>" & vbCrLf)
                            End If
                            dsReportLogs = Nothing
                        Else
                            If (Row.Item("SubSubMenuID") > 0) Then
                                objStringBuilder.Append("   <li class=""dropdown""><a href=""" & menuTags(Replace(Row.Item("SubMenuURL"), "/net", "")) & """ " & strSubMenuNameOnClick & "><i class=""" & Row.Item("SubMenuIcon") & """></i>" & Row.Item("SubMenuName") & "<i class=""icon-chevron-right sub-menu-caret""></i></a>")
                            Else
                                objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubMenuURL"), "/net", "")) & """ " & strSubMenuNameOnClick & "><i class=""" & Row.Item("SubMenuIcon") & """></i>" & Row.Item("SubMenuName") & "</a></li>")
                            End If
                        End If
                        If (Row.Item("SubMenuID") <> intPreviousSubMenuID) Then
                            If (intPreviousSubMenuID > 0) Then
                                If (intPreviousSubSubMenuID > 0) Then ' Finish Sub Sub Menu
                                    objStringBuilder.Append("</ul>" & vbCrLf)
                                End If
                            End If
                            If (Row.Item("SubSubMenuID") > 0) Then ' Start Sub Sub Menu
                                objStringBuilder.Append("<ul class=""dropdown-menu sub-menu"">")
                                objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubSubMenuURL"), "/net", "")) & """ " & strSubSubMenuNameOnClick & ">" & Row.Item("SubSubMenuName") & "</a></li>" & vbCrLf)
                            End If
                        Else
                            objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubSubParentMenuURL"), "/net", "")) & """ " & strSubSubMenuNameOnClick & ">" & Row.Item("SubSubMenuName") & "</a></li>" & vbCrLf)
                        End If
                    Else
                        If (checkValue(Row.Item("ParentMenuName"))) Then
                            objStringBuilder.Append("<a href=""" & Replace(Row.Item("ParentMenuURL"), "/net", "") & """ " & strParentMenuNameOnClick & "><i class=""" & Row.Item("ParentMenuIcon") & """></i>" & Row.Item("ParentMenuName") & "</a>")
                        Else
                            objStringBuilder.Append("<a href=""" & Replace(Row.Item("ParentMenuURL"), "/net", "") & """ " & strParentMenuNameOnClick & "><i class=""" & Row.Item("ParentMenuIcon") & """></i></a>")
                        End If
                        objStringBuilder.Append("</li>" & vbCrLf) ' Finish Parent Menu
                    End If
                Else
                    If (Row.Item("SubMenuID") <> intPreviousSubMenuID) Then
                        If (intPreviousSubMenuID > 0) Then
                            If (intPreviousSubSubMenuID > 0) Then ' Finish Sub Menu
                                objStringBuilder.Append("</ul>" & vbCrLf)
                                'If (intPreviousSubMenuID > 0) Then ' Finish Sub Menu
                                objStringBuilder.Append("</li>")
                                'End If
                            End If
                        End If
                        If (Row.Item("SubSubMenuID") > 0) Then
                            objStringBuilder.Append("   <li class=""dropdown""><a href=""" & menuTags(Replace(Row.Item("SubMenuURL"), "/net", "")) & """ " & strSubMenuNameOnClick & "><i class=""" & Row.Item("SubMenuIcon") & """></i>" & Row.Item("SubMenuName") & "<i class=""icon-chevron-right sub-menu-caret""></i></a>")
                        Else
                            objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubMenuURL"), "/net", "")) & """ " & strSubMenuNameOnClick & "><i class=""" & Row.Item("SubMenuIcon") & """></i>" & Row.Item("SubMenuName") & "</a>")
                        End If
                        If (Row.Item("SubSubMenuID") > 0) Then ' Start Sub Sub Menu
                            objStringBuilder.Append("<ul class=""dropdown-menu sub-menu"">")
                            objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubSubMenuURL"), "/net", "")) & """ " & strSubSubMenuNameOnClick & ">" & Row.Item("SubSubMenuName") & "</a></li>" & vbCrLf)
                        End If
                    Else
                        objStringBuilder.Append("   <li><a href=""" & menuTags(Replace(Row.Item("SubSubMenuURL"), "/net", "")) & """ " & strSubSubMenuNameOnClick & ">" & Row.Item("SubSubMenuName") & "</a></li>" & vbCrLf)

                    End If
                End If
                intPreviousParentMenuID = Row.Item("ParentMenuID")
                intPreviousSubMenuID = Row.Item("SubMenuID")
                intPreviousSubSubMenuID = Row.Item("SubSubMenuID")
                x += 1
                strClass = ""
            Next

            If (intPreviousSubSubMenuID > 0) Then ' Finish Sub Sub Menu
                objStringBuilder.Append("</ul>" & vbCrLf)
            End If

            If (intPreviousSubMenuID > 0) Then ' Finish Sub Menu
                objStringBuilder.Append("</ul>" & vbCrLf)
            End If

            objStringBuilder.Append("</ul>" & vbCrLf)

        End If
        dsMenu = Nothing
        objDatabase = Nothing
        'objStringBuilder.Append("</div>")
        Return objStringBuilder.ToString
    End Function

    Shared Function callOptions(ByVal AppID As String) As String
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .AppendLine("<ul class=""nav"">")
			.AppendLine("<li class=""nav-icon active""><a href=""#""><i class=""icon-phone""></i><span></span></a></li>") 
            .AppendLine("<li class=""dropdown"">")			
            .AppendLine("<a href=""javascript:;"" class=""dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-comment""></i>Dispositions<b class=""caret""></b></a>")
            .AppendLine("<ul class=""dropdown-menu"">")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','ENG',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Engaged');"">Busy/Engaged</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','MSG',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Message Left');"">Message Left</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','ANS',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Answerphone');"">Answerphone</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','NOA',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': No Answer');"">No Answer</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','DUN',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Destination Unavailable');"">Destination Unavailable</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#AppID').text(),'','WRN',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Wrong Number');"">Wrong Number</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Invalid', '/prompts/endcall.aspx?AppID=" & AppID & "&StatusCode=INV','400','350');"">Invalid</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','We Turned Down', '/prompts/endcall.aspx?AppID=" & AppID & "&StatusCode=WTD','400','350');"">We Turned Down</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Turned Us Down', '/prompts/endcall.aspx?AppID=" & AppID & "&StatusCode=TUD','400','350');"">Turned Us Down</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""$('#cbk-prompt').click();return false;"">Call Back</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""cancelCallBack(" & AppID & ")"">Cancel Call Back</a></li>")
            .AppendLine("</ul>")
            .AppendLine("</li>")
            .AppendLine("<li class=""dropdown"">")
            .AppendLine("<a href=""javascript:;"" class=""dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-envelope""></i>Contact<b class=""caret""></b></a>")
            .AppendLine("<ul class=""dropdown-menu"">")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Create Letter', '/prompts/letters.aspx?AppID=" & AppID & "','600', '600');"">Letter</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Create Pack', '/prompts/packs.aspx?AppID=" & AppID & "','600', '600');"">Packs</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Document History', '/prompts/letterhistory.aspx?AppID=" & AppID & "','800', '400');"">History</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Upload Document', '/prompts/upload.aspx?AppID=" & AppID & "','400', '440');"">Upload</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Send Email', '/prompts/email.aspx?AppID=" & AppID & "','400', '400');"">Email</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Send SMS', '/prompts/sms.aspx?AppID=" & AppID & "','400', '400');"">SMS</a></li>")
            .AppendLine("</ul>")
            .AppendLine("</li>")
            .AppendLine("<li class=""dropdown"">")
            .AppendLine("<a href=""javascript:;"" class=""dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-cog""></i>Utilities<b class=""caret""></b></a>")
            .AppendLine("<ul class=""dropdown-menu"">")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Notes', '/prompts/casenotes.aspx?frmAppID=" & AppID & "', '600', '600');"">Notes</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Call History', '/prompts/callhistory.aspx?frmAppID=" & AppID & "', '1200', '600');"">Call History</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','View Rules', '/prompts/rules.aspx?AppID=" & AppID & "', '600', '500');"">View Rules</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Set Reminder', '/prompts/reminder.aspx?AppID=" & AppID & "', '400', '450');"">Set Reminder</a></li>")
            .AppendLine("</ul>")
            .AppendLine("</li>")
            .AppendLine("</ul>")
        End With
        Return objStringBuilder.ToString
    End Function

    Shared Function businessCallOptions(ByVal BusinessObjectID As String) As String
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .AppendLine("<ul class=""nav"">")
            .AppendLine("<li class=""dropdown"">")
            .AppendLine("<a href=""javascript:;"" class=""dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-comment""></i>Dispositions<b class=""caret""></b></a>")
            .AppendLine("<ul class=""dropdown-menu"">")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','ENG',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Engaged');"">Busy/Engaged</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','MSG',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Message Left');"">Message Left</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','ANS',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Answerphone');"">Answerphone</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','NOA',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': No Answer');"">No Answer</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','DUN',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Destination Unavailable');"">Destination Unavailable</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""saveCallOutcome($('#BusinessObjectID').text(),'','WRN',''," & Config.DefaultUserID & ",$('#TelephoneNumber').text() + ': Wrong Number');"">Wrong Number</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""$('#cbk-prompt').click();return false;"">Call Back</a></li>")
            .AppendLine("<li><a href=""#"" onclick=""cancelCallBack(" & BusinessObjectID & ")"">Cancel Call Back</a></li>")
            .AppendLine("</ul>")
            .AppendLine("</li>")
            .AppendLine("<li class=""dropdown"">")
            .AppendLine("<a href=""javascript:;"" class=""dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-cog""></i>Utilities<b class=""caret""></b></a>")
            .AppendLine("<ul class=""dropdown-menu"">")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Notes', '/prompts/businesscasenotes.aspx?frmBusinessObjectID=" & BusinessObjectID & "', '600', '600');"">Notes</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Call History', '/prompts/businesscallhistory.aspx?frmBusinessObjectID=" & BusinessObjectID & "', '1200', '600');"">Call History</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Document History', '/prompts/letterhistory.aspx?AppID=" & BusinessObjectID & "','800', '400');"">History</a></li>")
            .AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Upload Document', '/prompts/upload.aspx?AppID=" & BusinessObjectID & "','400', '440');"">Upload</a></li>")
            '.AppendLine("<li><a href=""#modal-iframe"" data-toggle=""modal"" onclick=""setPrompt('#modal-iframe','Set Reminder', '/prompts/reminder.aspx?AppID=" & BusinessObjectID & "', '400', '450');"">Set Reminder</a></li>")
            .AppendLine("</ul>")
            .AppendLine("</li>")
            .AppendLine("</ul>")
        End With
        Return objStringBuilder.ToString
    End Function

    Shared Function formatReportName(query As String) As String
        Dim strReportName As String = ""
        Dim arrQuery As Array = Split(query, "&")
        Dim y As Integer = 0
        For x As Integer = 0 To UBound(arrQuery)
            Dim arrFieldValues As Array = Split(arrQuery(x), "=")
            If (checkValue(arrFieldValues(1)) And arrFieldValues(0) <> "frmTitle" And arrFieldValues(0) <> "frmSearch" And arrFieldValues(0) <> "frmSelectColumns" And arrFieldValues(0) <> "strMessage" And arrFieldValues(0) <> "strMessageTitle" And arrFieldValues(0) <> "strMessageType" And arrFieldValues(0) <> "__EVENTTARGET" And arrFieldValues(0) <> "__EVENTVALIDATION" And arrFieldValues(0) <> "__EVENTARGUMENT" And arrFieldValues(0) <> "__VIEWSTATE") Then
                If (y = 0) Then
                    strReportName += Replace(Regex.Replace(arrFieldValues(0), "([A-Z0-9])", " $1"), "frm", "") & "=" & decodeURL(arrFieldValues(1))
                Else
                    strReportName += ", " & Replace(Regex.Replace(arrFieldValues(0), "([A-Z0-9])", " $1"), "frm", "") & "=" & decodeURL(arrFieldValues(1))
                End If
                y += 1
            End If
        Next
        Return strReportName
    End Function

    Shared Function menuTags(ByVal url As String) As String
        Dim strReplacedTags As String = url
        ' Future
        strReplacedTags = Replace(strReplacedTags, "{Today}", Config.DefaultDate)
        strReplacedTags = Replace(strReplacedTags, "{EndMonth}", Config.DefaultDate.AddMonths(1).AddDays(-Config.DefaultDate.Day))
        strReplacedTags = Replace(strReplacedTags, "{NextFiveDays}", Config.DefaultDate.AddDays(5))
        strReplacedTags = Replace(strReplacedTags, "{NextWeek}", Config.DefaultDate.AddDays(7))
        strReplacedTags = Replace(strReplacedTags, "{NextMonth}", Config.DefaultDate.AddMonths(1))
        ' Past
        strReplacedTags = Replace(strReplacedTags, "{Yesterday}", Config.DefaultDate.AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FiveDays}", Config.DefaultDate.AddDays(-5))
        strReplacedTags = Replace(strReplacedTags, "{LastWeek}", Config.DefaultDate.AddDays(-7))
        strReplacedTags = Replace(strReplacedTags, "{LastYear}", Config.DefaultDate.AddYears(-1))
        strReplacedTags = Replace(strReplacedTags, "{LastTwoYears}", Config.DefaultDate.AddYears(-2))
        strReplacedTags = Replace(strReplacedTags, "{LastTenYears}", Config.DefaultDate.AddYears(-10))
        strReplacedTags = Replace(strReplacedTags, "{FirstMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstYear}", New DateTime(Config.DefaultDate.Year, 1, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddMonths(-1))
        strReplacedTags = Replace(strReplacedTags, "{EndLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfWeek}", Config.DefaultDate.AddDays(1).AddDays(-Config.DefaultDate.AddDays(1).DayOfWeek))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfYear}", Config.DefaultDate.AddDays(1).AddDays(-Config.DefaultDate.DayOfYear))

        strReplacedTags = Replace(strReplacedTags, "/net/reports/reportbuilder.aspx", "/reports.aspx")
        strReplacedTags = Replace(strReplacedTags, "/reports/reportbuilder.aspx", "/reports.aspx")

        Return strReplacedTags
    End Function

    Shared Function simpleFooter(ByVal name As String, ByVal toolbar As Boolean, Optional ByVal logout As Boolean = True) As String
        Return "" ' Not in use
        'Dim objStringBuilder As New StringBuilder
        'objStringBuilder.Append("<script type=""text/javascript"">setReportHeight();</script>" & vbCrLf)
        'If (toolbar) Then
        '    objStringBuilder.Append("<div id=""toolBar""><img id=""userIcon"" src=""/net/images/user.gif"" width=""16"" height=""16"" /><div id=""userInfo"" class=""sml"">" & name & "</div>")
        '    objStringBuilder.Append("<div id=""reminder""><a href=""#"" title=""Reminders due""><img src=""/net/images/icons/reminder.gif"" width=""16"" height=""16"" border=""0"" /></a><div id=""reminderAlert""><p><span id=""reminderAmount""></span> new reminder(s) due</p><a href=""#"" onclick=""hideReminders();""><img src=""/net/images/icons/close.gif"" width=""14"" height=""14"" class=""close"" border=""0"" /></a></div></div>")
        '    objStringBuilder.Append("<script type=""text/javascript"">$(document).ready(function () { checkReminders(); });</script>")
        '    If (logout) Then
        '        objStringBuilder.Append("<a><input type=""button"" value=""Logout"" class=""buttonLogout"" onclick=""location.href='/net/logout.aspx'"" /></a>")
        '    End If
        '    objStringBuilder.Append("</div>" & vbCrLf)
        'End If
        'Return objStringBuilder.ToString()
    End Function

    Shared Function getAnyField(ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT " & fld & " AS Value FROM " & tbl & " WHERE " & idfld & " = '" & id & "' "
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    Shared Function getAnyFieldByCompanyID(ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT " & fld & " AS Value FROM " & tbl & " WHERE " & idfld & " = '" & id & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    Shared Function getAnyFieldFromDataStore(ByVal fld As String, ByVal AppID As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT StoredDataValue FROM tbldatastore WHERE StoredDataName = '" & fld & "' AND AppID = '" & AppID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    Shared Function getAnyFieldFromUserStore(ByVal fld As String, ByVal UserID As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT StoredUserValue FROM tbluserstore WHERE StoredUserName = '" & fld & "' AND UserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    Shared Function getAnyFieldCached(ByVal cache As Web.Caching.Cache, ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT " & fld & " AS Value FROM " & tbl & " WHERE " & idfld & " = '" & id & "' AND CompanyID = '" & CompanyID & "'"
        strAnyField = New Caching(cache, strSQL, "", "", "").returnCacheString()
        Return strAnyField
    End Function

    Shared Function getSystemConfigValue(ByVal fld As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationName = '" & fld & "' AND CompanyID = '" & CompanyID & "'"
        strAnyField = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        Return strAnyField
    End Function

    Shared Function getUserAccessLevel(ByVal UserID As String, ByVal level As String) As Boolean
        Dim strQry As String = "SELECT UserID FROM vwuseraccesslevels WHERE UserID = @UserID AND UserAccessLevel = '" & level & "'"
        Dim arrParams As SqlParameter() = {New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
        Dim strResult As String = New Caching(Nothing, strQry, "", "", "", CommandType.Text, arrParams).returnCacheString
        If (checkValue(strResult) Or loggedInUserValue("UserSuperAdmin") = "True") Then
            Return True
        Else
            Return False
        End If
    End Function

    Shared Function getAdminLevel(ByVal UserID As String, ByVal level As String) As Boolean
        Dim strQry As String = "SELECT UserID FROM tblusers WHERE UserID = @UserID AND User" & level & " = 1"
        Dim arrParams As SqlParameter() = {New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
        Dim strResult As String = New Caching(Nothing, strQry, "", "", "", CommandType.Text, arrParams).returnCacheString
        If (checkValue(strResult) Or loggedInUserValue("UserSuperAdmin") = "True") Then
            Return True
        Else
            Return False
        End If
    End Function

    Shared Function getUserAccessLevels(ByVal UserID As String, ByVal arr As ArrayList) As ArrayList
        Dim strQry As String = "SELECT UserAccessLevel FROM vwuseraccesslevels WHERE UserID = @UserID AND UserAccessLevel LIKE '%Access%'"
        Dim arrParams As SqlParameter() = {New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
        Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "", CommandType.Text, arrParams).returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                Try
                    arr.Add(Row.Item("UserAccessLevel"))
                Catch e As Exception
                End Try
            Next
        End If
        Return arr
    End Function

    ' ******************************
    ' *** Execute query          ***
    ' ******************************
    Shared Sub executeNonQuery(ByVal qry As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "")
        Dim objDatabase As DatabaseManager = New DatabaseManager
        objDatabase.executeNonQuery(qry, cmdType, parameters, AccessLevel)
        objDatabase = Nothing
        logQuery(qry)
    End Sub

    ' ******************************************
    ' *** Execute query  and return identity ***
    ' ******************************************
    Shared Function executeIdentityQuery(ByVal qry As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As Integer
        Dim strIdentity As Integer = 0
        qry = "SET NOCOUNT ON;" & qry & ";" & "SELECT @@IDENTITY AS ID;"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(qry, cmdType, parameters, AccessLevel)
        If (objResult IsNot Nothing) Then
            strIdentity = objResult.ToString
        End If
        'Dim rsExecute As SqlDataReader = objDatabase.executeReader(qry)
        'If (rsExecute.HasRows) Then
        '    While rsExecute.Read
        '        strIdentity = rsExecute("ID")
        '    End While
        'End If
        'rsExecute.Close()
        'rsExecute = Nothing
        objResult = Nothing
        objDatabase = Nothing
        logQuery(qry)
        Return strIdentity
    End Function

    ' ********************************************************
    ' *** Execute query and return number of rows effected ***
    ' ********************************************************
    Shared Function executeRowsAffectedQuery(ByVal qry As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As Integer
        Dim intRowsAffected As Integer = 0
        Dim objDatabase As DatabaseManager = New DatabaseManager
        intRowsAffected = objDatabase.executeRowsAffectedQuery(qry, cmdType, parameters, AccessLevel)
        objDatabase = Nothing
        logQuery(qry)
        Return intRowsAffected
    End Function

    Shared Sub logQuery(qry As String)
        If (InStr(qry, "tbllocks") = 0 And InStr(qry, "tblxmlreceived") = 0 And InStr(qry, "tblloginattempts") = 0 And InStr(qry, "tblreportlogs") = 0 And InStr(qry, "tblpdfhistory") = 0 And InStr(qry, "tblcallhistory") = 0 And InStr(qry, "tblquerylogs") = 0) Then
            'Dim strTimeStamp As String = Config.DefaultDateTime.Year & Config.DefaultDateTime.Month & Config.DefaultDateTime.Day
            'Dim strRequestLogFile = HttpContext.Current.Server.MapPath("/net/csvfiles/logs/query_logs_" & CompanyID & "_" & strTimeStamp & ".csv")
            'Dim strLine As String = CompanyID & "," & Config.DefaultUserID & ",""" & qry & """,""" & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & """," & Config.DefaultDateTime
            'If (File.Exists(strRequestLogFile)) Then
            '    File.AppendAllText(strRequestLogFile, strLine & vbCrLf)
            'Else
            '    File.AppendAllText(strRequestLogFile, "CompanyID,UserID,Query,IPAddress,Date" & vbCrLf)
            'End If
            'Dim intCompanyID As String = CompanyID
            'If (Not checkValue(intCompanyID)) Then intCompanyID = 0
            'Dim intUserID As String = Config.DefaultUserID
            'If (Not checkValue(intUserID)) Then intUserID = 0
            'Dim strQry As String = "INSERT INTO [crm-logs].dbo.tblquerylogs (QueryCompanyID, QueryUserID, QueryText, QueryIPAddress) VALUES (" & _
            '    "@CompanyID" & ", " & _
            '    "@UserID" & ", " & _
            '    formatField(qry, "", "") & ", " & _
            '    formatField(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), "", "") & ") "
            'Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, intCompanyID), _
            '                              New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, intUserID)}
            'Dim objDatabase As DatabaseManager = New DatabaseManager
            'objDatabase.executeNonQuery(strQry, CommandType.Text, arrParams, "")
            'objDatabase = Nothing
        End If
    End Sub

    Shared Function encodeURL(ByVal url As String) As String
        If (checkValue(url)) Then
            Return HttpUtility.UrlEncode(url)
        Else
            Return ""
        End If
    End Function

    Shared Function decodeURL(ByVal url As String) As String
        If (checkValue(url)) Then
            Return HttpUtility.UrlDecode(url)
        Else
            Return ""
        End If
    End Function

    Shared Function tickCross(ByVal str As String, ByVal val As String) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                Return "<i class=""icon-check""></i>"
            Else
                Return "<i class=""icon-times""></i>"
            End If
        End If
    End Function

    Shared Function tickCrossTitle(ByVal str As String, ByVal val As String, ByVal title As String) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                Return "<img src=""/net/images/tick.gif"" width=""16"" height=""16"" border=""0"">"
            Else
                Return "<img src=""/net/images/cross.gif"" width=""16"" height=""16"" border=""0"" title=""" & title & """>"
            End If
        End If
    End Function

    Shared Function tickCrossValid(ByVal str As String, ByVal val As String, Optional ByVal hover As Boolean = False) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                If (hover) Then
                    Return "<img src=""/net/images/invalid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" onmouseover=""$(this).attr('src','/net/images/valid.png')"" onmouseout=""$(this).attr('src','/net/images/invalid.png')"" />"
                Else
                    Return "<img src=""/net/images/valid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" />"
                End If
            Else
                If (hover) Then
                    Return "<img src=""/net/images/valid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" onmouseover=""$(this).attr('src','/net/images/invalid.png')"" onmouseout=""$(this).attr('src','/net/images/valid.png')"" />"
                Else
                    Return "<img src=""/net/images/invalid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" />"
                End If
            End If
        End If
    End Function

    '***********************************************************************************************************************
    ' Format the data before placing it in the database (value,[U = uppercase,L = lowercase, T = title case, N = Numeric, B = Boolean, M = Money, TM = Time, DT = Date, DTTM = DateTime, X = no format], default value)
    '***********************************************************************************************************************
    Shared Function formatField(ByVal val As String, ByVal typ As String, ByVal dflt As String) As String
        Dim strTemp As String = "", strValClean As String = "", strFinished As String = "n", strPrev As String = ""
        If (Not checkValue(val)) Then
            If (dflt = "NULL") Then
                Return dflt
                strFinished = "y"
            Else
                val = Trim(dflt)
            End If
        End If
        If (strFinished = "n") Then
            strValClean = Replace(val, "'", "''")
            Select Case typ
                Case "U"
                    Return "'" & Trim(UCase(strValClean)) & "'"
                Case "L"
                    Return "'" & Trim(LCase(strValClean)) & "'"
                Case "T"
                    Dim i As Integer
                    For i = 1 To Len(strValClean)
                        If (i = 1) Or (strPrev = " ") Or (strPrev = "-") Then
                            strTemp = strTemp & UCase(Mid(strValClean, i, 1))
                        Else
                            strTemp = strTemp & LCase(Mid(strValClean, i, 1))
                        End If
                        strPrev = Mid(strValClean, i, 1)
                    Next
                    Return "'" & Trim(strTemp) & "'"
                Case "M"
                    Return "CONVERT(money," & val & ")"
                Case "N"
                    Return Trim(strValClean)
                Case "B"
                    If (strValClean = "True") Or (UCase(strValClean) = "Y") Or (UCase(strValClean) = "YES") Or (strValClean = "1") Or (strValClean = "on") Then
                        Return "1"
                    Else
                        Return "0"
                    End If
                Case "TM"
                    Return "'" & TimeValue(strValClean) & "'"
                Case "DT"
                    If (checkValue(strValClean)) Then
                        Return "CONVERT(DateTime, '" & strValClean & "',103)"
                    Else
                        Return "NULL"
                    End If
                Case "DTISO"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & "'"
                    Else
                        Return "NULL"
                    End If
                Case "DTTM"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & " " & TimeValue(val) & "'"
                    Else
                        Return "NULL"
                    End If
                Case Else
                    Return "'" & Trim(strValClean) & "'"
            End Select
        Else
            Return ""
        End If
    End Function

    Shared Function displayCurrency(ByVal am As Decimal) As String
        If checkValue(am) Then
            Return FormatCurrency(am)
        Else
            Return FormatCurrency(0)
        End If
    End Function

    Shared Function padZeros(ByVal val As String, ByVal no As Integer) As String
        If (Not checkValue(val)) Then
            Return ""
        Else
            Dim i As Integer
            For i = 1 To (no - Len(val))
                val = "0" & val
            Next
            Return val
        End If
    End Function

    Shared Function getSystemUser() As String
        Return returnSystemUser(CompanyID)
        'Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserName = 'System' AND CompanyID = '" & CompanyID & "'"
        'Dim intSystemUser As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        'If (checkValue(intSystemUser)) Then
        '    Return CInt(intSystemUser)
        'Else
        '    Return 0
        'End If
    End Function

    Shared Function returnSystemUser(CompanyID As String) As String
        Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserName = 'System' AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
        Dim intSystemUser As String = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams).returnCacheString()
        If (checkValue(intSystemUser)) Then
            Return intSystemUser
        Else
            Return "0"
        End If
    End Function

    Shared Function regexTest(ByVal pat As String, ByVal val As String) As Boolean
        If Not checkValue(val) Then val = "" ' 27/02/2012 - added to stop NULL values
        If (Not checkValue(pat)) Then
            Return True
        Else
            If (Regex.IsMatch(val, pat, RegexOptions.IgnoreCase)) Then
                Return True ' Expression found
            Else
                Return False ' Expression not found
            End If
        End If
    End Function

    Shared Function regexTestCase(ByVal pat As String, ByVal val As String) As Boolean
        If Not checkValue(val) Then val = "" ' 27/02/2012 - added to stop NULL values
        If (Not checkValue(pat)) Then
            Return True
        Else
            If (Regex.IsMatch(val, pat)) Then
                Return True ' Expression found
            Else
                Return False ' Expression not found
            End If
        End If
    End Function

    Shared Function regexCount(ByVal pat As String, ByVal val As String) As Integer
        If (Not checkValue(pat)) Then
            Return 0
        Else
            Return Regex.Matches(val, pat).Count
        End If
    End Function

    '*******************************************************
    ' Replace using regular expression (replace,with,string)
    '*******************************************************
    Shared Function regexReplace(ByVal pat As String, ByVal rep As String, ByVal val As String) As String
        If (Not checkValue(pat)) Then
            Return True
        Else
            Return Regex.Replace(val, pat, rep, RegexOptions.IgnoreCase)
        End If
    End Function

    Shared Function regexFirstMatch(ByVal pat As String, ByVal val As String) As String
        If (Not checkValue(pat) Or Not (checkValue(val))) Then
            Return ""
        Else
            Dim objMatches As MatchCollection = Regex.Matches(val, pat, RegexOptions.IgnoreCase)
            If (objMatches.Count > 0) Then
                Return regexReplace(pat, "$1", objMatches.Item(0).ToString)
            Else
                Return ""
            End If
        End If
    End Function

    '*******************************************************
    ' Execute any method from Common
    '*******************************************************
    Shared Function executeMethodByName(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(Common)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        'Try
        Return objMethodInfo.Invoke(inst, params)
        'Catch e As TargetInvocationException
        'Return ""
        'End Try
    End Function
    '*******************************************************
    ' Execute any dropdown method from CommonDropdowns
    '*******************************************************
    Shared Function executeDropdownByName(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(CommonDropdowns)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        'Try
        Return objMethodInfo.Invoke(inst, params)
        'Catch e As TargetInvocationException
        'Return ""
        'End Try
    End Function

    '    public static string InvokeStringMethod2
    '    (string typeName, string methodName, string stringParam)
    '{
    '    // Get the Type for the class
    '    Type calledType = Type.GetType(typeName);

    '    // Invoke the method itself. The string returned by the method winds up in s.
    '    // Note that stringParam is passed via the last parameter of InvokeMember,
    '    // as an array of Objects.
    '    String s = (String)calledType.InvokeMember(
    '                    methodName,
    '                    BindingFlags.InvokeMethod | BindingFlags.Public | 
    '                        BindingFlags.Static,
    '                    null,
    '                    null,
    '                    new Object[] { stringParam });

    '    // Return the string that was returned by the called method.
    '    return s;

    Shared Function checkDate(ByVal dt As String) As String
        If (checkValue(dt)) Then
            Try
                dt = CDate(dt)
            Catch e As InvalidCastException
                dt = ""
            End Try
        End If
        Return dt
    End Function

    Shared Function formatTelephone(ByVal tel As String) As String
        If (checkValue(tel)) Then
            tel = Replace(tel, " ", "")
            If (Left(tel, 2) = 44) Then
                tel = Right(tel, Len(tel) - 2)
            End If
            If (Left(tel, 1) <> 0) Then
                tel = "0" & tel
            End If
        End If
        Return tel
    End Function

    Shared Function replaceSpaces(ByVal str As String) As String
        If (checkValue(str)) Then
            str = Replace(str, " ", "")
        End If
        Return str
    End Function

    Shared Function titleLookup(ByVal strTitle As String) As Boolean
        Dim boolTitleFound As Boolean = False
        Dim strSQL As String = "SELECT TOP 1 ApplicationDropdownValues FROM tblimportvalidation WHERE ValName = 'App1Title' AND (CompanyID = @CompanyID Or CompanyID = 0)"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblimportvalidation", CommandType.Text, arrParams)
        Dim dsTitles As DataTable = objDataSet.Tables("tblimportvalidation")
        If (dsTitles.Rows.Count > 0) Then
            For Each Row As DataRow In dsTitles.Rows
                Dim arrTitles As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
                For x As Integer = 0 To UBound(arrTitles)
                    If (strTitle = arrTitles(x)) Then
                        boolTitleFound = True
                        Exit For
                    End If
                Next
            Next
        Else
            boolTitleFound = False
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsTitles = Nothing
        objDatabase = Nothing
        Return boolTitleFound
    End Function

    Shared Function postWebRequest(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False, Optional ByVal soap As Boolean = False, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
        If (boolCertificate = False Or InStr(url, "engagedcrm.co.uk")) Then
            Call Certificate.OverrideCertificateValidation()
        End If
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                If (boolCertificate = False Or InStr(url, "engagedcrm.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                If (soap) Then
                    .Headers.Add("SOAPAction", url) ' Required for Cisco
                End If
                If (xml) Then
                    .ContentType = "text/xml; charset=utf-8"
                Else
                    .ContentType = "application/x-www-form-urlencoded"
                End If
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
            '    responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Shared Function getWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                If (InStr(url, "engagedcrm.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                .Method = WebRequestMethods.Http.Get
                .KeepAlive = False
            End With
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Shared Function checkResponse(ByRef objResponse As HttpWebResponse, Optional data As String = "") As Boolean
        Dim boolValidResponse = True
        Try ' Make sure the response is valid
            If (objResponse Is Nothing) Then
                Try ' Make sure the response is valid
                    Dim strMessage As String = _
                        "<h1>Error Details</h1>" & vbCrLf & _
                        "<div><strong>URI:</strong> " & objResponse.ResponseUri.PathAndQuery & "</div>" & vbCrLf & _
                        "<div><strong>Method:</strong> " & objResponse.Method & "</div>" & vbCrLf & _
                        "<div><strong>Status:</strong> " & objResponse.StatusCode & "</div>" & vbCrLf & _
                        "<div><strong>Data:</strong> " & data & "</div>" & vbCrLf
                    postEmail("", "devteam@engagedcrm.co.uk", "CRM HTTP Connection Error", strMessage, True, "", True)
                    boolValidResponse = False
                Catch e As Exception
                    reportErrorMessage(e)
                    boolValidResponse = False
                End Try
            Else
                If (objResponse.StatusCode = HttpStatusCode.OK) Then
                    boolValidResponse = True
                End If
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValidResponse = False
        End Try
        Return boolValidResponse
    End Function

    Shared Sub reportErrorMessage(ByVal ex As Exception)
        Dim st As StackTrace = New StackTrace(ex, True)
        Dim sf As StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        postEmail("", "devteam@engagedcrm.co.uk", "CRM Error from " & loggedInUserValue("UserFullName"), strMessage, True, "", True)
    End Sub

    '*************************************************
    ' Increment any field in any table
    '*************************************************
    Shared Sub incrementField(ByVal name As String, ByVal keyfld As String, ByVal fld As String, ByVal tbl As String, ByVal incr As Integer)
        If checkValue(name) And checkValue(keyfld) And checkValue(fld) And checkValue(tbl) Then
            If (Not checkValue(incr)) Then incr = 1
            Dim strQry As String = "UPDATE " & tbl & " SET " & fld & " = " & fld & " + " & incr & " WHERE " & name & " = " & keyfld & " AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
            executeNonQuery(strQry, CommandType.Text, arrParams)
        End If
    End Sub

    Shared Sub incrementDataStoreField(ByVal AppID As String, ByVal name As String, ByVal incr As Decimal)
        If (checkValue(AppID)) Then
            Dim strValue As String = getAnyFieldFromDataStore(name, AppID)
            If (IsNumeric(strValue)) Then
                Dim strQry As String = "UPDATE tbldatastore SET StoredDataValue = CONVERT(DECIMAL(18,2),StoredDataValue) + CONVERT(DECIMAL(18,2)," & incr & ") WHERE StoredDataName = @StoredDataName AND AppID = @AppID AND CompanyID = @CompanyID"
                Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                   New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                                   New SqlParameter("@StoredDataName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "StoredDataName", DataRowVersion.Current, name)}
                executeNonQuery(strQry, CommandType.Text, arrParams)
            End If
        End If
    End Sub

    Shared Sub incrementSystemConfigurationField(ByVal name As String, ByVal incr As Integer)
        Dim strValue As String = getAnyFieldByCompanyID("SystemConfigurationValue", "tblsystemconfiguration", "SystemConfigurationName", "SMSNumberSent")
        If (IsNumeric(strValue)) Then
            Dim strQry As String = "UPDATE tblsystemconfiguration SET SystemConfigurationValue = SystemConfigurationValue + " & incr & " WHERE SystemConfigurationName = @SystemConfigurationName AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@SystemConfigurationName", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "SystemConfigurationName", DataRowVersion.Current, name)}
            executeNonQuery(strQry, CommandType.Text, arrParams)
        End If
    End Sub

    '***********************
    ' Update a single database field by AppID
    ' tbl = table, fld = field, typ = field type, val = value, dft = default field value)
    '***********************
    Shared Sub updateSingleDatabaseField(ByVal AppID As String, ByVal tbl As String, ByVal fld As String, ByVal typ As String, ByVal val As String, ByVal dft As String)
        If (checkValue(AppID)) Then
            Dim strQry As String = "UPDATE " & tbl & " SET " & fld & " = " & formatField(val, typ, dft) & " WHERE AppID = @AppID AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
            executeNonQuery(strQry, CommandType.Text, arrParams)
        End If
    End Sub

    '***********************
    ' Update any database field
    ' name = key field, id = key field value, tbl = table, fld = field, typ = field type, val = value, dft = default field value)
    '***********************
    Shared Sub updateAnyDatabaseField(ByVal name As String, ByVal id As String, ByVal tbl As String, ByVal fld As String, ByVal typ As String, ByVal val As String, ByVal dft As String)
        If (checkValue(id)) Then
            Dim strQry As String = "UPDATE " & tbl & " SET " & fld & " = " & formatField(val, typ, dft) & " WHERE " & name & " = @IDField AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@IDField", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "IDField", DataRowVersion.Current, id)}
            executeNonQuery(strQry, CommandType.Text, arrParams)
        End If
    End Sub

    Shared Sub updateDataStoreField(AppID As String, name As String, val As String, ByVal typ As String, dft As String)
        Dim intRowsAffected As Integer = 0
        Dim strQry As String = "UPDATE tbldatastore SET StoredDataValue = " & formatField(val, typ, dft) & " WHERE StoredDataName = @StoredDataName AND AppID = @AppID AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                           New SqlParameter("@StoredDataName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "StoredDataName", DataRowVersion.Current, name)}
        intRowsAffected = executeRowsAffectedQuery(strQry, CommandType.Text, arrParams)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tbldatastore (CompanyID, AppID, StoredDataName, StoredDataValue) VALUES (@CompanyID, @AppID, @StoredDataName," & formatField(val, typ, dft) & ")"
            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                               New SqlParameter("@StoredDataName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "StoredDataName", DataRowVersion.Current, name)}
            Call executeNonQuery(strQry, CommandType.Text, arrParams2)
        End If
    End Sub

    Shared Sub updateUserStoreField(UserID As String, name As String, val As String, ByVal typ As String, dft As String)
        Dim intRowsAffected As Integer = 0
        Dim strQry As String = "UPDATE tbluserstore SET StoredUserValue = " & formatField(val, typ, dft) & " WHERE StoredUserName = @StoredUserName AND UserID = @UserID AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID), _
                                           New SqlParameter("@StoredUserName", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "StoredUserName", DataRowVersion.Current, name)}
        intRowsAffected = executeRowsAffectedQuery(strQry, CommandType.Text, arrParams)
        If (intRowsAffected = 0) Then
            strQry = "INSERT INTO tbluserstore (CompanyID, UserID, StoredUserName, StoredUserValue) VALUES (@CompanyID, @UserID, @StoredUserName," & formatField(val, typ, dft) & ")"
            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID), _
                                                New SqlParameter("@StoredUserName", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "StoredUserName", DataRowVersion.Current, name)}
            Call executeNonQuery(strQry, CommandType.Text, arrParams2)
        End If
    End Sub

    Shared Function postEmail(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String, Optional ByVal boolUseDefault As Boolean = False) As String
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=" & boolUseDefault
        '        responseWrite(Config.ApplicationURL & "/webservices/sendemail.aspx?" & strEmailString & "&UserSessionID=" & UserSessionID)
        '        responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&UserSessionID=" & UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function postEmailWithMediaCampaignID(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String) As String
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=False"
        'responseWrite(Config.ApplicationURL & "/webservices/sendemail.aspx?" & strEmailString & "&UserSessionID=" & UserSessionID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function postEmailWithProfile(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String, Optional ByVal intEmailProfileID As Integer = 0, Optional strEmailReplyTo As String = "") As String
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=False&frmEmailProfileID=" & intEmailProfileID & "&frmEmailReplyTo=" & strEmailReplyTo
        'responseWrite(Config.ApplicationURL & "/webservices/sendemail.aspx?" & strEmailString & "&UserSessionID=" & UserSessionID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&UserSessionID=" & UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function postSMS(ByVal telno As String, ByVal body As String) As String
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/outbound/sms.aspx", "frmTelephoneTo=" & telno & "&frmSMSBody=" & body & "&UserSessionID=" & Config.UserSessionID)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function getDateFromWeek(ByVal wkday As Integer, ByVal week As Integer, ByVal year As Integer) As String
        Dim dteInitalDate As Date = CDate("01/01/" & year)
        Dim dteAdjustedWeeks As Date = DateAdd(DateInterval.WeekOfYear, week - 1, dteInitalDate)
        Dim intWeekDayDiff As Integer = wkday - Weekday(dteAdjustedWeeks, FirstDayOfWeek.Sunday)
        Dim dteAdjustedWeekDays As Date = DateAdd(DateInterval.Weekday, intWeekDayDiff, dteAdjustedWeeks)
        Return FormatDateTime(dteAdjustedWeekDays, DateFormat.ShortDate).ToString
    End Function

    Shared Function getNextBatchNumber() As Integer
        Dim intBatchNumber As Integer = 0
        Dim strSQL As String = "SELECT MAX(MediaCampaignScheduleBatchNo) AS NextMediaCampaignScheduleBatchNo FROM tblmediacampaignschedulebatches WHERE CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblmediacampaignschedulebatches", CommandType.Text, arrParams)
        Dim dsBatches As DataTable = objDataSet.Tables("tblmediacampaignschedulebatches")
        If (dsBatches.Rows.Count > 0) Then
            For Each Row As DataRow In dsBatches.Rows
                If (checkValue(Row.Item("NextMediaCampaignScheduleBatchNo").ToString)) Then
                    intBatchNumber = Row.Item("NextMediaCampaignScheduleBatchNo") + 1
                Else
                    intBatchNumber = 1
                End If
            Next
        Else
            intBatchNumber = 1
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsBatches = Nothing
        objDatabase = Nothing
        Return intBatchNumber
    End Function

    Shared Function getIndexNumber() As String
        Randomize()
        Dim strFront As String = DateDiff(DateInterval.Second, CDate("01/01/1900"), Now)
        Dim strTail As String = Int(1000000 * Rnd()).ToString
        Return strFront & strTail
    End Function

    Shared Function breadCrumbTrail(ByVal section As String, ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strTrail As String = "LDM / " & section
        If (checkValue(fld)) Then
            strTrail += " / " & getAnyField(fld, tbl, idfld, id)
        End If
        Return strTrail
    End Function

    Shared Function applicationPages(ByVal AppID As String, ByVal pg As Integer) As String
        Dim strPages As String = "<div class=""floatLeft"">Pages: </div>"
        Dim strSQL As String = "SELECT ApplicationPageOrder, ApplicationPageName FROM vwapplicationformpages WHERE AppID = @AppID AND CompanyID = @CompanyID ORDER BY ApplicationPageOrder"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        Dim strView As String = HttpContext.Current.Request("frmView")
        If (Not checkValue(strView)) Then strView = HttpContext.Current.Request("frmView2")
        Dim strClass As String = ""
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "vwapplicationformpages", CommandType.Text, arrParams)
        Dim dsApplicationForm As DataTable = objDataSet.Tables("vwapplicationformpages")
        If (dsApplicationForm.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplicationForm.Rows
                If (pg = Row.Item("ApplicationPageOrder")) Then
                    strClass = "numberSelected"
                Else
                    strClass = "number"
                End If
                strPages += "&nbsp;<a href=""/net/application/applicationform.aspx?AppID=" & AppID & "&Page=" & Row.Item("ApplicationPageOrder") & "&frmView=" & strView & """ class=""none"" title=""" & Row.Item("ApplicationPageName") & """><div class=""" & strClass & """>" & Row.Item("ApplicationPageOrder") + 1 & "</div></a>&nbsp;"
            Next
            If (pg = -1) Then
                strClass = "numberSelected"
            Else
                strClass = "number"
            End If
            strPages += "&nbsp;<a href=""/net/webservices/outbound/hotkey.aspx?AppID=" & AppID & "&frmView=" & strView & """ class=""none"" title=""Exit Case / Hotkey""><div class=""" & strClass & """>EX</div></a>&nbsp;"
            Return strPages
        Else
            Return ""
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsApplicationForm = Nothing
        objDatabase = Nothing
    End Function

    Shared Function splitTelephone(ByVal tel As String) As String()

        Dim strSTD As String = "", strLocal As String = ""
        Dim arrTelephone As String() = {"", ""}

        If (checkValue(tel)) Then

            tel = Replace(tel, " ", "")

            If (Left(tel, 2) = "07") Then

                strSTD = Left(tel, 5)
                strLocal = Right(tel, 6)

                arrTelephone(0) = strSTD
                arrTelephone(1) = strLocal
                Return arrTelephone

            Else

                Dim strSQL As String = "SELECT STDCode FROM tbltelephonestdcodes"
                Dim objDatabase As DatabaseManager = New DatabaseManager
                Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tbltelephonestdcodes")
                Dim dsSTD As DataTable = objDataSet.Tables("tbltelephonestdcodes")
                If (dsSTD.Rows.Count > 0) Then
                    For Each Row As DataRow In dsSTD.Rows
                        If (Left(tel, Len(Row.Item("STDCode"))) = Row.Item("STDCode")) Then
                            strSTD = Row.Item("STDCode")
                            strLocal = Right(tel, Len(tel) - Len(strSTD))
                            Exit For
                        End If
                    Next
                End If
                objDataSet.Clear()
                objDataSet = Nothing
                dsSTD = Nothing
                objDatabase = Nothing

                If (strSTD = "") Then
                    strSTD = Left(tel, 4)
                    If (Len(tel) > 4) Then
                        strLocal = Right(tel, Len(tel) - 4)
                    Else
                        strLocal = ""
                    End If
                End If

                arrTelephone(0) = strSTD
                arrTelephone(1) = strLocal
                Return arrTelephone

            End If

        Else

            arrTelephone(0) = ""
            arrTelephone(1) = ""
            Return arrTelephone

        End If

    End Function

    Shared Function shorten(ByVal str As String, ByVal length As Integer) As String
        If (checkValue(str)) Then
            If (Len(str) > length) Then
                Return Left(str, length) & "..."
            Else
                Return str
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function shortenShowTitle(ByVal str As String, ByVal length As Integer) As String
        If (checkValue(str)) Then
            If (Len(str) > length) Then
                Return "<span title=""" & str & """>" & Left(str, length) & "...</span>"
            Else
                Return str
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function checkInt(ByVal int As String) As Integer
        If (Not checkValue(int)) Then
            Return 0
        Else
            Return int
        End If
    End Function

    Shared Function checkDec(ByVal dec As String) As Decimal
        If (Not checkValue(dec)) Then
            Return 0.0
        Else
            Return dec
        End If
    End Function

    Shared Function highLevelDetails(ByVal AppID As String) As String
        Dim strDetails As String = "", App1FullName As String = "", App1DOB As String = ""
        strDetails = AppID & " -"
        App1FullName = getAnyField("App1FullName", "vwexport", "AppID", AppID)
        If (checkValue(App1FullName)) Then
            strDetails += " " & App1FullName
        End If
        Return strDetails
    End Function

    Shared Function statusCodes(ByVal AppID As String) As String
        Dim strDetails As String = "", StatusCode As String, SubStatusCode As String, StatusDescription As String, SubStatusDescription As String
        StatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
        StatusDescription = getAnyField("StatusDescription", "tblstatuses", "StatusCode", StatusCode)
        SubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
        SubStatusDescription = getAnyField("SubStatusDescription", "tblsubstatuses", "SubStatusCode", SubStatusCode)
        If (checkValue(StatusCode)) Then
            strDetails += "<span title=""" & StatusDescription & """>" & StatusCode & "</span>"
        End If
        If (checkValue(SubStatusCode)) Then
            strDetails += " - <span title=""" & SubStatusDescription & """>" & SubStatusCode & "</span>"
        End If
        Return strDetails
    End Function

    Shared Function nextClass(ByVal cls As String) As String
        If (checkValue(cls)) Then
            If (cls = "even") Then
                cls = "odd"
                Return cls
            Else
                cls = "even"
                Return cls
            End If
        Else
            cls = "even"
        End If
        Return cls
    End Function

    Shared Function loggedInUserValue(ByVal fld As String) As String
        Dim strSQL As String = "SELECT " & fld & " AS Value FROM tblusers WHERE UserSessionID = @UserSessionID AND UserLoggedIn = 1 AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@UserSessionID", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "UserSessionID", DataRowVersion.Current, cookieValue("ASP.NET_SessionId", ""))}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL, CommandType.Text, arrParams)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function cookieValue(ByVal nm As String, Optional ByVal val As String = "") As String
        If (Not HttpContext.Current.Request.Cookies(nm) Is Nothing) Then
            If (checkValue(val)) Then
                If (Not HttpContext.Current.Request.Cookies(nm)(val) Is Nothing) Then
                    Return HttpContext.Current.Request.Cookies(nm)(val).ToString
                Else
                    Return ""
                End If
            Else
                Return HttpContext.Current.Request.Cookies(nm).Value.ToString
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function passwordPolicyCheck(ByVal pw As String) As Boolean
        If (Len(pw) < 7 Or Len(pw) > 12) Then
            Return False
        ElseIf (regexTest("[a-z]", pw) = 0) Then
            Return False
        ElseIf (regexCount("[0-9]", pw) < 2) Then
            Return False
        ElseIf (regexCount("[""""|'|&]", pw) > 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Shared Function generatePassword(ByVal length As Integer) As String
        Dim intRandChar As Integer = 0, strPassword As String = ""
        Do Until Len(strPassword) = length
            Randomize()
            intRandChar = Int(Rnd() * 122) + 1
            If (intRandChar > 64 And intRandChar < 91) Or (intRandChar > 96 And intRandChar < 123) Or (intRandChar > 47 And intRandChar < 58) Then
                strPassword += Chr(intRandChar)
            End If
        Loop
        Return strPassword
    End Function

    Shared Function multiplyRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round((a * b), intRound), intRound)
        End If
    End Function

    Shared Function divideRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round(CDec(a / b), intRound), intRound)
        End If
    End Function

    Shared Function divideRoundString(ByVal a As String, ByVal b As String) As Decimal
        a = CDec(a)
        b = CDec(b)
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, 0)
        Else
            Return formatNumber(Decimal.Round(CDec(a / b), 0), 0)
        End If
    End Function

    Shared Function percentageRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round(CDec((a / b) * 100), intRound), intRound)
        End If
    End Function

    Shared Function formatNumber(ByVal a As Decimal, ByVal intRound As Integer) As String
        Dim strFormat As String = "0"
        If (intRound > 0) Then
            For i As Integer = 1 To intRound
                If (i = 1) Then strFormat += "."
                strFormat += "0"
            Next
        End If
        Return Format(a, strFormat)
    End Function

    Shared Function getDateTypes() As String
        Dim strTypes As String = ""
        Dim strSQL As String = "SELECT COLS.name AS ColumnName FROM dbo.sysobjects AS OBJ INNER JOIN dbo.syscolumns AS COLS ON OBJ.id = COLS.id INNER JOIN dbo.systypes AS TYP ON COLS.xusertype = TYP.xusertype WHERE (OBJ.name = N'tblapplicationstatus') AND (COLS.name LIKE N'%date%') ORDER BY ColumnName"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "sysobjects")
        Dim dsDataType As DataTable = objDataSet.Tables("sysobjects")
        Dim x As Integer = 0
        If (dsDataType.Rows.Count > 0) Then
            For Each Row As DataRow In dsDataType.Rows
                If (x = 0) Then
                    strTypes += Row.Item("ColumnName")
                Else
                    strTypes += "," & Row.Item("ColumnName")
                End If
                x += 1
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsDataType = Nothing
        objDataBase = Nothing
        Return strTypes
    End Function

    Shared Function ddmmyyyy(ByVal yyyymmdd As String) As String
        If (checkValue(yyyymmdd)) Then
            Return Right(yyyymmdd, 2) & "/" & Left(Right(yyyymmdd, 4), 2) & "/" & Left(yyyymmdd, 4)
        Else
            Return "N/A"
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD/MM/YYYY
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddmmyyyy(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return padZeros(Day(tm), 2) & padZeros(Month(tm), 2) & Year(tm)
        Else
            Return ""
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD/MM hh:mm
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddmmhhmm(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return Day(tm) & "/" & Month(tm) & "&nbsp;" & FormatDateTime(tm, 4)
        Else
            Return "&nbsp;"
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD Short Month yyyy
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddsmyyyy(ByVal tm As Date) As String
        If (checkValue(tm)) Then
            Return padZeros(tm.Day, 2) & " " & MonthName(tm.Month, True) & " " & tm.Year
        Else
            Return "&nbsp;"
        End If
    End Function

    ' ************************************************************
    ' Display a date DD/MM/YYYY to YYYY-MM-DD
    ' ************************************************************
    Shared Function ddmmyyyy2yyyymmdd(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return CDate(tm).Year & "-" & padZeros(CDate(tm).Month, 2) & "-" & padZeros(CDate(tm).Day, 2)
        Else
            Return ""
        End If
    End Function

    Shared Function ss2mmss(ByVal secs As Integer) As String
        Dim intMinutes As Integer = Fix(secs / 60)
        Dim intRemainingSeconds As Integer = secs Mod 60
        Return padZeros(intMinutes.ToString, 2) & ":" & padZeros(intRemainingSeconds.ToString, 2)
    End Function

    Shared Function ss2hhmmss(ByVal secs As Integer) As String
        Dim intMinutes As Integer = Fix(secs / 60)
        Dim intRemainingSeconds As Integer = secs Mod 60
        Dim intHours As Integer = Fix(intMinutes / 60)
        Dim intRemainingMinutes As Integer = intMinutes Mod 60
        Return padZeros(intHours.ToString, 2) & ":" & padZeros(intRemainingMinutes.ToString, 2) & ":" & padZeros(intRemainingSeconds.ToString, 2)
    End Function

    Shared Function ddmmyyyy2javascript(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return CDate(tm).Year & "," & CDate(tm).Month - 1 & "," & CDate(tm).Day & "," & CDate(tm).Hour & "," & CDate(tm).Minute
        Else
            Return ""
        End If
    End Function

    Shared Function ddmmyyhhmmss2utc(ByVal tm As Date, Optional ByVal timezone As Boolean = True, Optional ms As Boolean = True) As String
        If (checkValue(tm)) Then
            If (timezone) Then
                If (ms) Then
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 4) & "+00:00"
                Else
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "+00:00"
                End If
            Else
                If (ms) Then
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 4)
                Else
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2)
                End If
            End If
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2utcz(ByVal tm As Date, Optional ms As Boolean = True) As String
        If (checkValue(tm)) Then
            If (ms) Then
                Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 3) & "Z"
            Else
                Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "Z"
            End If
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2rfc(ByVal tm As Date) As String
        If (checkValue(tm)) Then
            Return tm.ToString("ddd, dd MMM yyyy hh:mm:ss") & " GMT"
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2number(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return CDate(tm).Year & padZeros(CDate(tm).Month, 2) & padZeros(CDate(tm).Day, 2) & padZeros(CDate(tm).Hour, 2) & padZeros(CDate(tm).Minute, 2) & padZeros(CDate(tm).Second, 2) & padZeros(CDate(tm).Millisecond, 3)
        Else
            Return "0"
        End If
    End Function

    Shared Function unix2ddmmyyhhss(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Dim dteDateTime = New DateTime(1970, 1, 1, 0, 0, 0, 0)
            dteDateTime = dteDateTime.AddSeconds(CInt(tm))
            Return dteDateTime.ToLocalTime
        Else
            Return ""
        End If
    End Function

    Shared Function ddmmyyhhss2unix(ByVal tm As DateTime) As Integer
        If (checkValue(tm)) Then
            Dim timeSpan As TimeSpan = (tm - New DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc))
            Return timeSpan.TotalSeconds
        Else
            Return 0
        End If
    End Function

    Shared Sub cloneApplication(ByVal AppID As String, ByVal ProductType As String)
        getWebRequest(Config.ApplicationURL & "/webservices/inbound/cloneapplication.aspx?AppID=" & AppID & "&ProductType=" & ProductType & "&UserSessionID=" & UserSessionID)
    End Sub

    Shared Function tHeadStart() As String
        Return "<thead>" & vbCrLf
    End Function

    Shared Function tHeadEnd() As String
        Return "</thead>" & vbCrLf
    End Function

    Shared Function tBodyStart() As String
        Return "<tbody>" & vbCrLf
    End Function

    Shared Function tBodyEnd() As String
        Return "</tbody>" & vbCrLf
    End Function

    Shared Function rowStart(ByVal strClass As String) As String
        Return "<tr class=""" & strClass & """>" & vbCrLf
    End Function

    Shared Function rowEnd() As String
        Return "</tr>" & vbCrLf
    End Function

    Shared Function colDatabaseField(ByRef obj As Object, ByVal field As String, Optional ByVal type As String = "", Optional ByVal strClass As String = "smlc") As String
        Dim strFieldText As String = ""
        Select Case type
            Case "DT" ' Date + Time
                strFieldText = ddmmyyyy(obj.Item(field).ToString)
            Case "M" ' Currency
                strFieldText = "£" & formatNumber(obj.Item(field).ToString, 2)
            Case "N" ' Number
                strFieldText = formatNumber(obj.Item(field).ToString, 2)
            Case Else
                strFieldText = obj.Item(field).ToString
        End Select
        Return "<td class=""" & strClass & """>" & strFieldText & "</td>" & vbCrLf
    End Function

    Shared Function colField(ByVal val As String, Optional ByVal type As String = "", Optional ByVal strClass As String = "smlc", Optional ByVal colspan As String = "", Optional ByVal tag As String = "td", Optional ByVal mouseover As String = "", Optional ByVal mouseout As String = "", Optional ByVal id As String = "") As String
        Dim strFieldText As String = "", strColSpan As String = "", strMouseOver As String = "", strMouseOut As String = "", strId As String = ""
        If (Not checkValue(tag)) Then tag = "td"
        Select Case type
            Case "M" ' Currency
                strFieldText = "£" & formatNumber(val, 2)
            Case "N" ' Number
                strFieldText = formatNumber(val, 2)
            Case Else
                strFieldText = val
        End Select
        If (checkValue(id)) Then
            strId = " id=""" & id & """"
        End If
        If (checkValue(colspan)) Then
            strColSpan = " colspan=""" & colspan & """"
        End If
        If (checkValue(mouseover)) Then
            strMouseOver = " onmouseover=""" & mouseover & """"
        End If
        If (checkValue(mouseout)) Then
            strMouseOut = " onmouseout=""" & mouseout & """"
        End If
        Return "<" & tag & strId & strColSpan & " class=""" & strClass & """" & strMouseOver & strMouseOut & ">" & strFieldText & "</" & tag & ">" & vbCrLf
    End Function

    Shared Function colNbsp(Optional ByVal strClass As String = "smlc", Optional ByVal tag As String = "td") As String
        Return "<" & tag & " class=""" & strClass & """>&nbsp;</" & tag & ">" & vbCrLf
    End Function

    Shared Function lookupValue(value As String, media As String, table As String) As String
        Dim strValue As String = ""
        Dim strSQL As String = "SELECT TOP 1 LookupReturnValue FROM tbllookups " & _
            "WHERE LookupMediaID = '" & media & "' AND LookupTableName = '" & table & "' AND LookupValue = '" & value & "'"
        strValue = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        Return strValue
    End Function

    Shared Function lookupReturnValue(value As String, media As String, table As String) As String
        Dim strValue As String = ""
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE LookupMediaID = '" & media & "' AND LookupTableName = '" & table & "' AND LookupReturnValue = '" & value & "'"
        strValue = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        Return strValue
    End Function

    Shared Function experianProductPurpose(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE (LookupMediaID = 1013 OR LookupMediaID = 1031) AND LookupTableName = 'tblproductpurposes' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function experianMaritalStatus(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE (LookupMediaID = 1013 OR LookupMediaID = 1031) AND LookupTableName = 'tblmaritalstatuses' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function experianResidentialStatus(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE (LookupMediaID = 1013 OR LookupMediaID = 1031) AND LookupTableName = 'tbllivingarrangements' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function experianEmploymentStatus(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE (LookupMediaID = 1013 OR LookupMediaID = 1031) AND LookupTableName = 'tblemploymentstatuses' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function experianPropertyType(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE (LookupMediaID = 1013 OR LookupMediaID = 1031) AND LookupTableName = 'tblpropertytypes' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function floEmploymentStatus(ByVal code As String) As String
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE LookupMediaID = 1020 AND LookupTableName = 'tblemploymentstatuses' AND LookupReturnValue = '" & code & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            Return objResult.ToString
        Else
            Return ""
        End If
        objResult = Nothing
        objDatabase = Nothing
    End Function

    Shared Function years2Months(ByVal years As String) As String
        If (checkValue(years)) Then
            Return CInt(years) * 12
        Else
            Return ""
        End If
    End Function

    Shared Function combineDate(ByVal day As Integer, ByVal month As Integer, ByVal year As Integer) As String
        Return padZeros(day, 2) & "/" & padZeros(month, 2) & "/" & year
    End Function

    Shared Sub setCookie(ByVal nm As String, ByVal fld As String, ByVal val As String)
        If (checkValue(val)) Then
            If (Not checkValue(cookieValue(nm, fld))) Then
                Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies(nm)
                If (objCookie Is Nothing) Then
                    objCookie = New HttpCookie(nm)
                End If
                objCookie.Domain = Config.CurrentDomain
                HttpContext.Current.Response.Cookies.Add(objCookie)
                objCookie.Values(fld) = val
            End If
        End If
    End Sub

    Shared Sub clearCookie(ByVal nm As String, ByVal fld As String)
        If (checkValue(cookieValue(nm, fld))) Then
            Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies(nm)
            If (objCookie Is Nothing) Then
                objCookie = New HttpCookie(nm)
            End If
            objCookie.Domain = Config.CurrentDomain
            HttpContext.Current.Response.Cookies.Add(objCookie)
            objCookie.Values(fld) = ""
        End If
    End Sub

    Shared Sub lockCase(ByVal UserID As String, ByVal AppID As String)
        Dim intLockCount As Integer = 0
        Dim strQry As String = "SELECT COUNT(LockID) AS LockCount FROM tbllocks WHERE AppID = @AppID AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strQry, CommandType.Text, arrParams)
        If (objResult IsNot Nothing) Then
            intLockCount = objResult.ToString
        End If
        objResult = Nothing
        objDatabase = Nothing
        If (intLockCount = 0) Then
            unlockCases(UserID)
            strQry = "INSERT INTO tbllocks(CompanyID, AppID, LockUserID) VALUES(@CompanyID, @AppID, @UserID)"
            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                               New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
            executeNonQuery(strQry, CommandType.Text, arrParams2)
            CommonSave.saveUpdatedDate(AppID, UserID, "LastAccessed")
        End If
    End Sub

    Shared Sub lockBusinessCase(ByVal UserID As String, ByVal BusinessObjectID As String)
        Dim intLockCount As Integer = 0
        Dim strQry As String = "SELECT COUNT(LockID) AS LockCount FROM tbllocks WHERE BusinessObjectID = @BusinessObjectID AND CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "BusinessObjectID", DataRowVersion.Current, BusinessObjectID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strQry, CommandType.Text, arrParams)
        If (objResult IsNot Nothing) Then
            intLockCount = objResult.ToString
        End If
        objResult = Nothing
        objDatabase = Nothing
        If (intLockCount = 0) Then
            unlockCases(UserID)
            strQry = "INSERT INTO tbllocks(CompanyID, BusinessObjectID, LockUserID) VALUES(@CompanyID, @BusinessObjectID, @UserID)"
            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "BusinessObjectID", DataRowVersion.Current, BusinessObjectID), _
                                               New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
            executeNonQuery(strQry, CommandType.Text, arrParams2)
            CommonSave.saveBusinessUpdatedDate(BusinessObjectID, UserID, "LastAccessed")
        End If
    End Sub

    Shared Sub unlockCases(ByVal UserID As String, Optional ByVal AppID As String = "")
        Dim strQry As String = ""
        If (checkValue(AppID)) Then
            strQry = "DELETE FROM tbllocks WHERE AppID <> @AppID AND LockUserID = @UserID AND CompanyID = @CompanyID"
            Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                         New SqlParameter("@LockUserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "LockUserID", DataRowVersion.Current, UserID), _
                         New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
            executeNonQuery(strQry, CommandType.Text, arrParams)
        Else
            If (Not checkValue(UserID)) Then UserID = 0
            strQry = "DELETE FROM tbllocks WHERE LockUserID = '" & UserID & "' AND CompanyID = '" & CompanyID & "'"
            Dim arrParams2 As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                               New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
            executeNonQuery(strQry, CommandType.Text, arrParams2)
        End If
    End Sub

    Shared Function checkCaseLock(ByVal UserID As String, ByVal AppID As String) As String
        Dim strCaseLock As String = ""
        If (Not checkValue(AppID)) Then AppID = 0
        Dim strSQL As String = "SELECT LockUserID, UserFullName FROM tbllocks LCK INNER JOIN tblusers USR ON USR.UserID = LCK.LockUserID WHERE AppID = @AppID AND LCK.CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tbllocks", CommandType.Text, arrParams)
        Dim dsDataType As DataTable = objDataSet.Tables("tbllocks")
        Dim x As Integer = 0
        If (dsDataType.Rows.Count > 0) Then
            For Each Row As DataRow In dsDataType.Rows
                If (CInt(Row.Item("LockUserID")) = UserID) Then
                    strCaseLock = ""
                Else
                    strCaseLock = Row.Item("UserFullName")
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsDataType = Nothing
        objDataBase = Nothing
        Return strCaseLock
    End Function

    Shared Function checkBusinessCaseLock(ByVal UserID As String, ByVal BusinessObjectID As String) As String
        Dim strCaseLock As String = ""
        If (Not checkValue(BusinessObjectID)) Then BusinessObjectID = 0
        Dim strSQL As String = "SELECT LockUserID, UserFullName FROM tbllocks LCK INNER JOIN tblusers USR ON USR.UserID = LCK.LockUserID WHERE BusinessObjectID = @BusinessObjectID AND LCK.CompanyID = @CompanyID"
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, BusinessObjectID)}
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "tbllocks", CommandType.Text, arrParams)
        Dim dsDataType As DataTable = objDataSet.Tables("tbllocks")
        Dim x As Integer = 0
        If (dsDataType.Rows.Count > 0) Then
            For Each Row As DataRow In dsDataType.Rows
                If (CInt(Row.Item("LockUserID")) = UserID) Then
                    strCaseLock = ""
                Else
                    strCaseLock = Row.Item("UserFullName")
                End If
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsDataType = Nothing
        objDataBase = Nothing
        Return strCaseLock
    End Function

    Shared Function caseLockImage(ByVal username As String) As String
        If (checkValue(username)) Then
            Return "<div class=""floatLeft""><img src=""/net/images/icons/lock_small.gif"" width=""12"" height=""12"" title=""Case locked by " & username & "."" />&nbsp;</div>"
        Else
            Return ""
        End If
    End Function

    Shared Function calcNextCallColour(ByVal dte As Date) As String
        Dim strColour As String = ""
        If checkValue(dte) Then
            If (DateDiff("s", Now, dte) < 0) Then
                strColour = " rowRed"
            End If
        End If
        Return strColour
    End Function

    Shared Function calcWorkingHour(ByVal dt As Date) As Date
        Select Case Weekday(dt)
            Case 2, 3, 4, 5, 6 ' Mon - Friday - 09:00 - 20:00
                If (DateDiff("s", "20:00", FormatDateTime(dt, 4)) > 0) Or (DateDiff("s", "09:00", FormatDateTime(dt, 4)) < 0) Then
                    dt = DateAdd("h", 13, dt)
                    dt = New Date(dt.Year, dt.Month, dt.Day, 9, 0, 0)
                End If
            Case 7 ' Saturday - 10:00 - 15:00
                If (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    dt = DateAdd("h", 14, dt)
                ElseIf (DateDiff("s", "15:00", FormatDateTime(dt, 4)) > 0) Then
                    dt = DateAdd("h", 42, dt)
                End If
            Case 1 ' Sunday - Not open
                dt = DateAdd("h", 42, dt)
        End Select
        Return dt
    End Function

    Shared Function checkWorkingHour(ByVal dt As Date) As Boolean
        Dim boolWorkingHour As Boolean = True
        Select Case Weekday(dt)
            Case 2, 3, 4, 5, 6 ' 10:00 - 20:00
                If (DateDiff("s", "20:00", FormatDateTime(dt, 4)) > 0) Or (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    boolWorkingHour = False
                End If
            Case 7 ' 10:00 - 15:00
                If (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    boolWorkingHour = False
                ElseIf (DateDiff("s", "15:00", FormatDateTime(dt, 4)) > 0) Then
                    boolWorkingHour = False
                End If
            Case 1 ' Not open
                boolWorkingHour = False
        End Select
        Return boolWorkingHour
    End Function

    Shared Sub calcTimeToFirstCall(ByVal AppID As String)
        Dim dteCreatedDate As String = getAnyField("CreatedDate", "tblapplicationstatus", "AppID", AppID)
        Dim intTimeToFirstCall As Integer = 0
        intTimeToFirstCall = DateDiff(DateInterval.Second, CDate(dteCreatedDate), Config.DefaultDateTime)
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        Call executeNonQuery("UPDATE tblapplicationstatus SET TimeToFirstCall = " & formatField(intTimeToFirstCall, "N", 0) & " WHERE TimeToFirstCall = 0 AND CreatedInsideBusinessHours = 1 AND AppID = @AppID AND CompanyID = @CompanyID", CommandType.Text, arrParams)
    End Sub

    Shared Function checkKickOut(ByVal UserID As String, ByVal reset As Boolean) As Boolean
        If (checkValue(UserID)) Then
            Dim boolKickOut As Boolean = getAnyField("UserWorkflowKickOut", "tblusers", "UserID", UserID)
            If (reset) Then
                Dim strQry As String = "UPDATE tblusers SET UserWorkflowKickOut = 0 WHERE UserID = " & UserID & " AND CompanyID = '" & CompanyID & "'"
                Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                   New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, UserID)}
                executeNonQuery(strQry, CommandType.Text, arrParams)
            End If
            Return boolKickOut
        Else
            Return False
        End If
    End Function

    Shared Sub cacheDependency(ByVal cache As Web.Caching.Cache, ByVal search As String)
        Dim objCache As Caching = New Caching(cache, "", "", "", 0)
        Dim arrSearch As Array = Split(search, ",")
        For x As Integer = 0 To UBound(arrSearch)
            objCache.cacheDependency(arrSearch(x))
        Next
    End Sub

    Shared Sub executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array)
        Dim objMethodType As Type = GetType(Common)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        objMethodInfo.Invoke(inst, params)
    End Sub

    Shared Function checkDateField(ByVal AppID As String, ByVal datefield As String) As Boolean
        Dim strQry As String = "", boolDateField As Boolean = False
        If (checkValue(datefield)) Then
            Select Case datefield
                Case "Created", "Updated", "NextCall", "CallBack", "LastContacted", "LastRemarketed", "Lock"
                    Dim strDate As String = getAnyField(datefield & "Date", "tblapplicationstatus", "AppID", AppID)
                    If (checkValue(strDate)) Then
                        boolDateField = True
                    End If
                Case Else
                    strQry = _
                        "SELECT ApplicationStatusDate FROM tblapplicationstatusdates " & _
                        "WHERE ApplicationStatusDateAppID = @AppID AND ApplicationStatusDateName = @ApplicationStatusDateName AND CompanyID = @CompanyID"
                    Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                       New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                                       New SqlParameter("@ApplicationStatusDateName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "ApplicationStatusDateName", DataRowVersion.Current, datefield)}
                    Dim objDataBase As New DatabaseManager
                    Dim objDataSet As DataSet = objDataBase.executeReader(strQry, "tblapplicationstatusdates", CommandType.Text, arrParams)
                    Dim dsStatusDates As DataTable = objDataSet.Tables("tblapplicationstatusdates")
                    If (dsStatusDates.Rows.Count > 0) Then
                        For Each Row As DataRow In dsStatusDates.Rows
                            If (checkValue(Row.Item("ApplicationStatusDate").ToString)) Then
                                boolDateField = True
                            End If
                        Next
                    End If
                    objDataSet.Clear()
                    objDataSet = Nothing
                    dsStatusDates = Nothing
                    objDataBase = Nothing
            End Select
        Else
            boolDateField = True
        End If
        Return boolDateField
    End Function

    Shared Function getDateField(ByVal AppID As String, ByVal datefield As String) As String
        Dim strQry As String = "", strDateField As String = ""
        If (checkValue(datefield)) Then
            Select Case datefield
                Case "Created", "Updated", "NextCall", "CallBack", "LastContacted", "LastRemarketed", "Lock"
                    Dim strDate As String = getAnyField(datefield & "Date", "tblapplicationstatus", "AppID", AppID)
                    If (checkValue(strDate)) Then
                        strDateField = strDate
                    End If
                Case Else
                    strQry = _
                        "SELECT ApplicationStatusDate FROM tblapplicationstatusdates " & _
                        "WHERE ApplicationStatusDateAppID = @AppID AND ApplicationStatusDateName = @ApplicationStatusDateName AND CompanyID = @CompanyID"
                    Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                       New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID), _
                                                       New SqlParameter("@ApplicationStatusDateName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "ApplicationStatusDateName", DataRowVersion.Current, datefield)}
                    Dim objDataBase As New DatabaseManager
                    Dim objDataSet As DataSet = objDataBase.executeReader(strQry, "tblapplicationstatusdates", CommandType.Text, arrParams)
                    Dim dsStatusDates As DataTable = objDataSet.Tables("tblapplicationstatusdates")
                    If (dsStatusDates.Rows.Count > 0) Then
                        For Each Row As DataRow In dsStatusDates.Rows
                            If (checkValue(Row.Item("ApplicationStatusDate").ToString)) Then
                                strDateField = Row.Item("ApplicationStatusDate").ToString
                            End If
                        Next
                    End If
                    objDataSet.Clear()
                    objDataSet = Nothing
                    dsStatusDates = Nothing
                    objDataBase = Nothing
            End Select
        Else
            strDateField = ""
        End If
        Return strDateField
    End Function

    Shared Function checkBusinessDateField(ByVal BusinessObjectID As String, ByVal datefield As String) As Boolean
        Dim strQry As String = "", boolDateField As Boolean = False
        If (checkValue(datefield)) Then
            Select Case datefield
                Case "Created", "Updated", "NextCall"
                    Dim strDate As String = getAnyField(datefield & "Date", "tblbusinessobjectstatus", "BusinessObjectID", BusinessObjectID)
                    If (checkValue(strDate)) Then
                        boolDateField = True
                    End If
                Case Else
                    strQry = _
                        "SELECT BusinessObjectStatusDate FROM tblbusinessobjectstatusdates " & _
                        "WHERE BusinessObjectID = @BusinessObjectID AND BusinessObjectStatusDateName = @BusinessObjectStatusDateName AND CompanyID = @CompanyID"
                    Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                       New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "BusinessObjectID", DataRowVersion.Current, BusinessObjectID), _
                                                       New SqlParameter("@BusinessObjectStatusDateName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "BusinessObjectStatusDateName", DataRowVersion.Current, datefield)}
                    Dim objDataBase As New DatabaseManager
                    Dim objDataSet As DataSet = objDataBase.executeReader(strQry, "tblbusinessobjectstatusdates", CommandType.Text, arrParams)
                    Dim dsStatusDates As DataTable = objDataSet.Tables("tblbusinessobjectstatusdates")
                    If (dsStatusDates.Rows.Count > 0) Then
                        For Each Row As DataRow In dsStatusDates.Rows
                            If (checkValue(Row.Item("BusinessObjectStatusDate").ToString)) Then
                                boolDateField = True
                            End If
                        Next
                    End If
                    objDataSet.Clear()
                    objDataSet = Nothing
                    dsStatusDates = Nothing
                    objDataBase = Nothing
            End Select
        Else
            boolDateField = True
        End If
        Return boolDateField
    End Function

    Shared Function getBusinessDateField(ByVal BusinessObjectID As String, ByVal datefield As String) As String
        Dim strQry As String = "", strDateField As String = ""
        If (checkValue(datefield)) Then
            Select Case datefield
                Case "Created", "Updated", "NextCall"
                    Dim strDate As String = getAnyField(datefield & "Date", "tblbusinessobjectstatus", "BusinessObjectID", BusinessObjectID)
                    If (checkValue(strDate)) Then
                        strDateField = strDate
                    End If
                Case Else
                    strQry = _
                        "SELECT BusinessObjectStatusDate FROM tblbusinessobjectstatusdates " & _
                        "WHERE BusinessObjectID = @BusinessObjectID AND BusinessObjectStatusDateName = @BusinessObjectStatusDateName AND CompanyID = @CompanyID"
                    Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                       New SqlParameter("@BusinessObjectID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "BusinessObjectID", DataRowVersion.Current, BusinessObjectID), _
                                                       New SqlParameter("@BusinessObjectStatusDateName", SqlDbType.NVarChar, 100, ParameterDirection.Input, False, 0, 0, "BusinessObjectStatusDateName", DataRowVersion.Current, datefield)}
                    Dim objDataBase As New DatabaseManager
                    Dim objDataSet As DataSet = objDataBase.executeReader(strQry, "tblbusinessobjectstatusdates", CommandType.Text, arrParams)
                    Dim dsStatusDates As DataTable = objDataSet.Tables("tblbusinessobjectstatusdates")
                    If (dsStatusDates.Rows.Count > 0) Then
                        For Each Row As DataRow In dsStatusDates.Rows
                            If (checkValue(Row.Item("BusinessObjectStatusDate").ToString)) Then
                                strDateField = Row.Item("BusinessObjectStatusDate").ToString
                            End If
                        Next
                    End If
                    objDataSet.Clear()
                    objDataSet = Nothing
                    dsStatusDates = Nothing
                    objDataBase = Nothing
            End Select
        Else
            strDateField = ""
        End If
        Return strDateField
    End Function

    Shared Function replaceTags(ByVal str As String, ByVal AppID As String) As String
        ' *** Remove spaces (for PDF checkbox values) *** '
        Dim boolReplaceSpaces As Boolean = False, boolGrid As Boolean = False, boolUpper As Boolean = False, strMid As String = "", strMidStart As String = "", strMidLength As String = ""
        If (regexTest("\{(DS:|SD:|US:|QS:|QT:){0,3}[A-Za-z0-9]{0,100}:NOSPACE\}", str)) Then
            str = Replace(str, ":NOSPACE", "")
            boolReplaceSpaces = True
        End If
        ' *** Upper Case *** '
        If (regexTest("\{(DS:|SD:|US:|QS:|QT:){0,3}[A-Za-z0-9]{0,100}:UPPER\}", str)) Then
            str = Replace(str, ":UPPER", "")
            boolUpper = True
        End If
        ' *** Render grid *** '
        If (regexTest("\{(DS:|SD:|US:|QS:|QT:){0,3}[A-Za-z0-9]{0,100}:GRID\}", str)) Then
            str = Replace(str, ":GRID", "")
            boolGrid = True
        End If
        ' *** MID of string *** '
        If (regexTest("\{(DS:|SD:|US:|QS:|QT:){0,3}[A-Za-z0-9]{0,100}:MID\([0-9]{1,2},[0-9]{1,2}\)\}", str)) Then
            strMid = regexFirstMatch("(MID\([0-9]{1,2},[0-9]{1,2}\))", str)
            str = Replace(str, ":" & strMid, "")
            strMidStart = regexFirstMatch("MID\(([0-9]{1,2}),[0-9]{1,2}\)", strMid)
            strMidLength = regexFirstMatch("MID\([0-9]{1,2},([0-9]{1,2})\)", strMid)
        End If
        ' *** Data store values *** '
        Dim objMatches As MatchCollection = Regex.Matches(str, "\{DS:[A-Za-z0-9]{0,100}\}")
        Dim strField As String = ""
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{DS:", "")
            strField = Replace(strField, "}", "")
            Try
                If (boolReplaceSpaces) Then
                    str = Replace(str, objMatches.Item(x).Value, Replace(getAnyFieldFromDataStore(strField, AppID), " ", ""))
                ElseIf (boolGrid) Then
                    ' *** Change commas to line breaks to allow grid to line up *** '
                    str = Replace(str, objMatches.Item(x).Value, Replace(getAnyFieldFromDataStore(strField, AppID), ",", vbCrLf))
                ElseIf (boolUpper) Then
                    ' *** Change To Upper Case *** '
                    str = Replace(str, objMatches.Item(x).Value, UCase(getAnyFieldFromDataStore(strField, AppID)))
                Else
                    str = Replace(str, objMatches.Item(x).Value, getAnyFieldFromDataStore(strField, AppID))
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** Quote values *** '
        objMatches = Regex.Matches(str, "\{QT:[A-Za-z0-9]{0,100}\}")
        strField = ""
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{QT:", "")
            strField = Replace(strField, "}", "")
            Try
                Dim strQuote As String = getAnyField("QuoteData", "tblquotes", "QuoteID", AppID)
                Dim strQuoteValue As String = queryStringValue(strField, strQuote)
                If (boolReplaceSpaces) Then
                    str = Replace(str, objMatches.Item(x).Value, Replace(strQuoteValue, " ", ""))
                ElseIf (boolUpper) Then
                    str = Replace(str, objMatches.Item(x).Value, UCase(strQuoteValue))
                Else
                    str = Replace(str, objMatches.Item(x).Value, strQuoteValue)
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** GET/POST values *** '
        objMatches = Regex.Matches(str, "\{QS:[A-Za-z0-9]{0,100}\}")
        strField = ""
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{QS:", "")
            strField = Replace(strField, "}", "")
            Try
                If (boolReplaceSpaces) Then
                    str = Replace(str, objMatches.Item(x).Value, Replace(HttpContext.Current.Request(strField), " ", ""))
                ElseIf (boolUpper) Then
                    ' *** Change To Upper Case *** '
                    str = Replace(str, objMatches.Item(x).Value, UCase(HttpContext.Current.Request(strField)))
                Else
                    str = Replace(str, objMatches.Item(x).Value, HttpContext.Current.Request(strField))
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** Milestone values *** '
        objMatches = Regex.Matches(str, "\{SD:[A-Za-z0-9]{0,100}\}")
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{SD:", "")
            strField = Replace(strField, "}", "")
            Try
                Dim strDate As String = getDateField(AppID, strField)
                If (checkValue(strDate)) Then
                    str = Replace(str, objMatches.Item(x).Value, Left(CDate(strDate), 10))
                Else
                    str = Replace(str, objMatches.Item(x).Value, "")
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** User store values *** '
        objMatches = Regex.Matches(str, "\{US:[A-Za-z0-9]{0,100}\}")
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{US:", "")
            strField = Replace(strField, "}", "")
            Try
                If (boolReplaceSpaces) Then
                    str = Replace(str, objMatches.Item(x).Value, Replace(getAnyFieldFromUserStore(strField, Config.DefaultUserID), " ", ""))
                Else
                    str = Replace(str, objMatches.Item(x).Value, getAnyFieldFromUserStore(strField, Config.DefaultUserID))
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** Application values *** '
        objMatches = Regex.Matches(str, "\{[A-Za-z0-9]{0,100}\}")
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{", "")
            strField = Replace(strField, "}", "")
            Try
                Select Case strField
                    Case "Date"
                        str = Replace(str, objMatches.Item(x).Value, Config.DefaultDate)
                    Case "UserName"
                        str = Replace(str, objMatches.Item(x).Value, getAnyField("UserFullName", "tblusers", "UserID", Config.DefaultUserID))
                    Case "SMSValidationCode", "EmailValidationCode"
                        str = Replace(str, objMatches.Item(x).Value, getValidationCode(AppID, strField))
                    Case "Password"
                        str = Replace(str, objMatches.Item(x).Value, Encryption.encrypt(getAnyField(strField, "vwexportapplication", "AppID", AppID)))
                    Case "App2FullName"
                        Dim strDataBaseField As String = getAnyField("App2FullName", "vwexportapplication", "AppID", AppID)
                        If checkValue(strDataBaseField) Then
                            str = Replace(str, objMatches.Item(x).Value, " and " & strDataBaseField)
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "")
                        End If
                    Case "App2Salutation"
                        Dim strDataBaseField As String = getAnyField("App2Salutation", "vwexportapplication", "AppID", AppID)
                        If checkValue(strDataBaseField) Then
                            str = Replace(str, objMatches.Item(x).Value, " and " & strDataBaseField)
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "")
                        End If
                    Case "Applicant2"
                        Dim strDataBaseField As String = getAnyField("App2FullName", "vwexportapplication", "AppID", AppID)
                        If checkValue(strDataBaseField) Then
                            str = Replace(str, objMatches.Item(x).Value, " and " & strDataBaseField & " (b. " & getAnyField("App2DOB", "vwexportapplication", "AppID", AppID) & ")")
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "")
                        End If
                    Case "OverdueDays"
                        Dim dtePaymentDue As Date = getDateField(AppID, "PaymentDue")
                        If (IsDate(dtePaymentDue)) Then
                            str = Replace(str, objMatches.Item(x).Value, DateDiff(DateInterval.Day, dtePaymentDue, Config.DefaultDateTime))
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "0")
                        End If
                    Case "QuoteDate"
                        Dim strDataBaseField As String = getAnyField(strField, "vwexportapplication", "AppID", AppID)
                        If checkValue(strDataBaseField) Then
                            str = Replace(str, objMatches.Item(x).Value, "You were previously quoted on " & strDataBaseField & ".")
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "")
                        End If
                    Case "CallBackDate"
                        Dim strDataBaseField As String = getAnyField(strField, "vwexportapplication", "AppID", AppID)
                        If checkValue(strDataBaseField) Then
                            str = Replace(str, objMatches.Item(x).Value, "You arranged a call back for " & strDataBaseField & ".")
                        Else
                            str = Replace(str, objMatches.Item(x).Value, "")
                        End If
                    Case "NotesHTML"
                        Dim strNotes As String = ""
                        Dim strSQL As String = "SELECT NTS.NoteID, NTS.Note, NTS.CreatedDate, USR.UserFullName FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                            "WHERE AppID = @AppID AND NTS.CompanyID = @CompanyID AND NoteActive = 1 ORDER BY NTS.CreatedDate DESC"
                        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
                        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams).returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            For Each Row As DataRow In dsCache.Rows
                                strNotes += "<p><strong>" & Row.Item("UserFullName") & "</strong>: " & Row.Item("Note") & "</p>"
                                strNotes += "<p>" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</p><br />"
                            Next
                        End If
                        dsCache = Nothing
                        str = Replace(str, objMatches.Item(x).Value, strNotes)
                    Case "Notes"
                        Dim strNotes As String = ""
                        Dim strSQL As String = "SELECT NTS.NoteID, NTS.Note, NTS.CreatedDate, USR.UserFullName FROM tblnotes NTS INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID " & _
                            "WHERE AppID = @AppID AND NTS.CompanyID = @CompanyID AND NoteActive = 1 ORDER BY NTS.CreatedDate DESC"
                        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                                           New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
                        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "", CommandType.Text, arrParams).returnCache()
                        If (dsCache.Rows.Count > 0) Then
                            For Each Row As DataRow In dsCache.Rows
                                strNotes += Row.Item("UserFullName") & ": " & Row.Item("Note") & vbCrLf
                                strNotes += Replace(ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")), "&nbsp;", " ") & vbCrLf & vbCrLf
                            Next
                        End If
                        dsCache = Nothing
                        str = Replace(str, objMatches.Item(x).Value, strNotes)
                    Case Else
                        If (boolReplaceSpaces) Then
                            str = Replace(str, objMatches.Item(x).Value, Replace(getAnyField(strField, "vwexportapplication", "AppID", AppID), " ", ""))
                        ElseIf (boolUpper) Then
                            str = Replace(str, objMatches.Item(x).Value, UCase(getAnyField(strField, "vwexportapplication", "AppID", AppID)))
                        Else
                            str = Replace(str, objMatches.Item(x).Value, getAnyField(strField, "vwexportapplication", "AppID", AppID))
                        End If
                End Select
                Try
                    If (checkValue(strMidStart)) Then
                        str = Mid(str, strMidStart, strMidLength)
                    End If
                Catch ex As System.ArgumentException
                    ' Do nothing
                End Try
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
        Next
        Return str
    End Function

    Shared Function replaceBusinessTags(ByVal str As String, ByVal BusinessObjectID As String) As String
        ' *** Remove spaces (for PDF checkbox values) *** '
        Dim boolReplaceSpaces As Boolean = False, boolGrid As Boolean = False, boolUpper As Boolean = False, strMid As String = "", strMidStart As String = "", strMidLength As String = ""
        If (regexTest("\{(DS:|SD:|US:){0,3}[A-Za-z0-9]{0,100}:NOSPACE\}", str)) Then
            str = Replace(str, ":NOSPACE", "")
            boolReplaceSpaces = True
        End If
        ' *** Upper Case *** '
        If (regexTest("\{(DS:|SD:|US:){0,3}[A-Za-z0-9]{0,100}:UPPER\}", str)) Then
            str = Replace(str, ":UPPER", "")
            boolUpper = True
        End If
        ' *** Render grid *** '
        If (regexTest("\{(DS:|SD:|US:){0,3}[A-Za-z0-9]{0,100}:GRID\}", str)) Then
            str = Replace(str, ":GRID", "")
            boolGrid = True
        End If
        ' *** MID of string *** '
        If (regexTest("\{(DS:|SD:|US:){0,3}[A-Za-z0-9]{0,100}:MID\([0-9]{1,2},[0-9]{1,2}\)\}", str)) Then
            strMid = regexFirstMatch("(MID\([0-9]{1,2},[0-9]{1,2}\))", str)
            str = Replace(str, ":" & strMid, "")
            strMidStart = regexFirstMatch("MID\(([0-9]{1,2}),[0-9]{1,2}\)", strMid)
            strMidLength = regexFirstMatch("MID\([0-9]{1,2},([0-9]{1,2})\)", strMid)
        End If
        ' *** Milestone values *** '
        Dim objMatches As MatchCollection = Regex.Matches(str, "\{SD:[A-Za-z0-9]{0,100}\}")
        Dim strField As String = ""
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{SD:", "")
            strField = Replace(strField, "}", "")
            Try
                Dim strDate As String = getBusinessDateField(BusinessObjectID, strField)
                If (checkValue(strDate)) Then
                    str = Replace(str, objMatches.Item(x).Value, Left(CDate(strDate), 10))
                Else
                    str = Replace(str, objMatches.Item(x).Value, "")
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** User store values *** '
        objMatches = Regex.Matches(str, "\{US:[A-Za-z0-9]{0,100}\}")
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{US:", "")
            strField = Replace(strField, "}", "")
            Try
                If (boolReplaceSpaces) Then
                    str = Replace(str, objMatches.Item(x).Value, Replace(getAnyFieldFromUserStore(strField, Config.DefaultUserID), " ", ""))
                Else
                    str = Replace(str, objMatches.Item(x).Value, getAnyFieldFromUserStore(strField, Config.DefaultUserID))
                End If
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
            Try
                If (checkValue(strMidStart)) Then
                    str = Mid(str, strMidStart, strMidLength)
                End If
            Catch ex As System.ArgumentException
                ' Do nothing
            End Try
        Next
        ' *** Application values *** '
        objMatches = Regex.Matches(str, "\{[A-Za-z0-9]{0,100}\}")
        For x As Integer = 0 To objMatches.Count - 1
            strField = objMatches.Item(x).Value
            strField = Replace(strField, "{", "")
            strField = Replace(strField, "}", "")
            Try
                Select Case strField
                    Case "Date"
                        str = Replace(str, objMatches.Item(x).Value, Config.DefaultDate)
                    Case "UserName"
                        str = Replace(str, objMatches.Item(x).Value, getAnyField("UserFullName", "tblusers", "UserID", Config.DefaultUserID))

                    Case Else
                        If (boolReplaceSpaces) Then
                            str = Replace(str, objMatches.Item(x).Value, Replace(getAnyField(strField, "vwbusinessobjects", "BusinessObjectID", BusinessObjectID), " ", ""))
                        ElseIf (boolUpper) Then
                            str = Replace(str, objMatches.Item(x).Value, UCase(getAnyField(strField, "vwbusinessobjects", "BusinessObjectID", BusinessObjectID)))
                        Else
                            str = Replace(str, objMatches.Item(x).Value, getAnyField(strField, "vwbusinessobjects", "BusinessObjectID", BusinessObjectID))
                        End If
                End Select
                Try
                    If (checkValue(strMidStart)) Then
                        str = Mid(str, strMidStart, strMidLength)
                    End If
                Catch ex As System.ArgumentException
                    ' Do nothing
                End Try
            Catch ex As SqlDatabaseException
                str = Replace(str, objMatches.Item(x).Value, "<strong>[Warning: No field found]</strong>")
            End Try
        Next
        Return str
    End Function

    Shared Function dateTags(ByVal str As String) As String
        Dim strReplacedTags As String = str
        ' Future dates
        strReplacedTags = Replace(strReplacedTags, "{Today}", Config.DefaultDate)
        strReplacedTags = Replace(strReplacedTags, "{EndMonth}", Config.DefaultDate.AddMonths(1).AddDays(-Config.DefaultDate.Day))
        strReplacedTags = Replace(strReplacedTags, "{NextFiveDays}", Config.DefaultDate.AddDays(5))
        strReplacedTags = Replace(strReplacedTags, "{NextWeek}", Config.DefaultDate.AddDays(7))
        strReplacedTags = Replace(strReplacedTags, "{NextMonth}", Config.DefaultDate.AddMonths(1))
        'Past Dates
        strReplacedTags = Replace(strReplacedTags, "{Yesterday}", Config.DefaultDate.AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FiveDays}", Config.DefaultDate.AddDays(-5))
        strReplacedTags = Replace(strReplacedTags, "{LastWeek}", Config.DefaultDate.AddDays(-7))
        strReplacedTags = Replace(strReplacedTags, "{LastYear}", Config.DefaultDate.AddYears(-1))
        strReplacedTags = Replace(strReplacedTags, "{LastTwoYears}", Config.DefaultDate.AddYears(-2))
        strReplacedTags = Replace(strReplacedTags, "{LastTenYears}", Config.DefaultDate.AddYears(-10))
        strReplacedTags = Replace(strReplacedTags, "{FirstMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstYear}", New DateTime(Config.DefaultDate.Year, 1, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddMonths(-1))
        strReplacedTags = Replace(strReplacedTags, "{EndLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfWeek}", Config.DefaultDate.AddDays(1).AddDays(-Config.DefaultDate.AddDays(1).DayOfWeek))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfYear}", Config.DefaultDate.AddDays(-Config.DefaultDate.DayOfYear))
        Return strReplacedTags
    End Function

    Shared Function getFileTypeImage(ByVal ext As String) As String
        Dim strFileTypeImage As String = ""
        Select Case ext
            Case "jpg", "jpeg", "gif", "png", "tif", "tiff", "eps"
                strFileTypeImage = "doc_image.png"
            Case "doc", "docx"
                strFileTypeImage = "doc_word.png"
            Case "csv"
                strFileTypeImage = "doc_excel_csv.png"
            Case "xls", "xlsx"
                strFileTypeImage = "doc_excel_table.png"
            Case "pdf"
                strFileTypeImage = "doc_pdf.png"
            Case "mp3"
                strFileTypeImage = "doc_mp3.png"
        End Select
        Return strFileTypeImage
    End Function

    Shared Function getNodeText(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String) As String
        Dim strNodeText As String = ""
        Dim objNode As XmlNode = Nothing
        If (Not nsm Is Nothing) Then
            objNode = el.SelectSingleNode(path, nsm)
        Else
            objNode = el.SelectSingleNode(path)
        End If
        If Not (objNode Is Nothing) Then
            strNodeText = objNode.InnerText
        End If
        Return strNodeText
    End Function

    Shared Function getNodeListText(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String) As String
        Dim strNodeText As String = ""
        Dim objNodeList As XmlNodeList = el.SelectNodes(path, nsm)
        If (objNodeList.Count > 0) Then
            Dim x As Integer = 0
            For Each objNode As XmlNode In objNodeList
                If (x = 0) Then
                    strNodeText += objNode.InnerText
                Else
                    strNodeText += "<br />" & objNode.InnerText
                End If
                x += 1
            Next
        End If
        Return strNodeText
    End Function

    Shared Function getNodeAttribute(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String, ByVal attr As String) As String
        Dim strNodeAttribute As String = ""
        Dim objNode As XmlNode = el.SelectSingleNode(path, nsm)
        If Not (objNode Is Nothing) Then
            strNodeAttribute = objNode.Attributes(attr).Value
        End If
        Return strNodeAttribute
    End Function

    Shared Function getValidationCode(AppID As String, type As String) As String
        Dim strValidationCode As String = New Random().Next(1000, 9999).ToString ' generatePassword(6)
        Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, CompanyID), _
                                   New SqlParameter("@AppID", SqlDbType.Int, 7, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        executeNonQuery("UPDATE tblapplications SET " & type & " = " & formatField(strValidationCode, "", "") & " WHERE AppID = @AppID AND CompanyID = @CompanyID", CommandType.Text, arrParams)
        CommonSave.saveUpdatedDate(AppID, DefaultUserID, "Updated")
        Return strValidationCode
    End Function

    Shared Sub checkBlackList(ByVal ip As String, url As String)
        Dim boolBlackListed As Boolean
        Dim strSQL As String = "SELECT BlackListID FROM tblblacklist WHERE BlackListAddress = '" & ip & "' AND BlackListActive = 1"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolBlackListed = True
        Else
            boolBlackListed = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolBlackListed = True) Then
            If (checkValue(url)) Then
                HttpContext.Current.Response.Redirect(url)
            Else
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

    Shared Function checkSQLInjection(str As String) As Boolean
        If (regexTest("DROP |EXECUTE |EXEC |SELECT |TRUNCATE |DELETE |UPDATE |INSERT ", str)) Then
            'postEmail("", "devteam@engagedcrm.co.uk", "SQL Injection", "SQL injection statement: " & str & " from host " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
            Return True
        Else
            Return False
        End If
    End Function

    Shared Function checkRequestSQLInjection(ip As String) As Boolean
        Dim boolInjection As Boolean = False
        For Each Item In HttpContext.Current.Request.QueryString
            If (checkValue(Item)) Then
                If (checkSQLInjection(HttpContext.Current.Request.QueryString(Item))) Then
                    CommonSave.blackList(ip)
                    boolInjection = True
                End If
            End If
            If (boolInjection) Then Exit For
        Next
        If (Not boolInjection) Then
            For Each Item In HttpContext.Current.Request.Form
                If (checkValue(Item)) Then
                    If (checkSQLInjection(HttpContext.Current.Request.QueryString(Item))) Then
                        CommonSave.blackList(ip)
                        boolInjection = True
                    End If
                End If
                If (boolInjection) Then Exit For
            Next
        End If
        Return boolInjection
    End Function

    Shared Function checkReferer() As Boolean
        ' Check refering page is in same domain
        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), Config.CurrentDomain) = 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Shared Sub logRequestData()

        Dim intCompanyID As String = CompanyID
        If (Not checkValue(intCompanyID)) Then intCompanyID = 0
        Dim intUserID As String = Config.DefaultUserID
        If (Not checkValue(intUserID)) Then intUserID = 0
        Dim strPostData As String = "", strGetData As String = ""

        Dim x As Integer = 0
        For Each Item In HttpContext.Current.Request.Form
            If (x = 0) Then
                If (checkValue(Item)) Then
                    strPostData = Item & "=" & encodeURL(HttpContext.Current.Request.Form(Item))
                End If
            Else
                If (checkValue(Item)) Then
                    strPostData += "&" & Item & "=" & encodeURL(HttpContext.Current.Request.Form(Item))
                End If
            End If
            x += 1
        Next
        Dim y As Integer = 0
        For Each Item In HttpContext.Current.Request.QueryString
            If (y = 0) Then
                If (checkValue(Item)) Then
                    strGetData = Item & "=" & encodeURL(HttpContext.Current.Request.QueryString(Item))
                End If
            Else
                If (checkValue(Item)) Then
                    strGetData += "&" & Item & "=" & encodeURL(HttpContext.Current.Request.QueryString(Item))
                End If
            End If
            y += 1
        Next

        'Dim strTimeStamp As String = Config.DefaultDateTime.Year & Config.DefaultDateTime.Month & Config.DefaultDateTime.Day
        'Dim strRequestLogFile = HttpContext.Current.Server.MapPath("/net/csvfiles/logs/request_logs_" & CompanyID & "_" & strTimeStamp & ".csv")
        'Dim strLine As String = CompanyID & "," & Config.DefaultUserID & ",""" & strPostData & """,""" & strGetData & """,""" & HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & """,""" & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & """," & Config.DefaultDateTime
        'If (File.Exists(strRequestLogFile)) Then
        '    File.AppendAllText(strRequestLogFile, strLine & vbCrLf)
        'Else
        '    File.AppendAllText(strRequestLogFile, "CompanyID,UserID,PostData,GetData,URL,IPAddress,Date" & vbCrLf)
        'End If

        'Dim strQry As String = "INSERT INTO [crm-logs].dbo.tblrequestlogs (RequestCompanyID, RequestUserID, RequestPostData, RequestGetData, RequestURL, RequestIPAddress) VALUES (" & _
        '    "@CompanyID" & ", " & _
        '    "@UserID" & ", " & _
        '    formatField(strPostData, "", "") & ", " & _
        '    formatField(strGetData, "", "") & ", " & _
        '    formatField(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "", "") & ", " & _
        '    formatField(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), "", "") & ") "
        'Dim arrParams As SqlParameter() = {New SqlParameter("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "CompanyID", DataRowVersion.Current, intCompanyID), _
        '                              New SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "UserID", DataRowVersion.Current, intUserID)}
        'Dim objDatabase As DatabaseManager = New DatabaseManager
        'objDatabase.executeNonQuery(strQry, CommandType.Text, arrParams, "")
        'objDatabase = Nothing

    End Sub

    Shared Function getOutOfOfficeStatus(ByVal UserOutOfOfficeID As Integer) As String
        Select Case UserOutOfOfficeID
            Case OutOfOfficeStatus.Absent
                Return "Absent"
            Case OutOfOfficeStatus.Away
                Return "Away"
            Case OutOfOfficeStatus.Break
                Return "Break"
            Case OutOfOfficeStatus.Holiday
                Return "Holiday"
            Case OutOfOfficeStatus.Lunch
                Return "Lunch"
            Case OutOfOfficeStatus.Meeting
                Return "Meeting"
            Case OutOfOfficeStatus.NotReady
                Return "Not Ready"
            Case OutOfOfficeStatus.Ready
                Return "Ready"
            Case Else
                Return "Ready"
        End Select
    End Function

    Shared Function stringToXML(ByVal xml As String) As XmlDocument
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(xml)
        Return xmlDoc
    End Function

    Shared Function getPaymentStatus(ByVal st As String) As String
        Select Case st
            Case "0"
                Return "<a href=""#""><i class=""icon-circle-o-notch fa-1x ui-tooltip"" title=""Pending""></i></a>"
            Case "1"
                Return "<a href=""#""><i class=""icon-check fa-1x ui-tooltip"" title=""Successful""></i></a>"
            Case "2"
                Return "<a href=""#""><i class=""icon-exclamation-triangle fa-1x ui-tooltip"" title=""Failed""></i></a>"
            Case "3"
                Return "<a href=""#""><i class=""icon-backward fa-1x ui-tooltip"" title=""Bounced""></i></a>"
            Case "4"
                Return "<a href=""#""><i class=""icon-times fa-1x ui-tooltip"" title=""Cancelled""></i></a>"
            Case Else
                Return "<a href=""#""><i class=""icon-circle-o-notch fa-1x ui-tooltip"" title=""Pending""></i></a>"
        End Select
    End Function

    Shared Function getPaymentMethod(ByVal pmtMethod As String) As String
        Select Case pmtMethod
            Case "1"
                Return "DEB"
            Case "2"
                Return "BACS"
            Case "3"
                Return "FPO"
            Case "4"
                Return "Man."
            Case Else
                Return "Man."
        End Select
    End Function

    Shared Function getCreditHistoryType(ByVal code As String, ByVal provider As String) As String
        Dim strSQL As String = "SELECT TOP 1 CreditHistoryName FROM tblcredithistorytypes WHERE CreditHistoryProvider = '" & provider & "' AND CreditHistoryCode = '" & code & "'"
        Dim strResult As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
        Return strResult
    End Function

    Shared Function hashString256(ByVal key As String, ByVal text As String) As String
        Dim objHash As HMACSHA256 = New HMACSHA256(Encoding.UTF8.GetBytes(key))
        Dim bHash As Byte() = objHash.ComputeHash(Encoding.UTF8.GetBytes(text))
        Return encodeURL(Convert.ToBase64String(bHash))
    End Function

    Shared Function getUserReference(ByVal name As String) As String
        Dim strUserReference As String = ""
        If (checkValue(name)) Then
            strUserReference = Left(name, 1)
            Dim arrUserFullName As Array = Split(name, " ")
            strUserReference += Left(arrUserFullName(UBound(arrUserFullName)), 1) & Right(arrUserFullName(UBound(arrUserFullName)), 1)
            Dim x As Integer = 0
            If (userReferenceExists(strUserReference)) Then
                While userReferenceExists(strUserReference) And x < 20
                    x += 1
                    arrUserFullName = Split(name, " ")
                    strUserReference = Left(name, 1)
                    strUserReference += Right(Left(arrUserFullName(UBound(arrUserFullName)), 1 + x), 1) & Right(arrUserFullName(UBound(arrUserFullName)), 1)
                End While
                Return UCase(strUserReference)
            Else
                Return UCase(strUserReference)
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function userReferenceExists(ByVal ref As String) As Boolean
        Dim boolExists As Boolean = False
        Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserReference = '" & ref & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                boolExists = True
            Next
        Else
            boolExists = False
        End If
        dsUsers.Clear()
        dsUsers = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolExists
    End Function

    Shared Function getUserName(ByVal name As String) As String
        Dim strUserName As String = ""
        Dim arrUserFullName As Array = Split(name, " ")
        Dim x As Integer = 0
        For Each Item As String In arrUserFullName
            If (x = 0) Then
                strUserName += LCase(Item)
            Else
                strUserName += "." & LCase(Item)
            End If
            x += 1
        Next
        If (userNameExists(strUserName)) Then
            x = 1
            While userNameExists(strUserName) And x < 20
                strUserName += x.ToString
                x += 1
            End While
        Else
            Return strUserName
        End If
        Return strUserName
    End Function

    Shared Function userNameExists(ByVal name As String) As Boolean
        Dim boolExists As Boolean = False
        Dim strSQL As String = "SELECT UserID FROM tblusers WHERE UserName = '" & name & "' AND CompanyID = '" & CompanyID & "'"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblusers")
        Dim dsUsers As DataTable = objDataSet.Tables("tblusers")
        If (dsUsers.Rows.Count > 0) Then
            For Each Row As DataRow In dsUsers.Rows
                boolExists = True
            Next
        Else
            boolExists = False
        End If
        dsUsers.Clear()
        dsUsers = Nothing
        objDataSet = Nothing
        objDatabase = Nothing
        Return boolExists
    End Function

    Shared Function queryStringValue(val As String, str As String) As String
        Return regexFirstMatch("[&]{0,1}" & val & "=([0-9A-Z, \/\-\+]{1,255})", str)
    End Function

End Class
